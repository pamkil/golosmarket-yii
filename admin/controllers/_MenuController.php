<?php

namespace admin\controllers;

use admin\models\setting\Menu;
use common\models\settings\MenuSearch;
use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class MenuController extends SecurityController
{
    public function actionIndex($id = null)
    {
        if (!Yii::$app->user->can('viewMenu')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }
        if (isset($id)) {
            $model = Menu::find()
                ->with('defaultTranslation')
                ->where(['id' => $id])
                ->one();
        } else {
            $model = Menu::find()->roots()->one();

            $this->view->title = 'Меню';

            $this->view->params['breadcrumbs'][] = [
                'label' => $this->view->title
            ];
        }
        /**
         * @var Menu $model
         */
        if (!$model) {
            throw new NotFoundHttpException;
        }

        $parents = $model->parents()->with('translations')->all();
        if (isset($parents) && $model->depth != 0) {
            if ($model->depth == 0) {
                $this->view->title = 'Меню';
            }

            /** @var Menu[] $parents */
            foreach ($parents as $parent) {
                if ($parent->depth == 0) {
                    $this->view->params['breadcrumbs'][] = [
                        'label' => 'Меню',
                        'url' => Url::to(['menu/index']),
                    ];
                } else {
                    $this->view->params['breadcrumbs'][] = [
                        'label' => $parent->name,
                        'url' => Url::to(['menu/index', 'id' => $parent->id]),
                    ];
                }
            }

            $this->view->title = 'Меню "' . $model->name . '"';
        }

        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search((int)$id, Yii::$app->request->get());


        return $this->render('index', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('updateMenu')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Menu $model */
        $model = Menu::findOne(['id' => $id]);
        if (!$model) {
            throw new NotFoundHttpException;
        }

        if ($post = Yii::$app->request->post()) {
            return $this->saveMenu($post, $model);
        }

        return $this->viewMenu($model);
    }

    public function actionCreate($id)
    {
        if (!Yii::$app->user->can('updateMenu')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }
        $parentModel = Menu::findOne(['id' => $id]);
        if (!$parentModel) {
            throw new NotFoundHttpException;
        }
        /* @var Menu $model */
        $model = new Menu();
        if ($post = Yii::$app->request->post()) {
            return $this->saveMenu($post, $model, $parentModel);
        }

        return $this->viewMenu($model);
    }

    private function saveMenu(array $post, Menu $model, Menu $parent = null)
    {
        if ($model->load($post) && Model::loadMultiple($model->getVariationModels(), $post) && $model->isNewRecord ? $model->appendTo($parent) : $model->save()) {
            Yii::$app->session->addFlash('success', 'Успешно обновлено');
        }

        foreach ($model->getErrors() as $error) {
            Yii::$app->session->addFlash('error', implode(', ', $error));
        }

        if (isset($post['fileId']) && Yii::$app->request->isAjax && $returnEr = $model->getErrorAction()) {
            return $returnEr;
        } else {
            return $this->redirectAjax('menu/update', [
                'id' => $model->id
            ]);
        }
    }

    private function viewMenu(Menu $model)
    {
        if (isset($model->date)) {
            $model->date = Yii::$app->formatter->asDatetime($model->date, 'php:d.m.Y H:i:s');
        }

        $parents = $model->parents()->with('translations')->all();
        if (isset($parents)) {
            if(empty($model->name)) {
                $breadcrumb = $this->view->title = 'Новый раздел меню';
            } else {
                $this->view->title = 'Меню "' . $model->name . '"';
                $breadcrumb = $model->name;
            }
            /** @var Menu[] $parents */
            foreach ($parents as $parent) {
                if ($parent->depth == 0) {
                    $this->view->params['breadcrumbs'][] = [
                        'label' => 'Меню',
                        'url' => Url::to(['menu/index']),
                    ];
                } else {
                    $this->view->params['breadcrumbs'][] = [
                        'label' => $parent->name,
                        'url' => Url::to(['menu/index', 'id' => $parent->id]),
                    ];
                }
            }

            $this->view->params['breadcrumbs'][] = [
                'label' => $breadcrumb
            ];
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    public function actionMoveUp($id)
    {
        if (!Yii::$app->user->can('updateMenu')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Menu $model */
        $model = Menu::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $parent = $model->parents(1)->one();
        $prev = $model->prev()->one();
        if ($prev) {
            $model->insertBefore($prev);
        }

        if ($parent && $parent->depth > 0) {
            return $this->redirectAjax('menu/index', ['id' => $parent->id]);
        }

        return $this->redirectAjax('menu/index');
    }

    public function actionMoveDown($id)
    {
        if (!Yii::$app->user->can('updateMenu')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Menu $model */
        $model = Menu::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $parent = $model->parents(1)->one();
        $next = $model->next()->one();
        if ($next) {
            $next->insertBefore($model);
        }

        if ($parent && $parent->depth > 0) {
            return $this->redirectAjax('menu/index', ['id' => $parent->id]);
        }

        return $this->redirectAjax('menu/index');
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('updateMenu')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Menu $model */
        $model = Menu::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }
        $parent = $model->parents(1)->one();

        if ($model->deleteWithChildren()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        if ($parent && $parent->depth > 0) {
            return $this->redirectAjax('menu/index', ['id' => $parent->id]);
        }

        return $this->redirectAjax('menu/index');
    }
}

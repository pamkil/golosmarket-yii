<?
use yii\db\ActiveQuery;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use common\models\content\Banner;
use common\models\content\BannerSearch;
use admin\widgets\RestCheckbox;
use admin\components\grid\SocketGrid;

/* @var ActiveDataProvider $dataProvider */
/* @var BannerSearch $searchModel */
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <? if (Yii::$app->user->can('updateBanners')) { ?>
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<?= Url::to(['create']) ?>" class="btn sbold green"><?= Yii::t('admin', 'Добавить') ?>
                                    <i class="fa fa-plus"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            <? } ?>

            <?= SocketGrid::widget([
                'baseClass' => Banner::class,
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'active',
                        'format' => 'raw',
                        'value' => function ($data) use ($dataProvider) {
                            if (!Yii::$app->user->can('updateBanners')) {
                                return $data->active ? 'Да' : 'Нет';
                            }

                            /* @var ActiveQuery $query */
                            $query = $dataProvider->query;
                            return RestCheckbox::widget([
                                'id' => $data->id,
                                'name' => 'active',
                                'modelClass' => $query->modelClass,
                                'checked' => $data->active
                            ]);
                        },
                        'contentOptions' => ['style' => 'width: 100px; text-align: center;'],
                        'enableSorting' => true,
                        'filter' => false
                    ],
                    'name',
                    'position',
                    'date_start:datetime',
                    'date_end:datetime',
                    'updated_at:datetime',
                    [
                        'class' => 'admin\components\grid\DuplicateColumn',
                        'template' => Yii::$app->user->can('updateBanners') ? '{update} {duplicate} {delete}' : '',
                        'urlCreator' => function ($action, $model, $key, $index) {
                            return Url::to([$action, 'id' => $key]);
                        }
                    ]
                ]
            ]) ?>
        </div>
    </div>
</div>

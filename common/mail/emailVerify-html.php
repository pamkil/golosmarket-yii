<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $userName string */
/* @var $verification_token string */
/* @var $verifyUrl string */
?>
<div class="verify-email">
    <p>Здравствуйте, <?= Html::encode($userName) ?>. Для завершения регистрации на golosmarket.ru, <?= Html::a('перейдите по этой ссылке', $verifyUrl) ?></p>
</div>

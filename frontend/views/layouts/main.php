<?php

/* @var $this View */
/* @var $content string */

use Carbon\Carbon;
use frontend\assets\ReduxAsset;
use frontend\widgets\FooterMenu;
use frontend\widgets\TopMenu;
use yii\helpers\Html;
use yii\web\View;
use frontend\assets\AppAsset;

AppAsset::register($this);
ReduxAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <link rel="preconnect" href="https://mc.yandex.ru" crossorigin="">
    <?php //<!-- Google Tag Manager -->?>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-K8FR2GK');</script>
    <?php //<!-- End Google Tag Manager -->?>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <meta name="theme-color" content="#77BF22">
    <?php $this->head() ?>
</head>
<body>
    <?php //<!-- Google Tag Manager (noscript) -->?>
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K8FR2GK"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <?php //<!-- End Google Tag Manager (noscript) -->?>
    <?php $this->beginBody() ?>
    <header class="header">
        <?php /*
<!--        <div class="container all-center">-->
<!--            <div class="logo">-->
<!--                <a href="/">GolosMarket</a>-->
<!--            </div>-->
<!--            <div class="box box-main">-->
<!--                --><?//= TopMenu::widget([
//                    'options' => [
//                        'id' => 'top-menu',
//                        'class' => 'menu ',
//                    ],
//                ]); ?>
<!--            </div>-->
<!--            <div id="header" class="login"></div>-->
<!--        </div>-->
*/ ?>
        <div class="container all-center">
            <?php if (isset($this->params['hideMobileArrow']) && $this->params['hideMobileArrow']) :?>
                <input class="menu-btn" type="checkbox" id="menu-btn" />
                <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
            <?php endif; ?>

            <?php if (!isset($this->params['hideMobileArrow']) || !$this->params['hideMobileArrow']) :?>
                <div class="back-link">
                    <a href="/">
                        <div class="icon">
                       <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M19 12H6M12 5l-7 7 7 7"/></svg>

                        </div>
                    </a>
                </div>
            <?php endif; ?>
            <div class="logo <?= false && (!isset($this->params['hideMobileArrow']) || !$this->params['hideMobileArrow']) ? 'mobile-no' : '' ?>">
                <a href="/">GolosMarket</a>
            </div>

            <div class="box box-chat">
                <?= TopMenu::widget([
                    'options' => [
                        'class' => 'menu',
                    ],
                ]) ?>
            </div>
            <div id="header" class="login"></div>
        </div>
    </header>

    <?= $content ?>

    <section class="menu-footer">
        <div class="container">
            <?= FooterMenu::widget([
                'options' => [
                    'id' => 'footer-menu',
                ],
            ]); ?>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="copy">© <?= Carbon::now()->year; ?> GolosMarket</div>
            <div class="user-agreement">
                <a href="/politics">Пользовательское соглашение</a>
            </div>
            <div class="user-agreement">
                <a href="/rules">Правила сайта</a>
            </div>
            <div class="logo-bottom">
                <a href="/">GolosMarket</a>
            </div>
        </div>
    </footer>
    <div id="player"></div>
    <script>
        // Флаг, указывающий, было ли уже совершено нажатие на кнопку плей
        var playButtonClicked = false;

        // Находим все кнопки с классом play
        var playButtons = document.querySelectorAll('.play');

        // Проходим по каждой кнопке
        playButtons.forEach(function(playButton) {
            // Устанавливаем обработчик события для каждой кнопки
            playButton.addEventListener('click', function() {
                // Если уже было совершено нажатие на кнопку плей, выходим из обработчика
                if (playButtonClicked) return;

                // Сохраняем ссылку на текущую кнопку play
                var currentPlayButton = this;

                // Устанавливаем флаг, указывающий, что уже было совершено нажатие на кнопку плей
                playButtonClicked = true;

                // Ждем полсекунды
                setTimeout(function() {
                    // Проверяем наличие чего-либо внутри div с id player
                    var playerDiv = document.getElementById('player');
                    if (playerDiv.innerHTML.trim() !== '') {
                        // Если есть что-то внутри, находим элемент с классом close
                        var closeButton = playerDiv.querySelector('.close');
                        if (closeButton) {
                            // Нажимаем на элемент с классом close
                            closeButton.click();
                        }
                    }

                    // Нажимаем на текущую кнопку play снова после выполнения других действий
                    currentPlayButton.click();
                }, 300); // Пауза в полсекунды (500 миллисекунд)
            });
        });
    </script>
    <?php $this->endBody() ?>
    <div id="orientation">
        <div>Чтобы продолжить,<br>поверните ваше устройство</div>
        <img src="/images/rotate.png">
    </div>
</body>
</html>
<?php $this->endPage() ?>

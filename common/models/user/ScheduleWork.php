<?php

namespace common\models\user;

use Carbon\CarbonImmutable;
use common\models\access\User;
use common\models\announcer\Announcer;
use common\models\Model;
use DateInterval;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Автор публикаций
 *
 * Class ScheduleWork
 * @package common\models\user
 * @property int $id
 * @property int $user_id [int(11)]
 * @property int $type [smallint(6)]
 * @property string $time_1_at [time]
 * @property string $time_1_to [time]
 * @property string $time_2_at [time]
 * @property string $time_2_to [time]
 * @property string $time_3_at [time]
 * @property string $time_3_to [time]
 * @property string $time_4_at [time]
 * @property string $time_4_to [time]
 * @property string $time_5_at [time]
 * @property string $time_5_to [time]
 * @property string $time_6_at [time]
 * @property string $time_6_to [time]
 * @property string $time_7_at [time]
 * @property string $time_7_to [time]
 * @property string $undefined_text
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property int $deleted_at [timestamp]
 * @property User $announcer
 * @property ScheduleWork $scheduleWork
 */
class ScheduleWork extends Model
{
    public const TYPE_MANUAL = 1; // установка вручную времени на каждый день
    public const TYPE_WEEK = 2; //доступен по расписанию по дням неделям
    /** @deprecated */
    public const TYPE_SIGN_IN = 3; // доступен по постоянному логину
    public const TYPE_UNDEFINED = 4; //не доступен всегда - на долгое время

    public const LAST_LOGIN_MINUTES = 5;

    private $isOnline;

    public static function tableName()
    {
        return '{{%user_schedule_work}}';
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }

    public function rules()
    {
        return [
            [
                ['user_id'],
                'unique'
            ],
            [
                ['user_id', 'type',],
                'integer'
            ],
            [
                [
                    'time_1_at',
                    'time_1_to',//Если значения NULL то выходной
                    'time_2_at',
                    'time_2_to',
                    'time_3_at',
                    'time_3_to',
                    'time_4_at',
                    'time_4_to',
                    'time_5_at',
                    'time_5_to',
                    'time_6_at',
                    'time_6_to',
                    'time_7_at',
                    'time_7_to',
                    'undefined_text',
                ],
                'string'
            ],
            [
                'time_1_at',
                'filter',
                'filter' => function () {
                    $fields = [
                        'time_1_at',
                        'time_1_to',
                        'time_2_at',
                        'time_2_to',
                        'time_3_at',
                        'time_3_to',
                        'time_4_at',
                        'time_4_to',
                        'time_5_at',
                        'time_5_to',
                        'time_6_at',
                        'time_6_to',
                        'time_7_at',
                        'time_7_to',
                    ];

                    foreach ($fields as $field) {
                        if (empty($this->{$field})) {
                            continue;
                        }

                        $this->{$field} = str_replace('-', ':', $this->{$field});
                    }

                    return $this->time_1_at;
                }
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Диктор',
            'type' => 'Клиент',

            'time_1_at' => 'Время работы от в понедельник',
            'time_1_to' => 'Время работы до в понедельник',

            'time_2_at' => 'Время работы от во вторник',
            'time_2_to' => 'Время работы до во вторник',

            'time_3_at' => 'Время работы от в среду',
            'time_3_to' => 'Время работы до в среду',

            'time_4_at' => 'Время работы от в четверг',
            'time_4_to' => 'Время работы до в четверг',

            'time_5_at' => 'Время работы от в пятницу',
            'time_5_to' => 'Время работы до в пятницу',

            'time_6_at' => 'Время работы от в субботу',
            'time_6_to' => 'Время работы до в субботу',

            'time_7_at' => 'Время работы от в воскресенье',
            'time_7_to' => 'Время работы до в воскресенье',

            'created_at' => 'Время Создания',
            'updated_at' => 'Время Обновления',
        ];
    }

    public function getAnnouncer()
    {
        return $this->hasOne(Announcer::class, ['user_id' => 'user_id'])->inverseOf('scheduleWork');
    }

    /**
     * Return Query for user Online
     *
     * @return ActiveQuery
     */
    public static function getQueryOnline()
    {
        $timeNow = new Expression('NOW()');
        $day = CarbonImmutable::now()->dayOfWeekIso;

        $users = static::find()
            ->select(['user_id'])
            ->where([
                'AND',
                ['type' => self::TYPE_WEEK],
                ['<=', 'time_' . $day . '_at', $timeNow],
                ['>=', 'time_' . $day . '_to', $timeNow],
            ]);

        return $users;
    }

    public static function getUsersOnLine()
    {
        return User::find()
            ->where(['id' => self::getQueryOnline()])
            ->asArray()
            ->all();
    }

    /**
     * @param User $user
     *
     * @return array [isOnline, DiffTime]
     */
    public function getIsOnline($user)
    {
        if (isset($this->isOnline[$user['id']])) {
            return $this->isOnline[$user['id']];
        }

        return $this->isOnline[$user['id']] = static::isOnline($this, $user);
    }

    /**
     * @param ScheduleWork|array $scheduleWork
     * @param User $user
     *
     * @return array [isOnline, DiffTime]
     */
    public static function isOnline($scheduleWork, $user): array
    {
        if (!$scheduleWork) {
            return [false, null];
        }
        $isOnline = false;
        $result = null;
        $now = CarbonImmutable::now(3);
        $dateNowStr = $now->toDateString() . ' ';

        switch ($scheduleWork['type']) {
            case self::TYPE_MANUAL: //not use
                $timeAt = CarbonImmutable::parse($dateNowStr . $scheduleWork['time_1_at'], 3);
                $timeTo = CarbonImmutable::parse($dateNowStr . $scheduleWork['time_1_to'], 3);

                $isOnline = $timeAt <= $now && $timeTo >= $now;

                if ($isOnline) {
                    $result = $timeTo->diff($now);
                } else {
                    $timeTo = $timeTo->subDay();
                    $result = $now->diff($timeTo);
                }
                break;

            case self::TYPE_WEEK:
                $day = CarbonImmutable::now(3)->dayOfWeekIso;
                $timeAt = CarbonImmutable::parse($dateNowStr . $scheduleWork["time_{$day}_at"], 3);
                $timeTo = CarbonImmutable::parse($dateNowStr . $scheduleWork["time_{$day}_to"], 3);

                $isOnline = $timeAt <= $now && $timeTo >= $now;
                if ($isOnline) {
                    $result = $timeTo->diff($now);
                } elseif ($timeAt >= $now && $timeTo >= $now) {
                    $result = $now->diff($timeAt);
                } else {
                    $nextDay = self::nextDay($scheduleWork, $day);
                    if ($nextDay) {
                        $timeAt = $timeAt->addDays($nextDay);
                        $result = $now->diff($timeAt);
                    } else {
                        $result = null;
                    }
                }
                break;

            case self::TYPE_SIGN_IN: //not use
                $now = $now->subMinutes(self::LAST_LOGIN_MINUTES);
                $lastVisit = CarbonImmutable::parse($user['last_visit'], 3);

                $isOnline = $lastVisit >= $now;
                if ($isOnline) {
                    $result = $lastVisit - $now;
                } else {
                    $result = $now - $lastVisit;
                }
                break;

            case self::TYPE_UNDEFINED:
                $isOnline = false;
                $result = null;
                break;
        }

        return [$isOnline, $result];
    }

    /**
     * @param array|ScheduleWork $scheduleWork
     * @param int $currentDay
     * @param int $iteration
     * @return int
     */
    private static function nextDay($scheduleWork, int $currentDay, int $iteration = 1)
    {
        if ($iteration > 7) {
            return null;
        }
        $nextDay = $currentDay < 7 ? $currentDay + 1 : 1;
        if (isset($scheduleWork["time_{$nextDay}_at"])) {
            return $iteration;
        } else {
            return self::nextDay($scheduleWork, $nextDay, $iteration + 1);
        }
    }

    public static function getTimeOnlineString(?bool $isOnline = null, ?DateInterval $diff = null)
    {
        if (!isset($isOnline) || !isset($diff)) {
            return 'Недоступен';
        }

        $time = '';
        if ($diff->m) {
            $time = 'более ' . $diff->m . ' мес';
        } elseif ($diff->d) {
            $time = $diff->d . ' д.';
//            if ($diff->h) {
//                $time .= ' ' . $diff->h . ' ч.';
//            }
        } elseif ($diff->h) {
            $time = $diff->h . ' ч';
        } elseif ($diff->i) {
            $time = $diff->i . ' мин';
        } elseif ($diff->s) {
            $time = '1 мин';
        }

        return $isOnline
            ? "В доступе для заказа ещё $time"
            : "Буду через $time";
    }
}

<?php

use yii\db\Migration;

/**
 * Class m200809_175121_create_faq
 */
class m200809_175121_create_faq extends Migration
{
    private $question = '{{%content_question}}';
    private $answer = '{{%content_question_answer}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            $this->question,
            [
                'id' => $this->primaryKey(),
                'active' => $this->boolean()->notNull()->defaultValue(false),
                'position' => $this->integer()->null(),
                'text' => $this->text()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'updated_at' => $this->dateTime()->notNull(),
                'INDEX `IX-content_question-position` (position)',
                'INDEX `IX-content_question-active` (active)',
            ],
            $tableOptions
        );

        $this->createTable(
            $this->answer,
            [
                'id' => $this->primaryKey(),
                'question_id' => $this->integer()->notNull(),
                'active' => $this->integer()->notNull(),
                'position' => $this->integer()->null(),
                'text' => $this->text()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'updated_at' => $this->dateTime()->notNull(),
                'INDEX `IX-content_question_answer-question_id` (question_id)',
                'INDEX `IX-content_question_answer-position` (position)',
                'INDEX `IX-content_question_answer-active` (active)',
                'FOREIGN KEY `FK-content_question_answer-user_id` (question_id) REFERENCES ' . $this->question . ' (id) ON DELETE CASCADE ON UPDATE CASCADE'
            ],
            $tableOptions
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->answer);
        $this->dropTable($this->question);
    }
}

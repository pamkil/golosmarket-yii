export const PLAYER = {
    TO_PLAY: 'PLAYER_played',
    PLAYING: 'PLAYER_playing',
    STOP: 'PLAYER_stop',
}
import React from 'react';
import PhoneInput2 from 'react-phone-input-2';
import ru from 'react-phone-input-2/lang/ru.json';
import 'react-phone-input-2/lib/style.css';


const PhoneInput = props => {
    const { meta, input, placeholder} = props;
    const { touched, error, warning } = meta;

    return (
        <>
            <PhoneInput2
                country={'ru'}
                placeholder={placeholder}
                name={input.name}
                className={ touched && error ? 'error' : '' }
                value={input.value}
                onChange={value => input.onChange(`+${value}`)}
                localization={ru}
                autocompleteSearch
                inputStyle={{ paddingLeft: '50px' }}
                enableAreaCodes
            />
            {
                touched && (
                    (error && <div className="error-text">{error}</div>)
                    ||
                    (warning && <div className="warning-text">{warning}</div>)
                )
            }
        </>
    );
};

export default PhoneInput;
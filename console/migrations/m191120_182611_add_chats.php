<?php

use yii\db\Migration;

/**
 * Class m191120_182611_add_chats
 */
class m191120_182611_add_chats extends Migration
{

    private $chat = '{{%user_chat}}';
    private $user = '{{%user}}';
    private $file = '{{%file}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->chat, [
            'id' => $this->primaryKey(),
            'announcer_id' => $this->integer()->notNull(),
            'client_id' => $this->integer()->notNull(),
            'file_id' => $this->integer()->null(),
            'text' => $this->text()->null(),
            'is_write_client' => $this->boolean()->notNull(),
            'is_read' => $this->boolean()->notNull()->defaultValue(false),
            'is_send' => $this->boolean()->notNull()->defaultValue(false),
            'is_edit' => $this->boolean()->notNull()->defaultValue(false),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'deleted_at' => $this->timestamp(),
        ], $tableOptions);

        $this->createIndex('IX-user_chat-announcer_id', $this->chat, 'announcer_id');
        $this->createIndex('IX-user_chat-client_id', $this->chat, 'client_id');
        $this->createIndex('IX-user_chat-file_id', $this->chat, 'file_id');

        $this->addForeignKey(
            'FK-user_chat-announcer_id',
            $this->chat,
            'announcer_id',
            $this->user,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK-user_chat-client_id',
            $this->chat,
            'client_id',
            $this->user,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK-user_chat-file_id',
            $this->chat,
            'file_id',
            $this->file,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK-user_chat-file_id', $this->chat);
        $this->dropForeignKey('FK-user_chat-client_id', $this->chat);
        $this->dropForeignKey('FK-user_chat-announcer_id', $this->chat);

        $this->dropTable($this->chat);
    }
}

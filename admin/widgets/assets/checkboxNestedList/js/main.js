$(document).ready(function () {
    $('ul.list-unstyled input[type="checkbox"]').change(function () {
        var $el = $(this),
          parent = $el.parents('.container-nested-checked-list'),
          checkedParent = parent.data('checkedParent');
        if (checkedParent === undefined || parent.data('checkedParent') == true) {
          if ($el.is(':checked')) {
            $el.parents('li').children('label').find('input[type="checkbox"]').prop('checked', true);
          } else {
            $el.closest('li').find('li input[type="checkbox"]').prop('checked', false);
          }
        }

        $.uniform.update();
    });
});
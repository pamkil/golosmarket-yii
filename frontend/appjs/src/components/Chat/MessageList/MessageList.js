import React, { Component, } from 'react';
import './MessageList.css';
import MessageBox from '../MessageBox/MessageBox';
import FaChevronDown from 'react-icons/lib/fa/chevron-down';
import classNames from 'classnames';
import NotifyCount from "../../Common/NotifyCount/NotifyCount";

export const SCROLL_TO_BOTTOM_MESSAGES_EVENT = 'SCROLL_TO_BOTTOM_MESSAGES_EVENT';

export class MessageList extends Component {
    state = {
        scrollBottom: 0,
        downButton: false,
        paginationLoading: false
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (!this.mlistRef) return;

        this.observeUnread();

        if (prevProps.messages.length !== this.props.messages.length) {
            this.paginationLoading = false;
            this.setState({ paginationLoading: false });
        }
    }

    getBottom = (e) => e.scrollHeight - e.scrollTop - e.offsetHeight;

    onOpen = (item, i, e) => {
        if (this.props.onOpen instanceof Function) {
            this.props.onOpen(item, i, e);
        }
    }

    onDownload = (item, i, e) => {
        if (this.props.onDownload instanceof Function) {
            this.props.onDownload(item, i, e);
        }
    }

    onPhotoError = (item, i, e) => {
        if (this.props.onPhotoError instanceof Function) {
            this.props.onPhotoError(item, i, e);
        }
    }

    onClick = (item, i, e) => {
        if (this.props.onClick instanceof Function) {
            this.props.onClick(item, i, e);
        }
    }

    onTitleClick = (item, i, e) => {
        if (this.props.onTitleClick instanceof Function) {
            this.props.onTitleClick(item, i, e);
        }
    }

    onForwardClick = (item, i, e) => {
        if (this.props.onForwardClick instanceof Function) {
            this.props.onForwardClick(item, i, e);
        }
    }

    onReplyClick = (item, i, e) => {
        if (this.props.onReplyClick instanceof Function) {
            this.props.onReplyClick(item, i, e);
        }
    }

    onEditClick = (item, i, e) => {
        if (this.props.onEditClick instanceof Function) {
            this.props.onEditClick(item, i, e);
        }
    }

    onReplyMessageClick = (item, i, e) => {
        if (this.props.onReplyMessageClick instanceof Function) {
            this.props.onReplyMessageClick(item, i, e);
        }
    }

    onContextMenu = (item, i, e) => {
        if (this.props.onContextMenu instanceof Function) {
            this.props.onContextMenu(item, i, e);
        }
    }

    onMessageFocused = (item, i, e) => {
        if (this.props.onMessageFocused instanceof Function) {
            this.props.onMessageFocused(item, i, e);
        }
    }

    onMeetingMessageClick = (item, i, e) => {
        if (this.props.onMeetingMessageClick instanceof Function) {
            this.props.onMeetingMessageClick(item, i, e);
        }
    }

    loadRef = (ref) => {
        this.mlistRef = ref;
        if (this.props.cmpRef instanceof Function) {
            this.props.cmpRef(ref);
        }
    }

    paginationLoading = false;

    onScroll = (e) => {
        const canShow = e.target.scrollTop < -98;

        if (canShow !== this.state.downButton) {
            this.setState({ downButton: canShow });
        }

        if (!this.props.messages.length || this.state.paginationLoading || this.paginationLoading) return;

        if (e.target.scrollTop + e.target.scrollHeight - e.target.clientHeight < 10) {
            this.setState({ paginationLoading: true }, () => {
                this.paginationLoading = true;
                this.props.onLoadPreviews(this.props.messages[this.props.messages.length - 1].id);
            });
        }

        if (this.props.onScroll instanceof Function) {
            this.props.onScroll(e);
        }
    }

    toBottom = (e, isSmooth) => {
        if (!this.mlistRef) return;

        if (isSmooth) {
            this.mlistRef.scrollTo({ top: this.mlistRef.scrollHeight, behavior: 'smooth' })
        } else {
            this.mlistRef.scrollTop = this.mlistRef.scrollHeight;
        }

        if (this.props.onDownButtonClick instanceof Function) {
            this.props.onDownButtonClick(e);
        }

        this.observeUnread();
    }

    observeUnread = () => {
        const { messages, unreadCount, onReadMessages } = this.props;

        // if (this.unreadObserver) {
        //     this.unreadObserver.disconnect();
        // }

        // if (!unreadCount) return;

        // this.unreadObserver = new IntersectionObserver(entries => {
        //     const readMessageIds = [];

        //     for (let entry of entries) {
        //         if (entry.isIntersecting) {
        //             readMessageIds.push(parseInt(entry.target.getAttribute('data-id')));
        //         }
        //     }

        //     if (readMessageIds.length) {
        //         onReadMessages(readMessageIds);
        //         this.unreadObserver.disconnect();
        //     }
        //     console.log(readMessageIds)
        // }, { threshold: 1 });

        const unreadMessageIds = messages
            .filter(({ isRead, isWriteOpponent }) => !isRead && isWriteOpponent)
            .map(({ id }) => id);
        onReadMessages(unreadMessageIds)
        // for (let messageId of unreadMessageIds) {
        //     const messageElement = document.querySelector(`.rce-container-mbox[data-id="${messageId}"]`);
        //     this.unreadObserver.observe(messageElement);
        // }
    }
    

    render() {

        return (
            <div className={classNames(['rce-container-mlist', this.props.className])}>
                <div
                    ref={this.loadRef}
                    onScroll={this.onScroll}
                    className='rce-mlist'>
                    {
                        this.props.messages.map((x, i) => (
                            <MessageBox
                                key={x.id}
                                messages={this.props.messages}
                                {...x}
                                onOpen={this.props.onOpen && ((e) => this.onOpen(x, i, e))}
                                onPhotoError={this.props.onPhotoError && ((e) => this.onPhotoError(x, i, e))}
                                onDownload={this.props.onDownload && ((e) => this.onDownload(x, i, e))}
                                onTitleClick={this.props.onTitleClick && ((e) => this.onTitleClick(x, i, e))}
                                onForwardClick={this.props.onForwardClick && ((e) => this.onForwardClick(x, i, e))}
                                onReplyClick={this.props.onReplyClick && ((e) => this.onReplyClick(x, i, e))}
                                onReplyMessageClick={this.props.onReplyMessageClick && ((e) => this.onReplyMessageClick(x, i, e))}
                                onClick={this.props.onClick && ((e) => this.onClick(x, i, e))}
                                onContextMenu={this.props.onContextMenu && ((e) => this.onContextMenu(x, i, e))}
                                onMessageFocused={this.props.onMessageFocused && ((e) => this.onMessageFocused(x, i, e))}
                                onMeetingMessageClick={this.props.onMeetingMessageClick && ((e) => this.onMeetingMessageClick(x, i, e))}
                                onMeetingTitleClick={this.props.onMeetingTitleClick}
                                onMeetingVideoLinkClick={this.props.onMeetingVideoLinkClick}
                                onShareFile={this.props.onShareFile}
                                onDelete={this.props.onDelete}
                                onEdit={this.props.onEdit}
                                onEditClick={this.props.onEditClick && ((e) => this.onEditClick(x, i, e))}
                                onPlay={this.props.onPlay}
                                onStop={this.props.onStop}
                                onReadMode={this.props.onReadMode}
                                notch={false}
                                isAnnouncer={this.props.isAnnouncer}
                                replyButton
                                reply={x.parent}
                            />
                        ))
                    }
                </div>
                {
                    this.state.downButton && (
                        <div
                            className='rce-mlist-down-button'
                            onClick={this.toBottom.bind(this, true)}
                        >
                            <FaChevronDown/>
                            { !!this.props.unreadCount &&
                                <NotifyCount className='message-list__button-badge' count={ this.props.unreadCount }/>
                            }
                        </div>
                    )
                }
            </div>
        );
    }
}

MessageList.defaultProps = {
    onClick: null,
    onTitleClick: null,
    onForwardClick: null,
    onReplyClick: null,
    onReplyMessageClick: null,
    onMeetingMessageClick: null,
    onDownButtonClick: null,
    onOpen: null,
    onPhotoError: null,
    onDownload: null,
    messages: [],
    lockable: false,
    toBottomHeight: 300,
    downButton: true,
    downButtonBadge: null,
};

export default MessageList;

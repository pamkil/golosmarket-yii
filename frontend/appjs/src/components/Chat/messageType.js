export const MESSAGE_TYPE_RU = {
    text: 'Сообщение',
    file: 'Файл',
    image: 'Изображение',
    location: 'Локация',
    audio: 'Аудио',
    spotify: 'Spotify',
    meeting: 'Встреча',
}

export const MESSAGE_TYPE = {
    text: 'text',
    file: 'file',
    image: 'image',
    location: 'location',
    audio: 'audio',
    spotify: 'spotify',
    meeting: 'meeting',
}
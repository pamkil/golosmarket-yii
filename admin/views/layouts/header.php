<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
/* @var \common\models\access\User $user */
$user = Yii::$app->user->identity;
$userIcon = $user->photo ? $user->photo->getUrl() : $directoryAsset . '/img/user2-160x160.jpg';
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">Golos</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $userIcon; ?>" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= $user->firstname; ?> <?= $user->lastname; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= $userIcon ?>"
                                 class="img-circle"
                                 alt="User Image"
                            />
                            <p>
                                <?= $user->firstname; ?> <?= $user->lastname; ?> - <?= $user->getTypeName(); ?>
                                <small><?= $user->city ? $user->city->title : '' ?></small>
                            </p>
                        </li>
                        <!-- Menu Body -->

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <?= Html::a(
                                    'Выйти',
                                    ['/default/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

<?php
namespace api\models;

use Yii;
use yii\base\Model;
use common\models\access\User;

class Api extends Model
{
    static $version = 'v1';

    public static function getUrl($path, $id = null)
    {
        $server = Yii::$app->request->getServerName();
        $isLocal = false;

        if (strpos($server, '.test') !== false) {
            $isLocal = true;
            $server = str_replace('.test', '', $server);
        }
        $url = (Yii::$app->request->isSecureConnection ? 'https://' : 'http://') . 'api.' . $server . ($isLocal ? '.test' : '') . '/' . self::$version;

        $url .= '/' . $path;

        if ($id) {
            $url .= '/' . $id;
        }

        /* @var User $user */
        $user = Yii::$app->user->getIdentity();

        $url .= '?access-token=' . $user->access_token;

        return $url;
    }
}
<?php

use yii\db\Migration;

/**
 * Class m210627_150056_add_similar_page
 */
class m210627_150056_add_similar_page extends Migration
{
    public $table = '{{%content_page_2_similar}}';
    public $filter = '{{%content_page_2_filter}}';
    public $page = '{{%content_pages}}';
    public $gender = '{{%user_announcer_genders}}';
    public $age = '{{%user_announcer_age}}';
    public $presentation = '{{%user_announcer_presentations}}';
    public $language = '{{%user_announcer_languages}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->addColumn(
            $this->page,
            'show_similar',
            $this->boolean()->notNull()->defaultValue(false)->after('show_filters')
        );

        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'page_id' => $this->integer()->notNull(),
                'gender_id' => $this->integer()->null(),
                'age_id' => $this->integer()->null(),
                'presentation_id' => $this->integer()->null(),
                'language_id' => $this->integer()->null(),
                'count' => $this->smallInteger()->null(),
                'cost_by_a4' => $this->boolean()->notNull()->defaultValue(false),
                'cost_at' => $this->integer()->null()->defaultValue(0),
                'cost_to' => $this->integer()->null()->defaultValue(0),
                'sound_by_key' => $this->boolean()->null(),
                'script' => $this->boolean()->null(),
                'photo' => $this->boolean()->null(),
                'online' => $this->boolean()->null(),
                'created_at' => $this->dateTime()->null(),
                'updated_at' => $this->dateTime()->null(),
                'INDEX `IX-content_page_2_similar-page_id` (page_id)',
            ],
            $tableOptions
        );

        $this->addForeignKey(
            'FK-content_page_2_similar-page_id',
            $this->table,
            'page_id',
            $this->page,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK-content_page_2_similar-gender_id',
            $this->table,
            'gender_id',
            $this->gender,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK-content_page_2_similar-age_id',
            $this->table,
            'age_id',
            $this->age,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK-content_page_2_similar-presentation_id',
            $this->table,
            'presentation_id',
            $this->presentation,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK-content_page_2_similar-language_id',
            $this->table,
            'language_id',
            $this->language,
            'id',
            'CASCADE',
            'CASCADE'
        );


        $this->renameColumn($this->filter, 'cost', 'cost_at');
        $this->renameColumn($this->filter, 'cost_a4', 'cost_to');

        $this->addColumn(
            $this->filter,
            'cost_by_a4',
            $this->boolean()->notNull()->defaultValue(false)->after('language_id')
        );
        $this->addColumn(
            $this->filter,
            'count',
            $this->smallInteger()->null()->after('language_id')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
        $this->dropColumn($this->page, 'show_similar');

        $this->renameColumn($this->filter, 'cost_at', 'cost');
        $this->renameColumn($this->filter, 'cost_to', 'cost_a4');

        $this->dropColumn($this->filter, 'cost_by_a4');
        $this->dropColumn($this->filter, 'count');
    }
}

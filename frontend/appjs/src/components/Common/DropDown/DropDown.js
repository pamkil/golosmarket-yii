import React, {PureComponent} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './dropdown.css';

export default class DropDown extends PureComponent {
    static propTypes = {
        className: PropTypes.string,
        items: PropTypes.array.isRequired,
        onOpen: PropTypes.func,
        onClose: PropTypes.func,
        children: PropTypes.object,
        ignoreOutsideClickClass: PropTypes.string
    };

    static defaultProps = {
        onOpen: () => {},
        onClose: () => {}
    };

    state = {
        isOpen: false,
        styles: null
    };

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside, true);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside, true);
    }

    handleClickOutside = (event) => {
        const domNode = ReactDOM.findDOMNode(this);
        const isInnerClick = domNode && domNode.contains(event.target);
        const ignoreClick = this.props.ignoreOutsideClickClass &&
            event.target.classList.contains(this.props.ignoreOutsideClickClass);

        if (!isInnerClick && !ignoreClick && this.state.isOpen) {
            this.close();
        }
    };

    toggle = (params) => {
        if (this.state.isOpen) this.close();
        else this.open(params);
    };

    open = (params = {style: null}) => {
        this.setState({isOpen: true, styles: params.style}, this.props.onOpen);
    };

    close = () => {
        this.setState({isOpen: false}, this.props.onClose);
    };

    render() {
        const {className, items, dropUp} = this.props;
        const {isOpen, styles} = this.state;

        return isOpen ? (
            <div className={`dropdown ${className} ${dropUp ? 'dropup':''}`} style={styles}>
                <div className='dropdown__list' role='listbox'>
                    {items.map(({
                        link,
                        title,
                        disabled,
                        onClick,
                        closeOnClick,
                        danger
                    }, itemIndex) => link ? (
                        <a
                            key={itemIndex}
                            className={cx({
                                'dropdown__list-item': true,
                                'dropdown__list-item--disabled': disabled,
                                'dropdown__list-item--danger': danger
                            })}
                            href={link}
                            role='option'
                        >{title}</a>
                    ) : (
                        <div
                            key={itemIndex}
                            className={cx({
                                'dropdown__list-item': true,
                                'dropdown__list-item--disabled': disabled,
                                'dropdown__list-item--danger': danger
                            })}
                            onClick={() => {
                                onClick();

                                if (closeOnClick) this.close();
                            }}
                            role='option'
                        >{title}</div>
                    ))}
                </div>
            </div>
        ) : null;
    }
}

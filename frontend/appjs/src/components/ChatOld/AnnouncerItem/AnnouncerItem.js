import React from 'react';
import avatarImg from '../../../images/guest.jpg';
import Date from "../../Common/Date/Date";

const AnnouncerItem = (
    {
        opponentId: id,
        avatar = avatarImg,
        name = 'Диктор',
        unread: quantityUnread = 0,
        createdAt,
        text = '',
        onChange,
        isActive,
        online = false,
        style = {}
    }
) => (
    <div className={ "item" + (isActive ? ' active' : '') } onClick={ () => onChange(id) } style={ style }>
        <div className="ava-icon">
            <div className="box">
                <img src={ avatar } alt={ name }/>
            </div>
            {
                online && <div className="link-icon"/>
            }
        </div>
        <div className="desc">
            <div className="name">
                { name }
                { quantityUnread > 0 && <span>{ quantityUnread }</span> }
            </div>
            <div className="date"><Date value={ createdAt }/></div>
            <div className="content">{ text }</div>
        </div>
    </div>
);

export default AnnouncerItem;
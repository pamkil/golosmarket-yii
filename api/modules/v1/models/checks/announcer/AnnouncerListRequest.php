<?php

namespace api\modules\v1\models\checks\announcer;

use yii\base\Model;

/**
 * @OA\Schema(title="Форма запроса дикторов")
 */
class AnnouncerListRequest extends Model
{
    /**
     * @OA\Property(
     *     type="integer",
     *     description="Пол голоса",
     *     title="gender",
     * )
     *
     * @var string
     */
    public $gender;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Возраст голоса",
     *     title="age",
     * )
     *
     * @var string
     */
    public $age;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="цена за 30 сек предел от",
     *     title="cost_at",
     * )
     *
     * @var string
     */
    public $cost_at;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="цена за 30 сек предел до",
     *     title="cost_to",
     * )
     *
     * @var string
     */
    public $cost_to;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="язык голоса",
     *     title="language",
     * )
     *
     * @var string
     */
    public $language;

    /**
     * @OA\Property(
     *     type="boolean",
     *     description="только онлайн",
     *     title="online",
     * )
     *
     * @var string
     */
    public $online;

    /**
     * @OA\Property(
     *     type="boolean",
     *     description="только в избаном",
     *     title="favorite",
     * )
     *
     * @var string
     */
    public $favorite;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="тембр голоса",
     *     title="timber",
     * )
     *
     * @var string
     */
    public $timber;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="подача голоса",
     *     title="presentation",
     * )
     *
     * @var string
     */
    public $presentation;

    /**
     * @OA\Property(
     *     type="string",
     *     description="сободный запрос",
     *     title="query",
     * )
     *
     * @var string
     */
    public $query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['query'],
                'string',
            ],
            [
                ['gender', 'age', 'presentation', 'timber', 'language', 'cost_to', 'cost_at'],
                'integer',
            ],
            [
                ['favorite', 'online',],
                'boolean',
            ],
        ];
    }


    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $result = [];

        foreach (parent::toArray($fields, $expand, $recursive) as $field => $value) {
            if (isset($value)) {
                if (in_array($field, ['favorite', 'online'])) {
                    $value = (bool)$value;
                }
                $result[$field] = $value;
            }
        }

        return $result;
    }
}

import React, { useCallback, useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';
import classnames from 'classnames';

import './Modal.css';

const Modal = ({ className, children, onCloseRequest, show, addClassName = '' }) => {
    const close = useCallback(() => {
        window.removeEventListener('keyup', handleKeyUp, false);
        document.removeEventListener('click', handleOutsideClick, false);
        document.body.style.overflow = '';
        document.documentElement.style.overflow = '';
        onCloseRequest();
        // eslint-disable-next-line
    }, []);

    const handleKeyUp = useCallback(
        e => {
            const keys = {
                27: () => {
                    e.preventDefault();
                    close();
                },
            };

            if (keys[e.keyCode]) {
                keys[e.keyCode]();
            }
        },
        [close]
    );

    const handleOutsideClick = useCallback(
        e => {
            if (e.target === inputEl.current) {
                close();
            }
        },
        [close]
    );

    useEffect(() => {
        window.addEventListener('keyup', handleKeyUp, false);
        if (show) {
            document.body.style.overflow = 'hidden';
            document.documentElement.style.overflow = 'hidden';
        } else {
            close();
        }
    }, [show, handleKeyUp, close]);

    useEffect(() => {
        return () => {
            close();
        }
    }, [close]);

    const inputEl = useRef(null);

    if (!show) {
        return null;
    }

    const result = (
        <div className="remodal-overlay" onMouseDown={ handleOutsideClick } ref={ inputEl }>
            <div className={ classnames('remodal', addClassName, className) }>
                { children }
                <button onClick={ close } className="remodal-close"/>
            </div>
        </div>
    );

    return ReactDOM.createPortal(result, document.body);
};

const ModalNew = props => {
    if (!props.show) {
        return null;
    }

    return <Modal {...props} />
}

export default ModalNew;

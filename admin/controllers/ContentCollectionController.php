<?php

namespace admin\controllers;

use common\repositories\contract\CollectionRepositoryContract;
use common\repositories\exceptions\EmptyFieldError;
use common\repositories\exceptions\FieldError;
use common\service\collection\contract\CollectionServiceContract;
use common\service\collection\dto\CollectionDto;
use Yii;
use common\models\content\Collection;
use admin\models\content\CollectionSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContentCollectionController implements the CRUD actions for Collection model.
 */
class ContentCollectionController extends SecurityController
{
    private CollectionRepositoryContract $collectionRepository;
    private CollectionServiceContract $collectionService;

    public function __construct(
        $id,
        $module,
        CollectionRepositoryContract $collectionRepository,
        CollectionServiceContract $collectionService,
        $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->collectionRepository = $collectionRepository;
        $this->collectionService = $collectionService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Collection models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CollectionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->title = 'Подборки';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Collection model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->collectionRepository->getById($id),
        ]);
    }

    /**
     * Creates a new Collection model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->view->title = 'Добавление';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Подборки',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $params = Yii::$app->request->bodyParams;

        if (!empty($params)) {
            try {
                $model = $this->collectionService->create($params);
            } catch (EmptyFieldError $e) {
            }

            if (!$model->hasErrors()) {
                Yii::$app->session->addFlash('success', 'Подборка успешно добавлена');
                return $this->redirect(['view', 'id' => $model->id]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error[0]);
            }
        } else {
            $model = $this->collectionService->getEmptyModel();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Collection model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $params = Yii::$app->request->bodyParams;

        if (!empty($model) && !empty($params)) {
            try {
                $model = $this->collectionService->update($model, $params);
            } catch (EmptyFieldError $e) {
            }

            if (!$model->hasErrors()) {
                Yii::$app->session->addFlash('success', 'Подборка успешно обновлена');
                return $this->redirect(['view', 'id' => $model->id]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error[0]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Collection model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Collection model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Collection the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Collection::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * TODO доделать и перевести создание и обновление на ДТО
     * @return CollectionDto
     */
    private function getCollectionDto()
    {
        return new CollectionDto([
            'name' => Yii::$app->request->getBodyParam('name', ''),
            'description' => Yii::$app->request->getBodyParam('description', ''),
            'active' => (int) Yii::$app->request->getBodyParam('name', 0),
        ]);
    }
}

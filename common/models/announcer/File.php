<?php

namespace common\models\announcer;

use Carbon\Carbon;
use common\models\Model;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\NotAcceptableHttpException;
use yii\web\UploadedFile;

/**
 * Автор публикаций
 *
 * Class Age
 * @package common\models\user
 *
 * @property int $id
 * @property string $name
 * @property string $path [varchar(255)]
 * @property string $filename [varchar(255)]
 * @property string $ext [varchar(10)]
 * @property int $size [int(11)]
 * @property string $type [varchar(100)]
 * @property string $url
 * @property boolean $default
 * @property string $expired [datetime()]
 * @property boolean $is_delete [bool(1)]
 * @property string $created_at
 * @property string $updated_at
 */
class File extends Model
{
    public const EXPIRED_DAYS = 7;

    public static $directoryLevel = 2;
    public static $basePath = 'upload';

    public static function tableName()
    {
        return '{{%file}}';
    }

    /**
     * @param File|array $file
     * @param string $default
     *
     * @return string
     */
    public static function url($file, string $default = ''): string
    {
        if (!$file) {
            return $default;
        }

        $basePath = self::$basePath;

        return "/$basePath{$file['path']}/{$file['filename']}.{$file['ext']}";
    }

    /**
     * @param UploadedFile $uploadedFile
     * @param int|null $width
     * @return File
     * @throws NotAcceptableHttpException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public static function setFileByUpload(UploadedFile $uploadedFile, ?int $width = 630)
    {
        if ($uploadedFile->size >= 1024 * 1024 * 16) {
            throw new NotAcceptableHttpException('Uploaded file too large');
        }
        $file = new static();
        $file->name = $uploadedFile->name;
        $file->size = $uploadedFile->size;
        $file->ext = $uploadedFile->getExtension();
        $file->type = FileHelper::getMimeType($uploadedFile->tempName);
        $file->filename = Yii::$app->security->generateRandomString();
        $file->expired = Carbon::now()->addDays(self::EXPIRED_DAYS)->toDateTimeString();
        $file->setPath();
        if (
            !file_exists($file->getPathUpload())
            && !mkdir($dir = $file->getPathUpload(), 0777, true)
            && !is_dir($dir)
        ) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
        }

        $uploadedFile->saveAs($file->getFullPath());
        try {
            $exts = ['jpg', 'jpeg', 'gif', 'png'];
            if ($width && in_array($file->ext, $exts)) {
                Image::resize($file->getFullPath(), $width, null, false, false)
                    ->strip()
                    ->save($file->getFullPath(), ['quality' => 90]);
            }
        } catch (\Throwable $exception) {
            Yii::error($exception);
        }

        return $file;
    }

    public function setPath()
    {
        $path = '';

        if (self::$directoryLevel > 0) {
            for ($i = 0; $i < self::$directoryLevel; ++$i) {
                if (($prefix = substr($this->filename, $i + $i, 2)) !== false) {
                    $path .= DIRECTORY_SEPARATOR . $prefix;
                }
            }
        }
        $this->path = $path;

        return $this;
    }

    public function getPathUpload()
    {
        return Yii::getAlias('@uploadPath') . ($this->path ?: '');
    }

    public function getFullPath()
    {
        $path = $this->getPathUpload();
        $path .= DIRECTORY_SEPARATOR . $this->filename . '.' . $this->ext;

        return $path;
    }

    /**
     * @param File $file
     * @return null|File
     * @throws \yii\base\InvalidConfigException
     */
    public static function setWatermark($file)
    {
        $watermark = Yii::getAlias('@runtimeRoot/watermark.wav');

        $source = $file->getFullPath();
        $mimeType = mime_content_type($source);

        if (strpos($mimeType, 'audio/') === false) {
            return null;
        }

        $sourceFile = $source;
        $outFile = tempnam(sys_get_temp_dir(), 'watermark_');
        unlink($outFile);
        $outFile .= '.mp3';

        //ffmpeg -y -i source.mp3 -vn -ar 44100 -ac 2 -b:a 64k -filter_complex "amovie=watermark.wav:loop=0,asetpts=N/SR/TB[beep];[0][beep]amix=duration=shortest,volume=2" -map 0 -map_metadata -1 outFile.mp3
        $command = 'ffmpeg -y -i ';
        $command .= $sourceFile . ' ';
        $command .= '-vn -ar 44100 -ac 2 -b:a 64k -filter_complex ';
        $command .= '"amovie=' . $watermark . ':loop=0,asetpts=N/SR/TB[beep];[0][beep]amix=duration=shortest,volume=2" ';
        $command .= '-map 0 -map_metadata -1 ' . $outFile;
        $process = Process::fromShellCommandline($command);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $fileNew = new static();
        $fileNew->name = $file->name;
        $fileNew->size = filesize($outFile);
        $fileNew->ext = 'mp3';
        $fileNew->type = FileHelper::getMimeType($outFile);
        $fileNew->filename = sha1_file($outFile);
        $fileNew->expired = Carbon::now()->addDays(self::EXPIRED_DAYS)->toDateTimeString();
        $fileNew->setPath();
        if (!file_exists($fileNew->getPathUpload())) {
            mkdir($fileNew->getPathUpload(), 0777, true);
        }

        rename($outFile, $fileNew->getFullPath());

        return $fileNew;
    }

    public static function getEmptyAvatarUrl()
    {
        return '/images/avatar.png';
    }

    public static function getDeletedAvatarUrl()
    {
        return '/images/avatar_deleted.png';
    }

    /**
     * @param string|null $date
     * @return \yii\db\ActiveQuery
     */
    public static function getExpiredFiles(?string $date = null)
    {
        $nowDate = (!empty($date) ? Carbon::parse($date) : Carbon::now($date))->toDateTimeString();

        return static::find()
            ->where([
                'AND',
                ['<=', 'expired', $nowDate],
                ['is_delete' => false],
            ]);
    }

    public function rules()
    {
        return [
            [
                ['name', 'path', 'filename', 'ext', 'type', 'expired'],
                'string'
            ],
            [
                ['size',],
                'number'
            ],
            [
                ['default', 'is_delete'],
                'boolean'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Реальное Наименование файла (отображаемы заголовок для файла)',
            'path' => 'Под путь к файлу /ha/01',
            'filename' => 'имя файла на диске',
            'ext' => 'Расширение',
            'type' => 'Тип',
            'size' => 'Размер',
            'expired' => 'Удалить после времени',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }

    public function getUrl()
    {
        $basePath = self::$basePath;
        $url = "/$basePath";
        if (!empty($this->path)) {
            $url .= "{$this->path}";
        }

        $url .= "/{$this->filename}";
        if (mb_strpos($this->filename, $this->ext) === false) {
            $url .= ".{$this->ext}";
        }

        return $url;
    }
}

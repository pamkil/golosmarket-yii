<?php

namespace admin\controllers;

use common\models\reviews\ReviewRoute;
use common\models\reviews\ReviewSource;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use common\models\reviews\Review;
use common\models\reviews\ReviewSearch;
use common\models\settings\Site;
use yii\web\Response;

class ReviewController extends SecurityController
{
    public function actionIndex()
    {
        if (!Yii::$app->user->can('viewReviews')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $searchModel = new ReviewSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Отзывы';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionView($id)
    {
        if (!Yii::$app->user->can('viewReviews')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Review $model */

        $model = Review::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $this->view->title = 'Просмотр отзыва от "' . $model->name . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список отзывов',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = 'от ' . $model->name;

        $leaves = $model->leaves()->all();

        return $this->render('view', [
            'model' => $model,
            'leaves' => $leaves
        ]);
    }

    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('updateReviews')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Review $model */
        $model = Review::findOne(['id' => $id]);
        if (!$model) {
            throw new NotFoundHttpException;
        }

        if ($post = Yii::$app->request->post()) {
            return $this->saveReview($post, $model);
        }

        return $this->viewReview($model);
    }

    public function actionCreate()
    {
        if (!Yii::$app->user->can('updateReviews')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Review $model */
        $model = new Review();

        if ($post = Yii::$app->request->post()) {
            return $this->saveReview($post, $model);
        }

        return $this->viewReview($model);
    }

    private function saveReview(array $post, Review $model)
    {
        if ($model->load($post) && $model->isNewRecord ? $model->makeRoot() : $model->save()) {
            Yii::$app->session->addFlash('success', 'Успешно обновлено');
        }

        $model->loadImage('images', false, false);

        foreach ($model->getErrors() as $error) {
            Yii::$app->session->addFlash('error', implode(', ', $error));
        }

        if (isset($post['fileId']) && Yii::$app->request->isAjax && $returnEr = $model->getErrorAction()) {
            return $returnEr;
        } else {
            return $this->redirectAjax('review/update', [
                'id' => $model->id
            ]);
        }
    }

    private function viewReview(Review $model)
    {
        if (isset($model->date)) {
            $model->date = Yii::$app->formatter->asDatetime($model->date, 'php:d.m.Y H:i:s');
        }

        $sites = Site::getSitesList();
        $statuses = Review::getStatuses();
        $statusesFull = Review::getStatusesFull();
        $sources = ReviewSource::getList();

        return $this->render('_form', [
            'model' => $model,
            'sites' => $sites,
            'sources' => $sources,
            'statuses' => $statuses,
            'statusesFull' => $statusesFull,
        ]);
    }

    public function actionReply($id, $reply = null)
    {
        if (!Yii::$app->user->can('updateReviews')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Review $review */

        $review = Review::findOne(['id' => $id]);
        if ($review === NULL) {
            throw new NotFoundHttpException;
        }
        if ($reply) {
            $reply = Review::findOne((int)$reply);
        }
        if ($reply) {
            $model = $reply;
        } else {
            $model = new Review;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->appendTo($review)) {
                Yii::$app->session->addFlash('success', 'Успешно создано');

                return $this->redirectAjax('review/view', [
                    'id' => $review->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', is_array($error) ? $error[0] : $error);
            }
        }

        $model->name = 'Служба контроля качества';
        $model->status = Review::STATUS_ANSWER;

        return $this->render('reply', [
            'review' => $review,
            'model' => $model
        ]);
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('updateReviews')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Review $model */

        $model = Review::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if ($model->deleteWithChildren()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirectAjax('review/index');
    }

    public function actionCreateRoute($id)
    {
        if (!Yii::$app->user->can('updateReviewRoutes')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Review $model */
        $model = Review::findOne(['id' => $id]);
        if (!$model) {
            throw new NotFoundHttpException;
        }

        $reviewRoute = new ReviewRoute(['status_id' => ReviewRoute::STATUS_NEW]);

        $reviewRoute->order_id = $model->order_id;
        $reviewRoute->client_id = $model->order->user_id ?? null;
        $reviewRoute->city_id = $model->order->city_id ?? $model->city_id ?? null;
        $reviewRoute->partner_id = $model->order->to_partner_id ?? null;
        $reviewRoute->reviews = [$model->id];
        $reviewRoute->date = $model->date;

        $reviewRoute->save();

        return $this->redirectAjax('review-route/update', ['id' => $reviewRoute->id]);
    }

    public function actionChangeStatus($id, $status = null)
    {
        if (!Yii::$app->user->can('updateReviews')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Review $review */
        $review = Review::findOne(['id' => $id]);
        if (!$review) {
            throw new NotFoundHttpException;
        }

        if (!$status) {
            $status = Yii::$app->request->post('status');
        }

        if (!isset(Review::getStatusesFull()[$status])) {
            throw new NotFoundHttpException('Неверный статус');
        }

        $review->status = $status;
        $review->save();

        if (Yii::$app->request->post('hasEditable', false)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->statusCode = 201;

            if ($review->hasErrors()) {
                return ['output'=> $review->status, 'message'=> implode(', ', $review->firstErrors)];
            }

            return ['output'=> $review->status, 'message'=> ''];
        }

        return $this->redirectAjax('review/view', ['id' => $id]);
    }
}

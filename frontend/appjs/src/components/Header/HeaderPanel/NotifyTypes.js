export const NOTIFY_TYPE = {
    TYPE_BILL: 'bill',
    TYPE_NO_PAID_BILL: 'no_paid_bill',
    TYPE_REVIEW: 'review',
    TYPE_REVIEW_SEND: 'review_send',
    TYPE_NEWS: 'news',
    TYPE_REVIEW_SERVICE_CLIENT: 'review_service',
}

import React  from 'react';
import Date from "../../Common/Date/Date";

const FileView = ({ file: { url, name, ext } }) => (
    <a href={ url } download target="_blank" rel="noopener noreferrer">
        { name }
    </a>
)

const createTextLinks = text => {
    return (text || "").replace(
        /([^\S]|^)(((https?\:\/\/)|(www\.))(\S+))/gi,
        (match, space, url) => {
            let hyperlink = url;
            if (!hyperlink.match('^https?:\/\/')) {
                hyperlink = 'http://' + hyperlink;
            }

            return space + '<a href="' + hyperlink + '" target="_blank" rel="noopener noreferrer">' + text + '</a>';
        }
    );
};

const MessageItem = ({ text: message, createdAt: date, isWriteOpponent, file }) => (
    <div className={ isWriteOpponent ? 'message-opponent' : 'message' }>
        <div className="text">
            { message && <span dangerouslySetInnerHTML={ { __html: createTextLinks(message) } }></span> }
            {
                file && <FileView file={ file }/>
            }
        </div>
        <div className="time"><Date value={ date }/></div>
    </div>
);

export default MessageItem;
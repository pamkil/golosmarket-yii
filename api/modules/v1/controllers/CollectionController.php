<?php

namespace api\modules\v1\controllers;

use api\modules\v1\actions\collection\Sounds;
use common\models\content\Collection;
use yii\helpers\ArrayHelper;

/**
 * @SWG\Tag(
 *   name="Collection",
 *   description="Everything about Collections"
 * )
 */
class CollectionController extends RestController
{
    public $modelClass = Collection::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['optional'] = [
            'index',
            'view',
            'sounds'
        ];

        return $behaviors;
    }

    public function checkAccess($action, $model = null, $params = [])
    {

    }

    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'sounds' => [
                    'class' => Sounds::class
                ],
                'update' => false,
                'delete' => false,
            ]
        );
    }
}

import React from 'react';
import PropTypes from 'prop-types';

const Input = ({ placeholder = 'Поиск по имени', className = 'search', onChange, type = 'text', value = '', ...props }) => (
    <input
        type={ type }
        placeholder={ placeholder }
        className={ className }
        onChange={ event => {
            onChange(event.target.value);
        } }
        value={ value }
        { ...props }
    />
);

Input.propTypes = {
    placeholder: PropTypes.string,
    className: PropTypes.string,
};

export default Input;
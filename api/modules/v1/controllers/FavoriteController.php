<?php

namespace api\modules\v1\controllers;

use api\modules\v1\actions\favorite\Create;
use api\modules\v1\actions\favorite\Delete;
use api\modules\v1\actions\favorite\Update;
use api\modules\v1\actions\favorite\View;
use common\models\user\Favorite;
use Yii;
use yii\web\ForbiddenHttpException;

/**
 * @SWG\Tag(
 *   name="Favorite",
 *   description="usage Favorite"
 * )
 */
class FavoriteController extends RestController
{
    public $modelClass = Favorite::class;

    public function checkAccess($action, $model = null, $params = [])
    {
        parent::checkAccess($action, $model, $params);

        if ($action !== 'options' && !Yii::$app->user->identity) {
            throw new ForbiddenHttpException(Yii::t('api', 'Access denied'));
        }
    }

    public function actions()
    {
        return [
            'view' => [
                'class' => View::class,
                'modelClass' => $this->modelClass,
            ],
            'create' => [
                'class' => Create::class,
                'modelClass' => $this->modelClass,
            ],
            'update' => [
                'class' => Update::class,
                'modelClass' => $this->modelClass,
            ],
            'delete' => [
                'class' => Delete::class,
                'modelClass' => $this->modelClass,
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],

        ];
    }
}

import Close from "../../images/close.svg";
import React from "react";

const ProfilePhoto = ({input: {value: file, name}, onRemove, srvr}) => {
    return (
        <div className="photo-slick min-image">
            <img src={srvr + String(file.url || '/images/avatar.png')} alt={name} className="img-fluid"/>
            <button
                type="button"
                className="remove-photo-button"
                onClick={() => onRemove(file, name)}
                title="Удалить фото"
            >
                <img src={Close} alt="" style={{width: '10px'}}/>
            </button>
        </div>
    )
};

export default ProfilePhoto;

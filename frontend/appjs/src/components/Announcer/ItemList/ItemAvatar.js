import React from 'react';
import PropTypes from 'prop-types';
import guest from '../../../images/guest.jpg';
import {connect} from "react-redux";
import config from '../../../data/config/';

const ItemAvatar = ({
                      id,
                      avatar,
                      localDev,
                      firstname,
                      isOnline,
                      isBefore,
                      timeText
                    }) => {

  let srvr = localDev ? config.URL : '';

  return <div className="block avatar">
    <a target={"_blank"} href={`/profile/${id}`}>
      <div className="image">
        <div className="box">
          <img src={srvr + avatar} alt={`Фото ${firstname}`}/>
        </div>
        {isOnline && <div className="link-icon"/>}
      </div>
      <div className="desc">
        <div className="name">{firstname}</div>
        <span className={isBefore ? 'gray' : 'reds'}>{timeText}</span>
      </div>
    </a>
  </div>
};

ItemAvatar.propTypes = {
  avatar: PropTypes.string,
  name: PropTypes.string,
  like: PropTypes.number,
  dislike: PropTypes.number,
  cost: PropTypes.number,
  isOnline: PropTypes.bool
};

ItemAvatar.defaultProps = {
  avatar: guest,
  name: 'Игорь Иванович',
  like: 325,
  dislike: -2,
  cost: 2002,
  isOnline: false
};


function mapStateToProps({app}) {
  return {localDev: app.localDev};
}

export default connect(mapStateToProps)(ItemAvatar);

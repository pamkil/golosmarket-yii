function checkExcludes() {
    var select = $('select[id$="cities"]'),
        settings = select.attr('data-krajee-select2'),
        citiesGroup = $('.form-group[class$="cities"]'),
        excludes = [];

    if (!select.length) {
        return;
    }

    settings = window[settings];

    $('div[id$="catalogs"] input[type="checkbox"]').each(function () {
        var that = $(this);
        if (that.is(':checked')) {
            excludes.push('exclude[]=' + that.val());
        }
    });

    if (excludes.length == 5) {
        citiesGroup.hide();
    } else {
        settings.ajax.url = settings.ajax.url + (settings.ajax.url.indexOf('?') === -1 ? '?' : '&') + excludes.join('&');
        settings.ajax.delay = 250;
        settings.ajax.data = function(params) { return {q:params.term, page: params.page}; };
        settings.ajax.cache = true;
        select.select2(settings);
        citiesGroup.show();
    }
}

$(document).ready(function () {
    checkExcludes();

    $('div[id$="catalogs"] input[type="checkbox"]').change(function () {
        var that = $(this),
            base = that.closest('div[id$="catalogs"]'),
            select = $('select[id$="cities"]');

        if (that.val() == 1) { // Главные города
            if (that.is(':checked')) {
                base.find('input[type="checkbox"][value="4"]').prop('checked', true);
                base.find('input[type="checkbox"][value="5"]').prop('checked', true);
                base.find('input[type="checkbox"][value="4"]').closest('.checkbox').find('label').addClass('readonly');
                base.find('input[type="checkbox"][value="5"]').closest('.checkbox').find('label').addClass('readonly');
            } else {
                base.find('input[type="checkbox"][value="4"]').closest('.checkbox').find('label').removeClass('readonly');
                base.find('input[type="checkbox"][value="5"]').closest('.checkbox').find('label').removeClass('readonly');
            }
        }

        checkExcludes();

        $.uniform.update();
    });

    $('div[id$="catalogs"] .checkbox label').on('click', function (e) {
        if ($(this).hasClass('readonly')) {
            e.preventDefault();
        }
    });
});

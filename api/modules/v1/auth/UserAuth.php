<?php

namespace api\modules\v1\auth;

use yii\filters\auth\HttpBearerAuth;
use api\modules\v1\models\records\auth\User;

class UserAuth extends HttpBearerAuth
{
    /**
     * @param \yii\web\User $user
     * @param \yii\web\Request $request
     * @param \yii\web\Response $response
     *
     * @return null
     *
     * @throws \yii\web\UnauthorizedHttpException
     */
    public function authenticate($user, $request, $response)
    {
        $userModel = new User;

        $authHeader = $request->getHeaders()->get('Authorization');
        preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches);
        if (!$matches) {
            preg_match('/^(.*?)$/', $authHeader, $matches);
        }
        if ($authHeader !== null && $matches) {
            $identity = $userModel->findIdentityByAccessToken($matches[1]);
            if ($identity === null) {
                $this->handleFailure($response);
            }

            return $identity;
        }

        return null;
    }
}

<?php

namespace api\modules\v1\models\filters\user;

use api\modules\v1\models\records\announcer\Announcer;
use api\modules\v1\models\records\auth\User;
use api\modules\v1\models\records\user\Chat;
use api\modules\v1\models\records\user\ListChat;
use common\models\user\Favorite;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * @OA\Schema(title="Форма запроса для поиска дикторов")
 */
class ChatSearch extends Model
{
    /**
     * @OA\Property(
     *     type="integer",
     *     description="From user",
     *     title="interlocutorUserId",
     * )
     *
     * @var string
     */
    public $interlocutorUserId;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Another user ",
     *     title="currentUserId",
     * )
     *
     * @var string
     */
    public $currentUserId;

    /**
     * @param int      $opponentId
     * @param int      $myId
     * @param bool     $isAnnouncer
     * @param int|null $lastId
     * @param int      $perPage
     *
     * @return ActiveDataProvider
     */
    public function search(int $opponentId, int $myId, bool $isAnnouncer, ?int $lastId, int $perPage)
    {
        $perPage = $perPage > 80 ? 80 : $perPage;
        $query = Chat::find()
            ->with(['announcer', 'client']);

        if ($lastId) {
            $query->andWhere(['<', 'id', $lastId]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'created_at' => [
                        'asc' => ['created_at' => SORT_ASC],
                        'desc' => ['created_at' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'id',
                ],
                'defaultOrder' => ['id' => SORT_DESC]
            ],
            'pagination' => [
                'pageSize' => $perPage,
            ]
        ]);

        $fieldMy = $isAnnouncer ? 'announcer_id' : 'client_id';
        $field = $isAnnouncer ? 'client_id' : 'announcer_id';

        $query->andWhere([
            $field => $opponentId,
            $fieldMy => $myId,
        ]);

        return $dataProvider;
    }

    /**
     * @param int $userId
     * @param bool $isAnnouncer
     * @param string $filter
     * @param int $page
     * @param int $perPage
     *
     * @return ActiveDataProvider
     */
    public function getList(int $userId, bool $isAnnouncer, string $filter, int $page, int $perPage)
    {
        $perPage = $perPage > 80 ? 80 : $perPage;
        $query = ListChat::find()
            ->with(['announcer', 'client']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'created_at',
                ],
                'defaultOrder' => ['created_at' => SORT_DESC]
            ],
            'pagination' => [
                'pageSize' => $perPage,
                'page' => $page - 1,
            ]
        ]);

        $field = $isAnnouncer ? 'announcer_id' : 'client_id';

        $query->andWhere([
            $field => $userId,
        ]);

        $subQuery = (new Query());
        $subQuery->select(["max(id) as id"])
            ->from(['uc2' => Chat::tableName()])
            ->where(["uc2.$field" => $userId])
            ->andWhere(['deleted_at' => 0])
            ->groupBy(['announcer_id', 'client_id']);

        $query
            ->innerJoin(['uc3' => $subQuery], 'uc3.id = ' . Chat::tableName() . '.id');

        $fieldOp = $isAnnouncer ? 'client_id' : 'announcer_id';
        switch ($filter) {
            case 'woman':
            case 'man':
                $subQuery = Announcer::find()
                    ->select('user_id')
                    ->joinWith(['genders genders'])
                    ->where(['genders.code' => $filter]);

                $query->andWhere([$fieldOp => $subQuery]);
                break;

            case 'favorite':
                $isClient = !Yii::$app->user->identity || Yii::$app->user->identity->type === User::TYPE_CLIENT;
                $favorite = Favorite::find()
                    ->select($isClient ? 'announcer_id' : 'client_id')
                    ->where([$isClient ? 'client_id' : 'announcer_id' => Yii::$app->user->id]);

                $query->andWhere([$fieldOp => $favorite]);
                break;
        }

        return $dataProvider;
    }
}

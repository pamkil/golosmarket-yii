<?php

namespace api\modules\v1\models\checks\favorite;

use yii\base\Model;

class UpdateForm extends Model
{
    public $localFavList;

    public function formName()
    {
        return '';
    }

    public function rules()
    {
        return [
            [
                'localFavList', 'required'
            ],
            [
                'localFavList',
                'each',
                'rule' => [
                    'number'
                ],
                'skipOnEmpty' => false
            ]
        ];
    }

    public function loadFromBody()
    {
        $this->load(\Yii::$app->request->bodyParams);
    }
}

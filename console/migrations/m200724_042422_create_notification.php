<?php

use yii\db\Migration;

/**
 * Class m200724_042422_create_notification
 */
class m200724_042422_create_notification extends Migration
{
    private $notification = '{{%user_notification}}';
    private $user = '{{%user}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            $this->notification,
            [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer()->notNull(),
                'other_id' => $this->integer()->null(),
                'status' => $this->smallInteger()->notNull(),
                'type' => $this->smallInteger()->notNull(),
                'text' => $this->text()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'updated_at' => $this->dateTime()->notNull(),
                'INDEX `IX-user_notification-user_id` (user_id)',
                'INDEX `IX-user_notification-status` (status)',
                'FOREIGN KEY `FK-user_notification-user_id` (user_id) REFERENCES ' . $this->user . ' (id) ON DELETE CASCADE ON UPDATE CASCADE'
            ],
            $tableOptions
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->notification);
    }
}

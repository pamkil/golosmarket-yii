import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";

import ProfileForm from "./ProfileForm";
import {
  getProfileFull,
  updateProfile,
  removeFile,
  soundDefault,
  setShowWorkTime,
} from "../../data/redux";

import "./bootstrap.css";
import "./profile.css";

const Profile = () => {
  const dispatch = useDispatch();
  const [showWorkTimePopUp, setShowWorkTimePopUp] = useState(false);
  const [showTelegramQRPopUp, setShowTelegramQRPopUp] = useState(false)
  const [fileLoading, setFileLoading] = useState("");
  const [keyForm, setKeyForm] = useState(1);
  const [telegramLink, setTelegramLink] = useState("")
  const { isInit, isLoading, profile, isLogin, user, notifications } = useSelector(
    (state) => ({
      isInit: state.app.initialized,
      isLoading: state.profile.isLoading,
      profile: state.profile.profile,
      notifications: state.profile.notifications,
      user: state.user,
      isLogin: !!state.auth.token,
    })
  );

  const [notificationsState, setNotificationsState] = useState({
    sms: notifications.sms !== undefined && notifications.sms,
    email: notifications.email !== undefined && notifications.email,
    telegram: notifications.telegram !== undefined && notifications.telegram,
  });
  useEffect(()=>{
    setNotificationsState({
      sms: notifications.sms,
      email: notifications.email,
      telegram: notifications.telegram,
    })
  },[notifications])

  useEffect(() => {
    if (isInit && isLogin) {
      dispatch(getProfileFull());
    }
  }, [isInit, isLogin]);

  useEffect(() => {
    if (profile.update_time) {
      if (fileLoading) {
        setFileLoading("");
      } else {
        toast.info(
          "Изменения вступили в силу. Скоро данные появятся в профиле"
        );
      }
    }
  }, [profile.update_time]);

  if (!isInit || isLoading) {
    return null;
  }

  //if (isInit && !isLogin && document.location.pathname.includes('profile')) {
  //  window.location = '/';
  //}

  if (!isLogin) {
    return null;
  }

  const showTelegramQRPopUpHandler = () => {
    setShowTelegramQRPopUp(true)
  }

  const showWorkTimePopUpHandler = () => {
    setShowWorkTimePopUp(true);
  };

  const handleSubmit = (values) => {
    let params = Object.assign({}, values);
    params.notificationSms = notificationsState.sms
    params.notificationEmail = notificationsState.email
    params.notificationTelegram = notificationsState.telegram
    if (params.hasOwnProperty("addAvatar")) {
      setFileLoading("avatar");
    }

    if (params.hasOwnProperty("addSounds")) {
      setFileLoading("sounds");
    }

    if (params.hasOwnProperty("addPhotos")) {
      setFileLoading("photos");
    }

    if(!params.hasOwnProperty("addPhotos") &&
    !params.hasOwnProperty("addSounds") &&
    !params.hasOwnProperty("addAvatar") && user.user.isAnnouncer){
      showWorkTimePopUpHandler()
    }

    setKeyForm((key) => ++key);
    dispatch(updateProfile(params));
  };

  const handleUpload = (values) => {
    //setKeyForm(key => ++key);
    dispatch(updateProfile(values));
  };

  const removeSoundHandler = ({ url }) => {
    if (url) {
      dispatch(removeFile({ field: "sounds", url }));
    }
  };

  const removePhotoHandler = ({ url }) => {
    dispatch(removeFile({ field: "photos", url }));
  };

  const removeAvatarHandler = ({ url }) => {
    dispatch(removeFile({ field: "photo", url }));
  };

  const defaultSoundHandler = ({ url }) => {
    if (url) {
      dispatch(soundDefault({ url }));
    }
  };

  return (
    <>
      <ProfileForm
        user={user}
        profile={profile}
        keyForm={keyForm}
        fileLoading={fileLoading}
        onSubmit={handleSubmit}
        onFileUpload={handleUpload}
        onRemoveSound={removeSoundHandler}
        onRemovePhoto={removePhotoHandler}
        onRemoveAvatar={removeAvatarHandler}
        showWorkTimePopUp={showWorkTimePopUp}
        setShowWorkTimePopUp={setShowWorkTimePopUp}
        showTelegramQRPopUp={showTelegramQRPopUp}
        setShowTelegramQRPopUp={setShowTelegramQRPopUp}
        onDefaultSound={defaultSoundHandler}
        showWorkTimePopUpHandler={showWorkTimePopUpHandler}
        notifications={notificationsState}
        showTelegramQRPopUpHandler={showTelegramQRPopUpHandler}
        setTelegramLink={setTelegramLink}
        setNotifications={setNotificationsState}
        telegramLink={telegramLink}
      />
    </>
  );
};

export default Profile;

<?php

namespace common\service\chat\enum;

class FileTypeEnum
{
    public const FILE_TYPE_ALL = 'all';
    public const FILE_TYPE_PUBLIC = 'public';
    public const FILE_TYPE_ORIGIN = 'origin';
    public const FILE_TYPE_WATERMARK = 'watermark';

    public static function getKeys()
    {
        return [
            self::FILE_TYPE_ALL,
            self::FILE_TYPE_PUBLIC,
            self::FILE_TYPE_ORIGIN,
            self::FILE_TYPE_WATERMARK,
        ];
    }
}

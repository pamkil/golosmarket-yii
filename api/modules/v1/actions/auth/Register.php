<?php

namespace api\modules\v1\actions\auth;

use api\modules\v1\actions\common\CreateAction;
use api\modules\v1\models\checks\auth\CreateForm;
use api\modules\v1\models\records\auth\User;
use Exception;
use Yii;

class Register extends CreateAction
{
    public $modelClass = User::class;

    /**
     * Созадние
     * @OA\Post(
     *      path="/auth/register",
     *      summary="Create user. Создание пользователя",
     *      tags={"User"},
     *      @OA\RequestBody(
     *          description="Создание пользователя",
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/CreateForm")
     *      ),
     *    @OA\Response(
     *        response="200",
     *        description="Объект токена",
     *        @OA\JsonContent(ref="#/components/schemas/Token")
     *    )
     * )
     *
     * @return mixed|null
     * @throws \yii\base\Exception
     * @throws Exception
     */
    public function run()
    {
        $model = new CreateForm();
        $model->load(Yii::$app->request->bodyParams);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            // создание пользователя и отправка письма
            if ($model->signup()) {
                // если создание успешно, то добавляем ему права для выбранной роли
                $roles = [User::ROLE_GUEST];

                switch ($model->type) {
                    case User::TYPE_STRING_ANNOUNCER:
                        $roles[] = User::ROLE_ANNOUNCER;
                        break;
                    case User::TYPE_STRING_CLIENT:
                        $roles[] = User::ROLE_CLIENT;
                        break;
                    default:
                        break;
                }

                $model->getUser()->loadRoles($roles ?? []);
                $transaction->commit();
            }
        } catch (Exception $exception) {
            $transaction->rollBack();

            if ($model->hasErrors('uniqueEmail')) {
                Yii::$app->response->statusCode = 406;
                return [[
                    'field' => 'email',
                    'message' => 'Этот e-mail занят.'
                ]];
            }

            throw $exception;
        }

        if ($model->errors) {
            Yii::$app->response->statusCode = 406;

            foreach ($model->getFirstErrors() as $name => $message) {
                return [[
                    'field' => $name,
                    'message' => $message
                ]];
            }
        }

        return $model;
    }
}

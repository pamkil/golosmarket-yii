import React, { useCallback, useEffect, useState } from 'react';
import { connect } from "react-redux";
import copy from "copy-to-clipboard";

import ListAnnouncers from "./ListAnnouncers/ListAnnouncers";
import Selector from "./Selector/Selector";
import ListMessages from "./ListMessages/ListMessages";
import Top from "./Top/Top";
import {
    announcerChangeFavorite,
    cancelBill,
    createBill,
    requestChats,
    requestMessages,
    sendFiles,
    sendMessage,
    updateBill,
} from "../../data/redux/chat";
import { initializeApp } from "../../data/redux";

import "./chat.css";
import { FavoriteService } from "../../data/services";
import usePathName from "../../data/pathName/usePathName";
import { isMobile as isMobileFunc } from "../../data/helpers/Tools"
import Loader from "../Common/Loader/Loader";

const filters = [
    {
        id: 'man',
        title: 'Мужские'
    },
    {
        id: 'woman',
        title: 'Женские',
    },
    {
        id: 'favorite',
        title: 'Избранные'
    }
];
const copyHandler = (fileUrl) => {
    copy(document.location.origin + fileUrl);
};

// let lastIndex = 0;
function Chat(
    {
        isLogin,
        announcers,
        messages,
        isLoadUser,
        sendMessage,
        sendFiles,
        requestChats,
        requestMessages,
        inProgressMessage,
        pageMessage,
        totalCountMessage,
        sizePageMessage,
        loadNewAnnouncer,

        initializeApp,
        isInit,
        bill,
        isAnnouncer, inProgressAnnouncer,
        opponent, cancelBill, createBill, updateBill, announcerChangeFavorite,
        sizePageAnnouncer,
        totalCountAnnouncer,
        pageAnnouncer
    }
) {
    const setInitialize = useCallback( initializeApp, [initializeApp] );
    const [pathName, setPathParams] = usePathName();
    const [filter, setFilter] = useState( '' );
    const [idAnnouncerActive, setIdActive] = useState( null );
    const [isMobile, setIsMobile] = useState( isMobileFunc() );
    const [showAnnouncerList, setShowAnnouncerList] = useState( false )
    const [listAnnouncers, setListAnnouncers] = useState( [] );
    const [loadedPage, setLoadedPage] = useState( [] );
    const requests = useCallback( filter => {
        requestChats( { filter } );
    }, [requestChats] );
    const emptyFunc = () => {
    };

    const checkResize = () => {
        const isMobile = isMobileFunc();
        setIsMobile( isMobile );
        const sd = document.querySelector('.right-chat-block');
        if ( sd ) {
        if ( isMobile ) {
                sd.style.height = (window.innerHeight - 0) + 'px';
            } else {
                sd.style.height = (window.innerHeight - 56) + 'px';
            }
        }
    }

    useEffect( () => {
        window.addEventListener( 'resize', checkResize );
        setTimeout( checkResize, 500 );

        return () => {
            window.removeEventListener( 'resize', checkResize );
        };
    }, [loadNewAnnouncer, idAnnouncerActive] );

    useEffect( () => {
        setInitialize();
    }, [setInitialize] );

    useEffect( () => {
        if ( pathName && pathName.length > 1 ) {
            setIdActive( parseInt( pathName[1] ) );
        } else {
            setIdActive( null );
        }
    }, [pathName] );

    useEffect( () => {
        if ( loadedPage.findIndex( item => item === pageAnnouncer ) > -1 ) {
            return;
        }
        const list = [
            ...listAnnouncers,
            ...announcers
        ];

        setLoadedPage( [...loadedPage, pageAnnouncer] );
        setListAnnouncers( list );
        // eslint-disable-next-line
    }, [announcers, pageAnnouncer] );

    useEffect( () => {
        setShowAnnouncerList(
            !isMobile ||
            !!( isMobile && !idAnnouncerActive )
        );
    }, [isMobile, idAnnouncerActive] )

    useEffect( () => {
        if ( isLogin ) {
            requests( filter );
        }
    }, [requests, filter, isLogin] );

    const hasNextPage = listAnnouncers.length < ( totalCountAnnouncer - 1 );
    const loadAnnouncerPage = ({ startIndex }) => {
        const pageStart = parseInt( startIndex / sizePageAnnouncer ) + 1;
        requestChats( {
            page: pageStart
        } );
    }
    const getAnnouncer = index => listAnnouncers[index] || false;
    const isRowLoaded = ({ index }) => !hasNextPage || index < listAnnouncers.length;
    const loadMoreRows = inProgressAnnouncer ? emptyFunc : loadAnnouncerPage;
    const announcerCount = hasNextPage ? listAnnouncers.length + 1 : listAnnouncers.length;

    //region state
    useEffect( () => {
        if ( !isMobile && !idAnnouncerActive && listAnnouncers.length ) {
            setIdActive( listAnnouncers[0].opponentId );
        }
    }, [listAnnouncers, idAnnouncerActive, isMobile] );

    useEffect( () => {
        const body = document.body;
        const nameClass = 'hide-header';
        if ( isMobile && idAnnouncerActive > 0 ) {
            if ( !body.classList.contains( nameClass ) ) {
                body.classList.add( nameClass )
            }
        } else {
            if ( body.classList.contains( nameClass ) ) {
                body.classList.remove( nameClass )
            }
        }
    }, [idAnnouncerActive] );

    const createBillHandler = bill => {
        createBill( idAnnouncerActive, bill.sum )
    }

    const updateBillHandler = bill => {
        updateBill( idAnnouncerActive, bill.sum )
    }

    const cancelBillHandler = () => {
        cancelBill( idAnnouncerActive )
    }

    const filterChangeHandler = (newFilter) => {
        setFilter( filter === newFilter ? {} : newFilter );
        setPathParams( '/chat' );
        setLoadedPage( [] );
        setListAnnouncers( [] );
    };

    const addToFavoriteHandler = (id, isFavorite) => {
        if ( isLogin ) {
            let service;
            if ( isFavorite ) {
                service = FavoriteService.removeFromFavorite( id );
            } else {
                service = FavoriteService.addToFavorite( id );
            }
            service.then( () => {
                announcerChangeFavorite( !isFavorite );
            } );
        }
    };

    if ( !isLogin || !isInit ) {
        return <div className="non-auth">
            <p>Необходимо <a href="#login">войти</a> или <a href="#registry">зарегистрироваться</a></p>
            <a href="/">Перейти на главную страницу?</a>
        </div>;
    }

    const setAnnouncerIdHandler = id => {
        setPathParams( `/chat/${ id }` );
    }

    const backClickHandler = event => {
        event.preventDefault();
        if ( showAnnouncerList ) {
            window.location = '/';
        } else {
            setPathParams( '/chat/' );
        }
    }

    if ( !isLoadUser ) {
        return (
            <section className="catalog" key="loader-announcer">
                <Loader/>
            </section>
        );
    }

    //endregion

    return (
        <div className="container no-padding">
            <div className="chat-block">
                {
                    showAnnouncerList
                        ? <ListAnnouncers
                            idAnnouncerActive={ idAnnouncerActive }
                            setIdActive={ setAnnouncerIdHandler }
                            totalCount={ announcerCount }
                            getAnnouncer={ getAnnouncer }
                            isRowLoaded={ isRowLoaded }
                            loadMoreRows={ loadMoreRows }
                            keyItem={ JSON.stringify( filter ) + isLoadUser + announcerCount }
                        >
                            <Selector
                                filters={ filters }
                                onChange={ filterChangeHandler }
                                filterActive={ filter }
                                isAnnouncer={ isAnnouncer }
                            />
                        </ListAnnouncers>
                        : null
                }
                {
                    idAnnouncerActive
                        ? <div className="right-chat-block">
                            <>
                                <Top
                                    createBill={ createBillHandler }
                                    updateBill={ updateBillHandler }
                                    bill={ bill }
                                    cancelBill={ cancelBillHandler }
                                    isAnnouncer={ isAnnouncer }
                                    addToFavorite={ addToFavoriteHandler }
                                    opponent={ opponent }
                                    copyHandler={ copyHandler }
                                    onBackClick={ backClickHandler }
                                />
                                {
                                    loadNewAnnouncer
                                        ? <div className="margin-top-60"><Loader/></div>
                                        : (
                                            <ListMessages
                                                totalCount={ messageCount }
                                                isRowLoaded={ isRowLoadedMessage }
                                                loadMoreRows={ emptyFunc }
                                                getMessage={ getMessage }
                                                version={storeMessages.length}
                                                toScroll={ scrollMessage }
                                                announcerId={ idAnnouncerActive }
                                            />
                                        )
                                }
                            </>
                        </div>
                        : ''
                }
            </div>
        </div>
    );
}

function mapStateToProps(
    {
        chat: {
            chat: {
                items: announcers,
                pagination: { perPage: sizePageAnnouncer, page: pageAnnouncer, totalCount: totalCountAnnouncer },
                inProgress: inProgressAnnouncer,
            },
            message: {
                opponent,
                items: messages,
                newMessages,
                bill,
                pagination: { perPage: sizePageMessage, page: pageMessage = 0, totalCount: totalCountMessage },
                inProgress: inProgressMessage,
                loadNewAnnouncer,
            },
        },
        auth, app, user
    }
) {
    return {
        announcers: announcers || [],
        messages: messages || [],
        newMessages: newMessages || [],
        filters,
        isLogin: !!auth.token,
        isInit: app.initialized,
        isLoadUser: !!( user && user.user ),
        isAnnouncer: !!( user && user.user && user.user.isAnnouncer ),

        // loadMessages,
        opponent,
        bill,
        inProgressMessage,
        sizePageMessage,
        pageMessage,
        loadNewAnnouncer,
        totalCountMessage: totalCountMessage || -1,

        inProgressAnnouncer,
        sizePageAnnouncer,
        pageAnnouncer,
        totalCountAnnouncer: totalCountAnnouncer || 0,
    };
}

export default connect(
    mapStateToProps,
    {
        requestChats, requestMessages, sendMessage, sendFiles, initializeApp,
        cancelBill, createBill, updateBill, announcerChangeFavorite
    }
)( Chat );

<?
use yii\db\ActiveQuery;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use common\models\content\Article;
use common\models\content\ArticleSearch;
use admin\widgets\RestCheckbox;
use admin\components\grid\SocketGrid;

/**
 * @var ActiveDataProvider $dataProvider
 * @var ArticleSearch $searchModel
 */
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <? if (Yii::$app->user->can('updateNews')) { ?>
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<?= Url::to(['create']) ?>" class="btn sbold green"> <?= Yii::t('admin', 'Добавить') ?>
                                    <i class="fa fa-plus"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            <? } ?>

            <?= SocketGrid::widget([
                'baseClass' => Article::class,
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'active',
                        'format' => 'raw',
                        'value' => function ($data) use ($dataProvider) {
                            if (!Yii::$app->user->can('updateArticles')) {
                                return $data->active ? 'Да' : 'Нет';
                            }

                            /* @var ActiveQuery $query */
                            $query = $dataProvider->query;
                            return RestCheckbox::widget([
                                'id' => $data->id,
                                'name' => 'active',
                                'modelClass' => $query->modelClass,
                                'checked' => $data->active
                            ]);
                        },
                        'contentOptions' => ['style' => 'width: 100px; text-align: center;'],
                        'enableSorting' => false,
                        'filter' => false
                    ],
                    'name',
                    'date:date',
                    'updated_at:datetime',
                    [
                        'class' => 'admin\components\grid\DuplicateColumn',
                        'template' => Yii::$app->user->can('updateArticles') ? '{update} {duplicate} {delete}' : ''
                    ]
                ]
            ]) ?>
        </div>
    </div>
</div>

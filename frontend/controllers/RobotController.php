<?php

namespace frontend\controllers;

use common\helpers\locations\City;
use Yii;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\models\Sitemap;

class RobotController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ]
        ];
    }

    public function actionSitemap()
    {
        $this->layout = '/empty';

        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/xml');

        $now = time();
        $timestamp = 0;
        $path = Yii::getAlias('@runtime/sitemap.xml');
        if (file_exists($path)) {
            $timestamp = date('U', filemtime($path));
        }

        if ($now - $timestamp >= (86400 + rand(20000, 100000))) {
            $sitemap = new Sitemap;
            $urls = $sitemap->generate();

            $xml = $this->renderPartial('sitemap', [
                'urls' => $urls
            ]);

            FileHelper::createDirectory(dirname($path));
            file_put_contents($path, $xml);
        }

        return file_get_contents($path);
    }
}

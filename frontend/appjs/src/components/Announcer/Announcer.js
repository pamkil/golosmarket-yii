import React, { useEffect, useState } from 'react';

import { connect, useDispatch } from 'react-redux';
import copy from 'copy-to-clipboard';
import { toast } from 'react-toastify';

import { announcerChangeFavorite, setFavLocalList } from '../../data/redux';
import ListView from './ListView/ListView';
import CompilationList from './CompilationList/CompilationList';
import Reviews from './Reviews/Reviews';
import { FavoriteService } from '../../data/services';

import { eventPlay, eventStop } from '../Common/PlayerEvent/playerEvent';
import { PLAYER } from '../Common/PlayerEvent/playerConst';
import { requestCollections } from '../../data/redux/collection';

const copyHandler = (fileUrl) => {
  copy(document.location.origin + fileUrl);
};

export const textNoLogin = 'Пожалуйста войдите в систему или зарегистрируйтесь';
export const textAddFavorite = 'Голос сохранен в Избранное';
export const textRemoveFavorite = 'Голос удалён из Избранное';
export const textCopy = 'Ссылка на демо исполнителя скопирована';
export const textCopyFile = 'Ссылка на файл скопирована';
export const textProfile = 'Ссылка на профиль исполнителя скопирована';
export const textRegister =
  'Почти готово! Для завершения регистрации зайдите в почту и перейдите по ссылке из письма.';
export const textRestore = 'Вам отправлено письмо на указанный адрес';

const Announcer = ({
  localDev,
  mobile,
  announcers,
  reviews,
  isLogin,
  favoritePage,
  announcerChangeFavorite,
  favList,
  setFavLocalList,
  isLoadingAnnouncer,
  isAnnouncer,
}) => {
  const dispatch = useDispatch();

  useEffect(() => {
    document.addEventListener(PLAYER.PLAYING, playingHandler);
    document.addEventListener(PLAYER.STOP, stopHandler);

    dispatch(requestCollections());

    return () => {
      document.removeEventListener(PLAYER.PLAYING, playingHandler);
      document.removeEventListener(PLAYER.STOP, stopHandler);
    };
  }, []);

  if (localDev) {
    reviews = [
      {
        clientAvatar: '/images/avatar.png',
        clientName: 'Владимир',
        createdAt: '2021-01-29 20:01:04',
        id: 9,
        rating: 5,
        text: 'Отличный сервис!',
        updatedAt: '2021-01-29 20:39:32',
        user_id: 1706,
      },
      {
        clientAvatar: '/images/avatar.png',
        clientName: 'Владимир',
        createdAt: '2021-01-29 20:01:04',
        id: 9,
        rating: 5,
        text: 'Отличный сервис!',
        updatedAt: '2021-01-29 20:39:32',
        user_id: 1706,
      },
      {
        clientAvatar: '/images/avatar.png',
        clientName: 'Владимир',
        createdAt: '2021-01-29 20:01:04',
        id: 9,
        rating: 5,
        text: 'Отличный сервис!',
        updatedAt: '2021-01-29 20:39:32',
        user_id: 1706,
      },
      {
        clientAvatar: '/images/avatar.png',
        clientName: 'Владимир',
        createdAt: '2021-01-29 20:01:04',
        id: 9,
        rating: 5,
        text: 'Отличный сервис!',
        updatedAt: '2021-01-29 20:39:32',
        user_id: 1706,
      },
      {
        clientAvatar: '/images/avatar.png',
        clientName: 'Владимир',
        createdAt: '2021-01-29 20:01:04',
        id: 9,
        rating: 5,
        text: 'Отличный сервис!',
        updatedAt: '2021-01-29 20:39:32',
        user_id: 1706,
      },
      {
        clientAvatar: '/images/avatar.png',
        clientName: 'Владимир',
        createdAt: '2021-01-29 20:01:04',
        id: 9,
        rating: 5,
        text: 'Отличный сервис!',
        updatedAt: '2021-01-29 20:39:32',
        user_id: 1706,
      },
      {
        clientAvatar: '/images/avatar.png',
        clientName: 'Владимир',
        createdAt: '2021-01-29 20:01:04',
        id: 9,
        rating: 5,
        text: 'Отличный сервис!',
        updatedAt: '2021-01-29 20:39:32',
        user_id: 1706,
      },
      {
        clientAvatar: '/images/avatar.png',
        clientName: 'Владимир',
        createdAt: '2021-01-29 20:01:04',
        id: 9,
        rating: 5,
        text: 'Отличный сервис!',
        updatedAt: '2021-01-29 20:39:32',
        user_id: 1706,
      },
      {
        clientAvatar: '/images/avatar.png',
        clientName: 'Владимир',
        createdAt: '2021-01-29 20:01:04',
        id: 9,
        rating: 5,
        text: 'Отличный сервис!',
        updatedAt: '2021-01-29 20:39:32',
        user_id: 1706,
      },
      {
        clientAvatar: '/images/avatar.png',
        clientName: 'Владимир',
        createdAt: '2021-01-29 20:01:04',
        id: 9,
        rating: 5,
        text: 'Отличный сервис!',
        updatedAt: '2021-01-29 20:39:32',
        user_id: 1706,
      },
    ];
  }

  const [playing, setPlaying] = useState('');
  const [compList, setCompList] = useState(
    window.collectionsList
      ? window.collectionsList
      : [
        ],
  );
  const addToFavoriteHandler = (id, isFavorite, quantityFavorite) => {
    if (isLogin) {
      let service;
      if (isFavorite || favList.indexOf(id) >= 0) {
        service = FavoriteService.removeFromFavorite(id);
        if (favList.length) {
          favList.splice(favList.indexOf(id), 1);
        }
      } else {
        service = FavoriteService.addToFavorite(id);
        favList.push(id);
      }
      service.then(({ data: { quantity } }) => {
        announcerChangeFavorite(id, !isFavorite, quantity);
      });
      toast.info(isFavorite ? textRemoveFavorite : textAddFavorite);
    } else {
      if (favList.indexOf(id) >= 0) {
        if (favList.length) {
          favList.splice(favList.indexOf(id), 1);
        }

        announcerChangeFavorite(id, false, quantityFavorite - 1);

        toast.info(textRemoveFavorite);
      } else {
        favList.push(id);

        announcerChangeFavorite(id, true, quantityFavorite + 1);

        toast.info(textAddFavorite);
      }

      setFavLocalList(favList);
    }
  };

  const copyInnerHandler = (url) => {
    copyHandler(url);
    toast.info(textCopy);
  };

  const playHandler = (file, itemFiles, announcer) => {
    const files = [];
    for (let annons of [announcers]) {
      for (let item of annons) {
        if (item.sound && item.sound.length > 0) {
          item.sound[0].announcer = item;
          files.push(item.sound[0]);
        }
      }
    }
    eventPlay(announcer, file, files, true);
  };

  const stopClickHandler = () => {
    if (!mobile) {
      localStorage.setItem('isWavePlaying', false);
    }
    eventStop();
  };

  const stopHandler = () => {
    setPlaying('');
  };

  const playingHandler = ({ detail: { file } }) => {
    if (!mobile) {
      localStorage.setItem('isWavePlaying', true);
    }
    setPlaying(file.url);
  };

  const sendMessageHandler = () => {
    if (!isAnnouncer && !isLogin) {
      window.location.hash = '#registry';
    }
  };

  return (
    <>
      <section className="catalog">
        <ListView
          isLoadingAnnouncer={isLoadingAnnouncer}
          items={announcers}
          showMore={true}
          showFavorite={false}
          copyHandler={copyInnerHandler}
          addToFavorite={addToFavoriteHandler}
          onClickSendMessage={sendMessageHandler}
          onClickPlay={playHandler}
          onClickStop={stopClickHandler}
          playingSong={playing}
          isAnnouncer={isAnnouncer}
          isLogin={isLogin}
        />
      </section>

      {localDev ? <div className="dev-info">{favoritePage ? 'favorite' : 'announcer'}</div> : null}

      {favoritePage ? null : (
        <>
          {compList.length ? (
            <section className="catalog">
              <div className="container">
                <div className="compilation_title">Подборки голосов</div>
              </div>

              <CompilationList mobile={mobile} items={compList} />
            </section>
          ) : null}

          {reviews && reviews.length ? (
            <section className="reviews">
              <Reviews reviews={reviews} />
            </section>
          ) : null}
        </>
      )}
    </>
  );
};

const mapStateToProps = ({ review, announcer, auth, user, app, favLocalList, profile }) => {
  return {
    localDev: app.localDev,
    mobile: app.mobile,
    userProfile: profile.userProfile,
    favList: favLocalList.favList,
    announcers: announcer.announcers,
    isLoadingAnnouncer: announcer.isLoadingAnnouncer || false,
    isAnnouncer: user.user ? user.user.isAnnouncer : false,
    reviews: review.reviews,
    isLoadingReview: review.isLoading,
    isLogin: !!auth.token,
  };
};

export default connect(mapStateToProps, { setFavLocalList, announcerChangeFavorite })(Announcer);

<?php

namespace common\service;

use api\modules\v1\models\checks\favorite\UpdateForm;
use common\models\user\Favorite as FavoriteModel;
use yii\db\Exception;
use Yii;

class Favorite
{
    public static function batchAdd(UpdateForm $form)
    {
        $clientId = Yii::$app->user->id;
        $favList = $form->localFavList;
        foreach ($favList as $favoriteId) {
            try {
                Yii::$app->db->createCommand()
                    ->insert(
                        FavoriteModel::tableName(),
                        [
                            'announcer_id' => $favoriteId,
                            'client_id' => $clientId,
                        ]
                    )->execute();
            } catch (Exception $e) {
            }
        }

        foreach ($favList as $favoriteId) {
            FavoriteModel::calculate($favoriteId);
        }

        return FavoriteModel::find()->where(['client_id' => $clientId])->count();
    }
}

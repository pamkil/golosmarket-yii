<?php

namespace api\modules\v1\models\checks\site;

use yii\base\Model;

/**
 * @OA\Schema(required={"text", "rating"}, title="Форма добавления отзыва о сервисе")
 */
class ReviewServiceForm extends Model
{
    /**
     * @OA\Property(
     *     type="string",
     *     description="Текст",
     *     title="text",
     * )
     *
     * @var string
     */
    public $text;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Оценка отзыва",
     *     title="rating",
     * )
     *
     * @var int
     */
    public $rating;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="",
     *     title="show_quantity",
     * )
     *
     * @var int
     */
    public $show_quantity;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Пользователь",
     *     title="user_id",
     * )
     *
     * @var int
     */
    public $user_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['show_quantity', 'user_id', 'rating'],
                'integer',
                'min' => 0
            ],
            [
                ['rating'],
                'integer',
                'min' => 1,
                'max' => 5,
            ],
            [
                ['user_id', 'rating', 'text'],
                'required',
            ],
            [
                ['text'],
                'string',
                'min' => 5
            ],
        ];
    }
}

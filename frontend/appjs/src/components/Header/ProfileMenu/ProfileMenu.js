import React, { useEffect, useRef, useState } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import avatarImgDef from "../../../images/avatar.png";
import config from "../../../data/config/";
import { getPaymentInfo } from "../../../data/redux";

const ProfileMenu = (props) => {
  let {
    name,
    updatePayment,
    localDev,
    yamoney,
    onLogout,
    avatarImg,
    id,
    showWorkTimeHandler,
    isAnnouncer,
    paidBills,
    cash,
  } = props;
  const dispatch = useDispatch();
  const [copied, setCopied] = useState(false);
  const [showCommissionPayedMessage, setShowCommissionPayedMessage] =
    useState(false);
  const [showApproveCommissionPayed, setShowApproveCommissionPayed] =
    useState(false);
  const [showProfileMenu, setShowProfileMenu] = useState(false);
  const profileMenuRef = useRef(null);
  const menuList = [
    {
      href: "/grafic",
      title: "График работы",
      isClick: showWorkTimeHandler,
      add: isAnnouncer,
    },
    { href: `/profile/${id}`, title: "Мой профиль" },
    { href: "/profile/edit", title: "Настройки профиля" },
    { href: "/history", title: "История заказов" },
    { href: "/logout", title: "Выход", isClick: onLogout },
  ];
  const { commissionData } = useSelector((state) => ({
    commissionData: state.user.payed,
  }));
  
  useEffect(() => {
      if(isAnnouncer){
        dispatch(getPaymentInfo());
      }
  }, [isAnnouncer]);

  let srvr = localDev ? config.URL : "";
  useEffect(() => {
    if (profileMenuRef === null || !window) {
      return;
    }

    
    const handleMouseDown = (e) => {
      if (!e) {
        return;
      }
      if (profileMenuRef.current.contains(e.target)) {
        return;
      } else {
        setShowProfileMenu(false);
      }
    };

    window.addEventListener("mousedown", handleMouseDown);
    return () => {
      window.removeEventListener("mousedown", handleMouseDown);
    };
  }, [profileMenuRef]);

  const copyHandler = () => {
    navigator.clipboard
      .writeText(commissionData.paymentInfo[0].commission_cardnumber)
      .then(() => {
        console.log("Скопировано");
      })
      .catch((error) => {
        console.error(`Текст не скопирован ${error}`);
      });
  };

  useEffect(() => {
    if (
      commissionData === null ||
      Object.keys(commissionData).length === 0 ||
      !isAnnouncer
    ) {
      return;
    }
    setShowCommissionPayedMessage(commissionData.confirmationWait);
  }, [commissionData]);

  console.log(commissionData, isAnnouncer)
  return (
    <div ref={profileMenuRef} className="name menu-hover">
      <ul>
        <li className="sub-menu">
          <div
            onClick={() => {
              setShowProfileMenu((prev) => !prev);
            }}
            className="miniava"
          >
            <span className="mobile-no">
              {name}
              <br />
              {isAnnouncer ? "исполнитель" : "клиент"}
            </span>
            <div className="frame">
              <img src={srvr + (avatarImg || avatarImgDef)} alt={name} />
            </div>
          </div>
          {showProfileMenu ? (
            <ul style={{ display: "block" }} className="menu-show-hover">
              {menuList.map(({ href, title, isClick = false, add = true }) => {
                if (!add) {
                  return null;
                }
                return (
                  <li key={href}>
                    {isClick ? (
                      <button type="button" onClick={() => isClick()}>
                        {title}
                      </button>
                    ) : (
                      <a href={href}>{title}</a>
                    )}
                  </li>
                );
              })}

              {isAnnouncer !== undefined && isAnnouncer ? (
                <li className="yamoney">
                  <div className="speaker-price-features-commission">
                    <div className="title-commission title">
                      {(!commissionData || commissionData === null ||
                        Object.keys(commissionData).length < 0 ||
                        commissionData.paymentInfo === null) && (
                          <>
                            <span className="title-commission-text">
                              Как работать на Голосмаркет:
                              <span>1. Выставьте счет в чате</span>
                              <span>
                                2. Клиент оплатит по номеру вашей карты
                              </span>
                              <span>3. После оплачивается комиссия</span>
                            </span>
                          </>
                        )}
                    </div>
                  </div>
                  {commissionData &&
                    Object.keys(commissionData).length > 0 &&
                    commissionData.paymentInfo !== null && (
                      <>
                        <p className="title-commission-text">
                          Всего зачислено:{" "}
                          <b style={{ color: "black" }}>
                            {commissionData.paymentInfo[0].sum}{" "}
                          </b>
                        </p>
                        <p className="title-commission-text">
                          {" "}
                          Общ. комиссия:{" "}
                          <b style={{ color: "black" }}>
                            {commissionData.paymentInfo[0].commission_sum}{" "}
                          </b>
                        </p>
                        {paidBills && (
                          <div><p className="title-commission-text">Ваши заказы:</p>
                            {paidBills.length !== 0 && paidBills.map((item) => {
                                return <div className="paid-bills-container" key={item.id}><p style={{fontWeight:"bold", paddingRight: "5px"}}>{item.sum}Р</p><p>{"(" + item.date + ")"}</p></div>
                            })}
                          </div>
                        )}
                        <p style={{ fontSize: "12px" }}>
                          Оплатите общую комиссию сразу или одним платежом до
                          первого числа каждого месяца. <br/> Инструкцию пришлём на
                          почту
                        </p>
                      </>
                    )}
                </li>
              ) : (
                <></>
              )}
            </ul>
          ) : (
            <></>
          )}
        </li>
      </ul>
    </div>
  );
};

function mapStateToProps({ app }) {
  return { localDev: app.localDev };
}

export default connect(mapStateToProps)(ProfileMenu);

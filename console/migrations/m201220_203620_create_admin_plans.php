<?php

use yii\db\Migration;

/**
 * Class m201220_203620_create_admin_plans
 */
class m201220_203620_create_admin_plans extends Migration
{
    private $table = '{{%admin_plans}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'date_plan' => $this->dateTime()->notNull(),
                'value' => $this->integer()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'updated_at' => $this->dateTime()->notNull(),
                'INDEX `IX-admin_plans-date_plan` (date_plan)',
            ],
            $tableOptions
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}

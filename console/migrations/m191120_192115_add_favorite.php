<?php

use yii\db\Migration;

/**
 * Class m191120_192114_add_reviews
 */
class m191120_192115_add_favorite extends Migration
{
    private $favorite = '{{%user_favorite}}';
    private $user = '{{%user}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            $this->favorite,
            [
                'id' => $this->primaryKey(),
                'announcer_id' => $this->integer()->notNull(),
                'client_id' => $this->integer()->notNull(),
                'created_at' => $this->timestamp()->notNull(),
                'updated_at' => $this->timestamp()->notNull(),
            ],
            $tableOptions
        );

        $this->createIndex(
            'IX-user_favorite-announcer_id-client_id',
            $this->favorite,
            ['announcer_id', 'client_id'],
            true
        );
        $this->createIndex('IX-user_favorite-announcer_id', $this->favorite, 'announcer_id');
        $this->createIndex('IX-user_favorite-client_id', $this->favorite, 'client_id');

        $this->addForeignKey(
            'FK-user_favorite-announcer_id',
            $this->favorite,
            'announcer_id',
            $this->user,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK-user_favorite-client_id',
            $this->favorite,
            'client_id',
            $this->user,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK-user_favorite-client_id', $this->favorite);
        $this->dropForeignKey('FK-user_favorite-announcer_id', $this->favorite);
        $this->dropTable($this->favorite);
    }
}

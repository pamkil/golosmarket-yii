<?php

namespace admin\widgets;

use Carbon\Carbon;
use common\models\user\Bill;
use yii\base\Widget;

class MonthCountOrder extends Widget
{
    public function run()
    {
        parent::run();

        /**
         * "Заказов за этот/прошлый мес: 18/39 - смотрим на оплаченные клиентом счета только?"
         * - Да, все верно. Только оплаченные.
         */

        $dateLast = Carbon::today()->subMonth()->startOfMonth()->toDateTimeString();
        $dateEndLast = Carbon::today()->subMonth()->endOfMonth()->toDateTimeString();
        $dateCur = Carbon::today()->startOfMonth()->toDateTimeString();
        $dateEndCur = Carbon::today()->endOfDay()->toDateTimeString();

        $quantityLast = Bill::find()
            ->cache(60 * 3)
            ->where(
                [
                    'AND',
                    ['>=', 'created_at', $dateLast],
                    ['<=', 'created_at', $dateEndLast],
//                    ['status' => [Bill::STATUS_PAID_CLIENT, Bill::STATUS_PAID_ANNOUNCER]],
                ]
            )
            ->count('sum');

        $quantityCur = Bill::find()
            ->cache(60 * 3)
            ->where(
                [
                    'AND',
                    ['>=', 'created_at', $dateCur],
                    ['<=', 'created_at', $dateEndCur],
//                    ['status' => [Bill::STATUS_PAID_CLIENT, Bill::STATUS_PAID_ANNOUNCER]],
                ]
            )
            ->count('sum');

        $data = [
            'title' => 'Заказов за месяц',
            'value1' => $quantityCur ?? 0,
            'smallValue1' => '(текущий)',
            'value2' => $quantityLast ?? 0,
            'smallValue2' => '(прошлый)',
        ];

        echo $this->render('allTwoRowWidgets', $data);
    }
}
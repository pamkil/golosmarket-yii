<?php

namespace admin\controllers;

use admin\models\content\QuestionSearch;
use common\models\content\Question;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ContentQuestionController extends SecurityController
{
    public function actionIndex()
    {
        $searchModel = new QuestionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionCreate()
    {
        $model = new Question();

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно создано');

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Создание';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

//        $model->active = 1;

        return $this->render('_form', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        /* @var Question $model */
        $model = Question::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Изменение';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('_form', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        /* @var Question $model */
        $model = Question::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect('index');
    }

    public function actionAjax($q = null, $page = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $page = $page ?? 1;
        $limit = 10;
        $offset = ($page - 1) * $limit;

        $out = [
            'results' => ['id' => '', 'text' => ''],
            'pagination' => ['more' => false]
        ];

        $query = Question::find()
            ->alias('question')
            ->distinct()
            ->select(['question.id', 'text'])
            ->orderBy(['question.position' => SORT_ASC])
            ->asArray();

        if (!empty($q)) {
            $query->andWhere(
                [
                    'OR',
                    ['like', 'text', $q . '%', false],
                    ['position' => (int)$q],
                ]
            );
        }

        $query->offset($offset)->limit($limit + 1);
        $out['results'] = $query->all();
        $out['pagination']['more'] = count($out['results']) > $limit;

        return $out;
    }
}
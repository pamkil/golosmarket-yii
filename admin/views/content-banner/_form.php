<?php

use common\models\content\Banner;
use common\models\content\Question;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var Banner $model
 */

?>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data' // important
    ]
]); ?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <?= Html::submitButton(
            '<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord ? 'Создать' : 'Сохранить'),
            ['class' => 'btn btn-success']
        ) ?>
        <?php if (!$model->isNewRecord) : ?>
            <?= Html::a(
                '<i class="fa fa-times"></i>&nbsp;' . 'Удалить',
                ['delete', 'id' => $model->id],
                [
                    'class' => ['btn', 'btn-danger'],
                    'title' => 'Удалить',
                    'data-confirm' => 'Вы уверены что хотите удалить?',
                    'data-method' => 'post'
                ]
            ) ?>
        <?php endif; ?>
    </div>
    <div class="box-body">
        <?= $form->field($model, 'active')->checkbox() ?>
        <?= $form->field($model, 'position') ?>
        <?= $form->field($model, 'file_id')->hiddenInput() ?>
        <?= $form->field($model, 'file')->fileInput() ?>
        <?php if (!empty($model->file)) : ?>
            <div class="row">
                <div class="col-md-3">
                    <img src="<?= $model->file->getUrl() ?>" class="img-responsive" alt="">
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

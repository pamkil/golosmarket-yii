<?php

namespace api\modules\v1\models\checks\auth;

use api\modules\v1\models\records\auth\Token;
use api\modules\v1\models\records\auth\User;
use yii\base\Model;

/**
 * @OA\Schema(required={"refresh_token"}, title="Форма обноыления токена пользователя")
 */
class RefreshForm extends Model
{
    /**
     * @OA\Property(
     *     type="string",
     *     description="refresh token of User",
     *     title="refresh_token",
     * )
     *
     * @var string
     */
    public $refresh_token;

    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['refresh_token'], 'required'],
        ];
    }

    /**
     * @return Token|null
     * @throws \yii\base\Exception
     */
    public function refresh()
    {
        if ($this->validate() && $user = $this->getUser()) {
            $token = Token::createToken($user->id);
            if ($token->save()) {
                Token::deleteAll(['refresh_token' => $this->refresh_token]);

                return $token;
            }
        }

        return null;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findIdentityByAccessToken($this->refresh_token, User::TOKEN_REFRESH);
        }

        return $this->_user;
    }
}
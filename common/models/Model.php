<?php

namespace common\models;

use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\BadRequestHttpException;

class Model extends ActiveRecord
{
    const SLUG_LENGTH = 155;

    public function behaviors()
    {
        date_default_timezone_set('UTC');
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at']
                ],
                'value' => date('Y-m-d H:i:s')
            ],
            'sluggableBehavior' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'ensureUnique' => true,
                'immutable' => true,
                'slugAttribute' => 'code',
            ],
        ];
    }

    /**
     * @param $params
     * @param bool $throwGen
     *
     * @return static
     * @throws BadRequestHttpException
     */
    public static function create($params, $throwGen = true)
    {
        $model = new static();
        if ((!$model->load($params, '') || !$model->save()) && $throwGen) {
            throw new BadRequestHttpException(
                'Failed to update the object for unknown reason. ' . implode(';', $model->getFirstErrors())
            );
        }

        return $model;
    }

    /**
     * @param string $identifier
     * @param bool|null $asArray
     *
     * @return static|array|ActiveRecord|null
     * @throws BadRequestHttpException
     */
    public static function findModel(string $identifier, bool $asArray = null)
    {
        $model = static::find()
            ->where(['id' => $identifier])
            ->asArray($asArray)
            ->one();

        if (!$model) {
            throw new BadRequestHttpException(
                'Failed to update the object for unknown reason. Not Found by class'
                . static::class . '. With id: ' . $identifier
            );
        }

        return $model;
    }

    /**
     * @param string $identifier
     * @param $params
     *
     * @return static
     * @throws BadRequestHttpException
     */
    public static function updateModel(string $identifier, $params)
    {
        $model = static::findModel($identifier);

        if (!$model->load($params, '') || !$model->save()) {
            throw new BadRequestHttpException(
                'Failed to update the object for unknown reason. ' . implode(';', $model->getFirstErrors())
            );
        }

        return $model;
    }

    public static function findOrCreate(array $findParams, array $createParams = null)
    {
        $model = static::find()->andWhere($findParams)->one();

        if (!$model) {
            $model = static::create($createParams ?? $findParams);
        }

        return $model;
    }

    /**
     * @param string $fieldName
     * @param string $indexField
     *
     * @return array[id => name]
     */
    public static function getAllList(string $fieldName = 'menu_title', string $indexField = 'code'): array
    {
        return static::find()
            ->select([$fieldName, $indexField])
            ->where(['active' => true])
            ->indexBy($indexField)
            // ->orderBy(['position' => SORT_ASC])
            ->column();
    }
}

<?php

namespace api\modules\v1\actions\review;

use api\modules\v1\models\checks\user\ReviewForm;
use api\modules\v1\models\records\user\Review;
use common\models\access\User;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;

/**
 * @OA\Post(
 *      path="/review",
 *      summary="Добавление в избранное",
 *      tags={"Chat"},
 *      @OA\Response(
 *          response="200",
 *          description="Объект токена",
 *          @OA\JsonContent(ref="#/components/schemas/Chat")
 *      ),
 *      @OA\Response(
 *          response="400",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="401",
 *          description="Unauthorized",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */

/**
 * Class Create
 * @package api\modules\v1\actions\review
 *
 */
class Create extends Action
{
    /**
     * @var string the scenario to be assigned to the new model before it is validated and saved.
     */
    public $scenario = Model::SCENARIO_DEFAULT;

    /**
     * @return Review
     * @throws ForbiddenHttpException
     */
    public function run($id)
    {
        $clientId = Yii::$app->user->id;
        if (!$clientId) {
            throw new ForbiddenHttpException();
        }
        $toId = $id;
        $isAnnouncer = (int)Yii::$app->user->identity->type === User::TYPE_ANNOUNCER;

        $postData = Yii::$app->request->post();

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $review = new ReviewForm();
            $review->load($postData, '');
            $reviewModel = new Review($review->toArray());

            if ($isAnnouncer) {
                $reviewModel->announcer_id = $clientId;
                $reviewModel->client_id = $toId;
                $reviewModel->type_writer = Review::TYPE_WRITE_ANNOUNCER;
            } else {
                $reviewModel->client_id = $clientId;
                $reviewModel->announcer_id = $toId;
                $reviewModel->type_writer = Review::TYPE_WRITE_CLIENT;
            }

            if ($review->validate() && $reviewModel->save()) {
                $transaction->commit();
                $reviewModel->refresh();

                return $reviewModel;
            }
            throw new BadRequestHttpException($review->errors);
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }
}

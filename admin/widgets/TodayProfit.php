<?php

namespace admin\widgets;

use Carbon\Carbon;
use common\models\user\Bill;
use yii\base\Widget;

class TodayProfit extends Widget
{
    public function run()
    {
        parent::run();

        /**
         * Сегодня заработано: 11
         * Заработано - считаем только чистый доход сервиса (30% от суммы всех счетов)
         */

        $dateCur = Carbon::today()->startOfDay()->toDateTimeString();
        $dateEndCur = Carbon::today()->endOfDay()->toDateTimeString();

        $sum = Bill::find()
            ->cache(60 * 3)
            ->where(
                [
                    'AND',
                    ['>=', 'updated_at', $dateCur],
                    ['<=', 'updated_at', $dateEndCur],
                    ['status' => Bill::STATUS_PAID_COMMISSION],
                ]
            )
            ->sum('`sum` * `percent` / 100');

        $data = [
            'title' => 'Сегодня заработано',
            'value' => \Yii::$app->formatter->asCurrency(round($sum ?? 0, 2)),
        ];
        echo $this->render('allWidgets', $data);
    }
}

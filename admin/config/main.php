<?php

$params = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$urlManager = require(__DIR__ . '/urlManager.php');

$config = [
    'id' => 'admin.golosmarket',
    'name' => 'golosmarket admin',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'admin\controllers',
    'defaultRoute' => 'default/index',
    'aliases' => [
        '@admin' => dirname(dirname(__DIR__)) . '/admin',
    ],
    'components' => [
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => common\models\access\User::class
        ],
        'request' => [
            'class' => \yii\web\Request::class,
            'enableCsrfCookie' => false,
//            'csrfParam' => '_csrf-frontend',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
//            'cookieValidationKey' => 'O7Rk79g5ETH4Z-2GRaOrQ-PsHDE6RU8x'
        ],
        'errorHandler' => [
            'errorAction' => 'default/error'
        ],
        'urlManager' => $urlManager,
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'datetimeFormat' => 'dd.MM.yyyy HH:mm:ss',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'baseUrl' => '/admin/assets/'
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/views'
                ],
            ],
        ],
    ],
    'params' => $params,
    'modules' => [
        'gridview' => [
            'class' => 'kartik\grid\Module',
        ],
        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module'
        ],
    ],
];

if (!YII_ENV_TEST && YII_DEBUG) {
    // configuration adjustments for 'dev' environment
//    $config['bootstrap'][] = 'debug';
//    $config['modules']['debug'] = [
//        'class' => 'yii\debug\Module',
//        'traceLine' => '<a href="phpstorm://open?file={file}&line={line}">{file}:{line}</a>',
//    ];
//    $config['bootstrap'][] = 'gii';
//    $config['modules']['gii'] = [
//        'class' => 'yii\gii\Module',
//        'allowedIPs' => ['127.0.0.1', '::1'],
//        'generators' => [
//            'crud' => [
//                'class' => 'yii\gii\generators\crud\Generator', // generator class
//                'templates' => [
//                    'myCrud' => '@console/templates/gii/crud/default',
//                ]
//            ]
//        ],
//    ];
}

return $config;

<?php

namespace admin\models\setting;

use common\models\site\Redirect;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class RedirectSearch extends Model
{
    public $from;
    public $active;
    public $code;
    public $to;

    public function rules()
    {
        return [
            [
                ['from', 'to',],
                'string'
            ],
            [
                ['active',],
                'boolean'
            ],
            [
                ['code',],
                'number'
            ],
        ];
    }

    public function search($params)
    {
        $query = Redirect::find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'from', 'to', 'code', 'active'
                    ]
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $dataProvider->sort->defaultOrder = ['from' => SORT_ASC,];

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'AND',
                ['like', 'from', $this->from],
                ['like', 'to', $this->to],
                ['code' => $this->code],
                ['active' => $this->active],
            ]
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}

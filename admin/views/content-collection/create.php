<?php

/* @var $this yii\web\View */
/* @var $model common\models\content\Collection */

?>
<div class="collection-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

import React, { useEffect, useState } from 'react';

import ItemAvatar from "./ItemAvatar";
import PlayElement from "./PlayElement";
import ItemReviews from "./ItemReviews";
import ItemPrice from './ItemPrice';
import ItemRating from './ItemRating';
import ItemButtons from './ItemButtons';
import ItemInfo from '../../Announcer/ItemList/ItemInfo';

const ItemList = props => {
  const [announcerFullInfo, setAnnouncerFullInfo] = useState({});
  const [isVisibleItemInfo, setIsVisibleItemInfo] = useState(false)
  let {id} = props
  

  useEffect(() => {
    fetch(`https://golosmarket.ru/api/v1/user/full?id=${id}&expand=recommended}`)
      .then((response) => response.json())
      .then((json) => {
        setAnnouncerFullInfo(json);
        setIsVisibleItemInfo(prev => !prev)
      });
  }, []); 
    return (
    <div className="item">
      {isVisibleItemInfo && <ItemInfo announcerFullInfo={announcerFullInfo}/>}
      <div className="item-main-part">
        <div className="row">
          <ItemAvatar {...props}/>
          <ItemPrice {...props} />
        </div>
        <div className="row">
          <div className="flex item__info">
            <ItemRating {...props} />
            <ItemReviews {...props}/>
          </div>
          <PlayElement
            copyToClipboard={props.copyHandler}
            {...props}
          />
  
          <div className="flex item__controls">
            <ItemButtons {...props} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ItemList;

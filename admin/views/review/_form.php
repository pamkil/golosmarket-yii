<?php

use admin\models\user\Review;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var Review $model
 */

$form = ActiveForm::begin(
    [
        'options' => [
            'enctype' => 'multipart/form-data' // important
        ]
    ]
) ?>

<div class="box box-primary">
    <div class="box-header with-border" style="text-align: right">
        <?= Html::submitButton(
            '<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord ? 'Создать' : 'Сохранить'),
            ['class' => 'btn btn-success']
        ) ?>
        <?php
        if (!$model->isNewRecord) {
            echo Html::a(
                '<i class="fa fa-times"></i>&nbsp;' . 'Удалить',
                ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'title' => 'Удалить',
                    'data-confirm' => 'Вы уверены что хотите удалить?',
                    'data-method' => 'post'
                ]
            );
        } ?>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'is_public')->checkbox(); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'rating')->dropDownList(
                    [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5]
                ); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'type_writer')->dropDownList(Review::getTypes()); ?>
            </div>
        </div>


        <?= $form->field($model, 'text')
            ->widget(TinyMce::class, Yii::$app->params['tinyMceOptions'] ?? ''); ?>

        <div class="row">
            <div class="col-md-6">
                <?php
                if ($model->announcer) {
                    echo "<h3>Диктор</h3>";
                    echo DetailView::widget(
                        [
                            'model' => $model->announcer,
                            'attributes' => [
                                'id',
                                'firstname',
                                'lastname',
                                'email',
                                'phone',
                            ],
                        ]
                    );
                } ?>
            </div>
            <div class="col-md-6">
                <?php
                if ($model->client) {
                    echo "<h3>Клиент</h3>";
                    echo DetailView::widget(
                        [
                            'model' => $model->client,
                            'attributes' => [
                                'id',
                                'firstname',
                                'lastname',
                                'email',
                                'phone',
                            ],
                        ]
                    );
                } ?>
            </div>
        </div>
    </div>
</div>

<?php
ActiveForm::end(); ?>

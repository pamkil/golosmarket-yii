<?php

use yii\db\Migration;

/**
 * Class m201208_064108_create_user_send_notify
 */
class m201208_064108_create_user_send_notify extends Migration
{
    private $sendNotify = '{{%user_send_notify}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            $this->sendNotify,
            [
                'id' => $this->primaryKey(),
                'other_id' => $this->integer()->null(),
                'type' => $this->smallInteger()->notNull(),
                'status' => $this->smallInteger()->notNull()->defaultValue(0),
                'date' => $this->dateTime()->notNull(),
                'text' => $this->text()->null(),
                'created_at' => $this->dateTime()->notNull(),
                'updated_at' => $this->dateTime()->notNull(),
                'INDEX `IX-user_send_notify-other_id` (other_id)',
                'INDEX `IX-user_send_notify-type` (type)',
                'INDEX `IX-user_send_notify-status` (status)',
            ],
            $tableOptions
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->sendNotify);
    }
}

import React from "react";

const ItemInfo = ({ announcerFullInfo }) => {
  if(!announcerFullInfo) {
    return <></>
  }
  return (
    <div className="item-info-container">
      <p className="item-info-text">
        {announcerFullInfo.info === ""
          ? "Послушайте демо, напишите в чат"
          : announcerFullInfo.info}
      </p>
    </div>
  );
};

export default ItemInfo;

<?php

namespace common\models\user;

use common\models\bill\PayedSearch;
use Carbon\Carbon;
use common\models\access\User;
use common\models\Model;
use common\models\site\Setting;
use common\service\Notification as Notify;
use Yii;

/**
 * Class Bill
 * @package common\models\user
 * @property int $id
 * @property int $announcer_id
 * @property int $client_id
 * @property int $status
 * @property float $sum
 * @property float $percent
 * @property string $date
 * @property bool $is_send
 * @property bool $is_canceled
 * @property bool $is_payed
 * @property string $ya_notification_type
 * @property string $ya_operation_id
 * @property float $ya_amount
 * @property float $ya_withdraw_amount
 * @property string $ya_currency
 * @property string $ya_datetime
 * @property bool $ya_codepro
 * @property string $ya_label
 * @property string $ya_sha1_hash
 * @property bool $ya_test_notification
 * @property bool $ya_unaccepted
 * @property bool $ya_success
 * @property string $created_at
 * @property string $updated_at
 * @property string $card_number
 *
 * @property User $announcer
 * @property User $client
 */
class Bill extends Model
{
    // https://yandex.ru/dev/money/doc/dg/reference/notification-p2p-incoming-docpage/
    public const STATUS_NEW = 1; //Новый счет
    public const STATUS_PAID_CLIENT = 2; //Подтверждено клиентом
    /** @deprecated */
    public const STATUS_PAID_ANNOUNCER = 3; //Подтверждено диктором
    public const STATUS_PAID_COMMISSION = 6; //Комиссия оплачена (к сожалению сдвинуть нумерацию не получиться из-за совместимости со старыми счетами)
    /** @deprecated */
    public const STATUS_CANCEL_ANNOUNCER = 4; //Отменен диктором
    public const STATUS_CANCEL_CLIENT = 5; //Отменен клиентом
    /** @deprecated */
    public const STATUS_NOT_PAID = 7; //Счёт закрыт и не оплачен клиентом

    public $isImport = false;

    public static function tableName()
    {
        return '{{%user_bill}}';
    }

    public function rules()
    {
        return [
            [
                ['announcer_id', 'client_id', 'sum'],
                'required',
            ],
            [
                ['sum', 'percent', 'status', 'card_number'],
                'number',
            ],
            [
                ['is_send', 'is_payed'],
                'boolean',
            ],
            [
                ['status'],
                'default',
                'value' => Bill::STATUS_NEW,
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'announcer_id' => 'Диктор',
            'client_id' => 'Клиент',
            'status' => 'Статус',
            'sum' => 'Сумма',
            'card_number' => 'Номер карты',
            'percent' => 'Процент сайту',
            'date' => 'Дата формирования',
            'is_send' => 'Оплачена комиссия',
            'is_canceled' => 'Отменен одной из сторон',
            'is_payed' => 'Оплачен диктору',
            'ya_notification_type' => 'Тип перевода',
            'ya_operation_id' => 'Идентификатор операции в истории счета получателя.',
            'ya_amount' => 'Сумма операции',
            'ya_withdraw_amount' => 'Сумма, которая списана со счета отправителя',
            'ya_currency' => 'Код валюты счета пользователя. Всегда 643',
            'ya_datetime' => 'Дата и время совершения перевода',
            'ya_codepro' => 'Для переводов из кошелька — перевод защищен кодом протекции',
            'ya_label' => 'Метка платежа',
            'ya_sha1_hash' => 'SHA-1 hash параметров уведомления',
            'ya_test_notification' => 'Флаг означает, что уведомление тестовое',
            'ya_unaccepted' => 'Флаг означает, что пользователь не получил перевод',
            'ya_success' => 'Флаг означает, что перевод удачный',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);
        if ($this->isImport) {
            unset($behaviors['timestamp']);
        }

        return $behaviors;
    }

    public function getAnnouncer()
    {
        return $this->hasOne(User::class, ['id' => 'announcer_id'])->where([]);
    }

    public function getClient()
    {
        return $this->hasOne(User::class, ['id' => 'client_id'])->where([]);
    }

    public function beforeSave($insert)
    {
        if ($this->isImport) {
            return parent::beforeSave($insert);
        }

        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->status = self::STATUS_NEW;
            } else {
                $oldStatus = $this->getOldAttribute('status');
                if ($oldStatus === $this->status) {
                    return true;
                }

                $successStatuses = [
                    self::STATUS_PAID_CLIENT,
                    self::STATUS_PAID_ANNOUNCER,
                    self::STATUS_CANCEL_ANNOUNCER,
                    self::STATUS_CANCEL_CLIENT
                ];

                if ($oldStatus === self::STATUS_NEW && !in_array($this->status, $successStatuses)) {
                    $this->addError('status', 'Нельзя оплатить комиссию не оплаченного счёта');
                    return false;
                }

                /*if (
                    $oldStatus === self::STATUS_PAID_CLIENT &&
                    !in_array($this->status, [self::STATUS_PAID_ANNOUNCER, self::STATUS_NOT_PAID])
                ) {
                    $this->addError('status', 'Необходимо что бы диктор подтвердил оплату');
                    return false;
                }*/

                if (
                    $this->status === self::STATUS_PAID_ANNOUNCER &&
                    $this->is_payed !== 1
                ) {
                    $this->addError('is_payed', 'Статус оплаты при подтверждении должен смениться');
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    public function setCancelStatus(bool $isAnnouncer): bool
    {
        if ($this->status === self::STATUS_NEW) {
            $this->status = $isAnnouncer ? self::STATUS_CANCEL_ANNOUNCER : self::STATUS_CANCEL_CLIENT;

            return true;
        }

        $this->addError('status', 'Нельзя отменить оплаченный счет');

        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            Notify::newBill($this);
        } else {
            if (empty($changedAttributes['status'])) {
                return;
            }

            if (
                in_array($changedAttributes['status'], [self::STATUS_NEW/*, self::STATUS_PAID_CLIENT*/]) &&
                $this->status === self::STATUS_PAID_CLIENT
            ) {
                Notify::paidBill($this);
                Notify::needReview($this);

                $template = file_get_contents(Yii::$app->params['commission-mail-path']);

                $announcerName = $this->announcer->firstname.' '.$this->announcer->lastname;
                $template = str_ireplace('{NAME}', $announcerName, $template);

                $sum = 0;
                $percent = (int)(Setting::getSetting('percent') ?? 30);
                $searchModel = new PayedSearch();
                $searchModel->announcer_id = $this->announcer_id;
                $dataProvider = $searchModel->search();
                foreach ($dataProvider->models[0]->bills as $bill) {
                    $sum += ($bill->sum * $percent) / 100;
                }
                $commission = Yii::$app->formatter->asCurrency($sum);
                $template = str_ireplace('{COMMISSION}', $commission, $template);

                \Yii::$app->mailer->compose(
                    ['html' => 'commissionMail-html', 'text' => 'commissionMail-text'],
                    ['template' => $template]
                )
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                    ->setTo($this->announcer->email)
                    ->setSubject('Оплата заказчиком подтверждена.')
                    ->send();

                if (self::isSecondPaidBill($this)) {
                    Notify::paidSecondBill($this);
                }
            }
        }
    }

    /**
     * @param Bill|array $bill with client and announcer
     *
     * @return string
     */
    public static function billToText($bill)
    {
        $billDate = Carbon::parse($bill['date'])->format('d.m.Y');

        return "счет № {$bill['id']} от $billDate на сумму {$bill['sum']} руб";
    }

    public function __toString()
    {
        return self::billToText($this);
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_PAID_CLIENT => 'Подтверждено клиентом',
            self::STATUS_PAID_ANNOUNCER => 'Подтверждено диктором',
            self::STATUS_CANCEL_ANNOUNCER => 'Отменен диктором',
            self::STATUS_CANCEL_CLIENT => 'Отменен клиетом',
            self::STATUS_PAID_COMMISSION => 'Комиссия оплачена',
            self::STATUS_NOT_PAID => 'Не оплачено',
        ];
    }

    public static function getForClientStatuses()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_PAID_CLIENT => 'Оплачен',
            self::STATUS_PAID_ANNOUNCER => 'Оплачен',
            self::STATUS_CANCEL_ANNOUNCER => 'Отменен диктором',
            self::STATUS_CANCEL_CLIENT => 'Отменен клиентом',
            self::STATUS_PAID_COMMISSION => 'Комиссия оплачена',
            self::STATUS_NOT_PAID => 'Не оплачено',
        ];
    }

    /**
     * Если есть второй оплаченный, но нет по нему уведомления
     * @param Bill $firstBill
     * @return bool
     */
    public static function isSecondPaidBill(Bill $firstBill): bool
    {
        $bill = Bill::find()
            ->select('id')
            ->where([
                'AND',
                ['status' => [Bill::STATUS_PAID_CLIENT, Bill::STATUS_PAID_ANNOUNCER]],
                ['client_id' => $firstBill['client_id']],
                ['NOT IN', 'id', $firstBill['id']],
            ])
            ->asArray()
            ->one();

        if (!$bill) {
            return false;
        }

        $notify = Notification::find()
            ->select('id')
            ->where([
                'type' => Notification::TYPE_REVIEW_SERVICE_CLIENT,
                'user_id' => $firstBill['client_id'],
            ])
            ->asArray()
            ->one();

        return empty($notify);
    }
}

<?php
namespace frontend\models;

use Carbon\Carbon;
use common\models\access\User;
use common\service\Mail;
use Yii;
use yii\base\Model;

/**
 * Password reset request form
 * @deprecated
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    public function formName()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'exist',
                'targetClass' => User::class,
                'filter' => [
                    'AND',
                    ['status' => User::STATUS_ACTIVE],
                    [
                        'OR',
                        ['<', 'blocked_at', Carbon::now()->toDateTimeString()],
                        ['blocked_at' => null]
                    ]
                ],
                'message' => 'There is no user with this email address.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findByEmail($this->email);

        if (!$user) {
            return false;
        }
        
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        return Mail::sendPasswordReset(
            $user->email,
            $user->password_reset_token,
            $user->firstname
        );
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: pamkil
 * Date: 12.02.19
 * Time: 1:02
 */

namespace api\modules\v1\models\records\auth;

use api\modules\v1\models\helpers\DateHelper;

/**
 * @OA\Schema(required={"name"}, title="Токен пользователя")
 */
class Token extends \common\models\access\Token
{
    /**
     * @OA\Property(property="token", type="string", description="токен")
     * @OA\Property(property="expired", type="string", description="время жизни токен")
     * @OA\Property(property="refresh_token", type="string", description="токен обновления")
     * @OA\Property(property="refresh_expired_at", type="string", description="время жизни токена обновления")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'token' => 'token',
            'expired' => DateHelper::parseRfc($this, 'expired_at'),
            'refresh_token' => 'refresh_token',
            'refresh_expired_at' => DateHelper::parseRfc($this, 'refresh_expired_at'),
        ];
    }
}
<?php

namespace api\modules\v1\actions\bill;

use api\modules\v1\models\records\user\Bill;
use Carbon\Carbon;
use common\models\access\User;
use common\models\site\Setting;
use Yii;
use yii\rest\Action;
use yii\web\BadRequestHttpException;

/**
 * @OA\Post(
 *      path="/chat/{id}/bill",
 *      summary="Создать новый счет клиенту, ИД клиента",
 *      tags={"Chat"},
 *      @OA\RequestBody(
 *          description="Счет",
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Bill")
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="Объект токена",
 *          @OA\JsonContent(ref="#/components/schemas/Chat")
 *      ),
 *      @OA\Response(
 *          response="400",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="401",
 *          description="Unauthorized",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */

/**
 * Class Create
 * @throws BadRequestHttpException
 * @package api\modules\v1\actions\chat
 *
 */
class Create extends Action
{
    public function run($id)
    {
        $postData = Yii::$app->request->post();
        if (empty($id)) {
            throw new BadRequestHttpException("Нет данных кому написать");
        }

        $typeReceiver = User::getType((int)$id);

        if (!$typeReceiver) {
            throw new BadRequestHttpException("Не найден пользователь которому вы пишете");
        }

        if ((int)Yii::$app->user->identity->type === $typeReceiver) {
            throw new BadRequestHttpException("Не можете написать этому пользователю");
        }

        if (Yii::$app->user->identity->type === User::TYPE_ANNOUNCER) {
            $postData['announcer_id'] = Yii::$app->user->id;
            $postData['client_id'] = $id;
        } else {
            $postData['announcer_id'] = $id;
            $postData['client_id'] = Yii::$app->user->id;
        }

        $transaction = Yii::$app->db->beginTransaction();
        $postData['percent'] = (int)(Setting::getSetting('percent') ?? 30);

        try {
            $bill = new Bill();
            $bill->date = Carbon::now()->toDateTimeString();
            $bill->load($postData, '');
            if ($bill->save()) {
                $transaction->commit();
                $bill->refresh();

                return $bill;
            }
            throw new BadRequestHttpException($bill->errors);
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }
}

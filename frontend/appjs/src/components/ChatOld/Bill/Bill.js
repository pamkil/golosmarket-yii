import React, { useMemo } from "react";

const PayText = ({ sum, onClick, url, cancelBill }) => (
  <div
    onClick={onClick}
    style={{ display: "flex", justifyContent: "space-between", width: "100%" }}
  >
    <div style={{ alignSelf: "center" }}>
       <b>{Number.isInteger(sum) ? Math.round(sum) : sum}</b> руб.
    </div>
    &nbsp;
    <div>
      <iframe
        title="bill"
        frameBorder="0"
        scrolling="no"
        width="127"
        height="25"
        src={url}
      />
    </div>
    <div style={{ marginLeft: "auto" }}>
      <button className="red" onClick={cancelBill}>
        Отказаться
      </button>
    </div>
  </div>
);

const Bill = ({ bill, cancelBill }) => {
  const url = useMemo(() => {
    if (!bill) {
      return null;
    }

    const { sum, num, account } = bill;
    const paramsUrl = {
      account: `${account}`,
      targets: `${num}`,
      label: `${num}`,
      quickpay: "small",
      "any-card-payment-type": "on",
      "button-text": "11",
      "button-size": "s",
      "button-color": "black",
      "default-sum": `${sum}`,
      successURL: `${window.location.href}`,
      // mail:"on",
    };

    const params = new URLSearchParams(paramsUrl);

    return `https://money.yandex.ru/embed/small.xml?${params.toString()}`;
  }, [bill]);

  return (
    <div className="payform">
      {bill && url ? (
        <PayText sum={bill.sum} url={url} cancelBill={cancelBill} />
      ) : (
        "После счета, появится форма оплаты"
      )}
    </div>
  );
};

export default Bill;

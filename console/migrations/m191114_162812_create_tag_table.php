<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_announcer_tags}}`.
 */
class m191114_162812_create_tag_table extends Migration
{
    private $tags = '{{%user_announcer_tags}}';
    private $announcer2tags = '{{%user_announcer_2_tags}}';
    private $announcer = '{{%user_announcer}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tags, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'active' => $this->boolean()->notNull()->defaultValue(true),
        ], $tableOptions);

        $this->createTable($this->announcer2tags, [
            'announcer_id' => $this->integer(),
            'tag_id' => $this->integer(),
            'PRIMARY KEY (announcer_id, tag_id)'
        ], $tableOptions);

        $this->createIndex(
            'IX-user_announcer_2_tags-announcer_id',
            $this->announcer2tags,
            'announcer_id'
        );
        $this->addForeignKey(
            'FK-user_announcer_2_tags-announcer_id',
            $this->announcer2tags,
            'announcer_id',
            $this->announcer,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('IX-user_announcer_2_tags-tag_id', $this->announcer2tags, 'tag_id');
        $this->addForeignKey(
            'FK-user_announcer_2_tags-tag_id',
            $this->announcer2tags,
            'tag_id',
            $this->tags,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->announcer2tags);
        $this->dropTable($this->tags);
    }
}

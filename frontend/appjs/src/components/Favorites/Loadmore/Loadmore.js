import React, {useMemo} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {setPage} from "../../../data/redux/announcer";
import classes from "classnames";

const Item = ({pageNumber, isActive, onClick, canClick, addClasses = ''}) => (
  <span
    className={classes({'active': isActive}, addClasses)}
    onClick={() => canClick ? onClick(pageNumber) : null}
  >
        {pageNumber}
    </span>
);

const Loadmore = () => {
  const dispatch = useDispatch();
  const {
    page: activePage = 1,
    quantityPages = 0
  } = useSelector(state => ({...state.announcer.pagination}));

  return (
    <div className="catalog__more">
      <button onClick={() => {
        //return dispatch(setPage(activePage + 1));
      }} className="green">Показать еще исполнителей
      </button>
    </div>
  )
};

export default Loadmore;

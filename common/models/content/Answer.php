<?php

namespace common\models\content;

use common\models\Model;

/**
 * Class Answer
 * @package common\models\content
 *
 * @property int $id
 * @property int $question_id
 * @property string $active
 * @property int $position
 * @property string $text
 * @property Question $question
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 */
class Answer extends Model
{
    public static function tableName()
    {
        return '{{%content_question_answer}}';
    }

    public function rules()
    {
        return [
            [
                ['active'],
                'boolean'
            ],
            [
                ['position', 'question_id'],
                'integer'
            ],
            [
                ['text'],
                'string'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Вопрос',
            'active' => 'Активность',
            'position' => 'Позиция',
            'text' => 'Содержимое страницы',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }

    public function getQuestion()
    {
        return $this->hasOne(Question::class, ['id' => 'question_id']);
    }
}
<?php

namespace admin\models\announcer;

use common\models\announcer\Age;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class AgeSearch extends Model
{
    public $name;
    public $code;
    public $position;
    public $active;
    public $menu_title;

    public function rules()
    {
        return [
            [
                ['name', 'code', 'menu_title'],
                'string'
            ],
            [
                ['position'],
                'integer'
            ],
            [
                ['active',],
                'boolean'
            ],
        ];
    }

    public function search($params)
    {
        $query = Age::find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'active',
                        'code',
                        'position',
                        'name',
                        'menu_title',
                    ]
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $dataProvider->sort->defaultOrder = ['name' => SORT_ASC,];

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'AND',
                ['like', 'name', $this->name],
                ['like', 'code', $this->code],
                ['like', 'menu_title', $this->menu_title],
                ['position' => $this->position],
                ['active' => $this->active],
            ]
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
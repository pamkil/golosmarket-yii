<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class RestController extends ActiveController
{
    public $modelClass = '';
    public $beforeAction = true;
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        $behaviors = ArrayHelper::merge(parent::behaviors(), [
            'authenticator' => [
                'class' => CompositeAuth::class,
                'except' => ['options'],
                'authMethods' => [
                    HttpBearerAuth::class,
                    QueryParamAuth::class // GET access-token={token}
                ]
            ],
            'corsFilter' => [
                'class' => Cors::class,
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => ['*'],
                ]
            ]
        ]);

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::class,
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ]
        ];

        return $behaviors;
    }

    /**
     * @param string $action
     * @param null $model
     * @param array $params
     *
     * @throws ForbiddenHttpException
     */
    public function checkAccess($action, $model = null, $params = [])
    {
//        parent::checkAccess($action, $model, $params);
        return true;
        if ($action == 'index' || $action == 'view') {
            if (!Yii::$app->user->can('viewApi')) {
                throw new ForbiddenHttpException(Yii::t('api', 'Access denied'));
            }
        } else {
            if (!Yii::$app->user->can('updateApi')) {
                throw new ForbiddenHttpException(Yii::t('api', 'Access denied'));
            }
        }
    }
}

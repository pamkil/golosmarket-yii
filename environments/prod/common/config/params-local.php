<?php
return [
    'sendRealEmail' => false,
    'ya_notification_secret' => '01234567890ABCDEF01234567890',
    'emailFrom' => 'test@test.test',
    'emailTo' => 'test@test.test',
    'supportEmail' => 'test@test.test',

    // Ключи доступа до отправки смс через EPochta
    'epochta.login' => '',
    'epochta.password' => '',
];

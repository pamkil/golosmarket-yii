<?php

namespace common\service;

use Exception;
use Yii;
use yii\helpers\ArrayHelper;

class Epochta
{
    private string $password = '';
    private string $login = '';

    public static function sendSms(string $message, string $phoneNumber)
    {
        $instance = new self();

        if (!$instance) {
            return false;
        }

        return $instance->send($message, $phoneNumber);
    }

    public function __construct()
    {
        $params = Yii::$app->params;
        if (empty($params['epochta.login']) || empty($params['epochta.password'])) {
            Yii::error('Not set Login and Password for Epochta SMS');
            return;
        }
        $this->login = $params['epochta.login'];
        $this->password = $params['epochta.password'];
    }

    public function checkBeforeSend(): bool
    {
        if (empty($this->login) || empty($this->password)) {
            Yii::error('Not set Login and Password for Epochta SMS');
            return false;
        }

        return true;
    }

    public function send(string $message, string $phoneNumber)
    {
        if (!$this->checkBeforeSend()) {
            return false;
        }
        $login = $this->login;
        $pwd = $this->password;
        $sender = 'golosmarket';

        $phones = [$phoneNumber];

        $src = "<?xml version='1.0' encoding='UTF-8'?>
        <SMS>
            <operations>
                <operation>SEND</operation>
            </operations>
            <authentification>
                <username>{$login}</username>
                <password>{$pwd}</password>
            </authentification>
            <message>
                <sender>{$sender}</sender>
                <text>{$message}</text>
            </message>
            <numbers>";

        foreach ($phones as $phone) {
            $src .= "<number>{$phone}</number>";
        }

        $src .= "</numbers>
        </SMS>";

        if (!($curl = curl_init())) {
            return false;
        }

        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_URL, 'http://api.atompark.com/members/sms/xml.php');
        curl_setopt($curl, CURLOPT_POSTFIELDS, ['XML' => $src]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_NOBODY, 0);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 15);
        curl_setopt($curl, CURLOPT_TIMEOUT, 100);

        try {
            if (($res = curl_exec($curl)) === false) {
                Yii::warning('SMS send Epochta error: ' . curl_error($curl));
            }

            if ($res !== false) {
                $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
                $body = substr($res, $header_size);
                $xml = simplexml_load_string($body);
                $result = ArrayHelper::toArray((array)$xml);
                if (isset($result['status']) && $result['status'] >= 0) {
                    curl_close($curl);

                    return $result['status'] ?? true;
                } else {
                    Yii::warning(['SMS send Epochta error: '] + $result, 'smsGateway');
                }
            }
        } catch (Exception $exception) {
            Yii::error($exception, 'smsGateway');
        }

        try {
            curl_close($curl);
        } catch (\Throwable $exception) {
            Yii::error($exception, 'smsGateway');
        }

        return false;
    }
}
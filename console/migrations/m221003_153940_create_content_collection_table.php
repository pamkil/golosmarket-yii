<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%content_collection}}`.
 */
class m221003_153940_create_content_collection_table extends Migration
{
    private string $tableName = '{{%content_collection}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'image_id' => $this->integer()->null(),
            'description' => $this->text()->null(),

            'active' => $this->boolean()->notNull()->defaultValue(false),
            'position' => $this->integer()->null(),

            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP')
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

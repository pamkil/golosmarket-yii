<?php

namespace frontend\models;

use common\models\access\User;
use yii\base\InvalidArgumentException;
use yii\base\Model;

class RestoreProfileForm extends Model
{
    /**
     * @var string
     */
    public $token;

    /**
     * @var User
     */
    private $_user;


    /**
     * Creates a form model with given token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws InvalidArgumentException if token is empty or not valid
     */
    public function __construct($token, array $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidArgumentException('Restore profile token cannot be blank.');
        }
        $this->_user = User::findDeleted()->andWhere([
            'status' => User::STATUS_DELETED,
            'verification_token' => $token
        ])->one();
        if (!$this->_user) {
            throw new InvalidArgumentException('Wrong restore profile token.');
        }
        parent::__construct($config);
    }

    /**
     * Verify email
     *
     * @return User|null the saved model or null if saving fails
     */
    public function restoreProfile()
    {
        $user = $this->_user;
        $user->status = User::STATUS_ACTIVE;

        return $user->save(false) ? $user : null;
    }
}

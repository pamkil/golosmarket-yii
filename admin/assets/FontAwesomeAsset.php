<?php

namespace admin\assets;

use kartik\icons\FontAwesomeAsset as FontAwesomeAssetLib;

class FontAwesomeAsset extends FontAwesomeAssetLib
{
    /**
     * @inheritdoc
     */
    public $publishOptions = [
        'only' => ['fonts/*', 'css/*']
    ];
}
import React from 'react';

const SelectorItem = ({ isActive = false, id, title = '', onClick }) => (
    <li className={ isActive ? 'active' : '' } onClick={ () => onClick(id) }>
        <button >{ title }</button>
    </li>
);

export default SelectorItem;
<?php

namespace api\modules\v1\controllers;

use api\modules\v1\actions\feedback\Create;
use api\modules\v1\models\records\site\Feedback;
use yii\helpers\ArrayHelper;
use yii\rest\OptionsAction;

/**
 * @SWG\Tag(
 *   name="Feedback",
 *   description="Feedback"
 * )
 */
class FeedbackController extends RestController
{
    public $modelClass = Feedback::class;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'authenticator' => [
                    'optional' => ['create'],
                    'except' => ['options'],
                ],
            ]
        );
    }

    public function checkAccess($action, $model = null, $params = [])
    {
    }

    public function actions()
    {
        return [
            'create' => [
                'class' => Create::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
            'options' => [
                'class' => OptionsAction::class,
            ],
        ];
    }
}
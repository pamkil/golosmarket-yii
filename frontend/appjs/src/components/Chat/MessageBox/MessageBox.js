import React, { PureComponent } from "react";
import "./MessageBox.css";
import {
  Avatar,
  SystemMessage,
  LocationMessage,
  SpotifyMessage,
  MeetingMessage,
} from "react-chat-elements";
import PhotoMessage from "./PhotoMessage/PhotoMessage";
import AudioMessage from "./AudioMessage/AudioMessage";
import FileMessage from "./FileMessage/FileMessage";
import { format } from "timeago.js";
import classNames from "classnames";
import DropDown from "../../Common/DropDown/DropDown";
import ReplyMessage from "./ReplyMessage/ReplyMessage";
import copy from "copy-to-clipboard";

import { ReactComponent as IconCheck } from "../../../images/checkmark.svg";
import FaForward from "react-icons/lib/fa/mail-forward";
import FaElipsisV from "react-icons/lib/fa/ellipsis-v";
import IoDoneAll from "react-icons/lib/io/android-done-all";
import MdIosTime from "react-icons/lib/md/access-time";
import MdCheck from "react-icons/lib/md/check";
import MdReply from "react-icons/lib/md/reply";
import { useDispatch, useSelector } from "react-redux";
import { requestMessages } from "../../../data/redux";

export class MessageBox extends PureComponent {
  constructor(props) {
    super(props);
    this.messageRef = React.createRef();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.focus !== this.props.focus && nextProps.focus === true) {
      if (this.messageRef) {
        this.messageRef.current.scrollIntoView({
          block: "center",
          behavior: "smooth",
        });

        this.props.onMessageFocused(nextProps);
      }
    }
  }
  formatTime(dateString) {
    const date = new Date(dateString);
    const hours = date.getHours().toString().padStart(2, '0');
    const minutes = date.getMinutes().toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const year = date.getFullYear().toString().slice(-2);
    return `${hours}:${minutes} ${day}/${month}/${year}`;
  }

  copyHandler = (fileUrl) => {
    copy(document.location.origin + fileUrl);
  };
  render() {
    const positionCls = classNames("rce-mbox", {
      "rce-mbox-right": this.props.position === "right",
    });
    const dateText =
      this.props.date &&
      !isNaN(this.props.date) &&
      (this.props.dateString || this.formatTime(this.props.date));

    return (
      <div
        ref={this.messageRef}
        data-id={this.props.id}
        className={classNames("rce-container-mbox", this.props.className)}
        onClick={this.props.onClick}
      >
        {this.props.renderAddCmp instanceof Function &&
          this.props.renderAddCmp()}
        {this.props.type === "system" ? (
          <SystemMessage text={this.props.text} />
        ) : (
          <div
            className={classNames(
              positionCls,
              // {'rce-mbox--clear-padding': thatAbsoluteTime},
              { "rce-mbox--clear-notch": !this.props.notch },
              { "message-focus": this.props.focus }
            )}
          >
            <div
              className="rce-mbox-body"
              onContextMenu={this.props.onContextMenu}
            >
              {this.props.forwarded === true && (
                <div
                  className={classNames(
                    "rce-mbox-forward",
                    {
                      "rce-mbox-forward-right": this.props.position === "left",
                    },
                    { "rce-mbox-forward-left": this.props.position === "right" }
                  )}
                  onClick={this.props.onForwardClick}
                >
                  <FaForward />
                </div>
              )}

              {this.props.replyButton === true && (
                <div
                  title="Ответить"
                  className={
                    this.props.forwarded !== true
                      ? classNames(
                          "rce-mbox-forward",
                          {
                            "rce-mbox-forward-right":
                              this.props.position === "left",
                          },
                          {
                            "rce-mbox-forward-left":
                              this.props.position === "right",
                          }
                        )
                      : classNames(
                          "rce-mbox-forward",
                          {
                            "rce-mbox-reply-btn-right":
                              this.props.position === "left",
                          },
                          {
                            "rce-mbox-reply-btn-left":
                              this.props.position === "right",
                          }
                        )
                  }
                  onClick={this.props.onReplyClick}
                >
                  <MdReply />
                </div>
              )}

              {(this.props.title || this.props.avatar) && (
                <div
                  style={
                    this.props.titleColor && { color: this.props.titleColor }
                  }
                  onClick={this.props.onTitleClick}
                  className={classNames("rce-mbox-title", {
                    "rce-mbox-title--clear": this.props.type === "text",
                  })}
                >
                  {this.props.avatar && (
                    <Avatar
                      letterItem={this.props.letterItem}
                      src={this.props.avatar}
                    />
                  )}
                  {this.props.title && <span>{this.props.title}</span>}
                </div>
              )}

              {this.props.reply && this.props.reply.id && (
                <ReplyMessage
                  message={this.props.reply}
                  onClick={this.props.onReplyMessageClick}
                  withBackground
                />
              )}

              {this.props.type === "location" && (
                <LocationMessage
                  onOpen={this.props.onOpen}
                  data={this.props.data}
                  target={this.props.target}
                  href={this.props.href}
                  apiKey={this.props.apiKey}
                  src={this.props.src}
                  zoom={this.props.zoom}
                  markerColor={this.props.markerColor}
                  text={this.props.text}
                />
              )}

              {this.props.type === "image" && (
                <PhotoMessage
                  onOpen={this.props.onOpen}
                  onDownload={this.props.onDownload}
                  onLoad={this.props.onLoad}
                  onPhotoError={this.props.onPhotoError}
                  data={this.props.file}
                  width={this.props.width}
                  height={this.props.height}
                  text={this.props.text}
                />
              )}

              {this.props.type === "audio" && (
                <AudioMessage
                  {...this.props}
                  data={this.props.file}
                  text={this.props.file.name}
                  fileType={this.props.fileType}
                />
              )}

              {this.props.type === "file" && (
                <FileMessage
                  onOpen={this.props.onOpen}
                  onDownload={this.props.onDownload}
                  data={this.props.file}
                  text={this.props.text}
                />
              )}

              {this.props.type === "spotify" && (
                <SpotifyMessage
                  width={this.props.width}
                  height={this.props.height}
                  theme={this.props.theme}
                  view={this.props.view}
                  data={this.props.data}
                  uri={this.props.uri || this.props.text}
                />
              )}

              {this.props.type === "meeting" && this.props.meeting && (
                <MeetingMessage
                  subject={this.props.meeting.subject}
                  title={this.props.meeting.title}
                  date={this.props.meeting.date}
                  dateString={this.props.meeting.dateString}
                  collapseTitle={this.props.meeting.collapseTitle}
                  participants={this.props.meeting.participants}
                  dataSource={this.props.meeting.dataSource}
                  onMeetingMessageClick={this.props.onMeetingMessageClick}
                  onMeetingVideoLinkClick={this.props.onMeetingVideoLinkClick}
                  onMeetingTitleClick={this.props.onMeetingTitleClick}
                />
              )}

              {this.props.type === "text" && (
                <div className="rce-mbox-text">
                  <span dangerouslySetInnerHTML={{ __html: this.props.text }} />
                  {this.props.isAnnouncer &&
                    this.props.text &&
                    this.props.text.length > 50 && (
                      <button
                        className="rce-mbox__read-mode-btn"
                        onClick={() => this.props.onReadMode(this.props.text)}
                      >
                        &#10020; Режим начитки
                      </button>
                    )}
                </div>
              )}
              <div className="rce-mbox-time"></div>
              <div
                className={classNames("rce-mbox-time", {
                  "non-copiable": !this.props.copiableDate,
                })}
                data-text={this.props.copiableDate ? undefined : dateText}
              >
                {this.props.status && (
                  <span className="rce-mbox-status">
                    {this.props.status === "waiting" && <MdIosTime />}

                    {this.props.status === "sent" && <MdCheck />}

                    {this.props.status === "received" && <IoDoneAll />}

                    {this.props.status === "read" && (
                      <IoDoneAll color="#4FC3F7" />
                    )}
                  </span>
                )}
                {this.props.isWriteOpponent ? null : (
                  <span
                    className={
                      "rce-mbox-status-icon" +
                      (this.props.isRead ? " __read" : "")
                    }
                  >
                    <IconCheck />
                    <IconCheck />
                  </span>
                )}
              </div>
            </div>

            {this.props.notch &&
              (this.props.position === "right" ? (
                <svg
                  className={classNames("rce-mbox-right-notch", {
                    "message-focus": this.props.focus,
                  })}
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                >
                  <path d="M0 0v20L20 0" />
                </svg>
              ) : (
                <div>
                  <svg
                    className={classNames("rce-mbox-left-notch", {
                      "message-focus": this.props.focus,
                    })}
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <defs>
                      <filter id="filter1" x="0" y="0">
                        <feOffset
                          result="offOut"
                          in="SourceAlpha"
                          dx="-2"
                          dy="-5"
                        />
                        <feGaussianBlur
                          result="blurOut"
                          in="offOut"
                          stdDeviation="3"
                        />
                        <feBlend
                          in="SourceGraphic"
                          in2="blurOut"
                          mode="normal"
                        />
                      </filter>
                    </defs>
                    <path d="M20 0v20L0 0" filter="url(#filter1)" />
                  </svg>
                </div>
              ))}
            {!this.props.isWriteOpponent && (
              <button
                className="rce-mbox__menu"
                onClick={() => this.dropDownRef.toggle({ right: 0 })}
              >
                <FaElipsisV />
                <DropDown
                  ref={(ref) => (this.dropDownRef = ref)}
                  dropUp={this.props.id === this.props.messages[0].id && this.props.type === "text"}
                  items={
                    this.props.type === "text"
                      ? [
                          {
                            title: "Удалить",
                            danger: true,
                            onClick: () => this.props.onDelete(this.props.id),
                          },
                          {
                            title: "Редактировать",
                            danger: false,
                            onClick: () => this.props.onEditClick(),
                          },
                        ]
                      : [
                          {
                            title: "Удалить",
                            danger: true,
                            onClick: () => this.props.onDelete(this.props.id),
                          },
                        ]
                  }
                />
              </button>
            )}
          </div>
        )}
      </div>
    );
  }
}

MessageBox.defaultProps = {
  position: "left",
  type: "text",
  text: "",
  title: null,
  titleColor: null,
  onTitleClick: null,
  onForwardClick: null,
  onReplyClick: null,
  onEditClick: null,
  onReplyMessageClick: null,
  date: new Date(),
  data: {},
  onClick: null,
  onOpen: null,
  onDownload: null,
  onLoad: null,
  onPhotoError: null,
  forwarded: false,
  reply: false,
  status: null,
  dateString: null,
  notch: true,
  avatar: null,
  renderAddCmp: null,
  copiableDate: false,
  onContextMenu: null,
  focus: false,
  onMessageFocused: null,
};

export default MessageBox;

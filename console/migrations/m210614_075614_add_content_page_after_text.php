<?php

use yii\db\Migration;

/**
 * Class m210614_075614_add_content_page_after_text
 */
class m210614_075614_add_content_page_after_text extends Migration
{
    public $page = '{{%content_pages}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->page, 'after_body', $this->text()->null()->after('body'));
        $this->addColumn($this->page, 'show_announcers', $this->boolean()->notNull()->after('id')->defaultValue(false));
        $this->addColumn(
            $this->page,
            'show_filters',
            $this->boolean()->notNull()->after('show_announcers')->defaultValue(false)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->page, 'after_body');
        $this->dropColumn($this->page, 'show_announcers');
        $this->dropColumn($this->page, 'show_filters');
    }
}

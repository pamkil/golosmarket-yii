import React, {useState} from 'react';
import {ReactComponent as IconPlay} from '../../../images/play.svg';

const CompilationList = ({items, mobile, ...props}) => {
  const visibleCountStep = 3;
  const desktopFactor = mobile ? 0 : 3;
  const [visibleCount, setVisibleCount] = useState(3);

  return (
    <div className="container">
      <ul className="compilation_list">
        {
          items.slice(0, visibleCount + desktopFactor).map((item, ind) => <li key={ind}>
            <div className="compilation_item">
              <div className="compilation_item--wrapper">
                <div className="compilation_item--content">
                  <a href={item.href}>
                    <div className="compilation_item--image">
                      <img src={item.image} alt=""/>
                    </div>
                  </a>
                  <div onClick={() => {
                    console.log('play', item.sound);
                  }} className="compilation_item--play">
                    <IconPlay/>
                  </div>
                </div>
              </div>
              <div className="compilation_item--description">
                <h4>{item.title}</h4>
                <p>{item.info}</p>
              </div>
            </div>
          </li>)
        }
      </ul>

      {items.length > (visibleCount + desktopFactor) ? <div className="catalog__more">
        <button onClick={() => {
          setVisibleCount(visibleCountStep + visibleCount);
        }} className="green">Показать больше подборок
        </button>
      </div> : null}
    </div>
  )
};

export default CompilationList;

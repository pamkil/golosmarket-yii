<?php

use yii\base\InvalidConfigException;
use yii\db\Migration;
use yii\rbac\DbManager;

/**
 * Class m191104_201626_create_Rbac
 */
class m191104_201626_create_Rbac extends Migration
{
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }

        return $authManager;
    }

    public function safeUp()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($authManager->ruleTable, [
            'name' => $this->string(64)->notNull(),
            'data' => $this->text(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
            'PRIMARY KEY (name)',
        ], $tableOptions);

        $this->createTable($authManager->itemTable, [
            'name' => $this->string(64)->notNull(),
            'type' => $this->integer()->notNull(),
            'description' => $this->text(),
            'rule_name' => $this->string(64),
            'data' => $this->text(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
            'PRIMARY KEY (name)',
            'FOREIGN KEY (rule_name) REFERENCES ' . $authManager->ruleTable . ' (name) ON DELETE SET NULL ON UPDATE CASCADE'
        ], $tableOptions);

        $this->createIndex('idx_type', $authManager->itemTable, 'type');

        $this->createTable($authManager->itemChildTable, [
            'parent' => $this->string(64)->notNull(),
            'child' => $this->string(64)->notNull(),
            'PRIMARY KEY (parent, child)',
            'FOREIGN KEY (parent) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY (child) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE'
        ], $tableOptions);

        $this->createTable($authManager->assignmentTable, [
            'item_name' => $this->string(64)->notNull(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'PRIMARY KEY (item_name, user_id)',
            'FOREIGN KEY (item_name) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE'
        ], $tableOptions);

        // create base roles
        $guest = $authManager->createRole('guest');
        $guest->description = 'Все пользователи';
        $authManager->add($guest);

        $admin = $authManager->createRole('admin');
        $admin->description = 'Администраторы';
        $authManager->add($admin);

        $announcer = $authManager->createRole('announcer');
        $announcer->description = 'Дикторы';
        $authManager->add($announcer);

        $client = $authManager->createRole('client');
        $client->description = 'Клиенты';
        $authManager->add($client);

        $viewAdmin = $authManager->createPermission('isAdmin');
        $viewAdmin->description = 'Администратор';
        $authManager->add($viewAdmin);

        $viewAnnouncer = $authManager->createPermission('isAnnouncer');
        $viewAnnouncer->description = 'Диктор';
        $authManager->add($viewAnnouncer);

        $viewClient = $authManager->createPermission('isClient');
        $viewClient->description = 'Клиент';
        $authManager->add($viewClient);

        $viewRules = $authManager->createPermission('changeRules');
        $viewRules->description = 'Изменение ролей';
        $authManager->add($viewRules);

        $authManager->addChild($admin, $viewRules);
        $authManager->addChild($admin, $viewAdmin);
        $authManager->addChild($admin, $viewAnnouncer);
        $authManager->addChild($client, $viewClient);
        $authManager->addChild($announcer, $viewAnnouncer);
    }

    public function safeDown()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        $this->dropTable($authManager->assignmentTable);
        $this->dropTable($authManager->itemChildTable);
        $this->dropTable($authManager->itemTable);
        $this->dropTable($authManager->ruleTable);
    }
}

import React, { useCallback, useEffect, useRef } from "react";
import FaFile from "react-icons/lib/fa/file";
import cx from "classnames";
import { MESSAGE_TYPE, MESSAGE_TYPE_RU } from "../../messageType";

import "./ReplyMessage.css";
import { format } from "timeago.js";

export default function ReplyMessage({ message, withBackground, onClick }) {
  const messageTextRef = useRef(null);
  const handleClick = useCallback(() => {
    if (typeof onClick === "function") {
      onClick(message);
    }
  }, []);
  useEffect(() => {
    if (messageTextRef === null || messageTextRef.current === null) {
      return;
    }
    messageTextRef.current.innerHTML = message.text;
  }, [messageTextRef]);
  return (
    <div
      className={cx({
        "reply-message": true,
        "reply-message--bg": withBackground,
      })}
      onClick={handleClick}
    >
      {/*<span className="reply-message__icon">>></span>*/}
      <div className="reply-message__box">
        <div className="reply-message__author">
          <span className="reply-message__author-name">{message.name}</span>
          <span className="reply-message__author-date">
            {format(message.updatedAt, "ru_RU")}
          </span>
        </div>
        {message.type !== MESSAGE_TYPE.text && (
          <div className="reply-message__type">
            <FaFile />
            <span className="reply-message__type-text">
              {MESSAGE_TYPE_RU[message.type]}{" "}
              {message.file ? `(${message.file.name})` : ""}
            </span>
          </div>
        )}
        {message.text && (
          <p ref={messageTextRef} className="reply-message__text">
            "{message.text}"
          </p>
        )}
      </div>
    </div>
  );
}

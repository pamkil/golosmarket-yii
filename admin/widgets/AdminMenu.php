<?php

namespace admin\widgets;

use yii\widgets\Menu;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;

class AdminMenu extends Menu
{
    protected function renderItem($item)
    {
        if (isset($item['url'])) {
            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);

            $replaces = [
                '{url}' => Html::encode(Url::to($item['url'])),
                '{label}' => $item['label'],
                '{icon}' => isset($item['icon']) ? $item['icon'] : ''
            ];
        } else {
            $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

            $replaces = [
                '{label}' => $item['label'],
                '{icon}' => isset($item['icon']) ? $item['icon'] : ''
            ];
        }

        if (isset($item['items'])) {
            $replaces['{arrow}'] = '<span class="arrow"></span>';
        } else {
            $replaces['{arrow}'] = '';
        }

        if (isset($item['new']) && $item['new']) {
            $replaces['{badge}'] = '<span class="badge badge-info">' . $item['new'] . '</span>';
        } else {
            $replaces['{badge}'] = '';
        }

        return strtr($template, $replaces);
    }
}
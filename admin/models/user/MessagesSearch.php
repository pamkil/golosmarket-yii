<?php

namespace admin\models\user;

use common\models\user\Chat;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class MessagesSearch extends Model
{
    public $username;
    public $firstname;
    public $lastname;
    public $email;
    public $phone;
    public $last_visit;
    public $status;
    public $is_blocked;
    public $likes;
    public $dislikes;
    public $cost;

    public function rules()
    {
        return [
            [
                ['announcer', 'client',],
                'string'
            ],
        ];
    }

    public function search($params, $msgId)
    {
        $msg = Chat::find()->where(['id' => $msgId])->asArray()->one();
        $query = Chat::find()
            ->alias('chat')
            ->with(['announcer', 'announcer.photo', 'client', 'client.photo'])
            ->where(
                [
                    'announcer_id' => $msg['announcer_id'],
                    'client_id' => $msg['client_id'],
                ]
            );

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'chat.created_at'
//                        'username',
//                        'firstname',
//                        'lastname',
//                        'email',
//                        'phone',
//                        'last_visit',
//                        'status',
//                        'is_blocked',
//                        'announcer.cost',
//                        'announcer.likes',
//                        'announcer.dislikes',
                    ],
                    'defaultOrder' => [
                        'chat.created_at' => SORT_DESC
                    ],
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );


//        $this->load($params);
//        if (!$this->validate()) {
//            $query->where('0=1');
//            return $dataProvider;
//        }

//        $query->andFilterWhere(
//            [
//                'AND',
//                ['like', 'user.username', $this->username],
//                ['like', 'user.firstname', $this->firstname],
//                ['like', 'user.lastname', $this->lastname],
//                ['like', 'user.email', $this->email],
//                ['like', 'user.phone', $this->phone],
//                ['like', 'user.last_visit', $this->last_visit],
//                ['user.status' => $this->status],
//                ['user.is_blocked' => $this->is_blocked],
//                ['announcer.likes' => $this->likes],
//                ['announcer.dislikes' => $this->dislikes],
//                ['announcer.cost' => $this->cost],
//            ]
//        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
<?php

use yii\db\Migration;

/**
 * Class m210621_195602_create_redirects
 */
class m210621_195602_create_redirects extends Migration
{
    private $table = '{{%redirect}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'active' => $this->boolean()->notNull()->defaultValue(true),
                'from' => $this->string(512)->notNull()->unique(),
                'to' => $this->string(512)->notNull(),
                'code' => $this->integer()->null(),
                'created_at' => $this->dateTime()->null(),
                'updated_at' => $this->dateTime()->null(),
            ],
            $tableOptions
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}

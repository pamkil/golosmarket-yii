<?php

use yii\helpers\Html;

/* @var $userName string */
/* @var $resetLink string */

?>
<div class="password-reset">
    <p>Здравствуйте, <?= Html::encode($userName) ?>. Мы рады вашему возвращению (◕‿◕)</p>
    <p>Для восстановления профиля, перейдите по ссылке и введите ваши логин и пароль для входа:</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    <p>Если вы забыли пароль, после перехода по ссылке, зайдите в раздел Восстановление доступа.</p>
</div>

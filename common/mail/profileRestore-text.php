<?php

/* @var $userName string */
/* @var $resetLink string */
?>
Здравствуйте, <?= $userName ?> Мы рады вашему возвращению ☺.

Для восстановления профиля, перейдите по ссылке и введите ваши логин и пароль для входа.

<?= $resetLink ?>

Если вы забыли пароль, после перехода по ссылке, зайдите в раздел Восстановление доступа.

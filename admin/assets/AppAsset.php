<?php

namespace admin\assets;

//use kartik\select2\Select2Asset;
//use kartik\select2\ThemeKrajeeAsset;
//use speixoto\amcharts\AmChartAsset;
use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
        "https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic",
//        'css/vendor/components-md.css',
//        'css/vendor/metronic-addon.css',
//        'css/vendor/plugins-md.css',
//        'css/vendor/layout.css',
//        'css/vendor/light.css',

//        'vendor/uniform/css/uniform.default.css',

        // search
//        'css/vendor/search.css',

        // custom
        'css/custom.css'
    ];

    public $js = [
    ];

    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap4\BootstrapAsset',
//        'yii\bootstrap\BootstrapAsset',
//        'yii\bootstrap\BootstrapPluginAsset',
    ];
}

<?php

namespace admin\models\content;

use common\models\content\Menu;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class MenuSearch extends Model
{
    public $path;
    public $title;
    public $type_id;
    public $parent_id;
    public $position;
    public $active;

    public function rules()
    {
        return [
            [
                ['path', 'title'],
                'string'
            ],
            [
                ['type_id', 'parent_id', 'position'],
                'integer'
            ],
            [
                ['active',],
                'boolean'
            ],
        ];
    }

    public function search($params)
    {
        $query = Menu::find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'path',
                        'title',
                        'position',
                        'type_id',
                    ]
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $dataProvider->sort->defaultOrder = [
            'type_id' => SORT_ASC,
            'position' => SORT_ASC,
        ];

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'AND',
                ['like', 'title', $this->title],
                ['like', 'path', $this->path],
                ['type_id' => $this->type_id],
                ['parent_id' => $this->parent_id],
                ['position' => $this->position],
                ['active' => $this->active],
            ]
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
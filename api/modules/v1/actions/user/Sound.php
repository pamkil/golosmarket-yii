<?php

namespace api\modules\v1\actions\user;

use api\modules\v1\actions\common\UpdateAction;
use Yii;
use yii\web\BadRequestHttpException;

class Sound extends UpdateAction
{
    /**
     * @OA\Put(
     *   path="/user/sound",
     *   summary="Update field Project. Обновление полей проекта",
     *   tags={"User"},
     *   security={{"BearerAuth": {}}},
     *   @OA\Response(
     *     response="200",
     *     description="Объект пользователя",
     *     @OA\JsonContent(ref="#/components/schemas/UserEdit")
     *   )
     * )
     * @param int $id
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id = 0)
    {
        /* @var $model \api\modules\v1\models\records\auth\User */
        $model = $this->findModel((int)Yii::$app->user->id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        $postData = Yii::$app->request->post();
        $transaction = Yii::$app->db->beginTransaction();

        if (empty($postData['url'])) {
            throw new BadRequestHttpException('Неверные данные для операции');
        }
        try {
            $url = basename($postData['url']);
            $isSetDefault = false;
            foreach ($model->announcer->sounds as $item) {
                if ($item->filename === $url || $item->filename . '.' . $item->ext === $url) {
                    $item->default = 1;
                    $isSetDefault = true;
                    $item->save();
                } else {
                    $item->default = 0;
                    $item->save();
                }
            }
            if ($isSetDefault) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
            Yii::$app->response->statusCode = 202;

            return '';
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }
}

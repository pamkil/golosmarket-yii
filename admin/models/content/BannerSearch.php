<?php

namespace admin\models\content;

use common\models\content\Banner;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class BannerSearch extends Model
{
    public $active;
    public $position;

    public function rules()
    {
        return [
            [
                ['active',],
                'boolean'
            ],
            [
                ['position',],
                'integer'
            ],
        ];
    }

    public function search($params)
    {
        $query = Banner::find();

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'active', 'position'
                    ]
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $dataProvider->sort->defaultOrder = ['position' => SORT_ASC,];

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'AND',
                ['active' => $this->active],
                ['position' => $this->position],
            ]
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
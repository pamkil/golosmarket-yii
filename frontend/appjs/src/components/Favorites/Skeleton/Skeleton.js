import React from 'react';
import ContentLoader from 'react-content-loader';

const Skeleton = props => {
  return (
    <ContentLoader
      speed={2}
      width={250}
      height={88}
      viewBox="0 0 250 80"
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
      {...props}
    >
      <rect x="48" y="8" rx="3" ry="3" width="70" height="8" />
      <rect x="48" y="26" rx="3" ry="3" width="100" height="6" />
      <circle cx="20" cy="20" r="20" />
      <rect x="0" y="54" rx="6" ry="6" width="50" height="18" />
      <circle cx="227" cy="62" r="17" />
      <rect x="60" y="54" rx="6" ry="6" width="113" height="18" />
    </ContentLoader>
  );
};

Skeleton.metadata = {
  name: '__REPLACE_ME__', // My name
  github: '__REPLACE_ME__', // Github username
  description: '__REPLACE_ME__', // Little tagline
  filename: '__REPLACE_ME__' // filename of your loader
};

export default Skeleton;

<?php

namespace common\service\user\dto;

class PayedSearchDto extends \Spatie\DataTransferObject\DataTransferObject
{
    public bool $isAnnouncer;
    public int $userId;
}

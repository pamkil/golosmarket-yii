<?php

use yii\db\Migration;

/**
 * Class m191121_182656_add_settings
 */
class m191121_182656_add_settings extends Migration
{
    private $setting = '{{%settings}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            $this->setting,
            [
                'id' => $this->primaryKey(),
                'code' => $this->string()->unique(),
                'value' => $this->string(),
                'description' => $this->string(1024),
                'created_at' => $this->timestamp()->notNull(),
                'updated_at' => $this->timestamp()->notNull(),
                'deleted_at' => $this->timestamp(),
            ],
            $tableOptions
        );

        $this->addData();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->setting);
    }

    private function addData()
    {
        $this->batchInsert(
            $this->setting,
            ['code', 'value', 'description'],
            [
                ['percent', '30', 'Величина процента комиссии с исполнителя, в процентах'],
                ['ya_money', '4100192642122', 'Номер кошелька куда переводить деньги'],
                ['email_feedback', 'golosmarket@golosmarket.ru', 'Почта для обратной связи'],
                ['smpt_port', '465', 'Исходящая почта: порт сервера'],
                ['smpt_server', 'ssl://smtp.yandex.com', 'Исходящая почта: адрес сервера'],
                ['smpt_username', 'golosmarket@golosmarket.ru', 'Исходящая почта: логин почты'],
                ['smpt_userpassword', 'password', 'Исходящая почта: пароль'],
            ]
        );
    }
}

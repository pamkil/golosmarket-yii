<?php

namespace common\repositories\contract;

use common\models\content\Collection;

interface CollectionRepositoryContract
{
    public function getAll(): array;
    public function getAllActive(): array;
    public function getById(int $id): ?Collection;
    public function getActiveById(int $id): ?Collection;
}

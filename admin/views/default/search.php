<?

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use common\models\ElasticSearch;

/**
 * @var ActiveDataProvider $dataProvider
 * @var string $query
 */
?>

<div class="search-page search-content-2">
    <div class="search-bar bordered">
        <div class="row">
            <div class="col-md-7">
                <? $form = ActiveForm::begin([
                    'method' => 'GET',
                    'action' => '/default/search',
                    'options' => [
                        'data' => ['pjax' => true,],
                        'enctype' => 'multipart/form-data' // important
                    ]
                ]) ?>
                    <div class="input-group">
                        <input type="text" name="query" class="form-control" placeholder="Я ищу..." value="<?= $query ?>">
                        <span class="input-group-btn">
                        <button class="btn blue uppercase bold" type="submit">Поиск</button>
                    </span>
                    </div>
                <? ActiveForm::end() ?>
            </div>
            <div class="col-md-5">
                <p class="search-desc clearfix">Для поиска введите фразу, которую вы хотите найти</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'label' => 'Название',
                            'attribute' => '_source.name',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return ElasticSearch::getLink($data['_source']['name'], $data['_type'], $data['_id']);
                            }
                        ]
                    ]
                ]) ?>
            </div>
        </div>
    </div>
</div>

<?php

namespace admin\controllers;

use Yii;
use yii\base\Action;
use yii\filters\VerbFilter;
use yii\web\Controller;

class SecurityController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
//                    '*' => ['get']
                    //Todo after correction (beforeAction()) it is necessary to correct in all controllers on VerbFilter
                    '*' => ['get', 'post', 'put', 'delete']
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (Yii::$app->user->id) {
            $lastLogin = Yii::$app->session->get('last_login_timestamp', time());
            if ((time() - $lastLogin) > 15 * 60) {
                Yii::$app->user->logout();
            } else {
                Yii::$app->session->set('last_login_timestamp', time());
            }
        }

        /**
         * @var Action $action
         */
        $redirect = false;
        if ($action->id != 'login' && !$this->isAccessAdminPanel() ) {
            $redirect = true;
        }

        $url = Yii::$app->request->get('url');
        if (empty($url)) {
            $url = $_SERVER['REQUEST_URI'];
        }

        if ($redirect && $url != '/admin') {
            $this->redirect(['/default/login', 'url' => $url]);
            return false;
        }

        Yii::$app->view->registerMetaTag(['name' => 'robots', 'content' => 'noindex, nofollow'], 'robots');

        return parent::beforeAction($action);
    }

    protected function isAccessAdminPanel()
    {
        return !Yii::$app->user->isGuest
            && Yii::$app->user->can('isAdmin');
    }
}

<?php

namespace common\service\user\dto;

class BillSearchDto extends \Spatie\DataTransferObject\DataTransferObject
{
    public bool $isAnnouncer;
    public int $userId;
    public int $page = 1;
    public int $perPage = 20;
}

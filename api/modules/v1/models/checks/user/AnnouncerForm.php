<?php

namespace api\modules\v1\models\checks\user;

use api\modules\v1\models\records\file\File;
use common\models\announcer\Age;
use common\models\announcer\Gender;
use common\models\announcer\Language;
use common\models\announcer\Presentation;
use common\models\announcer\Tag;
use common\models\announcer\Timber;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * @OA\Schema(title="Форма пользователя")
 */
class AnnouncerForm extends Model
{
    /**
     * @OA\Property(
     *     type="string",
     *     description="info",
     *     title="info",
     * )
     *
     * @var string
     */
    public $info;

    /**
     * @OA\Property(
     *     type="string",
     *     description="equipment",
     *     title="equipment",
     * )
     *
     * @var string
     */
    public $equipment;

    /**
     * @OA\Property(
     *     type="string",
     *     description="cost",
     *     title="cost",
     * )
     *
     * @var string
     */
    public $cost;

    /**
     * @OA\Property(
     *     type="string",
     *     description="cost_a4",
     *     title="cost_a4",
     * )
     *
     * @var string
     */
    public $cost_a4;

    /**
     * @OA\Property(
     *     type="string",
     *     description="cost_vocal",
     *     title="cost_vocal",
     * )
     *
     * @var string
     */
    public $cost_vocal;

    /**
     * @OA\Property(
     *     type="string",
     *     description="cost_parody",
     *     title="cost_parody",
     * )
     *
     * @var string
     */
    public $cost_parody;

    /**
     * @OA\Property(
     *     type="string",
     *     description="cost_sound_mount",
     *     title="cost_sound_mount",
     * )
     *
     * @var string
     */
    public $cost_sound_mount;

    /**
     * @OA\Property(
     *     type="string",
     *     description="cost_audio_adv",
     *     title="cost_audio_adv",
     * )
     *
     * @var string
     */
    public $cost_audio_adv;

    /**
     * @OA\Property(
     *     type="string",
     *     description="cash_yamoney",
     *     title="cash_yamoney"
     * )
     *
     * @var string
     */
    public $cash_yamoney;

    public $presentations;

    /**
     * @var UploadedFile[]
     */
    public $photos;
    /**
     * @var UploadedFile[]
     */
    public $sounds;

    /** @var File[] */
    private $photoModels;

    /** @var File[] */
    private $soundModels;

    public function formName()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['info', 'equipment'],
                'string',
            ],

            [
                ['cash_yamoney'],
                'string',
                'max' => 255
            ],
            [
                ['cost', 'cost_a4', 'cost_vocal', 'cost_parody', 'cost_sound_mount', 'cost_audio_adv'],
                'number',
            ],
            [
                ['presentations'],
                'default',
            ],
            [
                ['photos', 'sounds'],
                'each',
                'rule' => [
                    'file',
                    'maxSize' => 30 * 1024 * 1024,
                ],
            ],
        ];
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        foreach (['presentations'] as $item) {
            $out = [];
            foreach ($this->{$item} ?? [] as $field) {
                $out[] = $field['id'];
            }
            $this->{$item} = $out;
        }

        $out = [];
        if ($this->presentations) {
            $out = Presentation::find()
                ->select('id')
                ->where(['code' => $this->presentations])
                ->column();
        }
        $this->presentations = $out;

        $result = parent::toArray($fields, $expand, $recursive);
        unset($result['photos']);
        unset($result['sounds']);

        return $result;
    }

    public function upload()
    {
        return $this->uploadPhotos() && $this->uploadSounds();
    }

    public function uploadPhotos()
    {
        if ($this->validate()) {
            if ($this->photos) {
                $this->photoModels = [];
                foreach ($this->photos as $file) {
                    $this->photoModels[] = File::setFileByUpload($file);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function uploadSounds()
    {
        if ($this->validate()) {
            if ($this->sounds) {
                $this->soundModels = [];
                foreach ($this->sounds as $file) {
                    $this->soundModels[] = File::setFileByUpload($file);
                }
            };
            return true;
        } else {
            return false;
        }
    }

    public function getPhotoModels(): array
    {
        return $this->photoModels ?? [];
    }

    public function getSoundModels(): array
    {
        return $this->soundModels ?? [];
    }
}

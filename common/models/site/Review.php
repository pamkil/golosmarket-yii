<?php

namespace common\models\site;

use common\models\access\User;
use common\models\Model;

/**
 * Отзыв о сервисе
 *
 * Class Review
 * @package common\models\site
 * @property int $id
 * @property int $user_id [int(11)]
 * @property bool $is_public [tinyint(1)]
 * @property int $show_quantity [int(11)]
 * @property int $rating [tinyint(3)]
 * @property string $text
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property User $client
 */
class Review extends Model
{
    public const MAX_SHOW = 1;

    public static function tableName()
    {
        return '{{%site_review}}';
    }

    public function rules()
    {
        return [
            [
                ['user_id', 'rating', 'show_quantity'],
                'integer'
            ],
            [
                ['is_public',],
                'boolean'
            ],
            [
                ['text',],
                'string'
            ],
            [
                ['text',],
                'filter',
                'filter' => function ($value) {
                    if (!empty($value)) {
                        $value = strip_tags($value, '<br>');
                        $value = str_replace('<br>', PHP_EOL, $value);
                        $value = str_replace('<br/>', PHP_EOL, $value);
                    }

                    return $value;
                }
            ],
            [
                ['show_quantity'],
                'default',
                'value' => 0
            ],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Клиент',
            'is_public' => 'Опубликован',
            'show_quantity' => 'Кол-во показов',
            'text' => 'Текс отзыва',
            'rating' => 'Рейтинг',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function getClient()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public static function getReviews(int $quantity = 3, int $userId = null)
    {
        return static::find()
            ->where(['is_public' => true])
            ->andFilterWhere(['user_id' => $userId])
            ->with(['client', 'client.photo'])
            ->limit($quantity)
            ->asArray()
            ->all();
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id'])->where([]);
    }

    public static function getMax()
    {
        return static::find()
            ->max('updated_at');
    }
}
<?php

namespace api\modules\v1\models\records\user;

use api\modules\v1\models\records\announcer\Opponent;
use api\modules\v1\models\records\auth\User;
use common\models\site\Setting;

/**
 * @OA\Schema(title="Счет в профиле")
 */
class BillProfile extends Bill
{
    public static $isAnnouncer = false;
    /**
     * @OA\Property(property="id", type="integer", description="ИД")
     * @OA\Property(property="announcer_id", type="integer", description="ИД диктора")
     * @OA\Property(property="client_id", type="integer", description="ИД клиента")
     * @OA\Property(property="sum", type="integer", description="Сумма счета")
     * @OA\Property(property="num", type="string", description="Номер счета")
     * @OA\Property(property="created_at", type="boolean", description="Время создания")
     * @OA\Property(property="updated_at", type="boolean", description="Время обновления")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'announcer_id',
            'client_id',
            'sum' => 'sumFormat',
            'num',
            'opponent',
            'account',
            'status' => 'statusTitle',
            'sumBalance',
            'date' => 'createdFormat',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
        ];
    }

    public function getStatusTitle()
    {
        $statuses = self::$isAnnouncer ? self::getStatuses() : self::getForClientStatuses();

        return $statuses[$this->status] ?? '';
    }

    public function getSumBalance()
    {
        if (!self::$isAnnouncer) {
            return null;
        }

        return \Yii::$app->getFormatter()->asCurrency($this->sum * (100 - ($this->percent ?? 0)) / 100);
    }

    public function getSumFormat()
    {
        return \Yii::$app->getFormatter()->asCurrency($this->sum);
    }

    public function getCreatedFormat()
    {
        return \Yii::$app->getFormatter()->asDatetime($this->date);
    }

    public function getNum()
    {
        return "gm-$this->announcer_id-$this->id";
    }

    public function getAccount()
    {
        return Setting::getSetting(Setting::YA_MONEY);
    }

    public function getAnnouncer()
    {
        return $this->hasOne(Opponent::class, ['id' => 'announcer_id'])->where([]);
    }

    public function getClient()
    {
        return $this->hasOne(Opponent::class, ['id' => 'client_id'])->where([]);
    }

    public function getOpponent()
    {
        return $this->hasOne(Opponent::class, ['id' => self::$isAnnouncer ? 'client_id' : 'announcer_id'])->where([]);
    }

    /**
     * @param $announcerId
     * @param $clientId
     *
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getNew($announcerId, $clientId): ?Bill
    {
        return Bill::find()
            ->where([
                'AND',
                ['announcer_id' => $announcerId],
                ['client_id' => $clientId],
                ['status' => Bill::STATUS_NEW],
                ['is_canceled' => false],
            ])
            ->one();
    }
}

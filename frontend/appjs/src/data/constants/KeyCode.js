export const KEY_CODE = {
    ARROWS: {
        UP: 38,
        DOWN: 40,
        LEFT: 37,
        RIGHT: 39
    },
    BACKSPACE: 8,
    DELETE: 46,
    ENTER: 13,
    ESC: 27,
    SPACE: 32,
    TAB: 9
};

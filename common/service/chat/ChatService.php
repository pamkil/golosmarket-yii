<?php

namespace common\service\chat;

use Carbon\Carbon;
use common\models\access\User;
use api\modules\v1\models\records\user\Chat;
use common\models\user\SendNotify;
use common\repositories\contract\ChatRepositoryContract;
use common\service\chat\contract\ChatServiceContract;
use common\service\Epochta;
use Yii;
use yii\helpers\Console;
use common\service\chat\dto\CreateMessageDto;
use yii\helpers\Json;
use api\modules\v1\models\checks\chat\ChatForm;
use yii\web\UploadedFile;
use api\modules\v1\models\records\announcer\Opponent;
use api\modules\v1\models\records\user\Bill;
use api\modules\v1\models\filters\user\ChatSearch;
use common\models\announcer\Announcer;

class ChatService implements ChatServiceContract
{
    private ChatRepositoryContract $chatRepository;

    public function __construct(
        ChatRepositoryContract $chatRepository
    )
    {
        $this->chatRepository = $chatRepository;
    }


    public function sendSmsNotifyByUnreadMessages(): bool
    {
        //Отправляем только дикторам
        $subMinutes = Yii::$app->params['subMinutes'];

        $chatMessages = $this->chatRepository->findUnreadToSms();

        foreach ($chatMessages as $message) {
            $user = $message['is_write_client'] ? User::findOne($message['announcer_id']) : null;

            if (!$user->notify_sms) {
                continue;
            }

            if (empty($user) || empty($user->phone) || !$user->isAnnouncer || $message['is_read']) {
                continue;
            }

            $phone = $user->phone;
            $userId = $user->id;

            $notify = SendNotify::getNotify($userId, SendNotify::TYPE_SMS);
            $nowDate = Carbon::now();
            $firstMessageDate = Carbon::parse($message['first_message_time']);
            $lastMessageDate = Carbon::parse($message['last_message_time']);

            if ($notify) {
                $notifyDate = Carbon::parse($notify->date);
                $send = false;
                $firstOtherSend = $subMinutes['first_send'] + $subMinutes['other_send'];

                if ($notifyDate->diffInMinutes($nowDate) > $subMinutes['other_send']) {
                    if (
                        $firstMessageDate->diffInMinutes($nowDate) === $subMinutes['first_send']
                    ) {
                        $send = true;
                    } else if ($firstMessageDate->diffInMinutes($nowDate) > $subMinutes['other_send']) {
                        if ($lastMessageDate->diffInMinutes($nowDate) === $subMinutes['first_send']) {
                            $send = true;
                        }
                    }
                } else if ($notifyDate->diffInMinutes($nowDate) === $subMinutes['other_send']) {
                    if (
                        $firstMessageDate->diffInMinutes($nowDate) === $firstOtherSend ||
                        $lastMessageDate->diffInMinutes($nowDate) === $firstOtherSend
                    ) {
                        $send = true;
                    }
                }

                if (!$send) {
                    continue;
                }

                try {
                    $result = Epochta::sendSms('Na golosmarket.ru novoe soobshenie ot zakazchika. Zaidite v chat', $phone);
                    if ($result) {
                        SendNotify::setNow($userId);
                    }
                } catch (\Exception $exception) {
                    Yii::error($exception);
                }
            } else {
                $checkTime = $firstMessageDate->diffInMinutes($nowDate) === 3;

                if ($checkTime) {
                    try {
                        $result = Epochta::sendSms('Na golosmarket.ru novoe soobshenie ot zakazchika. Zaidite v chat', $phone);
                        if ($result) {
                            SendNotify::remove($user->id, SendNotify::TYPE_SMS);
                            SendNotify::send($userId, SendNotify::TYPE_SMS);
                        }
                    } catch (\Exception $exception) {
                        Yii::error($exception);
                    }
                }
            }
        }

        return empty($chatMessages);
    }

    public function createMessage(CreateMessageDto $createMessageDto)
    {
        $sender = User::findOne($createMessageDto->senderId);

        $user = User::findOne($createMessageDto->userId);

        if ($user->notify_telegram && $user->telegram_chat_id) {
            Yii::$app->telegram->sendMessage([
                'chat_id' => $user->telegram_chat_id,
                'text' => "Вам пришло сообщение от " . $sender->lastname . " " . $sender->firstname . "\n\n" . $createMessageDto->text,
                // 'reply_markup' => json_encode([
                //     'inline_keyboard' => [
                //         [
                //             ['text' => 'Я на связи', 'callback_data' => Json::encode(['text' => 'Я на связи', 'id' => $user->id, 'sender_id' => $sender->id])],
                //             ['text' => 'Буду рад', 'callback_data' => Json::encode(['text' => 'Буду рад', 'id' => $user->id, 'sender_id' => $sender->id])],
                //         ],
                //         [
                //             ['text' => 'Скоро отвечу', 'callback_data' => Json::encode(['text' => 'Скоро отвечу', 'id' => $user->id, 'sender_id' => $sender->id])],
                //             ['text' => 'Срок исполнения?', 'callback_data' => Json::encode(['text' => 'Срок исполнения?', 'id' => $user->id, 'sender_id' => $sender->id])],
                //         ],
                //         [
                //             ['text' => 'Жду задачу', 'callback_data' => Json::encode(['text' => 'Жду задачу', 'id' => $user->id, 'sender_id' => $sender->id])],
                //             ['text' => 'Нет, к сожалению', 'callback_data' => Json::encode(['text' => 'Нет, к сожалению', 'id' => $user->id, 'sender_id' => $sender->id])],
                //         ]
                //     ]
                // ])
            ]);
            Yii::$app->telegram->sendMessage([
                'chat_id' => $user->telegram_chat_id,
                'text' => "Для ответа перейдите в чат на golosmarket.ru",
            ]);
        }

        if (Yii::$app->request->post("edit")) {
            $row = (new \yii\db\Query())
                ->select(['announcer_id', 'client_id'])
                ->from('user_chat')
                ->where(['id' => Yii::$app->request->post("message_id")])
                ->limit(1)
                ->one();

            if (Yii::$app->user->identity->isAnnouncer) {
                if ((int)$row['announcer_id'] !== (int)Yii::$app->user->identity->id) {
                    throw new BadRequestHttpException("Вы анонсер и Нельзя редактировать чужое сообщение");
                }
            } else {
                if ((int)$row['client_id'] !== (int)Yii::$app->user->identity->id) {
                    throw new BadRequestHttpException("Вы клиент и Нельзя редактировать чужое сообщение");
                }
            }

            Yii::$app->db->createCommand()
                ->update('user_chat',
                    ['text' => Yii::$app->request->post("text"), 'is_edit' => true],
                    ['id' => Yii::$app->request->post("message_id")]
                )->execute();

            return;
        }

        if (empty($createMessageDto->userId)) {
            throw new BadRequestHttpException("Нет данных кому написать");
        }

        $typeReceiver = User::getType((int)$createMessageDto->userId);

        if (!$typeReceiver) {
            throw new BadRequestHttpException("Не найден пользователь которому вы пишете");
        }

        if ($sender->type === $typeReceiver) {
            throw new BadRequestHttpException("Не можете написать этому пользователю");
        }

        $_isAnnouncer = Announcer::findOne($sender->id);

        if (!$_isAnnouncer) {
            $temp = $createMessageDto->userId;

            $createMessageDto->userId = $sender->id;
            $createMessageDto->clientId = $temp;
            $createMessageDto->isWriteClient = false;
        } else {
            $createMessageDto->clientId = $sender->id;
            $createMessageDto->isWriteClient = true;
        }

        $transaction = Yii::$app->db->beginTransaction();
        $outModels = [];
        try {
            $chatForm = new ChatForm();
            
            $chatForm->announcer_id = $createMessageDto->userId;
            $chatForm->client_id = $createMessageDto->clientId;
            $chatForm->parent_id = $createMessageDto->parentId;
            $chatForm->is_write_client = $createMessageDto->isWriteClient;
            $chatForm->text = $createMessageDto->text;

            $chatForm->files = UploadedFile::getInstancesByName('files');
            if ($chatForm->upload()) {
                $model = new Chat();
                $model->load($chatForm->toArray(), '');
                if (!empty($model->text)) {
                    $model->save();
                    $model->refresh();
                    $outModels[] = $model;
                }

                if ($files = $chatForm->getFileModels()) {
                    $chatForm->text = null;
                    foreach ($files as $fileModels) {
                        $file = $fileModels['file'];
                        $fileWatermark = $fileModels['watermark'];
                        if ($file->save() === false || ($fileWatermark && $fileWatermark->save() === false)) {
                            throw new BadRequestHttpException($file->errors);
                        }
                        $outModels[] = $model = new Chat();
                        $model->load($chatForm->toArray(), '');
                        $model->file_id = $file->id;
                        $model->file_type = $fileWatermark ? FileTypeEnum::FILE_TYPE_ORIGIN : FileTypeEnum::FILE_TYPE_PUBLIC;
                        $model->file_watermark_id = $fileWatermark ? $fileWatermark->id : null;
                        if ($model->save() === false) {
                            throw new BadRequestHttpException($model->errors);
                        }
                        $model->refresh();
                    }
                }

                $transaction->commit();

                SendNotify::read($user->id, SendNotify::TYPE_SMS);

                $isAnnouncer = $_isAnnouncer;
                $announcerId = $isAnnouncer ? $sender->id : $user->id;
                $clientId = $isAnnouncer ? $user->id : $sender->id;

                $opponent = Opponent::findOne((int)$createMessageDto->userId);
                $bill = Bill::getNew($announcerId, $clientId);

                return [
                    'items' => (new ChatSearch())->search($createMessageDto->userId, $sender->id, (bool)$_isAnnouncer, 0, 50),
                    'opponent' => $opponent,
                    'bill' => $bill,
                    'unread' => Chat::getUnread($opponent->id, (bool)$_isAnnouncer)
                ];
            }
            throw new BadRequestHttpException(reset($chatForm->firstErrors));
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }
}
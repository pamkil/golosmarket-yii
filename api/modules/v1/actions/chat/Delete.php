<?php

namespace api\modules\v1\actions\chat;

use api\modules\v1\models\records\user\Chat;
use Carbon\Carbon;
use Yii;
use yii\rest\Action;
use yii\web\BadRequestHttpException;

/**
 * @OA\Delete(
 *      path="/chat/{id}/chat",
 *      summary="Отменить chat с собеседником, ИД chata",
 *      tags={"Chat"},
 *      @OA\Response(
 *          response="200",
 *          description="Объект токена",
 *          @OA\JsonContent(ref="#/components/schemas/Chat")
 *      ),
 *      @OA\Response(
 *          response="400",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="401",
 *          description="Unauthorized",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */

/**
 * Class Create
 * @throws BadRequestHttpException
 * @package api\modules\v1\actions\chat
 *
 */
class Delete extends Action
{
    public function run($id)
    {
        $userId = (int)Yii::$app->user->getId();
        if (empty($userId)) {
            throw new BadRequestHttpException("Не авторизован");
        }

        /** @var Chat $chat */
        $chat = Chat::find()->where(['id' => (int)$id])->one();

        if (!$chat) {
            throw new BadRequestHttpException("Не найден сообщение");
        }

        if ($chat->getIsWriteOpponent()) {
            throw new BadRequestHttpException("Не можете удалить это сообщение");
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $chat->deleted_at = Carbon::now()->toDateTimeString();
            if (!$chat->save()) {
                return $chat;
            }
            $transaction->commit();
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }

        return '';
    }
}

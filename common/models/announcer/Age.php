<?php

namespace common\models\announcer;

use common\models\access\User;
use common\models\Model;

/**
 * Возраст
 *
 * @OA\Schema(title="Возраст")
 *
 * Class Age
 * @package common\models\announcer
 *
 * @property int $id
 * @property string $name
 * @property bool $active [tinyint(1)]
 * @property User[] $announcers
 * @property int $position [int(11)]
 * @property string $code [varchar(25)]
 * @property string $menu_title [varchar(255)]
 */
class Age extends Model
{
    public static function tableName()
    {
        return '{{%user_announcer_age}}';
    }

    public function behaviors()
    {
        $return = parent::behaviors();

        unset($return['timestamp']);

        return $return;
    }

    public function rules()
    {
        return [
            [
                ['name', 'code'],
                'unique'
            ],
            [
                ['name', 'menu_title', 'code'],
                'string'
            ],
            [
                ['position'],
                'number'
            ],
            [
                ['active'],
                'boolean'
            ],
            [
                ['code', 'name', 'menu_title'],
                'required'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'active' => 'Активность',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function getAnnouncers()
    {
        return $this->hasMany(User::class, ['id' => 'announcer_id'])
            ->via('{{%user_announcer_2_age}}', ['age_id' => 'id']);
    }
}

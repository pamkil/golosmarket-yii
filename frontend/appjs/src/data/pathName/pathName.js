const setPathNameWithoutPageReload = path => {
    const newurl =
        window.location.protocol +
        "//" +
        window.location.host +
        path;
    window.history.pushState({ path: newurl }, "", newurl);
};

let parseResult = {
    key: '',
    result: {}
}
export const getPathNameValue = (pathName = window.location.pathname) => {
    if (parseResult.key !== pathName) {
        parseResult.key = pathName;
        pathName = pathName.split('/').filter(item => !!item);
        parseResult.result = pathName;
    }

    return parseResult.result;
};

export const setPathNameValue = newValues => {
    setPathNameWithoutPageReload(newValues);
};
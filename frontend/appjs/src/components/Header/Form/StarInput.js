import { ReactComponent as Star } from "../../../images/star_big_full.svg";
import { ReactComponent as StarEmpty } from "../../../images/star_big_empty.svg";
import React from "react";

const starsArray = [1, 2, 3, 4, 5];

const StartInput = ({
                        input: reduxFormProps,
                        ...inputProps
                    }) => {
    const clickHandler = key => {
        reduxFormProps.onChange(key);
    }

    return (
        <div className="input-stars">
            {
                starsArray.map((item, key) => (
                    <span className="greenstar pointer" key={ key } onClick={ () => clickHandler(key + 1) }>
                        {
                            key < reduxFormProps.value ? <Star /> : <StarEmpty />
                        }
                    </span>
                ))
            }
        </div>
    );
}

export default StartInput;
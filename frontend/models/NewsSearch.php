<?php

namespace frontend\models;

use common\models\content\News;
use common\models\Model;
use Yii;
use yii\data\ActiveDataProvider;

class NewsSearch extends Model
{
    public static function search()
    {
        $query = News::find()
            ->where([
                'active' => true,
            ]);

        return Yii::createObject([
            'class' => ActiveDataProvider::class,
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ],
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ]
            ],
        ]);
    }
}
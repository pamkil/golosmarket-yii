<?php

use admin\models\content\AnswerSearch;
use kartik\widgets\Select2;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * @var ActiveDataProvider $dataProvider
 * @var AnswerSearch $searchModel
 */

?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <a href="<?= Url::to(['create']) ?>" class="btn bg-olive btn-flat margin">
            Добавить
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <div class="box-body">
        <?= GridView::widget(
            [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    'id',
                    [
                        'value' => function ($data) {
                            return $data->question->text;
                        },
                        'filter' => Select2::widget(
                            [
                                'name' => 'question_id',
                                'value' => $searchModel['question_id'] ?? null,
                                'initValueText' => $searchModel['question']['text'] ?? null,
                                'options' => ['placeholder' => 'Не указан'],
                                'pluginOptions' => [
                                    'placeholder' => 'Не указан',
                                    'allowClear' => true,
                                    'ajax' => [
                                        'url' => Url::to(['content-question/ajax']),
                                        'dataType' => 'json',
                                        'delay' => 250,
                                        'data' => new JsExpression(
                                            'function(params) { return {q:params.term, page: params.page}; }'
                                        ),
                                        'cache' => true
                                    ]
                                ],
                                'size' => Select2::SMALL
                            ]
                        ),
                    ],
                    'active:boolean',
                    'position',
                    'text',
                    [
                        'class' => ActionColumn::class,
                        'template' => '{update}  {delete}',
                        'buttonOptions' => ['class' => 'margin']
                    ],
                ]
            ]
        ); ?>
    </div>
</div>

<?php

namespace admin\controllers;

use admin\models\user\ChatSearch;
use admin\models\user\MessagesSearch;
use common\models\user\Chat;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class ChatController extends SecurityController
{
    private function getChats()
    {
        $searchModel = new ChatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return [$dataProvider, $searchModel];
    }

    public function actionIndex()
    {
        [$dataProvider, $searchModel] = $this->getChats();

        $this->view->title = 'Список';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];
        $_GET = ArrayHelper::filter($_GET, ['id', 'page', 'msg-page']);

        return $this->render('index', compact(
            'dataProvider',
            'searchModel'
        ));
    }

    public function actionView($id, $page = 1)
    {
        /** @var ActiveDataProvider $dataProvider */
        [$dataProvider, $searchModel] = $this->getChats();
        $dataProvider->pagination->page = $page - 1;

        $searchMessagesModel = new MessagesSearch();
        $dataMessagesProvider = $searchMessagesModel->search(Yii::$app->request->get(), $id);

        $dataMessagesProvider->pagination->pageParam = 'msg-page';
        $dataMessagesProvider->pagination->pageSize = 15;
        $dataMessagesProvider->sort->sortParam = 'msg-sort';
        $_GET = ArrayHelper::filter($_GET, ['id', 'page', 'msg-page']);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'dataMessagesProvider' => $dataMessagesProvider,
            'searchMessagesModel' => $searchMessagesModel,
        ]);
    }

//    /**
//     * @param $id
//     * @return Response
//     * @throws NotFoundHttpException
//     * @throws Throwable
//     * @throws StaleObjectException
//     */
    public function actionDelete($id, $page = 1)
    {
        /* @var Chat $model */
        $model = Chat::findOne(['id' => $id]);
        if ($model === NULL) {
//            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect(['index', 'page' => $page]);//$this->redirect('index');
    }
}
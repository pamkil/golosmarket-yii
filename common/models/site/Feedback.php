<?php

namespace common\models\site;

use common\models\access\User;
use common\models\Model;
use common\service\Mail;

/**
 * Обратная связь
 *
 * Class Review
 * @package common\models\site
 * @property int $id
 * @property int $user_id [int(11)]
 * @property int $status [tinyint(6)]
 * @property string $email
 * @property string $phone
 * @property string $text
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property User $user
 */
class Feedback extends Model
{
    public const STATUS_NEW = 1;
    public const STATUS_VIEW = 2;

    public static function tableName()
    {
        return '{{%site_feedback}}';
    }

    public function rules()
    {
        return [
            [
                ['user_id'],
                'integer',
            ],
            [
                ['email'],
                'email',
            ],
            [
                ['text'],
                'string',
            ],
            [
                ['text'],
                'filter',
                'filter' => function ($value) {
                    if (!empty($value)) {
                        $value = strip_tags($value, '<br>');
                        $value = str_replace('<br>', PHP_EOL, $value);
                        $value = str_replace('<br/>', PHP_EOL, $value);
                    }

                    return $value;
                },
            ],
            [
                'phone',
                'filter',
                'filter' => function ($value) {
                    $value = preg_replace('/[^0-9+]/', '', $value);
                    if (mb_strlen($value) === 10 && $value[0] === '9') {
                        $value = '+7' . $value;
                    }
                    if ($value && $value[0] === '8') {
                        $value[0] = 7;
                    }
                    if ($value && $value[0] === '7') {
                        $value = '+' . $value;
                    }
                    if (mb_strlen($value) <= 8) {
                        return null;
                    }

                    return $value;
                },
            ],
            [
                ['phone'],
                'string',
                'max' => 20,
            ],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Клиент',
            'status' => 'Статус',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'text' => 'Текс',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id'])->where([]);
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_VIEW => 'Просмотренный',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            Mail::sendFeedback($this);
        }
    }
}

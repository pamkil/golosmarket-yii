<?php

namespace common\service\collection\enum;

class CollectionStatusEnum
{
    public const ACTIVE = 1;
    public const NOT_ACTIVE = 0;
}

<?php

namespace admin\models\content;

use common\models\content\Page;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class PageSearch extends Model
{
    public $path;
    public $code;
    public $title;
    public $description;
    public $h1;
    public $keywords;
    public $body;

    public function rules()
    {
        return [
            [
                ['path', 'code', 'title', 'description', 'h1', 'keywords', 'body',],
                'string'
            ],
//            [
//                ['active',],
//                'boolean'
//            ],
        ];
    }

    public function search($params)
    {
        $query = Page::find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'title', 'path', 'code', 'title', 'description', 'h1', 'keywords',
                    ]
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $dataProvider->sort->defaultOrder = ['path' => SORT_ASC,];

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'AND',
                ['like', 'path', $this->path],
                ['like', 'code', $this->code],
                ['like', 'title', $this->title],
                ['like', 'description', $this->description],
                ['like', 'h1', $this->h1],
                ['like', 'keywords', $this->keywords],
                ['like', 'body', $this->body],
            ]
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
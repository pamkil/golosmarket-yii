<?php

namespace api\modules\v1\controllers;

use api\modules\v1\actions\notification\Read;
use common\models\user\Notification;
use yii\helpers\ArrayHelper;
use yii\rest\OptionsAction;

/**
 * @SWG\Tag(
 *   name="Notify",
 *   description="Notification"
 * )
 */
class NotifyController extends RestController
{
    public $modelClass = Notification::class;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'authenticator' => [
                    'optional' => ['read'],
                    'except' => ['options'],
                ],
            ]
        );
    }

    public function checkAccess($action, $model = null, $params = [])
    {
    }

    public function actions()
    {
        return [
            'read' => [
                'class' => Read::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
            'options' => [
                'class' => OptionsAction::class,
            ],
        ];
    }
}
<?php

namespace api\modules\v1\actions\user;

use api\modules\v1\actions\common\UpdateAction;
use api\modules\v1\models\records\announcer\ScheduleWork;
use api\modules\v1\models\records\auth\User;
use Yii;
use yii\web\BadRequestHttpException;

class Schedule extends UpdateAction
{
    /**
     * @OA\Put(
     *   path="/user/schedule",
     *   summary="Update . Обновление",
     *   tags={"User"},
     *   security={{"BearerAuth": {}}},
     *   @OA\Response(
     *     response="200",
     *     description="Объект пользователя",
     *     @OA\JsonContent(ref="#/components/schemas/User")
     *   )
     * )
     * @param int $id
     *
     * @return \yii\web\IdentityInterface|null
     * @throws \yii\web\NotFoundHttpException
     * @throws BadRequestHttpException
     */
    public function run($id = 0)
    {
        /* @var $model User */
        $model = $this->findModel((int)Yii::$app->user->id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }
        if (!$model->getIsAnnouncer()) {
            throw new BadRequestHttpException('Don`t use for current user');
        }
        $scheduleModel = $model->scheduleWork ?? new ScheduleWork();
        $scheduleModel->user_id = Yii::$app->user->id;

        $postData = Yii::$app->request->post();
        $transaction = Yii::$app->db->beginTransaction();
        if (isset($postData['undefined'])) {
            $postData['type'] = $postData['undefined'] ? ScheduleWork::TYPE_UNDEFINED : ScheduleWork::TYPE_WEEK;
        }
        try {
            $scheduleModel->load($postData, '');

            if ($scheduleModel->save()) {
                $transaction->commit();
                $model->refresh();

                return Yii::$app->user->identity ?? null;
            }
            throw new BadRequestHttpException($scheduleModel->errors);
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }
}

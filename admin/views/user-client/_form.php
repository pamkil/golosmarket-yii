<?php

use common\models\access\User;
use kartik\widgets\SwitchInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var User $model
 */
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data' // important
    ]
]) ?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <?= Html::submitButton(
            '<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord ? 'Создать' : 'Сохранить'),
            ['class' => 'btn btn-success']
        ); ?>
        <?php if (!$model->isNewRecord) { ?>
            <?= Html::a(
                '<i class="fa fa-times"></i>&nbsp;Удалить',
                ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'title' => 'Удалить',
                    'data-confirm' => 'Вы уверены что хотите удалить?',
                    'data-method' => 'post'
                ]
            ); ?>
        <?php } ?>
    </div>
    <div class="box-body">
        <?= $form->field($model, 'type')->dropDownList(User::getTypes(), ['disabled' => true]); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'status')->dropDownList(User::getStatuses()); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'blocked_at')
                    ->widget(\kartik\datetime\DateTimePicker::class, [
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'format' => 'php:d.m.Y H:i:s',
                            'todayHighlight' => true,
                            'language' => 'ru',
                        ],
                        'options' => ['autocomplete' => 'off'],
                    ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'firstname'); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'lastname'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'email'); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'phone'); ?>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

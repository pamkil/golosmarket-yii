<?php

namespace api\modules\v1\models\records\user;


use Carbon\Carbon;

/**
 * @OA\Schema(required={"name"}, title="Чат")
 */
class ListChat extends Chat
{

    /**
     * @OA\Property(property="id", type="integer", description="ИД диктора")
     * @OA\Property(property="announcer_id", type="integer", description="ИД диктора")
     * @OA\Property(property="client_id", type="integer", description="ИД клиента")
     * @OA\Property(property="file_id", type="integer", description="ИД прикрепленного файла")
     * @OA\Property(property="text", type="string", description="URL фото диктора")
     * @OA\Property(property="is_read", type="boolean", description="Флаг прочитано сообщения")
     * @OA\Property(property="is_send", type="boolean", description="Фдаг доставлен")
     * @OA\Property(property="is_edit", type="boolean", description="Отредактирован")
     * @OA\Property(property="created_at", type="boolean", description="Время создания")
     * @OA\Property(property="updated_at", type="boolean", description="Время обновления")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'is_edit',
            'id',
            'avatar' => 'avatar',
            'name' => 'name',
            'isOnline',
            'unread' => 'quantityUnreadMessages',
            'deleteQuantity' => 'deleteQuantityMessages',
            'text',
            'opponentId' => 'opponentId',
            'isWriteOpponent' => 'isWriteOpponent',
            'createdAt' => 'createdAt',
            'updatedAt' => 'updatedAt',
        ];
    }

    public function getQuantityUnreadMessages()
    {
        return self::getUnread($this->getOpponentId());
    }

    public function getDeleteQuantityMessages()
    {
        return self::getDeletes($this->getOpponentId());
    }

    public static function getDeletes(int $opponentId)
    {
        $fieldSelf = self::isAnnouncer() ? 'announcer_id' : 'client_id';
        $fieldOpponent = self::isAnnouncer() ? 'client_id' : 'announcer_id';

        $result = (int)self::find()
            ->where([
                $fieldSelf => \Yii::$app->user->getId(),
                $fieldOpponent => $opponentId,
                'is_write_client' => self::isAnnouncer(),
            ])
            ->andWhere(['>', 'deleted_at', 0])
            ->count();

        $countUpdate = 0;
        $updateBill = Bill::find()
            ->select('updated_at')
            ->where([
                'AND',
                [$fieldSelf => \Yii::$app->user->getId()],
                [$fieldOpponent => $opponentId],
            ])
            ->orderBy(['updated_at' => SORT_DESC])
            ->scalar();
        if ($updateBill) {
            try {
                $countUpdate = (int)Carbon::parse($updateBill)->timestamp;
            } catch (\Exception $exception) {

            }
        }

        return $result + $countUpdate;
    }
}
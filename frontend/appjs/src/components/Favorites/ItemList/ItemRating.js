import React from 'react';
import {ReactComponent as Star} from "../../../images/star.svg";

const ItemRating = ({
                      stars
                      // quantityStarts,
                    }) => {
  return (
    stars && stars > 0
      ? <div className="item__rating mobile-no">
        <span>{stars && stars > 0 ? <Star className="item__rating-icon"/> : ''}</span>
        <span className="item__rating-count">{stars && stars > 0 ? stars : ''}
          {/*<span className="cntrat">({quantityStarts})</span>*/}
                    </span>
      </div>
      : null
  )
}

export default ItemRating

<?php

namespace common\models\announcer;

use common\models\access\User;
use common\models\Model;

/**
 * Пол
 *
 * @OA\Schema(title="Пол")
 *
 * Class Gender
 * @package common\models\announcer
 *
 * @property int $id
 * @property string $name
 * @property User[] $announcers
 * @property bool $active [tinyint(1)]
 * @property int $position [int(11)]
 * @property string $code [varchar(25)]
 * @property string $menu_title [varchar(255)]
 */
class Gender extends Model
{
    public static function tableName()
    {
        return '{{%user_announcer_genders}}';
    }

    public function rules()
    {
        return [
            [
                ['name', 'code'],
                'unique'
            ],
            [
                ['name', 'menu_title', 'code'],
                'string'
            ],
            [
                ['position'],
                'number'
            ],
            [
                ['active'],
                'boolean'
            ],
            [
                ['code', 'name', 'menu_title'],
                'required'
            ],
        ];
    }

    public function behaviors()
    {
        $return = parent::behaviors();

        unset($return['timestamp']);

        return $return;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'active' => 'Активность',
        ];
    }

    public function getAnnouncers()
    {
        return $this->hasMany(User::class, ['id' => 'announcer_id'])
            ->via('{{%user_announcer_2_genders}}', ['gender_id' => 'id']);
    }

    public static function getAll()
    {
        return static::find()
            ->select(['name', 'id'])
            ->where(['active' => true])
            ->indexBy(['id'])
            ->column();
    }
}

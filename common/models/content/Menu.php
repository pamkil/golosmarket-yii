<?php

namespace common\models\content;

use common\models\access\User;
use common\models\Model;

/**
 * Автор публикаций
 *
 * Class Menu
 * @package common\models\content
 *
 * @property int $id
 * @property int $type_id
 * @property string $title
 * @property string $path
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property int $deleted_at [timestamp]
 */
class Menu extends Model
{
    public const TYPE_TOP = 1;
    public const TYPE_BOTTOM = 2;

    public static function tableName()
    {
        return '{{%content_menu}}';
    }

    public function rules()
    {
        return [
            [
                ['title', 'path'],
                'string'
            ],
            [
                ['type_id',],
                'integer'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Тип',
            'parent_id' => 'Родительский элемент',
            'title' => 'Наименование',
            'path' => 'Путь',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }

    public static function getTopMenu()
    {
        $menu = static::find()
            ->where([
                'type_id' => static::TYPE_TOP
            ])
            ->orderBy(['parent_id' => SORT_ASC, 'position' => SORT_ASC])
            ->asArray()
            ->all();

        $new = [];
        foreach ($menu as $item) {
            $itemN = [
                'url' => $item['path'],
                'label' => $item['title'],
                'items' => $new[$item['id']]['items'] ?? [],
            ];

            if (!empty($item['parent_id'])) {
                $new[$item['parent_id']]['url'] = null;
                $new[$item['parent_id']]['options'] = ['class' => 'has-submenu'];
                $new[$item['parent_id']]['template'] = '<div class="arrowhead">
                        <div class="arrowhead-left"></div>
                        <div class="arrowhead-right"></div>
                    </div>
                    <span>{label}</span>';
                $new[$item['parent_id']]['submenuTemplate'] = '<ul class="info-dropdown">{items}</ul>';
                $new[$item['parent_id']]['items'][] = $itemN;
            } else {
                $new[$item['id']] = $itemN;
            }
        }

        return $new;
    }

    public static function getTypes()
    {
        return [
            self::TYPE_TOP => 'Верхнее меню',
            self::TYPE_BOTTOM => 'Нижнее меню',
        ];
    }
}

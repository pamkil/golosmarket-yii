<?php

namespace api\modules\v1\actions\collection;

use api\modules\v1\models\records\announcer\AnnouncerSound;
use common\models\content\CollectionSound;
use common\repositories\contract\CollectionSoundRepositoryContract;
use yii\rest\Action;

/**
 * TODO оформить описание
 * @OA\Get(
 *      path="/collection/sound",
 *      summary="List of collection sounds",
 *      tags={"Collection"},
 *      @OA\Parameter(
 *          name="collection-id",
 *          in="query",
 *          description="Collection id",
 *          required=false,
 *          @OA\Schema(
 *            type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="depdrop_all_params",
 *          description="announcer id",
 *          required=false,
 *          @OA\Schema(
 *            type="array"
 *          )
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="ok",
 *          @OA\JsonContent(
 *              type="array",
 *              @OA\Items(ref="#/components/schemas/CollectionSounds")
 *          )
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */
class Sounds extends Action
{
    public $modelClass = AnnouncerSound::class;
    private CollectionSoundRepositoryContract $collectionSoundRepository;

    public function __construct(
        $id,
        $controller,
        CollectionSoundRepositoryContract $collectionSoundRepository,
        $config = []
    )
    {
        parent::__construct($id, $controller, $config);
        $this->collectionSoundRepository = $collectionSoundRepository;
    }


    public function run()
    {
        $collectionId = (int) \Yii::$app->request->getQueryParam('collection-id');
        $announcerId = (int) \Yii::$app->request->getBodyParam('depdrop_parents')[0];
        $sounds = AnnouncerSound::findAll(['announcer_id' => $announcerId]);

        if (0 !== $announcerId) {
            $result = ['selected' => ''];
            foreach ($sounds as $sound) {
                $fileId = $sound->sound->id;
                $result['output'][] = [
                    'id' => $fileId,
                    'name' => $sound->sound->name
                ];

                if (0 !== $collectionId) {
                    $collectionSoundExists = $this->collectionSoundRepository->existsById($collectionId, $fileId);
                    if ($collectionSoundExists) {
                        $result['selected'] = $fileId;
                    }
                }
            }

            return $result;
        }

        return ['output' => '', 'selected' => ''];
    }
}

<?php

use yii\db\Migration;

/**
 * Class m200717_111832_add_expired_to_file
 */
class m200717_111832_add_expired_to_file extends Migration
{
    private $file = '{{%file}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->file,
            'expired',
            $this->dateTime()->null()->after('default')
                ->comment('Удалять после этого времени')
        );
        $this->addColumn($this->file, 'is_delete', $this->boolean()->notNull()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->file, 'expired');
        $this->dropColumn($this->file, 'is_delete');
    }
}

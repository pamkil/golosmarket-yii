import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './Loader.css';

const Loader = ({ size = 'm' }) => (
    <div
        className={
            cx({
                loader: true,
                [`loader--${size}`]: !!size,
            })
        }
    />
)

Loader.propTypes = {
    size: PropTypes.oneOf(['s', 'm', 'l'])
}

export default Loader;
<?php

namespace api\modules\v1\models\records\announcer;

use api\modules\v1\models\records\auth\User;
use api\modules\v1\models\records\file\File;
use Carbon\Carbon;
use common\models\announcer\Age;
use common\models\announcer\Presentation;
use common\models\user\Favorite;
use common\models\user\Review;
use DateInterval;
use stdClass;
use Yii;
use yii\db\Expression;

/**
 * @OA\Schema(required={"name"}, title="Диктор")
 */
class Announcer extends \common\models\announcer\Announcer
{
    public static $presentationCode;
    public $filterCost = 'cost';

    /**
     * @OA\Property(property="id", type="integer", description="ИД диктора")
     * @OA\Property(property="avatar", type="string", description="URL фото диктора")
     * @OA\Property(property="firstname", type="string", description="Имя диктора")
     * @OA\Property(property="lastname", type="string", description="Фамилия диктора")
     * @OA\Property(property="timeText", type="string", description="Время до окончания/начала работы, текстом")
     * @OA\Property(property="stars", type="string", description="")
     * @OA\Property(property="quantityStarts", type="string", description="Суммарная оценка по отзыву")
     * @OA\Property(property="cost", type="integer", description="Цена")
     * @OA\Property(property="costBy", type="integer", description="Тип цены")
     * @OA\Property(property="isOnline", type="boolean", description="Диктор онлайн")
     * @OA\Property(property="ifBefore", type="boolean", description="Флаг времен был - противополжный онлайн")
     * @OA\Property(property="isFavorite", type="boolean", description="")
     * @OA\Property(property="review", type="boolean", description="")
     * @OA\Property(property="reviews", type="boolean", description="")
     * @OA\Property(property="sound", type="boolean", description="")
     * @OA\Property(property="quantityFavorite", type="boolean", description="")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'id' => 'user_id',
            'avatar' => 'avatarView',
            'firstname',
            'lastname',
            'timeText',
            'stars' => 'starsView',
            'quantityStarts',
            'cost' => 'costFilter',
            'costBy',
            'isOnline',
            'isBefore' => 'isBeforeView',
            'isFavorite' => 'isFavoriteView',
            'review' => 'reviewText',
            'reviews' => 'reviewsText',
            'sound' => 'soundView',
            'quantityFavorite',
            'info'
        ];
    }

    public function getCreatedAt()
    {
        if (empty($this->updated_at)) {
            return null;
        }

        return Carbon::parse($this->created_at)->toIso8601String();
    }

    public function getUpdatedAt()
    {
        if (empty($this->updated_at)) {
            return null;
        }

        return Carbon::parse($this->updated_at)->toIso8601String();
    }

    public function getIsBeforeView(): bool
    {
        return !$this->getIsOnline();
    }

    public function getStarsView(): ?float
    {
        return round($this->user->rating, 1);
    }

    public function getQuantityFavorite()
    {
        return $this->user->getFavoritesCount();
    }

    public function getQuantityStarts()
    {
        return $this->user->quantity_reviews;
    }

    public function getCostFilter()
    {
        return $this->{$this->filterCost};
    }

    public function getCostBy()
    {
        return $this->filterCost;
    }

    public function getIsFavoriteView()
    {
        return $this->user->isFavorite;
    }

    public function getIsOnline()
    {
        if (!$this->scheduleWork) {
            return false;
        }

        [$isOnline] = $this->scheduleWork->getIsOnline($this->user);

        return !!$isOnline;
    }

    public function getTimeText()
    {
        if (!$this->scheduleWork) {
            return 'Недоступен';
        }

        /** @var DateInterval $diff */
        [$isOnline, $diff] = $this->scheduleWork->getIsOnline($this->user);

        return ScheduleWork::getTimeOnlineString($isOnline, $diff);
    }

    public function getSoundView()
    {
        return $this->sounds ? $this->sounds : [];
    }

    public function getAvatarView()
    {
        return $this->user
            ? $this->user->getPhotoUrl()
            : File::getEmptyAvatarUrl();
    }

    public function getReviewText()
    {
        return $this->review && $this->review->client
            ? [
                'text' => $this->review->text,
                'username' => $this->review->client->firstname,
                'photo' => $this->review->client->getPhotoUrl(),
                'stars' => $this->review->rating,
                'clientId' => $this->review->client_id,
            ]
            : new stdClass();
    }

    public function getReviewsText()
    {
        $results = [];
        foreach ($this->reviews as $review) {
            if (count($results) >= 2) {
                break;
            }
            $results[] = $review && $review->client
                ? [
                    'text' => $review->text,
                    'username' => $review->client->firstname,
                    'photo' => $review->client->getPhotoUrl(),
                    'stars' => $review->rating,
                    'clientId' => $review->client_id,
                ]
                : new stdClass();
        }

        return $results;
    }

    public function getFirstname()
    {
        return $this->user->firstname;
    }

    public function getLastname()
    {
        $lastName = $this->user->lastname ?? '';
        $lastName = mb_substr($lastName, 0, 1);

        return $lastName;

    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getReview()
    {
        return $this->hasOne(Review::class, ['announcer_id' => 'user_id'])
            ->andWhere(['<>', 'text', ''])
            ->orderBy(['created_at' => SORT_DESC]);
    }

    public function getReviews()
    {
        return $this->hasMany(Review::class, ['announcer_id' => 'user_id']);
    }

    public function getFavorite()
    {
        $userId = -1;
        if (Yii::$app->user && Yii::$app->user->identity) {
            $userId = Yii::$app->user->id;
        }

        return $this->hasOne(Favorite::class, ['announcer_id' => 'user_id'])
            ->andOnCondition(['client_id' => $userId]);
    }

    public function getPhotos()
    {
        return $this->hasMany(File::class, ['id' => 'photo_id'])
            ->viaTable('{{%user_announcer_2_photos}}', ['announcer_id' => 'id']);
    }

    public function getSounds()
    {
        $queryPresentation = null;
        if (self::$presentationCode) {
            $queryPresentation = Presentation::find()
                ->select('id')
                ->where(['code' => self::$presentationCode]);
        }

        return $this->hasMany(AnnouncerSound::class, ['announcer_id' => 'id'])
            ->alias('uas')
            ->filterWhere(['presentation_id' => $queryPresentation])
            ->leftJoin(['uap' => Presentation::tableName()], 'uap.id = uas.presentation_id')
            ->leftJoin(['uag' => Age::tableName()], 'uag.id = uas.age_id')
            ->orderBy([
                new Expression('-uap.position DESC'),
                new Expression('-uag.position DESC'),
            ]);
    }

    public function getSound()
    {
        return $this->hasOne(File::class, ['id' => 'sound_id'])
            ->viaTable('{{%user_announcer_2_sound}}', ['announcer_id' => 'id'])
            ->orderBy(['default' => SORT_DESC]);
    }

    public function getScheduleWork()
    {
        return $this->hasOne(ScheduleWork::class, ['user_id' => 'user_id'])->inverseOf('announcer');
    }
}

<?php

namespace api\modules\v1\controllers;

use yii\helpers\ArrayHelper;
use Yii;
use yii\helpers\Json;
use Firebase\JWT\JWT;
use common\models\access\User;
use common\service\chat\ChatService;
use common\service\chat\dto\CreateMessageDto;

class TelegramController extends RestController
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'authenticator' => [
                'except' => ['webhook', 'options']
                ]
            ]);
        }
        
    public function actionWebhook($token)
    {
        $params = array_merge(require(__DIR__ . '/../../../config/params-local.php'));

        if ($token !== $params['telegram-webhook-secret'])
        {
            return ['ok' => true];
        }

        $content = Yii::$app->request->getRawBody();
        $update = Json::decode($content, true);

        if (isset($update['message']) && str_starts_with($update['message']['text'], "/start"))
        {
            $telegram_start_id = substr($update['message']['text'], 7);

            if (!$telegram_start_id) {
                return ['ok' => true];
            }

            User::updateAll(['telegram_chat_id' => $update['message']['chat']['id']], ['telegram_start_id' => $telegram_start_id, 'notify_telegram' => 1]);

            Yii::$app->telegram->sendMessage([
                'chat_id' => $update['message']['chat']['id'],
                'text' => '🥳 Все готово! Как только вам напишут на Голосмаркет, мы сразу пришлем сюда уведомление с текстом сообщения.',
            ]);
        }
        else if (isset($update['callback_query']))
        {
            $data = Json::decode($update['callback_query']['data']);

            $createMessageDto = new CreateMessageDto([
                'userId' => $data['sender_id'],
                'text' => $data['text'],
                'senderId' => $data['id'],
            ]);

            $service = Yii::$container->get(ChatService::class);
            $service->createMessage($createMessageDto);

            Yii::$app->telegram->sendMessage([
                'chat_id' => $update['callback_query']['from']['id'],
                'text' => "Вы ответили\n\n" . $data['text'],
            ]);
        }
        else
        {   
            Yii::$app->telegram->sendMessage([
                'chat_id' => $update['message']['chat']['id'],
                'text' => '🤖 Этот бот только для получения уведомлений. Для ответа, перейдите на golosmarket.ru',
            ]);
        }
        
        return ['ok' => true];
    }
}

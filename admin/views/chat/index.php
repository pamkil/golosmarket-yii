<?php

use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use admin\models\user\ChatSearch;


/**
 * @var ActiveDataProvider $dataProvider
 * @var ChatSearch $searchModel
 * @var ActiveDataProvider $dataMessagesProvider
 */

$count = 1;
if (isset($dataMessagesProvider)) {
    $count = 2;
}

?>

<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-<?= $count === 1 ? 12 : 6 ?>">

                <?= GridView::widget(
                    [
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => "{items}\n{summary}\n{pager}",
                        'rowOptions' => function ($model, $key, $index, $grid) {
                            if ($key == Yii::$app->request->get('id')) {
                                return ['class' => 'active success'];
                            }

                            return [];
                        },
                        'columns' => [
                            [
                                'label' => 'Диктор',
                                'attribute' => 'announcer_id',
                                'content' => function ($bill) {
                                    if (!$bill->announcer) {
                                        return '';
                                    }

                                    return "<span title='$bill->announcer_id'>{$bill->announcer->lastname} {$bill->announcer->firstname} ({$bill->announcer->email})</span>";
                                },
                            ],
                            [
                                'label' => 'Заказчик',
                                'attribute' => 'client_id',
                                'content' => function ($bill) {
                                    if (!$bill->client) {
                                        return '';
                                    }

                                    return "<span title='$bill->client_id'>{$bill->client->lastname} {$bill->client->firstname} ({$bill->client->email})</span>";
                                },
                            ],
                            'created_at:datetime',

                            [
                                'class' => \yii\grid\ActionColumn::class,
                                'template' => '{view}',
                                'urlCreator' => function ($action, $model, $key, $index, $column) {
                                    $params = is_array($key) ? $key : ['id' => (string) $key];
                                    $params[0] = $column->controller ? $column->controller . '/' . $action : $action;
                                    $params['page'] = Yii::$app->request->getQueryParam('page', 1);

                                    return Url::toRoute($params);
                                },
                                'buttonOptions' => ['class' => 'margin']
                            ],
                        ]
                    ]
                ); ?>
            </div>
            <?php if ($count === 2) : ?>
                <div class="col-md-6">
                    <div class="box box-success direct-chat direct-chat-danger">
                        <div class="box-body">
                            <?= \yii\widgets\ListView::widget([
                                'dataProvider' => $dataMessagesProvider,
                                'options' => ['class' => ' direct-chat-messages123'],
                                'itemView' => '_message',
                                'itemOptions' => ['class' => '']
                            ]); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_announcer_photos}}`.
 */
class m191114_165412_create_photo_table extends Migration
{
    private $file = '{{%file}}';
    private $announcer2photos = '{{%user_announcer_2_photos}}';
    private $announcer = '{{%user_announcer}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->file, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'path' => $this->string(),
            'filename' => $this->string(),
            'ext' => $this->string(10),
            'size' => $this->integer(),
            'type' => $this->string(100),
        ], $tableOptions);

        $this->createTable($this->announcer2photos, [
            'announcer_id' => $this->integer(),
            'photo_id' => $this->integer(),
            'PRIMARY KEY (announcer_id, photo_id)'
        ], $tableOptions);

        $this->createIndex(
            'IX-user_announcer_2_photos-announcer_id',
            $this->announcer2photos,
            'announcer_id'
        );
        $this->addForeignKey(
            'FK-user_announcer_2_photos-announcer_id',
            $this->announcer2photos,
            'announcer_id',
            $this->announcer,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('IX-user_announcer_2_photos-photo_id', $this->announcer2photos, 'photo_id');
        $this->addForeignKey(
            'FK-user_announcer_2_photos-photo_id',
            $this->announcer2photos,
            'photo_id',
            $this->file,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->announcer2photos);
        $this->dropTable($this->file);
    }
}

import React, {useEffect, useRef, useState} from 'react';

import IconQu from '../../../images/icon_question.png'
import ReactDOM from "react-dom";

const SwitchButtonMulti = ({
                             children,
                             title = null,
                             activeFilter = '',
                             info = ''
                           }) => {

  const [open, setOpen] = useState(false);
  const dropdownRef = useRef();
  const buttonRef = useRef();

  const handleClickOutside = (event) => {
    const domNode = ReactDOM.findDOMNode(dropdownRef.current);
    const isInnerClick = domNode && domNode.contains(event.target);
    const ignoreClick = event.target.isEqualNode(buttonRef.current);

    if (!isInnerClick && !ignoreClick && open) {
      setOpen(false);
    }
  };

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true);

    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    }
  });

  return (
    <div className={'switch-holder'}>
      {
        title &&
        <div className="head">
          {title}
          {
            info ?
              <span className="tooltip">
                <span className="icon">
                  <img src={IconQu} alt="icon question"/>
                  <span>{info}</span>
                </span>
              </span>
              : null
          }
        </div>
      }
      <div className={'sorter-menu'}>
        <div ref={buttonRef} title={activeFilter || ''} className={"sorter-value" + (open ? ' __open' : '')}
             onClick={() => {
               setOpen(!open);
             }}>
          <span>{activeFilter || ''}</span>
          <div className="arrowhead">
            <div className="arrowhead-left"/>
            <div className="arrowhead-right"/>
          </div>
        </div>
        {
          open ?
            <div ref={dropdownRef} className="sorter-dropdown">
              {children}
            </div>
            : null
        }
      </div>
    </div>
  );
};

SwitchButtonMulti.propTypes = {};

export default SwitchButtonMulti;

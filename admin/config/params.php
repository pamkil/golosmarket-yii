<?php

return [
    'fileinput_options' => [
        'pluginOptions' => [
            'showCaption' => false,
            'showUpload' => false,
            'showClose' => false,
            'layoutTemplates' => [
                'actionUpload' => ''
            ]
        ],
        'pluginEvents' => [
            'fileclear' => "function(){ var that = $(this), name = that.attr('name'), form = that.closest('form'); form.append('<input type=\"hidden\" name=\"delete[' + name + ']\" value=\"1\" />') }"
        ]
    ],
    'fileinput_options_ajax' => [
        'pluginOptions' => [
            'showCaption' => false,
            'showClose' => false,
            'uploadAsync' => true,
            'showUpload' => true
        ],
        'pluginEvents' => [
            'fileclear' => "function(){ var that = $(this), name = that.attr('name'), form = that.closest('form'); form.append('<input type=\"hidden\" name=\"delete[' + name + ']\" value=\"1\" />') }"
        ]
    ],
    'datewyearpicker_options' => [
        'pluginOptions' => [
            'format' => 'dd.mm'
        ],
        'options' => ['autocomplete' => 'off'],
    ],
    'datepicker_options' => [
        'pluginOptions' => [
            'format' => 'dd.mm.yyyy',
            'todayHighlight' => true
        ],
        'options' => ['autocomplete' => 'off'],
    ],
    'datetimepicker_options' => [
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'dd.mm.yyyy hh:ii:ss',
            'todayHighlight' => true
        ],
        'options' => ['autocomplete' => 'off'],
    ],
    'timepicker_options' => [
        'pluginOptions' => [
            'showSeconds' => false,
            'showMeridian' => false
        ],
        'options' => ['autocomplete' => 'off'],
    ],
    'tinyMceOptions' => [
        'options' => ['rows' => 6, 'class' => 'form-control tinymce-me'],
        'language' => 'ru',
        'clientOptions' => [
            'plugins' => [
                'advlist autolink lists link charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste',
                'image imagetools'
            ],
            'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify ' .
                '| bullist numlist outdent indent | link image',
            'height' => '400',
            'relative_urls' => false,
            'allow_script_urls' => true,
            'extended_valid_elements' => "script[language|type|src],p[style]",
//            'file_browser_callback_types' => 'file image media',
//            'file_browser_callback' => new yii\web\JsExpression('fileBrowserCallback')
        ]
    ],
    'tinyMceOptionsShort' => [
        'options' => ['rows' => 6, 'class' => 'form-control tinymce-me'],
        'language' => 'ru',
        'clientOptions' => [
            'plugins' => [
                'advlist autolink lists link charmap anchor',
                'code fullscreen',
                'insertdatetime media table contextmenu paste',
            ],
            'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify ' .
                ' | bullist numlist outdent indent | link',
            'menubar' => 'edit insert format table tools',
            'height' => '400',
            'relative_urls' => false,
            'allow_script_urls' => true,
            'extended_valid_elements' => "script[language|type|src],p[style]",
            'file_browser_callback_types' => 'file image media',
            'file_browser_callback' => new yii\web\JsExpression('fileBrowserCallback')
        ]
    ],
];

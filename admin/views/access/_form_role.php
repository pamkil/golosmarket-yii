<?
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\select2\Select2;

/** @var array $users */
?>

<? ActiveForm::begin() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="actions btn-set">
                        <?= Html::submitButton('<i class="fa fa-check"></i>&nbsp;' . (!isset($model) ? Yii::t('admin', 'Создать') : Yii::t('admin', 'Сохранить')), ['class' => 'btn btn-success']) ?>
                        <? if (isset($model)) { ?>
                            <?= Html::a('<i class="fa fa-times"></i>&nbsp;' . Yii::t('admin', 'Удалить'), ['delete-role', 'name' => $model->name], [
                                'class' => 'btn btn-danger',
                                'title' => Yii::t('admin', 'Удалить'),
                                'data-confirm' => Yii::t('admin', 'Вы уверены что хотите удалить?'),
                                'data-method' => 'post'
                            ]) ?>
                        <? } ?>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_general" data-toggle="tab"><?= Yii::t('admin', 'Общие') ?></a>
                            </li>
                            <li>
                                <a href="#tab_permissions" data-toggle="tab"><?= Yii::t('admin', 'Доступы') ?></a>
                            </li>
                            <? if (isset($model)) : ?>
                                <li>
                                    <a href="#tab_users" data-toggle="tab"><?= Yii::t('admin', 'Пользователи') ?> </a>
                                </li>
                            <? endif; ?>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_general">
                                <div class="form-group">
                                    <?= Html::label(Yii::t('admin', 'Название роли'), '', ['class' => 'control-label']) ?>
                                    <?= Html::textInput('name', isset($model) ? $model->name : '', ['class' => 'form-control']) ?>
                                </div>
                                <div class="form-group">
                                    <?= Html::label(Yii::t('admin', 'Описание'), '', ['class' => 'control-label']) ?>
                                    <?= Html::textInput('description', isset($model) ? $model->description : '', ['class' => 'form-control']) ?>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_permissions">
                                <?= Html::checkboxList('permissions', isset($role_permit) ? $role_permit : null, $permissions, ['separator' => '<br>']) ?>
                            </div>
                            <? if (isset($model)) : ?>
                                <div class="tab-pane" id="tab_users">
                                    <?= Select2::widget([
                                        'name' => 'users',
                                        'options' => [
                                            'multiple' => true
                                        ],
                                        'value' => isset($users) ? ArrayHelper::map($users, 'id', 'id') : [],
                                        'initValueText' => isset($users) ? ArrayHelper::map($users, 'id', 'name') : [],
                                        'pluginOptions' => [
                                            'ajax' => [
                                                'url' => Url::to(['user/ajax']),
                                                'dataType' => 'json',
                                                'delay' => 250,
                                                'data' => new JsExpression('function(params) { 
                                                    return {q: params.term, contract: true, page: params.page}; 
                                                }'),
                                                'cache' => true
                                            ]
                                        ]
                                    ]) ?>

                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? ActiveForm::end() ?>

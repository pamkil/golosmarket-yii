<?php

namespace common\service;

use common\models\access\User;
use common\models\user\Bill;
use common\models\user\Notification as Notify;
use common\models\user\Review;

class Notification
{
    public static function needReview(Bill $bill)
    {
        $client = $bill['client']['firstname'];
        $announcer = $bill['announcer']['firstname'];

        $text = 'Как вам работать с пользователем';
        $textClient = "$text $announcer";
        $textAnnouncer = "$text $client";

        Notify::newNotify(
            $textClient,
            Notify::TYPE_REVIEW_SEND,
            $bill['id'],
            $bill['client_id']
        );
        Notify::newNotify(
            $textAnnouncer,
            Notify::TYPE_REVIEW_SEND,
            $bill['id'],
            $bill['announcer_id']
        );
    }

    public static function paidBill(Bill $bill)
    {
        $billText = Bill::billToText($bill);
        $textClient = "{$bill['client']['firstname']} оплатил $billText";
        $userId = (!empty(\Yii::$app->user)) ? \Yii::$app->user->getId() : $bill['announcer']['id'];

        Notify::newNotify(
            $textClient,
            Notify::TYPE_BILL,
            $bill['id'],
            $userId
        );
    }

    public static function paidSecondBill(Bill $bill)
    {
        $textClient = "Как вам Голосмаркет? Оставьте отзыв. Будем рады, если посоветуете нас другим людям.";

        Notify::newNotify(
            $textClient,
            Notify::TYPE_REVIEW_SERVICE_CLIENT,
            $bill['id'],
            $bill['client_id']
        );
    }

    public static function newBill(Bill $bill)
    {
        $billText = Bill::billToText($bill);
        $textClient = "{$bill['announcer']['firstname']} выставила Вам $billText";

        Notify::newNotify(
            $textClient,
            Notify::TYPE_BILL,
            $bill['id'],
            $bill['client_id']
        );
    }

    public static function addNewsNotify($db, $id)
    {
        $tableNotify = 'user_notification';
        $text = 'Появилась свежая информация в разделе Новости';

        $otherId = $id;
        $status = Notify::STATUS_NEW;
        $type = Notify::TYPE_NEWS;
        $userType = User::TYPE_ANNOUNCER;

        $db->createCommand("
            INSERT INTO $tableNotify (`user_id`, `other_id`, `status`, `type`, `text`, `created_at`, `updated_at`)
            SELECT `user`.`id`, $otherId, $status, $type, '$text', NOW(), NOW()
            FROM `user`
            WHERE `user`.`type`=$userType
        ")
            ->execute();
    }

    public static function addReviewNotify(Review $review)
    {
        if ($review->type_writer === Review::TYPE_WRITE_CLIENT) {
            $user = $review->client->firstname;
            $userId = $review->announcer_id;
        } else {
            $user = $review->announcer->firstname;
            $userId = $review->client_id;
        }
        $text = "Новый отзыв от пользователя $user";
        $type = Notify::TYPE_REVIEW;
        $otherId = $review->id;

        Notify::newNotify($text, $type, $otherId, $userId);
    }
}
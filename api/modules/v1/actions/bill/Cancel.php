<?php

namespace api\modules\v1\actions\bill;

use api\modules\v1\models\records\user\Bill;
use common\models\access\User;
use Yii;
use yii\rest\Action;
use yii\web\BadRequestHttpException;

/**
 * @OA\Delete(
 *      path="/chat/{id}/bill",
 *      summary="Отменить Последний новый счет с собеседником, ИД собеседника",
 *      tags={"Chat"},
 *      @OA\Response(
 *          response="200",
 *          description="Объект токена",
 *          @OA\JsonContent(ref="#/components/schemas/Chat")
 *      ),
 *      @OA\Response(
 *          response="400",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="401",
 *          description="Unauthorized",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */

/**
 * Class Create
 * @throws BadRequestHttpException
 * @package api\modules\v1\actions\chat
 *
 */
class Cancel extends Action
{
    public function run($id)
    {
        $typeReceiver = User::getType((int)$id);

        if (!$typeReceiver) {
            throw new BadRequestHttpException("Не найден пользователь которму вы пишете");
        }

        if ((int)Yii::$app->user->identity->type === $typeReceiver) {
            throw new BadRequestHttpException("Не можете написать этому пользователю");
        }

        $isAnnouncer = Yii::$app->user->identity->type === User::TYPE_ANNOUNCER;

        if ($isAnnouncer) {
            $announcerId = Yii::$app->user->id;
            $clientId = $id;
        } else {
            $announcerId = $id;
            $clientId = Yii::$app->user->id;
        }

        $bill = Bill::getNew($announcerId, $clientId);
        if (!$bill) {
            throw new BadRequestHttpException("Не найден счет для отмены");
        }
        $transaction = Yii::$app->db->beginTransaction();

        try {
            if ($bill->setCancelStatus($isAnnouncer) && $bill->save()) {
                $transaction->commit();
                $bill->refresh();

                return $bill;
            }
            throw new BadRequestHttpException($bill->errors);
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }
}

<?php

use common\models\access\User;
use kartik\editors\Summernote;
use kartik\widgets\DepDrop;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\content\Collection */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="box">
    <div class="box-body">
        <div class="collection-form">

            <?php $form = ActiveForm::begin([
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
                'validateOnChange' => true,
                'validateOnSubmit' => true,
                'validateOnBlur' => false,
            ]); ?>

            <p><b>Основная информация</b></p>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($model, 'active')->widget(SwitchInput::class) ?>

                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'description')->textarea(); ?>

                            <?= $form->field($model, 'position')->textInput(['type' => 'number', 'min' => 0]) ?>
                        </div>
                        <div class="col-sm-6">
                            <?php
                            $pluginOptions = [
                                'allowedFileExtensions' => ['jpg','gif','png'],
                                'showRemove' => false,
                                'showUpload' => false,
                                'browseClass' => 'btn btn-primary btn-block',
                                'browseLabel' =>  'Выберите изображение',
                                'maxFileSize'=>8000,
                                'maxFileCount' => 1,
                            ];
                            if (!empty($model->image)) {
                                $pluginOptions['initialPreview'] = $model->image->url;
                                $pluginOptions['initialPreviewAsData'] = true;
                                $pluginOptions['initialCaption'] = $model->image->name;
                                $pluginOptions['overwriteInitial'] = false;
                            }
                            ?>
                            <?= $form->field($model, 'image')->widget(FileInput::class, [
                                'options' => [
                                    'multiple' => false,
                                    'accept' => 'image/*'
                                ],
                                'pluginOptions' => $pluginOptions
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>

            <p><b>Список демок</b></p>
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $form->field($model, 'collectionSounds')->widget(MultipleInput::class, [
                        'id' => 'my_id',
                        'min' => 0,
                        'sortable' => true,
                        'addButtonPosition' => MultipleInput::POS_FOOTER,
                        'rowOptions' => [
                            'id' => 'row{multiple_index_my_id}',
                        ],
                        'columns' => [
                            [
                                'name' => 'collection_id',
                                'type' => 'hiddenInput',
                            ],
                            [
                                'title' => 'Диктор',
                                'name' => 'announcer_id',
                                'type' => Select2::class,
                                'options' => [
                                    'options' => [
                                        'placeholder' => 'Выберите диктора...'
                                    ],
                                    'data' => ArrayHelper::map(
                                        User::findAll([
                                            'type' => User::TYPE_ANNOUNCER,
                                            'status' => User::STATUS_ACTIVE
                                        ]),
                                        function (User $user) {
                                            return $user->announcer->id;
                                        },
                                        function (User $user) {
                                            return $user->lastname . ' ' . $user->firstname;
                                        }
                                    ),
                                    'id' => 'announcer-id-{multiple_index_my_id}',
                                ],
                                'headerOptions' => [
                                    'style' => 'width: 30%',
                                ],
                            ],
                            [
                                'title' => 'Демо',
                                'name' => 'sound_id',
                                'type' => DepDrop::class,
                                'options' => [
                                    'type' => DepDrop::TYPE_SELECT2,
                                    'options' => [
                                        'id' => 'sound_id-{multiple_index_my_id}'
                                    ],
                                    'pluginOptions' => [
                                        'initialize' => true,
                                        'depends' => ['announcer-id-{multiple_index_my_id}'],
                                        'placeholder' => 'Выберите демо...',
                                        'url' => Url::to(['../api/v1/collection/sounds', 'collection-id' => $model->id])
                                    ],
                                ],
                                'headerOptions' => [
                                    'style' => 'width: 70%',
                                ],
                            ],
                        ],
                    ])->label(false); ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

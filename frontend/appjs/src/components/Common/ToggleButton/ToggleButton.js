import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import IconQu from "../../../images/icon_question.png";

const ToggleButton = ({ label, labelNoMobile = null, quantity = null, onChange, defaultActive: isActive = false, title = null, info = '' }) => {
    const changeHandler = () => {
        const newActive = !isActive;
        onChange(newActive);
    };
    return (
        <>
            {
                title &&
                <div className="head">
                    { title } {
                    info &&
                    <span className="tooltip">
                            <span className="icon">
                                <img src={ IconQu } alt="icon question"/>
                                <span>{ info }</span>
                            </span>
                        </span>
                }
                </div>
            }
            <button
                className={ classNames({ active: isActive }) }
                onClick={ changeHandler }
            >
                {
                    // isActive && <div className="close-f mobile-no">X</div>
                }
                { label }
                {
                    labelNoMobile
                        ? <span className="mobile-no">{ labelNoMobile }</span>
                        : ''
                }
                {
                    quantity > 0
                        ? <span>{ quantity }</span>
                        : ''
                }
            </button>
        </>
    );
};

ToggleButton.propTypes = {
    label: PropTypes.string,
    onChange: PropTypes.func,
    defaultActive: PropTypes.bool
};

export default ToggleButton;
<?php

namespace api\modules\v1\actions\favorite;

use api\modules\v1\actions\common\ViewAction;
use api\modules\v1\models\records\auth\User;
use common\models\user\Favorite;
use Yii;

/**
 * @OA\Get(
 *      path="/favorite/{id}",
 *      summary="Последний новый счет с собеседником, ИД собеседника",
 *      tags={"Chat"},
 *      @OA\Parameter(
 *          name="id",
 *          in="path",
 *          description="User ID, ИД собеседника",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="ok",
 *          @OA\JsonContent(
 *              type="array",
 *              @OA\Items(ref="#/components/schemas/Bill")
 *          )
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */
class View extends ViewAction
{
    /** @inheritdoc */
    public function run($id)
    {
        $typeReceiver = \common\models\access\User::getType((int)$id);

        if (!$typeReceiver) {
            return false;
        }

        if ((int)Yii::$app->user->identity->type === $typeReceiver) {
            return false;
        }

        $isAnnouncer = Yii::$app->user->identity->type === User::TYPE_ANNOUNCER;

        if ($isAnnouncer) {
            $announcerId = Yii::$app->user->id;
            $clientId = $id;
        } else {
            $announcerId = $id;
            $clientId = Yii::$app->user->id;
        }

        return Favorite::getFavorite($announcerId, $clientId) ? true : false;
    }
}

import React from "react";
import {Field} from "redux-form";
import {requiredField} from "../../data/helpers/Tools";

import {ReactComponent as AlertTriangle} from "../../images/alert-triangle.svg";
import Pause from "../../images/pause.png";
import Play from "../../images/play.png";
import Download from "../../images/download.svg";
import Close from "../../images/close.svg";
import SelectRender from "./SelectRender";

const ProfileSound = (
    member, index, sound, isNewRecord, keyForm, playHandler, removeHandler, file, record,

    // опции для выпадающих списков
    optionLanguages, optionAges, optionGenders, optionPresentations,

    customLang, setCustomLang,

    // списки презентаций пародистов и вокалистов
    parodistListIds, setParodistListIds,
    vocalListIds, setVocalListIds,

    formStates
) => {
    const {url = '', name} = file;

    const langChange = e => {
        if (e.id === 'custom_lang') {
            setCustomLang(customLang.concat(index))
        } else {
            setCustomLang(customLang.filter(f => f !== index))
        }
    }

    const presentationChange = e => {
        switch (e.id) {
            case 'parodist':
                setParodistListIds(parodistListIds.concat(index));
                setVocalListIds(vocalListIds.filter(soundIndex => soundIndex !== index));
                break;
            case 'vocals':
                setParodistListIds(parodistListIds.filter(soundIndex => soundIndex !== index));
                setVocalListIds(vocalListIds.concat(index));
                break;
            default:
                setParodistListIds(parodistListIds.filter(soundIndex => soundIndex !== index));
                setVocalListIds(vocalListIds.filter(soundIndex => soundIndex !== index));
                break;
        }
    }

    let warningAlert = null, warningClass = '';
    if (formStates !== undefined && formStates.announcer &&
        (
            (parodistListIds.indexOf(index) > -1 && formStates.announcer.cost_parody <= 0) ||
            (vocalListIds.indexOf(index) > -1 && formStates.announcer.cost_vocal <= 0)
        )
    ) {
        warningAlert = <span className={'item-play__warning'}>Ниже укажите стоимость услуги</span>;
        warningClass = ' __warning';
    } else if (!record.lang_id) {
        warningAlert = <span className={'item-play__warning'}>Обязательно укажите язык, на котором сделана запись</span>
        warningClass = ' __warning';
    }

    return <div className="item-play">
        {warningAlert}

        <div className={"item-play__button item-play__cell __gender" + warningClass}>
              <div className="item-play__cell-1">
                  <AlertTriangle/>
                  <button
                      className="play-track"
                      onClick={() => playHandler(record)}
                      type="button"
                  >
                      <img src={sound && sound.url === record.url ? Pause : Play} alt=""/>
                  </button>
              </div>
              <div className="item-play__group">
                  <Field

                      className="item-play__gender"
                      component={SelectRender}
                      name={`${member}.gender_id`}
                      optionsToRender={optionGenders}
                      placeholder="Укажите пол"
                      isMulti={false}
                  />
              </div>
        </div>
        <div className={'item-play__cell __lang'}>
            <Field
                className="item-play__presentation"
                component={SelectRender}
                changeCallback={langChange}
                name={`${member}.lang_id`}
                optionsToRender={optionLanguages}
                placeholder="Введите язык демо"
                isMulti={false}
            />
            {customLang.length && customLang.indexOf(index) > -1 ? <div className="item-play__extra">
                <Field
                    name={`${member}.lang_name`}
                    type="text"
                    className={"input-v2"}
                    placeholder={"Язык"}
                    component="input"
                    label="First Name"
                />
            </div> : null}
        </div>
        <div className={'item-play__cell __theme'}>
            <div className="item-play__group">
                <Field
                    className="item-play__presentation"
                    component={SelectRender}
                    changeCallback={presentationChange}
                    name={`${member}.presentation_id`}
                    optionsToRender={optionPresentations}
                    placeholder="Что в записи?"
                    isMulti={false}
                />
                {
                    parodistListIds.length && parodistListIds.indexOf(index) > -1
                        ? <div className="item-play__extra">
                            <Field
                                name={`${member}.parodist_name`}
                                type="text"
                                placeholder={"Укажите персонажа"}
                                className={"input-v2"}
                                component="input"
                                label="Parodist Name"
                                validate={[requiredField]}
                            />
                        </div>
                        : null
                }
            </div>
        </div>
        <div className={'item-play__cell __age'}>
            <Field
                className="item-play__presentation"
                component={SelectRender}
                name={`${member}.age_id`}
                defaultValue={{id: 'any', name: 'Любой'}}
                optionsToRender={optionAges.concat({id: 'any', name: 'Любой'})}
                placeholder="Возраст звучания"
                isMulti={false}
            />
        </div>
        <div className={'item-play__cell __controls'}>
            <div className="item-play__track">
                <a download={name} target="_blank" href={url} rel="noopener noreferrer"><img className="item-play__icon" src={Download} alt="Download"/></a>
                <button
                    type="button"
                    className="remove-button"
                    onClick={() => {
                        removeHandler(file, index)
                    }}
                ><img className="item-play__icon item-play__icon--close" src={Close} alt="Close"/></button>
            </div>
        </div>
    </div>
};

export default ProfileSound;

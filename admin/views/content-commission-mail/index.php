<?php

use admin\models\content\CommissionMailForm;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/**
 * @var CommissionMailForm $model
 */
?>

<?php $form = ActiveForm::begin() ?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <div class="text-left">
            Редактировать шаблона для письма о необходимости оплатить комиссию.<br>
<br>
            Тэги которые можно использовать в теле письма:<br>
            <b>{NAME}</b> - имя пользователя<br>
            <b>{COMMISSION}</b> - комиссия сайту<br>
        </div>
        <?= Html::submitButton('<i class="fa fa-check"></i>&nbsp;Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <div class="box-body">
        <?= $form->field($model, 'template')
            ->widget(TinyMce::class, Yii::$app->params['tinyMceOptions'] ?? ''); ?>
    </div>
</div>

<?php ActiveForm::end() ?>

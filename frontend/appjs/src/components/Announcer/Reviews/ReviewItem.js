import React from "react";
import PropTypes from "prop-types";

import guestImg from "../../../images/guest.jpg";
import {ReactComponent as Star} from "../../../images/star_review.svg";
import {ReactComponent as StarEmpty} from "../../../images/star_review_empty.svg";

const starsArray = [1, 2, 3, 4, 5];

const ReviewItem = ({user_id, clientName, rating, text, clientAvatar}) => {
  return (
    <div className="item">
      <a className="icon" href={'/profile/' + user_id}>
        <img src={clientAvatar ? clientAvatar : guestImg} alt={clientName}/>
      </a>
      <div className="name"><a href={'/profile/' + user_id}>{clientName}</a></div>
      <div className="like">
        {
          starsArray.map((item, key) => (
            <span className="greenstar" key={key}>{key < rating ? <Star/> : <StarEmpty/>}</span>
          ))
        }
      </div>
      <div className="desc">{text}</div>
    </div>
  );
};

ReviewItem.propTypes = {
  name: PropTypes.string,
  like: PropTypes.number,
  dislike: PropTypes.number,
  text: PropTypes.string,
  img: PropTypes.string
};

ReviewItem.defaultProps = {
  like: 0,
  dislike: 0
};

export default ReviewItem;

<?php

namespace api\modules\v1\actions\feedback;

use yii\rest\CreateAction;

/**
 * @OA\Post(
 *      path="/feedback",
 *      summary="Добавление в обратную связь",
 *      tags={"Feedback"},
 *      @OA\Response(
 *          response="200",
 *          description="Объект токена",
 *          @OA\JsonContent(ref="#/components/schemas/Feedback")
 *      ),
 *      @OA\Response(
 *          response="400",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="401",
 *          description="Unauthorized",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */

/**
 * Class Create
 * @package api\modules\v1\actions\feedback
 *
 */
class Create extends CreateAction
{
    /**
     * @return \yii\db\ActiveRecord|\yii\db\ActiveRecordInterface
     * @throws \yii\web\ServerErrorHttpException
     */
    public function run()
    {
        $model = parent::run();

        $model->refresh();

        return $model;
    }
}

<?php

namespace common\models\site;

use common\models\Model;

/**
 * Автор публикаций
 *
 * Class Setting
 * @package common\models\site
 *
 * @property int $id
 * @property string $code [varchar(255)]
 * @property string $value [varchar(255)]
 * @property string $description [varchar(1024)]
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property int $deleted_at [timestamp]
 */
class Setting extends Model
{
    public const PERCENT = 'percent';
    public const SMPT_PORT = 'smpt_port';
    public const SMPT_SERVER = 'smpt_server';
    public const SMPT_USERNAME = 'smpt_username';
    public const SMPT_USERPASSWORD = 'smpt_userpassword';
    public const EMAIL_FEEDBACK = 'email_feedback';
    public const YA_MONEY = 'ya_money';
    public const COMMISSION_CARDNUMBER = 'commission_cardnumber';
    public const HOURS_TO_BLOCKED = 'hours_to_blocked';

    public static function tableName()
    {
        return '{{%settings}}';
    }

    public function rules()
    {
        return [
            [
                ['value', 'description', 'code'],
                'string'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Символьный код',
            'value' => 'Значение',
            'description' => 'Описание',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    private static $settings = [];

    /**
     * @param $code
     *
     * @return string|null
     */
    public static function getSetting($code)
    {
        if (empty(self::$settings)) {
            self::$settings = self::find()
                ->select(['value', 'code'])
                ->indexBy('code')
                ->column();
        }

        return self::$settings[$code] ?? null;
    }
}

<?php

namespace admin\models\admin;

use common\models\admin\Plan;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class PlanSearch extends Model
{
    public $date_plan;
    public $value;

    public function rules()
    {
        return [
            [
                ['date_plan',],
                'safe'
            ],
            [
                ['value'],
                'number'
            ],
        ];
    }

    public function search($params)
    {
        $query = Plan::find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'date_plan',
                        'value',
                    ],
                    'defaultOrder' => [
                        'date_plan' => SORT_DESC
                    ],
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            ['date_plan' => $this->date_plan],
            ['value' => $this->value],
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
<?php

namespace api\modules\v1\models\filters\announcer;

use api\modules\v1\models\records\announcer\Announcer;
use api\modules\v1\models\records\announcer\AnnouncerSound;
use api\modules\v1\models\records\auth\User;
use api\modules\v1\models\records\user\Review;
use Carbon\CarbonImmutable;
use common\models\announcer\Age;
use common\models\announcer\Gender;
use common\models\announcer\Presentation;
use common\models\user\Favorite;
use common\models\user\ScheduleWork;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * @OA\Schema(title="Форма запроса для поиска дикторов")
 */
class AnnouncerSearch extends Model
{
    /**
     * @OA\Property(
     *     type="string",
     *     description="показывать стоимость за 30 секунд или за А4",
     *     title="costBy",
     *     enum={"cost", "cost_a4", "cost_vocal", "cost_parodi", "cost_sound_mount", "cost_audio_adv"}
     * )
     *
     * @var string
     */
    public $costBy;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Пол голоса",
     *     title="gender",
     * )
     *
     * @var string
     */
    public $gender;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Возраст голоса",
     *     title="age",
     * )
     *
     * @var string
     */
    public $age;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="цена от",
     *     title="costAt",
     * )
     *
     * @var string
     */
    public $costAt;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="цена до",
     *     title="costTo",
     * )
     *
     * @var string
     */
    public $costTo;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="язык голоса",
     *     title="language",
     * )
     *
     * @var string
     */
    public $language;

    /**
     * @OA\Property(
     *     type="boolean",
     *     description="только онлайн",
     *     title="online",
     *     enum={"0", "1"}
     * )
     *
     * @var string
     */
    public $online;

    /**
     * @OA\Property(
     *     type="boolean",
     *     description="только в избаном",
     *     title="favorite",
     * )
     *
     * @var string
     */
    public $favorite;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="подача голоса",
     *     title="presentation",
     * )
     *
     * @var string
     */
    public $presentation;

    /**
     * @OA\Property(
     *     type="string",
     *     description="сободный запрос",
     *     title="text",
     * )
     *
     * @var string
     */
    public $text;

    /**
     * @OA\Property(
     *     type="boolean",
     *     description="Фото студии",
     *     title="photo",
     * )
     *
     * @var string
     */
    public $photo;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Идентификатор пользователя",
     *     title="id[]",
     * )
     *
     * @var string
     */
    public $id;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Сортировка",
     *     title="sort",
     * )
     *
     * @var string
     */
    public $sort;

    public function rules()
    {
        return [
            [
                ['text'],
                'string',
            ],
            [
                ['gender', 'age', 'presentation', 'language', 'sort'],
                'string',
            ],
            [
                ['costTo', 'costAt'],
                'number',
            ],
            [
                ['favorite', 'online', 'photo'],
                'boolean',
            ],
            [
                ['costBy'],
                'in',
                'range' => ['cost', 'cost_a4', 'cost_parody', 'cost_vocal', 'cost_sound_mount', 'cost_audio_adv']
            ],
            [
                ['id'], 'each', 'rule' => ['integer']
            ]
        ];
    }

    public function formName()
    {
        return '';
    }

    public function search(int $page = 1, int $perPage = 12)
    {
        $defaultParam = Yii::$app->request->get('default');

        if ($defaultParam == "1") {
            $code = Presentation::find()->where(['use_default' => true])->one()->code;
            return $code;
        }

        $perPage = $perPage > 50 ? 50 : $perPage;
        $query = Announcer::find()
            ->alias('user_announcer')
            ->select(['user_announcer.*', 'user.last_visit'])
            ->cache(-1)
            ->with([
                'scheduleWork',
                'review' => function (\yii\db\ActiveQuery $query) {
                    $query->andWhere(['type_writer' => Review::TYPE_WRITE_CLIENT]);
                },
                'review.client',
                'review.client.photo',
                'sounds',
                'sounds.presentation',
                'sounds.sound',
                'user',
                'user.photo',
            ]);
        /**
        select IF (usw.user_id, 1, 0) as isOnline, ua.*
        from user_announcer ua
        left join user_schedule_work usw on ua.user_id = usw.user_id
        and  type = 2 and time_1_at <= now() and time_1_to >= now()
        order by isOnline desc ;
         */

        $day = CarbonImmutable::now()->dayOfWeekIso;
        $expressionNow = new Expression('NOW()');

        $query->addSelect(['IF (usw.user_id, 1, 0) as isOnline'])
            ->leftJoin(
            ['usw' => ScheduleWork::tableName()],
                [
                    'AND',
                    ['usw.user_id' => new Expression('`user_announcer`.`user_id`')],
                    ['usw.type' => ScheduleWork::TYPE_WEEK],
                    ['<=', 'time_' . $day . '_at', $expressionNow],
                    ['>=', 'time_' . $day . '_to', $expressionNow],
                ]
            );

        if (Yii::$app->request->get('ignore_unavailable')) {
            $query->andWhere([
                'NOT',
                ['usw.type' => ScheduleWork::TYPE_UNDEFINED],
            ]);
        }
            
        $query->joinWith(['user']);

        $sounds = (new Query())->select('announcer_id')->from(Announcer::table2Sounds());
        $query->andWhere(['user_announcer.id' => $sounds]);

        if (Yii::$app->user->identity && !Yii::$app->user->identity->isAnnouncer) {
            $query->with('favorite');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'isOnline',
                    'cost',
                    'cost_a4',
                    'cost_vocal',
                    'cost_parody',
                    'cost_sound_mount',
                    'cost_audio_adv',
                    'user.last_visit',
                    'user.rating',
                    'quantity' => [
                        'asc' => ['quantity_reviews' => SORT_ASC],
                        'desc' => ['quantity_reviews' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                ],
                'defaultOrder' => [
                    'isOnline' => SORT_DESC,
                    'user.last_visit' => SORT_DESC,
                    'user.rating' => SORT_DESC,
                    'quantity' => SORT_DESC,
                ],
            ],
            'pagination' => [
                'pageSize' => 12
            ]
        ]);

        $userActive = User::find()
            ->select('id')
            ->andWhere([
                'is_blocked' => 0,
                'status' => User::STATUS_ACTIVE,
                'type' => User::TYPE_ANNOUNCER
            ]);

        $query->andWhere(['user_announcer.user_id' => $userActive]);

        if (!empty($this->favorite) && Yii::$app->user) {
            $favorite = Favorite::find()
                ->select('announcer_id')
                ->where(['client_id' => Yii::$app->user->id]);

            $query->andWhere(['user_announcer.user_id' => $favorite]);
        }

        if (!empty($this->online)) {
            $query->andWhere(['user_announcer.user_id' => ScheduleWork::getQueryOnline()]);
        }

        if (!empty($this->presentation) || !empty($this->language) || !empty($this->age) || !empty($this->gender)) {
            $queryPresentation = null;
            $queryAge = null;
            $queryGender = null;
            if (!empty($this->presentation)) {
                $queryPresentation = Presentation::find()
                    ->select('id')
                    ->where(['code' => $this->presentation]);
            }
            if (!empty($this->age)) {
                $queryAge = Age::find()
                    ->select('id')
                    ->where(['code' => $this->age]);
            }
            if (!empty($this->gender)) {
                $queryGender = Gender::find()
                    ->select('id')
                    ->where(['code' => $this->gender]);
            }

            $queryJoin = Announcer::find()
                ->select('user_id')
                ->joinWith([
                    'sounds' => function (ActiveQuery $query) use ($queryPresentation, $queryAge, $queryGender) {
                        if (!empty($queryPresentation)) {
                            $query->andFilterWhere(['presentation_id' => $queryPresentation]);
                        }

                        $query->andFilterWhere(['lang_code' => $this->language]);

                        if (!empty($queryAge)) {
                            $query->andFilterWhere(['age_id' => $queryAge]);
                        }

                        if (!empty($queryGender)) {
                            $query->andFilterWhere(['gender_id' => $queryGender]);
                        }
                    }
                ]);
            $query->andWhere(['user_announcer.user_id' => $queryJoin]);

            $soundsFilterQuery = AnnouncerSound::find();
            if (!empty($queryPresentation)) {
                $soundsFilterQuery->andFilterWhere(['presentation_id' => $queryPresentation]);
            }

            $soundsFilterQuery->andFilterWhere(['lang_code' => $this->language]);

            if (!empty($queryAge)) {
                $soundsFilterQuery->andFilterWhere(['age_id' => $queryAge]);
            }

            if (!empty($queryGender)) {
                $soundsFilterQuery->andFilterWhere(['gender_id' => $queryGender]);
            }

            $soundsFilterItems = ArrayHelper::index($soundsFilterQuery->all(), null, 'announcer_id');
        }

        if (!empty($this->photo)) {
            $queryJoin = (new Query())
                ->from('user_announcer_2_photos')
                ->select('announcer_id');
            $query->andWhere(['user_announcer.id' => $queryJoin]);
        }

        if (!empty($this->costBy)) {
            $query->andWhere(['>=', 'user_announcer.'.$this->costBy, 0]);
        }
        if (!empty($this->costAt)) {
            $query->andFilterWhere(['>=', 'user_announcer.'.($this->costBy ?? 'cost'), $this->costAt]);
        }
        if (!empty($this->costTo)) {
            $query->andFilterWhere(['<=', 'user_announcer.'.($this->costBy ?? 'cost'), $this->costTo]);
        }

        if (!empty($this->text)) {
            $presentationQuery = AnnouncerSound::find()
                ->alias('as')
                ->select('a.user_id')
                ->innerJoin(
                    ['a' => Announcer::tableName()],
                    'a.id = as.announcer_id'
                )
                ->andWhere(['like', 'as.parodist_name', $this->text]);

            $userQuery = User::find()
                ->select('id')
                ->where([
                    'OR',
                    ['like', 'firstname', $this->text],
                    ['like', 'lastname', $this->text],
                ]);

            $query->andWhere([
                'OR',
                ['user_announcer.user_id' => $presentationQuery],
                ['user_announcer.user_id' => $userQuery],
            ]);
        }

        if (!empty($this->id)) {
            $query->andWhere([
                'IN',
                '`user`.`id`',
                $this->id
            ]);
        }

        if (!empty($this->sort)) {
            switch ($this->sort) {
                case 'cost':
                    $order = !empty($this->costBy) ? $this->costBy : 'cost';
                    $query->orderBy(['user_announcer.'.$order => SORT_ASC]);
                    break;
                case '-cost':
                    $order = !empty($this->costBy) ? $this->costBy : 'cost';
                    $query->orderBy(['user_announcer.'.$order => SORT_DESC]);
                    break;
                case 'quantity':
                    $query->orderBy(['user.rating' => SORT_ASC]);
                    break;
                case '-quantity':
                    $query->orderBy(['user.rating' => SORT_DESC]);
                    break;
                default:
                    break;
            }
        }

        $dataProvider->pagination->page = $page - 1;
        $dataProvider->pagination->pageSize = $perPage;

        $ids = [];
        /**
         * @var Announcer $announcer
         */
        foreach ($dataProvider->models as $announcer) {
            $ids[] = $announcer->user_id;
            if (!empty($this->costBy)) {
                $announcer->filterCost = $this->costBy;
            }
        }

        $subQueryRank = (new Query())
            ->select(['id', 'num' => 'RANK() OVER(PARTITION BY announcer_id ORDER BY created_at desc, client_id)'])
            ->from(Review::tableName())
            ->where([
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'announcer_id' => $ids,
            ]);

        $reviewQuery = (new Query())
            ->select('id')
            ->from(['X' => $subQueryRank])
            ->where(['<=', 'num', 2]);

        $reviews = Review::find()
            ->where(['id' => $reviewQuery])
            ->with(['client', 'client.photo'])
            ->all();

        $reviews = ArrayHelper::index($reviews, 'id', 'announcer_id');

        /**
         * @var Announcer $announcer
         */
        foreach ($dataProvider->models as $announcer) {
            $announcer->populateRelation('reviews', $reviews[$announcer->user_id] ?? []);

            if (isset($soundsFilterItems)) {
                $announcer->populateRelation('sounds', $soundsFilterItems[$announcer->id]);
            }
        }

        return $dataProvider;
    }
}

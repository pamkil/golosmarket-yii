<?php

namespace api\modules\v1\actions\bill;

use api\modules\v1\actions\common\ViewAction;
use api\modules\v1\models\filters\user\ChatSearch;
use api\modules\v1\models\records\announcer\Opponent;
use api\modules\v1\models\records\auth\User;
use api\modules\v1\models\records\user\Bill;
use Yii;

/**
 * @OA\Get(
 *      path="/chat/{id}/bill",
 *      summary="Последний новый счет с собеседником, ИД собеседника",
 *      tags={"Chat"},
 *      @OA\Parameter(
 *          name="id",
 *          in="path",
 *          description="User ID, ИД собеседника",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="ok",
 *          @OA\JsonContent(
 *              type="array",
 *              @OA\Items(ref="#/components/schemas/Bill")
 *          )
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */
class View extends ViewAction
{
    /** @inheritdoc */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $request = Yii::$app->request;

        $lastId = (int)$request->get('id', 0);
        $perPage = (int)$request->get('per_page', 100);
        $isAnnouncer = Yii::$app->user->identity->type === User::TYPE_ANNOUNCER;
        $announcerId = $isAnnouncer ? Yii::$app->user->id : $id;
        $clientId = $isAnnouncer ? $id : Yii::$app->user->id;

        $opponent = Opponent::findOne((int)$id);
        $bill = Bill::getNew($announcerId, $clientId) ?? Bill::getLastPaid($announcerId, $clientId); // {id: opponent && opponent.id, sum: 222, num: user.id}

        return [
            'items' => (new ChatSearch())->search($id, Yii::$app->user->id, $isAnnouncer, $lastId, $perPage),
            'opponent' => $opponent,
            'bill' => $bill,
        ];
    }
}

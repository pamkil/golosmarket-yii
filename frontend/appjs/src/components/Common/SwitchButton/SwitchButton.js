import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import IconQu from '../../../images/icon_question.png'

const SwitchButton = (
    {
        buttons = [],
        onChange,
        defaultValueActive: checkedValue,
        title = null,
        info = '',
        addClassName = ''
    }
) => {
    const changeHandler = (newValue) => {
        let value = newValue;
        if (newValue === checkedValue) {
            value = null;
        }
        onChange(value);
    };

    return (
        <>
            {
                title &&
                <div className="head">
                    { title }
                    <span className="tooltip">
						<span className="icon">
							<img src={ IconQu } alt="icon question"/>
							<span>{ info }</span>
						</span>
                    </span>
                </div>
            }
            {
                buttons.map(({ title, value, count = null }) =>
                    <div key={ value } className="item">
                        <button
                            className={ classes(addClassName, { active: checkedValue === value }) }
                            onClick={ () => changeHandler(value) }
                        >
                            {
                                // checkedValue === value && <div className="close-f mobile-no">X</div>
                            }
                            { title }
                            { count ? <span>{ count }</span> : '' }
                        </button>
                    </div>
                )
            }
        </>
    );
};

SwitchButton.propTypes = {
    buttons: PropTypes.array,
    onChange: PropTypes.func,
    defaultActive: PropTypes.bool
};

export default SwitchButton;
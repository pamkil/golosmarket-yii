import React, {useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames'

import IconQu from '../../../images/icon_question.png'
import ReactDOM from "react-dom";

const SwitchButtonAdditional = ({
                                  buttons = [],
                                  onChange,
                                  activeFilter,
                                  value: currentValue = null,
                                  title = null,
                                  emptyString = '',
                                  info = null,
                                  addClass = ''
                                }) => {


  const [open, setOpen] = useState(false);
  const dropdownRef = useRef();
  const buttonRef = useRef();

  const handleClickOutside = (event) => {
    const domNode = ReactDOM.findDOMNode(dropdownRef.current);
    const isInnerClick = domNode && domNode.contains(event.target);
    const ignoreClick = event.target.isEqualNode(buttonRef.current);

    if (!isInnerClick && !ignoreClick && open) {
      setOpen(false);
    }
  };

  const filterChange = (value) => {
    setOpen(false);
    onChange(value);
  }

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true);

    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    }
  });

  let btnLabel = '';
  let el = buttons.find((f, fi) => f.id === activeFilter);

  if (el) {
    btnLabel = el.name;
  }

  return (
    <div className={'switch-holder'}>
      {
        title &&
        <div className="head">
          {title}
          {info === null ? null :
            <span className="tooltip">
              <span className="icon">
                <img src={IconQu} alt="icon question"/>
                <span>{info}</span>
              </span>
            </span>
          }
        </div>
      }
      <div className={'sorter-menu'}>
        <div ref={buttonRef} className={"sorter-value" + (open ? ' __open' : '')} onClick={() => {
          setOpen(!open);
        }}>
          <span className={''}>{btnLabel || emptyString}</span>
          <div className="arrowhead">
            <div className="arrowhead-left"/>
            <div className="arrowhead-right"/>
          </div>
        </div>
        {
          open ?
            <div ref={dropdownRef} className="sorter-dropdown">
              {
                buttons.map(b => {
                  if (b.hasOwnProperty('id')) {
                    b.value = b.id;
                  }
                  if (b.hasOwnProperty('name')) {
                    b.title = b.name;
                  }
                  return b;
                }).filter(f => f.value !== activeFilter).map(({title, value, count = null}) =>
                  <div
                    key={value}
                    className={classes(addClass, {active: currentValue === value})}
                    onClick={() => filterChange(value)}
                  >
                    {
                      // currentValue === value && <div className="close-f mobile-no">X</div>
                    }
                    {title} {count ? count : ''}
                  </div>
                )
              }
            </div>
            : null
        }
      </div>
    </div>
  );
};

SwitchButtonAdditional.propTypes = {
  buttons: PropTypes.array,
  onChange: PropTypes.func,
  activeFilter: PropTypes.string,
  emptyString: PropTypes.string,
  defaultActive: PropTypes.bool
};

export default SwitchButtonAdditional;

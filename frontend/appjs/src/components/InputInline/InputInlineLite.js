import React from "react";

const InputInlineLite = ({
                           children,
                           refName = null,
                           input,
                           disabled,
                           inputClass,
                           floatPlaceholder,
                           type = 'text',
                           placeholder = null,
                           meta: {touched, error, warning}
                         }) => {

    if (input.value === 0 && !disabled) {
        input.value = undefined;
    }

    return <>
    {
      type === 'textarea'
        ? (
          <div className={'input-holder'}>
            <textarea
              ref={refName}
              {...input}
              placeholder={placeholder}
              className={(inputClass || '') + (touched && error ? ' error' : '')}
              disabled={disabled}
            />
            {floatPlaceholder && placeholder ? <span className={'input-placeholder'}>{placeholder}</span> : null}
          </div>
        )
        : (
          <div className={'input-holder'}>
            <input
              ref={refName}
              type={type}
              {...input}
              placeholder={placeholder}
              className={(inputClass || '') + (touched && error ? ' error' : '')}
              disabled={disabled}
            />
            {floatPlaceholder && placeholder ? <span className={'input-placeholder'}>{placeholder}</span> : null}
          </div>
        )
    }
    {children}
    {
      touched && (
        (error && <div className="error-text">{error}</div>)
        ||
        (warning && <div className="warning-text">{warning}</div>)
      )
    }
  </>
};

export default InputInlineLite;

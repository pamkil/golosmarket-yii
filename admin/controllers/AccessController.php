<?php

namespace admin\controllers;

use common\models\access\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Role;
use yii\rbac\Permission;
use yii\web\ForbiddenHttpException;
use yii\data\ArrayDataProvider;
use yii\web\BadRequestHttpException;
use yii\validators\RegularExpressionValidator;

class AccessController extends SecurityController
{
    protected $error = [];
    protected $pattern4Role = '/^[a-zA-Z0-9_-]+$/';
    protected $pattern4Permission = '/^[a-zA-Z0-9_\/-]+$/';

    public function actionRole()
    {
        if (!Yii::$app->user->can('viewRules')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $this->view->title = 'Управление ролями';
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $dataProvider = new ArrayDataProvider([
            'allModels' => Yii::$app->authManager->getRoles(),
            'sort' => [
                'attributes' => ['name', 'description']
            ],
            'pagination' => [
                'pageSize' => (int)Yii::$app->request->get('per-page', 30)
            ]
        ]);

        return $this->render('role', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreateRole()
    {
        if (!Yii::$app->user->can('updateRules')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $authManager = Yii::$app->authManager;

        $post = Yii::$app->request->post();
        if ($post) {
            if (isset($post['name']) && $this->validate($post['name'], $this->pattern4Role) && $this->isUnique($post['name'], 'role')) {
                $role = $authManager->createRole($post['name']);
                $role->description = $post['description'];
                $authManager->add($role);
                $this->setPermissions(isset($post['permissions']) ? $post['permissions'] : [], $role);

                $model = Yii::$app->authManager->getRole($post['name']);

                $userIds = $authManager->getUserIdsByRole($model->name);
                if (isset($post['users']) && $post['users']) {
                    foreach ($post['users'] as $userId) {
                        if (!in_array($userId, $userIds)) {
                            $authManager->assign($model, $userId);
                        }

                        unset($userIds[array_search($userId, $userIds)]);
                    }
                }

                if ($userIds) {
                    foreach ($userIds as $userId) {
                        $authManager->revoke($model, $userId);
                    }
                }

                Yii::$app->session->addFlash('success', 'Успешно создано');

                return $this->redirect([
                    'update-role',
                    'name' => $role->name
                ]);
            }

            foreach ($this->error as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Новая роль';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список ролей',
            'url' => ['role']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $permissions = ArrayHelper::map(Yii::$app->authManager->getPermissions(), 'name', 'description');

        return $this->render('_form_role', [
            'permissions' => $permissions,
            'error' => $this->error
        ]);
    }

    public function actionUpdateRole($name)
    {
        if (!Yii::$app->user->can('updateRules')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $model = Yii::$app->authManager->getRole($name);

        if (!($model instanceof Role)) {
            throw new BadRequestHttpException('Страница не найдена');
        }

        $authManager = Yii::$app->authManager;

        $post = Yii::$app->request->post();
        if ($post) {
            if ($this->validate($post['name'], $this->pattern4Role) && !$this->isUnique($post['name'], 'role')) {
                $model = $this->setAttribute($model, $post);
                $authManager->update($name, $model);
                $authManager->removeChildren($model);
                $this->setPermissions(isset($post['permissions']) ? $post['permissions'] : [], $model);

                $userIds = $authManager->getUserIdsByRole($model->name);
                if (isset($post['users']) && $post['users']) {
                    foreach ($post['users'] as $userId) {
                        if (!in_array($userId, $userIds)) {
                            $authManager->assign($model, $userId);
                        }

                        unset($userIds[array_search($userId, $userIds)]);
                    }
                }

                if ($userIds) {
                    foreach ($userIds as $userId) {
                        $authManager->revoke($model, $userId);
                    }
                }

                Yii::$app->session->addFlash('success', 'Успешно обновлено');

                return $this->redirect([
                    'update-role',
                    'name' => $model->name
                ]);
            }

            foreach ($this->error as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Изменение роли: "' . $model->name . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список ролей',
            'url' => ['role']
        ];
        $this->view->params['breadcrumbs'][] = $model->name;

        $permissions = ArrayHelper::map($authManager->getPermissions(), 'name', 'description');
        $role_permit = array_keys($authManager->getPermissionsByRole($name));

        $roleUsers = $authManager->getUserIdsByRole($model->name);
        $limit = 100;
        $users = User::find()->where(['IN', 'id', $roleUsers])->limit($limit)->all();
        if (count($users) > $limit) {
            $limitView = true;
        } else {
            $limitView = false;
        }
        return $this->render('_form_role', [
            'model' => $model,
            'permissions' => $permissions,
            'role_permit' => $role_permit,
            'limitView' => $limitView,
            'users' => $users
        ]);
    }

    public function actionDeleteRole($name)
    {
        if (!Yii::$app->user->can('updateRules')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $role = Yii::$app->authManager->getRole($name);
        if ($role) {
            Yii::$app->authManager->removeChildren($role);
            Yii::$app->authManager->remove($role);
        }

        Yii::$app->session->addFlash('success', 'Успешно удалено');

        return $this->redirect(['role']);
    }

    public function actionPermission()
    {
        if (!Yii::$app->user->can('viewRules')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $this->view->title = 'Правила доступа';
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $dataProvider = new ArrayDataProvider([
            'allModels' => Yii::$app->authManager->getPermissions(),
            'sort' => [
                'attributes' => ['name', 'description']
            ],
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('permission', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreatePermission()
    {
        if (!Yii::$app->user->can('updateRules')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $post = Yii::$app->request->post();
        if ($post) {
            $permission = $this->clear($post['name']);
            if ($permission && $this->validate($permission, $this->pattern4Permission) && $this->isUnique($permission, 'permission')) {
                $permit = Yii::$app->authManager->createPermission($permission);
                $permit->description = $post['description'];
                Yii::$app->authManager->add($permit);

                Yii::$app->session->addFlash('success', 'Успешно создано');

                return $this->redirect([
                    'update-permission',
                    'name' => $permit->name
                ]);
            }

            foreach ($this->error as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Новое правило';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список правил доступа',
            'url' => ['permission']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('_form_permission');
    }

    public function actionUpdatePermission($name)
    {
        if (!Yii::$app->user->can('updateRules')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $model = Yii::$app->authManager->getPermission($name);
        if (!($model instanceof Permission)) {
            throw new BadRequestHttpException('Страница не найдена');
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if (isset($post['name'])) {
                $permission = $this->clear($post['name']);
                if ($this->validate($permission, $this->pattern4Permission)) {
                    if ($permission !== $model->name && $this->isUnique($permission, 'permission')) {
                        $model->name = $permission;
                    }

                    $model->description = $post['description'];
                    Yii::$app->authManager->update($name, $model);

                    Yii::$app->session->addFlash('success', 'Успешно обновлено');

                    return $this->redirect([
                        'update-permission',
                        'name' => $model->name
                    ]);
                }

                foreach ($this->error as $error) {
                    Yii::$app->session->addFlash('error', $error);
                }
            }
        }

        $this->view->title = 'Изменение правила: "' . $model->description . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список правил доступа',
            'url' => ['permission']
        ];
        $this->view->params['breadcrumbs'][] = $model->description;

        return $this->render('_form_permission', [
            'model' => $model
        ]);
    }

    public function actionDeletePermission($name)
    {
        if (!Yii::$app->user->can('updateRules')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $permit = Yii::$app->authManager->getPermission($name);
        if ($permit) {
            Yii::$app->authManager->remove($permit);
        }

        Yii::$app->session->addFlash('success', 'Успешно удалено');

        return $this->redirect(['permission']);
    }

    protected function setAttribute($object, $data)
    {
        $object->name = $data['name'];
        $object->description = $data['description'];

        return $object;
    }

    protected function setPermissions($permissions, $role)
    {
        foreach ($permissions as $permit) {
            $newPermit = Yii::$app->authManager->getPermission($permit);
            Yii::$app->authManager->addChild($role, $newPermit);
        }
    }

    protected function validate($field, $regex)
    {
        $validator = new RegularExpressionValidator(['pattern' => $regex]);
        if ($validator->validate($field, $error)) {
            return true;
        }

        $this->error[] = 'Значение "' . $field . '" содержит не допустимые символы';
        return false;
    }

    protected function isUnique($name, $type)
    {
        if ($type == 'role') {
            $role = Yii::$app->authManager->getRole($name);
            if ($role instanceof Role) {
                $this->error[] = 'Роль с таким именем уже существует: ' . $name;
                return false;
            } else {
                return true;
            }
        } elseif ($type == 'permission') {
            $permission = Yii::$app->authManager->getPermission($name);
            if ($permission instanceof Permission) {
                $this->error[] = 'Правило с таким именем уже существует: ' . $name;
                return false;
            } else {
                return true;
            }
        }

        return false;
    }

    protected function clear($value)
    {
        if (!empty($value)) {
            $value = trim($value, "/ \t\n\r\0\x0B");
        }

        return $value;
    }
}

import React from 'react';
import './NotifyCount.css';

const NotifyCount = ({ count, className }) =>
    count ? <div title={count} className={`notify-count ${className || ''}`}>{count > 9 ? '9+' : count}</div> : null;

export default NotifyCount;

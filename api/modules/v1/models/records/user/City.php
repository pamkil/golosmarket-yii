<?php

namespace api\modules\v1\models\records\user;

/**
 * @OA\Schema(title="Город")
 */
class City extends \common\models\user\City
{
    /**
     * @OA\Property(property="id", type="integer", description="ИД")
     * @OA\Property(property="code", type="string", description="code")
     * @OA\Property(property="title", type="string", description="title")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'code',
            'title',
        ];
    }
}
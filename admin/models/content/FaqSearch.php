<?php

namespace admin\models\content;

use common\models\content\Faq;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class FaqSearch extends Model
{
    public $question;
    public $answer;
    public $active;
    public $position;

    public function rules()
    {
        return [
            [
                ['question', 'answer'],
                'string'
            ],
            [
                ['active',],
                'boolean'
            ],
            [
                ['position',],
                'number'
            ],
        ];
    }

    public function search($params)
    {
        $query = Faq::find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'active',
                        'position',
                        'question',
                        'answer',
                    ],
                    'defaultOrder' => [
                        'position' => SORT_ASC
                    ],
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'AND',
                ['like', 'question', $this->question],
                ['like', 'answer', $this->answer],
                ['active' => $this->active],
            ]
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
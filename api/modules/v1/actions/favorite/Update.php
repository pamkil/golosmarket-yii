<?php

namespace api\modules\v1\actions\favorite;

use api\modules\v1\models\checks\favorite\UpdateForm;
use common\service\Favorite;
use yii\rest\Action;
use yii\web\BadRequestHttpException;

/**
 * @OA\Patch(
 *      path="/favorite",
 *      summary="Обновление списка избранных",
 *      tags={"Favorite"},
 *      @OA\Response(
 *          response="200",
 *          description="Объект токена",
 *          @OA\JsonContent(
 *              @OA\Property(
 *                  property="favoritesCount",
 *                  type="integer"
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *          response="400",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="401",
 *          description="Unauthorized",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */

/**
 *
 * Class Update
 * @package api\modules\v1\actions\favorite
 *
 */
class Update extends Action
{
    /**
     * @throws BadRequestHttpException
     */
    public function run()
    {
        $form = new UpdateForm();
        $form->loadFromBody();

        if (!$form->validate()) {
            throw new BadRequestHttpException($form->errors);
        }

        return [
            'favoritesCount' => Favorite::batchAdd($form)
        ];
    }
}

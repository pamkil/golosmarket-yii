import React, {useEffect, useState} from "react";
import PhoneInput2 from 'react-phone-input-2';

const checkPlus = (val) => {
  let newVal = val || '';
  return newVal.length ? ('+' + newVal).replace('++', '+') : '';
}

const PhoneInputMask = props => {
  const {meta, input, placeholder, inputClass, floatPlaceholder, type, setPhoneValid} = props;
  const {touched, error, warning} = meta;
  const [value, setValue] = useState(checkPlus(input.value));

  useEffect(() => {
    input.onChange(value);
  }, [value]);

  const phoneChange = (val, country, e, formattedValue) => {
    setValue(checkPlus(val || ''));

    if (country && country.hasOwnProperty('format') && String(formattedValue).length === country.format.length) {
      setPhoneValid('');
    } else {
      setPhoneValid(val.length ? 'Неверный формат телефона' : 'Необходимо заполнить');
    }
  }

  return (
    <>
      <div className={'input-holder'}>
        <PhoneInput2
          placeholder={placeholder}
          name={input.name}
          copyNumbersOnly={false}
          containerClass={(inputClass + '') + ((value || input.value.length > 0) ? '' : ' __empty') + (touched && error ? ' __error' : '')}
          value={input.value}
          onChange={phoneChange}
          type={type || "text"}
        />
        {floatPlaceholder && placeholder ? <span className={'input-placeholder'}>{placeholder}</span> : null}
      </div>
      {
        touched && (
          (error && <div className="error-text">{error}</div>)
          ||
          (warning && <div className="warning-text">{warning}</div>)
        )
      }
    </>
  );
};

export default PhoneInputMask;

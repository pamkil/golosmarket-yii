<?php

use admin\models\content\NewsSearch;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Url;

/**
 * @var ActiveDataProvider $dataProvider
 * @var NewsSearch $searchModel
 */
?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <a href="<?= Url::to(['create']) ?>" class="btn bg-olive btn-flat margin">
            Добавить
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                'id',
                'active:boolean',
                'title',
                'date:date',
                'h1:text',
                [
                    'attribute' => 'body',
                    'format' => 'text',
                    'value' => function($model, $key) {
                        return \yii\helpers\StringHelper::truncate($model->body, 100);
                    }
                ],
                [
                    'class' => ActionColumn::class,
                    'template' => '{update}  {delete}',
                    'buttonOptions' => ['class' => 'margin']
                ],
            ]
        ]); ?>
    </div>
</div>

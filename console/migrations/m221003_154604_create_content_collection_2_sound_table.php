<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%content_collection_2_sound}}`.
 */
class m221003_154604_create_content_collection_2_sound_table extends Migration
{
    private string $tableName = '{{%content_collection_2_sound}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            $this->tableName,
            [
                'collection_id' => $this->integer()->notNull(),
                'announcer_id' => $this->integer()->notNull(),
                'sound_id' => $this->integer()->notNull(),
                'position' => $this->integer()->null(),
                'PRIMARY KEY (collection_id, sound_id)'
            ],
            $tableOptions
        );

        $this->addForeignKey(
            'FK-content_collection_2_sound-collection_id',
            $this->tableName,
            'collection_id',
            '{{%content_collection}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK-content_collection_2_sound-announcer_id',
            $this->tableName,
            'announcer_id',
            '{{%user_announcer}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK-content_collection_2_sound-sound_id',
            $this->tableName,
            'sound_id',
            '{{%file}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->createIndex(
            'IDX-content_collection-collection_id-sound_id',
            $this->tableName,
            ['collection_id', 'sound_id'],
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('IDX-content_collection-collection_id-sound_id', $this->tableName);
        $this->dropForeignKey('FK-content_collection_2_sound-collection_id', $this->tableName);
        $this->dropForeignKey('FK-content_collection_2_sound-announcer_id', $this->tableName);
        $this->dropForeignKey('FK-content_collection_2_sound-sound_id', $this->tableName);
        $this->dropTable('{{%content_collection_2_sound}}');
    }
}

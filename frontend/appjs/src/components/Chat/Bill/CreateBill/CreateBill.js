import React, { useState, useEffect, useRef } from "react";
import classNames from "classnames";
import "./CreateBill.css";
import {
  CreateUserReview,
  getPaymentInfo,
  read,
  updatePayment,
} from "../../../../data/redux";
import { connect, useDispatch, useSelector } from "react-redux";
import wallet from "../../../../images/wallet.svg";
import billIcon from "../../../../images/bill.svg";
import ModalNew from "../../../Common/Modal/Modal";
import ReviewUserForm from "../../../Header/Form/ReviewUserForm";
import { toast } from "react-toastify";

function addSpacesToCardNumber(input) {
  let cardNumber = input.replace(/\s/g, "");

  let formattedCardNumber = "";
  for (let i = 0; i < cardNumber.length; i++) {
    if (i > 0 && i % 4 === 0) {
      formattedCardNumber += " ";
    }
    formattedCardNumber += cardNumber[i];
  }

  return formattedCardNumber;
}

const PaymentForm = ({
  sumValue,
  payedBillHandler,
  sumValueChange,
  cardValue,
  cardValueChange,
  createBillHandler,
  bill,
  cancelBillHandler,
  errorSumText,
  errorCardText,
}) => {
  const dispatch = useDispatch();
  const [showApprovePayed, setShowApprovdePayed] = useState(false);
  const [showCommissionNotify, setShowCommissionNotify] = useState(false);

  useEffect(() => {
    dispatch(getPaymentInfo());
  }, [bill]);
  return (
    <>
      <div className="payment-form">
        <div className="payment-form__row">
          {bill !== null && bill.status !== 2 ? (
            <p
              style={{
                display: "flex",
                alignItems: "center",
                gap: "10px",
                color: "#000000",
              }}
            >
              <img src={billIcon} style={{ width: "20px", height: "20px" }} />{" "}
              <b>Счет выставлен.</b> Клиент переведет оплату на карту и подтвердит кнопкой в форме оплаты. Дальше вы сможете оставить отзыв
            </p>
          ) : bill && bill.status === 2 && bill.is_payed !== 1 ? (
            <p style={{ display: "flex", alignItems: "center", gap: "10px" }}>
              <img style={{ width: "20px", height: "20px" }} src={wallet} />{" "}
              <b>Клиент оплатил предыдущий счёт</b> Если вы не получили оплату -
              оставьте отзыв о клиенте и напишите в Обратную связь
            </p>
          ) : (
            <p style={{ display: "flex", alignItems: "center", gap: "10px" }}>
              <img style={{ width: "20px", height: "20px" }} src={wallet} />{" "}
              <b>Выставьте счет.</b> Клиент оплатит по номеру карты. Не
              передавайте платежные реквизиты в чате
            </p>
          )}
        </div>
        <div style={{ alignItems: "baseline" }} className="payment-form__row">
          <div className="payment-form__column">
            <input
              className="payment-form__input"
              type="number"
              min={2}
              disabled={bill ? true : false}
              onChange={sumValueChange}
              value={sumValue}
              placeholder="Введите сумму"
            />
            <div className="error-message-container">
              {errorSumText ? (
                <p style={{ color: "red" }}>{errorSumText}</p>
              ) : (
                <p style={{ fontSize: "10px", color: "gray" }}>
                  Комиссия составит {((sumValue / 100) * 30).toFixed(2)}₽
                </p>
              )}
            </div>
          </div>
          <div className="payment-form__column">
            <input
              placeholder="Номер вашей карты"
              className="payment-form__input"
              min={2}
              id="ccn"
              type="tel"
              inputmode="numeric"
              pattern="[0-9\s]{13,19}"
              autocomplete="cc-number"
              maxlength="19"
              disabled={bill ? true : false}
              onChange={cardValueChange}
              value={cardValue}
            />
            <div className="error-message-container">
              {errorCardText ? (
                <p style={{ color: "red" }}>{errorCardText}</p>
              ) : (
                <></>
              )}
            </div>
          </div>
          <div className="payment-form__column">
            {!bill && (
              <button onClick={createBillHandler} className="green">
                Выставить счет
              </button>
            )}
          </div>
          <div className="payment-form__column">
          {bill === null ? (
              <></>
            ) : bill.status !== 2 && (
              <button
                onClick={cancelBillHandler}
                className="payment-form__button payment-form__button--red"
              >
                Отозвать счет
              </button>)}
            {/* {bill === null ? (
              <></>
            ) : bill.status !== 2 ? (
              <button
                onClick={cancelBillHandler}
                className="payment-form__button payment-form__button--red"
              >
                Отозвать счет
              </button>
            ) : bill.status === 2 && !showApprovePayed ? (
              <button
                onClick={() => {
                  setShowApprovdePayed(true);
                }}
                className="gray"
              >
                Оплату не получил. Отзыв
              </button>
            ) : (
              bill &&
              showApprovePayed && (
                <button
                  onClick={() => {
                    payedBillHandler(7);
                  }}
                  className="gray"
                >
                  Оставить негативный отзыв?
                </button>
              )
            )} */}
          </div>
        </div>
      </div>
      <div className="payment-form-mobile">
        <div className="payment-form__row">
          {bill ? (
            <p
              style={{
                display: "flex",
                alignItems: "center",
                gap: "10px",
                color: "#000000",
              }}
            >
              <img src={billIcon} style={{ width: "20px", height: "20px" }} />{" "}
              <b>Счет выставлен.</b>  Клиент переведет оплату на карту и подтвердит кнопкой в форме оплаты. Дальше вы сможете оставить отзыв
            </p>
          ) : bill && bill.status === 2 && bill.is_payed !== 1 ? (
            <p style={{ display: "flex", alignItems: "center", gap: "10px" }}>
              <img style={{ width: "20px", height: "20px" }} src={wallet} />{" "}
              <b>Клиент оплатил предыдущий счёт</b> Если вы не получили оплату -
              оставьте отзыв о клиенте и напишите в Обратную связь
            </p>
          ) : (
            <p style={{ display: "flex", alignItems: "center", gap: "10px" }}>
              <img style={{ width: "20px", height: "20px" }} src={wallet} />{" "}
              <b>Выставьте счет.</b> Клиент оплатит по номеру карты. Не
              передавайте платежные реквизиты в чате
            </p>
          )}
        </div>

        <div className="payment-form__row">
          {" "}
          <div className="payment-form__column">
            <input
              className="payment-form__input"
              type="number"
              min={2}
              disabled={bill ? true : false}
              onChange={sumValueChange}
              value={sumValue}
              placeholder="Введите сумму"
            />
            <div className="error-message-container">
              {errorSumText ? (
                <p style={{ color: "red" }}>{errorSumText}</p>
              ) : (
                <p style={{ fontSize: "10px", color: "gray" }}>
                  Комиссия составит {((sumValue / 100) * 30).toFixed(2)}₽
                </p>
              )}
            </div>
          </div>
        </div>
        <div className="payment-form__row">
          <div className="payment-form__column">
            <input
              placeholder="Номер вашей карты"
              className="payment-form__input"
              min={2}
              id="ccn"
              type="tel"
              inputmode="numeric"
              pattern="[0-9\s]{13,19}"
              autocomplete="cc-number"
              maxlength="19"
              disabled={bill ? true : false}
              onChange={cardValueChange}
              value={cardValue}
            />
            <div className="error-message-container">
              {errorCardText ? (
                <p style={{ color: "red" }}>{errorCardText}</p>
              ) : (
                <></>
              )}
            </div>
          </div>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            paddingBottom: "10px",
          }}
          className="payment-form__row"
        >
          <div className="payment-form__column">
          {!bill && (
              <button onClick={createBillHandler} className="green">
                Выставить счет
              </button>
            )}
            {/* {bill == null ? 
              !showApprovePayed ? (
              <button
                onClick={() => {
                  setShowApprovdePayed(true);
                }}
                className="gray"
              >
                Оплата прошла
              </button>
            ) : showApprovePayed ? (
              <button
                onClick={() => {
                  payedBillHandler();

                  setTimeout(() => {
                    setShowCommissionNotify(false);
                  }, 5000);
                  setShowCommissionNotify(true);
                }}
                className="green"
              >
                Нажмите еще раз, если получили перевод
              </button>
            )} */}
            {showCommissionNotify && (
              <div className="error-message-containerё">
                <b>Оплата зафиксирована</b>
              </div>
            )}
          </div>
          <div className="payment-form__column">
            {bill && bill.status !== 2 && (
              <button
                onClick={cancelBillHandler}
                className="payment-form__button payment-form__button--red"
              >
                Отозвать счет
              </button>
            )}
          </div>
        </div>
      </div>
    </>
  );
};
const CreateBill = ({
  read,
  createBill,
  bill = null,
  payedBill,
  cancelBill,
}) => {
  const { opponent } = useSelector((state) => ({
    opponent: state.chat.message.opponent,
  }));
  const [open, setOpen] = useState(false);
  const [showReviewUserForm, setShowReviewUserForm] = useState(false);
  const [sumValue, setSumValue] = useState(bill ? bill.sum : "");
  const [cardValue, setCardValue] = useState(bill ? bill.card_number : "");
  const [errorSumText, setErrorSumText] = useState("");
  const [errorCardText, setErrorCardText] = useState("");
  const [prevIsPaid, setPrevIsPaid] = useState(false);

  useEffect(() => {
    if (bill) {
      // setSumValue(bill.sum);
      setPrevIsPaid(bill.isPaid)
    } 
    if(bill && !prevIsPaid && bill.isPaid === 1){
      setSumValue("")
      setCardValue("")
    }
  }, [bill]);

  const showHandler = () => setOpen((oldShow) => !oldShow);
  const sumValueChangeHandler = (event) => {

    setSumValue(event.target.value);
  };
  const cardValueChangeHandler = (event) => {
    const formattedInput = addSpacesToCardNumber(event.target.value);
    setCardValue(formattedInput);
  };

  const handleCloseReviewForm = (closeFunction = () => {}) => {
    closeFunction(false);
  };

  // подтверждает диктор
  const payedBillHandler = (status = 3) => {
    if (status === 7) {
      payedBill(bill, status);
      setShowReviewUserForm(true);
    }
    approveCommissionPayed();
  };
  const approveCommissionPayed = async () => {
    await getPaymentInfo();
  };

  const createBillHandler = () => {
    setErrorCardText("");
    setErrorSumText("");
    if (!cardValue.replace(/\s/g, "") || cardValue.replace(/\s/g, "") === "0") {
      setErrorCardText("Поле не может быть пустым");
      return;
    }
    if (sumValue.replace(/\s/g, "") === "0" || sumValue == 0 || !sumValue) {
      setErrorSumText("Поле не может быть пустым");
      return;
    }
    if (
      cardValue.replace(/\s/g, "").length !== 16 &&
      cardValue.replace(/\s/g, "").length !== 18
    ) {
      setErrorCardText("Введите корректный номер карты");
      return;
    }
    const newBill = {
      ...bill,
      sum: parseInt(sumValue),
      card_number: cardValue.replace(/\s/g, ""),
    };
    createBill(newBill);
  };

  const reviewUserFormSubmitHandler = (values) => {
    values.opponent_id = opponent.id || null;
    values.other_id = opponent.otherId || null;
    CreateUserReview(values);
    setShowReviewUserForm(false);
    read([opponent.notifyId]);

    toast.info("Отзыв успешно отправлен");
  };

  const cancelBillHandler = () => {
    setOpen(false);
    cancelBill(bill);
  };
  return (
    <>
      <ModalNew
        show={showReviewUserForm}
        onCloseRequest={() => handleCloseReviewForm(setShowReviewUserForm)}
      >
        <ReviewUserForm
          isAnnouncer={false}
          onSubmit={reviewUserFormSubmitHandler}
          createUserReviewFormNotificationTimer={null}
        />
      </ModalNew>
      <div
        className={classNames("create-bill", {
          "create-bill--is-paid": bill && bill.isPaid,
        })}
      >
        <div
          className={classNames({
            "create-bill__button": true,
            "create-bill__button--right": open,
            "create-bill__button--warning": bill,
          })}
          onClick={showHandler}
        >
          {!open ? (
            bill ? (
              bill.isPaid ? (
                `Счет №${bill.id} на сумму ${bill.sum} руб. оплачен. Нажмите, чтобы выставить новый. Письмо по комиссии отправили вам на почту`
              ) : (
                <div className="create-bill-message__button">
                  <p>
                    Вы уже выставили счет. Нажмите, чтобы открыть
                  </p>
                </div>
              )
            ) : (
              <p>Нажмите здесь, чтобы выставить счёт</p>
            )
          ) : (
            <span className="create-bill__button-close">&#10005;</span>
          )}
        </div>
        {open && (
          <PaymentForm
            payedBillHandler={payedBillHandler}
            errorSumText={errorSumText}
            errorCardText={errorCardText}
            createBillHandler={createBillHandler}
            cancelBillHandler={cancelBillHandler}
            sumValueChange={sumValueChangeHandler}
            cardValueChange={cardValueChangeHandler}
            sumValue={sumValue}
            cardValue={cardValue}
            bill={!bill || bill.isPaid ? null : bill}
          />
        )}
      </div>
    </>
  );
};

const mapStateToProps = ({ user, app, chat }) => {
  const profile = user.user || {
    newNotifications: { quantity: 0, messages: [] },
  };
  return {
    user: profile,
    amountCash: profile.amountCash,
  };
};

export default connect(mapStateToProps, {
  read,
})(CreateBill);

<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    private $table = '{{%user}}';
    private $announcer = '{{%user_announcer}}';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->null(),
            'type' => $this->smallInteger()->notNull()->defaultValue(1),
            'username' => $this->string()->notNull()->unique(),
            'firstname' => $this->string()->comment('name'),
            'lastname' => $this->string()->comment('fam or familiya'),
            'auth_key' => $this->string(32)->notNull(),
            'access_token' => $this->string(), // for rest api
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'phone' => $this->string(20),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'is_blocked' => $this->boolean()->notNull()->defaultValue(false),
            'last_visit' => $this->timestamp(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'deleted_at' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable($this->announcer, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'operating_schedule' => $this->string()->comment('график работы'),
            'info' => $this->text()->notNull()->defaultValue(''),
            'equipment' => $this->text()->notNull()->defaultValue('')->comment('оборудование'),
            'avatar' => $this->string(),
            'cost' => $this->integer()->notNull()->defaultValue(0)->comment('стоимость начитки'),
            'discount' => $this->string()->comment('Сделаю скидку'),
            'likes' => $this->integer()->notNull()->defaultValue(0),
            'dislikes' => $this->integer()->notNull()->defaultValue(0),
            'cash_yamoney' => $this->string(),
            'cash_sber' => $this->string(),
            'updated_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
        ], $tableOptions);

        $this->createIndex('IX-user_announcer-user_id', $this->announcer, 'user_id', true);
        $this->addForeignKey(
            'FK-user_announcer-user_id',
            $this->announcer,
            'user_id',
            $this->table,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable($this->announcer);
        $this->dropTable($this->table);
    }
}

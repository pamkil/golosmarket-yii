<?php

namespace common\models\content;

use common\models\Model;

/**
 * Автор публикаций
 *
 * Class Page
 * @package common\models\content
 *
 * @property int $id
 * @property bool $active
 * @property int $position
 * @property string $question
 * @property string $answer
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property int $deleted_at [timestamp]
 */
class Faq extends Model
{
    public static function tableName()
    {
        return '{{%content_faq}}';
    }

    public function rules()
    {
        return [
            [
                ['active'],
                'boolean'
            ],
            [
                ['question', 'answer'],
                'string'
            ],
            [
                ['position'],
                'integer'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Активность',
            'position' => 'Позиция (сортировка)',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }
}
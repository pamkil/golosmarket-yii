<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user_bill}}`.
 */
class m240524_121522_add_card_number_column_to_user_bill_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_bill', 'card_number', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user_bill', 'card_number');
    }
}

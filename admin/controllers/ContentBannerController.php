<?php

namespace admin\controllers;

use admin\models\content\BannerSearch;
use common\models\announcer\File;
use common\models\content\Banner;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class ContentBannerController extends SecurityController
{
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionCreate()
    {
        $model = new Banner();

        $post = Yii::$app->request->post();
        if ($post) {
            $photo = UploadedFile::getInstanceByName('Banner[file]');
            $file = File::setFileByUpload($photo);
            $file->save();
            $post['Banner']['file_id'] = $file->id;
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно создано');

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Создание';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('_form', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        /* @var Banner $model */
        $model = Banner::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            $photo = UploadedFile::getInstanceByName('Banner[file]');
            if ($photo) {
                $file = File::setFileByUpload($photo);
                $file->save();
                $post['Banner']['file_id'] = $file->id;
            }
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Изменение';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('_form', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        /* @var Banner $model */
        $model = Banner::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect('index');
    }
}

<?php

namespace admin\widgets;

use Carbon\Carbon;
use common\models\access\User;
use yii\base\Widget;

class RegisterClient extends Widget
{
    public function run()
    {
        parent::run();

        /**
         * "Новых клиентов за этот/прошлый месяц: 7/15 - смотрим это те кто оплатил или те кто зарегистрировался?"
         * - Я имел ввиду регистрации. Давай формулировку другую сделаем, ато правда непонятно. Зарегистрировано клиентов за этот/прошлый месяц: 7/15
         */

        $dateLast = Carbon::today()->subMonth()->startOfMonth()->toDateTimeString();
        $dateEndLast = Carbon::today()->subMonth()->endOfMonth()->toDateTimeString();
        $dateCur = Carbon::today()->startOfMonth()->toDateTimeString();
        $dateEndCur = Carbon::today()->endOfDay()->toDateTimeString();

        $quantityLast = User::find()
            ->cache(60 * 3)
            ->where(
                [
                    'AND',
                    ['>=', 'created_at', $dateLast],
                    ['<=', 'created_at', $dateEndLast],
                    ['type' => User::TYPE_CLIENT],
                ]
            )
            ->count('id');

        $quantityCur = User::find()
            ->cache(60 * 3)
            ->where(
                [
                    'AND',
                    ['>=', 'created_at', $dateCur],
                    ['<=', 'created_at', $dateEndCur],
                    ['type' => User::TYPE_CLIENT],
                ]
            )
            ->count('id');

        $data = [
            'prev' => $quantityLast ?? 0,
            'cur' => $quantityCur ?? 0,
        ];

        echo $this->render('registerClient', $data);
    }
}
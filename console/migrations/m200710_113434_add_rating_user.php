<?php

use yii\db\Migration;

/**
 * Class m200710_113434_add_rating_user
 */
class m200710_113434_add_rating_user extends Migration
{
    private $user = '{{%user}}';
    private $announcer = '{{%user_announcer}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->user,
            'rating',
            $this->decimal(3, 1)
                ->after('is_blocked')
                ->notNull()
                ->defaultValue(0)
                ->comment('Вычесляемое поле по среднему значению оценки отзывов')
        );

        $this->addColumn(
            $this->user,
            'quantity_reviews',
            $this->integer()
                ->notNull()
                ->defaultValue(0)
                ->after('rating')
                ->comment('Вычесляемое поле количество отзывов у пользователя')
        );

        $this->addColumn(
            $this->user,
            'quantity_favorite',
            $this
                ->integer()
                ->notNull()
                ->defaultValue(0)
                ->after('quantity_reviews')
                ->comment('Вычесляемое поле по суммам всех избранных')
        );

        $this->dropColumn($this->announcer, 'rating');
        $this->dropColumn($this->announcer, 'quantity_reviews');
        $this->dropColumn($this->announcer, 'quantity_favorite');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn($this->announcer, 'rating', $this->decimal(3, 1)->after('discount'));
        $this->addColumn($this->announcer, 'quantity_reviews', $this->integer()->after('rating'));
        $this->addColumn(
            $this->announcer,
            'quantity_favorite',
            $this
                ->integer()
                ->notNull()
                ->defaultValue(0)
                ->after('user_id')
                ->comment('Вычесляемое поле по суммам всех избранных')
        );

        $this->dropColumn($this->user, 'rating');
        $this->dropColumn($this->user, 'quantity_reviews');
        $this->dropColumn($this->user, 'quantity_favorite');
    }
}

<?php

use Carbon\Carbon;
use common\models\access\User;
use common\models\announcer\Announcer;
use common\models\announcer\File;
use common\models\user\Review;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/**
 * @var User $user
 * @var string $name
 * @var Announcer $announcer
 * @var array $weekDays
 * @var int $quantityReview
 * @var Review[] $reviews
 * @var string $timeOnline
 * @var ActiveDataProvider $reviewProvider
 * @var int $ordersCount
 * @var int $paidOrdersCount
 */
?>
<section class="content profile">
    <div class="container">
        <div class="titlepage">
            <div class="icon mobile-no">
                <a href="<?= Url::to(['profile/view', 'id' => $user->id]);?>">
                    <svg xmlns="http://www.w3.org/2000/svg"
                         width="24"
                         height="24"
                         viewBox="0 0 24 24"
                         fill="none"
                         stroke="#000000"
                         stroke-width="2"
                         stroke-linecap="round"
                         stroke-linejoin="round"><path d="M19 12H6M12 5l-7 7 7 7"/></svg>
                </a>
            </div>
            <h1>Отзывы <?= $user->isAnnouncer ? 'об исполнителе' : 'о клиенте' ?>
                <a href="<?= Url::to(['profile/view', 'id' => $user->id]);?>">
                <?= Html::encode($name); ?>
            </a>
            </h1>
        </div>
        <div class="left">
            <div class="announcer-reviews">
                <?php foreach ($reviewProvider->models as $review) : ?>
                    <?php /** @var Review $review */?>
                    <?php if ($review->type_writer != Review::TYPE_WRITE_ANNOUNCER) {
                        $userReview = $review->client;
                        if (!$userReview) {
                            $userReview = User::findDeleted()->where(['id' => $review->client_id])->one();
                        }
                    } else {
                        $userReview = $review->announcer;
                        if (!$userReview) {
                            $userReview = User::findDeleted()->where(['id' => $review->announcer_id])->one();
                        }
                    } ?>
                    <div class="item">
                        <div class="item-left">
                            <div class="image">
                                <div class="box">
                                    <a href="<?= Url::to(['profile/view', 'id' => $userReview->id]); ?>">
                                        <img
                                            src="<?= $userReview->getPhotoUrl() ?>"
                                            class="img-fluid"
                                            alt="<?= Html::encode($userReview->firstname . ' ' . $userReview->lastname) ?>"
                                        >
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="item-right">
                            <div class="recent-review">
                                <a href="<?= Url::to(['profile/view', 'id' => $userReview->id]) ?>">
                                    <div class="name">
                                        <?= $userReview->firstname ?? $userReview->lastname ?? '' ?>
                                    </div>
                                </a>
                                <div class="stars">
                                    <?php $ratingReview = (int)$review->rating; ?>
                                    <?php foreach (range(5, 1) as $item) : ?>
                                        <?php if ($item <= $ratingReview) : ?>
                                            <span class="greenstar">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="11" height="15" viewBox="0 -6 24 24" fill="#77BF22" stroke="#77BF22" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                            </span>
                                        <?php else: ?>
                                            <span class="whitestar">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="11" height="15" viewBox="0 -6 24 24" fill="none" stroke="#77BF22" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                            </span>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                                <div class="date">
                                    <?= Carbon::parse($review->created_at)->format('d.m.Y'); ?>
                                </div>
                            </div>
                            <div class="content">
                                <?= $review->text ?: 'Отзыв без текста' ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?= LinkPager::widget([
                'pagination' => $reviewProvider->pagination,
                'maxButtonCount' => 5,
                'linkOptions' => ['rel' => 'canonical'],
                'prevPageLabel' => false,
                'nextPageLabel' => false,
                'firstPageLabel' => $reviewProvider->pagination->page > 5,
                'lastPageLabel' => $reviewProvider->pagination->page < $reviewProvider->pagination->pageCount - 4,
                'options' => ['tag' => 'div', 'class' => 'pagination'],
                'linkContainerOptions' => ['tag' => 'span'],
            ]); ?>
        </div>

        <div class="right">
            <div class="profile-avatar">
                <div class="image">
                    <div class="box">
                        <img
                            src="<?= $user->getPhotoUrl() ?>"
                            alt="<?= Html::encode($user->firstname . ' ' . $user->lastname) ?>"
                            class="img-fluid"
                        >
                    </div>
                </div>
            </div>

            <div id="profile-info"
                 data-orders="<?= $ordersCount ?>"
                 data-payments="<?= $paidOrdersCount ?>"
                 data-announcer="<?= ($announcer === null ? 'false' : 'true') ?>"
                 data-online="<?= $timeOnline ?>"
                 data-user="<?= $user->id ?>"
                 class="info">
            </div>
        </div>

        <?php
        if ($user->isAnnouncer): ?>
            <div id="save-row" class="save-row"></div>
        <?php endif; ?>
    </div>
</section>
<?php if (isset($page)) : ?>
    <section class="quote">
        <div class="container">
            <div class="content">
                <?php if (!empty($page['title'])) : ?>
                    <h2><?= $page['title']; ?></h2>
                <?php endif; ?>
                <?= $page['body']; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<style>
   .profile .pagination {
       justify-content: center;
    }
</style>

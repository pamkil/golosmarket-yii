<?php

namespace common\models\announcer;

use common\models\access\User;
use common\models\Model;

/**
 * Язык
 *
 * @OA\Schema(title="Язык")
 *
 * Class Language
 * @package common\models\announcer
 *
 * @property int $id
 * @property string $name
 * @property User[] $announcers
 * @property int $position
 * @property bool $active [tinyint(1)]
 * @property string $code [varchar(25)]
 * @property string $menu_title [varchar(255)]
 */
class Language extends Model
{
    public static function tableName()
    {
        return '{{%user_announcer_languages}}';
    }

    public function behaviors()
    {
        $return = parent::behaviors();

        unset($return['timestamp']);

        return $return;
    }

    public function rules()
    {
        return [
            [
                ['name', 'code'],
                'unique'
            ],
            [
                ['name', 'menu_title', 'code'],
                'string'
            ],
            [
                ['position'],
                'number'
            ],
            [
                ['active'],
                'boolean'
            ],
            [
                ['code', 'name', 'menu_title'],
                'required'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }

    public function getAnnouncers()
    {
        return $this->hasMany(User::class, ['id' => 'announcer_id'])
            ->via('{{%user_announcer_2_languages}}', ['language_id' => 'id']);
    }
}

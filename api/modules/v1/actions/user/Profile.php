<?php

namespace api\modules\v1\actions\user;

use api\modules\v1\actions\common\ViewAction;
use api\modules\v1\models\records\auth\User;
use Carbon\Carbon;
use common\models\user\Bill;
use Yii;

class Profile extends ViewAction
{
    /**
     * @OA\Get(
     *   path="/user/profile",
     *   summary="Profile of user. Получение полей пользователя",
     *   tags={"User"},
     *   security={{"BearerAuth": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="query",
     *     description="id of user",
     *     required=false,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Объект ползователя",
     *     @OA\JsonContent(ref="#/components/schemas/User")
     *   )
     * )
     *
     * @param null|int $id
     * @return array
     */
    public function run($id = null)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var User */
        $user = Yii::$app->user->identity;
        if ($user->isAnnouncer) {
            $user->last_visit = Carbon::now()->setTimezone('UTC')->toDateTimeString();
            $user->save();
            $user->refresh();
        }

        return $user ?? null;
    }
}

import ListView from '../Favorites/ListView/ListView';
import { toast, ToastContainer } from 'react-toastify';
import { connect } from 'react-redux';
import React, { useEffect, useState } from 'react';
import { textAddFavorite, textCopy, textRemoveFavorite } from '../Announcer/Announcer';
import copy from 'copy-to-clipboard';
import { FavoriteService } from '../../data/services';
import { announcerChangeFavorite, setFavLocalList } from '../../data/redux';
import { eventPlay, eventStop } from '../Common/PlayerEvent/playerEvent';
import ApiList from '../../data/api/apiList';

import './Collection.css';
import { PLAYER } from '../Common/PlayerEvent/playerConst';
import ShareButton from '../Common/ShareButton/ShareButton';

const Collection = ({
  localDev,
  mobile,
  collection,
  reviews,
  isLogin,
  favoritePage,
  announcerChangeFavorite,
  userProfile,
  favList,
  setFavLocalList,
  isLoadingAnnouncer,
  isAnnouncer,
}) => {
  useEffect(() => {
    document.addEventListener(PLAYER.PLAYING, playingHandler);
    document.addEventListener(PLAYER.STOP, stopHandler);

    return () => {
      document.removeEventListener(PLAYER.PLAYING, playingHandler);
      document.removeEventListener(PLAYER.STOP, stopHandler);
    };
  }, []);

  const [playing, setPlaying] = useState('');

  let announcers = [];
  for (let sound of collection.collectionSounds) {
    let announcer = sound.announcer;
    announcer.sound = [sound.sound];
    announcers.push(announcer);
  }

  const copyInnerHandler = (url) => {
    copy(document.location.origin + url);
    toast.info(textCopy);
  };

  const addToFavoriteHandler = (id, isFavorite, quantityFavorite) => {
    if (isLogin) {
      let service;
      if (isFavorite) {
        service = FavoriteService.removeFromFavorite(id);
        favList.splice(favList.indexOf(id), 1);
      } else {
        service = FavoriteService.addToFavorite(id);
        favList.push(id);
      }
      service.then(({ data: { quantity } }) => {
        announcerChangeFavorite(id, !isFavorite, quantity);
      });
      toast.info(isFavorite ? textRemoveFavorite : textAddFavorite);
    } else {
      if (favList.indexOf(id) >= 0) {
        favList.splice(favList.indexOf(id), 1);

        announcerChangeFavorite(id, false, quantityFavorite - 1);

        toast.info(textRemoveFavorite);
      } else {
        favList.push(id);

        announcerChangeFavorite(id, true, quantityFavorite + 1);

        toast.info(textAddFavorite);
      }

      setFavLocalList(favList);
    }
  };

  const playHandler = (file, itemFiles, announcer) => {
    const files = [];
    for (let annons of [announcers]) {
      for (let item of annons) {
        if (item.sound && item.sound.length > 0) {
          item.sound[0].announcer = item;
          files.push(item.sound[0]);
        }
      }
    }
    eventPlay(announcer, file, files, true);
  };

  const stopHandler = () => {
    setPlaying('');
  };

  const stopClickHandler = () => {
    eventStop();
  };

  const playingHandler = ({ detail: { file } }) => {
    setPlaying(file.url);
  };

  const sendMessageHandler = () => {
    if (!isAnnouncer && !isLogin) {
      window.location.hash = '#registry';
    }
  };

  return (
    <>
      <section className="catalog collection">
        <div className="container">
          <div className="compilation_item">
            <div className="compilation_item--wrapper">
              <div className="compilation_item--content">
                <ShareButton
                  addClass="collection-list"
                  popup="Ссылка на подборку скопированна"
                  isCollection={true}
                  url={document.location.origin + '/collection/' + collection.id}
                />
                <div className="compilation_item--image">
                  <img
                    src={
                      '/upload' +
                      collection.image.path +
                      '/' +
                      collection.image.filename +
                      '.' +
                      collection.image.ext
                    }
                    alt={collection.name}
                  />
                </div>
              </div>
            </div>
          </div>
          <a className="compilation_add-your-demo" href={ApiList.FEEDBACK}>
            Ваше демо тоже может быть в подборке. Напишите нам
          </a>
        </div>
      </section>
      <br />
      <section className="catalog">
        <ListView
          isLoadingAnnouncer={false}
          items={announcers}
          showMore={false}
          showFavorite={false}
          copyHandler={copyInnerHandler}
          addToFavorite={addToFavoriteHandler}
          onClickPlay={playHandler}
          onClickSendMessage={sendMessageHandler}
          onClickStop={stopClickHandler}
          playingSong={playing}
          isAnnouncer={isAnnouncer}
          isLogin={isLogin}
        />
      </section>

      <ToastContainer />
    </>
  );
};

const mapStateToProps = ({ review, announcer, auth, user, app, favLocalList, profile }) => {
  return {
    localDev: app.localDev,
    mobile: app.mobile,
    userProfile: profile.userProfile,
    favList: favLocalList.favList,
    collection: window.collection ? window.collection : [],
    isLoadingAnnouncer: announcer.isLoadingAnnouncer || false,
    isAnnouncer: user.user ? user.user.isAnnouncer : false,
    reviews: review.reviews,
    isLoadingReview: review.isLoading,
    isLogin: !!auth.token,
  };
};

export default connect(mapStateToProps, { setFavLocalList, announcerChangeFavorite })(Collection);

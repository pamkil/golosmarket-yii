<?php

use admin\components\grid\SocketGrid;
use kartik\daterange\DateRangePicker;
use kartik\daterange\DateRangePickerAsset;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;

/**
 *
 * @var array $countsOperator;
 * @var array $countsCities;
 * @var array $statuses;
 * @var array $cities;
 * @var array $operators;
 * @var string $dateDeliveryStart;
 * @var string $dateDeliveryEnd;
 * @var string $dateOrderStart;
 * @var string $dateOrderEnd;
 * @var ArrayDataProvider $operatorsProvider;
 * @var ArrayDataProvider $citiesProvider;
 */
DateRangePickerAsset::register($this);

$columnsOperator = [
    [
        'attribute' => 'operator_id',
        'label' => 'Оператор',
        'enableSorting' => true,
        'format' => 'raw',
        'value' => function ($model) use ($operators, $dateDeliveryStart, $dateDeliveryEnd) {
            $operator = Html::a(
                $operators[$model['operator_id']],
                [
                    'order/index',
                    'operator_id' => $model['operator_id'],
                    'delivery_date_at' => $dateDeliveryStart,
                    'delivery_date_to' => $dateDeliveryEnd
                ]
            );

            return "<b>$operator</b>";
        }
    ]
];
$columnsCity = [
    [
        'attribute' => 'city_id',
        'label' => 'Город',
        'enableSorting' => true,
        'format' => 'raw',
        'value' => function ($model) use ($cities) {
            $city = $cities[$model['city_id']] ?? '';

            return "<b>$city</b>";
        }
    ]
];
$columnAll = [
    'attribute' => 'all',
    'label' => 'Всего',
    'enableSorting' => true,
    'format' => 'raw',
    'value' => function ($model) {
        $all = $model['all'];

        return "<b>$all</b>";
    }
];
$columnsOperator[] = $columnAll;
$columnsCity[] = $columnAll;
foreach ($statuses as $status) {
    $columnsOperator[] = [
        'label' => $status['name'] ?? '',
        'attribute' => $status['code'],
        'enableSorting' => true,
        'value' => function ($model) use ($status) {
            return $model[$status['code']] ?? '';
        }
    ];
    $columnsCity[] = [
        'label' => $status['name'] ?? '',
        'attribute' => $status['code'],
        'enableSorting' => true,
        'value' => function ($model) use ($status) {
            return $model[$status['code']] ?? '';
        }
    ];
}

?>
<style>
    .drp-container {
        display: inline-block;
        width: auto;
        vertical-align: middle;
    }
    .align-middle td {
        text-align: center;
        vertical-align: middle !important;
    }
</style>
<div class="col-md-12">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="form-inline pull-right col-xs-offset-1">
                <div class="form-group">
                    <label for="exampleInputName2">Дата доставки</label>
                    <div class="drp-container">
                        <?= DateRangePicker::widget([
                            'pjaxContainerId' => 'mainPjaxContainer',
                            'name' => 'currentDeliveryOrderDate',
                            'value' => "$dateDeliveryStart-$dateDeliveryEnd",
                            'convertFormat' => true,
                            'presetDropdown' => true,
                            'hideInput' => true,
                            'pluginOptions' => [
                                'locale' => ['format' => 'd.m.Y', 'separator' => '-'],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="form-inline pull-right">
                <div class="form-group">
                    <label for="exampleInputName2">Дата оформления</label>
                    <div class="drp-container">
                        <?= DateRangePicker::widget([
                            'name' => 'currentOrderDate',
                            'value' => "$dateOrderStart-$dateOrderEnd",
                            'convertFormat' => true,
                            'presetDropdown' => true,
                            'hideInput' => true,
                            'pluginOptions' => [
                                'locale' => ['format' => 'd.m.Y', 'separator' => '-'],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>

            <? if (Yii::$app->user->can('statisticCurrentOperatorOrders')) : ?>
                <div class="row"></div>

                <?= SocketGrid::widget([
                    'dataProvider' => $operatorsProvider,
                    'filterSelector' => '[name="currentDeliveryOrderDate"],[name="currentOrderDate"]',
                    'columns' => $columnsOperator,
                    'tableOptions' => [
                        'class' => 'table align-middle table-bordered table-hover order-column table-striped'
                    ]
                ]) ?>
            <? endif; ?>

            <? if (Yii::$app->user->can('statisticCurrentCityOrders')) : ?>
                <div class="row"><hr></div>

                <?= SocketGrid::widget([
                    'dataProvider' => $citiesProvider,
                    'columns' => $columnsCity,
                    'tableOptions' => [
                        'class' => 'table align-middle table-bordered table-hover order-column table-striped'
                    ]
                ]) ?>
            <? endif; ?>
        </div>
    </div>
</div>
<?php

use common\models\access\User;
use kartik\widgets\SwitchInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use common\models\announcer\AnnouncerSound as AnnouncerSoundModel;
use yii\helpers\ArrayHelper;
use common\models\announcer\Age;
use common\models\announcer\Gender;
use common\models\announcer\Presentation;

/**
 * @var User $model
 */
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data' // important
    ]
]) ?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <?= Html::submitButton(
            '<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord ? 'Создать' : 'Сохранить'),
            ['class' => 'btn btn-success']
        ); ?>
        <?php if (!$model->isNewRecord) { ?>
            <?= Html::a(
                '<i class="fa fa-times"></i>&nbsp;Удалить',
                ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'title' => 'Удалить',
                    'data-confirm' => 'Вы уверены что хотите удалить?',
                    'data-method' => 'post'
                ]
            ); ?>
        <?php } ?>
    </div>
    <div class="box-body">
        <?= $form->field($model, 'type')->dropDownList(User::getTypes(), ['disabled' => true]); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'status')->dropDownList(User::getStatuses()); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'blocked_at')
                    ->widget(\kartik\datetime\DateTimePicker::class, [
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'format' => 'php:d.m.Y H:i:s',
                            'todayHighlight' => true,
                            'language' => 'ru',
                        ],
                        'options' => ['autocomplete' => 'off'],
                    ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'firstname'); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'lastname'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'email'); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'phone'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'rating', ['inputOptions' => ['disabled' => true]]); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'quantity_reviews', ['inputOptions' => ['disabled' => true]]); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'quantity_favorite', ['inputOptions' => ['disabled' => true]]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model->announcer, 'sound_by_key')->checkbox(); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model->announcer, 'script')->checkbox(); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model->announcer, 'cost'); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model->announcer, 'cost_a4'); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model->announcer, 'sum', ['inputOptions' => ['disabled' => true]]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model->announcer, 'cash_yamoney'); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model->announcer, 'cash_sber'); ?>
            </div>
        </div>

        <?= $form->field($model->announcer, 'info')->textarea(); ?>
        <?= $form->field($model->announcer, 'equipment')->textarea(); ?>
        <?= $form->field($model->announcer, 'discount'); ?>
        <h3 style="padding-top: 10px;" class="text-center">Все демо</h3>
    <?php

    function convertOptions(array $options)
    {
        $out = [];

        $index = 1;
        foreach ($options as $code => $option) {
            $out[] = [
                'id' => $code,
                'name' => $option,
                'index' => $index
            ];
            $index += 1;
        }

        return $out;
    }

    $presentations = convertOptions(Presentation::getAllList('name'));
    $presentations = ArrayHelper::map($presentations, 'index', 'name');

    $optionLanguages = AnnouncerSoundModel::getAllLanguages();
    $optionLanguages = ArrayHelper::map($optionLanguages, 'id', 'name');

    $ages = convertOptions(Age::getAllList('name'));
    $ages = ArrayHelper::map($ages, 'index', 'name');

    $gender = convertOptions(Gender::getAllList('name'));
    $gender = ArrayHelper::map($gender, 'index', 'name');

    $index = 0;
    $itsRowTime = 0;
    foreach ($model->announcer->sounds as $sound) {
        if ($itsRowTime === 2)
        {
            echo '<div style="border-bottom: 2px solid lightgray;" class="row">';
        }

        if ($itsRowTime !== 1) {
            echo '<div class="col-md-4">';
        } else {
            echo '<div style="border-left: 2px solid lightgray; border-right: 2px solid lightgray;" class="col-md-4">';
        }

        echo $form->field($sound->sound, "[$index]name")->label("Имя");
        $link = $sound->sound->url;
        echo "<a href='$link'>Прослушать демо</a>";
        echo $form->field($sound, "[$index]sound_id")->hiddenInput()->label(false);
        echo $form->field($sound, "[$index]gender_id")->dropDownList($gender, [
            'prompt' => '-'
        ])->label("Пол");
        echo $form->field($sound, "[$index]lang_code")->dropDownList($optionLanguages, [
            'prompt' => '-'
        ])->label("Язык");
        echo $form->field($sound, "[$index]presentation_id")->dropDownList($presentations, [
            'prompt' => '-'
        ])->label("Презентация");
        echo $form->field($sound, "[$index]age_id")->dropDownList($ages, [
            'prompt' => '-'
        ])->label("Возраст");

        echo '</div>';
        
        $index += 1;

        if ($itsRowTime === 2)
        {
            echo '</div>';
            $itsRowTime = 0;
            continue;
        }

        $itsRowTime += 1;
    }
    ?>
</div>

<?php ActiveForm::end(); ?>

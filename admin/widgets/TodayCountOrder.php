<?php

namespace admin\widgets;

use Carbon\Carbon;
use common\models\user\Bill;
use yii\base\Widget;

class TodayCountOrder extends Widget
{
    public function run()
    {
        parent::run();

        /**
         * Сегодня заказов: 11
         */
        $dateCur = Carbon::today()->startOfDay()->toDateTimeString();
        $dateEndCur = Carbon::today()->endOfDay()->toDateTimeString();

        $quantity = Bill::find()
            ->cache(60 * 3)
            ->where(
                [
                    'AND',
                    ['>=', 'created_at', $dateCur],
                    ['<=', 'created_at', $dateEndCur],
//                    ['status' => [Bill::STATUS_PAID_CLIENT, Bill::STATUS_PAID_ANNOUNCER]],
                ]
            )
            ->count('id');

        $data = [
            'title' => 'Сегодня заказов',
            'value' => $quantity ?? 0,
        ];

        echo $this->render('allWidgets', $data);
    }
}
import { useState, useCallback } from "react";
import { getPathNameValue, setPathNameValue } from "./pathName";

const usePathName = (initialValue = []) => {
    let pathName = getPathNameValue();

    const [value, setValue] = useState(pathName);
    const onSetValue = useCallback(
        newValue => {
            setPathNameValue(newValue);
            setValue(getPathNameValue());
        },
        []
    );

    return [value, onSetValue];
}

export default usePathName;
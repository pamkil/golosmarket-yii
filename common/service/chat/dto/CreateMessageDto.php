<?php

namespace common\service\chat\dto;

use Spatie\DataTransferObject\DataTransferObject;

class CreateMessageDto extends DataTransferObject
{
    public int $userId;
    public int $senderId;
    public ?int $clientId = null;
    public ?int $parentId = null;
    public ?bool $isWriteClient = null;
    public string $text;
    public ?array $files = null;

    public static $rules = [
        'userId' => 'required|int',
        'senderId' => 'required|int',
        'clientId' => 'nullable|int',
        'parentId' => 'nullable|int',
        'isWriteClient' => 'nullable|boolean',
        'text' => 'required|string',
        'files' => 'nullable|array',
    ];
}
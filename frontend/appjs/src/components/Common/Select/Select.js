import React from 'react';
import PropTypes from 'prop-types';

const Select = ({label, onChange, options, title, value = ''}) => {
    const changeHandler = (event) => {
        const value = event.target.value;
        onChange(value);
    };
    return (
        <>
            {
                title &&
                <div className="head">{title}</div>
            }
            <select onChange={changeHandler} value={value ? value : ''}>
                <option value="">{label}</option>
                {
                    options.map(({id, name}) =>
                        <option
                            value={id}
                            key={id}>
                            {name}
                        </option>
                    )
                }
            </select>
        </>
    );
};

Select.propTypes = {
    label: PropTypes.string,
    onChange: PropTypes.func,
    options: PropTypes.array
};

Select.defaultProps = {
    label: '',
    onChange: () => {
    },
    options: [],
};

export default Select;
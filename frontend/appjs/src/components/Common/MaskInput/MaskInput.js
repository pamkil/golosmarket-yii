import React from "react";
import InputMask from "react-input-mask";

const MaskInput = props => {
  const {mask, meta, input, placeholder, inputClass, floatPlaceholder} = props;
  const {touched, error, warning} = meta;
  return (
    <>
      <div className={'input-holder'}>
        <InputMask
          mask={mask}
          placeholder={placeholder}
          name={input.name}
          className={(inputClass || '') + (touched && error ? ' error' : '')}
          value={input.value}
          onChange={input.onChange}
          type="text"
        />
        {floatPlaceholder && placeholder ? <span className={'input-placeholder'}>{placeholder}</span> : null}
      </div>
      {
        touched && (
          (error && <div className="error-text">{error}</div>)
          ||
          (warning && <div className="warning-text">{warning}</div>)
        )
      }
    </>
  );
};

export default MaskInput;

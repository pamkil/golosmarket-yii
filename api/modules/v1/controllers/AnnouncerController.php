<?php

namespace api\modules\v1\controllers;

use api\modules\v1\actions\announcer\Index;
use api\modules\v1\actions\announcer\View;
use api\modules\v1\models\records\announcer\Announcer;
use yii\helpers\ArrayHelper;

/**
 * @SWG\Tag(
 *   name="Announcer",
 *   description="Everything about Announcer"
 * )
 */
class AnnouncerController extends RestController
{
    public $modelClass = Announcer::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['optional'] = ['index', 'view'];

        return $behaviors;
    }

    public function checkAccess($action, $model = null, $params = [])
    {

    }

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'index' => [
                'class' => Index::class
            ],
            'view' => [
                'class' => View::class
            ]
        ]);
    }
}

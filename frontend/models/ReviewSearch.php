<?php

namespace frontend\models;

use common\models\Model;
use common\models\user\Review;
use Yii;
use yii\data\ActiveDataProvider;

class ReviewSearch extends Model
{
    public static function search(?int $announcerId, bool $isAnnouncer = true)
    {
        $query = Review::find()
            ->with(['client', 'client.photo', 'announcer', 'announcer.photo'])
            ->where([
                'is_public' => true,
            ]);
        if (isset($announcerId)) {
            $field = $isAnnouncer ? 'announcer_id' : 'client_id';
            $type = $isAnnouncer ? Review::TYPE_WRITE_CLIENT : Review::TYPE_WRITE_ANNOUNCER;
            $query
                ->andFilterWhere([
                    $field => $announcerId,
                    'type_writer' => $type,
            ]);
        }

        return Yii::createObject([
            'class' => ActiveDataProvider::class,
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);
    }
}

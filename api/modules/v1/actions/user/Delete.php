<?php

namespace api\modules\v1\actions\user;

use common\models\access\User;
use Yii;
use yii\rest\DeleteAction;
use yii\web\BadRequestHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class Delete extends DeleteAction
{
    /**
     * @OA\Delete(
     *   path="/user/{id}",
     *   summary="Удаление пользователя",
     *   tags={"User"},
     *   security={{"BearerAuth": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="query",
     *     description="Подтверждение пользователя",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="check",
     *     description="Подтверждение пользователя",
     *     required=true,
     *     @OA\Schema(
     *       type="boolean"
     *     )
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Объект пользователя",
     *     @OA\JsonContent(ref="#/components/schemas/UserEdit")
     *   )
     * )
     */
    public function run($id)
    {
        $userId = Yii::$app->user->identity->getId();

        if ((int) $id !== $userId) {
            throw new NotAcceptableHttpException('Вы не можете удалить выбранного пользователя');
        }

        $user = User::findOne(['id' => $userId]);

        if (empty($user)) {
            throw new NotFoundHttpException('Пользователь не найден');
        }
        if (!$user->delete()) {
            throw new ServerErrorHttpException('Не удалось удалить пользователя');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }
}

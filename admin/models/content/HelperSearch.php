<?php

namespace admin\models\content;

use common\models\content\Helper;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class HelperSearch extends Model
{
    public $code;
    public $body;
    public $active;

    public function rules()
    {
        return [
            [
                ['code', 'body'],
                'string'
            ],
            [
                ['active',],
                'boolean'
            ],
        ];
    }

    public function search($params)
    {
        $query = Helper::find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'active',
                        'body',
                        'code',
                    ]
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $dataProvider->sort->defaultOrder = ['code' => SORT_ASC,];

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere(
            [
                'AND',
                ['like', 'code', $this->code],
                ['like', 'body', $this->body],
                ['active' => $this->active],
            ]
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
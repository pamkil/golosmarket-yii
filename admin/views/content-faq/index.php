<?php

use admin\models\content\FaqSearch;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * @var ActiveDataProvider $dataProvider
 * @var FaqSearch $searchModel
 */
?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <a href="<?= Url::to(['create']) ?>" class="btn bg-olive btn-flat margin">
            Добавить
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                'id',
                'active:boolean',
                'position:integer',
                'question',
                [
                    'attribute' => 'answer',
                    'format' => 'text',
                    'value' => function($model, $key) {
                        return StringHelper::truncate($model->answer, 200);
                    }
                ],
                [
                    'class' => ActionColumn::class,
                    'template' => '{update}  {delete}',
                    'buttonOptions' => ['class' => 'margin']
                ],
            ]
        ]); ?>
    </div>
</div>

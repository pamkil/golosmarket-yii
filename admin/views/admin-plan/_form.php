<?php

use common\models\admin\Plan;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var Plan $model
 */
?>

<? $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data' // important
    ]
]) ?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <?= Html::submitButton('<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord ? 'Создать' : 'Сохранить'), ['class' => 'btn btn-success']) ?>
        <? if (!$model->isNewRecord) { ?>
            <?= Html::a('<i class="fa fa-times"></i>&nbsp;' . 'Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'title' => 'Удалить',
                'data-confirm' => 'Вы уверены что хотите удалить?',
                'data-method' => 'post'
            ]) ?>
        <? } ?>
    </div>
    <div class="box-body">
        <?= $form->field($model, 'date_plan')->widget(DatePicker::class, [
            'pluginOptions' => [
                'format' => 'dd.mm.yyyy',
                'todayHighlight' => true
            ]
        ]); ?>
        <?= $form->field($model, 'value'); ?>
    </div>
</div>

<? ActiveForm::end() ?>

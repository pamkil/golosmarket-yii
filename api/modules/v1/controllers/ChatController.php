<?php

namespace api\modules\v1\controllers;

use api\modules\v1\actions\bill\Cancel as CancelBill;
use api\modules\v1\actions\bill\Create as CreateBill;
use api\modules\v1\actions\bill\Update as UpdateBill;
use api\modules\v1\actions\bill\View as ViewBill;
use api\modules\v1\actions\chat\Create;
use api\modules\v1\actions\chat\Delete;
use api\modules\v1\actions\chat\Index;
use api\modules\v1\actions\chat\ShareFile;
use api\modules\v1\actions\chat\View;
use api\modules\v1\actions\chat\Read;
use common\models\user\Chat;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * @SWG\Tag(
 *   name="Chat",
 *   description="usage chat"
 * )
 */
class ChatController extends RestController
{
    public $modelClass = Chat::class;

    public function behaviors()
    {
        $behaviors = ArrayHelper::merge(
            parent::behaviors(),
            [
                'authenticator' => [
                    'except' => ['options',],
                ],
            ]
        );

        return $behaviors;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        parent::checkAccess($action, $model, $params);

        if ($action !== 'options' && !Yii::$app->user->identity) {
            throw new ForbiddenHttpException(Yii::t('api', 'Access denied'));
        }
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => Index::class,
                'modelClass' => $this->modelClass,
            ],
            'view' => [
                'class' => View::class,
                'modelClass' => $this->modelClass,
            ],
            'create' => [
                'class' => Create::class,
                'modelClass' => $this->modelClass,
            ],
            'read' => [
                'class' => Read::class,
                'modelClass' => $this->modelClass,
            ],
            'shareFile' => [
                'class' => ShareFile::class,
                'modelClass' => $this->modelClass,
            ],
            'delete' => [
                'class' => Delete::class,
                'modelClass' => $this->modelClass,
            ],
            'viewBill' => [
                'class' => ViewBill::class,
                'modelClass' => $this->modelClass,
            ],
            'createBill' => [
                'class' => CreateBill::class,
                'modelClass' => $this->modelClass,
            ],
            'updateBill' => [
                'class' => UpdateBill::class,
                'modelClass' => $this->modelClass,
            ],
            'deleteBill' => [
                'class' => CancelBill::class,
                'modelClass' => $this->modelClass,
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],

        ];
    }
}

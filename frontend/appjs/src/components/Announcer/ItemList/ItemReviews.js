import React from "react";
import avatar from "../../../images/img-ava.png";
import {ReactComponent as Star} from "../../../images/star_review.svg";
import {ReactComponent as StarEmpty} from "../../../images/star_review_empty.svg";
import {connect} from "react-redux";
import config from '../../../data/config/';

const starsArray = [1, 2, 3, 4, 5];
const reviews = [{photo: avatar, username: 'Роман'}, {photo: avatar, username: 'Ромен'}]

const ItemReviews = ({
                       localDev,
                       reviews,
                       quantityStarts,
                       ...props
                     }) => {

  let srvr = localDev ? config.URL : '';

  let btn = <>
    <div className={'tooltip tooltip__abs'}>
      <div className="icon">
        <span>Перейти&nbsp;в&nbsp;отзывы</span>
      </div>
    </div>
    {quantityStarts > 0 && (
      <div className="item_reviews-avatars">
        {reviews.map(({username, photo}, index) => (
          <img
            key={index}
            className="item__reviews-avatar"
            src={srvr + (photo || avatar)}
            alt={username}
          />
        ))}
      </div>
    )}

    <div className="item__reviews-counts">
      {!quantityStarts && <span className="item__reviews-text">
        <span className={'mobile-no'}>Отзывов пока нет&nbsp;</span>
        <span className="mobile-only">Отз. 0</span>
      </span>}
      {quantityStarts > 0 && (
        <span className="item__reviews-text">
              <span className={'mobile-no'}>отзывов&nbsp;</span>
              <span className="item__reviews-count">{quantityStarts}</span>
            </span>
      )}
    </div>
    {/*{*/}
    {/*    text &&*/}
    {/*    <div className="recent-review">*/}
    {/*        <a href={ `/profile/${ clientId }` }>*/}
    {/*            <div className="box">*/}
    {/*                <img src={ photo ? photo : avatar } alt="avatar" />*/}
    {/*            </div>*/}

    {/*            <span className="username">{ username }</span>*/}
    {/*            &nbsp;*/}
    {/*            {*/}
    {/*                starsArray.map((item, key) => (*/}
    {/*                    <span className="greenstar" key={ key }>*/}
    {/*                        {*/}
    {/*                            key < stars ? <Star /> : <StarEmpty />*/}
    {/*                        }*/}
    {/*                    </span>*/}
    {/*                ))*/}
    {/*            }*/}
    {/*        </a>*/}
    {/*    </div>*/}
    {/*}*/}
    {/*<div className="recent-text">*/}
    {/*    { text }*/}
    {/*</div>*/}
  </>

  return (
    quantityStarts ? <a target={'_blank'} href={`/profile/${props.announcer.id}/review`} className="item__reviews">
      {btn}
    </a> : <div className="item__reviews">
      {btn}
    </div>
  )
};

function mapStateToProps({app}) {
  return {localDev: app.localDev};
}

export default connect(mapStateToProps)(ItemReviews);

import { PLAYER } from "./playerConst";

const event = (eventName, data = {}) => {
    const event = new CustomEvent(eventName, {
        detail: data
    });
    document.dispatchEvent(event);
};

const eventPlay = (announcer, file, files = [], show = false) => {
    event(PLAYER.TO_PLAY, {
        announcer,
        file,
        files,
        showPlayer: show
    });
};

const eventPlayed = (file) => {
    event(PLAYER.PLAYING, {
        file,
    });
}

const eventStop = () => {
    event(PLAYER.STOP);
};

export { eventPlay, eventStop, eventPlayed };
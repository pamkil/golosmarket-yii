<?php

namespace admin\controllers;

use admin\models\review\FeedbackSearch;
use common\models\site\Feedback;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class FeedbackController extends SecurityController
{
    public function actionIndex()
    {
        $searchModel = new FeedbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Обратные отзывы';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionUpdate($id)
    {
        /* @var Feedback $model */
        $model = Feedback::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Изменение новости "' . $model->id . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список новостей',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->id;

        return $this->render('_form', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        /* @var Feedback $model */
        $model = Feedback::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect('/news/index');
    }
}
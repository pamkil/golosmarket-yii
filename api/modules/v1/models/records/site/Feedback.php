<?php

namespace api\modules\v1\models\records\site;

/**
 * @OA\Schema(title="Обратная связь")
 */
class Feedback extends \common\models\site\Feedback
{
    /**
     * @OA\Property(property="id", type="integer", description="ИД")
     * @OA\Property(property="user_id", type="integer", description="ИД клиента")
     * @OA\Property(property="status", type="integer", description="Рейтинг")
     * @OA\Property(property="text", type="string", description="Текст отзыва")
     * @OA\Property(property="email", type="string", description="Имя клиента")
     * @OA\Property(property="phone", type="string", description="Аватар клиента")
     * @OA\Property(property="created_at", type="boolean", description="Время создания")
     * @OA\Property(property="updated_at", type="boolean", description="Время обновления")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'user_id',
            'status',
            'email',
            'phone',
            'text',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
        ];
    }
}

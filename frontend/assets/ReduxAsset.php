<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ReduxAsset extends AssetBundle
{
    /**
     * @var string
     *
     *
     */
    public $sourcePath = '@webroot/../appjs/build';

    public $basePath = '@web';

    public function init()
    {
        parent::init();
        $string = file_get_contents(\Yii::getAlias('@webroot') . '/static/asset-manifest.json');
        $jsonFiles = json_decode($string, true);

        $innerJs = $jsonFiles['entrypoints'];

        foreach ($innerJs as $key) {
            if (strpos($key, 'js') > 0) {
                $this->js[] = $key;
            } elseif (strpos($key, 'css') > 0) {
                $this->css[] = $key;
            }
        }
    }
}

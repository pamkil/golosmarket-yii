<?php

namespace api\modules\v1\actions\chat;

use api\modules\v1\actions\common\ViewAction;
use api\modules\v1\models\filters\user\ChatSearch;
use api\modules\v1\models\records\announcer\Opponent;
use api\modules\v1\models\records\auth\User;
use api\modules\v1\models\records\user\Bill;
use api\modules\v1\models\records\user\Chat;
use Yii;

/**
 * @OA\Get(
 *      path="/chat/{id}",
 *      summary="Список сообщений с собеседником, ИД собеседника",
 *      tags={"Chat"},
 *      @OA\Parameter(
 *          name="id",
 *          in="path",
 *          description="User ID, ИД собеседника",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="number Page",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="per_page",
 *          in="query",
 *          description="quantity messages in Page",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="ok",
 *          @OA\JsonContent(
 *              type="array",
 *              @OA\Items(ref="#/components/schemas/Chat")
 *          )
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */
class View extends ViewAction
{
    /** @inheritdoc */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $request = Yii::$app->request;

        $lastId = (int)$request->get('chatId', 0);
        $perPage = (int)$request->get('per_page', 50);
        $isAnnouncer = Yii::$app->user->identity->type === User::TYPE_ANNOUNCER;
        $announcerId = $isAnnouncer ? Yii::$app->user->id : $id;
        $clientId = $isAnnouncer ? $id : Yii::$app->user->id;

        $opponent = Opponent::findOne((int)$id);
        $bill = Bill::getNew($announcerId, $clientId) ?? Bill::getLastPaid($announcerId, $clientId); // {id: opponent && opponent.id, sum: 222, num: user.id}

        $items = (new ChatSearch())->search(
            $id,
            Yii::$app->user->id,
            $isAnnouncer,
            $lastId,
            $perPage
        );

        return [
            'items' => $items,
            'opponent' => $opponent,
            'bill' => $bill,
            'unread' => Chat::getUnread($opponent->id),
            'lastId' => $lastId,
        ];
    }
}

export const URL = {
    LOGIN: '/loginAdmin',
    REGISTER: '/registerAdmin',
    FORGOT: '/registerAdmin',
    LOGOUT: '/registerAdmin',
    PROFILE: '/user/profile',
    HOME: '/',
    LOCATION: {
        CITIES: '/cities',
        REGIONS: '/regions',
        DISTRICT: '/federal-districts',
        COUNTRIES: '/countries',
    },
};

<?php

namespace api\modules\v1\controllers;

use api\modules\v1\actions\auth\RefreshToken;
use api\modules\v1\actions\auth\Register;
use api\modules\v1\actions\auth\Login;
use api\modules\v1\actions\auth\Restore;
use api\modules\v1\models\records\auth\User;
use yii\helpers\ArrayHelper;
use yii\rest\OptionsAction;

/**
 * @OA\Tag(
 *   name="Auth",
 *   description="login, activate, recovery, validate, login by token"
 * )
 */
class AuthController extends RestController
{
    public $modelClass = User::class;

    public function behaviors()
    {
        $behaviors = ArrayHelper::merge(parent::behaviors(), [
            'authenticator' => [
                'except' => ['login', 'refresh', 'options', 'restore', 'create', 'register'],
            ],
        ]);

        return $behaviors;
    }

    public function actions()
    {
        \Yii::$app->request->hostInfo = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
        \Yii::$app->request->baseUrl = '';

        return [
            'options' => [
                'class' => OptionsAction::class,
            ],
            'login' => [
                'class' => Login::class,
            ],
            'refresh' => [
                'class' => RefreshToken::class,
            ],
            'register' => [
                'class' => Register::class,
            ],
            'restore' => [
                'class' => Restore::class,
            ],
        ];
    }

    public function verbs()
    {
        return [
            'login' => ['POST'],
            'create' => ['POST'],
            'restore' => ['POST'],
            'roles' => ['POST', 'GET', 'HEAD'],
        ];
    }
}

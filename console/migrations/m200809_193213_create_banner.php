<?php

use yii\db\Migration;

/**
 * Class m200809_193213_create_banner
 */
class m200809_193213_create_banner extends Migration
{
    private $content_banner = '{{%content_banner}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            $this->content_banner,
            [
                'id' => $this->primaryKey(),
                'active' => $this->boolean()->notNull()->defaultValue(false),
                'position' => $this->integer()->notNull()->defaultValue(0),
                'file_id' => $this->integer()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'updated_at' => $this->dateTime()->notNull(),
                'INDEX `IX-content_banner-position` (position)',
                'INDEX `IX-content_banner-active` (active)',
            ],
            $tableOptions
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->content_banner);
    }
}

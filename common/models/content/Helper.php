<?php

namespace common\models\content;

use common\models\Model;

/**
 * Автор публикаций
 *
 * Class Helper
 * @package common\models\content
 *
 * @property int $id
 * @property bool $active [tinyint(1)]
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property int $deleted_at [timestamp]
 * @property string $code [varchar(255)]
 * @property string $body
 */
class Helper extends Model
{
    public static function tableName()
    {
        return '{{%content_helpers}}';
    }

    public function rules()
    {
        return [

            [
                ['code', 'body'],
                'string'
            ],
            [
                ['active'],
                'boolean'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Активность',
            'code' => 'Символьный код',
            'body' => 'Текст',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }
}
<?php

namespace common\models;

use Carbon\Carbon;
use common\queries\SoftDeleteQuery;
use Exception;
use Throwable;

/**
 * Class SoftDelete
 * @package common\models
 *
 * @property string $deleted_at
 */
class SoftDelete extends Model
{
    public static function find()
    {
        return new SoftDeleteQuery(static::class);
    }

    public static function findDeleted()
    {
        return new SoftDeleteQuery(static::class, true);
    }

    public function delete()
    {
        if (!$this->beforeDelete()) {
            return false;
        }

        $transaction = static::getDb()->beginTransaction();
        try {
            $this->deleted_at = Carbon::now()->toDateTimeString();
            $this->status = 0;
            $result = $this->save();
            $this->afterDelete();
            if ($result === false) {
                $transaction->rollBack();
            } else {
                $transaction->commit();
            }

            return $result;
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function deleteAll($condition = null, $params = [])
    {
        return static::updateAll(['deleted_at' => Carbon::now()->toDateString()], $condition, $params);
    }
}

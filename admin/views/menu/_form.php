<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use admin\widgets\RestCheckbox;
use admin\widgets\AvailableInline;
use common\models\settings\Menu;
use common\models\settings\Lang;

/**
 * @var Menu $model
 */
?>
<?php $form = ActiveForm::begin([
    'options' => [
        'data' => ['key' => $model->id, 'pjax' => true,],
        'enctype' => 'multipart/form-data', // important
    ]
]); ?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <?= $form->field($model, 'active', [
                    'options' => ['tag' => 'div', 'style' => 'display: inline-block;']
                ])
                    ->widget(RestCheckbox::class, ['size' => 'big', 'isAction' => false])->label(false); ?>
                <div class="actions btn-set">
                    <?= Html::submitButton(
                        '<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord
                            ? Yii::t('admin', 'Создать')
                            : Yii::t('admin', 'Сохранить')
                        ),
                        ['class' => 'btn btn-success']
                    ); ?>
                    <?php if (!$model->isNewRecord) : ?>
                        <?= Html::a(
                            '<i class="fa fa-times"></i>&nbsp;' . Yii::t('admin', 'Удалить'),
                            ['delete', 'id' => $model->id],
                            [
                                'class' => 'btn btn-danger',
                                'title' => Yii::t('admin', 'Удалить'),
                                'data-confirm' => Yii::t('admin', 'Вы уверены что хотите удалить?'),
                                'data-method' => 'post'
                            ]
                        ); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_general" data-toggle="tab"><?= Yii::t('admin', 'Общие'); ?></a>
                        </li>
                        <li>
                            <a href="#tab_available" data-toggle="tab"><?= Yii::t('admin', 'Доступность'); ?></a>
                        </li>
                        <?php if (Yii::$app->user->can('viewHistory')) : ?>
                            <li>
                                <a href="#tab_history" data-toggle="tab"><?= Yii::t('admin', 'История'); ?></a>
                            </li>
                        <?php endif; ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_general">
                            <div class="row">
                                <div class="col-sm-6"><?= $form->field($model, 'is_bold')->checkbox(); ?></div>
                                <div class="col-sm-6"><?= $form->field($model, 'is_group')->checkbox(); ?></div>
                                <div class="col-sm-6"><?= $form->field($model, 'class'); ?></div>
                            </div>
                            <?= $form->field($model, 'link'); ?>
                            <?= $form->field($model, 'code'); ?>

                            <div class="tabbable-line">
                                <?php $langs = Lang::getAll();
                                $translations = $model->getVariationModels(); ?>
                                <ul class="nav nav-tabs">
                                    <?php $i = 0; ?>
                                    <?php foreach ($langs as $lang) : ?>
                                        <li class="<?= $i++ == 0 ? 'active' : ''; ?> <?= !$lang->active ? 'hide' : ''; ?>">
                                            <a href="#lang_<?= $lang->id; ?>" data-toggle="tab">
                                                <?= $lang->name; ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                                <div class="tab-content">
                                    <?php $i = 0; ?>
                                    <?php foreach ($langs as $lang) : ?>
                                        <div class="tab-pane fade <?= $i == 0 ? 'in active' : ''; ?>"
                                             id="lang_<?= $lang->id; ?>">
                                            <?php if ($lang->active) $i++; ?>

                                            <?php foreach ($translations as $index => $translation) : ?>
                                                <?php if ($translation->lang_id != $lang->id) continue; ?>

                                                <?= Html::activeHiddenInput(
                                                    $translation, '[' . $index . ']lang_id',
                                                    ['value' => $translation->lang->id]
                                                ); ?>
                                                <?= $form->field($translation, '[' . $index . ']name'); ?>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_available">
                            <?= AvailableInline::widget([
                                'form' => $form,
                                'model' => $model,
                                'isCitiesDomains' => false,
                            ]); ?>
                        </div>
                        <?php if (Yii::$app->user->can('viewHistory')) : ?>
                            <div class="tab-pane" id="tab_history">
                                <?= $this->render('//history/_list', ['model' => $model, 'data' => $model->history]); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

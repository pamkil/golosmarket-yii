<?php

namespace api\modules\v1\models\records\announcer;

/**
 * @OA\Schema(title="Время работы")
 */
class ScheduleWork extends \common\models\user\ScheduleWork
{
    /**
     * @OA\Property(property="id", type="integer", description="ИД диктора")
     * @OA\Property(property="undefined_text", type="string", description="Текст вместо графика работы в профиле")
     * @OA\Property(property="undefined", type="boolean", description="НЕДОСТУПЕН. Планирую длительный перерыв")
     * @OA\Property(property="time_1_at", type="string", description="Начало работы в понедельник")
     * @OA\Property(property="time_1_to", type="string", description="Окончание работы в понедельник")
     * @OA\Property(property="time_2_at", type="string", description="Начало работы во вторник")
     * @OA\Property(property="time_2_to", type="string", description="Окончание работы во вторник")
     * @OA\Property(property="time_3_at", type="string", description="Начало работы в среду")
     * @OA\Property(property="time_3_to", type="string", description="Окончание работы в среду")
     * @OA\Property(property="time_4_at", type="string", description="Начало работы в четверг")
     * @OA\Property(property="time_4_to", type="string", description="Окончание работы в четверг")
     * @OA\Property(property="time_5_at", type="string", description="Начало работы в пятницу")
     * @OA\Property(property="time_5_to", type="string", description="Окончание работы в пятницу")
     * @OA\Property(property="time_6_at", type="string", description="Начало работы в субботу")
     * @OA\Property(property="time_6_to", type="string", description="Окончание работы в субботу")
     * @OA\Property(property="time_7_at", type="string", description="Начало работы в воскресенье")
     * @OA\Property(property="time_7_to", type="string", description="Окончание работы в воскресенье")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'id' => 'user_id',

            'time_1_at',
            'time_1_to',
            'time_2_at',
            'time_2_to',
            'time_3_at',
            'time_3_to',
            'time_4_at',
            'time_4_to',
            'time_5_at',
            'time_5_to',
            'time_6_at',
            'time_6_to',
            'time_7_at',
            'time_7_to',
            'undefined_text',
            'undefined'
        ];
    }

   public function getUndefined()
   {
       return $this->type === self::TYPE_UNDEFINED;
   }

   public function toArray(array $fields = [], array $expand = [], $recursive = true)
   {
       $values = parent::toArray($fields, $expand, $recursive);
       $fields = [
           'time_1_at',
           'time_1_to',
           'time_2_at',
           'time_2_to',
           'time_3_at',
           'time_3_to',
           'time_4_at',
           'time_4_to',
           'time_5_at',
           'time_5_to',
           'time_6_at',
           'time_6_to',
           'time_7_at',
           'time_7_to',
       ];

       foreach ($fields as $field) {
           if (empty($values[$field])) {
               continue;
           }
           $val = explode(':', $values[$field]);
           $val = array_slice($val, 0, 2);

           $values[$field] = implode('-', $val);
       }

       return $values;
   }
}

<?php

namespace common\models\access;

use api\modules\v1\actions\user\Sound;
use api\modules\v1\models\records\announcer\AnnouncerSound;
use Carbon\Carbon;
use common\models\announcer\Announcer;
use api\modules\v1\models\records\announcer\Announcer as ApiAnnouncer;
use common\models\announcer\File;
use common\models\SoftDelete;
use common\models\user\Bill;
use common\models\user\City;
use common\models\user\Favorite;
use common\models\user\ScheduleWork;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property int $id
 * @property int $city_id [int(11)]
 * @property int $photo_id [int(11)]
 * @property int $type [smallint(6)]
 * @property int $rating [smallint(6)]
 * @property int $quantity_reviews [int(11)]
 * @property int $quantity_favorite [int(11)]
 * @property string $username [varchar(255)]
 * @property string $firstname [varchar(255)] name
 * @property string $lastname [varchar(255)] fam or familiya
 * @property string $auth_key [varchar(32)]
 * @property string $access_token [varchar(255)]
 * @property string $password_hash [varchar(255)]
 * @property string $password_reset_token [varchar(255)]
 * @property string $email [varchar(255)]
 * @property string $phone [varchar(20)]
 * @property int $status [smallint(6)]
 * @property bool $is_blocked [tinyint(1)]
 * @property bool $isAnnouncer
 * @property bool $isDeleted
 * @property string $blocked_at [timestamp]
 * @property string $last_visit [timestamp]
 * @property string $created_at [timestamp]
 * @property string $updated_at [timestamp]
 * @property string $verification_token [varchar(255)]
 * @property-read string $password write-only password
 * @property Announcer $announcer
 * @property File $photo
 * @property City $city
 * @property Favorite $favorite
 * @property Favorite $myFavorite
 * @property ScheduleWork $scheduleWork
 * @property Bill[] $bills
 * @property bool $isFavorite
 * @property Announcer[] $recommended
 * @property string $telegram_chat_id [varchar(255)]
 * @property bool $notify_email
 * @property bool $notify_sms
 * @property bool $notify_telegram
 * @property string $telegram_start_id
 */
class User extends SoftDelete implements IdentityInterface
{
    public const ROLE_GUEST = 'guest';
    public const ROLE_ADMIN = 'admin';
    public const ROLE_ANNOUNCER = 'announcer';
    public const ROLE_CLIENT = 'client';

    public const PERMISSION_IS_ADMIN = 'isAdmin';
    public const PERMISSION_IS_ANNOUNCER = 'isAnnouncer';
    public const PERMISSION_IS_CLIENT = 'isClient';

    public const STATUS_DELETED = 0;
    public const STATUS_INACTIVE = 9;
    public const STATUS_ACTIVE = 10;

    public const TYPE_ANNOUNCER = 1;
    public const TYPE_CLIENT = 2;

    public const TYPE_STRING_ANNOUNCER = 'announcer';
    public const TYPE_STRING_CLIENT = 'client';

    public const TOKEN_ACCESS = 1;
    public const TOKEN_REFRESH = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public function formName()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => Carbon::now()->toDateTimeString(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                'telegram_start_id', 'unique'
            ],
            [
                'blocked_at',
                'filter',
                'filter' => function ($value) {
                    if (empty($value)) {
                        return null;
                    }

                    return Carbon::parse($value)->toDateTimeString();
                }
            ],
            [
                'status',
                'default',
                'value' => self::STATUS_ACTIVE
            ],
            [
                'status',
                'in',
                'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]
            ],
            [
                ['city_id', 'photo_id', 'type', 'old_id'],
                'integer'
            ],
            [
                ['type'],
                'in',
                'range' => array_keys(self::getTypes())
            ],
            [
                ['firstname', 'email'],
                'required'
            ],
            [
                ['username', 'email'],
                'unique'
            ],
            [
                ['email'],
                'email'
            ],
            [
                ['password', 'blocked_at'],
                'safe'
            ],
            [
                ['lastname'],
                'string'
            ],
            [
                'phone',
                'filter',
                'filter' => function ($value) {
                    $value = explode(',', $value ?? '');
                    $value = reset($value) ?? '';
                    if ($value && $value[0] === '8') {
                        $value[0] = 7;
                    }
                    if ($value && $value[0] === '7') {
                        $value = '+' . $value;
                    }
                    $value = preg_replace("/[^0-9+]/", "", $value);
                    if (mb_strlen($value) <= 8) {
                        return null;
                    }

                    return $value;
                }
            ],
            [
                ['phone'],
                'string',
                'max' => 20
            ],
            [
                'phone',
                'filter',
                'filter' => function ($value) {
                    return empty($value) ? null : $value;
                }
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'username' => 'username',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'email' => 'email',
            'description' => 'description',
            'type' => 'Тип пользователя',
            'phone' => 'Телефон',
            'status' => 'Статус',
            'rating' => 'Суммарный Рейтинг',
            'is_blocked' => 'Заблокирован',
            'last_visit' => 'Последний визит',

            'quantity_favorite' => 'Количество избранных',
            'quantity_reviews' => 'Рейтинг по отзывам',
            'quantity_rating' => 'Количество оценок',

            'city_id' => 'Город',
            'photo_id' => 'Фото',

            'blocked_at' => 'Заблокирован до',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
            'deleted_at' => 'Время удаления',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::find()
            ->where(
                [
                    'AND',
                    ['id' => $id],
                    ['status' => self::STATUS_ACTIVE],
                    [
                        'OR',
                        ['<', 'blocked_at', Carbon::now()->toDateTimeString()],
                        ['blocked_at' => null]
                    ],
                ]
            )
            ->one();
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = self::TOKEN_ACCESS)
    {
        if ($type === self::TOKEN_REFRESH) {
            return static::findByRefreshToken($token);
        }

        return static::findByAccessToken($token);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken(string $token)
    {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }

    /**
     * Find by Access Token
     *
     * @param $token
     *
     * @return User|ActiveRecord|null
     */
    public static function findByAccessToken($token)
    {
        return static::find()
            ->joinWith('tokens t')
            ->andWhere(['t.token' => $token])
            ->andWhere(['>', 't.expired_at', Carbon::today()->toDateTimeString()])
            ->andWhere(
                [
                    'AND',
                    ['status' => self::STATUS_ACTIVE],
                    [
                        'OR',
                        ['<', 'blocked_at', Carbon::now()->toDateTimeString()],
                        ['blocked_at' => null]
                    ],
                ]
            )
            ->one();
    }

    /**
     * Find by refresh token
     *
     * @param $token
     *
     * @return User|ActiveRecord|null
     */
    public static function findByRefreshToken(string $token)
    {
        return static::find()
            ->joinWith('tokens t')
            ->andWhere(['t.refresh_token' => $token])
            ->andWhere(['>', 't.refresh_expired_at', Carbon::today()->toDateTimeString()])
            ->andWhere(
                [
                    'AND',
                    ['status' => self::STATUS_ACTIVE],
                    [
                        'OR',
                        ['<', 'blocked_at', Carbon::now()->toDateTimeString()],
                        ['blocked_at' => null]
                    ],
                ]
            )
            ->one();
    }

    /**
     * Finds user by username
     *
     * @param string $username
     *
     * @return array|User|ActiveRecord|null
     */
    public static function findByUsername($username)
    {
        return static::find()
            ->where(
                [
                    'AND',
                    ['username' => $username],
                    ['status' => self::STATUS_ACTIVE],
                    [
                        'OR',
                        ['<', 'blocked_at', Carbon::now()->toDateTimeString()],
                        ['blocked_at' => null]
                    ],
                ]
            )
            ->one();
    }

    /**
     * Finds user by username
     *
     * @param $email
     *
     * @return array|User|ActiveRecord|null
     */
    public static function findByEmail($email)
    {
        return static::find()
            ->where(
                [
                    'AND',
                    ['email' => $email],
                    ['status' => self::STATUS_ACTIVE],
                    [
                        'OR',
                        ['<', 'blocked_at', Carbon::now()->toDateTimeString()],
                        ['blocked_at' => null]
                    ],
                ]
            )
            ->one();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     *
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne(['password_reset_token' => $token]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     *
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     *
     * @throws Exception
     */
    public function setPassword(string $password)
    {
        $this->setAttribute('password_hash', Yii::$app->security->generatePasswordHash($password));
    }

    /**
     * Generates "remember me" authentication key
     * @throws Exception
     */
    public function generateAuthKey()
    {
        $this->setAttribute('auth_key', Yii::$app->security->generateRandomString());
    }

    /**
     * Generates new password reset token
     * @throws Exception
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Generates new token for email verification
     */
    public function generateEmailVerificationToken()
    {
        $this->setAttribute('verification_token', Yii::$app->security->generateRandomString() . '_' . time());
    }

    /**
     * @return ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Token::class, ['user_id' => 'id']);
    }

    public function getIsAnnouncer()
    {
        return self::isAnnouncer($this);
    }

    public function getIsDeleted()
    {
        return $this->status === self::STATUS_DELETED;
    }

    public static function isAnnouncer($user)
    {
        return ((int)($user['type'] ?? -1)) === static::TYPE_ANNOUNCER;
    }

    public function getFavorite()
    {
        if ($this->isAnnouncer) {
            return $this->hasMany(Favorite::class, ['announcer_id' => 'id']);
        }

        return $this->hasMany(Favorite::class, ['client_id' => 'id']);
    }

    public function getMyFavorite()
    {
        if ($this->isAnnouncer) {
            return $this->hasMany(Favorite::class, ['client_id' => 'id']);
        }

        return $this->hasMany(Favorite::class, ['announcer_id' => 'id']);
    }

    public function getIsFavorite()
    {
        $isFavorite = false;

        if (!Yii::$app->user->isGuest) {
            /** @var User $user */
            $user = Yii::$app->user->identity;
            $isFavorite = $this->getFavorite()->andWhere([
                $this->isAnnouncer ? 'client_id' : 'announcer_id' => $user->id
            ])->exists();
        }

        return $isFavorite;
    }

    public function getAnnouncer()
    {
        return $this->hasOne(Announcer::class, ['user_id' => 'id'])->inverseOf('user');
    }

    /**
     * @param string $tokenClass
     *
     * @return Token|null|\api\modules\v1\models\records\auth\Token
     */
    public function auth(string $tokenClass = Token::class)
    {
        /** @var Token $token */
        $token = new $tokenClass();
        $token->user_id = $this->id;
        $token->generateToken();

        return $token->save() ? $token : null;
    }

    public static function getTypes()
    {
        return [
            self::TYPE_ANNOUNCER => 'Диктор',
            self::TYPE_CLIENT => 'Клиент',
        ];
    }

    public static function getStringTypes()
    {
        return [
            self::TYPE_STRING_ANNOUNCER => 'Диктор',
            self::TYPE_STRING_CLIENT => 'Клиент',
        ];
    }

    public static function getStringTypesToInt()
    {
        return [
            self::TYPE_STRING_ANNOUNCER => self::TYPE_ANNOUNCER,
            self::TYPE_STRING_CLIENT => self::TYPE_CLIENT,
        ];
    }

    /**
     * @param int $userId
     *
     * @return int|null return type Id/ If null not found user
     */
    public static function getType(int $userId): ?int
    {
        $result = self::find()
            ->select(['type'])
            ->where(['id' => $userId])
            ->scalar();

        if (!isset($result)) {
            return null;
        }

        return (int)$result;
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_DELETED => 'Удален',
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_INACTIVE => 'Регистрация',
        ];
    }

    public function getPhoto()
    {
        return $this->hasOne(File::class, ['id' => 'photo_id']);
    }

    public function getPhotoUrl()
    {
        $url = File::getEmptyAvatarUrl();

        if ($this->status === self::STATUS_DELETED) {
            $url = File::getDeletedAvatarUrl();
        } elseif (!empty($this->photo)) {
            $url = $this->photo->getUrl();
        }

        return $url;
    }

    public function getTypeName()
    {
        return static::getTypes()[$this->type] ?? '';
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getScheduleWork()
    {
        return $this->hasOne(ScheduleWork::class, ['user_id' => 'id']);
    }

    public function getBills()
    {
        return $this->hasMany(Bill::class, ['announcer_id' => 'id']);
    }

    public function getFavoritesCount()
    {
        return count($this->favorite);
    }

    public function getMyFavoritesCount()
    {
        return count($this->myFavorite);
    }

    public function getSkills()
    {
        $skills = [
            'languages' => [],
            'tasks' => [],
            'ages' => [],
            'parodies' => [],
        ];

        if (!empty($this->announcer->sounds)) {
            $languages = [];
            $tasks = [];
            $ages = [];
            $parodies = [];

            /** @var AnnouncerSound $sound */
            foreach ($profile->announcer->sounds ?? [] as $sound) {
                if (!empty($sound->lang_code)) {
                    $languages[] = StringHelper::mb_ucfirst(\Locale::getDisplayLanguage($sound->lang_code));
                }
                if (!empty($sound->presentation->name)) {
                    $tasks[] = $sound->presentation->name;
                }
                if (!empty($sound->age->name)) {
                    $ages[] = $sound->age->name;
                }
                if (!empty($sound->parodist_name)) {
                    $parodies[] = $sound->parodist_name;
                }
            }
            $languages = !empty($languages) ? array_unique($languages) : ['Любой'];
            $tasks = array_unique($tasks);
            $ages = !empty($ages) ? array_unique($ages) : ['Любой'];
            $parodies = !empty($parodies) ? array_unique($parodies) : ['Любой'];
            $skills = [
                'languages' => implode(', ', $languages),
                'tasks' => implode(', ', $tasks),
                'ages' => implode(', ', $ages),
                'parodies' => implode(', ', $parodies),
            ];
        }

        return $skills;
    }

    public function getRecommended()
    {
        if (empty($this->announcer->sounds)) {
            return [];
        }

        $languages = [];
        $genders = [];
        $sounds = $this->announcer->sounds;

        /** @var Sound $sound */
        foreach ($sounds as $sound) {
            if (!empty($sound->lang_code)) {
                $languages[] = $sound->lang_code;
            }
            if (!empty($sound->gender_id)) {
                $genders[] = $sound->gender_id;
            }
        }
        $soundsLanguages = array_unique($languages);
        $soundsGenders = array_unique($genders);

        return ApiAnnouncer::find()
            ->alias('a')
            ->leftJoin(['u' => self::tableName()], 'u.id = a.user_id')
            ->innerJoin(['s' => ApiAnnouncer::table2Sounds()], 's.announcer_id = a.id')
            ->andWhere(['!=', 'a.user_id', $this->id])
            ->andWhere(['>=', 'u.rating', 0.0])
            ->andFilterWhere([
                'IN',
                's.lang_code',
                $soundsLanguages
            ])
            ->andFilterWhere([
                'IN',
                's.gender_id',
                $soundsGenders
            ])
            ->groupBy(['u.id','u.rating'])
            ->orderBy(['u.rating' => SORT_ASC])
            ->limit(3)
            ->all();
    }
}

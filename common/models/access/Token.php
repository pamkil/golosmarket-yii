<?php

namespace common\models\access;

use Carbon\Carbon;
use common\models\Model;
use Yii;
use yii\base\Exception;

/**
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $expired_at
 * @property string $token
 * @property string $refresh_expired_at
 * @property string $refresh_token
 * @property string $created_at [timestamp(0)]
 * @property string $updated_at [timestamp(0)]
 */
class Token extends Model
{
    public static function tableName()
    {
        return '{{%user_token}}';
    }

    public function behaviors()
    {
        return [parent::behaviors()['timestamp']];
    }

    /**
     * @param null $expire
     * @throws Exception
     */
    public function generateToken($expire = null)
    {
        if (!isset($this->token)) {
            $expire = time() + 3600 * 24;
        }
        $this->expired_at = Carbon::parse($expire)->toDateTimeString();
        $this->token = Yii::$app->security->generateRandomString();
    }

    /**
     * @param null $expire
     * @throws Exception
     */
    public function generateRefreshToken($expire = null)
    {
        if (!isset($this->refresh_token)) {
            $expire = time() + 3600 * 24 * 6;
        }
        $this->refresh_expired_at = Carbon::parse($expire)->toDateTimeString();
        $this->refresh_token = Yii::$app->security->generateRandomString(64);
    }

    /**
     * @param $userId
     * @return static
     * @throws Exception
     */
    public static function createToken($userId)
    {
        $token = new static();
        $token->user_id = $userId;
        $time = time() + 3600 * 24;
        $token->generateToken($time);
        $token->generateRefreshToken($time + 3600 * 24 * 5);

        return $token->save() ? $token : null;
    }
}

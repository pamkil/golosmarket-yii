<?php

use yii\db\Migration;

/**
 * Class m200813_151555_add_user_ban_time
 */
class m200813_151555_add_user_ban_time extends Migration
{
    private $user = '{{%user}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->user, 'blocked_at', $this->dateTime()->null()->after('quantity_favorite'));
        $this->createIndex('IX-user-blocked_at', $this->user, 'blocked_at');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->user, 'blocked_at');
    }
}

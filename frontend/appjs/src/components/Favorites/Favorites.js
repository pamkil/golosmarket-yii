import React, {useEffect, useState} from 'react';
import imgBillBoard from '../../images/compilation/billboard.png';
import imgNativeEng from '../../images/compilation/native_eng.png';
import imgFlow from '../../images/compilation/flow.png';
import imgSkit from '../../images/compilation/skit.png';
import imgTop from '../../images/compilation/top.png';
import imgBrand from '../../images/compilation/brand.png';

import {connect} from 'react-redux';
import copy from "copy-to-clipboard";
import {ToastContainer, toast} from 'react-toastify';

import {announcerChangeFavorite, setFavLocalList} from "../../data/redux";
import ListView from "../Announcer/ListView/ListView";
import {FavoriteService} from '../../data/services'

import {eventPlay, eventStop} from "../Common/PlayerEvent/playerEvent";
import {PLAYER} from "../Common/PlayerEvent/playerConst";

const copyHandler = (fileUrl) => {
  copy(document.location.origin + fileUrl);
};

export const textNoLogin = 'Пожалуйста войдите в систему или зарегистрируйтесь';
export const textAddFavorite = 'Голос сохранен в Избранное';
export const textRemoveFavorite = 'Голос удалён из Избранное';
export const textCopy = 'Ссылка на демо исполнителя скопирована';
export const textRegister = 'Почти готово! Для завершения регистрации зайдите в почту и перейдите по ссылке из письма.';
export const textRestore = 'Вам отправлено письмо на указанный адрес';

const Favorites = ({
                     localDev,
                     announcers,
                     reviews,
                     isLogin,
                     announcerChangeFavorite,
                     favList,
                     setFavLocalList,
                     isLoadingAnnouncer,
                     isAnnouncer
                   }) => {
  useEffect(() => {
    document.addEventListener(PLAYER.PLAYING, playingHandler);
    document.addEventListener(PLAYER.STOP, stopHandler);
    return () => {
      document.removeEventListener(PLAYER.PLAYING, playingHandler);
      document.removeEventListener(PLAYER.STOP, stopHandler);
    }
  }, []);

  if (localDev) {
    reviews = [
      {
        clientAvatar: "/images/avatar.png",
        clientName: "Владимир",
        createdAt: "2021-01-29 20:01:04",
        id: 9,
        rating: 5,
        text: "Отличный сервис!",
        updatedAt: "2021-01-29 20:39:32",
        user_id: 1706
      },
      {
        clientAvatar: "/images/avatar.png",
        clientName: "Владимир",
        createdAt: "2021-01-29 20:01:04",
        id: 9,
        rating: 5,
        text: "Отличный сервис!",
        updatedAt: "2021-01-29 20:39:32",
        user_id: 1706
      },
      {
        clientAvatar: "/images/avatar.png",
        clientName: "Владимир",
        createdAt: "2021-01-29 20:01:04",
        id: 9,
        rating: 5,
        text: "Отличный сервис!",
        updatedAt: "2021-01-29 20:39:32",
        user_id: 1706
      },
      {
        clientAvatar: "/images/avatar.png",
        clientName: "Владимир",
        createdAt: "2021-01-29 20:01:04",
        id: 9,
        rating: 5,
        text: "Отличный сервис!",
        updatedAt: "2021-01-29 20:39:32",
        user_id: 1706
      },
      {
        clientAvatar: "/images/avatar.png",
        clientName: "Владимир",
        createdAt: "2021-01-29 20:01:04",
        id: 9,
        rating: 5,
        text: "Отличный сервис!",
        updatedAt: "2021-01-29 20:39:32",
        user_id: 1706
      },
      {
        clientAvatar: "/images/avatar.png",
        clientName: "Владимир",
        createdAt: "2021-01-29 20:01:04",
        id: 9,
        rating: 5,
        text: "Отличный сервис!",
        updatedAt: "2021-01-29 20:39:32",
        user_id: 1706
      },
      {
        clientAvatar: "/images/avatar.png",
        clientName: "Владимир",
        createdAt: "2021-01-29 20:01:04",
        id: 9,
        rating: 5,
        text: "Отличный сервис!",
        updatedAt: "2021-01-29 20:39:32",
        user_id: 1706
      },
      {
        clientAvatar: "/images/avatar.png",
        clientName: "Владимир",
        createdAt: "2021-01-29 20:01:04",
        id: 9,
        rating: 5,
        text: "Отличный сервис!",
        updatedAt: "2021-01-29 20:39:32",
        user_id: 1706
      },
      {
        clientAvatar: "/images/avatar.png",
        clientName: "Владимир",
        createdAt: "2021-01-29 20:01:04",
        id: 9,
        rating: 5,
        text: "Отличный сервис!",
        updatedAt: "2021-01-29 20:39:32",
        user_id: 1706
      },
      {
        clientAvatar: "/images/avatar.png",
        clientName: "Владимир",
        createdAt: "2021-01-29 20:01:04",
        id: 9,
        rating: 5,
        text: "Отличный сервис!",
        updatedAt: "2021-01-29 20:39:32",
        user_id: 1706
      }
    ];
  }

  const [playing, setPlaying] = useState('');
  const [compList] = useState([
    {
      title: 'Голоса для автоответчиков',
      image: imgBillBoard,
      href: '#',
      sound: 'sound.mp3',
      info: 'Послушайте демо отличных голосов для автоответчиков'
    },
    {
      title: 'Носители английского',
      image: imgNativeEng,
      href: '#',
      sound: 'sound.mp3',
      info: 'Только настоящие носители английского, которые отлично прозвучат в вашем проекте'
    },
    {
      title: 'Детские голоса',
      image: imgFlow,
      href: '#',
      sound: 'sound.mp3',
      info: 'Мы подобрали детские голоса, чтобы вам было проще найти исполнителя'
    },
    {
      title: 'Пародии',
      image: imgSkit,
      href: '#',
      sound: 'sound.mp3',
      info: 'Отличные пародии, которые понравились нам. Возможно подойдут для ваших задач'
    },
    {
      title: 'Топ поиска',
      image: imgTop,
      href: '#',
      sound: 'sound.mp3',
      info: 'Мы собрали 5 мужских и 5 женских голосов, у которых больше всего отзывов'
    },
    {
      title: 'Брендвойсы',
      image: imgBrand,
      href: '#',
      sound: 'sound.mp3',
      info: 'Мы собрали голоса, которые слышали по всей стране'
    },
    {
      title: 'Детские голоса',
      image: imgFlow,
      href: '#',
      sound: 'sound.mp3',
      info: 'Мы подобрали детские голоса, чтобы вам было проще найти исполнителя'
    },
    {
      title: 'Топ поиска',
      image: imgTop,
      href: '#',
      sound: 'sound.mp3',
      info: 'Мы собрали 5 мужских и 5 женских голосов, у которых больше всего отзывов'
    },
    {
      title: 'Пародии',
      image: imgSkit,
      href: '#',
      sound: 'sound.mp3',
      info: 'Отличные пародии, которые понравились нам. Возможно подойдут для ваших задач'
    },
    {
      title: 'Носители английского',
      image: imgNativeEng,
      href: '#',
      sound: 'sound.mp3',
      info: 'Только настоящие носители английского, которые отлично прозвучат в вашем проекте'
    },
    {
      title: 'Брендвойсы',
      image: imgBrand,
      href: '#',
      sound: 'sound.mp3',
      info: 'Мы собрали голоса, которые слышали по всей стране'
    }
  ]);

  const addToFavoriteHandler = (id, isFavorite, quantityFavorite) => {
    if (isLogin) {
      let service;

      if (isFavorite) {
        service = FavoriteService.removeFromFavorite(id);
        if (favList.length) {
          favList.splice(favList.indexOf(id), 1);
        }
      } else {
        service = FavoriteService.addToFavorite(id);
        favList.push(id);
      }

      service.then(({data: {quantity}}) => {
        announcerChangeFavorite(
            id,
            !isFavorite,
            !isFavorite
              ? ++quantityFavorite
              : --quantityFavorite
        );
        setFavLocalList(favList);
      });
      toast.info(isFavorite ? textRemoveFavorite : textAddFavorite);
    } else {
      if (isFavorite || favList.indexOf(id) >= 0) {
        if (favList.length) {
          favList.splice(favList.indexOf(id), 1);
        }
        toast.info(textRemoveFavorite);
      } else {
        favList.push(id);
        toast.info(textAddFavorite);
      }

      announcerChangeFavorite(
          id,
          !isFavorite,
          !isFavorite
              ? ++quantityFavorite
              : --quantityFavorite
      );
      setFavLocalList(favList);
    }
  };

  const copyInnerHandler = url => {
    copyHandler(url);
    toast.info(textCopy);
  }

  const playHandler = (file, itemFiles, announcer) => {
    const files = [];
    for (let annons of [announcers]) {
      for (let item of annons) {
        if (item.sound && item.sound.length > 0) {
          item.sound[0].announcer = item;
          files.push(item.sound[0]);
        }
      }
    }
    eventPlay(announcer, file, files, true);
  };

  const stopClickHandler = () => {
    eventStop();
  };

  const stopHandler = () => {
    setPlaying('');
  };

  const playingHandler = ({detail: {file}}) => {
    setPlaying(file.url);
  };

  const sendMessageHandler = () => {
    if (!isAnnouncer && !isLogin) {
      window.location.hash = '#registry';
    }
  };

  return (
    <>
      <section className="catalog">
        <ListView
          isLoadingAnnouncer={isLoadingAnnouncer}
          items={announcers}
          showMore={true}
          showFavorite={false}
          copyHandler={copyInnerHandler}
          addToFavorite={addToFavoriteHandler}
          onClickPlay={playHandler}
          onClickSendMessage={sendMessageHandler}
          playingSong={playing}
          onClickStop={stopClickHandler}
          isAnnouncer={isAnnouncer}
          isLogin={isLogin}
        />
      </section>

      <ToastContainer/>
    </>
  );
}

const mapStateToProps = ({review, announcer, auth, user, app, favLocalList}) => {
  return {
    localDev: app.localDev,
    mobile: app.mobile,
    favList: favLocalList.favList,
    announcers: announcer.announcers,
    isLoadingAnnouncer: announcer.isLoadingAnnouncer || false,
    isAnnouncer: user.user ? user.user.isAnnouncer : false,
    reviews: review.reviews,
    isLoadingReview: review.isLoading,
    isLogin: !!auth.token
  };
};

export default connect(
  mapStateToProps,
  {setFavLocalList, announcerChangeFavorite}
)(Favorites);

import React, {useCallback, useEffect, useState} from 'react'
import LikeButton from "../../Common/LikeButton/LikeButton";
import SendAnnouncerButton from "../../Common/SendAnnouncerButton/SendAnnouncerButton";
import ShareButton from "../../Common/ShareButton/ShareButton";

const ItemButtons = (props) => {
  const {
    id,
    firstname,
    addToFavorite,
    quantityFavorite,
    isFavorite,
    favList,
    favListCount
  } = props

  const [isLocalFav, setIsLocalFav] = useState(false);

  const handleLike = useCallback(() => {
    addToFavorite(id, isFavorite || isLocalFav, quantityFavorite);
  }, []);

  useEffect(() => {
      if (favList.length > 0) {
          setIsLocalFav(favList.indexOf(id) >= 0);
      }
  }, [favListCount]);

  return (
    <div className="item__buttons flex align-center">
      <SendAnnouncerButton {...props} />

      <LikeButton
        onClick={handleLike}
        active={isFavorite || isLocalFav}
        count={quantityFavorite}
      />

      <ShareButton
        announcerId={id}
        announcerName={firstname}
      />
    </div>
  )
}

export default ItemButtons

<?php

use yii\db\Migration;

/**
 * Class m191120_192114_add_reviews
 */
class m191120_192114_add_reviews extends Migration
{
    private $review = '{{%user_review}}';
    private $user = '{{%user}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            $this->review,
            [
                'id' => $this->primaryKey(),
                'announcer_id' => $this->integer()->notNull(),
                'client_id' => $this->integer()->notNull(),
                'is_public' => $this->boolean()->notNull()->defaultValue(true),
                'rating' => $this->tinyInteger()->notNull(),
                'text' => $this->text(),
                'created_at' => $this->timestamp()->notNull(),
                'updated_at' => $this->timestamp(),
                'deleted_at' => $this->timestamp(),
            ],
            $tableOptions
        );
        $this->createIndex(
            'IX-user_review-announcer_id-client_id',
            $this->review,
            ['announcer_id', 'client_id']
        );
        $this->createIndex('IX-user_review-announcer_id', $this->review, 'announcer_id');
        $this->createIndex('IX-user_review-client_id', $this->review, 'client_id');
        $this->createIndex('IX-user_review-is_public', $this->review, 'is_public');

        $this->addForeignKey(
            'FK-user_review-announcer_id',
            $this->review,
            'announcer_id',
            $this->user,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK-user_review-client_id',
            $this->review,
            'client_id',
            $this->user,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK-user_review-client_id', $this->review);
        $this->dropForeignKey('FK-user_review-announcer_id', $this->review);
        $this->dropTable($this->review);
    }
}

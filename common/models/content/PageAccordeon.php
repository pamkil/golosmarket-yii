<?php

namespace common\models\content;

use common\models\Model;
use Yii;

/**
 * This is the model class for table "{{%content_page_accordeon}}".
 *
 * @property int $id
 * @property int $page_id
 * @property string $title
 * @property string $body
 * @property string $created_at
 * @property string $updated_at
 */
class PageAccordeon extends Model
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%content_page_accordeon}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'title', 'body'], 'required'],
            [['page_id'], 'integer'],
            [['body'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'ID страницы',
            'title' => 'Заголовок',
            'body' => 'Текст',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }
}

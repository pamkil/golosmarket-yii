<?php

use yii\data\ActiveDataProvider;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

/**
 * @var ActiveDataProvider $provider
 */
?>
<section class="content contentpage">
    <div class="container">
        <div class="titlepage">
            <a class="icon mobile-no" href="/">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M19 12H6M12 5l-7 7 7 7"/></svg>
            </a>
            <h1>Новости</h1>
        </div>
        <div class="list-news">
            <?= ListView::widget(
                [
                    'dataProvider' => $provider,
                    'itemView' => 'item',
                    'itemOptions' => ['class' => 'item'],
                    'options' => ['class' => 'list-view'],
                    'layout' => "{items}",
                ]
            ) ?>
        </div>
        <?= LinkPager::widget([
            'pagination' => $provider->pagination,
            'options' => ['tag' => 'div', 'class' => 'pagination'],
            'linkOptions' => ['rel' => 'canonical'],
            'linkContainerOptions' => ['tag' => 'span'],
            'prevPageLabel' => false,
            'nextPageLabel' => false,
        ]); ?>
    </div>
</section>
<style>
    .list-news {
        width:100%;
    }
    .list-news .item {
        width:100%;
        margin-bottom:30px;
        background:url(images/bg-news.svg) right 0 top 0 / 40px no-repeat;
    }
    .list-news .item .date {
        color:#A4A4A4;
        font-size:14px;
        text-align: right;
    }
    .list-news .item .description {
        font-size:16px;
        line-height:30px;
        padding-top:15px;
        margin-top:10px;
        border-top:1px dashed #A4A4A4;
    }
    .pagination {
        margin-top:60px;
        display:flex;
        justify-content:space-between;
    }
    .pagination span {
        width:40px;
        height:40px;
        border-radius:5px;
        display:flex;
        justify-content:center;
        align-items:center;
        font-size:15px;
        color:#111;
        transition:all 0.5s;
    }
    .pagination span a{
       color: black;
        text-decoration: none;
    }
    .pagination span.active {
        width:40px;
        height:40px;
        border-radius:5px;
        display:flex;
        justify-content:center;
        align-items:center;
        font-size:15px;
        color:#fff;
        background:#77BF22;
    }
    .pagination span.active a{
       color: white;
    }
    .pagination span:hover {
        color:#77BF22;
        transition:all 0.5s;
        cursor:pointer;
    }
    .pagination span.active:hover {
        color:#fff;
    }
</style>

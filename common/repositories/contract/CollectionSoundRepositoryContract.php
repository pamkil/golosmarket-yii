<?php

namespace common\repositories\contract;

use common\models\content\CollectionSound;

interface CollectionSoundRepositoryContract
{
    public function getAll(): array;
    public function getById(int $collectionId, int $soundId): ?CollectionSound;
    public function existsById(int $collectionId, int $soundId): bool;
}

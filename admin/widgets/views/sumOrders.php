<?php
/** @var string $value */
?>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-green-sharp">
                    <span data-counter="counterup" data-value="<?= $value; ?>">
                        <?= number_format($value, 0, '.', ' '); ?>
                    </span>
                    <small class="font-green-sharp">₽</small>
                </h3>
                <small>Оборот за сегодня</small>
            </div>
            <div class="icon">
                <i class="icon-pie-chart"></i>
            </div>
        </div>
    </div>
</div>


<?php

namespace api\modules\v1\models\checks\auth;

use yii\base\Model;

/**
 * @OA\Schema(required={"email"}, title="Форма восстановления пароля пользователя")
 */
class RecoveryForm extends Model
{
    /**
     * @OA\Property(
     *     type="string",
     *     description="username of email",
     *     title="email",
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
        ];
    }
}
<?php

namespace api\modules\v1\controllers;

use api\modules\v1\actions\user\Bill;
use api\modules\v1\actions\user\Delete;
use api\modules\v1\actions\user\File;
use api\modules\v1\actions\user\Full;
use api\modules\v1\actions\user\Index;
use api\modules\v1\actions\user\PayedUpdate;
use api\modules\v1\actions\user\PayedView;
use api\modules\v1\actions\user\Profile;
use api\modules\v1\actions\user\Telegram;
use api\modules\v1\actions\user\Roles;
use api\modules\v1\actions\user\Schedule;
use api\modules\v1\actions\user\Sound;
use api\modules\v1\actions\user\Update;
use api\modules\v1\models\records\announcer\UserEdit;
use api\modules\v1\models\records\auth\User;
use api\modules\v1\models\records\user\Bill as BillModel;
use yii\rbac\Role;
use yii\rest\OptionsAction;

/**
 * @OA\Tag(
 *   name="User",
 *   description="profile"
 * )
 */
class UserController extends RestController
{
    public $modelClass = User::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['optional'] = ['full'];
        return $behaviors;
    }


    public function actions()
    {
        return [
            'options' => [
                'class' => OptionsAction::class,
            ],
            'profile' => [
                'class' => Profile::class,
                'modelClass' => $this->modelClass,
            ],
            'telegram' => [
                'class' => Telegram::class,
                'modelClass' => $this->modelClass,
            ],
            'bill' => [
                'class' => Bill::class,
                'modelClass' => BillModel::class,
            ],
            'payedView' => [
                'class' => PayedView::class,
                'modelClass' => BillModel::class,
            ],
            'payedUpdate' => [
                'class' => PayedUpdate::class,
                'modelClass' => BillModel::class,
            ],
            'full' => [
                'class' => Full::class,
                'modelClass' => $this->modelClass,
            ],
            'schedule' => [
                'class' => Schedule::class,
                'modelClass' => $this->modelClass,
            ],
            'index' => [
                'class' => Index::class,
                'modelClass' => $this->modelClass,
            ],
            'update' => [
                'class' => Update::class,
                'modelClass' => UserEdit::class,
            ],
            'roles' => [
                'class' => Roles::class,
                'modelClass' => Role::class,
            ],
            'file' => [
                'class' => File::class,
                'modelClass' => $this->modelClass,
            ],
            'sound' => [
                'class' => Sound::class,
                'modelClass' => $this->modelClass,
            ],
            'delete' => [
                'class' => Delete::class,
                'modelClass' => $this->modelClass,
            ],
        ];
    }

    public function verbs()
    {
        return [
            'profile' => ['GET'],
            'telegram' => ['GET'],
            'index' => ['GET', 'HEAD'],
        ];
    }
}


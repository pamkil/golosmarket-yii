<?php

namespace admin\models\user;

use common\models\user\Chat;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;

class ChatSearch extends Model
{
    public $username;
    public $firstname;
    public $lastname;
    public $email;
    public $phone;
    public $last_visit;
    public $status;
    public $is_blocked;
    public $likes;
    public $dislikes;
    public $cost;

    public function rules()
    {
        return [
            [
                ['announcer', 'client',],
                'string'
            ],
        ];
    }

    public function search($params)
    {
        $query = Chat::find()
            ->alias('chat');

        $msgQuery = (new Query())
            ->from(Chat::tableName())
            ->select(new Expression('max(id)'))
            ->groupBy(['announcer_id', 'client_id']);

        $query->where(['chat.id' => $msgQuery]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'chat.created_at'
                ],
                'defaultOrder' => [
                    'chat.created_at' => SORT_DESC
                ],
            ],
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
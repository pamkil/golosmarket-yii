<?php

use admin\models\review\FeedbackSearch;
use common\models\site\Feedback;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\helpers\ArrayHelper;

/**
 * @var ActiveDataProvider $dataProvider
 * @var FeedbackSearch $searchModel
 */
?>

<div class="box box-primary">
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                'id',
                [
                    'attribute' => 'status',
                    'filter' => Feedback::getStatuses(),
                    'filterInputOptions' => ['prompt' => 'Все', 'class' => 'form-control', 'id' => null],
                    'value' => function ($model) {
                        return Feedback::getStatuses()[$model->status] ?? '';
                    }
                ],
                'email',
                'phone',
                'text:text',
                'created_at' => [
                    'class' => DataColumn::class,
                    'attribute' => 'created_at',
                    'format' => 'datetime',
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'hAlign' => GridView::ALIGN_CENTER,
                    'width' => '180px',
                    'filter' => DatePicker::widget(
                        ArrayHelper::merge(
                            [
                                'model' => $searchModel,
                                'attribute' => 'date_at',
                                'attribute2' => 'date_to',
                                'separator' => '-',
                                'type' => DatePicker::TYPE_RANGE,
                                'options' => ['placeholder' => 'От'],
                                'options2' => ['placeholder' => 'До', 'autocomplete' => 'off'],
                            ],
                            Yii::$app->params['datepicker_options']
                        )
                    ),
                ],
                [
                    'class' => ActionColumn::class,
                    'template' => '{update}  {delete}',
                    'buttonOptions' => ['class' => 'margin']
                ],
            ]
        ]); ?>
    </div>
</div>

<?php

namespace common\models\content;

use common\models\Model;

/**
 * Class Question
 * @package common\models\content
 *
 * @property int $id
 * @property string $active
 * @property int $position
 * @property string $text
 * @property Answer[] $answers
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 */
class Question extends Model
{
    public static function tableName()
    {
        return '{{%content_question}}';
    }

    public function rules()
    {
        return [
            [
                ['active'],
                'boolean'
            ],
            [
                ['position'],
                'integer'
            ],
            [
                ['text'],
                'string'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Активность',
            'position' => 'Позиция',
            'text' => 'Содержимое страницы',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }

    public function getAnswers()
    {
        return $this->hasMany(Answer::class, ['question_id' => 'id']);
    }
}
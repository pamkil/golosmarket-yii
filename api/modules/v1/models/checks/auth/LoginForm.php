<?php
/**
 * Created by PhpStorm.
 * User: pamkil
 * Date: 12.02.19
 * Time: 0:27
 */

namespace api\modules\v1\models\checks\auth;

use api\modules\v1\models\records\auth\Token;
use api\modules\v1\models\records\auth\User;
use Carbon\Carbon;
use yii\base\Model;
use yii\db\Query;

/**
 * @OA\Schema(required={"username","password"}, title="Форма входа пользователя")
 */
class LoginForm extends Model
{
    /**
     * @OA\Property(
     *     type="string",
     *     description="username of email",
     *     title="email",
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     type="string",
     *     description="password of User",
     *     title="password",
     * )
     *
     * @var string
     */
    public $password;

    private $_user;

    public function formName()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            [['email'], 'email'],

            ['email', 'checkEmail'],

            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный логин или пароль.');
            }
        }
    }

    public function checkEmail($attribute)
    {
        $userQuery = (new Query)->from(User::tableName())->where(['email' => $this->{$attribute}]);

        if ($userQuery->exists()) {
            $userRow = $userQuery->one();

            if ((int)$userRow['status'] === User::STATUS_DELETED) {
                $this->addError('uniqueEmail', 'Профиль удалён.');
            }
        }
    }

    /**
     * @return Token|null
     * @throws \yii\base\Exception
     */
    public function auth()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $user->last_visit = Carbon::now()->setTimezone('UTC')->toDateTimeString();
            $user->save();
            $user->refresh();

            return Token::createToken($user->id);
        } else {
            return null;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}

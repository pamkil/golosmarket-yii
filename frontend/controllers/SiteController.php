<?php

namespace frontend\controllers;

use api\modules\v1\models\filters\announcer\AnnouncerSearch;
use api\modules\v1\models\filters\user\ReviewSearch;
use common\models\announcer\Announcer;
use common\models\content\Banner;
use common\models\content\Collection;
use common\models\content\Page;
use common\models\content\PageAccordeon;
use frontend\models\ResetPasswordForm;
use frontend\models\RestoreProfileForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     * @throws \JsonException
     */
    public function actionMain($filter = null)
    {
        $queryString = Yii::$app->getRequest()->getQueryString();
        $page = Page::getMainPage($queryString);
        $filter = $this->getFilterByUrl($filter);

        /** @var Banner $banner */
        $banner = Banner::find()->where(['active' => true])->one();

        $this->addSeo($page);
        $this->view->params['hideMobileArrow'] = true;
        $currentFilter = $this->jsonEncode([]);

        if ($filter) {
            // TODO нафиг это тут, нужно разобраться
            $currentFilter = $this->jsonEncode($this->getSelectFilers($filter));
        }

        $reviewsModel = (new ReviewSearch())->search()->getModels() ?? [];
        $reviews = [];
        foreach ($reviewsModel as $model) {
            $reviews[] = $model->toArray();
        }
        $reviewsStr = $this->jsonEncode($reviews);

        $accordion = PageAccordeon::findAll([
            'page_id' => $page['id']
        ]);
        $collections = Collection::findAll(['active' => true]);
        $collectionsStr = $this->jsonEncode($collections);

        $this->view->registerJsVar('filtersList',Json::decode($this->getAllFilters()));
        $this->view->registerJsVar('currentFilter',Json::decode($currentFilter));
        $this->view->registerJsVar('reviews',Json::decode($reviewsStr));
        $this->view->registerJsVar('collectionsList',Json::decode($collectionsStr));

        return $this->render(
            'main',
            [
                'banner' => $banner && $banner->file ? $banner->file->getUrl() : '',
                'page' => $page,
                'accordion' => $accordion
            ]
        );
    }
    private function getFilterByUrl($filter = null)
    {
        if ($filter === null) {
            return null;
        }

        if (!is_string($filter)) {
            throw new NotFoundHttpException();
        }

        $filters = explode('-', $filter);

        if (empty($filters)) {
            throw new NotFoundHttpException();
        }

        $results = [];
        $rule = '/\[(?<filter>\w*)\](?<value>\w*)/';
        foreach ($filters as $filter) {
            preg_match($rule, $filter, $match);
            if (empty($match['filter']) || empty($match['value'])) {
                throw new NotFoundHttpException();
            }
            $results[$match['filter']] = $match['value'];
        }

        return $results;
    }

    /**
     * Displays chatpage.
     *
     * @return mixed
     */
    public function actionChat()
    {
        return $this->render('chat');
    }

    /**
     * Displays chat page.
     *
     * @return mixed
     */
    public function actionHistory()
    {
        return $this->render('history');
    }

    /**
     * Displays favorite page.
     *
     * @return mixed
     */
    public function actionFavorite($filter = null)
    {
        $filter = $this->getFilterByUrl($filter);
        $currentFilter = $this->jsonEncode([]);

        if ($filter) {
            $currentFilter = $this->jsonEncode($this->getSelectFilers($filter));
        }
        return $this->render('favorite', [
            'filters' => $this->getAllFilters(),
            'currentFilter' => $currentFilter,
        ]);
    }

    /**
     * Displays profile page.
     *
     * @param $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAnnouncer($id)
    {
        if (!$id) {
            throw new NotFoundHttpException();
        }

        /** @var Announcer $announcer */
        $announcer = Announcer::find()->where(['id' => (int)$id])->one();

        if (!$announcer) {
            throw new NotFoundHttpException();
        }

        $page = Page::getByPath('announcer', 'announcer/' . $announcer->id);
        $this->addSeo($page);

        return $this->render('profile', ['announcer' => $announcer]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     *
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionReset($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render(
            'resetPassword',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Resets password.
     *
     * @param string $token
     *
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionVerifyEmail(string $token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');

                return $this->render('verifyEmail');
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');

        return $this->goHome();
    }

    /**
     * Resets password.
     *
     * @param string $token
     *
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionRestoreProfile(string $token)
    {
        try {
            $model = new RestoreProfileForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($user = $model->restoreProfile()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your profile has been restored!');

                return $this->render('restoreProfile');
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to restore your account with provided token.');

        return $this->goHome();
    }

    public function actionCollection(int $collectionId)
    {
        $collection = Collection::findOne($collectionId);
        $title = $collection->name ?? '';

        if (!$collection) {
            throw new NotFoundHttpException();
        }

        $this->view->registerJsVar('collection',Json::decode($this->jsonEncode($collection)));

        return $this->render('collection', compact('collection', 'title'));
    }

    private function generateFilterUrl(array $filters = [], string $firstChar = '/filter-')
    {
        $fields = [
            'gender',
            'presentation',
            'language',
            'age',
            'cost',
            'cost_a4',
            'sound_by_key',
            'script',
            'photo',
            'online',
        ];

        $results = [];

        foreach ($fields as $field) {
            if (empty($filters[$field])) {
                continue;
            }
            $value = $filters[$field];
            $results[] = "[$field]$value";
        }

        if (empty($results)) {
            return '';
        }

        return $firstChar . implode('-', $results);
    }
}

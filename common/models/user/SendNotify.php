<?php

namespace common\models\user;

use Carbon\Carbon;
use common\models\Model;

/**
 * Class SendNotify
 *
 * @package common\models\user
 *
 * @property int    $id
 * @property int    $other_id
 * @property int    $status
 * @property int    $type
 * @property string $date
 * @property string $text
 * @property string $created_at
 * @property string $updated_at
 */
class SendNotify extends Model
{
    public const STATUS_NEW = 1; //Новый - Отправили сообщение
    public const STATUS_READ = 3; //Сообщение отправленно, и были сообщения прочитаны - в этом статусе нельзя отправлять повторные сообщения

    public const TYPE_EMAIL = 1;
    public const TYPE_SMS = 2;
    public const TYPE_TELEGRAM = 3;

    public static function tableName()
    {
        return '{{%user_send_notify}}';
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }

    public function rules()
    {
        return [
            [
                ['type'],
                'required',
            ],
            [
                ['type', 'status', 'other_id'],
                'number',
            ],
            [
                ['status'],
                'default',
                'value' => Notification::STATUS_NEW,
            ],
            [
                ['date'],
                'default',
                'value' => function () {
                    return Carbon::now()->toDateTimeString();
                },
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'other_id' => 'Зависимы ID',
            'status' => 'Статус',
            'text' => 'Текст',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public static function nameStatuses()
    {
        return [
            self::STATUS_NEW => 'new',
            self::STATUS_READ => 'read',
        ];
    }

    public static function nameTypes()
    {
        return [
            self::TYPE_EMAIL => 'email',
            self::TYPE_SMS => 'sms',
            self::TYPE_TELEGRAM => 'telegram',
        ];
    }

    public static function send(int $otherId = null, int $type = self::TYPE_EMAIL, string $text = null): SendNotify
    {
        $model = new static();

        $model->type = $type;
        $model->text = $text;
        $model->other_id = $otherId;
        $model->date = Carbon::now()->toDateTimeString();
        $model->save();

        return $model;
    }

    public static function getNotify(int $otherId = null, int $type = self::TYPE_EMAIL): ?SendNotify
    {
        return static::find()
            ->where(
                [
                    'type' => $type,
                    'other_id' => $otherId,
                ]
            )
            ->orderBy(['date' => SORT_DESC])
            ->one();
    }

    public static function remove(int $otherId = null, int $type = self::TYPE_EMAIL): bool
    {
        return !!static::deleteAll(
            [
                'other_id' => $otherId,
                'type' => $type,
            ]
        );
    }

    public static function setNow(int $otherId = null): bool
    {
        return static::updateAll(
            [
                'date' => Carbon::now()->toDateTimeString(),
            ],
            [
                'other_id' => $otherId,
            ]
        );
    }

    public static function read(int $otherId, int $type): bool
    {
        return !!self::updateAll(
            [
                'status' => self::STATUS_READ,
            ],
            [
                'other_id' => $otherId,
                'type' => $type,
            ]
        );
    }
}
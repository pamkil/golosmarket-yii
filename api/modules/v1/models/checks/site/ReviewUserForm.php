<?php

namespace api\modules\v1\models\checks\site;

use yii\base\Model;

/**
 * @OA\Schema(required={"text", "rating"}, title="Форма добавления отзыва о сервисе")
 */
class ReviewUserForm extends Model
{
    /**
     * @OA\Property(
     *     type="string",
     *     description="Текст",
     *     title="text",
     * )
     *
     * @var string
     */
    public $text;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Оценка отзыва",
     *     title="rating",
     * )
     *
     * @var int
     */
    public $rating;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="",
     *     title="opponent_id",
     * )
     *
     * @var int
     */
    public $opponent_id;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Id bill",
     *     title="other_id",
     * )
     *
     * @var int
     */
    public $other_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['other_id', 'opponent_id', 'rating'],
                'integer',
                'min' => 0
            ],
            [
                ['rating'],
                'integer',
                'min' => 1,
                'max' => 5,
            ],
            [
                ['opponent_id', 'rating', 'text'],
                'required',
            ],
            [
                ['text'],
                'string',
                'min' => 5
            ],
        ];
    }
}

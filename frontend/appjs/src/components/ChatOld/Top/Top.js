import React, { useEffect, useMemo, useState } from 'react';
import { ReactComponent as Favorite } from '../../../images/favorite.svg'
import downloadImg from '../../../images/download.svg'
import copyImg from '../../../images/copy.svg'
import { ReactComponent as LeftImg } from '../../../images/left-arrow.svg'
import Price from "../../Common/Price/Price";
import Bill from "../Bill/Bill";
import { ReactComponent as FavoriteRed } from "../../../images/favorite-red.svg";
import CreateBill from "../Bill/CreateBill";
import { eventPlay, eventStop } from "../../Common/PlayerEvent/playerEvent";
import pause from "../../../images/pause.png";
import play from "../../../images/play.png";
import { PLAYER } from "../../Common/PlayerEvent/playerConst";

const Top = (
    {
        addToFavorite,
        opponent,
        isAnnouncer = false,
        createBill,
        cancelBill,
        updateBill,
        bill,
        copyHandler,
        onBackClick,
    }
) => {
    const sound = useMemo(() => {
        if (opponent && opponent.sound && opponent && opponent.sound.length) {
            return opponent.sound[0];
        }

        return null;
    }, [opponent]);
    const [playing, setPlaying] = useState('');
    useEffect(() => {
        document.addEventListener(PLAYER.PLAYING, playingHandler);
        document.addEventListener(PLAYER.STOP, stopHandler);

        return () => {
            document.removeEventListener(PLAYER.PLAYING, playingHandler);
            document.removeEventListener(PLAYER.STOP, stopHandler);
        }
    }, []);
    if (!opponent) {
        return <div className="chat-top"/>;
    }

    const playHandler = () => {
        eventPlay(opponent, sound, opponent.sound, false);
    }
    const stoppingHandler = () => {
        eventStop();
    };

    const stopHandler = () => {
        setPlaying('');
    };

    const playingHandler = ({ detail: { file } }) => {
        setPlaying(file.url);
    };

    return (
        <div className="chat-top">

            <div className="item">
                <div className="icon">
                    <LeftImg onClick={ onBackClick }/>
                </div>
                <a href={ `/profile/${ opponent.id }` } className="profile-link">
                    <div className="miniavachat">
                        <div className="box">
                            <img src={ opponent.avatar } alt={ opponent && opponent.name }/>
                        </div>
                        {
                            opponent.isOnline && <div className="link-icon"/>
                        }
                    </div>
                    <div className="desc">
                        <div className="name">{ opponent && opponent.name }</div>
                        {
                            !isAnnouncer && <div className="price"><Price>{ opponent && opponent.cost }</Price></div>
                        }
                    </div>
                </a>
            </div>
            <div className="item play-chat-block">
                {
                    sound && (
                        playing
                            ? <img src={ pause } alt="Остановить" onClick={ stoppingHandler }/>
                            : <img src={ play } alt="Воспроизвести" onClick={ playHandler }/>
                    )
                }
            </div>
            <div className="item">
                {
                    !isAnnouncer && (
                        <span onClick={ () => addToFavorite(opponent.id, opponent.isFavorite) }>
                            {
                                opponent.isFavorite
                                    ? <FavoriteRed/>
                                    : <Favorite/>
                            }
                        </span>
                    )
                }
                {
                    sound &&
                    <>
                        <a download={ sound.name } target="_blank" href={ sound.url } rel="noopener noreferrer">
                            <img src={ downloadImg } alt="скачать" title="Скачать звуковую дорожку"/>
                        </a>
                        <span className="pointer" onClick={ () => copyHandler(sound.url) }>
                            <img src={ copyImg } alt="копировать" title="Скопировать ссылку на файл"/>
                        </span>
                    </>
                }
            </div>
            {
                isAnnouncer
                    ? (bill
                        ? <CreateBill createBill={ updateBill } bill={ bill } cancelBill={ cancelBill }/>
                        : <CreateBill createBill={ createBill }/>
                    )
                    : <Bill bill={ bill } cancelBill={ cancelBill }/>
            }
        </div>
    );
}

export default Top;

<?php

return yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/test.php'),
    require(__DIR__ . '/main.php'),
    require(__DIR__ . '/main-local.php'),
    [
        'id' => 'app-backend-tests'
    ]
);

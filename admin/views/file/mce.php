<?
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use admin\assets\AppAsset;

AppAsset::register($this);

/**
 * @var ActiveDataProvider $dataProvider
 */
?>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <? if (Yii::$app->user->can('updateFiles')) { ?>
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <span data-href="<?= Url::to(['create']) ?>" class="btn sbold green js-mce-create"><?= Yii::t('admin', 'Добавить') ?>
                                        <i class="fa fa-plus"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                <? } ?>

                <div class="mce-list">
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'options' => ['tag' => 'table'],
                        'itemOptions' => ['tag' => false],
                        'layout' => "<tr>{items}</tr>\n<tr><td colspan=\"5\">{pager}</td></tr>",
                        'itemView' => '_item'
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
<?
$this->registerJs('
$(\'.js-mce-create\').click(function(){
    var href = $(this).attr(\'data-href\');
    win = window.open(href, \'_blank\');
    win.focus();
});

var parentWin = (!window.frameElement && window.dialogArguments) || opener || parent || top;
$(function() {
    var singleClickCalled = false;

    $(\'img.mceImage\').singleAndDouble(
        function (event) {
            singleClickCalled = true;

            var that = $(event.currentTarget);
            aSrc = that.attr(\'src\');
            divInput = $(\'input#\' + parentWin.inputSrc, parent.document).parent().attr(\'id\');
            $(\'input#\' + parentWin.inputSrc, parent.document).val(aSrc);
            $(\'.mce-close\', parent.document).last().trigger(\'click\');

            setTimeout(function () {
                singleClickCalled = false;
            }, 300);
        },
        function (event) {
            if (!singleClickCalled) {
                var that = $(event.currentTarget);
                win = window.open(that.attr("data-edit-url"), \'_blank\');
                win.focus();
            }

            singleClickCalled = false;
        }
    );
});
');
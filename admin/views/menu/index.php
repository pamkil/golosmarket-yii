<?php

use admin\models\setting\Menu;
use common\models\settings\MenuSearch;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use admin\components\grid\SocketGrid;
use yii\i18n\Formatter;

/**
 * @var Menu $model
 *
 * @var ActiveDataProvider $dataProvider
 * @var MenuSearch $searchModel
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <?php if (Yii::$app->user->can('updateMenu')) : ?>
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<?= isset($model)
                                    ? Url::to(['menu/create', 'id' => $model->id])
                                    : Url::to(['menu/create']) ?>"
                                   class="btn sbold green">
                                    <?= Yii::t('admin', 'Добавить меню'); ?>
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <? if (isset($model)) : ?>
                            <div class="col-md-6">
                                <div class="btn-group pull-right">
                                    <a href="<?= Url::to(['menu/update', 'id' => $model->id]); ?>"
                                       class="btn btn-success">
                                        <i class="fa fa-pencil"></i>
                                        <?= Yii::t('admin', 'Редактировать меню'); ?>
                                    </a>
                                </div>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?= SocketGrid::widget([
                'baseClass' => Menu::class,
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'formatter' => [
                    'class' => Formatter::class,
                    'booleanFormat' => ['Нет', 'Да'],
                    'datetimeFormat' => 'php: d.m.y H:i:s'
                ],
                'columns' => [
                    'name' => [
                        'attribute' => 'name',
                        'format' => 'raw',
                        'content' => function($menu) {
                            return Html::a(
                                $menu->name,
                                ['menu/index', 'id' => $menu->id]
                            );
                        }

                    ],
                    'active:boolean',
                    'code',
                    'link',
                    'is_bold:boolean',
                    'is_group:boolean',
                    'updated_at:datetime',
                    [
                        'class' => 'admin\components\grid\DuplicateColumn',
                        'template' => Yii::$app->user->can('updateBouquets') ? '{down} {up}' : '{up} {down}',
                        'visibleButtons' => [
                            'up' => function($model) {
                                return $model->depth > 1;
                            },
                            'down' => function($model) {
                                return $model->depth > 1;
                            },
                        ],
                        'buttons' => [
                            'up' => function ($url, $model) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-upload"></span>',
                                    ['menu/move-up', 'id' => $model->id],
                                    ['title' => Yii::t('admin', 'Вверх'),]
                                );
                            },
                            'down' => function ($url, $model) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-download"></span>',
                                    ['menu/move-down', 'id' => $model->id],
                                    ['title' => Yii::t('admin', 'Вниз'),]
                                );
                            },
                        ]
                    ],
                    [
                        'class' => 'admin\components\grid\DuplicateColumn',
                        'template' => Yii::$app->user->can('updateBouquets') ? '{view} {update}' : '{view}',
                        'visibleButtons' => [
                            'update' => function($model) {
                                return $model->depth > 1;
                            },
                        ],
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-eye-open"></span>',
                                    ['menu/index', 'id' => $model->id],
                                    ['title' => Yii::t('admin', 'Посмотреть'),]
                                );
                            },
                            'update' => function ($url, $model) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-pencil"></span>',
                                    ['menu/update', 'id' => $model->id],
                                    ['title' => Yii::t('admin', 'Редактировать'),]
                                );
                            },
                        ]
                    ]
                ],
            ]); ?>
        </div>
    </div>
</div>

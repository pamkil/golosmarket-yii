<?php
/**
 * @var array $today
 * @var array $yesterday
 */
?>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-green-sharp">
                    <span title="Заказов с прибылью: <?= number_format($today['count'], 0, ',', ' '); ?>"
                          class="tooltips">
                        <?= number_format($today['average'], 0, ',', ' '); ?>
                    </span>
                    <small class="font-green-sharp">₽</small>
                    <span style="font-size: 20px;">
                        (<span title="Заказов с прибылью: <?= number_format($yesterday['count'], 0, ',', ' '); ?>"
                               class="tooltips">
                            <?= number_format($yesterday['average'], 0, ',', ' '); ?>
                        </span>
                        <small class="font-green-sharp">₽</small>)
                    </span>
                </h3>
                <small>Ср. прибыль за сегодня (вчера)</small>
            </div>
            <div class="icon">
                <i class="icon-pie-chart"></i>
            </div>
        </div>
    </div>
</div>
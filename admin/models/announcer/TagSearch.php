<?php

namespace admin\models\announcer;

use common\models\announcer\Tag;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class TagSearch extends Model
{
    public $name;
    public $active;

    public function rules()
    {
        return [
            [
                ['name'],
                'string'
            ],
            [
                ['active',],
                'boolean'
            ],
        ];
    }

    public function search($params)
    {
        $query = Tag::find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'active',
                        'name',
                    ]
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $dataProvider->sort->defaultOrder = ['name' => SORT_ASC,];

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'AND',
                ['like', 'name', $this->name],
                ['active' => $this->active],
            ]
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
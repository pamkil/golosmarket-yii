<?php

namespace api\modules\v1\actions\money;

use api\modules\v1\models\records\user\Bill;
use Carbon\Carbon;
use Yii;
use yii\rest\Action;
use yii\web\BadRequestHttpException;

/**
 * Class Create
 * @throws BadRequestHttpException
 * @package api\modules\v1\actions\money
 *
 */
class Test extends Action
{
    public function run()
    {
        $sum = Yii::$app->request->getQueryParam('sum');
        /** @var Bill $bill */
        $bill = Bill::find()
            ->where([
                'AND',
                ['sum' => $sum],
                ['status' => Bill::STATUS_NEW],
            ])
            ->orderBy(['id' => SORT_DESC])
            ->one();

        if (!$bill) {
            return 'not bill found';
        }

        $bill->ya_datetime = Carbon::now()->toDateTimeString();
        $bill->ya_success = true;
        $bill->status = Bill::STATUS_PAID_CLIENT;

        $bill->save();

        return $bill->id;
    }
}

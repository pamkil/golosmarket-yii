<?php

namespace admin\controllers;

use admin\models\review\ReviewServiceSearch;
use Carbon\Carbon;
use common\models\site\Review;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ReviewServiceController extends SecurityController
{
    public function actionIndex()
    {
        $searchModel = new ReviewServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список отзывов';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel
            ]
        );
    }

    public function actionCreate()
    {
        $model = new Review;

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно создано');

                return $this->redirect(
                    [
                        'update',
                        'id' => $model->id
                    ]
                );
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Создание новости';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список новостей',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render(
            '_form',
            [
                'model' => $model
            ]
        );
    }

    public function actionUpdate($id)
    {
        /* @var Review $model */
        $model = Review::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');

                return $this->redirect(
                    [
                        'update',
                        'id' => $model->id
                    ]
                );
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Изменение отзыва "' . $model->id . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список отзывов',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->id;

        if (isset($model->date)) {
            $model->date = Carbon::parse($model->date)->format('d.m.Y');
        }

        return $this->render(
            '_form',
            [
                'model' => $model
            ]
        );
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        /* @var Review $model */
        $model = Review::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect('/news/index');
    }
}
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $feedback \common\models\site\Feedback */

?>
<div class="feedback">
    <p>Поступила Обратная связь на сайте golosmarket.ru</p>

    <table>
        <tbody>
        <tr>
            <th>E-mail</th>
            <td><?= Html::encode($feedback->email) ?></td>
        </tr>
        <tr>
            <th>Телефон</th>
            <td><?= Html::encode($feedback->phone) ?></td>
        </tr>
        <tr>
            <th>Сообщение</th>
            <td><?= Html::encode($feedback->text) ?></td>
        </tr>
        <tr>
            <th>Пользователь</th>
            <td>
                <?= $feedback->user
                    ? (Html::encode($feedback->user->firstname) . ' (' . $feedback->user_id . ')')
                    : 'Гость' ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>

<?php

/**
 * @var \common\models\user\Chat $model
 */
$page = Yii::$app->request->get('page');
?>
<?php
if (!$model->is_write_client): ?>
    <div class="direct-chat-msg">
        <div class="direct-chat-info clearfix">
            <span class="direct-chat-name pull-left"><?= $model->announcer->lastname ?> <?= $model->announcer->firstname ?></span>
            <span class="direct-chat-timestamp text-right"><?= Yii::$app->formatter->asDatetime(
                    $model->created_at
                ) ?></span>
        </div>
        <img class="direct-chat-img" src="<?= $model->announcer->getPhotoUrl(); ?>" alt="message user image">
        <div class="direct-chat-text">
            <?= $model->text; ?>
        </div>
        <a class="btn btn-box-tool text-right" href="<?= \yii\helpers\Url::to(['delete', 'id' => $model->id, 'page' => $page]) ?>" data-method="DELETE"><i class="fa fa-times"></i></a>
    </div>
<?php
else: ?>
    <div class="direct-chat-msg right">
        <div class="direct-chat-info clearfix">
            <span class="direct-chat-name text-right"><?= $model->client->lastname ?> <?= $model->client->firstname ?></span>
            <span class="direct-chat-timestamp pull-left"><?= Yii::$app->formatter->asDatetime(
                    $model->created_at
                ) ?></span>
        </div>


        <img class="direct-chat-img" src="<?= $model->client->getPhotoUrl(); ?>" alt="message user image">

        <div class="direct-chat-text">
            <?= $model->text; ?>
        </div>
        <a class="btn btn-box-tool" href="<?= \yii\helpers\Url::to(['delete', 'id' => $model->id, 'page' => $page]) ?>" data-method="DELETE"><i class="fa fa-times"></i></a>
    </div>
<?php
endif; ?>
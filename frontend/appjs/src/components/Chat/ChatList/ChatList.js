import React, { Component } from 'react';
import ChatItem from './ChatItem/ChatItem';
import classNames from 'classnames';

export class ChatList extends Component {
    onClick = (item, i, e) => {
        if (this.props.onClick instanceof Function) {
            this.props.onClick(item, i, e);
        }
    }

    onContextMenu = (item, i, e) => {
        e.preventDefault();

        if (this.props.onContextMenu instanceof Function) {
            this.props.onContextMenu(item, i, e);
        }
    }

    onAvatarError = (item, i, e) => {
        if (this.props.onAvatarError instanceof Function) {
            this.props.onAvatarError(item, i, e);
        }
    }

    render() {
        let srvr = this.props.srvr;

        return (
            <div
                ref={this.props.cmpRef}
                className={classNames('rce-container-clist', this.props.className)}>
                {
                    this.props.dataSource.map((item, index) => (
                        <ChatItem
                            id={item.id || index}
                            srvr={srvr}
                            isActive={item.id === this.props.activeChatId}
                            isOnline={item.isOnline}
                            key={index}
                            lazyLoadingImage={this.props.lazyLoadingImage}
                            {...item}
                            onAvatarError={(e) => this.onAvatarError(item, index, e)}
                            onContextMenu={(e) => this.onContextMenu(item, index, e)}
                            onClick={(e) => this.onClick(item, index, e)}
                        />
                    ))
                }
            </div>
        );
    }
}

ChatList.defaultProps = {
    dataSource: [],
    srvr: '',
    onClick: null,
    lazyLoadingImage: undefined,
};

export default ChatList;

import React, {useEffect} from 'react';
import ItemList from "../ItemList/ItemList";
import Skeleton from "../Skeleton/Skeleton";
import Pagination from "../Pagination/Pagination";
import {connect} from "react-redux";

const ListView = (props) => {
  const {
    mobile,
    items,
    filter,
    copyHandler,
    isLoadingAnnouncer,
    favList
  } = props;

  let skeletonItem = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  return (
    <>
      <div className="container">
        {isLoadingAnnouncer ?
          skeletonItem.slice(mobile ? -5 : 0).map((item, ind) => <div key={ind} className="item item-skeleton">
            <Skeleton/></div>)
          : items.map(item =>{
            return <ItemList key={item.id}
                                        {...props}
                                        {...item}
                                        copyHandler={copyHandler}
                                        favList={favList}
                                        announcer={item}
                                        costFilter={filter.currentCost}
          />})
        }
      </div>
      <div className="container">
        <Pagination/>
      </div>
    </>
  )
};

function mapStateToProps({app, favLocalList, filter}) {
  return {
    filter,
    favList: favLocalList.favList,
    mobile: app.mobile
  };
}

export default connect(mapStateToProps)(ListView);

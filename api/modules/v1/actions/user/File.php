<?php

namespace api\modules\v1\actions\user;

use api\modules\v1\actions\common\UpdateAction;
use common\models\announcer\AnnouncerSound;
use Yii;
use yii\web\BadRequestHttpException;

class File extends UpdateAction
{
    /**
     * @OA\Put(
     *   path="/user/file",
     *   summary="Update field Project. Обновление полей проекта",
     *   tags={"User"},
     *   security={{"BearerAuth": {}}},
     *   @OA\Response(
     *     response="200",
     *     description="Объект пользователя",
     *     @OA\JsonContent(ref="#/components/schemas/UserEdit")
     *   )
     * )
     * @param int $id
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id = 0)
    {
        /* @var $model \api\modules\v1\models\records\auth\User */
        $model = $this->findModel((int)Yii::$app->user->id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        $postData = Yii::$app->request->post();
        $transaction = Yii::$app->db->beginTransaction();

        if (empty($postData['field']) || empty($postData['url'])) {
            throw new BadRequestHttpException('Неверные данные для операции');
        }
        try {
            $url = basename($postData['url']);
            if ($postData['field'] === 'photo') {
                if (!empty($model->photo)) {
                    $model->photo->delete();
                    $model->photo_id = null;
                    $model->save();
                }
            } elseif ($postData['field'] === 'photos') {
                if (!empty($model->announcer->photos)) {
                    foreach ($model->announcer->photos as $photo) {
                        $fileUrl = $photo->filename . '.' . $photo->ext;
                        if ($url === $fileUrl) {
                            $photo->delete();
                        }
                    }
                    $model->announcer->save();
                }
            } else {
                /** @var AnnouncerSound $item */
                foreach ($model->announcer->{$postData['field']} as $item) {
                    if (mb_ereg_match($item->sound->filename, $url)) {
                        $filePath = $item->sound->getFullPath();
                        if (file_exists($filePath)) {
                            unlink($filePath);
                            rmdir($item->sound->getPathUpload());
                            rmdir(dirname($item->sound->getPathUpload()));
                        }
                        $item->sound->delete();
                        $item->delete();
                    }
                }

                $model->refresh();
                if (!$model->announcer->vocalExists() && $model->announcer->cost_vocal > 0) {
                    $model->announcer->cost_vocal = 0;
                }
                if (!$model->announcer->parodistExists() && $model->announcer->cost_parody > 0) {
                    $model->announcer->cost_parody = 0;
                }
                $model->announcer->save();
            }

            $transaction->commit();
            Yii::$app->response->statusCode = 202;

            return '';
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }
}

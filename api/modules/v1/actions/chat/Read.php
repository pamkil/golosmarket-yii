<?php

namespace api\modules\v1\actions\chat;

use api\modules\v1\models\records\auth\User;
use api\modules\v1\models\records\user\Chat;
use common\models\user\SendNotify;
use Yii;
use yii\rest\Action;
use yii\web\BadRequestHttpException;

/**
 * @OA\Post(
 *      path="/chat/{id}/read",
 *      summary="Пометить как прочитано",
 *      tags={"Chat"},
 *      @OA\Response(
 *          response="200",
 *          description="Объект токена",
 *          @OA\JsonContent(ref="#/components/schemas/Notification")
 *      ),
 *      @OA\Response(
 *          response="400",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="401",
 *          description="Unauthorized",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */

/**
 * Class Create
 * @package api\modules\v1\actions\feedback
 *
 */
class Read extends Action
{
    /**
     * @param int $id OpponentId
     * @return array
     * @throws BadRequestHttpException
     */
    public function run(int $id)
    {
        if (empty(Yii::$app->user->id)) {
            throw new BadRequestHttpException("Не авторизован");
        }
        if (!Yii::$app->user->identity->type) {
            throw new BadRequestHttpException("Не можете написать этому пользователю");
        }
        if (!$id) {
            throw new BadRequestHttpException("Не задан оппонент у которого вы прочитали");
        }
        $userId = (int) Yii::$app->user->id;
        $ids = Yii::$app->request->getBodyParam('messageIds', []);

        if (!empty($ids)) {
            $ids = array_filter($ids, 'intval');
        }

        if (!$ids) {
            throw new BadRequestHttpException("Не заданы сообщения о прочтении");
        }

        $quantity = 0;
        foreach ($ids as $id) {
            Chat::updateModel($id,['is_read' => true]);
            ++$quantity;
        }

        SendNotify::setNow($userId);

        Yii::$app->response->setStatusCode(201);

        return ['quantity' => $quantity];
    }
}

import React from "react";
import {Field} from "redux-form";
import classes from "classnames";
import {eventPlay} from "../Common/PlayerEvent/playerEvent";

import Loader from "../../images/loader.svg"
import CustomFileInput from "./CustomFileInput";
import ProfileSound from "./ProfileSound";

const ProfileSounds = (
    {
        fields,
        onRemove,
        profile,
        keyForm,
        optionPresentations,
        optionAges,
        optionLanguages,
        optionGenders,
        customLang,
        setCustomLang,
        parodistListIds, setParodistListIds,
        vocalListIds, setVocalListIds,
        onFileUpload,
        fileLoading,
        formStates,
        meta: {error, submitFailed},
        sound,
        isLoading
    }
) => {
    const playHandler = file => {
        const files = [];
        fields.forEach((member, index) => {
            const elem = fields.get(index)
            files.push(elem);
        });
        eventPlay(profile, file, files, true);
    }

    const removeHandler = (file, index) => {
        onRemove(file);
        fields.remove(index);
    };

    return (
        <ul className="sounds">
            {
                fields.map((member, index, manage) => {
                    const record = manage.get(index);

                    if (!record) {
                        return null
                    }

                    const isNewRecord = record.isNew === true;
                    const file = isNewRecord ? {} : record.sound;

                    return (
                        isNewRecord ? null :
                            <li key={index} className={classes('sounds__item', {error: error && error[index]})}>
                                {
                                    ProfileSound(
                                        member, index, sound, isNewRecord, keyForm, playHandler, removeHandler,
                                        file, record,

                                        optionLanguages, optionAges, optionGenders, optionPresentations,

                                        customLang, setCustomLang,
                                        parodistListIds, setParodistListIds,
                                        vocalListIds, setVocalListIds,

                                        formStates
                                    )
                                }
                                {
                                    error && error[index] && <span className="error-text">{error[index]}</span>
                                }
                            </li>
                    )
                })
            }
            <li className="sounds__li-button">
                {fileLoading === 'sounds' && isLoading ?
                    <img alt="preloader" src={Loader}/> :
                    <label htmlFor="file_upload" className="sounds__button">
                        <Field
                            id={"file_upload"}
                            name={`addSounds[]${fields.length}`}
                            keyForm={keyForm}
                            component={CustomFileInput}
                            onChange={() => {
                                fields.push({isNew: true, presentation_id: null});
                                onFileUpload('sound');
                            }}
                            type="file"
                            accept=".mp3,.wav"
                        />
                        <span>Загрузить демо</span>
                    </label>}
            </li>
        </ul>
    )
};

export default ProfileSounds;

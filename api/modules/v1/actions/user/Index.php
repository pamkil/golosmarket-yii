<?php

namespace api\modules\v1\actions\user;

use api\modules\v1\actions\common\IndexAction;
use api\modules\v1\models\filters\user\UserSearch;
use api\modules\v1\models\records\auth\User;
use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataFilter;

class Index extends IndexAction
{
    /**
     * @OA\Get(
     *   path="/user",
     *   summary="Получение списка пользователей",
     *   tags={"User"},
     *   security={{"BearerAuth": {}}},
     *   @OA\Parameter(
     *     name="expand",
     *     in="query",
     *     description="дополнительные поля",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="sort",
     *     in="query",
     *     description="поля сортировки",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="query",
     *     in="query",
     *     description="поля поиска",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Массив пользователей",
     *     @OA\JsonContent(
     *       type="array",
     *       @OA\Items(ref="#/components/schemas/User")
     *     )
     *   )
     * )
     */

    /**
     * @return array|object|ActiveDataFilter
     * @throws InvalidConfigException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $requestParams = Yii::$app->getRequest()->getBodyParams();
        $queryParams = Yii::$app->getRequest()->getQueryParams();
        if (empty($requestParams)) {
            $requestParams = $queryParams;
        }
        /* @var $modelClass User */
        return (new UserSearch())->search($requestParams);
    }
}

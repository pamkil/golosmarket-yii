import React from "react";

const CustomFileInput = ({input, meta, fields, keyForm, multiple, ...params}) => {
    delete input.value;

    return <input
        type="file"
        {...input}
        {...params}
        key={keyForm}
        multiple={multiple}
    />;
};

export default CustomFileInput;

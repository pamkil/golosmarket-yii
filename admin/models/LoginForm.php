<?php

namespace admin\models;

use Yii;
use yii\base\Model;
use common\models\access\User;

class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    public function attributeLabels()
    {
        return [
            'username' => 'Ваш email',
            'password' => 'Ваш пароль',
            'rememberMe' => 'Запомнить меня'
        ];
    }

    public function rules()
    {
        return [
            [
                ['email', 'password'],
                'required',
                'message' => 'Обязательное поле'
            ],
            [
                'email',
                'email'
            ],
            [
                ['rememberMe'],
                'boolean'
            ],
            [
                ['password'],
                'validatePassword',
                'message' => 'Неверный логин или пароль'
            ]
        ];
    }

    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('username', 'Неверный логин или пароль.');
            }
        }
    }

    /**
     * @return bool|User
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }
        if ($this->_user === false) {
            $this->_user = User::findByPhone($this->username);
        }

        return $this->_user;
    }

    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            
            return Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }
}

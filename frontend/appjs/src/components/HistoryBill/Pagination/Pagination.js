import React, { useMemo } from 'react';
import classes from "classnames";

const Item = ({ pageNumber, isActive, onClick, canClick, addClasses = '' }) => (
    <span
        className={ classes({'active' : isActive}, addClasses) }
        onClick={ () => canClick ? onClick(pageNumber) : null }
    >
        { pageNumber }
    </span>
);

const Pagination = ({ pagination: { page: activePage = 1, quantityPages = 0, }, setPage }) => {
    const pages = useMemo(() => {
        const pages = [];
        for (
            let page = 1;
            page <= quantityPages;
            page++
        ) {
            if (page === 1
                || page === quantityPages
                || (
                    page > activePage - 3
                    && page < activePage + 3
                )
            ) {
                pages.push(<Item
                    key={ page }
                    pageNumber={ page }
                    isActive={ activePage === page }
                    canClick={ true }
                    onClick={ () => setPage(page) }
                    addClasses={ page !== 1 && ((activePage - page > 1) || (activePage - page < -1) || (activePage === 4 && page === 1)) ? 'mobile-no' : '' }
                />);
            } else if (page === activePage - 3 || page === activePage + 3) {
                pages.push(<Item
                    key={ page + '...' }
                    pageNumber={ '...' }
                    isActive={ false }
                    canClick={ false }
                    addClasses={page === activePage + 3 ? 'mobile-no' : ''}
                />);
            }
        }

        return pages;
    }, [activePage, quantityPages, setPage]);

    if (quantityPages <= 1) {
        return null;
    }

    return (
        <div className="pagination">
            { pages }
        </div>
    )
};

export default Pagination;
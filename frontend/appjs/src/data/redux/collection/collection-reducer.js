import {CollectionService} from "../../services";

export const COLLECTION = {
  COLLECTION_LOADING: 'golosmarket/announcer/COLLECTION_LOADING',
  COLLECTION_LOADED: 'golosmarket/announcer/COLLECTION_LOADED',
};

export function requestCollections() {
  return async dispatch => {
    dispatch({
      type: COLLECTION.COLLECTION_LOADING
    });

    try {
      const response = await CollectionService.get();
      const data = {
        data: response.data
      };

      dispatch({
        type: COLLECTION.COLLECTION_LOADED,
        payload: {
          collection: data.data
        }
      });
    } catch (e) {
      console.error(e);
    }
  }
}

export const collection = (state = initialState, action) => {
  switch (action.type) {
    case COLLECTION.COLLECTION_LOADED:
      const {collection = []} = action.payload;
      return {
        ...initialState,
        collection,
        isLoadingCollection: false
    }
  }
}

const initialState = {
  isLoadingCollection: true,
  collection: []
};

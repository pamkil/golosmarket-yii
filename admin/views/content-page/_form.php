<?php

use common\models\announcer\Age;
use common\models\announcer\Gender;
use common\models\announcer\Language;
use common\models\announcer\Presentation;
use common\models\content\Page;
use common\models\content\PageFilter;
use common\models\content\PageSimilar;
use dosamigos\tinymce\TinyMce;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use unclead\multipleinput\MultipleInput;

/**
 * @var Page        $model
 * @var PageFilter  $filter
 * @var PageSimilar $similar
 */

$form = ActiveForm::begin(
    [
        'options' => [
            'enctype' => 'multipart/form-data' // important
        ],
    ]
) ?>
<div class="box box-primary">
    <div class="box-header with-border text-right">
        <?= Html::submitButton(
            '<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord ? 'Создать' : 'Сохранить'),
            ['class' => 'btn btn-success']
        ) ?>
        <?php if (!$model->isNewRecord) : ?>
            <?= Html::a(
                '<i class="fa fa-times"></i>&nbsp;' . 'Удалить',
                ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'title' => 'Удалить',
                    'data-confirm' => 'Вы уверены что хотите удалить?',
                    'data-method' => 'post',
                ]
            ) ?>
        <?php endif; ?>
    </div>
    <div class="box-body">
        <div class="tabbable-line">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_general" data-toggle="tab">Общие</a>
                </li>
                <?php if(!$model->isNewRecord) : ?>
                    <li>
                        <a href="#tab_accordeon" data-toggle="tab">Аккордеон</a>
                    </li>
                <?php endif; ?>
                <li>
                    <a href="#tab_after" data-toggle="tab">Содержимое страницы</a>
                </li>
                <li>
                    <a href="#tab_filter" data-toggle="tab">Фильтр дикторов на странице</a>
                </li>
                <li>
                    <a href="#tab_similar" data-toggle="tab">Похожие дикторы на странице</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_general">
                    <div class="box-body">
                        <?= $form->field($model, 'code') ?>
                        <?= $form->field($model, 'path') ?>
                        <?= $form->field($model, 'title') ?>
                        <?= $form->field($model, 'h1') ?>
                        <?= $form->field($model, 'description') ?>
                        <?= $form->field($model, 'keywords') ?>
                    </div>
                </div>
                <div class="tab-pane" id="tab_filter">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'show_announcers')->checkbox(); ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'show_filters')->checkbox(); ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-3">
                                        <?= $form->field($filter, 'sound_by_key')->checkbox(); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?= $form->field($filter, 'script')->checkbox(); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?= $form->field($filter, 'photo')->checkbox(); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?= $form->field($filter, 'online')->checkbox(); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4">
                                        <?= $form->field($filter, 'cost_by_a4')->checkbox(); ?>
                                    </div>

                                    <div class="col-md-4">
                                        <?= $form->field($filter, 'cost_at'); ?>
                                    </div>
                                    <div class="col-md-4">
                                        <?= $form->field($filter, 'cost_to'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <?= $form->field($filter, 'gender_id')->widget(
                                    Select2::class,
                                    [
                                        'options' => ['placeholder' => 'Выберите значение'],
                                        'data' => Gender::getAllList('name', 'id'),
                                        'pluginOptions' => ['allowClear' => true],
                                    ],
                                ); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($filter, 'age_id')->widget(
                                    Select2::class,
                                    [
                                        'options' => ['placeholder' => 'Выберите значение'],
                                        'data' => Age::getAllList('name', 'id'),
                                        'pluginOptions' => ['allowClear' => true],
                                    ],
                                ); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($filter, 'presentation_id')->widget(
                                    Select2::class,
                                    [
                                        'options' => ['placeholder' => 'Выберите значение'],
                                        'data' => Presentation::getAllList('name', 'id'),
                                        'pluginOptions' => ['allowClear' => true],
                                    ],
                                ); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($filter, 'language_id')->widget(
                                    Select2::class,
                                    [
                                        'options' => ['placeholder' => 'Выберите значение'],
                                        'data' => Language::getAllList('name', 'id'),
                                        'pluginOptions' => ['allowClear' => true],
                                    ],
                                ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <?= $form->field($filter, 'count'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_after">
                    <div class="box-body">
                        <?= $form->field($model, 'body')->widget(
                            TinyMce::class,
                            Yii::$app->params['tinyMceOptions'] ?? ''
                        ); ?>
                        <?= $form->field($model, 'after_body')->widget(
                            TinyMce::class,
                            Yii::$app->params['tinyMceOptions'] ?? ''
                        ); ?>
                    </div>
                </div>
                <?php if(!$model->isNewRecord) : ?>
                    <div class="tab-pane" id="tab_accordeon">
                        <div class="box-body">
                            <?= $form->field($model, 'accordeon')->widget(
                                MultipleInput::class,
                                [
                                    'min' => 0,
                                    'addButtonPosition' => MultipleInput::POS_FOOTER,
                                    'columns' => [
                                        [
                                            'name' => 'page_id',
                                            'type' => 'hiddenInput',
                                            'defaultValue' => $model->id ?? '',
                                        ],
                                        [
                                            'name' => 'title',
                                            'type' => 'textInput'
                                        ],
                                        [
                                            'name' => 'body',
                                            'type' => TinyMce::class,
                                            'options' => Yii::$app->params['tinyMceOptions'] ?? ''
                                        ],
                                    ]
                                ]
                            ); ?>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="tab-pane" id="tab_similar">
                    <div class="box-body">
                        <?= $form->field($model, 'show_similar')->checkbox(); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-3">
                                        <?= $form->field($similar, 'sound_by_key')->checkbox(); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?= $form->field($similar, 'script')->checkbox(); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?= $form->field($similar, 'photo')->checkbox(); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?= $form->field($similar, 'online')->checkbox(); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4">
                                        <?= $form->field($similar, 'cost_by_a4')->checkbox(); ?>
                                    </div>

                                    <div class="col-md-4">
                                        <?= $form->field($similar, 'cost_at'); ?>
                                    </div>
                                    <div class="col-md-4">
                                        <?= $form->field($similar, 'cost_to'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <?= $form->field($similar, 'gender_id')->widget(
                                    Select2::class,
                                    [
                                        'options' => ['placeholder' => 'Выберите значение'],
                                        'data' => Gender::getAllList('name', 'id'),
                                        'pluginOptions' => ['allowClear' => true],
                                    ],
                                ); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($similar, 'age_id')->widget(
                                    Select2::class,
                                    [
                                        'options' => ['placeholder' => 'Выберите значение'],
                                        'data' => Age::getAllList('name', 'id'),
                                        'pluginOptions' => ['allowClear' => true],
                                    ],
                                ); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($similar, 'presentation_id')->widget(
                                    Select2::class,
                                    [
                                        'options' => ['placeholder' => 'Выберите значение'],
                                        'data' => Presentation::getAllList('name', 'id'),
                                        'pluginOptions' => ['allowClear' => true],
                                    ],
                                ); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($similar, 'language_id')->widget(
                                    Select2::class,
                                    [
                                        'options' => ['placeholder' => 'Выберите значение'],
                                        'data' => Language::getAllList('name', 'id'),
                                        'pluginOptions' => ['allowClear' => true],
                                    ],
                                ); ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <?= $form->field($similar, 'count'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end() ?>

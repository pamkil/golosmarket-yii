<?php

namespace admin\controllers;

use admin\models\user\ClientSearch;
use common\models\access\User;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class UserClientController extends SecurityController
{
    public function actionIndex()
    {
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

//    public function actionCreate()
//    {
//        $model = new User();
//
//        $post = Yii::$app->request->post();
//        if ($post) {
//            if ($model->load($post) && $model->save()) {
//                Yii::$app->session->addFlash('success', 'Успешно создано');
//
//                return $this->redirect([
//                    'update',
//                    'id' => $model->id
//                ]);
//            }
//
//            foreach ($model->getErrors() as $error) {
//                Yii::$app->session->addFlash('error', $error);
//            }
//        }
//
//        $this->view->title = 'Создание';
//        $this->view->params['breadcrumbs'][] = [
//            'label' => 'Список',
//            'url' => ['index']
//        ];
//        $this->view->params['breadcrumbs'][] = $this->view->title;
//
//        return $this->render('_form', [
//            'model' => $model
//        ]);
//    }

    public function actionUpdate($id)
    {
        /* @var User $model */
        $model = User::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Изменение "' . $model->firstname . ' ' . $model->lastname . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->firstname . ' ' . $model->lastname;

        return $this->render('_form', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        /* @var User $model */
        $model = User::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect('index');
    }
}
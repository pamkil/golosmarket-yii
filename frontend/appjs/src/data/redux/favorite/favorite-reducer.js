import { ReviewService } from "../../services";
import avatar from "../../../images/avatar.png";

const FAVORITE = {
    LOADING: 'golosmarket/favorite/SEND',
    LOAD: 'golosmarket/favorite/LOADED',
    FAIL: 'golosmarket/favorite/FAIL',
};

export function requestReviews(filter) {
    return dispatch => {
        dispatch( {
            type: REVIEW.LOADING,
            payload: { isLoading: true }
        } );
        return Promise.all(
            ReviewService
                .get( filter )
                .then( response => response.data )
                .then( data => {
                    dispatch( {
                        type: REVIEW.LOAD,
                        payload: {
                            items: data.data,
                            pagination: {
                                perPage: +data.per_page,
                                page: +data.current_page,
                                isNextPage: !!data.next_page_url
                            },
                            isLoading: false
                        }
                    } );
                } ),
            setTimeout( () => {
                dispatch( {
                    type: REVIEW.LOAD,
                    payload: {
                        items: staticReviews,
                        pagination: {},
                        isLoading: false
                    }
                } );
            }, 2000 )
        );
    }
}

const initialState = {
    isLoading: false,
    reviews: [],
    message: '',
    pagination: {
        perPage: 0,
        page: 1,
        isNextPage: false
    },
};

export const review = (state = initialState, action) => {
    switch ( action.type ) {
        case REVIEW.LOADING:
            return {
                ...state,
                isLoading: true,
            };

        case REVIEW.LOAD:
            return {
                ...initialState,
                reviews: action.payload.items,
                pagination: action.payload.pagination,
            };

        case REVIEW.FAIL:
            return {
                ...state,
                isLoading: false,
                ...action.payload,
            };

        default:
            return state;
    }
};

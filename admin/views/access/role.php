<?
namespace app\modules\admin\views\access;

use Yii;
use yii\grid\GridView;
use yii\grid\DataColumn;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;

/* @var ArrayDataProvider $dataProvider */
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <? if (Yii::$app->user->can('updateRules')) { ?>
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<?= Url::to(['create-role']) ?>" class="btn sbold green"> <?= Yii::t('admin', 'Добавить') ?>
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <? } ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'class' => DataColumn::class,
                        'attribute' => 'name',
                        'label' => Yii::t('admin', 'Роль')
                    ],
                    [
                        'class' => DataColumn::class,
                        'attribute' => 'description',
                        'label' => Yii::t('admin', 'Название')
                    ],
                    Yii::$app->user->can('updateRules') ? [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::toRoute(['update-role', 'name' => $model->name]), [
                                    'title' => Yii::t('admin', 'Изменить'),
                                ]);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::toRoute(['delete-role', 'name' => $model->name]), [
                                    'title' => Yii::t('admin', 'Удалить'),
                                    'data-confirm' => Yii::t('admin', 'Вы уверены что хотите удалить?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '1'
                                ]);
                            }
                        ]
                    ] : []
                ],
                'layout' => '<div class="dataTables_wrapper no-footer"><div class="table-scrollable">{items}</div><div class="row"><div class="col-md-5 col-sm-5"><div class="dataTables_info">{summary}</div></div><div class="col-md-7 col-sm-7"><div class="dataTables_paginate paging_bootstrap_full_number">{pager}</div></div></div></div>',
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover order-column dataTable no-footer'
                ],
                'summary' => Yii::t('admin', 'Показано {begin} - {end} из {totalCount} элементов')
            ])?>
        </div>
    </div>
</div>
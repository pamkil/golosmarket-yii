<?php
/**
 * @var array $today
 * @var array $yesterday
 */
?>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-green-sharp">
                    <span class="tooltips" title="Заказов с прибылью: <?= $today['count'] . PHP_EOL; ?>
                    Минимальная прибыль: <?= $today['minProfit']; ?>
                    Максимальная прибыль: <?= $today['maxProfit']; ?>">
                        <?= $today['profit']; ?>
                    </span>
                    <small class="font-green-sharp">₽</small>
                    <span style="font-size: 20px;">
                        (<span class="tooltips" title="Заказов с прибылью: <?= $yesterday['count']; ?>
                        Минимальная прибыль: <?= $yesterday['minProfit']; ?>
                        Максимальная прибыль: <?= $yesterday['maxProfit']; ?>">
                            <?= $yesterday['profit']; ?>
                        </span>
                        <small class="font-green-sharp">₽</small>)
                    </span>
                </h3>
                <small>Прибыль за сегодня (вчера)</small>
            </div>
            <div class="icon">
                <i class="icon-pie-chart"></i>
            </div>
        </div>
    </div>
</div>
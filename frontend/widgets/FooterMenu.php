<?php

namespace frontend\widgets;

use common\models\announcer\Age;
use common\models\announcer\AnnouncerSound;
use common\models\announcer\Gender;
use yii\helpers\ArrayHelper;
use yii\widgets\Menu;

class FooterMenu extends Menu
{
    public function init()
    {
        parent::init();
        $key = 'footer-gender-age-lang';
        $this->items = \Yii::$app->cache->getOrSet($key, function () {
            $gender = $this->setUrl(Gender::getAllList(), 'gender');
            $age = $this->setUrl(Age::getAllList(), 'age');
            $lang = $this->setLanguagesUrl(AnnouncerSound::getExistingLanguages());

            return ArrayHelper::merge($gender, $age, $lang);
        });
    }

    private function setUrl(array $array, $key)
    {
        $results = [];
        foreach ($array as $id => $label) {
            $results[] = [
                'url' => "/?$key=$id",
                'label' => $label,
            ];
        }

        return $results;
    }

    private function setLanguagesUrl(array $array)
    {
        $results = [];
        foreach ($array as $lang) {
            $results[] = [
                'url' => "/?language=".$lang['id'],
                'label' => $lang['name'],
            ];
        }

        return $results;
    }
}

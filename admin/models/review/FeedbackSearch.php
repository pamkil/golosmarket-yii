<?php

namespace admin\models\review;

use Carbon\Carbon;
use common\models\site\Feedback;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class FeedbackSearch extends Model
{
    public $user;
    public $status;
    public $email;
    public $phone;
    public $text;
    public $date_at;
    public $date_to;

    public function rules()
    {
        return [
            [
                ['status'],
                'integer'
            ],
            [
                ['user', 'email', 'phone', 'text'],
                'string'
            ],
            [
                ['date_at'],
                'filter',
                'filter' => function ($val) {
                    if (empty($val)) {
                        return '';
                    }

                    return Carbon::parse($val)->toDateTimeString();
                }
            ],
            [
                ['date_to'],
                'filter',
                'filter' => function ($val) {
                    if (empty($val)) {
                        return '';
                    }

                    return Carbon::parse($val)->endOfDay()->toDateTimeString();
                }
            ],
        ];
    }

    public function search($params)
    {
        $query = Feedback::find()
            ->alias('feedback')
            ->joinWith(['user user']);
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'feedback.id',
                        'feedback.status',
                        'feedback.email',
                        'feedback.phone',
                        'feedback.text',
                        'feedback.created_at',
                    ],
                    'defaultOrder' => [
                        'feedback.created_at' => SORT_DESC
                    ],
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->user)) {
            $query->andWhere(
                [
                    'OR',
                    ['like', 'user.firstname', $this->user],
                    ['like', 'user.lastname', $this->user],
                ]
            );
        }

        $query->andFilterWhere(
            [
                'AND',
                ['feedback.status' => $this->status],
                ['feedback.email' => $this->email],
                ['feedback.phone' => $this->phone],
                ['feedback.text' => $this->text],
                ['>=', 'feedback.created_at', $this->date_at],
                ['<=', 'feedback.created_at', $this->date_to],
            ]
        );

        if (!empty($this->date_at)) {
            $this->date_at = Carbon::parse($this->date_at)->format('d.m.Y');
        }
        if (!empty($this->date_to)) {
            $this->date_to = Carbon::parse($this->date_to)->format('d.m.Y');
        }

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}

import React from "react";
import { Field, reduxForm } from 'redux-form';
import { FORMS } from '../../data/constants'
import messageImg from "../../images/icon_question.png";
import InputInline from "../InputInline/InputInline";
import PhoneInput from "../Common/PhoneInput/PhoneInput";
import { isValidPhoneNumber } from 'libphonenumber-js'

const validate = values => {
    const errors = {};

    if (!values.agree) {
        errors.agree = 'Необходимо согласие';
    }
    if (!values.firstname) {
        errors.firstname = 'Необходимо заполнить';
    } else if (values.firstname.length < 2) {
        errors.firstname = 'Необходимо ввести не менее 2 символов';
    }
    if (!values.password) {
        errors.password = 'Необходимо заполнить';
    } else if (values.password.length < 6) {
        errors.password = 'Необходимо ввести не менее 6 символов';
    }
    if (!values.email) {
        errors.email = 'Необходимо заполнить';
    } else if (!(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/).test(values.email)) {
        errors.email = 'Неверный формат email';
    }
    if (!values.city) {
        errors.city = 'Необходимо заполнить';
    }
    if (!values.phone) {
        errors.phone = 'Необходимо заполнить';
    } else if (!isValidPhoneNumber(values.phone)) {
        errors.phone = 'Неверный формат телефона';
    }

    return errors;
};

let RegisterAnnouncerForm = ({ handleSubmit, pristine, submitting, valid, showRecovery }) => (
    <form onSubmit={ handleSubmit } className="modal-form">
        <div className="item">
            <div className="block">
                <Field
                    name="firstname"
                    component={ InputInline }
                    type="text"
                    placeholder="Имя или псевдоним"
                />
            </div>
            <div className="block"/>
        </div>
        <div className="item">
            <div className="block">
                <Field
                    name="lastname"
                    component={ InputInline }
                    type="text"
                    placeholder="Фамилия или псевдоним"
                />
            </div>
            <div className="block"/>
        </div>
        {/*<div className="item">*/}
        {/*    <div className="block">*/}
        {/*        <Field*/}
        {/*            name="city"*/}
        {/*            component={ InputInline }*/}
        {/*            type="text"*/}
        {/*            placeholder="Город"*/}
        {/*        />*/}
        {/*    </div>*/}
        {/*    <div className="block"/>*/}
        {/*</div>*/}
        <div className="item">
            <div className="block">
                <Field
                    name="email"
                    component={ InputInline }
                    showRecovery={showRecovery}
                    type="email"
                    placeholder="E-mail"
                />
            </div>
            <div className="block">
                <div className="tooltip">
                    <span className="icon">
                        <img src={ messageImg } alt="Подсказка"/>
                        <span>На&nbsp;этот&nbsp;e-mail<br/>будет&nbsp;отправлена<br/>ссылка&nbsp;для&nbsp;активации</span>
                    </span>
                </div>
            </div>
        </div>
        <div className="item">
            <div className="block">
                <Field
                    name="phone"
                    component={ PhoneInput }
                    placeholder="Телефон"
                    required
                />
            </div>
            <div className="block"/>
        </div>
        <div className="item">
            <div className="block">
                <Field
                    name="password"
                    component={ InputInline }
                    type="password"
                    placeholder="Придумайте пароль"
                />
            </div>
            <div className="block">
                <div className="tooltip">
                    <span className="icon">
                        <img src={ messageImg } alt="Подсказка"/>
                        <span>Не менее 6&nbsp;символов</span>
                    </span>
                </div>
            </div>
        </div>
        <div className="item">
            <div className="block agree">
                <Field
                    name="agree"
                    component={ InputInline }
                    type="checkbox"
                />
                Нажимая "Зарегистрироваться", я соглашаюсь с <a href="/rules">правилами использования
                сайта</a> и <a href="/pdn">даю согласие на обработку персональных данных</a>
            </div>
            <div className="block"/>
        </div>
        <div className="item">
            <button type="submit" disabled={ pristine || submitting } className={ !valid ? 'error-submit' : '' }>
                Зарегистрироваться
            </button>
        </div>
    </form>
);

RegisterAnnouncerForm = reduxForm( {
    form: FORMS.REGISTER_ANNOUNCER,
    validate
} )( RegisterAnnouncerForm );

export default RegisterAnnouncerForm;

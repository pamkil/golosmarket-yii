import React, {useEffect, useState, useMemo} from "react";
import {useDispatch, useSelector} from "react-redux";
import {isEqual} from "lodash";
import {toast} from "react-toastify";

import FilterElem from "../Announcer/Filter/Filter";
import {requestAnnouncers, setPage, getProfileLoad, setFavLocalList, setCostFilter} from "../../data/redux";
import useQueryString from "../../data/queryParams/useQueryString";
import {clearFilter} from "../../data/helpers/Tools";
import {textNoLogin} from "../Announcer/Announcer";
import {FavoriteService} from "../../data/services";

const emptyFilter = {
  gender: null,
  age: null,
  costAt: '',
  costTo: '',
  language: null,
  text: '',
  demo: false,
  presentation: null,
  online: false,
  favorite: false,
  costBy: 'any',
  page: 1
};

const Filter = ({favoritePage}) => {
  const dispatch = useDispatch();
  const {
    mobile,
    isLoadingAnnouncer,
    buttonsGender,
    buttonsCost,
    optAge,
    optLang,
    optPresents,
    addFilters,
    quantity,
    page = 1,
    isInit,
    sortFields,
    profileId,
    favList,
    isLogin
  } = useSelector(state => {
    return {
      ...state.filter,
      isLoadingAnnouncer: state.announcer.isLoadingAnnouncer,
      quantity: state.announcer.quantity,
      page: state.announcer.pagination.page,
      isInit: state.app.initialized,
      mobile: state.app.mobile,
      profileId: state.user.user ? state.user.user.id : null,
      stateAll: state,
      favList: state.favLocalList.favList || [],
      isLogin: !!state.auth.token
    }
  });

  const emptyFilterNew = useMemo(() => {
    const efilter = {...emptyFilter};
    addFilters.map(({name, value}) => efilter[name] = value);
    return clearFilter(efilter);
    // eslint-disable-next-line
  }, [emptyFilter, addFilters]);

  const [params, setParams] = useQueryString(emptyFilterNew);
  const [request, setRequest] = useState(params);
  const [prevRequest, setPrevRequest] = useState('');

  useEffect(() => {
    if (!isEqual(request, params)) {
      for (const [key, value] of Object.entries(request)) {
        if (emptyFilter[key] === value) {
          delete request[key];
        }
      }

      setParams(request);
    }
    // eslint-disable-next-line
  }, [request]);

  let combineFilter = {...emptyFilterNew, ...params};

  useEffect((e) => {
    if (isInit && prevRequest !== JSON.stringify(combineFilter)) {
      setPrevRequest(JSON.stringify(combineFilter) + profileId);
      let promises = [];

      if (profileId && favList && favList.length) {
        promises.push(new Promise((res, rej) => {
          if (profileId && favList && favList.length) {
            FavoriteService.patchFavoriteList({localFavList: favList}).then(() => {
              dispatch(getProfileLoad());
              dispatch(setFavLocalList([]));
              res();
            });
          } else {
            res();
          }
        }))
      }

      Promise.all(promises).then(() => {
        if (combineFilter.costBy === 'any') {
          delete combineFilter.costBy;
        }

        dispatch(requestAnnouncers(combineFilter, page, profileId, mobile, favoritePage));
      });
    }
    // eslint-disable-next-line
  }, [JSON.stringify(combineFilter), isInit, profileId]);

  useEffect(() => {
    setRequest({...request, page})
    // eslint-disable-next-line
  }, [page]);

  const sendMessage = () => toast.info(textNoLogin);

  const filterHandler = (filter, sort) => {
    const newSort = {};
    if (sort) {
      newSort.sort = sort.sort;
      if (sort.dest && sort.dest === 'desc') {
        newSort.sort = '-' + sort.sort;
      }
    }

    delete sort.dest;
    const requestNew = {...filter, ...newSort, page};
    if (!isEqual(request, requestNew)) {
      if (page !== 1) {
        dispatch(setPage(1));
      }
      setRequest(requestNew);
    }
  };

  return (
    <FilterElem
      initialFilter={params}
      buttonsGender={buttonsGender}
      buttonsCost={buttonsCost}
      optAge={optAge}
      optLang={optLang}
      optPresents={optPresents}
      onChange={filterHandler}
      emptyFilter={emptyFilterNew}
      quantity={quantity}
      sortFields={sortFields}
      sendMessage={sendMessage}
      isLogin={isLogin}
      favoritePage={favoritePage}
    />
  )
};

export default Filter;

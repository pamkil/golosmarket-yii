<?php

use common\models\access\User;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;

/**
 * @var ActiveDataProvider $dataProvider
 */
?>
<div class="box box-primary">
    <div class="box-header with-border text-right">
        <a href="<?= Url::to(['create']) ?>" class="btn bg-olive btn-flat margin">
            Добавить
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                'id',
                'firstname',
                'lastname',
                'phone',
                'email',
                'type_id' => [
                    'attribute' => 'type_id',
                    'filter' => User::getTypes(),
                    'value' => function ($model) {
                        return $model->getTypeName();
                    }
                ],
                [
                    'class' => \yii\grid\ActionColumn::class,
                    'template' => '{update}  {delete}',
                    'buttonOptions' => ['class' => 'margin']
                ],
            ]
        ]); ?>
    </div>
</div>


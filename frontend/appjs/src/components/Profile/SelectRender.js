import Select from "react-select";
import React from "react";

const optionValue = ({id, value}) => id || value;
const optionLabel = ({name, label}) => name || label;

const SelectRender = ({input, optionsToRender, placeholder, changeCallback, isMulti = true, ...add}) => <Select
    {...add}
    value={input.value}
    onChange={(e) => {
        if (typeof changeCallback === 'function') {
            changeCallback(e);
        }

        input.onChange(e);
    }}
    onBlur={() => input.onBlur(input.value)}
    options={optionsToRender}
    placeholder={placeholder}
    blurInputOnSelect={false}
    isMulti={isMulti}
    getOptionValue={optionValue}
    getOptionLabel={optionLabel}
/>

export default SelectRender;

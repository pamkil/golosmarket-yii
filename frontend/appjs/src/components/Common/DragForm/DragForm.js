import React, { useState } from 'react';
import classes from "classnames";
import { ReactComponent as UploadImg } from "../../../images/file-upload.svg";

const DragForm = ({ children }) => {

    const [isDragOver, setIsDragOver] = useState(false);
    const [isUploading, setIsUploading] = useState(false);
    const [hasFocus, setHasFocus] = useState(false);

    //region Send File
    const stopEvent = event => {
        event.preventDefault();
        event.stopPropagation();
    }

    const dragHandler = event => {
        stopEvent(event);
    }

    const dragOverHandler = event => {
        stopEvent(event);
        if (isUploading) return false;
        setIsDragOver(true);
    }

    const dragOverEndHandler = event => {
        stopEvent(event);
        if (isUploading) return false;
        setIsDragOver(false);
    }

    const dropHandler = event => {
        stopEvent(event);
        if (isUploading) return false;

        setIsDragOver(false);
        triggerFormSubmit(event.dataTransfer.files);
    }

    const inputOnChangeHandler = event => {
        if (isUploading) return false;
        triggerFormSubmit(event.target.files);
    }

    const submitHandler = eventSubmit => stopEvent(eventSubmit);

    const triggerFormSubmit = files => {
        setIsUploading(true);
        try {
            if (files && files.length) {
                onSendFile(files);
            }
        } catch (e) {
        }
        setTimeout(() => {
            setIsUploading(false);
        }, 300);
    };
    //endregion

    return (
        <form
            encType="multipart/form-data"
            className={ classes(
                'form-box',
                'has-advanced-upload',
                {
                    'is-dragover': isDragOver,
                    'is-uploading': isUploading
                }
            ) }
            onSubmit={ submitHandler }
            onDrag={ dragHandler }
            onDragStart={ dragHandler }
            onDragEnd={ dragOverEndHandler }
            onDragOver={ dragOverHandler }
            onDragEnter={ dragOverHandler }
            onDragLeave={ dragOverEndHandler }
            onDrop={ dropHandler }
        >
            <div className="file-upload">
                <div className="box__input">
                    <label
                        htmlFor="file"
                        className={ classes({ 'has-focus': hasFocus }) }
                    >
                        <UploadImg />
                    </label>

                    <input
                        id="file"
                        type="file"
                        name="files[]"
                        multiple
                        className={ classes('box__file', { 'has-focus': hasFocus }) }
                        onChange={ inputOnChangeHandler }
                    />
                </div>
            </div>
            {
                children
            }
        </form>
    );
};
export
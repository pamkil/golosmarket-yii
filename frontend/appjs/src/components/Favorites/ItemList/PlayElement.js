import React, { useCallback } from "react";
import PropTypes from 'prop-types';

import play from "../../../images/play.png";
import pause from "../../../images/pause.png";

const PlayElement = ({
    sound = [],
    onClickPlay,
    onClickStop,
    playingSong,
    announcer
}) => {
    const file = sound && sound[0] ? sound[0] : '';
    const url = file ? file.url : '';

    const playHandler = useCallback(() => {
        onClickPlay(file, sound, announcer);
    }, []);


    return url ? (
        playingSong === url
            ? <img className="item__plat-element" src={ pause } alt="Остановить" onClick={ () => onClickStop() } />
            : <img className="item__plat-element" src={ play } alt="Воспроизвести" onClick={ playHandler } />
    ) : null;
};

PlayElement.propTypes = {
    sound: PropTypes.array,
    addToFavorite: PropTypes.func,
    copyToClipboard: PropTypes.func,
    onClickPlay: PropTypes.func,
    nameTrack: PropTypes.string,
    isFavorite: PropTypes.bool,
};

PlayElement.defaultProps = {
    isFavorite: false,
    nameTrack: 'Очень длинное название трека',
    onClickPlay: () => {},
    copyToClipboard: () => {},
};

export default PlayElement;

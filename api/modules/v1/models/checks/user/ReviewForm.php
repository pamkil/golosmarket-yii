<?php

namespace api\modules\v1\models\checks\user;

use yii\base\Model;

/**
 * @OA\Schema(required={"text", "rating"}, title="Форма добавления отзыва о сервисе")
 */
class ReviewForm extends Model
{
    /**
     * @OA\Property(
     *     type="string",
     *     description="Текст",
     *     title="text",
     * )
     *
     * @var string
     */
    public $text;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Оценка отзыва",
     *     title="rating",
     * )
     *
     * @var int
     */
    public $rating;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['rating'],
                'integer',
                'min' => 1,
                'max' => 5,
            ],
            [
                ['text', 'rating'],
                'required',
            ],
            [
                ['text'],
                'string',
                'min' => 5
            ],
        ];
    }
}

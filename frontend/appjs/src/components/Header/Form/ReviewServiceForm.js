import React from "react";
import { Field, reduxForm } from 'redux-form';
import { FORMS } from '../../../data/constants';
import StartInput from "./StarInput";

const validate = values => {
    const errors = {};

    if (!values.text) {
        errors.text = 'Необходимо заполнить';
    } else if (values.text.length < 5) {
        errors.text = 'Необходимо ввести не менее 5 символов';
    }
    if (!values.rating) {
        errors.rating = 'Необходимо заполнить рейтинг';
    }

    return errors;
};

let ReviewServiceForm = ({ handleSubmit, pristine, submitting, valid }) => (
    <form onSubmit={ handleSubmit } className="modal-form review-user-form">
        <div className="head-registration">Оцените работу <span className="underline">сервиса</span></div>
        <div className="item">
            <Field
                name="rating"
                id="rating"
                component={ StartInput }
                required
            />
        </div>
        <div className="item text-feedback">
            <Field
                name="text"
                component="textarea"
                type="text"
                placeholder="Как вам Голосмаркет? Оставьте отзыв. Будем рады, если посоветуете нас другим людям."
            />
        </div>
        <div className="item">
            <button type="submit" disabled={ pristine || submitting } className={ !valid ? 'error-submit' : '' }>
                Отправить
            </button>
        </div>
    </form>
);

ReviewServiceForm = reduxForm({
    form: FORMS.FEEDBACK,
    validate
})(ReviewServiceForm);

export default ReviewServiceForm;

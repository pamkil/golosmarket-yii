import API from "../api/api";
import ApiList from "../api/apiList";

export const FavoriteService = {
    get: (id) => API.get(`${ ApiList.FAVORITE.FAVORITE }/${ id }`),
    removeFromFavorite: (id) => API.delete(`${ ApiList.FAVORITE.FAVORITE }/${ id }`),
    addToFavorite: (id) => API.post(`${ ApiList.FAVORITE.FAVORITE }/${ id }`),
    patchFavoriteList: (ids) => API.patch(`${ ApiList.FAVORITE.FAVORITE }`, ids),
};

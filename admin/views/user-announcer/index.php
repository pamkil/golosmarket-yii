<?php

use admin\models\user\AnnouncerSearch;
use common\models\access\User;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/**
 * @var ActiveDataProvider $dataProvider
 * @var AnnouncerSearch $searchModel
 */
?>

<div class="box box-primary">
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => ['class' => 'table-responsive'],
            'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
            'layout' => "{items}\n{summary}\n{pager}",
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
            'columns' => [
                'id',
                'is_blocked:boolean:Заблокирован',
                'username',
                'firstname:text:Имя',
                'lastname:text:Фамилия',
                'email:email:Email',
                'phone:text:Телефон',
                [
                    'attribute' => 'status',
                    'label' => 'Статус',
                    'filter' => User::getStatuses(),
                    'content' => function ($model) {
                        /** @var User $model */
                        return User::getStatuses()[$model->status] ?? '';
                    }
                ],
                'last_visit:datetime:Последний вход',
//                'announcer.likes',
//                'announcer.dislikes',
                'announcer.cost',
                [
                    'class' => ActionColumn::class,
                    'template' => '{update}  {delete}',
                    'buttonOptions' => ['class' => 'margin']
                ],
            ]
        ]); ?>
    </div>
</div>

echo "Deploy script started"
[[ -s $HOME/.nvm/nvm.sh ]] && . $HOME/.nvm/nvm.sh
cd /app
php deploy.php
echo "Deploy script finished execution"

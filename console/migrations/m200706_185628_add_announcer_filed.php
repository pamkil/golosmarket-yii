<?php

use yii\db\Migration;

/**
 * Class m200706_185628_add_announcer_filed
 */
class m200706_185628_add_announcer_filed extends Migration
{
    private $table = '{{%user_announcer}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->table,
            'sound_by_key',
            $this->boolean()
                ->after('user_id')->notNull()->defaultValue(false)
        );

        $this->addColumn(
            $this->table,
            'script',
            $this->boolean()
                ->after('user_id')->notNull()->defaultValue(false)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'script');
        $this->dropColumn($this->table, 'sound_by_key');
    }
}

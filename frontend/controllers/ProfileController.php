<?php

namespace frontend\controllers;

use Carbon\Carbon;
use common\models\access\User;
use common\models\announcer\Announcer;
use common\models\announcer\File;
use common\models\content\Page;
use common\models\user\Bill;
use common\models\user\Review;
use common\models\user\ScheduleWork;
use common\service\profile\enum\WeekDaysEnum;
use frontend\models\ReviewSearch;
use Yii;
use yii\helpers\StringHelper;
use yii\web\NotFoundHttpException;

/**
 * Page controller
 */
class ProfileController extends BaseController
{
    /**
     * Displays homepage.
     *
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        if (!$id) {
            throw new NotFoundHttpException();
        }

        $this->view->registerCssFile("@web/css/profile.css");
        $this->view->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css');

        $this->view->registerJsFile('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js');
        $this->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js');

        $id = (int)$id;
        $key = json_encode(['review-profile', $id]);
        $result = Yii::$app->cache->getOrSet(
            $key,
            function () use ($id) {
                /** @var User $user */
                $user = User::find()
                    ->with([
                        'photo',
                        'announcer',
                        'announcer.scheduleWork',
                        'announcer.photos',
                        'announcer.tags',
                    ])
                    ->where(['id' => $id])
                    ->one();

                if (!$user) {
                    throw new NotFoundHttpException();
                }

                /** @var Announcer $announcer */
                $announcer = $user->announcer ?? null;

                $page = Page::getByPath('profile', "profile/$id");

                $quantityReview = $user->quantity_reviews;
                $reviews = Review::getReviews(3, $id, $user->isAnnouncer);

                [$isOnline, $diff] = ScheduleWork::isOnline(
                    $announcer->scheduleWork ?? null,
                    $announcer->user ?? null
                );
                $timeOnline = ScheduleWork::getTimeOnlineString($isOnline, $diff);

                $textSchedule = null;
                $showSchedule = $user->isAnnouncer;
                if (isset($announcer->scheduleWork)) {
                    $showSchedule = (int)$announcer->scheduleWork->type !== ScheduleWork::TYPE_UNDEFINED;
                    $textSchedule = $announcer->scheduleWork->undefined_text;
                    if (empty($textSchedule)) {
                        $textSchedule = 'НЕДОСТУПЕН. Длительный перерыв в работе';
                    }
                }
                $name = $user->firstname . ' ' . ($announcer ? $user->lastname : mb_substr($user->lastname, 0, 1));
                if ($user->isDeleted) {
                    $title = $name . ' - аккаунт удалён';
                    $description = $name . ' – аккаунт удалён';
                } else {
                    if ($user->isAnnouncer) {
                        $title = $name . ' -';
                        $addTitle = [];
                        if ($announcer->cost && !$announcer->cost_a4) {
                            $addTitle[] = ' заказать озвучку';
                        }
                        if ($announcer->cost_a4) {
                            $addTitle[] = ' диктор для озвучки';
                        }
                        if ($announcer->cost_vocal) {
                            $addTitle[] = ' заказать вокал';
                        }
                        if ($announcer->cost_parody) {
                            $addTitle[] = ' озвучка пародии';
                        }
                        if ($announcer->cost_sound_mount) {
                            $addTitle[] = ' изготовление аудиоролика';
                        }
                        if ($announcer->cost_audio_adv) {
                            $addTitle[] = ' заказать сценарий';
                        }
                        foreach ($addTitle as $key => $item) {
                            if ($key !== 0) {
                                $title .= ',';
                            }
                            $title .= $item;
                        }
                        $description = $name . ' – диктора, стоимость выполнения заказа, пример звучания голоса и характеристики. Подробности на нашем сайте.';
                    } else {
                        $title = $name . ' - клиент';
                        $description = $name . ' – клиент';
                    }
                }

                $sounds = [];
                foreach ($announcer->sounds ?? [] as $sound) {
                    if (!empty($sound->lang_code)) {
                        $lang_name = StringHelper::mb_ucfirst(\Locale::getDisplayLanguage($sound->lang_code));
                    } else {
                        $lang_name = 'Любой';
                    }
                    $sounds[$lang_name][] = $sound;
                }

                if ($user->isAnnouncer) {
                    $condition = ['announcer_id' => $user->id];
                } else {
                    $condition = ['client_id' => $user->id];
                }

                $ordersCount = Bill::find()->andWhere([
                    'AND',
                    $condition,
                    ['IN', 'status', [
                        Bill::STATUS_NEW,
                        Bill::STATUS_PAID_ANNOUNCER,
                        Bill::STATUS_PAID_CLIENT,
                        Bill::STATUS_PAID_COMMISSION
                    ]]
                ])->count();
                $paidOrdersCount = Bill::find()->andWhere([
                    'AND',
                    $condition,
                    ['IN', 'status', [
                        Bill::STATUS_PAID_ANNOUNCER,
                        Bill::STATUS_PAID_CLIENT,
                        Bill::STATUS_PAID_COMMISSION
                    ]]
                ])->count();

                return [
                    'announcer' => $announcer,
                    'sounds' => $sounds,
                    'user' => $user,
                    'weekDays' => WeekDaysEnum::getShortLabels(),
                    'quantityReview' => $quantityReview,
                    'reviews' => $reviews,
                    'timeOnline' => $timeOnline,
                    'showSchedule' => $showSchedule,
                    'textSchedule' => $textSchedule,
                    'nowDay' => Carbon::now()->dayOfWeekIso,
                    'name' => $name,
                    'title' => $title,
                    'description' => $description,
                    'page' => $page,
                    'ordersCount' => $ordersCount,
                    'paidOrdersCount' => $paidOrdersCount,
                ];
            },
            60
        );

        $this->addSeo($result['page']);

        $this->addMetaTags(
            $result['title'],
            '',
            $result['description'] ?? '',
            File::url($result['user']['photo'] ?? null)
        );

        if ($result['user']->status === User::STATUS_DELETED) {
            return $this->render('delete_profile', $result);
        }

        return $this->render('profile', $result);
    }

    /**
     * Displays homepage.
     *
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionReview($id)
    {
        if (!$id) {
            throw new NotFoundHttpException();
        }

        $id = (int)$id;

        $key = json_encode(['review-review', $id]);
        $result = Yii::$app->cache->getOrSet(
            $key,
            function () use ($id) {
                /** @var User $user */
                $user = User::find()
                    ->with([
                        'photo',
                        'announcer',
                        'announcer.scheduleWork',
                    ])
                    ->where(['id' => $id])
                    ->one();

                if (!$user || $user->isDeleted) {
                    throw new NotFoundHttpException();
                }

                /** @var Announcer $announcer */
                $announcer = $user->announcer ?? null;

                $page = Page::getByPath('profile/*/review', "profile/$id/review");
                $timeOnline = null;

                if ($user->isAnnouncer) {
                    [$isOnline, $diff] = ScheduleWork::isOnline(
                        $announcer->scheduleWork ?? null,
                        $announcer->user ?? null
                    );
                    $timeOnline = ScheduleWork::getTimeOnlineString($isOnline, $diff);
                }

                $reviewProvider = ReviewSearch::search($id, !!$announcer);
                $name = $user->firstname . ' ' . ($user->isAnnouncer ? $user->lastname : mb_substr($user->lastname, 0, 1));
                $title = $name;
                $description = "$name (отзывы) – диктора, стоимость выполнения заказа, пример звучания голоса и характеристики. Подробности на нашем сайте.";
                if ($announcer) {
                    $title = "$name (отзывы) - голос диктора, цена";
                }

                if ($user->isAnnouncer) {
                    $condition = ['announcer_id' => $user->id];
                } else {
                    $condition = ['client_id' => $user->id];
                }
                $ordersCount = Bill::find()->andWhere($condition)->count();
                $paidOrdersCount = Bill::find()->andWhere([
                    'AND',
                    $condition,
                    ['IN', 'status', [Bill::STATUS_PAID_ANNOUNCER,Bill::STATUS_PAID_CLIENT]]
                ])->count();

                return [
                    'user' => $user,
                    'name' => $name,
                    'announcer' => $announcer,
                    'reviewProvider' => $reviewProvider,
                    'timeOnline' => $timeOnline,
                    'page' => $page,
                    'title' => $title,
                    'description' => $description,
                    'ordersCount' => $ordersCount,
                    'paidOrdersCount' => $paidOrdersCount,
                ];
            },
            60
        );

        $this->addSeo($result['page']);
        unset($result['page']);

        $this->addMetaTags($result['title'], '', $result['description'] ?? '');

        return $this->render('review', $result);
    }

    public function actionUpdate()
    {
        return $this->render('update');
    }
}

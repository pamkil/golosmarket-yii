<?php

use common\models\content\Answer;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * @var Answer $model
 */

$form = ActiveForm::begin(
    [
        'options' => [
            'enctype' => 'multipart/form-data' // important
        ]
    ]
); ?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <?= Html::submitButton(
            '<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord ? 'Создать' : 'Сохранить'),
            ['class' => 'btn btn-success']
        ); ?>
        <?php if (!$model->isNewRecord) : ?>
            <?= Html::a(
                '<i class="fa fa-times"></i>&nbsp;' . 'Удалить',
                ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'title' => 'Удалить',
                    'data-confirm' => 'Вы уверены что хотите удалить?',
                    'data-method' => 'post'
                ]
            ); ?>
        <?php endif; ?>
    </div>
    <div class="box-body">
        <?= $form->field($model, 'active')->checkbox(); ?>
        <?= $form->field($model, 'question_id')->widget(
            Select2::class,
            [
                'value' => $model->question_id ? [$model->question_id => $model->question_id] : [],
                'initValueText' => $model->question_id ? [$model->question_id => $model->question->text] : [],
                'pluginOptions' => [
                    'ajax' => [
                        'url' => Url::to(['content-question/ajax']),
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new JsExpression(
                            'function(params) { 
                                                    return {q: params.term, contract: true, page: params.page}; 
                                                }'
                        ),
                        'cache' => true
                    ]
                ]
            ]
        ); ?>
        <?= $form->field($model, 'position'); ?>
        <?= $form->field($model, 'text')->textarea(); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

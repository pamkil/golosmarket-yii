<?
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\cmenu\ContextMenu;
use common\models\settings\File;

/**
 * @var File $model
 * @var int $index
 */

$items = [
    ['label' => Yii::t('admin', 'Оригинал'), 'url' => $model->url]
];

foreach ($model->thumbnails as $thumb) {
    $items[] = [
        'label' => $thumb->profile,
        'url' => $model->getUrl($thumb->profile)
    ];
}

$scriptBefore = <<< 'JS'
function (e, element, target) {
    $('.item div[id^="w"]').contextmenu('closemenu');
    return true;
}
JS;

$scriptOpen = <<< 'JS'
function (element, event) {
    var that = $(event.currentTarget),
        aSrc = that.find('a').attr('href');

        $('input#' + parentWin.inputSrc, parent.document).val(aSrc);
        $('.mce-close', parent.document).last().trigger('click');
    return true;
}
JS;

?>

<td class="item">
    <? ContextMenu::begin(['items' => $items, 'options' => ['tag' => 'div'], 'pluginOptions' => ['before' => $scriptBefore, 'onItem' => $scriptOpen]]) ?>
    <div class="image">
        <?= Html::img($model->url, ['class' => 'mceImage', 'data-edit-url' => Url::to(['file/update', 'id' => $model->id])]) ?>
    </div>
    <? ContextMenu::end() ?>
</td>

<? if (($index + 1) % 5 === 0){ ?>
</tr>
<tr>
    <? } ?>

<?php

use admin\models\setting\Robot;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var Robot $model
 */
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]) ?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <?= Html::submitButton(
            '<i class="fa fa-check"></i>&nbsp; Сохранить',
            ['class' => 'btn btn-success']
        ); ?>
    </div>
    <div class="box-body">
        <?= $form->field($model, 'message')->textarea(['rows' => 10]); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

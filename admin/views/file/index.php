<?
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use common\models\settings\File;
use common\models\settings\FileSearch;
use admin\components\grid\SocketGrid;

/**
 * @var ActiveDataProvider $dataProvider
 * @var FileSearch $searchModel
 */
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <? if (Yii::$app->user->can('updateFiles')) { ?>
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<?= Url::to(['create']) ?>" class="btn sbold green"><?= Yii::t('admin', 'Добавить') ?>
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <? } ?>

            <?= SocketGrid::widget([
                'baseClass' => File::class,
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'name',
                    'size',
                    'type',
                    'updated_at:datetime',
                    [
                        'class' => 'admin\components\grid\DuplicateColumn',
                        'template' => Yii::$app->user->can('viewFiles') ? '{update} {duplicate} {delete}' : ''
                    ]
                ]
            ]) ?>
        </div>
    </div>
</div>

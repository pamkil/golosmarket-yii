<?php

namespace api\modules\v1\models\records\file;

/**
 * @OA\Schema(title="Файл")
 */
class File extends \common\models\announcer\File
{
    /**
     * @OA\Property(property="name", type="string", description="Наименвание файла")
     * @OA\Property(property="ext", type="string", description="Расширение")
     * @OA\Property(property="size", type="integer", description="Размер файла в Байтах")
     * @OA\Property(property="type", type="string", description="MIME Тип файла")
     * @OA\Property(property="url", type="string", description="url файла")
     */

    public function fields()
    {
        return [
            'name',
            'ext',
            'size',
            'type',
            'url' => 'urlView',
            'size',
            'type',
            'default',
        ];
    }


    /**
     * @return string
     */
    public function getUrlView()
    {
        return parent::getUrl();
    }
}

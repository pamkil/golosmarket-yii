<?php
/** @var string $todayCount */
/** @var string $yesterdayCount */
/** @var string $todaySum */
/** @var string $yesterdaySum */
?>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-blue-sharp">
                    <span
                        title="Выручка сегодня: <?= number_format($todaySum, 0, '.', ' '); ?> ₽"
                        class="tooltips">
                        <?= $todayCount; ?>
                    </span>
                    <span style="font-size: 20px;">
                        (<span
                            title="Выручка за вчера: <?= number_format($yesterdaySum, 0, '.', ' '); ?> ₽"
                            class="tooltips">
                            <?= $yesterdayCount; ?>
                        </span>)
                    </span>
                </h3>
                <small>Кол-во заказов за сегодня (вчера)</small>
            </div>
            <div class="icon">
                <i class="icon-basket"></i>
            </div>
        </div>
    </div>
</div>
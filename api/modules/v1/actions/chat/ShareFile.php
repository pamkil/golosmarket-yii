<?php

namespace api\modules\v1\actions\chat;

use api\modules\v1\models\filters\user\ChatSearch;
use api\modules\v1\models\records\user\Bill;
use api\modules\v1\models\records\user\Chat;
use common\models\access\User;
use common\service\chat\enum\FileTypeEnum;
use Exception;
use Yii;
use yii\rest\Action;
use yii\web\BadRequestHttpException;

/**
 * @OA\Post(
 *      path="/chat/{id}/share",
 *      summary="Загрузить файл",
 *      tags={"Chat"},
 *      @OA\Response(
 *          response="200",
 *          description="Объект токена",
 *          @OA\JsonContent(ref="#/components/schemas/Notification")
 *      ),
 *      @OA\Response(
 *          response="400",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="401",
 *          description="Unauthorized",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */

/**
 * Class ShareFile
 * @package api\modules\v1\actions\chat
 *
 */
class ShareFile extends Action
{
    /**
     * @param int $id
     * @return Chat|array
     * @throws BadRequestHttpException
     * @throws \yii\db\Exception
     */
    public function run(int $id)
    {
        /** @var User $identityUser */
        $identityUser = Yii::$app->user->identity;
        $userId = $identityUser->id;

        if (empty($userId)) {
            throw new BadRequestHttpException("Не авторизован");
        }

        /** @var Chat $chat */
        $chat = Chat::find()->where(['id' => $id])->one();

        if (!$chat) {
            throw new BadRequestHttpException("Не найден сообщение которое вы прочитали");
        }

        if (!$identityUser->isAnnouncer) {
            throw new BadRequestHttpException("Не можете написать этому пользователю");
        }

        $transaction = Yii::$app->db->beginTransaction();
        $announcerId = $chat->announcer_id;
        $clientId = $chat->client_id;

        try {
            $postData['announcer_id'] = $announcerId;
            $postData['client_id'] = $clientId;
            $postData['is_write_client'] = false;
            $postData['file_id'] = $chat->file_id;
            $postData['file_watermark_id'] = $chat->file_id;
            $postData['file_type'] = FileTypeEnum::FILE_TYPE_PUBLIC;

            $model = new Chat();
            $model->load($postData, '');

            if (!$model->save()) {
                return $model;
            }

            $transaction->commit();
        } catch (Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }

        $bill = Bill::getNew($announcerId, $clientId) ?? Bill::getLastPaid($announcerId, $clientId);

        return [
            'items' => (new ChatSearch())->search($clientId, $announcerId, true, 0, 50),
            'opponent' => $clientId,
            'bill' => $bill,
            'unread' => Chat::getUnread($clientId)
        ];
    }
}

<?php

namespace api\modules\v1\models\records\announcer;

use api\modules\v1\models\queries\UserQuery;
use api\modules\v1\models\records\file\File;
use api\modules\v1\models\records\user\City;
use Carbon\Carbon;
use common\models\user\Bill;
use yii\helpers\ArrayHelper;

/**
 * @OA\Schema(title="Пользователь")
 */
class UserEdit extends \common\models\access\User
{
    /**
     * @return UserQuery
     */
    public static function find()
    {
        return new UserQuery(static::class);
    }

    /**
     * @OA\Property(property="id", type="integer",description="ID пользователя")
     * @OA\Property(property="firstname", type="string", description="имя пользователя")
     * @OA\Property(property="lastname", type="string", description="фамилия пользователя")
     * @OA\Property(property="email", type="string", description="email пользователя")
     * @OA\Property(property="phone", type="string", description="phone пользователя")
     * @OA\Property(property="password", type="integer", description="password пользователя")
     * @OA\Property(property="city", type="object", ref="#/components/schemas/City", description="city")
     * @OA\Property(
     *     property="photo",
     *     type="object",
     *     description="количество новых сообщений у пользователя",
     *     ref="#/components/schemas/File"
     * )
     * @OA\Property(property="announcer", type="object", ref="#/components/schemas/AnnouncerEdit", description="announcer")
     * @OA\Property(property="isAnnouncer", type="boolean", description="пользователя является диктором")
     * @OA\Property(property="favoritesCount", type="integer", description="Количество избранных")
     *
     * @return array|false
     */
    public function fields()
    {
        $fields = [
            'id',
            'firstname',
            'lastname',
            'email',
            'phone',
            'avatar' => 'photoUrl',
            'city',
            'photo',
            'announcer',
            'isAnnouncer',
            'isDeleted',
            'favoritesCount',
            'isFavorite',
            'rating',
            'last_visit' => 'lastVisit'
        ];

        if (!$this->isAnnouncer) {
            $fields = ArrayHelper::merge(
                $fields,
                ['billsInfo']
            );
        }

        return $fields;
    }

    public function extraFields()
    {
        return [
            'recommended',
            'billsInfo'
        ];
    }

    public function formName()
    {
        return '';
    }

    public function getPhoto()
    {
        return $this->hasOne(File::class, ['id' => 'photo_id']);
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    public function getAnnouncer()
    {
        return $this->hasOne(AnnouncerEdit::class, ['user_id' => 'id']);
    }

    public function getBillsInfo()
    {
        if ($this->isAnnouncer) {
            $condition = ['announcer_id' => $this->id];
        } else {
            $condition = ['client_id' => $this->id];
        }
        $ordersCount = Bill::find()->andWhere([
            'AND',
            $condition,
            ['IN', 'status', [Bill::STATUS_NEW,Bill::STATUS_PAID_ANNOUNCER,Bill::STATUS_PAID_CLIENT]]
        ])->count();
        $paidOrdersCount = Bill::find()->andWhere([
            'AND',
            $condition,
            ['IN', 'status', [Bill::STATUS_PAID_ANNOUNCER,Bill::STATUS_PAID_CLIENT]]
        ])->count();

        return [
            'ordersCount' => $ordersCount,
            'paidOrdersCount' => $paidOrdersCount
        ];
    }

    public function getLastVisit()
    {
        return Carbon::parse($this->last_visit, 'UTC')->toIso8601String();
    }
}

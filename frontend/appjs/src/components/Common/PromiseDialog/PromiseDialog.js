import React, { PureComponent } from 'react';
import cx from 'classnames';
import './promise-dialog.css';

export default class PromiseDialog extends PureComponent {
    state = {
        modal: null,
        opened: false,
        pulse: false
    };

    componentDidMount() {
        document.addEventListener('keydown', this.handleDocumentKeyDown);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleDocumentKeyDown);
        this.isUnmonted = true;
    }

    handleDocumentKeyDown = (event) => {
        // ESC
        if (event.keyCode === 27) {
            this.close();
        }
    };

    handleClick = () => {
        this.setState({ pulse: true });
        setTimeout(() => {
            if (!this.isUnmonted) {
                this.setState({ pulse: false });
            }
        }, 1000);
    };

    isUnmonted = false;

    open = (props) => {
        const {
            title,
            subTitle,
            content = '',
            closeOnSubmit = true,
            submitText = 'Ok',
            cancelText = 'Отмена',
            danger,
            style // danger
        } = props;

        return new Promise((resolve, reject) => {
            const modal = (
                <div
                    className={ cx({
                        'promise-dialog__container': true,
                        [`promise-dialog__container--${style}`]: !!style,
                        'promise-dialog__container--danger': danger
                    }) }
                >
                    <div className='promise-dialog__header'>
                        <h3 className='promise-dialog__title'>{ title }</h3>
                        { subTitle && <h6 className='promise-dialog__sub-title'>{ subTitle }</h6> }
                    </div>

                    <div className='promise-dialog__body'>{ content }</div>

                    <div className='promise-dialog__footer'>
                        <button
                            className='promise-dialog__button promise-dialog__button--cancel'
                            onClick={ () => {
                                reject(props);
                                this.close();
                            } }
                        >{ cancelText }</button>

                        <button
                            className='promise-dialog__button promise-dialog__button--submit'
                            onClick={ () => {
                                resolve(props);
                                if (closeOnSubmit) this.close();
                            } }
                        >{ submitText }</button>
                    </div>
                </div>
            );

            this.setState({ opened: true, modal });
        });
    };

    close = () => {
        this.setState({ opened: false, modal: null });
    };

    render() {
        const { opened, modal } = this.state;

        return (
            <div
                className={cx({
                    'promise-dialog': true,
                    'promise-dialog--opened': opened
                })}
                onClick={ this.handleClick }
            >
                <div className='promise-dialog__container-wrapper'>{ modal }</div>
            </div>
        );
    }
}

import React from 'react';
import avatarImgDef from "../../images/avatar.png";
import PlayElement from "../Announcer/ItemList/PlayElement";
import './RecommendedAnnouncer.css';

const RecommendedAnnouncer = (props) => {
  const {announcer, srvr} = props;

  return (<div className="announcer-similar__unit">
      <span className="announcer-similar__avatar">
        <span className="announcer-similar__play" data-announcer-id={announcer.id}>
            <PlayElement
              {...props}
              {...announcer}
            />
        </span>
        <img src={srvr + (announcer.avatar || avatarImgDef)} className="img-fluid" alt="Winfield"/>
      </span>

      <a href={"/profile/" + announcer.id} target={"_blank"}
         className="announcer-similar__name">{announcer.firstname}</a>

      <span className="announcer-similar__price">
        <span>до 30 сек</span>
        {' '}
        <b>{announcer.cost} ₽</b>
      </span>
    </div>
  )
};

export default RecommendedAnnouncer;

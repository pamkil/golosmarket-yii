<?php

//https://ionicons.com/v4/usage/
//https://ionicons.com/v4/
$smallValue = $smallValue ?? '';

?>
<div class="info-box">
    <span class="info-box-icon bg-aqua"><i class="icon ion-md-shuffle"></i></span>
    <div class="info-box-content">
        <span class="info-box-text"><?= $title; ?></span>
        <span class="info-box-number"><?= $value; ?><small> <?= $smallValue?></small></span>
    </div>
</div>
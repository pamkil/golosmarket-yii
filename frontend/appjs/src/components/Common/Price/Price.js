import React from 'react';

import './price.css';

const toPriceFormat = ( price ) => {
    return ( typeof price === 'string' || typeof price === 'number' ) ?
        price.toString().replace( /(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1\u00a0' ) :
        price;
};
const Price = ( { children } ) => (
    children === 0 ?  <span className='price-format text-price'>По запросу</span> :
    <span className='price-format'>{toPriceFormat( children )} ₽</span>

);

export default Price;

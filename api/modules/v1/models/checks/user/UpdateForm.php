<?php

namespace api\modules\v1\models\checks\user;

use api\modules\v1\models\records\announcer\UserEdit;
use api\modules\v1\models\records\file\File;
use api\modules\v1\models\records\user\City;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * @OA\Schema(
 *     schema="UserUpdateForm",
 *     required={"username", "password", "email"},
 *     title="Форма создания пользователя"
 * )
 */
class UpdateForm extends Model
{
    /**
     * @OA\Property(
     *     type="string",
     *     description="Имя или псевдоним",
     *     title="firstname"
     * )
     *
     * @var string
     */
    public $firstname;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Фамилия",
     *     title="lastname",
     * )
     *
     * @var string
     */
    public $lastname;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Пароль",
     *     title="password",
     * )
     *
     * @var string
     */
    public $password;

    /**
     * @OA\Property(
     *     type="string",
     *     description="email",
     *     title="email",
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Телефон в международном формате",
     *     title="phone",
     * )
     *
     * @var string
     */
    public $phone;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Наименование города",
     *     title="city"
     * )
     *
     * @var string
     */
    public $city;

    /**
     * @OA\Property(
     *     type="file",
     *     description="Аватар",
     *     title="photo"
     * )
     *
     * @var string
     */
    public $photo;

    /**
     * @var UserEdit
     */
    private $_user;

    /**
     * @var UploadedFile
     */
    public $file;

    /** @var File */
    private $fileModel;

    private $id;

    public function formName()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'city',
                'default'
            ],
            [
                ['email'],
                'email',
            ],
            [
                ['email'],
                'unique',
                'targetClass' => UserEdit::class,
                'filter' => ['<>', 'id', $this->id],
            ],
            [
                ['firstname', 'lastname', 'phone', 'password'],
                'string',
                'max' => 50
            ],
            [
                ['phone'],
                'filter',
                'filter' => function ($value) {
                    $value = preg_replace('/[^0-9]/', '', $value);

                    preg_match('/(?<prefix>\+?7|8)?(?<normal>\d{10})/', $value, $matches);

                    if (empty($matches['normal'])) {
                        return '';
                    }

                    return '+7' . $matches['normal'];
                }
            ],
        ];
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Finds user by [[username]]
     *
     * @return UserEdit|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = UserEdit::findByEmail($this->email);
        }

        return $this->_user;
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $cityId = null;
        if (!empty($this->city['title'])) {
            $city = City::find()
                ->where(['title' => $this->city['title']])
                ->asArray()
                ->one();
            if (!$city) {
                $city = City::create(['title' => $this->city['title']]);
            }
            if ($city) {
                $cityId = $city['id'];
            }
        }

        $array = [
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'email' => $this->email,
            'phone' => $this->phone,
            'city_id' => $cityId,
        ];

        if (!empty($this->password)) {
            $array['password'] = $this->password;
        }

        return $array;
    }

    public function upload()
    {
        if ($this->validate()) {
            if ($this->file) {
                $this->fileModel = File::setFileByUpload($this->file, 180);
            }
            return true;
        } else {
            return false;
        }
    }

    public function getFileModel()
    {
        return $this->fileModel ?? null;
    }
}

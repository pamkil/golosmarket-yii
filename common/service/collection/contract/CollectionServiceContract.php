<?php

namespace common\service\collection\contract;

use common\models\content\Collection;

interface CollectionServiceContract
{
    public function getEmptyModel(): Collection;
    public function create(array $params): Collection;
    public function update(Collection $model, array $params): Collection;
}

<?php

use admin\models\review\ReviewServiceSearch;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\helpers\ArrayHelper;

/**
 * @var ActiveDataProvider $dataProvider
 * @var ReviewServiceSearch $searchModel
 */
?>

<div class="box box-primary">
    <div class="box-body">
        <?= GridView::widget(
            [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    'id',
                    'is_public:boolean',
                    'show_quantity:integer',

                    [
                        'label' => 'Рейтинг',
                        'attribute' => 'rating',
                        'filter' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5],
                        'filterInputOptions' => ['prompt' => 'Все', 'class' => 'form-control', 'id' => null],
                    ],
                    'text:text',
                    'created_at' => [
                        'class' => DataColumn::class,
                        'attribute' => 'created_at',
                        'format' => 'datetime',
                        'vAlign' => GridView::ALIGN_MIDDLE,
                        'hAlign' => GridView::ALIGN_CENTER,
                        'width' => '180px',
                        'filter' => DatePicker::widget(
                            ArrayHelper::merge(
                                [
                                    'model' => $searchModel,
                                    'attribute' => 'date_at',
                                    'attribute2' => 'date_to',
                                    'separator' => '-',
                                    'type' => DatePicker::TYPE_RANGE,
                                    'options' => ['placeholder' => 'От'],
                                    'options2' => ['placeholder' => 'До', 'autocomplete' => 'off'],
                                ],
                                Yii::$app->params['datepicker_options']
                            )
                        ),
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{update}  {delete}',
                        'buttonOptions' => ['class' => 'margin']
                    ],
                ]
            ]
        ); ?>
    </div>
</div>

<?php

namespace frontend\controllers;

use admin\models\content\FaqSearch;
use common\models\content\Faq;
use common\models\content\Question;
use yii\web\NotFoundHttpException;

/**
 * Faq controller
 */
class FaqController extends BaseController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $faqs = Faq::find()
            ->where(
                [
                    'active' => true,
                ]
            )
            ->orderBy(['position' => SORT_ASC])
            ->asArray()
            ->all();

        return $this->render(
            'index',
            [
                'faqs' => $faqs,
            ]
        );
    }
}

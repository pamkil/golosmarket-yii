import React, {useCallback, useEffect, useState} from "react";
import {connect, useDispatch, useSelector} from "react-redux";
import {toast} from 'react-toastify';

import {announcerChangeFavorite, getProfile, setFavLocalList} from "../../data/redux";
import {FavoriteService} from "../../data/services";
import ItemButtons from "../Announcer/ItemList/ItemButtons";
import {textAddFavorite, textRemoveFavorite} from "../Favorites/Favorites";

function convertDateFormat(dateString) {
  const date = new Date(dateString);
  const day = date.getDate().toString().padStart(2, '0');
  const month = (date.getMonth() + 1).toString().padStart(2, '0');
  const year = date.getFullYear().toString().slice(2);
  const hours = date.getHours().toString().padStart(2, '0');
  const minutes = date.getMinutes().toString().padStart(2, '0');
  return `Был(а) в сети ${day}.${month}.${year} в ${hours}:${minutes}`;
}

let ProfileControls = (props) => {
  let {
    isInit,
    isLogin,
    favList,
    userProfile,
    online,
    profile,
    payments,
    orders,
    announcerChangeFavorite,
    setFavLocalList
  } = props;
  const dispatch = useDispatch()
  useEffect(()=>{
    dispatch(getProfile());
  },[])

  const {
    user
  } = useSelector(state => {
    return {
      user: state.user
    }
  });
  let hideChatBtn = profile.isAnnouncer || (userProfile.id === profile.id) || (!userProfile.isAnnouncer);

  const addToFavoriteHandler = useCallback((id, isFavorite, quantityFavorite) => {
    if (isLogin) {
      let service;
      if (isFavorite) {
        service = FavoriteService.removeFromFavorite(id);
        favList.splice(favList.indexOf(id), 1);
      } else {
        service = FavoriteService.addToFavorite(id);
        favList.push(id);
      }
      service.then(({data: {quantity}}) => {
        announcerChangeFavorite(id, !isFavorite, quantity);
      });
      toast.info(isFavorite ? textRemoveFavorite : textAddFavorite);
    } else {
      if (favList.indexOf(id) >= 0) {
        favList.splice(favList.indexOf(id), 1);

        announcerChangeFavorite(id, false, quantityFavorite - 1);

        toast.info(textRemoveFavorite);
      } else {
        favList.push(id);

        announcerChangeFavorite(id, true, quantityFavorite + 1);

        toast.info(textAddFavorite);
      }

      setFavLocalList(favList);

    }
  }, [favList, userProfile.id]);

  useEffect(() => {
    let row = document.getElementById('save-row');
    row.innerHTML = hideChatBtn ? '' : '<a class="green to-message write" href="' + (isLogin ? '/chat/' + userProfile.id : '#registry') + '">Написать</a>';
  }, []);

  return (<>
      {parseFloat(userProfile.rating) > 0 ? <div className="likeunlike">
          <span className="star">
              <svg xmlns="http://www.w3.org/2000/svg" width="17" height="22" viewBox="0 -4 24 24"
                   fill="#77BF22" stroke="#77BF22" strokeWidth="1.5" strokeLinecap="round"
                   strokeLinejoin="round">
                  <polygon
                    points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"/></svg>
          </span>
        <span className="likeunlike-green">{userProfile.rating}</span>
      </div> : null}

      {userProfile.isAnnouncer ?
        <>
          <ItemButtons
            favList={favList}
            hideHeart={userProfile.id === profile.id}
            quantityFavorite={userProfile.favoritesCount}
            isFavorite={userProfile.isFavorite}
            addToFavorite={addToFavoriteHandler}
            id={userProfile.id}
            />
          <div className="online">{online}</div>
           <p className="date-format-text">{userProfile && convertDateFormat(userProfile.last_visit)}</p>
          {hideChatBtn ? null :
            <a className="green to-message write" href={isLogin ? "/chat/" + userProfile.id : "#registry"}>Написать</a>}
        </> : <div className={'likeunlike-stat'}>Заказов {orders} {orders > 0 ? ', оплачено ' + Math.ceil(100 * (parseInt(payments) / parseInt(orders))) + '%' : ''}</div>}

    </>
  );
}

const mapStateToProps = ({review, announcer, auth, user, app, favLocalList, profile, ...state}) => {
  return {
    localDev: app.localDev,
    mobile: app.mobile,
    favList: favLocalList.favList,
    announcers: announcer.announcers,
    userId: user.user ? user.user.id : 0,
    isLoadingAnnouncer: announcer.isLoadingAnnouncer || false,
    isAnnouncer: user.user ? user.user.isAnnouncer : false,
    reviews: review.reviews,
    isLoadingReview: review.isLoading,
    isLogin: !!auth.token,
    ...profile.options,
    initialValues: profile.profile,
    isInit: app.initialized
  };
};

ProfileControls = connect(
  mapStateToProps,
  {
    setFavLocalList, announcerChangeFavorite
  }
)(ProfileControls);

export default ProfileControls;

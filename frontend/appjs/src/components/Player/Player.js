import React, {
  useEffect,
  useMemo,
  useRef,
  useState,
  useCallback,
} from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import classes from "classnames";
import { toast } from "react-toastify";

import { isString } from "lodash";

import { AnnouncerService, FavoriteService } from "../../data/services";
import { announcerChangeFavorite } from "../../data/redux";
import { PLAYER } from "../Common/PlayerEvent/playerConst";
import { eventPlayed, eventStop } from "../Common/PlayerEvent/playerEvent";
import {
  textAddFavorite,
  textCopy,
  textRemoveFavorite,
} from "../Announcer/Announcer";

import { ReactComponent as FavoriteRed } from "../../images/icon-heart--active.svg";
import { ReactComponent as Favorite } from "../../images/icon-heart.svg";
import { ReactComponent as Volume } from "../../images/volume.svg";
import { ReactComponent as Prev } from "../../images/skip-track-l2.svg";
import { ReactComponent as Next } from "../../images/skip-track-r2.svg";
import { ReactComponent as Play } from "../../images/play-track.svg";
import { ReactComponent as Pause } from "../../images/pause-track.svg";
import ShareButton from "../Common/ShareButton/ShareButton";
import { ReactComponent as MsgSquare } from "../../images/icon-msg-square.svg";
import Price from "../Common/Price/Price";
import { setFavLocalList } from "../../data/redux";
import config from "../../data/config/";

const Player = ({
  localDev,
  favList,
  favListCount,
  announcerChangeFavorite,
  setFavLocalList,
}) => {
  const audioPlayer = useMemo(() => new Audio(), []);
  const volumeInput = useRef(null);
  const timestampInput = useRef(null);
  const [file, setFile] = useState(null);
  const [files, setFiles] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [announcerView, setAnnouncer] = useState(null);
  const [show, setShow] = useState(false);
  const [playing, setPlaying] = useState(false);
  const [volume, setVolume] = useState(100);
  const [prevVolume, setPrevVolume] = useState(0);
  const [position, setPosition] = useState(0);
  const dispatch = useDispatch();
  const isLogin = useSelector((state) => !!state.auth.token);
  const isAnnouncer = useSelector(
    (state) => !!state.user.user && state.user.user.isAnnouncer
  );
  const [isLocalFav, setIsLocalFav] = useState(false);

  useEffect(() => {
    if (!audioPlayer) {
      return;
    }

    const playEventHandler = () => setPlaying(true);
    const stopHandler = () => {
      setPlaying(false);
      eventStop();
    };
    const timeupdateHandler = () => {
      return setPosition(
        (audioPlayer.currentTime * 100) / audioPlayer.duration
      );
    };
    const errorHandler = () => {
      toast.error("Ошибка воспроизведения файла!");
      setPlaying(false);
      eventStop();
    };

    audioPlayer.addEventListener("playing", playEventHandler);
    audioPlayer.addEventListener("abort", stopHandler);
    audioPlayer.addEventListener("ended", stopHandler);
    audioPlayer.addEventListener("pause", stopHandler);
    audioPlayer.addEventListener("timeupdate", timeupdateHandler);
    audioPlayer.addEventListener("error", errorHandler);

    return () => {
      audioPlayer.removeEventListener("playing", playEventHandler);
      audioPlayer.removeEventListener("abort", stopHandler);
      audioPlayer.removeEventListener("ended", stopHandler);
      audioPlayer.removeEventListener("pause", stopHandler);
      audioPlayer.removeEventListener("timeupdate", timeupdateHandler);
      audioPlayer.removeEventListener("error", errorHandler);
    };
  }, [audioPlayer]);

  useEffect(() => {
    if (playing) {
      eventPlayed(file);
    }
    // eslint-disable-next-line
  }, [playing, eventPlayed]);

  useEffect(() => {
    const playEventHandler = ({
      detail: { announcer, file, files = [], showPlayer = false },
    }) => {
      const setAll = (dataAnnouncer) => {
        setFile({ ...file });
        setFiles(files);
        setShow(showPlayer);
        setAnnouncer(dataAnnouncer);
        setCurrentIndex(files.findIndex((item) => item.url === file.url));
      };

      if (audioPlayer && !audioPlayer.paused) {
        audioPlayer.pause();
      }

      if (
        (isString(announcer) && parseInt(announcer) > 0) ||
        Number.isInteger(announcer)
      ) {
        AnnouncerService.get({}, announcer)
          .then(({ data }) => {
            setAll(data);
          })
          .catch(() => {
            setAll({});
          });
      } else {
        setAll(announcer);
      }
    };
    document.addEventListener(PLAYER.TO_PLAY, playEventHandler);
    document.addEventListener(PLAYER.STOP, stopEventHandler);

    return () => {
      document.removeEventListener(PLAYER.TO_PLAY, playEventHandler);
      document.removeEventListener(PLAYER.STOP, stopEventHandler);
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    let startDown = false;
    const element = volumeInput.current;
    if (!element) {
      return;
    }
    const { width } = element.getBoundingClientRect();
    const percentWidth = (offset) => Math.min(100, (offset * 100) / width);

    const mouseupHandler = () => (startDown = false);
    const mousedownHandler = ({ offsetX }) => {
      startDown = true;
      setVolume(percentWidth(offsetX));
    };
    const mousemoveHandler = ({ offsetX }) => {
      if (startDown) {
        setVolume(percentWidth(offsetX));
      }
    };

    element.addEventListener("mousedown", mousedownHandler);
    element.addEventListener("mousemove", mousemoveHandler);
    window.addEventListener("mouseup", mouseupHandler);

    return () => {
      if (!element) {
        return;
      }
      element.removeEventListener("mousedown", mousedownHandler);
      element.removeEventListener("mousemove", mousemoveHandler);
      window.removeEventListener("mouseup", mouseupHandler);
    };
    // eslint-disable-next-line
  }, [show]);

  useEffect(() => {
    const element = timestampInput.current;
    if (!element) {
      return;
    }

    document.body.classList.add("player__opened");

    const { width } = element.getBoundingClientRect();
    const percentWidth = (offset) => (offset * 100) / width;

    const mousedownHandler = ({ offsetX }) => {
      if (!audioPlayer.duration) {
        return;
      }
      const percentPosition = percentWidth(offsetX);

      audioPlayer.currentTime = parseInt(
        (audioPlayer.duration * percentPosition) / 100
      );
    };

    element.addEventListener("mousedown", mousedownHandler);

    return () => {
      document.body.classList.remove("player__opened");

      if (element) {
        element.removeEventListener("mousedown", mousedownHandler);
      }
    };
    // eslint-disable-next-line
  }, [show]);

  const playHandler = useCallback(() => {
    if (!audioPlayer) {
      return;
    }

    audioPlayer.addEventListener("canplay", () => {
      audioPlayer.play();
    });

    if (audioPlayer.src !== file.url && !audioPlayer.src.includes(file.url)) {
      if (localDev) {
        audioPlayer.src = config.URL + file.url;
      } else if (process.env.NODE_ENV !== "production") {
        audioPlayer.src = "http://golosmarket.tk" + file.url;
      } else {
        audioPlayer.src = file.url;
      }
    } else {
      audioPlayer.play();
    }
  }, [audioPlayer, file]);

  useEffect(() => {
    if (!file) {
      setCurrentIndex(0);
      return;
    }

    playHandler();
  }, [file, playHandler]);

  useEffect(() => {
    if (!audioPlayer) {
      return;
    }

    audioPlayer.volume = Math.max(Math.min(volume / 100, 1), 0);
  }, [volume, audioPlayer]);

  useEffect(() => {
    if (
      announcerView &&
      announcerView.hasOwnProperty("id") &&
      favList.length > 0
    ) {
      setIsLocalFav(favList.indexOf(announcerView.id) >= 0);
    }
  }, [favListCount, announcerView]);

  const stopEventHandler = () => {
    if (!audioPlayer) {
      return;
    }

    stopHandler(false);
  };

  const stopHandler = (sendEvent = true) => {
    if (audioPlayer) {
      if (!audioPlayer.paused) {
        audioPlayer.pause();
      }

      if (sendEvent) {
        eventStop();
      }
    }
  };

  if (!show || !file || !announcerView) {
    return null;
  }

  const addToFavoriteHandler = (id, isFavorite, quantityFavorite) => {
    if (isLogin) {
      let service;
      if (isFavorite) {
        service = FavoriteService.removeFromFavorite(id);
        favList.splice(favList.indexOf(id), 1);
      } else {
        service = FavoriteService.addToFavorite(id);
        favList.push(id);
      }
      service.then(({ data: { quantity } }) => {
        announcerChangeFavorite(id, !isFavorite, quantity);
      });
      toast.info(isFavorite ? textRemoveFavorite : textAddFavorite);
    } else {
      if (favList.indexOf(id) >= 0) {
        favList.splice(favList.indexOf(id), 1);

        announcerChangeFavorite(id, false, quantityFavorite - 1);

        toast.info(textRemoveFavorite);
      } else {
        favList.push(id);

        announcerChangeFavorite(id, true, quantityFavorite + 1);

        toast.info(textAddFavorite);
      }

      setFavLocalList(favList);
    }
  };

  const closeHandler = () => {
    stopHandler(true);
    setShow(false);
  };

  const messageHandler = () => {
    if (!isAnnouncer && !isLogin) {
      window.location.hash = "#registry";
    }
  };

  const isNextFile = currentIndex > -1 && currentIndex < files.length - 1;
  const isPrevFile = currentIndex > -1 && currentIndex > 0;

  const nextHandler = () => {
    if (!isNextFile) {
      return;
    }

    const newIndex = currentIndex + 1;
    const newFile = files[newIndex];
    setFile(newFile);
    setCurrentIndex(newIndex);
    if (newFile.announcer) {
      setAnnouncer(newFile.announcer);
    }
  };

  const prevHandler = () => {
    if (!isPrevFile) {
      return;
    }

    const newIndex = currentIndex - 1;
    const newFile = files[newIndex];
    setFile(newFile);
    setCurrentIndex(newIndex);

    if (newFile.announcer) {
      setAnnouncer(newFile.announcer);
    }
  };

  const leadingZero = (val) => {
    return ("0" + val).slice(-2);
  };

  const timeFormatter = (val) => {
    val = val || 0;
    let H = Math.floor(val / 3600);
    let M = Math.floor(val / 60);
    let S = Math.floor(val % 60);

    return (H > 0 ? H + ":" : "") + leadingZero(M) + ":" + leadingZero(S);
  };

  let msgIcon = (
    <div className={"tooltip"}>
      <div className="icon">
        <MsgSquare />
        <span>Написать&nbsp;в&nbsp;чат</span>
      </div>
    </div>
  );

  let srvr = localDev ? config.URL : "";

  return (
    <section className="fixed-player">
      <div className="container">
        <div className="player">
          <div className="close pointer" onClick={closeHandler}>
            &#10005;
          </div>
          <div className="line-play pointer" ref={timestampInput}>
            <div className="track-name">{file.name || ""}</div>
            <div className="track-player">
              <span className={"track-dot"} style={{ width: `${position}%` }} />
              <span className={"track-time"}>
                {timeFormatter((position * audioPlayer.duration) / 100)}
              </span>
              <span className={"track-duration"}>
                {timeFormatter(audioPlayer.duration)}
              </span>
            </div>
          </div>
          {announcerView && announcerView.id && (
            <a
              href={`/profile/${announcerView.id}`}
              className="profile-link pointer"
            >
              <div className="item item-user">
                <div className="photo">
                  <div className="frame">
                    <img
                      src={srvr + announcerView.avatar || ""}
                      alt={announcerView.firstname}
                    />
                  </div>
                  <div
                    className={classes({ online: announcerView.isOnline })}
                  />
                </div>
                <div className="name-cost">
                  <span className={"name"}>
                    {announcerView.firstname}
                    {announcerView.cost ? (
                      <>
                        ,&nbsp;
                        <Price>{announcerView.cost}</Price>
                      </>
                    ) : (
                      ""
                    )}
                  </span>
                  <span className={"reds"}>{announcerView.timeText}</span>
                </div>
              </div>
            </a>
          )}

          <div className="item item-control">
            <button
              type="button"
              className="pointer player__button player__button--prev"
              onClick={prevHandler}
              disabled={!isPrevFile}
            >
              <Prev />
            </button>
            <button
              type="button"
              className="pointer player__button player__button--play"
            >
              {playing ? (
                <Pause onClick={() => stopHandler(true)} />
              ) : (
                <Play onClick={playHandler} />
              )}
            </button>
            <button
              type="button"
              className="pointer player__button player__button--next"
              onClick={nextHandler}
              disabled={!isNextFile}
            >
              <Next />
            </button>
          </div>
          <div className="item item-volume mobile-no">
            <button
              className={volume <= 0 && `button-volume-cross`}
              onClick={() => {
                if (volume !== 0) {
                  setPrevVolume(volume)
                  setVolume(0);
                }
                else{
                  setVolume(prevVolume)
                }
              }}
            >
              <Volume />
            </button>
            <div className="line-vol pointer" ref={volumeInput}>
              <span className="volume" style={{ width: `${volume}%` }} />
            </div>
          </div>

          <div className="item item-last">
            {!isAnnouncer &&
              (!isLogin ? (
                <button
                  className="send-announcer-button"
                  disabled={isAnnouncer}
                  type="button"
                  onClick={messageHandler}
                >
                  {msgIcon}
                </button>
              ) : (
                <a
                  className="send-announcer-button"
                  href={`/chat/${announcerView.id}`}
                >
                  {msgIcon}
                </a>
              ))}

            {announcerView && announcerView.id && (
              <span
                className="pointer player-fav"
                onClick={() =>
                  addToFavoriteHandler(
                    announcerView.id,
                    isLocalFav || announcerView.isFavorite,
                    announcerView.quantityFavorite
                  )
                }
              >
                <div className={"tooltip"}>
                  <div className="icon">
                    {isLocalFav || announcerView.isFavorite ? (
                      <FavoriteRed />
                    ) : (
                      <Favorite />
                    )}
                    <span
                      dangerouslySetInnerHTML={{
                        __html:
                          isLocalFav || announcerView.isFavorite
                            ? "Удалить&nbsp;из&nbsp;Избранного"
                            : "Добавить&nbsp;в&nbsp;Избранные&nbsp;голоса",
                      }}
                    />
                  </div>
                </div>
              </span>
            )}

            {/*<a*/}
            {/*  download={file.name}*/}
            {/*  href={file.url}*/}
            {/*  className="mobile-no pointer"*/}
            {/*  target="_blank"*/}
            {/*  rel="noopener noreferrer"*/}
            {/*>*/}
            {/*  <img src={download} alt="скачать" title="Скачать звуковую дорожку"/>*/}
            {/*</a>*/}

            <span className="pointer player-share">
              <ShareButton
                top={true}
                url={document.location.origin + file.url}
                popup={textCopy}
                announcerId={announcerView.id}
                announcerName={announcerView.firstname}
              />
            </span>
          </div>
        </div>
      </div>
    </section>
  );
};

function mapStateToProps({ app, favLocalList }) {
  return {
    favList: favLocalList.favList,
    favListCount: favLocalList.favList.length,
    localDev: app.localDev,
  };
}

export default connect(mapStateToProps, {
  setFavLocalList,
  announcerChangeFavorite,
})(Player);

<?php

namespace api\modules\v1\actions\announcer;

use api\modules\v1\actions\common\ViewAction;
use api\modules\v1\models\records\announcer\Announcer;

/**
 * @OA\Get(
 *      path="/announcer/id",
 *      summary="",
 *      tags={"Announcer"},
 *      @OA\Parameter(
 *          name="id",
 *          in="path",
 *          description="User ID, ИД собеседника",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="ok",
 *          @OA\JsonContent(
 *              type="array",
 *              @OA\Items(ref="#/components/schemas/Bill")
 *          )
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */
class View extends ViewAction
{
    /** @inheritdoc */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $announcer = Announcer::find()
            ->where(['user_id' => (int)$id])
            ->one();

        return $announcer;
    }
}

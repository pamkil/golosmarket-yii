<?php

use common\models\access\User;
use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/**
 * @var ActiveDataProvider $dataProvider
 * @var int $percent
 */
?>

<div class="box box-primary">
    <div class="box-body">
        <?= GridView::widget(
            [
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    'id:integer:ИД',
                    [
                        'label' => 'Диктор',
                        'attribute' => 'announcer',
                        'width' => '310px',
                        'value' => function ($user) {
                            return "{$user->lastname} {$user->firstname} ({$user->email})";
                        },
                        'filter' => false,
                        'group' => true,
                    ],
                    [
                        'label' => 'Исполнитель получил',
                        'attribute' => 'announcer.cash_yamoney',
                        'content' => function (User $user) use ($percent) {
                            $sum = 0.;
                            foreach ($user->bills as $bill) {
                                $sum += $bill->sum;
                            }

                            return Yii::$app->formatter->asCurrency($sum);
                        },
                    ],
                    [
                        'label' => 'Подробно',
                        'attribute' => 'announcer.cash_yamoney',
                        'content' => function (User $user) {
                            $billEc = [];
                            foreach ($user->bills as $bill) {
                                $sum = Yii::$app->formatter->asCurrency($bill->sum);
                                $date = Yii::$app->formatter->asDatetime($bill->date);
                                $billEc[] = "<span><b>$sum</b> ($date)</span>";
                            }

                            return implode('<br>', $billEc);
                        },
                    ],
                    [
                        'label' => 'Сумма сайту',
                        'attribute' => 'announcer.cash_yamoney',
                        'content' => function (User $user) use ($percent) {
                            $sum = 0.;
                            foreach ($user->bills as $bill) {
                                $sum += ($bill->sum * $percent) / 100;
                            }

                            return Yii::$app->formatter->asCurrency($sum);
                        },
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{update}',
                        'updateOptions' => ['label' => '<i class="glyphicon glyphicon-ruble" title="Оплатить"></i>'],
                        'buttons' => [
                            'update' => function ($url, User $model) {
                                $ids = [];
                                foreach ($model->bills as $bill) {
                                    $ids[] = $bill->id;
                                }

                                return Html::a(
                                    '<span class="glyphicon glyphicon-ok-sign" title="Отметить оплату вручную"></span>',
                                    [
                                        'update',
                                        'id' => $model->id,
                                    ],
                                    [
                                        'data' => [
                                            'confirm' => 'Вы уверены что хотите отметить оплату?',
                                            'form' => 'none',
                                            'method' => 'post',
                                            'pjax' => '1',
                                            'pjax-push' => '0',
                                            'params' => [
                                                'ids' => $ids
                                            ],
                                        ]
                                    ]
                                );
                            }
                        ]
                    ],
                ]
            ]
        ); ?>
    </div>
</div>

<?php

namespace admin\models\bill;

use Carbon\Carbon;
use common\models\access\User;
use common\models\user\Bill;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class BillSearch extends Model
{
    public $announcer;
    public $client;
    public $announcer_id;
    public $client_id;
    public $status;
    public $sum;
    public $percent;
    public $is_canceled;
    public $is_payed;
    public $is_send;
    public $date;
    public $date_at;
    public $date_to;

    public function rules()
    {
        return [
            [
                ['announcer_id', 'client_id', 'status'],
                'integer'
            ],
            [
                ['announcer', 'client'],
                'string'
            ],
            [
                ['sum', 'percent'],
                'number'
            ],
            [
                ['is_canceled', 'is_payed', 'is_send'],
                'boolean'
            ],
            [
                ['date'],
                'filter',
                'filter' => function ($val) {
                    if (empty($val)) {
                        return '';
                    }

                    return Carbon::parse($val)->toDateTimeString();
                }
            ],
            [
                ['date_at'],
                'filter',
                'filter' => function ($val) {
                    if (empty($val)) {
                        return '';
                    }

                    return Carbon::parse($val)->toDateTimeString();
                }
            ],
            [
                ['date_to'],
                'filter',
                'filter' => function ($val) {
                    if (empty($val)) {
                        return '';
                    }

                    return Carbon::parse($val)->endOfDay()->toDateTimeString();
                }
            ],
        ];
    }

    public function search($params)
    {
        $query = Bill::find()
            ->alias('bill')
            ->joinWith(['announcer announcer', 'client client']);
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'bill.id',
                        'bill.announcer_id',
                        'bill.client_id',
                        'bill.status',
                        'bill.sum',
                        'bill.percent',
                        'bill.date',
                        'bill.is_canceled',
                        'bill.is_payed',
                        'bill.is_send',
                    ],
                    'defaultOrder' => [
                        'bill.date' => SORT_DESC
                    ],
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->announcer)) {
            $query->andWhere(
                [
                    'OR',
                    ['like', 'announcer.firstname', $this->announcer],
                    ['like', 'announcer.lastname', $this->announcer],
                ]
            );
        }

        if (!empty($this->client)) {
            $query->andWhere(
                [
                    'OR',
                    ['like', 'client.firstname', $this->client],
                    ['like', 'client.lastname', $this->client],
                ]
            );
        }

        $query->andFilterWhere(
            [
                'AND',
//                ['like', 'user.username', $this->username],
//                ['like', 'user.firstname', $this->firstname],
//                ['like', 'user.lastname', $this->lastname],
//                ['like', 'user.email', $this->email],
//                ['like', 'user.phone', $this->phone],
//                ['like', 'user.last_visit', $this->last_visit],
//                ['user.status' => $this->status],
//                ['user.is_blocked' => $this->is_blocked],
//                ['announcer.likes' => $this->likes],
//                ['announcer.dislikes' => $this->dislikes],
//                ['announcer.cost' => $this->cost],
                ['bill.announcer_id' => $this->announcer_id],
                ['bill.client_id' => $this->client_id],
                ['bill.status' => $this->status],
                ['bill.sum' => $this->sum],
                ['bill.percent' => $this->percent],
                ['bill.is_canceled' => $this->is_canceled],
                ['bill.is_payed' => $this->is_payed],
                ['bill.is_send' => $this->is_send],
                ['bill.date' => $this->date],
                ['>=', 'bill.date', $this->date_at],
                ['<=', 'bill.date', $this->date_to],
            ]
        );

        if (!empty($this->date_at)) {
            $this->date_at = Carbon::parse($this->date_at)->format('d.m.Y');
        }
        if (!empty($this->date_to)) {
            $this->date_to = Carbon::parse($this->date_to)->format('d.m.Y');
        }

        return $dataProvider;
    }

    public function searchPay($params)
    {
        $query = User::find()
            ->alias('user')->with('announcer');
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'user.id',
                    ],
                    'defaultOrder' => [
                        'user.id' => SORT_DESC
                    ],
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $queryBill = Bill::find()
            ->select('announcer_id')
            ->where(['status' => Bill::STATUS_PAID_CLIENT]);

        $query->where(['id' => $queryBill])
            ->with(
                [
                    'bills' => function (\yii\db\ActiveQuery $query) {
                        $query->andWhere(['status' => Bill::STATUS_PAID_CLIENT]);
                    }
                ]
            )
            ->andFilterWhere(['user.announcer_id' => $this->announcer_id]);

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
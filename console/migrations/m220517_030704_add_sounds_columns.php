<?php

use yii\db\Migration;

/**
 * Class m220517_030704_add_sounds_columns
 */
class m220517_030704_add_sounds_columns extends Migration
{
    private $tableName = '{{%user_announcer_2_sound}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->tableName,
            'age_id',
            $this->integer()
        );
        $this->addColumn(
            $this->tableName,
            'lang_code',
            $this->string(2)->null()
        );
        $this->addColumn(
            $this->tableName,
            'parodist_name',
            $this->string()->null()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        if (Yii::$app->db->getTableSchema($this->tableName)->getColumn('parodist_name')) {
            $this->dropColumn($this->tableName, 'parodist_name');
        }
        if (Yii::$app->db->getTableSchema($this->tableName)->getColumn('lang_code')) {
            $this->dropColumn($this->tableName, 'lang_code');
        }
        if (Yii::$app->db->getTableSchema($this->tableName)->getColumn('age_id')) {
            $this->dropColumn($this->tableName, 'age_id');
        }
    }
}

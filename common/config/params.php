<?php
return [
    'adminEmail' => 'admin@example.local',
    'supportEmail' => 'support@example.local', //отсюда отсылаются все письма
    'senderEmail' => 'noreply@example.local',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,

    'emailFrom' => 'golosmarket@example.local',
    'emailTo' => 'golosmarket@example.local',

    'ya_notification_secret' => '01234567890ABCDEF01234567890',

    'subMinutes' => [
        'first_send' => 3, // если отправки смс небыло то ждём три минуты
        'other_send' => 60, // если отправка смс была то ждём час
    ],

    'commission-mail-path' => Yii::getAlias('@common').'/mail/commission-mail-template.txt'
];

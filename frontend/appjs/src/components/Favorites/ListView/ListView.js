import React from 'react';
import ItemList from "../ItemList/ItemList";
import Skeleton from "../Skeleton/Skeleton";
import Pagination from "../Pagination/Pagination";
import {connect} from "react-redux";

const ListView = ({
  isLoadingAnnouncer,
  items,
  showMore,
  copyHandler,
  mobile,
  showPagination,
  favList,
  favListCount,
  ...props
}) => {
  let skeletonItem = [1, 2, 3, 4, 5, 6, 7, 8];

  return (
    <>
      <div className="container">
        {isLoadingAnnouncer ?
          skeletonItem.slice(mobile ? -4 : 0)
              .map(
                  (item, ind) => <div key={ind} className="item item-skeleton"><Skeleton/></div>
              )
          : items.map(
              item => <ItemList copyHandler={copyHandler}
                                key={item.id} {...props} {...item}
                                favListCount={favListCount}
                                favList={favList}
                                announcer={item}/>
            )
        }
      </div>
      <div className="container">
        <Pagination/>
      </div>
    </>
  )
};

function mapStateToProps({app, favLocalList}) {
  return {
    favList: favLocalList.favList,
    favListCount: favLocalList.favList.length,
    mobile: app.mobile
  };
}

export default connect(mapStateToProps)(ListView);

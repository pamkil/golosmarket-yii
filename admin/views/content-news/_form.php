<?php

use common\models\content\News;
use dosamigos\tinymce\TinyMce;
use kartik\widgets\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var News $model
 */
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data' // important
    ]
]) ?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <?= Html::submitButton('<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord ? 'Создать' : 'Сохранить'), ['class' => 'btn btn-success']) ?>
        <?php if (!$model->isNewRecord) : ?>
            <?= Html::a('<i class="fa fa-times"></i>&nbsp;' . 'Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'title' => 'Удалить',
                'data-confirm' => 'Вы уверены что хотите удалить?',
                'data-method' => 'post'
            ]) ?>
        <?php endif; ?>
    </div>
    <div class="box-body">
        <?= $form->field($model, 'active')
            ->widget(\kartik\widgets\SwitchInput::class)->label(false) ?>
        <?= $form->field($model, 'date')
            ->widget(DatePicker::class, Yii::$app->params['datepicker_options']); ?>
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'description') ?>
        <?= $form->field($model, 'keywords') ?>
        <?= $form->field($model, 'h1') ?>
        <?= $form->field($model, 'body')
            ->widget(TinyMce::class, Yii::$app->params['tinyMceOptions'] ?? ''); ?>
    </div>
</div>

<?php ActiveForm::end() ?>

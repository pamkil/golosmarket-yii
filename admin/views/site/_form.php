<?
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use unclead\multipleinput\MultipleInput;
use admin\widgets\RestCheckbox;
use common\models\settings\Site;

/**
 * @var Site $model
 * @var array $currencies
 * @var array $domains
 */
?>

<? $form = ActiveForm::begin([
    'options' => [
        'data' => ['key' => $model->id, 'pjax' => true,],
        'enctype' => 'multipart/form-data' // important
    ]
]) ?>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <?= $form->field($model, 'active', ['options' => ['tag' => 'div', 'style' => 'display: inline-block;']])->widget(RestCheckbox::class, ['size' => 'big'])->label(false) ?>
                    <div class="actions btn-set">
                        <?= Html::submitButton('<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord ? Yii::t('admin', 'Создать') : Yii::t('admin', 'Сохранить')), ['class' => 'btn btn-success']) ?>
                        <? if (!$model->isNewRecord) { ?>
                            <?= Html::a('<i class="fa fa-copy"></i>&nbsp;' . Yii::t('admin', 'Копировать'), ['duplicate', 'id' => $model->id], [
                                'class' => 'btn yellow-crusta',
                                'title' => Yii::t('admin', 'Копировать'),
                                'data-confirm' => Yii::t('admin', 'Вы уверены что хотите скопировать?')
                            ]) ?>
                            <?= Html::a('<i class="fa fa-times"></i>&nbsp;' . Yii::t('admin', 'Удалить'), ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'title' => Yii::t('admin', 'Удалить'),
                                'data-confirm' => Yii::t('admin', 'Вы уверены что хотите удалить?'),
                                'data-method' => 'post'
                            ]) ?>
                        <? } ?>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_general" data-toggle="tab"><?= Yii::t('admin', 'Общие') ?> </a>
                            </li>
                            <li>
                                <a href="#tab_domains" data-toggle="tab"><?= Yii::t('admin', 'Домены') ?></a>
                            </li>
                            <? if (Yii::$app->user->can('viewHistory')) { ?>
                                <li>
                                    <a href="#tab_history" data-toggle="tab"><?= Yii::t('admin', 'История') ?> </a>
                                </li>
                            <? } ?>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_general">
                                <?= $form->field($model, 'name') ?>
                                <?= $form->field($model, 'position') ?>
                                <?= $form->field($model, 'lang_id')->dropDownList($langs) ?>
                                <?= $form->field($model, 'currency_id')->dropDownList($currencies) ?>
                                <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Создать') : Yii::t('admin', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="tab-pane" id="tab_domains">
                                <?= $form->field($model, 'domains')
                                    ->widget(MultipleInput::class, [
                                        'data' => $domains,
                                        'columns' => [
                                            [
                                                'name' => 'domains',
                                                'type' => 'textInput'
                                            ]
                                        ]
                                    ]) ?>
                            </div>
                            <? if (Yii::$app->user->can('viewHistory')) { ?>
                                <div class="tab-pane" id="tab_history">
                                    <?= $this->render('//history/_list', ['model' => $model, 'data' => $model->history]) ?>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? ActiveForm::end() ?>

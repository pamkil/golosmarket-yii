import React from "react";
import { Field, reduxForm } from 'redux-form';
import { FORMS } from '../../data/constants'
import InputInline from "../InputInline/InputInline";

let RestoreForm = ({ handleSubmit, pristine, submitting }) => (
    <form onSubmit={ handleSubmit } className="modal-form">
        <div className="itemы">
            <Field
                name="email"
                component={ InputInline }
                type="email"
                placeholder="E-mail"
            />
        </div>
        <div className="item">
            <button
                type="submit"
                disabled={ pristine || submitting }
            >
                Восстановить
            </button>
        </div>
    </form>
);

RestoreForm = reduxForm( {
    form: FORMS.RESTORE,
} )( RestoreForm );

export default RestoreForm;

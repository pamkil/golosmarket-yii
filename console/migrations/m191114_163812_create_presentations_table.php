<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_announcer_presentations}}`.
 */
class m191114_163812_create_presentations_table extends Migration
{
    private $presentations = '{{%user_announcer_presentations}}';
    private $announcer2presentations = '{{%user_announcer_2_presentations}}';
    private $announcer = '{{%user_announcer}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->presentations, [
            'id' => $this->primaryKey(),
            'active' => $this->boolean()->notNull()->defaultValue(true),
            'position' => $this->integer(),
            'code' => $this->string(25),
            'name' => $this->string(),
            'menu_title' => $this->string(),
        ], $tableOptions);
        $this->addCommentOnTable($this->presentations, 'Подача голоса');

        $this->createTable($this->announcer2presentations, [
            'announcer_id' => $this->integer(),
            'presentation_id' => $this->integer(),
            'PRIMARY KEY (announcer_id, presentation_id)'
        ], $tableOptions);

        $this->createIndex(
            'IX-user_announcer_2_presentations-announcer_id',
            $this->announcer2presentations,
            'announcer_id'
        );
        $this->addForeignKey(
            'FK-user_announcer_2_presentations-announcer_id',
            $this->announcer2presentations,
            'announcer_id',
            $this->announcer,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('IX-user_announcer_2_presentations-presentation_id', $this->announcer2presentations, 'presentation_id');
        $this->addForeignKey(
            'FK-user_announcer_2_presentations-presentation_id',
            $this->announcer2presentations,
            'presentation_id',
            $this->presentations,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addData();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->announcer2presentations);
        $this->dropTable($this->presentations);
    }

    private function addData()
    {
        $this->batchInsert(
            $this->presentations,
            ['name', 'menu_title', 'code', 'position'],
            [
//                ['Информационная', 'Информационная', 'informational', 1],
//                ['Игровая', 'Игровая', 'game', 2],
//                ['Дикторская', 'Дикторская', 'announcer', 3],
//                ['Автоответчик', 'Автоответчик', 'answering', 4],
//                ['Клубная', 'Клубная', 'клубная', 5],
//                ['Вокал', 'Вокал', 'vocals', 6],
//                ['Рэп', 'Рэп', 'rap', 7],
//                ['Брендвойс', 'Брендвойс', 'brandvois', 8],
//                ['Сексуальная', 'Сексуальная', 'sexy', 9],
//                ['Романтичная', 'Романтичная', 'romantic', 10],
//                ['Пафосная', 'Пафосная', 'pathetic', 11],
//                ['Сочная', 'Сочная', 'juicy', 12],
//                ['Звонкая', 'Звонкая', 'voiced', 13],
//                ['Глухая', 'Глухая', 'deaf', 14],
//                ['Конферансье', 'Конферансье', 'entertainer', 15],
//                ['Пародист', 'Пародист', 'parodist', 16],

                ['Главное демо', 'Главное демо', 'maindemo', 1],
                ['Рекламный ролик', 'Рекламный ролик', 'commercials', 2],
                ['Автоответчик', 'Автоответчик', 'answering', 3],
                ['Закадровый текст', 'Закадровый текст', 'offscreen', 4],
                ['Инструкция/скринкаст', 'Инструкция/скринкаст', 'instructions', 5],
                ['Вокал', 'Вокал', 'vocals', 6],
                ['Пародия', 'Пародия', 'parodist', 7],
                ['Аудиокнига', 'Аудиокнига', 'book', 8],
                ['Другая', 'Другая', 'other', 9],
                ['Нарезка разных работ', 'Нарезка разных работ', 'cutting_up_different_works', 10],
            ]
        );
    }
}

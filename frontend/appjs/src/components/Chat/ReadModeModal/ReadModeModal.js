import React, { useEffect, useRef, useState } from 'react';
import { ReactComponent as LeftImg } from '../../../images/left-arrow.svg';
import './ReadModeModal.css';


export default function ReadModeModal({ message, onClose }) {
    const [ fontSize, setFontSize ] = useState(20);
    const readModeTextRef = useRef(null);
    useEffect(()=>{
        if(readModeTextRef === null){
            return
        }
        readModeTextRef.current.innerHTML = message
    },[])
    return (
        <div className='read-mode-model'>
            <div className='read-mode-modal__container'>
                <div className='read-mode-model__header'>
                    <button
                        className='read-mode-modal__font-button'
                        onClick={() => setFontSize(fontSize - 2)}>A<sup>-</sup></button>
                    <button
                        className='read-mode-modal__font-button'
                        onClick={() => setFontSize(fontSize + 2)}>A <sup>+</sup></button>
                    <button
                        className='read-mode-modal__close-button'
                        onClick={onClose}
                    ><LeftImg/></button>
                </div>
                <p  
                    ref={readModeTextRef}
                    className='read-mode-model__text'
                    style={{ fontSize }}
                >{ message }</p>

                <div className='read-mode-modal__footer'>
                    <span>Режим начитки</span>
                    <p>GolosMarket</p>
                </div>
            </div>
        </div>
    )
}
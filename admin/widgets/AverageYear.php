<?php

namespace admin\widgets;

use Carbon\Carbon;
use common\models\user\Bill;
use yii\base\Widget;

class AverageYear extends Widget
{
    public function run()
    {
        parent::run();

        /**
         * "Средний доход за день: 950 - какой период брать? или за текущий день?"
         * - Давай возьмем период 12 мес. Можно так и показать 950/ 12мес
         *
         * Считаем сколько мы заработали
         */
        $dateLast = Carbon::today()->subMonth()->startOfMonth()->toDateTimeString();
        $dateEndLast = Carbon::today()->subMonth()->endOfMonth()->toDateTimeString();
        $sumLast = Bill::find()
            ->cache(60 * 3)
            ->where(
                [
                    'AND',
                    ['>=', 'updated_at', $dateLast],
                    ['<=', 'updated_at', $dateEndLast],
                    ['status' => Bill::STATUS_PAID_COMMISSION],
                ]
            )
            ->average('`sum` * `percent` / 100');

        echo $this->render('averageYear', ['money' => \Yii::$app->formatter->asCurrency(round($sumLast ?? 0, 2))]);
    }
}
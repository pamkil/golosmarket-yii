<?
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<? ActiveForm::begin() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="actions btn-set">
                        <?= Html::submitButton('<i class="fa fa-check"></i>&nbsp;' . (!isset($model) ? Yii::t('admin', 'Создать') : Yii::t('admin', 'Сохранить')), ['class' => 'btn btn-success']) ?>
                        <? if (isset($model)) { ?>
                            <?= Html::a('<i class="fa fa-times"></i>&nbsp;' . Yii::t('admin', 'Удалить'), ['delete-permission', 'name' => $model->name], [
                                'class' => 'btn btn-danger',
                                'title' => Yii::t('admin', 'Удалить'),
                                'data-confirm' => Yii::t('admin', 'Вы уверены что хотите удалить?'),
                                'data-method' => 'post'
                            ]) ?>
                        <? } ?>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_general" data-toggle="tab"><?= Yii::t('admin', 'Общие') ?></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_general">
                                <div class="form-group">
                                    <?= Html::label(Yii::t('admin', 'Символьный код'), '', ['class' => 'control-label']) ?>
                                    <?= Html::textInput('name', isset($model) ? $model->name : '', ['class' => 'form-control']) ?>
                                </div>
                                <div class="form-group">
                                    <?= Html::label(Yii::t('admin', 'Описание'), '', ['class' => 'control-label']) ?>
                                    <?= Html::textInput('description', isset($model) ? $model->description : '', ['class' => 'form-control']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? ActiveForm::end() ?>
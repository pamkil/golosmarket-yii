<?php

namespace api\modules\v1\actions\user;

use api\modules\v1\actions\common\IndexAction;
use api\modules\v1\models\filters\user\PayedSearch;
use common\service\user\dto\PayedSearchDto;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Yii;

class PayedView extends IndexAction
{
    /**
     * Список ролей
     * @OA\Get(
     *     path="/user/payed",
     *     summary="Получение информации о счёте и размере комиссии",
     *     tags={"User"},
     *     security={{"BearerAuth": {}}},
     *     @OA\Response(
     *         response="200",
     *         description="Объект токена",
     *         @OA\JsonContent(ref="#/components/schemas/Token")
     *     )
     * )
     *
     * @return mixed|null
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $dto = $this->getPayedSearchDto();
        if (!$dto->isAnnouncer) {
            throw new AccessDeniedException('Информация доступна только дикторам');
        }

        $payedSearchModel = new PayedSearch();
        $dataProvider = $payedSearchModel->search($dto);
        $confirmation = $payedSearchModel->searchNotConfirmation($dto);

        return [
            'paymentInfo' => empty($dataProvider->models) ? null : $dataProvider,
            'confirmationWait' => $confirmation,
        ];
    }

    private function getPayedSearchDto () {
        $user = Yii::$app->user;

        return new PayedSearchDto([
            'isAnnouncer' => $user->identity->isAnnouncer,
            'userId' => $user->id
        ]);
    }
}

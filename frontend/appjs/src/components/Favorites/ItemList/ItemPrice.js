import React from 'react'
import Price from '../../Common/Price/Price'

const ItemPrice = ({cost, costBy}) => {
  return <div className="cost">
    <Price>{cost}</Price>
    <span className="val-sec">
          до {costBy === 'is30sec' ? '30 сек' : costBy === 'vocal' ? 'Вокал' : '1 стр'}
        </span>
  </div>
}

export default ItemPrice

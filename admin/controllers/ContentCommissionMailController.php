<?php

namespace admin\controllers;

use admin\models\content\CommissionMailForm;
use Yii;

class ContentCommissionMailController extends SecurityController
{
    public function actionIndex()
    {
        $model = new CommissionMailForm();
        $data = Yii::$app->request->post();

        if (
            \Yii::$app->request->isPost &&
            $model->load($data) &&
            $model->validate()
        ) {
            $model->saveInFile();
        }

        $model->getFromFile();
        $this->view->title = 'Редактирование шаблона письма';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title,
        ];

        return $this->render(
            'index',
            [
                'model' => $model,
            ]
        );
    }
}

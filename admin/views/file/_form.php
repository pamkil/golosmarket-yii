<?
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\components\BaseColumn;
use common\models\settings\File;
use yii\helpers\Url;

/**
 * @var File $model
 */
?>

<? $form = ActiveForm::begin([
    'options' => [
         'data' => [             'key' => $model->id,             'pjax' => true,         ],
        'enctype' => 'multipart/form-data' // important
    ]
]) ?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="actions btn-set">
                    <?= Html::submitButton('<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord ? Yii::t('admin', 'Создать') : Yii::t('admin', 'Сохранить')), ['class' => 'btn btn-success']) ?><? if (!$model->isNewRecord) { ?>
                        <?= Html::a('<i class="fa fa-copy"></i>&nbsp;' . Yii::t('admin', 'Копировать'), ['duplicate', 'id' => $model->id], [
                            'class' => 'btn yellow-crusta',
                            'title' => Yii::t('admin', 'Копировать'),
                            'data-confirm' => Yii::t('admin', 'Вы уверены что хотите скопировать?')
                        ]) ?>
                        <?= Html::a('<i class="fa fa-times"></i>&nbsp;' . Yii::t('admin', 'Удалить'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'title' => Yii::t('admin', 'Удалить'),
                            'data-confirm' => Yii::t('admin', 'Вы уверены что хотите удалить?'),
                            'data-method' => 'post'
                        ]) ?><? } ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_general" data-toggle="tab"><?= Yii::t('admin', 'Общие') ?> </a>
                        </li>
                        <li>
                            <a href="#tab_thumbnails" data-toggle="tab"><?= Yii::t('admin', 'Превьюшки') ?></a>
                        </li>
                        <? if (Yii::$app->user->can('viewHistory')) { ?>
                            <li>
                                <a href="#tab_history" data-toggle="tab"><?= Yii::t('admin', 'История') ?> </a>
                            </li>
                        <? } ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_general">
                            <div class="form-group">
                                <label class="control-label">Файл</label>
                                <?= FileInput::widget(ArrayHelper::merge(Yii::$app->params['fileinput_options'], [
                                    'name' => 'file',
                                    'options' => ['accept' => 'image/*'],
                                    'pluginOptions' => [
                                        'showRemove' => false,
                                        'initialPreview' => !$model->isNewRecord ? [
                                            Html::a(Html::img($model->url), $model->url, ['rel' => 'fancybox'])
                                        ] : [],
                                        'initialPreviewConfig' => [['caption' => $model->name, 'size' => $model->size]],
                                        'overwriteInitial' => true
                                    ]
                                ])) ?>
                            </div>
                            <?= $form->field($model, 'name')->textInput(['readonly' => 'readonly']) ?>
                            <?= $form->field($model, 'path')->textInput(['readonly' => 'readonly']) ?>
                            <?= $form->field($model, 'filename')->textInput(['readonly' => 'readonly']) ?>
                            <?= $form->field($model, 'ext')->textInput(['readonly' => 'readonly']) ?>
                            <?= $form->field($model, 'type')->textInput(['readonly' => 'readonly']) ?>
                            <?= $form->field($model, 'size')->textInput(['readonly' => 'readonly']) ?>
                        </div>
                        <div class="tab-pane" id="tab_thumbnails">
                            <?= MultipleInput::widget([
                                'name' => 'Thumbs',
                                'data' => $model->thumbnails,
                                'columns' => [
                                    [
                                        'name' => 'profile',
                                        'title' => 'Профиль',
                                        'type' => BaseColumn::TYPE_TEXT_INPUT
                                    ]
                                ],
                                'allowEmptyList' => true
                            ]) ?>
                            <br/>
                            <? if ($model->thumbnails) { ?>
                                <?
                                $thumbs = $thumbsCapt = [];

                                foreach ($model->thumbnails as $thumb) {
                                    $thumbs[] = $model->getUrl($thumb->profile);
                                    $thumbsCapt[] = ['caption' => $thumb->profile, 'size' => $model->getSize($thumb->profile)];
                                }
                                ?>
                                <div class="form-group">
                                    <label class="control-label">Превью</label>

                                    <?= FileInput::widget(ArrayHelper::merge(Yii::$app->params['fileinput_options_ajax'], [
                                        'name' => 'File[thumbnail]',
                                        'options' => [
                                            'accept' => 'image/*',
                                            'multiple' => true
                                        ],
                                        'pluginOptions' => [
                                            'uploadUrl' => Url::to(['file/upload-thumb/', 'id' => $model->id]),
                                            'showRemove' => false,
                                            'initialPreview' => !$model->isNewRecord ? $thumbs : [],
                                            'initialPreviewAsData' => true,
                                            'initialCaption' => "Превью фотографий",
                                            'initialPreviewConfig' => $thumbsCapt,
                                            'overwriteInitial' => false
                                        ]
                                    ])) ?>
                                </div>
                            <? } ?>
                        </div>
                        <? if (Yii::$app->user->can('viewHistory')) { ?>
                            <div class="tab-pane" id="tab_history">
                                <?= $this->render('//history/_list', ['model' => $model, 'data' => $model->history]) ?>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? ActiveForm::end() ?>

<?php

namespace api\modules\v1\models\checks\chat;

use api\modules\v1\models\records\file\File;
use yii\base\Model;
use yii\web\UploadedFile;

class ChatForm extends Model
{
    public int $announcer_id;
    public int $client_id;
    public ?int $parent_id = null;
    public bool $is_write_client;
    public $is_send = true;
    /**
     * @var UploadedFile[]
     */
    public $files;
    public $text;

    /** @var File[] */
    private $fileModels;

    public function formName()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['announcer_id', 'client_id', 'is_write_client'],
                'required'
            ],
            [
                ['announcer_id', 'client_id', 'parent_id'],
                'number'
            ],
            [
                ['is_write_client'],
                'boolean'
            ],
            [
                ['text'],
                'string'
            ],
            [
                ['files'],
                'each',
                'rule' => [
                    'file',
                    'maxSize' => 30 * 1024 * 1024,
                ],

//                'extensions' => 'png, jpg'
            ]
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            if ($this->files) {
                $this->fileModels = [];
                foreach ($this->files as $file) {
                    $newFile = File::setFileByUpload($file);
                    $this->fileModels[] = [
                        'file' => $newFile,
                        'watermark' => $this->is_write_client ? null : File::setWatermark($newFile),
                    ];
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function getFileModels(): array
    {
        return $this->fileModels ?? [];
    }
}

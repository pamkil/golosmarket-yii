<?php

declare(strict_types=1);

namespace admin\models\setting;

use yii\base\Model;

class Robot extends Model
{
    public $message = '';

    public function rules()
    {
        return [
            [
                'message',
                'string'
            ],
            [
                'message',
                'required'
            ],
        ];
    }

    public function formName()
    {
        return '';
    }
}

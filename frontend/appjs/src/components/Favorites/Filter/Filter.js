import React, {useEffect, useMemo, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import debounce from "lodash/debounce";
import classNames from "classnames"

import Input from "../../Common/Input/Input";
import SwitchButton from "../../Common/SwitchButton/SwitchButton";
import SwitchButtonAdditional from "../../Common/SwitchButtonAdditional/SwitchButtonAdditional";
import ToggleButton from "../../Common/ToggleButton/ToggleButton";
import {clearFilter} from "../../../data/helpers/Tools";
import {ReactComponent as IconSearch} from '../../../images/search.svg';
import {ReactComponent as IconCheck} from '../../../images/checkmark.svg';

import IconQu from "../../../images/icon_question.png";
import ReactDOM from "react-dom";
import SwitchButtonMulti from "../../Common/SwitchButtonMulti/SwitchButtonMulti";

const Filter = (
  {
    emptyFilter, onChange, isLogin, sendMessage,
    buttonsGender, optAge, buttonsCost, optPresents, quantity,
    optLang, sortFields,
    initialFilter
  }
) => {
  const [filter, setFilterState] = useState(initialFilter);
  const [sort, setSortState] = useState({});
  const [countFiltersActive, setCountFiltersActive] = useState(0);
  const [showFilter, setShowFilterState] = useState(false);
  const [open, setOpen] = useState(false);
  const dropdownRef = useRef();
  const buttonRef = useRef();

  useEffect(() => {
    handleChange();

    // eslint-disable-next-line
  }, [filter, sort]);

  const SortItem = ({title, field, asc, onClick}) => {
    let active = false;

    if (sort.sort === field) {
      active = (asc && sort.dest === 'asc') || (!asc && sort.dest === 'desc');
    }

    return <div className={"sort_type" + (active ? ' active' : '')} onClick={() => onClick(field, asc)}><span>{title}</span>{active ? <IconCheck className={'sort_check'}/> : null}</div>;
  }

  const resetHandler = () => {
    setNewFilter({...emptyFilter});
    setSortState({});
  }

  const setCountFilter = () => {
    const countFiltersActive = Object.keys(filter).filter(item => item !== 'page' && item !== 'profile').length;
    setCountFiltersActive(countFiltersActive);

    return countFiltersActive;
  }
  const setNewFilter = (newFilter) => {
    setFilterState(clearFilter(newFilter));
  }

  const setFilter = (filterName, value) => {
    if (!isLogin && ((filterName === 'favorite' && value === true) || window.location.pathname === '/favorite')) {
      sendMessage();
      return;
    }
    const newFilter = {
      ...filter,
      [filterName]: value
    };
    setNewFilter(newFilter);
  };

  const sortClickHandler = (filterName, asc) => {
    setSortState(
      filterName
        ? {
          sort: filterName,
          dest: asc ? 'asc' : 'desc'
        }
        : {}
    );
    setOpen(false);
  }

  const handleChange = debounce(() => {
    const clearNewFilter = clearFilter(filter);
    const clearNewSort = clearFilter(sort);
    onChange(clearNewFilter, clearNewSort);
    setCountFilter(clearNewFilter);
  }, 500);

  const changeShowFilter = () => {
    setShowFilterState(!showFilter);
  };

  const buttons = useMemo(() => {
    const buttons = [];
    buttonsGender.forEach((item, index) => {
      item.count = quantity[item.value] || 0;
      buttons.push(item)
    });
    return buttons;
  }, [buttonsGender, quantity]);


  const {
    gender, language, text, costBy, presentation, age, photo,
    // demo,
    costAt = '', costTo = '', online
  } = filter;

  const handleClickOutside = (event) => {
    const domNode = ReactDOM.findDOMNode(dropdownRef.current);
    const isInnerClick = domNode && domNode.contains(event.target);
    const ignoreClick = event.target.isEqualNode(buttonRef.current);

    if (!isInnerClick && !ignoreClick && open) {
      setOpen(false);
    }
  };

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true);

    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    }
  });

  let sortLabel = '';
  let el = sortFields.find(f => f.field === sort.sort && (f.asc && sort.dest === 'asc' || !f.asc && sort.dest === 'desc'));

  if (el) {
    sortLabel = el.title;
  }

  return (
    <>
      <div className="line-f">
        <SwitchButton
          buttons={buttons}
          onChange={(val) => setFilter('gender', val)}
          defaultValueActive={gender}
        />

        <div className="item">
          <ToggleButton
            label="В доступе"
            labelNoMobile="сейчас"
            defaultActive={online}
            onChange={(val) => setFilter('online', val)}
          />
        </div>

        <div className="item hider">
          <button className="all-filters" onClick={changeShowFilter}>
            <div className="arrowhead">
              <div className="arrowhead-left"/>
              <div className="arrowhead-right"/>
            </div>
            <span className="arrowhead-add-left">
                <span>Фильтры</span>
                <span className={classNames({'filter-num': countFiltersActive})}>{countFiltersActive}</span>
            </span>
          </button>
        </div>

        <div className="item">
          <span className="filter-link" onClick={resetHandler}>Сбросить</span>
        </div>
      </div>

      <div className={classNames("filter-list", showFilter ? 'active' : 'disable')}>

        <div className="line-filter-list uno">
          <div className="item one">
            <SwitchButtonAdditional
              addClass="sort_type"
              buttons={buttonsCost}
              activeFilter={filter.costBy}
              onChange={(val) => setFilter('costBy', val)}
              value={costBy}
              title="Показать цену за"
              emptyString="Любая"
              info="Ориентируйтесь по параметру за 30 сек. при небольшом объеме текста. Если объем от половины страницы, подойдет параметр за стр. А4"
            />
          </div>

          <div className="item price-f">
            <div className="head prc">
              Интервал цены, ₽
              <span className="tooltip">
                <span className="icon">
                  <img src={IconQu} alt="icon question"/>
                  <span>Цена для ориентира. Точную стоимость назовет исполнитель в чате</span>
                </span>
              </span>
            </div>
            <div className="fullprice">
              от <Input
              value={costAt}
              type="number"
              className="price"
              onChange={(val) => setFilter('costAt', val)}
              placeholder="100"
            />

              до <Input
              value={costTo}
              type="number"
              className="price"
              onChange={(val) => setFilter('costTo', val)}
              placeholder="5000"
            />
            </div>
          </div>

          <div className="item double">
            <SwitchButtonAdditional
              addClass="sort_type"
              buttons={[{title: "Любая", value: ''}, ...optPresents]}
              activeFilter={filter.presentation}
              onChange={(val) => setFilter('presentation', val)}
              value={presentation}
              title="Задача голосу"
              emptyString="Любая"
              info={null}
            />
          </div>

          <div className="item double">
            <SwitchButtonAdditional
              addClass="sort_type"
              buttons={[{title: "Любой", value: ''}, ...optAge]}
              activeFilter={filter.age}
              onChange={(val) => setFilter('age', val)}
              value={age}
              title="Возраст звучания"
              emptyString="Любой"
              info={null}
            />
          </div>

          <div className="item double">
            <SwitchButtonAdditional
              addClass="sort_type"
              buttons={[{title: "Любой", value: ''}, ...optLang]}
              activeFilter={filter.language}
              onChange={(val) => setFilter('language', val)}
              value={language}
              title="Язык"
              emptyString="Любой"
              info={null}
            />
          </div>

          <div className="item one">
            <SwitchButtonMulti
              activeFilter={[(photo ? 'Фото студии' : '')].filter(f => f.length).join(', ') || 'Любой'}
              title="Дополнительно"
              info=""
            >
              <div className="dropdown-check">
                <ToggleButton
                  label="Фото студии"
                  defaultActive={photo}
                  onChange={(val) => setFilter('photo', val)}
                />
              </div>
            </SwitchButtonMulti>
          </div>

          <div className="item tres item-search item-search-text">
            <div className="item-search__wrapper">
              <Input
                value={text}
                onChange={(val) => setFilter('text', val)}
                placeholder="Поиск по имени или тегу"
              />
              <span className={'item-search__icon'}>
              <IconSearch className="icon icon--ui__search"/>
            </span>
            </div>
          </div>

          <div className="item one mobile-only">
            <div className="filter-collapse" onClick={changeShowFilter}>
              <span>Свернуть</span>
              <div className="arrowhead">
                <div className="arrowhead-left"/>
                <div className="arrowhead-right"/>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="sorter">
        {
          (quantity.filters || 0) > 0
            ? (
              <div className={'sorter-params'}>
                <span className="sorter-params__title">Найдено голосов для озвучки:&nbsp;{quantity.filters || 0}</span>
                <div className={'sorter-menu'}>
                  <div ref={buttonRef} className={"sorter-value" + (open ? ' __open' : '')} onClick={() => {
                    setOpen(!open);
                  }}>
                    {sort.hasOwnProperty('sort') ? <span>{sortLabel}</span> : <><span className={'mobile-no'}>По умолчанию</span><span className={'mobile-only'}>Сортировать по умолчанию</span></>}
                    <div className="arrowhead">
                      <div className="arrowhead-left"/>
                      <div className="arrowhead-right"/>
                    </div>
                  </div>
                  {
                    open ? <div ref={dropdownRef} className="sorter-dropdown">
                        {
                          sortFields.map(({title, field, asc}, ind) => <SortItem
                            key={ind}
                            asc={asc}
                            title={title}
                            field={field}
                            onClick={sortClickHandler}
                          />)
                        }
                      </div>
                      : null
                  }
                </div>
              </div>
            )
            : (
              <span className="not-found">По запросу ничего не нашлось. Попробуйте изменить параметры поиска</span>
            )
        }
      </div>
    </>
  );
}

Filter.propTypes = {
  buttonsGender: PropTypes.array,
  optAge: PropTypes.array,
  optStart: PropTypes.array,
  optStop: PropTypes.array,
  optLang: PropTypes.array,
  onChange: PropTypes.func,
  emptyFilter: PropTypes.object
};

Filter.defaultProps = {
  emptyFilter: [],
  onChange: () => {
  }
};

export default Filter;

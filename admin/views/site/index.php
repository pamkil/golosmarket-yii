<?
use yii\db\ActiveQuery;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use common\models\settings\Site;
use common\models\settings\SiteSearch;
use admin\widgets\RestCheckbox;
use admin\components\grid\SocketGrid;

/**
 * @var ActiveDataProvider $dataProvider
 * @var SiteSearch $searchModel
 */
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <? if (Yii::$app->user->can('updateSites')) { ?>
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<?= Url::to(['create']) ?>" class="btn sbold green"><?= Yii::t('admin', 'Добавить') ?>
                                    <i class="fa fa-plus"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            <? } ?>

            <?= SocketGrid::widget([
                'baseClass' => Site::class,
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'active',
                        'format' => 'raw',
                        'value' => function ($data) use ($dataProvider) {
                            if (!Yii::$app->user->can('updateSites')) {
                                return $data->active ? 'Да' : 'Нет';
                            }

                            /* @var ActiveQuery $query */
                            $query = $dataProvider->query;
                            return RestCheckbox::widget([
                                'id' => $data->id,
                                'name' => 'active',
                                'modelClass' => $query->modelClass,
                                'checked' => $data->active
                            ]);
                        },
                        'contentOptions' => ['style' => 'width: 100px; text-align: center;'],
                        'enableSorting' => false,
                        'filter' => false
                    ],
                    'name',
                    'position' => [
                        'attribute' => 'position',
                        'format' => 'raw',
                        'filter' => false
                    ],
                    'updated_at:datetime',
                    [
                        'class' => 'admin\components\grid\DuplicateColumn',
                        'template' => Yii::$app->user->can('updateSites') ? '{update} {duplicate} {delete}' : ''
                    ]
                ]
            ]) ?>
        </div>
    </div>
</div>

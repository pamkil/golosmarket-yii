<?php

namespace common\models\announcer;

use common\models\access\User;
use common\models\Model;

/**
 * Tag
 *
 * @OA\Schema(title="Tag")
 *
 * Class Tag
 * @package common\models\announcer
 *
 * @property int $id
 * @property string $name
 * @property User[] $announcers
 * @property bool $active [tinyint(1)]
 */
class Tag extends Model
{
    public static function tableName()
    {
        return '{{%user_announcer_tags}}';
    }

    public function rules()
    {
        return [
            [
                ['name',],
                'unique'
            ],
            [
                ['name',],
                'string',
                'max' => 150,
                'min' => 2
            ],
            [
                ['active',],
                'boolean'
            ],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);
        unset($behaviors['timestamp']);

        return $behaviors;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'active' => 'Активность',
        ];
    }

    public function getAnnouncers()
    {
        return $this->hasMany(User::class, ['id' => 'announcer_id'])
            ->via('{{%user_announcer_2_tags}}', ['tag_id' => 'id']);
    }
}

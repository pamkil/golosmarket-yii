<?php

use admin\models\announcer\TimberSearch;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Url;

/**
 * @var ActiveDataProvider $dataProvider
 * @var TimberSearch $searchModel
 */
?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <a href="<?= Url::to(['create']) ?>" class="btn bg-olive btn-flat margin">
            Добавить
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                'id',
                'active:boolean',
                'name',
                'code',
                'position',
                'menu_title',
                [
                    'class' => ActionColumn::class,
                    'template' => '{update}  {delete}',
                    'buttonOptions' => ['class' => 'margin']
                ],
            ]
        ]); ?>
    </div>
</div>

<?php

namespace frontend\controllers;

use yii\web\Controller;

class OfflineController extends Controller
{
    public function actionNotice()
    {
        \Yii::$app->layout = 'notice';
        return $this->render('notice');
    }
}
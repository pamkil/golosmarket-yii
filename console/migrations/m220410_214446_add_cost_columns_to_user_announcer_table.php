<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user_announcer}}`.
 */
class m220410_214446_add_cost_columns_to_user_announcer_table extends Migration
{
    private $table = '{{%user_announcer}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->table,
            'cost_vocal',
            $this->integer()->notNull()->defaultValue(0)->after('cost_a4')
        );
        $this->addColumn(
            $this->table,
            'cost_parody',
            $this->integer()->notNull()->defaultValue(0)->after('cost_vocal')
        );
        $this->addColumn(
            $this->table,
            'cost_sound_mount',
            $this->integer()->notNull()->defaultValue(0)->after('cost_parody')
        );
        $this->addColumn(
            $this->table,
            'cost_audio_adv',
            $this->integer()->notNull()->defaultValue(0)->after('cost_sound_mount')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'cost_audio_adv');
        $this->dropColumn($this->table, 'cost_sound_mount');
        $this->dropColumn($this->table, 'cost_parody');
        $this->dropColumn($this->table, 'cost_parody');
    }
}

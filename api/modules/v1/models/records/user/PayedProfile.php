<?php

namespace api\modules\v1\models\records\user;

use common\models\site\Setting;
use DateTimeImmutable;

/**
 * @OA\Schema(title="Информация о заработке и комиссии")
 */
class PayedProfile extends Bill
{
    public static $isAnnouncer = false;
    /**
     * @OA\Property(property="announcer_id", type="integer", description="ИД диктора")
     * @OA\Property(property="sum", type="integer", description="Общая сумма заработка")
     * @OA\Property(property="commission_sum", type="integer", description="Общая сумма заработка")
     * @OA\Property(property="commission_cardnumber", type="string", description="Номер карты для оплаты комиссии")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'announcer_id',
            'sum' => 'sumFormat',
            'commission_sum' => 'commissionSum',
            'commission_cardnumber' => 'commissionCardNumber',
            'date_of_blocking' => 'dateOfBlocking',
        ];
    }

    public function getSumFormat()
    {
        return \Yii::$app->getFormatter()->asCurrency($this->sum, null, [\NumberFormatter::MAX_FRACTION_DIGITS => 0]);
    }

    public function getCommissionSum()
    {
        $percent = Setting::getSetting(Setting::PERCENT);
        $commission = ($this->sum * $percent) / 100;
        return \Yii::$app->getFormatter()->asCurrency($commission, null, [\NumberFormatter::MAX_FRACTION_DIGITS => 0]);
    }

    public function getCommissionCardNumber()
    {
        return Setting::getSetting(Setting::COMMISSION_CARDNUMBER);
    }

    public function getDateOfBlocking()
    {
        $hours = Setting::getSetting(Setting::HOURS_TO_BLOCKED);
        $date = new DateTimeImmutable($this->updated_at);
        $dateInterval = new \DateInterval('PT'.$hours.'H');
        $blockedDate = $date->add($dateInterval);

        return $blockedDate->format('d.m.Y');
    }
}

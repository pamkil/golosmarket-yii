<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user_announcer_2_sound}}`.
 */
class m230327_203538_add_gender_id_column_to_user_announcer_2_sound_table extends Migration
{
    private $table = '{{%user_announcer_2_sound}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->table,
            'gender_id',
            $this->integer()
        );

        $this->dropIndex('IX-user_announcer_2_sound-all', $this->table);
        $this->createIndex(
            'IX-user_announcer_2_sound-all',
            $this->table,
            [
                'announcer_id',
                'sound_id',
                'presentation_id',
                'gender_id',
            ]
        );

        $this->addForeignKey(
            'FK-user_announcer_2_sound-gender_id',
            $this->table,
            'gender_id',
            '{{%user_announcer_genders}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK-user_announcer_2_sound-gender_id', $this->table);
        $this->dropIndex('IX-user_announcer_2_sound-all', $this->table);
        $this->createIndex(
            'IX-user_announcer_2_sound-all',
            $this->table,
            [
                'announcer_id',
                'sound_id',
                'presentation_id'
            ]
        );
        $this->dropColumn($this->table, 'gender_id');
    }
}

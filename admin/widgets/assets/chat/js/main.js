function setCaretAtEnd(elem) {
  var elem = $(elem).get(0);
  var elemLen = elem.value.length;
  // For IE Only
  if (document.selection) {
    // Set focus
    elem.focus();
    // Use IE Ranges
    var oSel = document.selection.createRange();
    // Reset position to 0 & then set at end
    oSel.moveStart('character', -elemLen);
    oSel.moveStart('character', elemLen);
    oSel.moveEnd('character', 0);
    oSel.select();
  } else if (elem.selectionStart || elem.selectionStart === '0') {
    // Firefox/Chrome
    elem.selectionStart = elemLen;
    elem.selectionEnd = elemLen;
    elem.focus();
  } // if
}

var delay = (function () {
  var timer = 0;
  return function (callback, ms) {
    clearTimeout(timer);
    timer = setTimeout(callback, ms);
  };
})();

function notifyMe(body, icon, title, url, haveSound) {
  var options = {
    body: body,
    icon: icon
  };
  var notification;
  if (!("Notification" in window)) {
    console.log("This browser does not support desktop notification");
  }
  // Проверка разрешения на отправку уведомлений
  else if (Notification.permission === "granted") {
    // Если разрешено, то создаем уведомление
    notification = new Notification(title, options);
  }

  // В противном случае, запрашиваем разрешение
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      if (permission === "granted") {
        notification = new Notification(title, options);
      }
    });
  }
  if (notification) {
    if (haveSound === true) {
      var popsound = new Audio('/audio/new-message-2.ogg');
      popsound.load();
      popsound.play();
    }
    notification.onclick = function (event) {
      event.preventDefault(); // prevent the browser from focusing the Notification's tab
      if (url) {
        window.open(url, '_blank');
      }
    }
  }
}

function getAllNotify(clear, sendMeNow) {
  var isClear = clear !== undefined && clear === true;
  sendMeNow = sendMeNow !== undefined ? sendMeNow : false;
  $.ajax({
    url: '/notification/get-all-notify?clear=' + (isClear ? '1' : '0')
  }).done(function (data) {
    var notifyWrapper = $('#notify-wrapper'),
      wrapper = $('#wrapper');

    if (wrapper.hasClass('visible') && (wrapper.find('.inner-wrapper').length <= 0 || wrapper.find('.inner-wrapper').hasClass('notify'))) {
      $.template('listTpl', $('#listTpl'));
      $.tmpl('listTpl', {title: 'Уведомления', type: 'notify', items: data.items}).appendTo('#wrapper');
    }

    notifyWrapper.html('');
    $.template('notifyTpl', $('#notifyTpl'));
    if (isClear) {
      $.tmpl('notifyTpl', {count: 0}).appendTo('#notify-wrapper');
    } else {
      $.tmpl('notifyTpl', {count: data.count}).appendTo('#notify-wrapper');
    }
    var msgWrapper = wrapper.find('ul');
    var height = msgWrapper.height();
    msgWrapper.slimScroll({start: 'top', height: (height > 150 ? height + 'px' : '300px')});
    if (sendMeNow) {
      var text = data.items[0] && data.items[0].text ? data.items[0].text : '';
      var url = data.items[0] && data.items[0].url ? data.items[0].url : '';
      if (text) {
        notifyMe(text, 'https://megaflowers.ru/img/ico/android-icon-192x192.png', 'Уведомление CRM', url, true);
      }
    }
    // todo set for socket
    $(document).trigger('notify');
  });
}

/**
 * Валидация зарегестрированного номера телефона активация отображения статуса линии
 */
function isRegisterPhone() {
  var call = $('.im-call-btn');
  if (window.coolPhone && window.coolPhone.isRegistered()) {
    if (call && call.hasClass('no-register')) {
      call.removeClass('no-register');
    }
  } else {
    if (call && call.hasClass('no-register') === false) {
      call.addClass('no-register');
    }
  }
}

/**
 * Валидация включенного DND у оператора
 */
function toogleDND() {
  var pause = $('.js-toogle-plug');
  if (isDND()) {
    if (pause && pause.hasClass('font-grey-steel')) {
      pause.addClass('font-green');
      pause.removeClass('font-grey-steel');
      pause.attr('title', 'Я онлайн - принимаю звонки');
    }
    Cookies('DND', 0);
  } else {
    if (pause && pause.hasClass('font-grey-steel') === false) {
      pause.addClass('font-grey-steel');
      pause.removeClass('font-green');
      pause.attr('title', 'Не беспокоить (DND)');
    }
    Cookies('DND', 1);
  }
}

/**
 * Валидация активности DND
 * @returns {boolean}
 */
function isDND() {
  return Cookies('DND') === '1';
}

function getAllChats(chatId) {
  var query = $.ajax({
    url: '/notification/get-all-chats' + (chatId !== undefined ? ('?chatId=' + chatId) : '')
  });

  query.done(function (data) {
    var wrapper = $('#chat-wrapper');
    wrapper.html('');

    $.template('chatListTpl', $('#chatListTpl'));
    $.tmpl('chatListTpl', data).appendTo('#chat-wrapper');

    // check register phone
    isRegisterPhone();
  });

  return query;
}

/**
 * Проверка открыт ли чат
 * @param chatId
 * @returns {boolean}
 */
function isOpenChat(chatId) {
  var wrapper = $('#wrapper.visible'),
    innerWrapper = wrapper.find('.inner-wrapper.chat');
  return !!(innerWrapper.length && parseInt(innerWrapper.data('chat')) === parseInt(chatId));
}

/**
 * Получение списка всех пользователей по ajax
 * @param search Строка поиска
 * @returns {*|jQuery|{getAllResponseHeaders, abort, setRequestHeader, readyState, getResponseHeader, overrideMimeType, statusCode}|*}
 */
function getAllUsers(search) {
  var query = $.ajax({
    url: '/notification/get-all-users?call=1' + (search !== undefined ? '&q=' + encodeURIComponent(search) : '')
  });

  query.done(function (data) {
    var wrapper = $('#wrapper'),
      listTpl = $('#listTpl'),
      userSearch = $('#userSearchTpl');

    wrapper.html('');

    if (listTpl) {
      $.template('listTpl', listTpl);
      $.tmpl('listTpl', {title: 'Пользователи', type: 'user', items: data.items, query: search}).appendTo('#wrapper');
    }

    if (userSearch) {
      $.template('userSearchTpl', userSearch);
      $('.bx-messenger-chatlist-category').html('');
      for (var i = 0; i < data.items.length; i++) {
        $.tmpl('userSearchTpl', data.items[i]).appendTo('.bx-messenger-chatlist-category');
      }
    }

    // hack to set the focus on last char
    wrapper.find('input').val(wrapper.find('input').val()).focus();
    setCaretAtEnd(wrapper.find('input'));
  });

  return query;
}

/**
 * Получение всех сообщений пользователя
 * @param chatId
 * @returns {*|jQuery|{getAllResponseHeaders, abort, setRequestHeader, readyState, getResponseHeader, overrideMimeType, statusCode}|*}
 */
function getAllMessages(chatId) {
  var query = $.ajax({
    url: '/notification/get-all-messages?chatId=' + chatId
  });

  query.done(function (data) {
    var wrapper = $('#wrapper');
    wrapper.html('');

    $.template('listTpl', $('#listTpl'));
    $.tmpl('listTpl', {
      title: data.name,
      chatId: chatId,
      user_id: data.user_id,
      type: data.type,
      items: data.items,
      types: data.types,
      phone: data.phone ? data.phone : '',
      isOnline: data.isOnline
    }).appendTo('#wrapper');

    var msgWrapper = wrapper.find('ul');
    var firstElem = msgWrapper.get(0);
    if (firstElem) {
      msgWrapper.animate({
        scrollTop: firstElem.scrollHeight
      }, 100);
    }
    var height = msgWrapper.height();
    msgWrapper.slimScroll({start: 'bottom', height: (height > 150 ? height + 'px' : '300px')});

    wrapper.find('input').focus();
  });

  return query;
}

function acceptMessages(chatId) {
  var query = $.ajax({
    url: '/notification/accept-messages?chatId=' + chatId
  });

  query.done(function () {
    getAllChats();
  });

  return query;
}

function openNotify() {
  var wrapper = $('#wrapper');

  if (wrapper.hasClass('visible') && wrapper.find('.inner-wrapper').hasClass('notify')) {
    closeWrapper();
    return;
  }

  wrapper.html('');
  wrapper.addClass('visible');

  getAllNotify(true);
}

function openList() {
  var wrapper = $('#wrapper');
  if (wrapper.hasClass('visible') && wrapper.find('.inner-wrapper').hasClass('user')) {
    closeWrapper();
    return;
  }

  wrapper.html('');
  wrapper.addClass('visible');

  getAllUsers();
}

function openChat(chatId) {
  var wrapper = $('#wrapper');

  if (chatId === undefined) {
    return;
  }

  if (wrapper.hasClass('visible') && wrapper.find('.inner-wrapper').hasClass('chat')) {
    closeChat();
    return;
  }

  wrapper.html('');
  wrapper.addClass('visible');
  wrapper.data('chat', chatId);

  getAllMessages(chatId);
  acceptMessages(chatId);
  Cookies('openChatId', chatId);
}

function shutdownChat(chatId) {
  closeChat();

  $.ajax({
    type: 'POST',
    url: '/notification/close-chat?chatId=' + chatId
  });
}

function closeChat() {
  closeWrapper();
  Cookies.remove('openChatId', {path: '/'});
}

function closeWrapper() {
  var wrapper = $('#wrapper');
  wrapper.removeClass('visible');
}

function sendMessage() {
  var currentChatId = Cookies('openChatId'),
    box = $('.js-message-box'),
    text = box.val(),
    type = $('*[name="chat-type"]').val();

  if (text.trim() !== '') {
    box.val('');

    $.ajax({
      url: '/notification/send-message?chatId=' + currentChatId + '&text=' + text + '&type=' + type
    }).done(function (data) {
      var wrapper = $('#wrapper'),
        msgWrapper = wrapper.find('ul');

      if (!msgWrapper.length) {
        wrapper.find('.empty').after('<ul></ul>');
        wrapper.find('.empty').remove();
      }

      $.template('chatItemTpl', $('#chatItemTpl'));
      $.tmpl('chatItemTpl', data).appendTo('#wrapper ul');
      msgWrapper = wrapper.find('ul');
      msgWrapper.animate({
        scrollTop: msgWrapper.get(0).scrollHeight
      }, 100);
    });
  }
}

/*------------------------------------call----------------------------------------------------*/

var dialCallAudio = new Audio('/audio/dialtone.ogg');
dialCallAudio.loop = true;
var endCallAudio = new Audio('/audio/video-error.ogg');

/**
 * Закрытие звонка
 */
function closeCalc() {
  var calc = $('#calc');

  calc.removeClass('visible');
  calc.find('input').val('');
}

function closeCall() {
  var callWrapper = $('#call');
  callWrapper.removeClass('visible');
  callWrapper.html('');
}

function toogleHiddenMainCall() {
  var call = $('#im-phone-call-view');
  var mini = $('.im-phone-call-panel-mini');
  if (call.is(":visible")) {
    call.hide();
    mini.show();
  } else {
    call.show();
    mini.hide();
  }
}

function updateTimer(el) {
  if (el && window.session) {
    setInterval(function () {
      var timer = getTimeCall();
      el.text(timer);
    }, 1000);
  }
}

// sip
function updateUI() {
  updateTimer($('.im-phone-call-status-timer-item').find('span').find('span'));
  $('.step-1').hide();
  $('.step-2').show();
  return true;
}

function stopCall() {
  if (window.session && (window.session.isInProgress() || window.session.isEstablished())) {
    window.session.terminate();
  }
  if (window.session) {
    window.session = null;
  }
  closeCall();
  if (window.incomingCallAudio instanceof Audio) window.incomingCallAudio.pause();
  register();
  $(window).unbind('beforeunload');
}

function addAudioStream() {
  if (window.session) {
    var remoteStream = window.session.connection.getRemoteStreams()[0];
    var remoteAudio = new window.Audio();
    remoteAudio.autoplay = true;
    remoteAudio.srcObject = remoteStream;
  }

  if (window.session2) {
    var remoteStream2 = window.session2.connection.getRemoteStreams()[0];
    var remoteAudio2 = new window.Audio();
    remoteAudio2.autoplay = true;
    remoteAudio2.srcObject = remoteStream2;
  }
}

function getTimeCall() {
  var diff;
  if (window.session && window.session.start_time instanceof Date) {
    diff = new Date(new Date() - window.session.start_time);
  } else {
    diff = new Date();
  }

  return diff.toISOString().split("T")[1].split('.')[0];
}

/**
 * Холд вкл/выкл
 * @param event
 */
function toogleHold(event) {
  if (window.session && window.session.isOnHold().local === true) {
    window.session.unhold();
    setTimeout(addAudioStream, 300);
    $(event.target).removeClass('active');
  } else if (window.session && window.session.isOnHold().local === false) {
    window.session.hold();
    $(event.target).addClass('active');
  }
}

function toogleMute(event) {
  if (window.session && window.session.isMuted().audio === true) {
    window.session.unmute();
    $(event.target).removeClass('active');
  } else if (window.session && window.session.isMuted().audio === false) {
    window.session.mute();
    $(event.target).addClass('active');
  }
}

// Перенаправление звонка на другой номер
function toogleRefer() {
  var popup = $('#bx-messenger-popup-transfer');
  if (popup.is(':visible')) {
    popup.hide();
  } else {
    popup.show();
  }
}

function hideSessionButtons() {
  var refer = $('.refer-buttons');
  if (refer.is(':visible')) {
    refer.hide();
  }
}

function returnSessionButtons() {
  var left = $('.im-phone-call-buttons-container-left');
  if (!left.is(':visible')) {
    left.show();
    $('.im-phone-call-buttons-container-right').show();
    $('.refer-buttons').hide();
    $('.refer-buttons-cancel').hide();
  }
}

function openSessionButtons() {
  var left = $('.im-phone-call-buttons-container-left');
  if (left.is(':visible')) {
    left.hide();
    $('.im-phone-call-buttons-container-right').hide();
    $('.refer-buttons').show();
  }
}

function toogleSessionButtons() {
  var left = $('.im-phone-call-buttons-container-left');
  var right = $('.im-phone-call-buttons-container-right');
  var refer = $('.refer-buttons');
  if (left.is(':visible')) {
    left.hide();
    right.hide();
    refer.show();
  } else {
    left.show();
    right.show();
    refer.hide();
  }
}

function refer(event, number) {
  var phone = $(event.currentTarget).data('phone');

  if (number !== undefined && number !== '') {
    phone = number
  }
  if (window.session && phone) {
    var sessionId = 2;//todo
    startCall(sessionId);
    if (this['coolPhone' + sessionId] !== undefined) {
      toogleHold(event);
      window['coolPhone' + sessionId].call('sip:' + phone, callOptions);
      var popup = $('#bx-messenger-popup-transfer');
      popup.hide();
      // Показывем кнопки
      toogleSessionButtons();
      // Статус дозвона
    }
  }
}

// отключиться от активного звонка
function cancelReferCall(sessionId) {
  if (window.session !== undefined && sessionId && window['session' + sessionId] !== undefined) {
    var options = {
      'eventHandlers': eventHandlersRefer,
      'replaces': window['session' + sessionId]
    };
    var phone = window['session' + sessionId].remote_identity.uri.user;
    if (window.window['session' + sessionId].status == 2) {
      return referCallback(2);
    }
    window.session.refer('sip:' + phone, options);
  }
}

// Прямое перенаправления без участия в звонке
function referCall(event, number) {
  var phone = $(event.currentTarget).data('phone');

  if (number !== undefined && number !== '') {
    phone = number
  }
  if (window.session !== undefined && phone) {
    var options = {
      'eventHandlers': eventHandlersRefer
    };
    window.session.refer('sip:' + phone, options);
  }
}

//todo Не работает
function referNow(sessionId) {
  if (window.session !== undefined && sessionId && window['session' + sessionId] !== undefined) {
    var options = {
      'eventHandlers': eventHandlersRefer,
      'replaces': window.session2
    };
    var phone = window['session' + sessionId].remote_identity.uri.user;
    window.session.refer('sip:' + phone, options);
    window.session.terminate();
  }
}

function referCallback(sessionId) {
  if (window.session !== undefined && sessionId && window['session' + sessionId] !== undefined) {
    window.session.unhold();
    setTimeout(addAudioStream, 300);
    window['session' + sessionId].terminate();
    window['coolPhone' + sessionId] = null;
    $('.im-phone-call-status-description-item').text('Соединение установлено');
    $('.im-phone-call-title-text').text('Звонок на ' + window.session.remote_identity.uri.user);
  }
  returnSessionButtons();
  $('.refer-buttons-cancel').hide();
}

function referCancel() {
  var popup = $('#bx-messenger-popup-transfer');
  popup.hide();
}

function parsePhone(string) {
  return string.match(/{?\+{0,1}\d+/g);
}

// handlers
var session;
var eventHandlers = {
  'progress': function () {
    if (window.session && window.session2) {
      //todo подумать с экранами что делать тут состояние что соединение есть
      $('.refer-buttons-cancel').show();
      hideSessionButtons();
      $('.im-phone-call-status-description-item').text('Перенаправление звонка на номер - ' + window.session2.remote_identity.uri.user + ': ожидание ответа...');
      $('.im-phone-call-title-text').text('(Переадресация на номер - ' + window.session2.remote_identity.uri.user + ')');
    }
    dialCallAudio.play();
  },
  'failed': function (e) {
    $('.bx-messenger-call-overlay-progress').hide();
    $('.bx-messenger-call-overlay-progress-offline').show();
    $('.im-phone-calling-process-status').text('Абонент не доступен');
    if (e.cause === JsSIP.C.causes.BUSY) {
      var busy = new Audio('/audio/new-message-2.ogg');
      busy.play();
      // send message for local users
      if (window.callPhone && callPhone.length === 4) {
        window.coolPhone.sendMessage('sip:' + window.callPhone, 'Пожалуйста перезвоните, не смог дозвонится');
      }
    }
    $(window).unbind('beforeunload');
    dialCallAudio.pause();
  },
  'confirmed': function () {
    $('.im-phone-call-status-description-item').text('Соединение установлено');
    $('.im-phone-call-title-text').text('Звонок на ' + window.session.remote_identity.uri.user);
    dialCallAudio.pause();
  },
  'ended': function () {
    if (!window.session2) {
      closeCalc();
      closeCall();
      dialCallAudio.pause();
      dialCallAudio.pause();
      $(window).unbind('beforeunload');
    }
  },
  'requestFailed': function () {
    console.log('requestFailed refer');
  },
  'trying': function () {
    console.log('trying refer');
  },
};

var callOptions = {
  'eventHandlers': eventHandlers,
  'mediaConstraints': {'audio': true, 'video': false},
  'step': 1
};

//todo
var eventHandlersRefer = {
  'requestSucceeded': function () {
    stopCall();
  },
  'requestFailed': function () {
    console.log('requestFailed refer');
  },
  'trying': function () {
    console.log('trying refer');
  },
  'progress': function () {
    console.log('progress refer');
  },
  'accepted': function () {
    console.log('accepted refer');
  },
  'failed': function () {
    console.log('failed refer');
  },
};

var callOptionsRefer = {
  'eventHandlers': eventHandlersRefer,
  'mediaConstraints': {'audio': true, 'video': false},
  'step': 1
};

function startCall(id) {
  // validate auto answer
  var answerState = Cookies('autoAnswer'),
    answerToogle = $('.js-toogle-autoanswer');
  if (answerState === '1') {
    answerToogle.removeClass('font-grey-steel');
    answerToogle.addClass('font-red');
    answerToogle.attr('title', 'Включен автоответ');
  }
  // validate DND
  var pause = $('.js-toogle-plug');
  if (!isDND()) {
    if (pause && pause.hasClass('font-grey-steel')) {
      pause.addClass('font-green');
      pause.removeClass('font-grey-steel');
      pause.attr('title', 'Я онлайн - принимаю звонки');
    }
  }
  var socket = new JsSIP.WebSocketInterface('wss://sip.megaflowers.ru:8089/ws');
  var sip = crmUser && !$.isEmptyObject(crmUser.lines) ? crmUser.lines[0].number : '';
  var configuration = {
    sockets: [socket],
    uri: 'sip:' + sip + '@sip.megaflowers.ru',
    session_timers_refresh_method: 'invite',
    password: crmUser && !$.isEmptyObject(crmUser.lines) ? crmUser.lines[0].password : '',
    display_name: crmUser && crmUser.name ? crmUser.name : ''
  };

  if (!sip) {
    return;
  }
  var session;
  var coolPhone = new JsSIP.UA(configuration);

  coolPhone.on('connecting', function () {
    //todo
  });

  coolPhone.on('connected', function () {
    //todo
  });

  coolPhone.on('disconnected', function () {
    //todo
  });

  coolPhone.on('registered', function () {
    isRegisterPhone();
  });

  coolPhone.on('unregistered', function () {
    isRegisterPhone();
  });

  coolPhone.on('newMessage', function (e) {
    if (e.message.direction === 'incoming') {
      var name = e.message.remote_identity.display_name ? e.message.remote_identity.display_name : '';
      notifyMe(e.request.body, 'https://megaflowers.ru/img/ico/android-icon-192x192.png', 'Новое сообщение от ' + name , null, true);
    }
  });

  coolPhone.on('registrationFailed', function () {
    configuration.uri = null;
    configuration.password = null;
    // disable call button
    isRegisterPhone();
    updateUI();
  });

  coolPhone.on('registrationExpiring', function () {
    this.register();
    isRegisterPhone();
  });

  coolPhone.on('referSubscriber', function () {
    console.error('referSubscriber');
  });

  coolPhone.on('newRTCSession', function (ev) {
    $(window).bind("beforeunload", function () {
        return confirm("Вы действительно хотите закрыть страницу? Сейчас у вас есть активный звонок.")
      }
    );
    var newSession = ev.session;

    if (window.session && newSession.direction === 'incoming' && session.remote_identity._uri.user !== newSession.remote_identity.uri.user) {
      newSession.terminate();
      return;
    }

    session = newSession;
    var completeSession = function (event) {

      if (window.session && window.session2) {
        window.session2 = null;
        // до абонента не дозвонились
        var textContainer = $('.im-phone-call-status-description-item'),
          text = 'Соединение установлено';
        textContainer.text('Абонент недоступен или вне сети. Возращаем к разговору через 3 сек.');

        //todo
        returnSessionButtons();
        endCallAudio.play();
        if (window.session) {
          window.session.unhold();
          setTimeout(function () {
            addAudioStream();
          }, 1000);
        }
        setTimeout(function () {
          textContainer.text(text);
        }, 2000);

        window.coolPhone2 = null;
      } else {
        var timeout = 1000,
          userMediaFail = false;
        if (event && event.cause === "User Denied Media Access") {
          var textContainer = $('.im-phone-calling-process-status');
          textContainer.text('Подключите гарнитуру или предоставьте доступ к микрофону в браузере');
          timeout = 3000;
          userMediaFail = true;
        }
        endCallAudio.play();
        if(!userMediaFail) {
          stopCall();
        }

        setTimeout(function () {
          if (userMediaFail) {
            stopCall();
          }
          closeCall();
          closeIncomingCall();
          updateUI();
        }, timeout);
      }
    };

    var acceptSession = function () {
      // todo При разговоре по второй линии.
      addAudioStream();
      updateUI();
    };
    session.on('ended', completeSession);
    session.on('failed', completeSession);
    session.on('accepted', acceptSession);

    // не работает
    session.on('refer', function () {
      alert('refered call');
    });
    // не работает 
    session.on('replace', function () {
      alert('replace call');
    });

    session.on('unhold', addAudioStream);
    session.on('confirmed', function () {
      var localStream = window.session.connection.getLocalStreams()[0];
      var dtmfSender = window.session.connection.createDTMFSender(localStream.getAudioTracks()[0]);
      addAudioStream();
      window.session.sendDTMF = function (tone) {
        dtmfSender.insertDTMF(tone);
      };
      if (window.incomingCallAudio instanceof Audio) window.incomingCallAudio.pause();
      updateUI();
    });
    if (session.direction === 'incoming') {
      if (isDND()) {
        session.terminate();
        return;
      }
      var rPhone = parsePhone(session.remote_identity.display_name);
      rPhone = rPhone ? rPhone : session.remote_identity.uri.user;
      getUserInfo(rPhone, 1);
      var options = {
        'phone': rPhone,
        'step': 1,
        'user_name': session.remote_identity.display_name ? session.remote_identity.display_name : rPhone,
        'user_id': 1,
        'user_photo': ''
      };
      openIncomingCall(options);
      //todo change sounds
      var incomingCallAudio = new Audio('/audio/video-ringtone.ogg');
      incomingCallAudio.load();
      incomingCallAudio.loop = true;
      window.incomingCallAudio = incomingCallAudio;
      if (incomingCallAudio) {
        incomingCallAudio.play();
      }
      setTimeout(function () {
        var userName = $('.js-user-name').first().text();
        notifyMe("Входящий звонок от " + (userName.length > 0 ? userName : options.user_name), 'https://megaflowers.ru/img/ico/android-icon-192x192.png', 'Входящий звонок', null, false);
        let autoAnswer = Cookies('autoAnswer');
        if (autoAnswer && autoAnswer === '1') {
          answer();
        }
      }, 200);
    }

    updateUI();
    if (id === undefined) {
      window['session'] = session;
    } else {
      window['session' + id] = session;
    }
  });


  coolPhone.start();
  if (id === undefined) {
    window.coolPhone = coolPhone;
  } else {
    window['coolPhone' + id] = coolPhone;
  }
}

function openCalc() {
  var calc = $('#calc');

  if (calc.hasClass('visible')) {
    closeCalc();
    return;
  }

  closeWrapper();

  calc.html('');
  $.template('calcTpl', $('#calcTpl'));
  $.tmpl('calcTpl').appendTo('#calc');

  calc.addClass('visible');
}

function openCall(callOptions) {
  var call = $('#call');
  if (call.hasClass('visible')) {
    closeCall();
    return;
  }

  closeWrapper();

  call.html('');
  $.template('callTpl', $('#callTpl'));
  $.tmpl('callTpl', callOptions).appendTo('#call');
  if (session && callOptions.step === 2) {
    updateTimer($('.im-phone-call-status-timer-item').find('span').find('span'));
  }
  updateTimer($('.im-phone-call-panel-mini-time'));

  call.addClass('visible');
  closeCalc();
}

function closeIncomingCall() {
  var call = $('#callIncoming');
  call.removeClass('visible');
}

function openIncomingCall(callOptions) {
  var callIncoming = $('#callIncoming');
  if (callIncoming.hasClass('visible')) {
    closeIncomingCall();
    return;
  }

  closeWrapper();

  callIncoming.html('');
  $.template('callIncomingTpl', $('#callIncomingTpl'));
  $.tmpl('callIncomingTpl', callOptions).appendTo('#callIncoming');

  callIncoming.addClass('visible');
  closeCalc();
}

function getUserOnline() {
  getAllUsers();
}

/**
 * Поиск пользователя по номеру телефона
 * @param type = 1 - incoming userInfo, 2 - outcoming user info
 * @param search
 */
function getUserInfo(search, type) {
  if (search) {
    $.ajax({
      url: '/notification/get-all-users' + (search ? '?call=1&q=' + encodeURIComponent(search) : ''),
      success: function (data) {
        if (data.items.length > 0) {
          // incoming
          var userName = $('.js-user-name'),
            userPhoto = $('.js-user-img'),
            userHref = $('.js-user-href');

          var user = data.items[0];
          if (user.name !== '') {
            userName.text(user.name);
          }
          if (userHref.length > 0 && user.id) {
            userHref.each(function () {
              $(this).attr('href', '/user/' + user.id);
            })
          }
          if (user.photo !== '' && userPhoto.length > 0) {
            userPhoto.each(function () {
              var imgTag = $(this).find('img');
              if (imgTag.length > 0) {
                imgTag.each(function () {
                  $(this).attr('src', user.photo);
                });
              } else {
                var img = $('<img />', {
                  class: type === 2 ? 'im-phone-calling-progress-customer' : '',
                  src: user.photo,
                  alt: user.name,
                  title: user.name
                });
                if (type === 2) {
                  userPhoto.find('div').remove();
                }
                img.appendTo(userPhoto);
              }
            });
          }
        }
      }
    });
  }
}

/**
 *
 * @returns {*}
 */
function getManagerInfo() {
  var userModel = $('.dropdown-user');
  if (userModel) {
    return {
      'id': userModel.data('id'),
      'name': userModel.find('.username').text().trim(),
      'phone': userModel.data('phone'),
      'photo': userModel.find('img').attr('src')
    }
  } else {
    return {
      'id': null,
      'name': '',
      'phone': '',
      'photo': ''
    }
  }
}

function call(phone) {
  window.callPhone = null;
  if (phone && window.coolPhone !== undefined) {
    window.callPhone = phone;
    window.coolPhone.call('sip:' + phone, callOptions);
    getUserInfo(phone, 2); // out call
    var manager = getManagerInfo();
    openCall({
      'phone': phone,
      'step': 1,
      'user_name': phone,
      'type': 'Исходяший вызов',
      'manager_id': manager.id,
      'manager_name': manager.name,
      'manager_phone': manager.phone,
      'manager_photo': manager.photo,
      'manager_href': '/user/' + manager.id,
      'items': []
    });
  }
}

/**
 * Метод для поднятия трубки входящего звонка
 */
function answer() {
  if (window.session) {
    window.session.answer();
    var rPhone = parsePhone(session.remote_identity.display_name);
    rPhone = rPhone ? rPhone : session.remote_identity.uri.user;
    var manager = getManagerInfo();
    getUserInfo(rPhone, 1); // incoming call
    getUserOnline();
    var options = {
      'phone': rPhone,
      'step': 2,
      'user_name': window.session.remote_identity.display_name ? window.session.remote_identity.display_name : rPhone,
      'type': 'Входящий звонок',
      'manager_id': manager.id,
      'manager_name': manager.name,
      'manager_phone': manager.phone,
      'manager_photo': manager.photo,
      'manager_href': '/user/' + manager.id,
      'items': []
    };
    openCall(options);
  }
  closeIncomingCall();
}

/**
 * Разрегистрация аккаунта
 */
function unregister() {
  if (window.coolPhone && window.coolPhone.isRegistered()) {
    window.coolPhone.unregister();
  }
}

/**
 * Регистрация аккаунта
 */
function register() {
  if (window.coolPhone) window.coolPhone.register();
}

/**
 * Метод для включения и отключения функции автоответа
 */
function toogleAutoAnswer() {
  var answer = $('.js-toogle-autoanswer');
  var autoAnswer = Cookies('autoAnswer');
  if (autoAnswer === undefined) {
    autoAnswer = 0;
  }
  if (autoAnswer === '1') {
    Cookies('autoAnswer', 0);
    if (answer.hasClass('font-red')) {
      answer.removeClass('font-red');
      answer.addClass('font-grey-steel');
      answer.attr('title', 'Автоответ не активирован');
    }
  } else {
    Cookies('autoAnswer', 1);
    if (answer.hasClass('font-grey-steel')) {
      answer.removeClass('font-grey-steel');
      answer.addClass('font-red');
      answer.attr('title', 'Включен автоответ');
    }
  }
}

// Часы
function updateClock() {
  var date = new Date(),
    hours = date.getHours(),
    minutes = date.getMinutes();

  if (hours < 10)
    hours = '0' + hours;
  if (minutes < 10)
    minutes = '0' + minutes;

  var str = hours + ":" + minutes,
    currentTime = $('.js-time');

  if (currentTime.text() !== str) {
    currentTime.text(str);
  }
}

$(document).ready(function () {
  setInterval(updateClock, 1000);
  getAllChats();
  getAllNotify(false);
  startCall();

  var currentChatId = Cookies('openChatId');
  if (currentChatId !== undefined) {
    openChat(currentChatId);
  }

  $(document).on('click', '.js-open-notify', function () {
    openNotify();
  });

  $(document).on('click', '.js-close-notify', function () {
    closeWrapper();
  });

  $(document).on('click', '.js-open-list', function () {
    openList();
  });

  $(document).on('click', '.js-close-list', function () {
    closeWrapper();
  });

  $(document).on('click', '.tel', function (event) {
    if (window.coolPhone && window.coolPhone.isRegistered()) {
      event.preventDefault();
    } else {
      return true;
    }

    var href = $(event.target).attr('href');

    if (!href) {
      href = $(event.target).parent().attr('href');
    }

    if (href !== undefined && href.indexOf('tel:') !== -1) {
      call(href.split(':')[1]);
      return true;
    } else {
      return false;
    }
  });

  $(document).on('click', '.js-cancel-call', function () {
    stopCall();
  });

  $(document).on('click', '.im-phone-call-btn-hold', function (e) {
    toogleHold(e);
  });

  $(document).on('click', '.im-phone-call-btn-mute', function (e) {
    toogleMute(e);
  });

  $(document).on('click', '.js-hidden-call', function () {
    toogleHiddenMainCall();
  });

  $(document).on('click', '.im-phone-call-btn-transfer', function (e) {
    toogleRefer(e);
  });

  $(document).on('click', '.js-hide-reffer', function () {
    referCancel();
  });

  // Переадресация звонка на конкретного человека из поиска
  $(document).on('click', '.bx-messenger-cl-item', function (e) {
    refer(e);
  });
  // Переадресация звонка на номер в импате
  $(document).on('click', '.js-refer', function (e) {
    refer(e, $('.js-phone-input').val());
  });
  // Перенаправление с отключением от линии и возвратом
  $(document).on('click', '.js-call-refer', function (e) {
    referCall(e, $('.js-phone-input').val());
  });

  // Переадресовать принудительно
  $(document).on('click', '.js-refer-now', function () {
    // todo lines!
    referNow(2);
  });

  // Вернуться на линию
  $(document).on('click', '.js-call-back', function () {
    // todo lines!
    referCallback(2);
  });

  // Завершить переадресацию и отключиться
  $(document).on('click', '.js-refer-cancel', function () {
    // todo lines!
    cancelReferCall(2);
  });

  // При выходе разрегистрируем пользователя в sip
  $(document).on('click', '.js-logout', function (e) {
    e.preventDefault();
    unregister();
    return true;
  });

  // Включение/выкл автоответа
  $(document).on('click', '.js-toogle-autoanswer', function () {
    toogleAutoAnswer();
  });

  // Включение/выкл онлайн (включение DND)
  $(document).on('click', '.js-toogle-plug', function () {
    if (window.coolPhone && window.coolPhone.isRegistered()) {
      toogleDND();
    }
  });


  var tmpQuery = null;
  $(document).on('keyup', '.js-search-user', function (e) {
    if (e.which <= 90 && e.which >= 48 || e.which >= 96 && e.which <= 105) {
      e.preventDefault();
      var that = $(this);
      if (tmpQuery !== null) {
        tmpQuery.abort();
      }
      delay(function () {
        tmpQuery = getAllUsers(that.val());
      }, 300);
    }
  });

  $(document).on('click', '.js-open-chat', function () {
    var that = $(this),
      wrapper = $('#wrapper'),
      chatId = that.data('chat'),
      userId = that.data('user');
    if (!userId) {
      userId = that.data('id');
    }
    userId = parseInt(userId);
    if (wrapper.hasClass('visible') && wrapper.find('.inner-wrapper').hasClass('chat') && Cookies('openChatId') === chatId) {
      closeChat();
      return;
    }

    closeWrapper();

    if (chatId !== undefined) {
      openChat(chatId);
    } else {
      var phone = $('.messenger-calc-panel-input').val();

      $.ajax({
        type: 'POST',
        url: '/notification/open-chat?toUserId=' + (userId !== undefined ? userId : '') + '&phone=' + (phone !== undefined ? phone : '')
      }).done(function (data) {
        openChat(data.chat_id);
        getAllChats();
      });
    }
  });

  $(document).on('click', '.js-call', function (e) {
    var phone = $('#phoneCallInput').val();

    if (!phone) {
      phone = $(e.target).data('phone');
    }

    if (phone && phone !== 'undefined') {
      call(phone);
    }
  });

  $(document).on('click', '.js-answer', function (e) {
    answer(e);
  });

  $(document).on('click', '#wrapper', function () {
    var wrapper = $('#wrapper'),
      innerWrapper = wrapper.find('.inner-wrapper.chat');
    if (innerWrapper.length && innerWrapper.data('chat')) {
      var chatId = innerWrapper.data('chat');
      acceptMessages(chatId);
    }
  });

  $(document).on('click', '.js-close-chat', function (e) {
    e.preventDefault();
    closeChat();
  });

  $(document).on('click', '.js-send-message', function (e) {
    e.preventDefault();
    sendMessage();
  });

  $(document).on('keydown', '.js-message-box', function (e) {
    var code = e.keyCode ? e.keyCode : e.which;
    if (code === 13) {
      e.preventDefault();
      sendMessage();
    }
  });

  $(document).on('click', '.js-shutdown-chat', function (e) {
    var that = $(this);

    e.stopPropagation();

    shutdownChat(that.data('chat'));

    that.closest('.chat-item').remove();
  });

  $(document).on('click', '.js-show-calc-window', function () {
    if (window.coolPhone.isRegistered()) {
      openCalc();
    }
  });

  $(document).on('click', '.messenger-calc-btn', function () {
    var that = $(this),
      panel = that.closest('.messenger-calc-body').find('.messenger-calc-panel'),
      input = that.closest('.messenger-calc-body').find('input');

    input.val(input.val() + that.data('digit'));

    panel.addClass('active');
  });

  $('.messenger-calc-body').find('input').keyup(function () {
    var that = $(this),
      panel = that.closest('.messenger-calc-panel'),
      input = that.closest('.messenger-calc-body').find('input');
    if (!input.val()) {
      panel.removeClass('active');
    } else {
      panel.addClass('active');
    }
  });

  $(document).on('click', '.js-calc-delete', function () {
    var that = $(this),
      panel = that.closest('.messenger-calc-panel'),
      input = that.closest('.messenger-calc-body').find('input');

    input.val(input.val().substring(0, input.val().length - 1));

    if (!input.val()) {
      panel.removeClass('active');
    }
  });

  if (typeof wsServerUrl !== 'undefined') {
    var socket = (window.MySocket !== undefined) ? window.MySocket : window.MySocket = io(wsServerUrl);
    socket.on('chat', function (data) {
      if (data.userId === parseInt(userId)) {
        getAllChats(data.chatId);
      }

      var chatContainer = $('.chat'),
        chatContainerText = chatContainer.find('.slimScrollDiv ul'),
        chatType = (data.userId === parseInt(chatContainer.data('user'))) ? 'youself' : '';
      //todo groups!
      if (chatType === 'youself' && data.fromUserId !== parseInt(userId)) {
        chatContainerText.append('<li class="' + chatType + '">' +
          '<div class="time">только что</div>' + data.text + '</li>');
        chatContainerText.slimScroll({
          scrollTo: '1000000px'
        });
      }
    });

    setInterval(function () {
      var currentChatId = Cookies('openChatId');
      if (currentChatId !== undefined) {
        getAllChats(currentChatId);
        if (isOpenChat(currentChatId) && $('.js-message-box').val() === '') {
          getAllMessages(currentChatId);
        }
      }
    }, 1000 * 180);

    socket.on('notify', function (data) {
      if (data.userId === parseInt(userId)) {
        var info = false;

        if (data && data.text) {
          info = true;
        }
        getAllNotify(false, info);
      }
    });
  }
});

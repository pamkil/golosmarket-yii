<?php

namespace console\controllers;

use api\modules\v1\models\records\announcer\Announcer;
use Carbon\Carbon;
use common\models\access\User;
use common\models\user\City;
use common\models\user\Review;
use Faker\Factory;
use api\modules\v1\models\records\auth\User as ApiUser;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

class InitLocalController extends Controller
{
    private $factory;
    private $city;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->factory = Factory::create();
    }

    public function init()
    {
        if (YII_ENV != 'dev') {
            echo $this->ansiFormat('Доступно только в DEV окружении' . PHP_EOL . PHP_EOL);
            return ExitCode::CONFIG;
        }

        parent::init();
    }


    /**
     * Регистрируем пользователя с указанным email как администратора
     *
     * @param string $email Email пользователя в БД, по умолчанию zvr84@mail.ru
     * @return int
     * @throws \Exception
     */
    public function actionAdmin(string $email = 'zvr84@mail.ru')
    {
        $user = ApiUser::find()->where(['email' => $email])->one();

        if (empty($user)) {
            Console::error('Email not found.');
            return ExitCode::DATAERR;
        }

        $authManager = \Yii::$app->getAuthManager();
        $admin = $authManager->getRole('admin');

        $authManager->assign($admin, $user->id);

        return ExitCode::OK;
    }

    /**
     * Заполняем БД данными по умолчанию
     *
     * @return int
     */
    public function actionSeeds()
    {
        Console::output('Started filling seeds in DB.');
        Console::output('Added cities.');
        $this->city = City::create([
            'title' => 'Cанкт-Петербург'
        ]);
        City::create([
            'title' => 'Москва'
        ]);
        Console::output('Added users.');
        $usersData = $this->getUsersSeeds();
        \Yii::$app->db->createCommand()->batchInsert(
            User::tableName(),
            [
                'id',
                'city_id',
                'email',
                'firstname',
                'lastname',
                'password_hash',
                'auth_key',
                'verification_token',
                'phone',
                'type',
                'status',
                'quantity_reviews',
                'created_at',
                'updated_at'
            ],
            $usersData
        )->execute();

        Console::output('Added announcers data.');
        $users = ApiUser::findAll(['status' => User::STATUS_ACTIVE]);
        foreach ($users as $user) {
            $roles = [User::ROLE_GUEST];
            if ((int)$user->type === User::TYPE_ANNOUNCER) {
                $roles[] = User::ROLE_ANNOUNCER;

                $announcer = new Announcer([
                    'user_id' => $user->id,
                    'cost' => 100,
                    'cost_a4' => 200,
                    'cost_sound_mount' => 250,
                    'cost_audio_adv' => 350
                ]);
                $announcer->genders = [rand(1,2)];
                $announcer->save();
            } elseif ((int)$user->type === User::TYPE_CLIENT) {
                $roles[] = User::ROLE_CLIENT;
            }

            if ($user->email === \Yii::$app->params['adminEmail']) {
                $roles[] = User::ROLE_ADMIN;
            }

            $user->loadRoles($roles ?? []);
        }

        Console::output('Added reviews.');
        foreach ($this->getReviewSeeds() as $reviewSeed) {
            $review = new Review($reviewSeed);
            $review->save();
        }

        Console::output('Filling success.');
        return ExitCode::OK;
    }

    private function getUsersSeeds()
    {
        $passHash = \Yii::$app->security->generatePasswordHash('123456');
        $dateTime = Carbon::now()->toDateTimeString();

        return [
            [
                1,
                $this->city['id'],
                \Yii::$app->params['adminEmail'],
                $this->factory->firstName,
                $this->factory->lastName,
                $passHash,
                \Yii::$app->security->generateRandomString(),
                \Yii::$app->security->generateRandomString() . '_' . time(),
                $this->factory->phoneNumber,
                User::TYPE_ANNOUNCER,
                User::STATUS_ACTIVE,
                0,
                $dateTime,
                $dateTime
            ],
            [
                2,
                $this->city['id'],
                "announcer1@test.ru",
                $this->factory->firstName,
                $this->factory->lastName,
                $passHash,
                \Yii::$app->security->generateRandomString(),
                \Yii::$app->security->generateRandomString() . '_' . time(),
                $this->factory->phoneNumber,
                User::TYPE_ANNOUNCER,
                User::STATUS_ACTIVE,
                4,
                $dateTime,
                $dateTime
            ],
            [
                3,
                $this->city['id'],
                "announcer2@test.ru",
                $this->factory->firstName,
                $this->factory->lastName,
                $passHash,
                \Yii::$app->security->generateRandomString(),
                \Yii::$app->security->generateRandomString() . '_' . time(),
                $this->factory->phoneNumber,
                User::TYPE_ANNOUNCER,
                User::STATUS_ACTIVE,
                4,
                $dateTime,
                $dateTime
            ],
            [
                4,
                $this->city['id'],
                "announcer3@test.ru",
                $this->factory->firstName,
                $this->factory->lastName,
                $passHash,
                \Yii::$app->security->generateRandomString(),
                \Yii::$app->security->generateRandomString() . '_' . time(),
                $this->factory->phoneNumber,
                User::TYPE_ANNOUNCER,
                User::STATUS_ACTIVE,
                4,
                $dateTime,
                $dateTime
            ],
            [
                5,
                $this->city['id'],
                "announcer4@test.ru",
                $this->factory->firstName,
                $this->factory->lastName,
                $passHash,
                \Yii::$app->security->generateRandomString(),
                \Yii::$app->security->generateRandomString() . '_' . time(),
                $this->factory->phoneNumber,
                User::TYPE_ANNOUNCER,
                User::STATUS_ACTIVE,
                4,
                $dateTime,
                $dateTime
            ],
            [
                6,
                $this->city['id'],
                "client1@test.ru",
                $this->factory->firstName,
                $this->factory->lastName,
                $passHash,
                \Yii::$app->security->generateRandomString(),
                \Yii::$app->security->generateRandomString() . '_' . time(),
                $this->factory->phoneNumber,
                User::TYPE_CLIENT,
                User::STATUS_ACTIVE,
                0,
                $dateTime,
                $dateTime
            ],
            [
                7,
                $this->city['id'],
                "client2@test.ru",
                $this->factory->firstName,
                $this->factory->lastName,
                $passHash,
                \Yii::$app->security->generateRandomString(),
                \Yii::$app->security->generateRandomString() . '_' . time(),
                $this->factory->phoneNumber,
                User::TYPE_CLIENT,
                User::STATUS_ACTIVE,
                0,
                $dateTime,
                $dateTime
            ],
            [
                8,
                $this->city['id'],
                "client3@test.ru",
                $this->factory->firstName,
                $this->factory->lastName,
                $passHash,
                \Yii::$app->security->generateRandomString(),
                \Yii::$app->security->generateRandomString() . '_' . time(),
                $this->factory->phoneNumber,
                User::TYPE_CLIENT,
                User::STATUS_ACTIVE,
                0,
                $dateTime,
                $dateTime
            ],
            [
                9,
                $this->city['id'],
                "client4@test.ru",
                $this->factory->firstName,
                $this->factory->lastName,
                $passHash,
                \Yii::$app->security->generateRandomString(),
                \Yii::$app->security->generateRandomString() . '_' . time(),
                $this->factory->phoneNumber,
                User::TYPE_CLIENT,
                User::STATUS_ACTIVE,
                0,
                $dateTime,
                $dateTime
            ],
        ];
    }

    private function getReviewSeeds()
    {
        return [
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 2,
                'client_id' => 6,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 2,
                'client_id' => 7,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 2,
                'client_id' => 8,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 2,
                'client_id' => 9,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 3,
                'client_id' => 6,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 3,
                'client_id' => 7,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 3,
                'client_id' => 8,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 3,
                'client_id' => 9,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 4,
                'client_id' => 6,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 4,
                'client_id' => 7,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 4,
                'client_id' => 8,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 4,
                'client_id' => 9,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 5,
                'client_id' => 6,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 5,
                'client_id' => 7,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 5,
                'client_id' => 8,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
            [
                'rating' => $this->factory->numberBetween(1, 5),
                'text' => $this->factory->text(50),
                'announcer_id' => 5,
                'client_id' => 9,
                'type_writer' => Review::TYPE_WRITE_CLIENT,
                'is_public' => true
            ],
        ];
    }
}

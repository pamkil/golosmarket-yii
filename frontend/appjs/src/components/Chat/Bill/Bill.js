import React, { useMemo, useState } from "react";
import classes from "classnames";
import rubCoin from "../../../images/rubCoinIcon.svg";
import { useDispatch } from "react-redux";
import { updateBill } from "../../../data/redux";

const PayText = ({
  sum,
  payedBill,
  cancelBill,
  isPaid,
  id,
  status,
  cardNumber,
  copyHandler,
}) => {
  const [copied, setCopied] = useState(false);
  const [showAcceptButton, setShowAcceptButton] = useState(false);

  return isPaid ? (
    <p className="payform__text">
      Счет №{id} на сумму <b>{sum}</b> руб. оплачен. Спасибо
    </p>
  ) : (
    <>
      <div className="payform__text-container">
        <pre className="payform__text">
          Переведите{" "}
          <b>{Number.isInteger(Number(sum)) ? Math.round(sum) : sum}</b> руб.
        </pre>
        <div className="payform__card_number">
          <p className="payform__text">
            {" "}
            По номеру карты исполнителя: {cardNumber
              .match(/.{1,4}/g)
              .join(" ")}{" "}
          </p>
          {copied ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              stroke="#000000"
              stroke-width="2"
              stroke-linecap="round"
              stroke-linejoin="round"
            >
              <polyline points="20 6 9 17 4 12"></polyline>
            </svg>
          ) : (
            <svg
              style={{ cursor: "pointer" }}
              onClick={() => {
                copyHandler();
                setCopied(true);
              }}
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              title={"Скопировать"}
              stroke="#000000"
              stroke-width="2"
              stroke-linecap="round"
              stroke-linejoin="round"
            >
              <rect x="9" y="9" width="13" height="13" rx="2" ry="2"></rect>
              <path d="M5 15H4a2 2 0 0 1-2-2V4a2 2 0 0 1 2-2h9a2 2 0 0 1 2 2v1"></path>
            </svg>
          )}
        </div>
      </div>
      <div className="payform__buttons">
 
        {status === 2 ? (
          <div style={{ display: "flex" }}>
            <span className="payform__appove-waiting-text">
              <img style={{ width: "20px", height: "20px" }} src={rubCoin} />
              <b>
                Спасибо! Исполнитель проверит поступление, рекомендуем отправить
                в чат скрин, подтверждающий оплату
              </b>
            </span>
          </div>
        ) : showAcceptButton ? (
          <button
            className="payform__button-green"
            onClick={() => {
              payedBill();
            }}
          >
            Подтвердить перевод исполнителю
          </button>
        ) : (
          <button
            className="payform__button-green"
            onClick={() => {
              setShowAcceptButton(true);
            }}
          >
            Нажмите, когда сделаете перевод
          </button>
        )}
        {status === 2 ? (
          <></>
        ) : (
          <button className="payform__button" onClick={cancelBill}>
            Отказаться
          </button>
        )}
      </div>
    </>
  );
};

const Bill = ({ bill, payedBill, cancelBill }) => {
  const dispatch = useDispatch();
  const copyHandler = () => {
    navigator.clipboard
      .writeText(bill.card_number)
      .then(() => {
        console.log("Скопировано");
      })
      .catch((error) => {
        console.error(`Текст не скопирован ${error}`);
      });
  };

  const payedBillHandler = () => {
    const status = 2; 
    if(!bill.announcer_id) return
    dispatch(updateBill(bill.announcer_id, 1, status));
  };

  return (
    <div
      className={classes("payform", {
        "payform--is-paid": bill && bill.isPaid,
      })}
    >
      {bill /*&& url*/ ? (
        <PayText
          copyHandler={copyHandler}
          id={bill.id}
          sum={bill.sum}
          status={bill.status}
          cardNumber={bill.card_number}
          /*url={ url }*/ payedBill={payedBillHandler}
          cancelBill={cancelBill}
          isPaid={bill.isPaid}
        />
      ) : (
        "Исполнитель выставит счет, тут появится форма оплаты"
      )}
    </div>
  );
};

export default Bill;

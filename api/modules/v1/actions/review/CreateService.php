<?php

namespace api\modules\v1\actions\review;

use api\modules\v1\models\checks\site\ReviewServiceForm;
use api\modules\v1\models\records\site\Review;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;

/**
 * @OA\Post(
 *      path="/review/service",
 *      summary="Добавление в избранное",
 *      tags={"Chat"},
 *      @OA\Response(
 *          response="200",
 *          description="Объект токена",
 *          @OA\JsonContent(ref="#/components/schemas/Chat")
 *      ),
 *      @OA\Response(
 *          response="400",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="401",
 *          description="Unauthorized",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */

/**
 * Class Create
 * @package api\modules\v1\actions\review
 *
 */
class CreateService extends Action
{
    /**
     * @var string the scenario to be assigned to the new model before it is validated and saved.
     */
    public $scenario = Model::SCENARIO_DEFAULT;

    /**
     * @return Review
     * @throws ForbiddenHttpException
     */
    public function run()
    {
        $clientId = Yii::$app->user->id;
        if (!$clientId) {
            throw new ForbiddenHttpException();
        }

        $postData = Yii::$app->request->post();
        $postData['user_id'] = $clientId;

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $review = new ReviewServiceForm();
            $review->load($postData, '');
            $reviewModel = new Review($review->toArray());

            if ($review->validate() && $reviewModel->save()) {
                $transaction->commit();
                $reviewModel->refresh();

                return $reviewModel;
            }
            throw new BadRequestHttpException($review->errors);
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }
}

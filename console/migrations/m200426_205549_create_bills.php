<?php

use yii\db\Migration;

/**
 * Class m200426_205549_create_bills
 */
class m200426_205549_create_bills extends Migration
{
    private $bill = '{{%user_bill}}';
    private $user = '{{%user}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->bill, [
            'id' => $this->primaryKey(),
            'announcer_id' => $this->integer(),
            'client_id' => $this->integer(),
            'status' => $this->smallInteger(),
            'sum' => $this->decimal(8, 2),
            'percent' => $this->decimal(5,2),
            'date' => $this->dateTime(),
            'is_canceled' => $this->boolean()->notNull()->defaultValue(false),
            'is_payed' => $this->boolean()->notNull()->defaultValue(false),
            'is_send' => $this->boolean()->notNull()->defaultValue(false),

            'ya_notification_type' => $this->string(),//p2p-incoming, card-incoming
            'ya_operation_id' => $this->string(),
            'ya_amount' => $this->decimal(8, 2),
            'ya_withdraw_amount' => $this->decimal(8, 2),
            'ya_currency' => $this->string(),//643
            'ya_datetime' => $this->dateTime(),
            'ya_label' => $this->string(),
            'ya_sha1_hash' => $this->string(),
            'ya_codepro' => $this->boolean(),
            'ya_test_notification' => $this->boolean(),
            'ya_unaccepted' => $this->boolean(),
            'ya_success' => $this->boolean()->notNull()->defaultValue(false),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),

        ], $tableOptions);

        $this->createIndex(
            'IX-user_bill-ya_label',
            $this->bill,
            'ya_label'
        );

        $this->createIndex(
            'IX-user_bill-announcer_id',
            $this->bill,
            'announcer_id'
        );
        $this->addForeignKey(
            'FK-user_bill-announcer_id',
            $this->bill,
            'announcer_id',
            $this->user,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'IX-user_bill-client_id',
            $this->bill,
            'client_id'
        );
        $this->addForeignKey(
            'FK-user_bill-client_id',
            $this->bill,
            'client_id',
            $this->user,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->bill);
    }
}

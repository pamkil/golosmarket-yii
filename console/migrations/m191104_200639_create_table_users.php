<?php

use yii\db\Migration;

/**
 * Class m191104_200639_create_table_users
 */
class m191104_200639_create_table_users extends Migration
{
//    private $users = '{{%users}}';
//    private $announcer = '{{%user_announcer}}';
//    private $announcer2tags = '{{%user_announcer_2_tags}}';
//    private $tags = '{{%user_announcer_tags}}';
//    private $presentation = '{{%user_announcer_presentations}}';
//    private $announcer2Present = '{{%user_announcer_2_presentations}}';
//    private $announcer2Lang = '{{%user_announcer_2_lang}}';
//    private $langs = '{{%user_announcer_lang}}';
//    private $age = '{{%user_announcer_age}}';
//    private $announcer2Age = '{{%user_announcer_2_age}}';
//    private $gender = '{{%user_announcer_gender}}';
//    private $announcer2gender = '{{%user_announcer_2_gender}}';
//    private $timbre = '{{%user_announcer_timbre}}';
//    private $announcer2timbre = '{{%user_announcer_2_timbre}}';
//    private $photo = '{{%user_announcer_photo}}';
//    private $announcer2photo = '{{%user_announcer_2_photo}}';


    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $tableOptions = null;
//        if ($this->db->driverName === 'mysql') {
//            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
//            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
//        }

//        $this->createTable($this->users, [
//            'id' => $this->primaryKey(),
//            'city_id' => $this->integer()->null(),
//            'type' => $this->smallInteger()->notNull()->defaultValue(1),
//            'status' => $this->smallInteger()->notNull()->defaultValue(1),
//            'is_blocked' => $this->boolean()->notNull()->defaultValue(false),
//            'firstname' => $this->string()->comment('name'),
//            'lastname' => $this->string()->comment('fam or familiya'),
//            'email' => $this->string(),
//            'phone' => $this->string(20),
//            'password' => $this->string(),
//            'access_token' => $this->string(), // for rest api
//            'auth_key' => $this->string(),
//            'last_visit' => $this->timestamp(),
//            'updated_at' => $this->timestamp(),
//            'created_at' => $this->timestamp(),
//            'deleted_at' => $this->timestamp(),
//        ], $tableOptions);

//        $this->createTable($this->announcer, [
//            'id' => $this->primaryKey(),
//            'user_id' => $this->integer()->null(),
//            'operating schedule' => $this->string()->comment('график работы'),
//            'info' => $this->text()->notNull()->defaultValue(''),
//            'equipment' => $this->text()->notNull()->defaultValue('')->comment('оборудование'),
//            'avatar' => $this->string(),
//            'cost' => $this->integer()->notNull()->defaultValue(0)->comment('стоимость начитки'),
//            'discount' => $this->string()->comment('Сделаю скидку'),
//            'likes' => $this->integer()->notNull()->defaultValue(0),
//            'dislikes' => $this->integer()->notNull()->defaultValue(0),
//            'cash_yamoney' => $this->string(),
//            'cash_sber' => $this->string(),
//            'updated_at' => $this->timestamp(),
//            'created_at' => $this->timestamp(),
//            'deleted_at' => $this->timestamp(),
//        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        echo "m191104_200639_create_table_users cannot be reverted.\n";
//
//        return false;
    }
}

<?php

namespace common\models\content;

use common\models\announcer\Age;
use common\models\announcer\Gender;
use common\models\announcer\Language;
use common\models\announcer\Presentation;
use common\models\Model;

/**
 * Автор публикаций
 *
 * Class Page
 *
 * @package common\models\content
 *
 * @property int $id
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property int $page_id [int(11)]
 * @property int $gender_id [int(11)]
 * @property int $age_id [int(11)]
 * @property int $presentation_id [int(11)]
 * @property int $language_id [int(11)]
 * @property bool $sound_by_key [tinyint(1)]
 * @property bool $script [tinyint(1)]
 * @property bool $photo [tinyint(1)]
 * @property Page $page
 * @property Language $language
 * @property Presentation $presentation
 * @property Age $age
 * @property Gender $gender
 * @property bool   $online [tinyint(1)]
 * @property bool   $cost_by_a4 [tinyint(1)]
 * @property int    $cost_at [int(11)]
 * @property int    $cost_to [int(11)]
 * @property int    $count [smallint(6)]
 */
class PageFilter extends Model
{
    public static function tableName()
    {
        return '{{%content_page_2_filter}}';
    }

    public function rules()
    {
        return [
            [
                [
                    'page_id',
                    'gender_id',
                    'age_id',
                    'presentation_id',
                    'language_id',
                    'cost_at',
                    'cost_to',
                    'count',
                ],
                'number',
            ],
            [
                [
                    'sound_by_key',
                    'script',
                    'photo',
                    'online',
                    'cost_by_a4',
                ],
                'boolean',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'page',
            'gender_id' => 'Род диктора',
            'age_id' => 'Возраст звучания',
            'presentation_id' => 'Задача голосу',
            'language_id' => 'Язык',
            'count' => 'Отображать кол-во дикторов',
            'cost_by_a4' => 'Цена за А4',
            'cost_at' => 'Цена от',
            'cost_to' => 'Цена до',
            'sound_by_key' => 'Звук под ключ',
            'script' => 'Сценарий',
            'photo' => 'Фото студии',
            'online' => 'В доступе сейчас',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }

    public function getPage()
    {
        return $this->hasOne(Page::class, ['id' => 'page_id']);
    }

    public function getGender()
    {
        return $this->hasOne(Gender::class, ['id' => 'gender_id']);
    }

    public function getAge()
    {
        return $this->hasOne(Age::class, ['id' => 'age_id']);
    }

    public function getPresentation()
    {
        return $this->hasOne(Presentation::class, ['id' => 'presentation_id']);
    }

    public function getLanguage()
    {
        return $this->hasOne(Language::class, ['id' => 'language_id']);
    }
}

<?php

namespace api\modules\v1\controllers;

use api\modules\v1\actions\money\Create;
use api\modules\v1\models\records\money\Money;
use yii\helpers\ArrayHelper;
use yii\rest\OptionsAction;

/**
 * @SWG\Tag(
 *   name="Money",
 *   description=""
 * )
 */
class MoneyController extends RestController
{
    public $modelClass = Money::class;

    private $lastModified = null;
    private $etag = '';

    public function behaviors()
    {
        $behaviors = ArrayHelper::merge(parent::behaviors(), [
            'authenticator' => [
                'optional' => [],
                'except' => ['options', 'create'],
            ],
        ]);

        return $behaviors;
    }

    public function checkAccess($action, $model = null, $params = [])
    {

    }

    public function actions()
    {
        return [
            'create' => [
                'class' => Create::class,
                'modelClass' => Money::class
            ],
            'options' => [
                'class' => OptionsAction::class,
            ]
        ];
    }

    protected function verbs()
    {
        return [
            'create' => ['GET', 'POST'],
        ];
    }
}

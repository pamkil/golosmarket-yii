<?php

namespace api\modules\v1\models\filters\user;

use api\modules\v1\models\records\announcer\Announcer;
use api\modules\v1\models\records\auth\User;
use api\modules\v1\models\records\user\BillProfile;
use api\modules\v1\models\records\user\Chat;
use common\models\user\Favorite;
use common\service\user\dto\BillSearchDto;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * @OA\Schema(title="Получение списка заказов")
 */
class BillSearch extends Model
{
    /**
     * @param BillSearchDto $dto
     * @return ActiveDataProvider
     */
    public function search(BillSearchDto $dto): ActiveDataProvider
    {
        $perPage = min($dto->perPage, 80);
        if ($perPage === 0) {
            $perPage = 20;
        }
        BillProfile::$isAnnouncer = $dto->isAnnouncer;

        $query = BillProfile::find()
            ->with(['opponent']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'announcer_id',
                    'client_id',
                    'created_at',
                    'date',
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
            'pagination' => [
                'pageSize' => $perPage,
                'page' => $dto->page - 1,
            ]
        ]);

        $field = $dto->isAnnouncer ? 'announcer_id' : 'client_id';

        $query->andWhere([
            $field => $dto->userId,
        ]);

        return $dataProvider;
    }

    /**
     * @param int $userId
     * @param bool $isAnnouncer
     * @param string $filter
     * @param int $page
     * @param int $perPage
     *
     * @return ActiveDataProvider
     */
    public function getList(int $userId, bool $isAnnouncer, string $filter, int $page, int $perPage)
    {
        $perPage = $perPage > 80 ? 80 : $perPage;
        $query = Chat::find()
            ->with(['announcer', 'client']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'created_at',
                ],
                'defaultOrder' => ['created_at' => SORT_DESC]
            ],
            'pagination' => [
                'pageSize' => $perPage,
                'page' => $page - 1,
            ]
        ]);

        $field = $isAnnouncer ? 'announcer_id' : 'client_id';

        $query->andWhere([
            $field => $userId,
        ]);

        $subQuery = (new Query());
        $subQuery->select(["max(id) as id"])
            ->from(['uc2' => Chat::tableName()])
            ->where(["uc2.$field" => $userId])
            ->groupBy(['announcer_id', 'client_id']);

        $query
            ->innerJoin(['uc3' => $subQuery], 'uc3.id = ' . Chat::tableName() . '.id');

        $fieldOp = $isAnnouncer ? 'client_id' : 'announcer_id';
        switch ($filter) {
            case 'woman':
            case 'man':

                $subQuery = Announcer::find()
                    ->select('user_id')
                    ->joinWith(['genders genders'])
                    ->where(['genders.code' => $filter]);

                $query->andWhere([$fieldOp => $subQuery]);
                break;

            case 'favorite':
                $isClient = !Yii::$app->user->identity || Yii::$app->user->identity->type === User::TYPE_CLIENT;
                $favorite = Favorite::find()
                    ->select($isClient ? 'announcer_id' : 'client_id')
                    ->where([$isClient ? 'client_id' : 'announcer_id' => Yii::$app->user->id]);

                $query->andWhere([$fieldOp => $favorite]);
                break;
        }

        return $dataProvider;
    }
}

<?php

namespace admin\models\review;

use Carbon\Carbon;
use admin\models\user\Review;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ReviewSearch extends Model
{
    public $announcer_id;
    public $client_id;
    public $announcer;
    public $client;
    public $type_writer;
    public $is_public;
    public $rating;
    public $text;
    public $date_at;
    public $date_to;

    public function rules()
    {
        return [
            [
                ['announcer_id', 'client_id', 'rating', 'type_writer'],
                'integer'
            ],
            [
                ['announcer', 'client', 'text'],
                'string'
            ],
            [
                ['is_public'],
                'boolean'
            ],
            [
                ['date_at'],
                'filter',
                'filter' => function ($val) {
                    if (empty($val)) {
                        return '';
                    }

                    return Carbon::parse($val)->toDateTimeString();
                }
            ],
            [
                ['date_to'],
                'filter',
                'filter' => function ($val) {
                    if (empty($val)) {
                        return '';
                    }

                    return Carbon::parse($val)->endOfDay()->toDateTimeString();
                }
            ],
        ];
    }

    public function search($params)
    {
        $query = Review::find()
            ->alias('review')
//            ->where([])
            ->joinWith(['announcer announcer', 'client client']);
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'review.id',
                        'review.type_writer',
                        'review.is_public',
                        'review.rating',
                        'review.created_at',
                    ],
                    'defaultOrder' => [
                        'review.created_at' => SORT_DESC
                    ],
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->announcer)) {
            $query->andWhere([
                'OR',
                ['like', 'announcer.firstname', $this->announcer],
                ['like', 'announcer.lastname', $this->announcer],
            ]);
        }

        if (!empty($this->client)) {
            $query->andWhere([
                'OR',
                ['like', 'client.firstname', $this->client],
                ['like', 'client.lastname', $this->client],
            ]);
        }

        $query->andFilterWhere(
            [
                'AND',
                ['review.type_writer' => $this->type_writer],
                ['review.is_public' => $this->is_public],
                ['review.rating' => $this->rating],
                ['review.text' => $this->text],
                ['>=', 'review.created_at', $this->date_at],
                ['<=', 'review.created_at', $this->date_to],
            ]
        );

        if (!empty($this->date_at)) {
            $this->date_at = Carbon::parse($this->date_at)->format('d.m.Y');
        }
        if (!empty($this->date_to)) {
            $this->date_to = Carbon::parse($this->date_to)->format('d.m.Y');
        }

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
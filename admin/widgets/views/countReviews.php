<?php
/** @var string $valueDay */
/** @var string $valueMonth */
/** @var string $message */
/** @var string $url */
?>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <a href="<?= $url; ?>">
                <div class="number">
                    <h3 class="font-red-haze">
                        <span>
                            <?= $valueDay; ?>
                        </span>
                        <span style="font-size: 20px;">
                            (<span>
                                <?= $valueMonth; ?>
                            </span>)
                        </span>
                    </h3>
                    <small><?= $message; ?></small>
                </div>
                <div class="icon">
                    <i class="icon-like"></i>
                </div>
            </a>
        </div>
    </div>
</div>
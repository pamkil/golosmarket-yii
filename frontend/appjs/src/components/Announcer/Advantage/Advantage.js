import React from 'react';

import dictorsImg from "../../../images/dictors.png";
import messageImg from "../../../images/message.png";
import cashImg from "../../../images/cash.png";
const styleHeader = {
    display: 'flex',
    justifyContent: 'flex-start',
};

const Advantage = () => (
    <div className="container no-flex">
        <div className="slick-advantage" style={ styleHeader }>
            <div className="item">
                <div className="icon"><img src={ dictorsImg } alt="Кол-во дикторов на сайте"/></div>
                <div className="title">Дикторов на сайте <span>495</span></div>
            </div>
            <div className="item">
                <div className="icon"><img src={ messageImg } alt="Ощайтес в чате"/></div>
                <div className="title">Общайтесь с исполнителем напрямую в чате</div>
            </div>
            <div className="item">
                <div className="icon"><img src={ cashImg } alt="Цена от исполнителя"/></div>
                <div className="title">Цена от исполнителя</div>
            </div>
        </div>
    </div>
);

export default Advantage;
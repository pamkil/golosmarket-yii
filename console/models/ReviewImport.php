<?php

namespace console\models;

use api\modules\v1\models\records\auth\Token;
use api\modules\v1\models\records\auth\User;
use common\models\user\Review;
use Exception;
use Yii;

class ReviewImport
{
    private $data;

    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return Token|User|bool|void|null
     *
     * @throws Exception
     */
    public function run()
    {
        return !!$this->create();
    }

    /**
     * @return Review
     * @throws \yii\db\Exception
     */
    private function create(): ?Review
    {
        $isPositive = ((int)$this->data['typ']) === 1;
        $announcerId = $this->getUserId($this->data['addressee']);
        $clientId = $this->getUserId($this->data['autor']);
        if (!$announcerId || !$clientId) {
            return null;
        }
        $data = [
            'announcer_id' => $announcerId,
            'client_id' => $clientId,
            'is_public' => $this->data['visible'],
            'rating' => $isPositive ? 5 : 1,
            'text' => $this->data['text'],
        ];
        $transaction = Yii::$app->db->beginTransaction();
        $review = new Review(['isImport' => true]);

        $review->load($data, '');
        $review->created_at = $this->data['date'];
        $review->updated_at = $this->data['date'];

        $result = $review->save();
        if (!$result) {
            $transaction->rollBack();
            dd($this->data, $review->errors);
        } else {
            $transaction->commit();
        }

        return $review;
    }

    private function getUserId($oldUserId)
    {
        return self::getUsers()[$oldUserId] ?? null;
    }


    private static $users;

    public static function getUsers()
    {
        if (!self::$users) {
            self::$users = User::find()
                ->select(['id', 'old_id'])
                ->indexBy('old_id')
                ->column();
        }

        return self::$users;
    }
}
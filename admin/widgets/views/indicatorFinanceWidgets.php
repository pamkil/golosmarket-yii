<?php
//https://ionicons.com/v4/usage/
//https://ionicons.com/v4/
$smallValue1 = $smallValue1 ?? '';
$smallValue2 = $smallValue2 ?? '';
?>
<div class="info-box">
    <span class="info-box-icon bg-aqua"><i class="icon ion-md-shuffle"></i></span>
    <div class="info-box-content">
        <span class="info-box-text"><?= $title; ?></span>
        <span class="info-box-number"><?= Yii::$app->formatter->asCurrency($value1); ?><small> <?= $smallValue1; ?></small></span>
        <span class="info-box-number"><?= Yii::$app->formatter->asCurrency($value2); ?><small> <?= $smallValue2; ?></small></span>
        <span class="info-box-number">Рост <?= $upValue > 0 ? '+' : ''; ?><?= round($upValue, 2); ?></span>
        <span class="info-box-number"><?= $upPlan; ?></span>
    </div>
</div>
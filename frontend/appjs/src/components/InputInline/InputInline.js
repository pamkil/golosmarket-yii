import React from "react";

const InputInline = ({
                       children,
                       input,
                       type = 'text',
                       showRecovery = null,
                       placeholder = null,
                       meta: {touched, error, warning}
                     }) => {
    return (
      <>
        {
          type === 'textarea'
            ? (
              <textarea
                placeholder={placeholder}
                {...input}
                className={touched && error ? 'error' : ''}
              />
            )
            : (
              <input
                type={type}
                placeholder={placeholder}
                {...input}
                className={touched && error ? 'error' : ''}
              />
            )
        }
        {children}
        {
          touched && (error || warning) && <br/>
        }
        {
          touched && (
            (error && <div className="error-text">{error}{showRecovery}</div>)
            ||
            (warning && <div className="warning-text">{warning}</div>)
          )
        }
      </>
    )
  }
;

export default InputInline;

<?php

namespace admin\models\content;

use common\models\content\Question;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class QuestionSearch extends Model
{
    public $active;
    public $position;
    public $text;

    public function rules()
    {
        return [
            [
                ['text',],
                'string'
            ],
            [
                ['active',],
                'boolean'
            ],
            [
                ['position',],
                'integer'
            ],
        ];
    }

    public function search($params)
    {
        $query = Question::find();

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'text', 'active', 'position'
                    ]
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $dataProvider->sort->defaultOrder = ['position' => SORT_ASC,];

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'AND',
                ['like', 'text', $this->text],
                ['active' => $this->active],
                ['position' => $this->position],
            ]
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
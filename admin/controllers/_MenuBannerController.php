<?
namespace admin\controllers;

use common\models\catalog\BouquetSection;
use Yii;
use yii\base\Model;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use common\models\content\MenuBanner;
use common\models\content\MenuBannerSearch;

class MenuBannerController extends SecurityController
{
    /**
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('viewMenuBanners')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $searchModel = new MenuBannerSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список меню баннеров';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('updateMenuBanners')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $model = new MenuBanner;

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && Model::loadMultiple($model->getVariationModels(), $post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно создано');

                $model->loadImage('image');

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Создание меню баннера';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список меню баннеров',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $model->active = 1;
        $sectionRoot = BouquetSection::findOne(['depth' => 0]);

        return $this->render('_form', [
            'root' => $sectionRoot,
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response|array
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('updateMenuBanners')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var MenuBanner $model */
        $model = MenuBanner::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && Model::loadMultiple($model->getVariationModels(), $post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');
            }

            $model->loadImage('image');

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }

            if (isset($post['fileId']) && Yii::$app->request->isAjax && $returnEr = $model->getErrorAction()) {
                return $returnEr;
            } else {
                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }
        }

        $this->view->title = 'Изменение меню баннера "' . $model->name . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список меню баннеров',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->name;
        $sectionRoot = BouquetSection::findOne(['depth' => 0]);

        return $this->render('_form', [
            'root' => $sectionRoot,
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \ErrorException
     */
    public function actionDuplicate($id)
    {
        if (!Yii::$app->user->can('updateMenuBanners')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var MenuBanner $model */

        $model = MenuBanner::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        /* @var MenuBanner $newModel */

        $newModel = $model->duplicate();

        if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/update') !== false) {
            return $this->redirect([
                'update',
                'id' => $newModel->id
            ]);
        } else {
            return $this->redirect([
                'index'
            ]);
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('updateMenuBanners')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var MenuBanner $model */

        $model = MenuBanner::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect([
            'index'
        ]);
    }
}
<?php

namespace api\modules\v1\models\records\money;

use Carbon\Carbon;
use common\models\user\Bill;
use yii\base\Model;

/**
 * @OA\Schema(title="Отзыв")
 */
class Money extends Model
{
    public $unaccepted = '';
    public $operation_id = '';
    public $notification_type = '';
    public $datetime = '';
    public $sha1_hash = '';
    public $sender = '';
    public $codepro = '';
    public $currency = '';
    public $amount = '';
    public $withdraw_amount = '';
    public $label = '';
    public $lastname = '';
    public $firstname = '';
    public $fathersname = '';
    public $zip = '';
    public $city = '';
    public $street = '';
    public $building = '';
    public $suite = '';
    public $flat = '';
    public $phone = '';
    public $email = '';
    public $test_notification = '';

    public $notification_secret = '';

    /**
     * @return array|false
     */
    public function fields()
    {
        return [];
    }

    public function rules()
    {
        return [
            [
                ['sha1_hash', 'datetime', 'amount'],
                'required',
            ],
            [
                [
                    'unaccepted',
                    'operation_id',
                    'notification_type',
                    'sender',
                    'codepro',
                    'currency',
                    'withdraw_amount',
                    'label',
                    'lastname',
                    'firstname',
                    'fathersname',
                    'zip',
                    'city',
                    'street',
                    'building',
                    'suite',
                    'flat',
                    'phone',
                    'email',
                    'test_notification',
                ],
                'safe',
            ],
            [
                'datetime',
                'filter',
                'filter' => function ($value) {
                    if (!empty($value)) {
                        try {
                            $value = Carbon::parse($value)->toDateTimeString();
                        } catch (\Exception $exception) {
                            $value = '';
                        }
                    }

                    return $value;
                },
            ],
//            [
//                ['unaccepted', 'codepro'],
//                'match',
//                'pattern' => '/false/'
//            ],
        ];
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        if (parent::validate($attributeNames, $clearErrors)) {
            if ($this->codepro === 'true' || $this->unaccepted === 'true' || $this->test_notification === 'true') {
                return false;
            }

            return true;

            return $this->sha1_hash === $this->genHash();
        }

        return false;
    }

    public function genHash()
    {
        return sha1($this->getStringForHash());
    }

    public function getStringForHash()
    {
        $fields = [
            'notification_type',
            'operation_id',
            'amount',
            'currency',
            'datetime',
            'sender',
            'codepro',
            'notification_secret',
            'label',
        ];

        $values = [];
        foreach ($fields as $field) {
            $values[] = $this[$field] ?? '';
        }

        return implode('&', $values);
    }

    public function setToBill(Bill $bill)
    {
        $bill->setAttributes(
            [
                'ya_notification_type' => $this->notification_type,
                'ya_operation_id' => $this->operation_id,
                'ya_amount' => round((float)$this->amount, 2),
                'ya_withdraw_amount' => round((float)$this->withdraw_amount, 2),
                'ya_currency' => $this->currency,
                'ya_datetime' => $this->getDate($this->datetime),
                'ya_codepro' => $this->codepro === 'true',
                'ya_label' => $this->label,
                'ya_sha1_hash' => $this->sha1_hash,
                'ya_test_notification' => $this->test_notification,
                'ya_unaccepted' => $this->unaccepted,
            ],
            false
        );
    }

    private function getDate($value)
    {
        if (!empty($value)) {
            try {
                $value = Carbon::parse($value)->toDateTimeString();
            } catch (\Exception $exception) {
                $value = '';
            }
        } else {
            $value = '';
        }

        return $value;
    }
}

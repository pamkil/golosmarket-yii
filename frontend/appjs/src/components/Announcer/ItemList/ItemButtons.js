import React, {useCallback, useEffect, useState} from 'react'
import LikeButton from "../../Common/LikeButton/LikeButton";
import SendAnnouncerButton from "../../Common/SendAnnouncerButton/SendAnnouncerButton";
import ShareButton from "../../Common/ShareButton/ShareButton";

const ItemButtons = (props) => {
  let {
    id,
    firstname,
    addToFavorite,
    quantityFavorite,
    isFavorite,
    favList,
    hideHeart,
    profile,
    userProfile
  } = props;

  //if (profile) {
  //  id = profile.id;
  //  isFavorite = profile.isFavorite;
  //}

  let keyCheck;

  if (favList.length > 0) {
    keyCheck = favList.indexOf(id);
  }
  const [isLocalFav, setIsLocalFav] = useState(keyCheck >= 0);

  const handleLike = useCallback(() => {
    addToFavorite(id, isFavorite || isLocalFav, quantityFavorite);
  }, [isLocalFav, isFavorite, quantityFavorite, id]);

  useEffect(() => {
    let keyCheck;
    if (Object.keys(favList).length > 0) {
      keyCheck = favList.indexOf(id);
    }
    setIsLocalFav(keyCheck >= 0);
  }, [favList.length]);

  return (
    <div className="item__buttons flex align-center">
      <SendAnnouncerButton {...props} />

      <LikeButton
        onClick={handleLike}
        active={isFavorite || isLocalFav}
        count={quantityFavorite || 0}
      />  

      {userProfile ? <ShareButton
        announcerId={userProfile.id}
        announcerName={userProfile.firstname}
      /> : <ShareButton
        announcerId={id}
        announcerName={firstname}
      />}
    </div>
  )
}

export default ItemButtons

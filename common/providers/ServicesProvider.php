<?php

namespace common\providers;

use common\repositories\ChatRepository;
use common\repositories\CollectionRepository;
use common\repositories\CollectionSoundRepository;
use common\repositories\contract\ChatRepositoryContract;
use common\repositories\contract\CollectionRepositoryContract;
use common\repositories\contract\CollectionSoundRepositoryContract;
use common\service\chat\ChatService;
use common\service\chat\contract\ChatServiceContract;
use common\service\collection\CollectionService;
use common\service\collection\contract\CollectionServiceContract;
use Yii;

class ServicesProvider implements \yii\base\BootstrapInterface
{
    /**
     * @inheritDoc
     */
    public function bootstrap($app): void
    {
        $container = Yii::$container;

        /** Service injection */
        $container->set(CollectionServiceContract::class, [
            'class' => CollectionService::class
        ]);
        $container->set(CollectionRepositoryContract::class, [
            'class' => CollectionRepository::class
        ]);
        $container->set(CollectionSoundRepositoryContract::class, [
            'class' => CollectionSoundRepository::class
        ]);

        $container->set(ChatRepositoryContract::class, [
            'class' => ChatRepository::class
        ]);
        $container->set(ChatServiceContract::class, [
            'class' => ChatService::class
        ]);
    }
}

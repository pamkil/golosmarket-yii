<?php

namespace admin\controllers;

use admin\models\user\AnnouncerSearch;
use Carbon\Carbon;
use common\models\access\User;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\db\Query;

use api\modules\v1\models\records\announcer\UserEdit;
use api\modules\v1\models\records\announcer\AnnouncerSound;

class UserAnnouncerController extends SecurityController
{
    public function actionIndex()
    {
        $searchModel = new AnnouncerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

//    public function actionCreate()
//    {
//        $model = new Announcer();
//
//        $post = Yii::$app->request->post();
//        if ($post) {
//            if ($model->load($post) && $model->save()) {
//                Yii::$app->session->addFlash('success', 'Успешно создано');
//
//                return $this->redirect([
//                    'update',
//                    'id' => $model->id
//                ]);
//            }
//
//            foreach ($model->getErrors() as $error) {
//                Yii::$app->session->addFlash('error', $error);
//            }
//        }
//
//        $this->view->title = 'Создание';
//        $this->view->params['breadcrumbs'][] = [
//            'label' => 'Список',
//            'url' => ['index']
//        ];
//        $this->view->params['breadcrumbs'][] = $this->view->title;
//
//        $model->active = 1;
//
//        return $this->render('_form', [
//            'model' => $model
//        ]);
//    }

    public function actionUpdate($id)
    {
        if (!empty(Yii::$app->request->get('updates'))) {
            $data = json_decode(Yii::$app->request->get('updates'), true);
        }

        /* @var User $model */
        $model = UserEdit::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            foreach ($post["AnnouncerSound"] as $row) {
                Yii::$app->db->createCommand()->update('user_announcer_2_sound', [
                    'gender_id' => $row['gender_id'],
                    'lang_code' => $row['lang_code'],
                    'presentation_id' => $row['presentation_id'],
                    'age_id' => $row['age_id'],
                ], ['sound_id' => $row['sound_id']])->execute();
            }

            $announcerId = (new Query())
                ->select('id')
                ->from('user_announcer')
                ->where(['user_id' => $id])
                ->scalar();

            $nameSoundMapping = (new Query())
                ->select(['sound_id'])
                ->from('user_announcer_2_sound')
                ->where(['announcer_id' => $announcerId])
                ->all();

            foreach ($post["File"] as $index => $file) {
                Yii::$app->db->createCommand()
                    ->update('file', ['name' => $file["name"]], ['id' => intval($post["AnnouncerSound"][$index]["sound_id"])])
                    ->execute();
            }

            try {
                $transaction = Yii::$app->db->beginTransaction();
                if ($model->load($post) && $model->announcer->load($post) && $model->save() && $model->announcer->save()) {
                    Yii::$app->session->addFlash('success', 'Успешно обновлено');
                    $transaction->commit();

                    return $this->redirect([
                        'update',
                        'id' => $model->id
                    ]);
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Изменение "' . $model->firstname . ' ' . $model->lastname . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->firstname . ' ' . $model->lastname;

        if (!empty($model->blocked_at)) {
            $model->blocked_at = Carbon::parse($model->blocked_at)->seconds(0)->format('d.m.Y H:i:s');
        }

        return $this->render('_form', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        /* @var User $model */
        $model = User::findOne(['id' => $id]);
        if ($model === NULL) {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
            return $this->redirect('index');
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect('index');
    }
}

<?php

namespace frontend\widgets;

use common\models\content\Menu as MenuModel;
use yii\widgets\Menu;

class TopMenu extends Menu
{
    public function init()
    {
        parent::init();

        $this->items = MenuModel::getTopMenu();
    }
}
<?php

use yii\db\Migration;

/**
 * Class m200515_185426_add_schedule_text
 */
class m200515_185426_add_schedule_text extends Migration
{
    private $table = '{{%user_schedule_work}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'undefined_text', $this->text()->null()->after('time_7_to'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'undefined_text');
    }
}

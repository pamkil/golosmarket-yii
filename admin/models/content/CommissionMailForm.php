<?php

namespace admin\models\content;

use Yii;
use yii\base\Model;

class CommissionMailForm extends Model
{
    public $template;
    private $file_pwd;

    public function formName()
    {
        return '';
    }


    public function attributeLabels()
    {
        return [
            'template' => 'Содержимое письма',
        ];
    }

    public function rules()
    {
        return [
            ['template', 'required',],
            ['template', 'string'],
        ];
    }

    public function saveInFile()
    {
        $template_pwd = Yii::$app->params['commission-mail-path'];
        file_put_contents($template_pwd, $this->template);
    }

    public function getFromFile()
    {
        $template_pwd = Yii::$app->params['commission-mail-path'];
        if (file_exists($template_pwd)) {
            $this->template = file_get_contents($template_pwd);
        }
    }
}
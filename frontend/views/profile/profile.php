<?php

use Carbon\Carbon;
use common\models\access\User;
use common\models\announcer\Announcer;
use common\models\announcer\AnnouncerSound;
use common\models\announcer\File;
use common\models\user\Review;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var string $name
 * @var Announcer $announcer
 * @var AnnouncerSound[] $sounds
 * @var User $user
 * @var array $weekDays
 * @var int $quantityReview
 * @var Review[] $reviews
 * @var string $timeOnline
 * @var string $textSchedule
 * @var bool $showSchedule
 * @var string $title
 * @var int $nowDay [1...7]
 * @var int $ordersCount
 * @var int $paidOrdersCount
 */

if ($user->isAnnouncer) {
    $skills = $user->getSkills();
}
?>
    <section class="content profile">
        <div class="container">
            <div class="titlepage">
                <a class="mobile-no icon" href="/">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M19 12H6M12 5l-7 7 7 7"/></svg>
                </a>
                <h1>
                    <span>
                        <?= Html::encode($title) ?>
                    </span>
                </h1>
            </div>
            <div class="left">
                <div class="work-time">
                    <?php if ($showSchedule || $textSchedule) : ?>
                            <div class="head">В доступе для заказа <span>(по московскому времени)</span></div>
                            <?php if ($showSchedule) : ?>
                                <div class="work-time__week">
                                    <?php foreach (range(1, 7) as $day) : ?>
                                        <div class="item <?= $day === $nowDay ? 'active' : '' ?>">
                                            <span><?= $weekDays[$day]; ?></span>
                                            <?php if (!empty($announcer['scheduleWork']['time_' . $day . '_at'])) : ?>
                                                <?= Carbon::parse(
                                                    '0001-01-01 ' . $announcer['scheduleWork']['time_' . $day . '_at']
                                                )->format('H:i'); ?>
                                                <?= Carbon::parse(
                                                    '0001-01-01 ' . $announcer['scheduleWork']['time_' . $day . '_to']
                                                )->format('H:i'); ?>
                                            <?php else: ?>
                                                <?= $weekDays['dayOff']; ?>
                                            <?php endif; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php elseif ($textSchedule): ?>
                                <?= strip_tags(Html::encode($textSchedule)); ?>
                            <?php endif; ?>
                    <?php endif; ?>

                    <?php if ($user->isAnnouncer) : ?>
                        <div class="service-block">
                            <div class="head">Услуги
                                 <span class="tooltip">
                                    <span class="icon">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             width="24" height="24" viewBox="0 0 24 24"
                                             fill="none" stroke="#000" stroke-width="2"
                                             stroke-linecap="round"
                                             stroke-linejoin="round">
                                            <circle cx="12" cy="12" r="10"></circle>
                                            <path d="M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3"></path>
                                            <line x1="12" y1="17"
                                                  x2="12.01" y2="17"></line>
                                        </svg>
                                        <span>Цена&nbsp;для&nbsp;ориентира. Точную&nbsp;стоимость&nbsp;назовет исполнитель&nbsp;в&nbsp;чате</span>
                                    </span>
                                </span>
                            </div>

                            <ul class="service-list">
                                <?php if (!empty($announcer->cost)) : ?>
                                    <li>
                                        <div class="service-name">Начитка до 30 сек.</div>
                                        <div class="service-price">от <b><?= $announcer->cost ?>Р</b></div>
                                    </li>
                                <?php endif; ?>
                                <?php if (!empty($announcer->cost_a4)) : ?>
                                    <li>
                                        <div class="service-name">Начитка 1 страницы</div>
                                        <div class="service-price">от <b><?= $announcer->cost_a4 ?>Р</b></div>
                                    </li>
                                <?php endif; ?>
                                <?php if (!empty($announcer->cost_vocal)) : ?>
                                    <li>
                                        <div class="service-name">Вокал</div>
                                        <div class="service-price">от <b><?= $announcer->cost_vocal ?>Р</b></div>
                                    </li>
                                <?php endif; ?>
                                <?php if (!empty($announcer->cost_parody)) : ?>
                                    <li>
                                        <div class="service-name">Пародия</div>
                                        <div class="service-price">от <b><?= $announcer->cost_parody ?>Р</b></div>
                                    </li>
                                <?php endif; ?>
                                <?php if (!empty($announcer->cost_sound_mount)) : ?>
                                    <li>
                                        <div class="service-name">Смонтировать звук (рекламный ролик)</div>
                                        <div class="service-price">от <b><?= $announcer->cost_sound_mount ?>Р</b></div>
                                    </li>
                                <?php endif; ?>
                                <?php if (!empty($announcer->cost_audio_adv)) : ?>
                                    <li>
                                        <div class="service-name">Сценарий для аудиорекламы</div>
                                        <div class="service-price">от <b><?= $announcer->cost_audio_adv ?>Р</b></div>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>

                <?php if (!empty($sounds)): ?>
                    <div class="example-stuffing">
                        <div class="head">Пример звучания голоса</div>
                        <?php foreach ($sounds as $lang_name => $sounds_demo) : ?>
                            <dl class="example-stuffing__list">
                                <dt>Язык:</dt>
                                <dd><?= $lang_name ?></dd>
                            </dl>
                            <?php /** @var AnnouncerSound $sound */
                            foreach ($sounds_demo as $sound) : ?>
                            <?php
                                $file = $sound->sound;
                                $sound_url = File::url($file);
                                $sound_name = mb_ereg_replace('[.][a-zA-Z34]{3,4}$', '', $file->name);
                                $sound_desc = '';

                                if (!empty($sound->gender)) {
                                    $sound_desc .= mb_substr($sound->gender->name, 0,3) . ', ';
                                }
                                if (!empty($sound->presentation)) {
                                    switch ($sound->presentation->code) {
                                        case 'maindemo':
                                            $sound_desc .= $sound->presentation->name;

                                            break;
                                        case 'parodist':
                                            $sound_desc .= $sound->presentation->name;
                                            if (!empty($sound->parodist_name)) {
                                                $sound_desc .= '('.$sound->parodist_name.')';
                                            }

                                            break;
                                        default:
                                            $sound_desc .= $sound->presentation->name;
                                            if ($sound->age) {
                                                $sound_desc .= '/' . $sound->age->name;
                                            }

                                            break;
                                    }
                                }
                            ?>
                                <div class="item-play">
                                    <button
                                            type="button"
                                            class="play-track online-play-url-sound"
                                            data-url="<?= $sound_url ?>"
                                            data-title="<?= $sound_name ?>"
                                            data-user="<?= $user->id ?>"
                                    >
                                        <img class="online-play-url-sound-img play" src="/images/play.png" alt="play">
                                        <img class="online-play-url-sound-img stop" src="/images/pause.png" alt="pause">
                                    </button>
                                    <div class="title-track">
                                        <b><?= $sound_name ?></b>
                                        <br>
                                        <span><?= $sound_desc ?></span>
                                    </div>
                                    <div class="icon-track">
                                        <a class="icon-btn"
                                           download="<?= $sound_url ?>"
                                           target="_blank"
                                           href="<?= $sound_url ?>"
                                           rel="noopener noreferrer"
                                        >
                                            <div class="tooltip">
                                                <div class="icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="#000" stroke-width="2"
                                                         stroke-linecap="round" stroke-linejoin="round">
                                                        <path d="M3 15v4c0 1.1.9 2 2 2h14a2 2 0 0 0 2-2v-4M17 9l-5 5-5-5M12 12.8V2.5"/>
                                                    </svg>
                                                    <span>Скачать&nbsp;демо</span>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="icon-btn">
                                            <div class="tooltip">
                                                <div data-url="<?= $sound_url ?>"
                                                     class="icon js-copy-url-sound">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         width="24" height="24" viewBox="0 0 24 24"
                                                         fill="none" stroke="#000" stroke-width="2"
                                                         stroke-linecap="round" stroke-linejoin="round">
                                                        <rect x="9" y="9" width="13" height="13" rx="2" ry="2"></rect>
                                                        <path d="M5 15H4a2 2 0 0 1-2-2V4a2 2 0 0 1 2-2h9a2 2 0 0 1 2 2v1"/>
                                                    </svg>
                                                    <span>Скопировать&nbsp;ссылку&nbsp;на&nbsp;демо</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>

                <div class="announcer-info">
                    <?php if (count($reviews)) : ?>
                        <div class="announcer-reviews">
                            <div class="head">Отзывы</div>
                            <?php foreach ($reviews as $review) : ?>
                                <?php /** @var Review $review */?>
                                <?php $userReview = (($review->type_writer === Review::TYPE_WRITE_CLIENT)
                                    ? $review->client : $review->announcer) ; ?>
                                <div class="item">
                                    <div class="item-left">
                                        <div class="image">
                                            <div class="box">
                                                <a href="<?= Url::to(['profile/view', 'id' => $userReview->id]); ?>">
                                                    <img
                                                        src="<?= $userReview->getPhotoUrl() ?>"
                                                        class="img-fluid"
                                                        alt="<?= Html::encode(
                                                            $userReview->firstname . ' ' . mb_substr($userReview->lastname, 0, 1)
                                                        ) ?>"
                                                    >
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-right">
                                        <div class="recent-review">
                                            <a href="<?= Url::to(['profile/view', 'id' => $userReview->id]); ?>">
                                                <div class="name">
                                                    <?= $userReview->firstname ?? $userReview->lastname ?? '' ?>
                                                </div>
                                            </a>
                                            <div class="stars">
                                                <?php $ratingReview = (int)$review->rating; ?>
                                                <?php foreach (range(5, 1) as $item) : ?>
                                                    <?php if ($item <= $ratingReview) : ?>
                                                        <span class="greenstar">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="11" height="15"
                                                                 viewBox="0 -6 24 24" fill="#77BF22" stroke="#77BF22"
                                                                 stroke-width="1.5" stroke-linecap="round"
                                                                 stroke-linejoin="round"><polygon
                                                                        points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                        </span>
                                                    <?php else: ?>
                                                        <span class="whitestar">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="11" height="15"
                                                             viewBox="0 -6 24 24" fill="none" stroke="#77BF22"
                                                             stroke-width="1.5" stroke-linecap="round"
                                                             stroke-linejoin="round"><polygon
                                                                    points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                    </span>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </div>
                                            <div class="date">
                                                <?= Carbon::parse($review->created_at)->format('d.m.Y') ?>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <?= $review->text ?: 'Отзыв без текста'; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>

                            <?php if ($quantityReview > count($reviews)) : ?>
                                <div class="announcer-reviews__footer">
                                    <a href="<?= Url::to(['profile/review', 'id' => $user->id]) ?>">
                                        Все отзывы (<?= $quantityReview ?>)
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php else: ?>
                        <div class="announcer-reviews">
                            <div class="head">
                                Отзывов и рейтинга пока нет
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($announcer->info)): ?>
                        <div class="more-announcer-service">
                            <div class="head">Дополнительно об услуге</div>
                            <div>
                                <?= $announcer->info; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

                <?php if (!empty($announcer->photos)): ?>
                    <div class="photo">
                        <div class="head">Фото студии, тон-кабины, оборудования</div>
                        <div class="fotorama js-equipment-slider swiper equipment-photos" data-nav="thumbs">
                            <?php foreach ($announcer->photos as $file): ?>
                                <img src="<?= File::url($file); ?>" alt="<?= $file->name; ?>">
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (!empty($announcer->equipment)): ?>
                    <div class="equipment">
                        <div class="head">Оборудование</div>
                        <div>
                            <?= $announcer->equipment; ?>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
            <div class="right">
                <div class="profile-avatar">
                    <div class="image">
                        <div class="box">
                            <img src="<?= $user->getPhotoUrl() ?>"
                                 alt="<?= Html::encode($name) ?>"
                                 class="img-fluid">
                        </div>
                    </div>
                </div>

                <div id="profile-info"
                     data-orders="<?= $ordersCount ?>"
                     data-payments="<?= $paidOrdersCount ?>"
                     data-announcer="<?= ($announcer === null ? 'false' : 'true') ?>"
                     data-online="<?= $timeOnline ?>"
                     data-user="<?= $user->id ?>" class="info">
                </div>
            </div>

            <div id="save-row" class="save-row"></div>
        </div>
    </section>

    <script>
      let equipmentSlier;

      const isMobile = function () {
        return getComputedStyle(document.body, ':before').getPropertyValue('content') === '\"mobile\"';
      }

      document.addEventListener('DOMContentLoaded', function () {
        let shareElements = document.querySelectorAll('.js-share-btn');

        //let $fotoramaDiv = $('.fotorama').fotorama();

        Array.prototype.forEach.call(shareElements, function (el, i) {
          el.addEventListener('click', function (e) {
            if (isMobile() && navigator.share) {
              navigator.share({
                title: window.location.hostname,
                text: el.getAttribute('data-name'),
                url: window.location.href
              })
                .then(function () {
                  console.log("Sharing successfully done")
                })
                .catch(function () {
                  console.log("Sharing failed")
                })
            } else {
              e.target.classList.toggle('__opened');
            }

            return false;
          }, false);
        });

        document.addEventListener('click', function (e) {
          if (!e.target.classList.contains('__opened') || e.target.closest('.js-share-btn') === null) {
            Array.prototype.forEach.call(shareElements, function (el, i) {
              el.classList.remove('__opened');
            });
          }
        });
      });
    </script>
<?php if (!empty($page)) : ?>
    <section class="quote">
        <div class="container">
            <div class="content">
                <?php if (!empty($page->title)) : ?>
                    <h2><?= $page->title ?></h2>
                <?php endif; ?>
                <?= $page->body ?? '' ?>
            </div>
        </div>
    </section>
<?php endif; ?>

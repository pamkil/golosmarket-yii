<?php

use yii\db\Migration;

/**
 * Class m200315_174343_add_city
 */
class m200315_174343_add_city extends Migration
{
    private $table = '{{%user_city}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'code' => $this->string()->notNull(),
                'title' => $this->string()->notNull(),
                'created_at' => $this->timestamp()->notNull(),
                'updated_at' => $this->timestamp()->notNull(),
                'deleted_at' => $this->timestamp(),
            ],
            $tableOptions
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}

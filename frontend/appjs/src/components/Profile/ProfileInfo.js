import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import ProfileControls from "./ProfileControls";
import {getProfileFull, getProfileId} from "../../data/redux";

import './bootstrap.css';
import './profile.css';

const ProfileInfo = (props) => {
  const dispatch = useDispatch();
  const {
    isInit,
    isLoading,
    profile,
    userProfile,
    showButton,
    isLogin,
    test
  } = useSelector(state => {
    return {
      test: state,
      isInit: state.app.initialized,
      isLoading: state.profile.isLoading,
      profile: state.profile.profile,
      userProfile: state.profile.userProfile,
      isLogin: !!state.auth.token
    }
  });

  let {profileId, online, isAnnouncer, payments, orders} = props;

  useEffect(() => {
    if (isInit && profileId) {
      dispatch(getProfileId(profileId + (isAnnouncer === 'true' ? '&expand=recommended' : '')));
    }
  }, [isInit, profileId]);

  useEffect(() => {
    if (isInit && isLogin) {
      dispatch(getProfileFull());
    }
  }, [isInit, isLogin]);
  

  if (!isInit || isLoading) {
    return null;
  }
  return profileId && userProfile && userProfile.hasOwnProperty('id') ? <ProfileControls
    key={userProfile.favoritesCount}
    profile={profile}
    userProfile={userProfile}
    showButton={showButton}
    online={online}
    orders={orders}
    payments={payments}
    quantityFavorite={userProfile && userProfile.hasOwnProperty('favoritesCount') ? userProfile.favoritesCount : 0}
  /> : null;
};

export default ProfileInfo;

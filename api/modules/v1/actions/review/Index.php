<?php

namespace api\modules\v1\actions\review;

use api\modules\v1\actions\common\IndexAction;
use api\modules\v1\models\filters\user\ReviewSearch;
use Yii;

/**
 * @OA\Get(
 *      path="/review",
 *      summary="List of reviews",
 *      tags={"Announcer"},
 *      @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="number Page",
 *          required=false,
 *          @OA\Schema(type="integer")
 *      ),
 *      @OA\Parameter(
 *          name="per_page",
 *          in="query",
 *          description="quantity announcers in Page",
 *          required=false,
 *          @OA\Schema(
 *            type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="filter",
 *          in="query",
 *          description="filter for search announcers",
 *          required=false,
 *          @OA\JsonContent(ref="#/components/schemas/AnnouncerSearch")
 *
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="ok",
 *          @OA\JsonContent(
 *              type="array",
 *              @OA\Items(ref="#/components/schemas/Announcer")
 *          )
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */
class Index extends IndexAction
{
    /** @inheritdoc
     * @return \yii\data\ActiveDataProvider
     */
    public function run()
    {
        parent::run();

        $request = Yii::$app->request;

        $page = (int)$request->get('page', 1);
        $perPage = (int)$request->get('per_page', 12);
        $reviews = (new ReviewSearch())->search($request->queryParams, $page, $perPage);

        return $reviews;
    }
}

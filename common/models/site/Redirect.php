<?php

namespace common\models\site;

use common\models\Model;

/**
 * Автор публикаций
 *
 * Class Setting
 * @package common\models\site
 *
 * @property int $id
 * @property int $code
 * @property string $from [varchar(255)]
 * @property string $to [varchar(1024)]
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 */
class Redirect extends Model
{
    public const CODE_301 = 301;
    public const CODE_302 = 302;

    public static function tableName()
    {
        return '{{%redirect}}';
    }

    public function rules()
    {
        return [
            [
                ['from', 'to'],
                'string'
            ],
            [
                ['code'],
                'number'
            ],
            [
                ['active'],
                'boolean'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from' => 'Путь откуда',
            'to' => 'Перевод на',
            'code' => 'Код редиректа',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public static function getCodes()
    {
        return [
            self::CODE_301 => '301',
            self::CODE_302 => '302',
        ];
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_announcer_timbres}}`.
 */
class m191114_165212_create_timber_table extends Migration
{
    private $timbres = '{{%user_announcer_timbres}}';
    private $announcer2timbres = '{{%user_announcer_2_timbres}}';
    private $announcer = '{{%user_announcer}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->timbres, [
            'id' => $this->primaryKey(),
            'active' => $this->boolean()->notNull()->defaultValue(true),
            'position' => $this->integer(),
            'code' => $this->string(25),
            'name' => $this->string(),
            'menu_title' => $this->string(),
        ], $tableOptions);
        $this->addCommentOnTable($this->timbres, 'Тембр, Высота голоса');

        $this->createTable($this->announcer2timbres, [
            'announcer_id' => $this->integer(),
            'timbre_id' => $this->integer(),
            'PRIMARY KEY (announcer_id, timbre_id)'
        ], $tableOptions);

        $this->createIndex(
            'IX-user_announcer_2_timbres-announcer_id',
            $this->announcer2timbres,
            'announcer_id'
        );
        $this->addForeignKey(
            'FK-user_announcer_2_timbres-announcer_id',
            $this->announcer2timbres,
            'announcer_id',
            $this->announcer,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('IX-user_announcer_2_timbres-timbre_id', $this->announcer2timbres, 'timbre_id');
        $this->addForeignKey(
            'FK-user_announcer_2_timbres-timbre_id',
            $this->announcer2timbres,
            'timbre_id',
            $this->timbres,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addData();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->announcer2timbres);
        $this->dropTable($this->timbres);
    }

    private function addData()
    {
        $this->batchInsert(
            $this->timbres,
            ['name', 'menu_title', 'code', 'position'],
            [
                ['Высокий', 'Высокий тембр', 'high', 1],
                ['Средний', 'Средний тембр', 'middle', 2],
                ['Низкий', 'Низкий тембр', 'low', 3],
                ['Бас', 'Бас', 'bas', 4],
                ['Универсал', 'Универсальный голос', 'universal', 5],
            ]
        );
    }
}

<?php

use yii\db\Migration;

/**
 * Class m191116_102020_add_access_token
 */
class m191116_102020_add_access_token extends Migration
{
    private $table = '{{%user_token}}';
    private $tableUser = '{{%user}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'token' => $this->string()->notNull()->unique(),
            'expired_at' => $this->dateTime()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ], $tableOptions);
        $this->createIndex('ix-user_token-user_id', $this->table, 'user_id');
        $this->addForeignKey(
            'fk-user_token-user_id',
            $this->table,
            'user_id',
            $this->tableUser,
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}

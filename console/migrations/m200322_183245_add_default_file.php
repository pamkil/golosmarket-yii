<?php

use yii\db\Migration;

/**
 * Class m200322_183245_add_default_file
 */
class m200322_183245_add_default_file extends Migration
{
    private $table = '{{%file}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->table,
            'default',
            $this->boolean()->notNull()->defaultValue(false)->after('type')
        );

        $this->addColumn($this->table, 'created_at', $this->dateTime()->notNull());
        $this->addColumn($this->table, 'updated_at', $this->dateTime()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'default');
        $this->dropColumn($this->table, 'created_at');
        $this->dropColumn($this->table, 'updated_at');
    }
}

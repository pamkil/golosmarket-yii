<?php

use common\models\content\Collection;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\content\Collection */

$this->title = 'Просмотр подборки: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Collections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="collection-view">

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить подборку?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'image_id',
                'value' => function (Collection $model) {
                    if (!empty($model->image)) {
                        return '<img src="' . $model->image->getUrl()
                            . '" class="img-responsive" style="max-height: 250px; max-width: 250px;"/>';
                    }

                    return '';
                },
                'format' => 'html'
            ],
            'description:html',
            'active:boolean',
            'position',
            'created_at',
            'updated_at'
        ],
    ]) ?>

</div>

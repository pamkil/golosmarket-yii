<?php

use yii\db\Migration;

/**
 * Class m200730_213850_add_user_review_bill_id
 */
class m200730_213850_add_user_review_bill_id extends Migration
{
    private $review = '{{%user_review}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->review, 'bill_id', $this->integer()->null()->after('client_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->review, 'bill_id');
    }
}

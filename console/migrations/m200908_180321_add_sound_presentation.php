<?php

use yii\db\Migration;

/**
 * Class m200908_180321_add_sound_presentation
 */
class m200908_180321_add_sound_presentation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{%user_announcer_2_sound}}',
            'presentation_id',
            $this->integer()->null()
        );
        $this->createIndex(
            'IX-user_announcer_2_sound-all',
            '{{%user_announcer_2_sound}}',
            [
                'announcer_id',
                'sound_id',
                'presentation_id',
            ]
        );

        $this->addForeignKey(
            'FK-user_announcer_2_sound-presentation_id',
            '{{%user_announcer_2_sound}}',
            'presentation_id',
            'user_announcer_presentations',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('IX-user_announcer_2_sound-all', '{{%user_announcer_2_sound}}');
        $this->dropForeignKey('FK-user_announcer_2_sound-presentation_id', '{{%user_announcer_2_sound}}');
        $this->dropColumn('{{%user_announcer_2_sound}}', 'presentation_id');
    }
}

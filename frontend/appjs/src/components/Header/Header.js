import React, { useCallback, useEffect, useState } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import copy from "copy-to-clipboard";
import { toast, ToastContainer } from "react-toastify";
import { Notifications } from "react-push-notification";
import { ProfileService } from "../../data/services";

import HeaderPanel from "./HeaderPanel/HeaderPanel";
import {
  Login as LoginAction,
  RegisterAnnouncer as RegisterAnnouncerAction,
  RegisterClient as RegisterClientAction,
  Restore as RestoreAction,
  STATUS,
  RegisterClientInit,
  RegisterAnnouncerInit,
  setFavLocalList,
  setShowRegisterAnnouncerForm,
  setShowRegisterClientForm,
  setShowLoginForm,
  setShowRestoreForm,
  setShowFeedbackForm,
  setShowMenuRegister,
  setShowConfirmationForm,
  setShowNewAnnouncerPopUp,
  clearUser,
  logout,
  getProfileId,
  setShowWorkTime,
} from "../../data/redux";
import {
  APP_UPDATE_EVENT,
  CreateFeedback,
  CreateReviewService,
  getProfileLoad,
  initializeApp,
  setIsMobile,
} from "../../data/redux";

import LoginForm from "../Auth/LoginForm";
import RegisterClientForm from "../Auth/RegisterClientForm";
import RegisterAnnouncerForm from "../Auth/RegisterAnnouncerForm";
import RestoreForm from "../Auth/RestoreForm";
import Modal from "../Common/Modal/Modal";
import FeedbackForm from "./Form/FeedbackForm";

import {
  textCopy,
  textProfile,
  textRegister,
  textRestore,
} from "../Announcer/Announcer";
import { eventPlay, eventStop } from "../Common/PlayerEvent/playerEvent";
import { PLAYER } from "../Common/PlayerEvent/playerConst";
import { ReactComponent as IconHeart } from "../../images/icon-heart.svg";
import avatarImgDef from "../../images/avatar.png";
import { isMobileBrowser } from "../../data/helpers/Tools";
import NotifyCount from "../Common/NotifyCount/NotifyCount";

const copyHandler = (fileUrl) => {
  copy(document.location.origin + fileUrl);
};

function Header({
  auth: { token },
  header,
  user,
  LoginAction,
  RestoreAction,
  RegisterAnnouncerAction,
  RegisterClientAction,
  CreateFeedback,
  initializeApp,
  setIsMobile,
  isInit,
  getProfileLoad,
  registerClient,
  RegisterClientInit,
  registerAnnouncer,
  favListCount,
  RegisterAnnouncerInit,
}) {
  const dispatch = useDispatch();
  const isLogin = !!token;
  const [userProfile, setUserProfile] = useState({});
  const [chaportReady, setChaportReady] = useState(false);
  const {
    showLoginForm,
    showFeedbackForm,
    showConfirmationForm,
    showMenuRegister,
    showRestoreForm,
    showRegisterClientForm,
    showRegisterAnnouncerForm,
    showNewAnnouncerPopUp,
    showWorkTime,
  } = header;

  const [acceptedCookies, setAcceptedCookies] = useState(false);
  useEffect(() => {
    const localStorageCookie = localStorage.getItem("acceptCookies");
    if (localStorageCookie === "true") {
      setAcceptedCookies(true);
    }
  }, []);
  const handleAcceptCookies = () => {
    localStorage.setItem("acceptCookies", true);
    setAcceptedCookies(true);
  };

  useEffect(() => {
    if (isInit && user.id && user.isAnnouncer) {
      fetch(
        `https://golosmarket.ru/api/v1/user/full?id=${user.id}&expand=recommended}`
      )
        .then((response) => response.json())
        .then((userProfileData) => {
          setUserProfile(() => userProfileData);
        });
    }
  }, [isInit, user.id]);

  const setInitialize = useCallback(() => {
    initializeApp();
  }, [initializeApp]);

  const deleteProfileHandler = (id) => {
    ProfileService.delete(id)
      .then(() => {
        logout();
        clearUser();
        toast.info("Профиль удален");

        setTimeout(() => {
          window.location = "/";
        }, 1000);
      })
      .catch((data) => {
        toast.error(data.data.message);
      });
  };

  useEffect(() => {
    const clickHandler = ({ target }) => {
      let elem;
      if (target.classList.contains("online-play-url-sound-img")) {
        elem = target.parentNode;
      } else if (target.classList.contains("online-play-url-sound")) {
        elem = target;
      }

      if (!elem) {
        return;
      }

      const file = {
        url: elem.dataset.url,
        name: elem.dataset.title,
      };
      const announcerId = elem.dataset.user;
      const elements = document.querySelectorAll(".online-play-url-sound");

      const files = [];
      if (elements && elements.length > 0) {
        for (let item of elements) {
          const fileItem = {
            url: item.dataset.url,
            name: item.dataset.title,
          };
          if (fileItem !== file) {
            files.push(fileItem);
          }
        }
      }

      const stopAll = () => {
        if (elements && elements.length > 0) {
          for (let item of elements) {
            item.classList.remove("play");
          }
        }
      };

      document.addEventListener(PLAYER.PLAYING, ({ detail: { file } }) => {
        for (let item of elements) {
          if (item.dataset.url === file.url) {
            item.classList.add("play");
          } else {
            item.classList.remove("play");
          }
        }
      });

      document.addEventListener(PLAYER.STOP, () => {
        stopAll();
      });

      if (elem.classList.contains("play")) {
        eventStop();
      } else {
        eventPlay(announcerId, file, files, true);
      }
    };

    document.body.addEventListener("click", clickHandler);

    /* Orientation Block */
    let viewportHeight;
    let viewportWidth;
    let isPortrait = true;

    const handleResize = () => {
      const mobile = isMobileBrowser();
      setIsMobile(mobile);

      if (!mobile) return;

      const newViewportHeight = Math.max(
        document.documentElement.clientHeight,
        window.innerHeight || 0
      );
      const newViewportWidth = Math.max(
        document.documentElement.clientWidth,
        window.innerWidth || 0
      );

      if (Math.max(newViewportHeight, newViewportWidth) < 390) return;
      if (
        (newViewportHeight > newViewportWidth
          ? newViewportHeight / newViewportWidth
          : newViewportWidth / newViewportHeight) < 1.3
      )
        return;

      const hasOrientationChanged =
        newViewportHeight > newViewportWidth !== isPortrait;

      viewportHeight = newViewportHeight;
      viewportWidth = newViewportWidth;
      isPortrait = viewportHeight > viewportWidth;

      if (hasOrientationChanged) {
        document.body.classList[isPortrait ? "remove" : "add"](
          "mobile-landscape"
        );
      }
    };

    handleResize();
    window.addEventListener("resize", handleResize);
    /* End Orientation Block */

    return () => {
      document.body.removeEventListener("click", clickHandler);
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  useEffect(() => {
    const clickHandler = ({ target }) => {
      let elem;
      if (target.classList.contains("js-js-copy-url-sound-img")) {
        elem = target.parentNode;
      } else if (target.classList.contains("js-copy-url-sound")) {
        elem = target;
      } else if (target.classList.contains("js-copy-url-profile")) {
        copyHandler(window.location.pathname);
        toast.info(textProfile);
        return;
      } else if (target.classList.contains("js-like-btn")) {
        let userID = target.dataset.id;
        let isFav = target.dataset.favorite === "true";
        let quantity = parseInt(target.dataset.count || 0);

        target.classList[isFav ? "remove" : "add"]("is-fav");
        target.dataset.favorite = isFav ? "false" : "true";

        return;
      }

      if (!elem) {
        return;
      }


      copyHandler(elem.dataset.url);
      toast.info(textCopy);
    };
    document.body.addEventListener("click", clickHandler);

    return () => document.body.removeEventListener("click", clickHandler);
  }, []);

  useEffect(() => {
    setInitialize();
  }, [setInitialize]);

  function createChatForNewUser(w, d, v3) {
    w.chaportConfig = {
      appId: "651a979b0af0424c3a191f31",
    };
    if (w.chaport) return;
    v3 = w.chaport = {};
    v3._q = [];
    v3._l = {};
    v3.q = function () {
      v3._q.push(arguments);
    };
    v3.on = function (e, fn) {
      if (!v3._l[e]) v3._l[e] = [];
      v3._l[e].push(fn);
    };
    var s = d.createElement("script");
    s.type = "text/javascript";
    s.async = false;
    s.src = isLogin ? "" : "https://app.chaport.com/javascripts/insert.js";
    var ss = d.getElementsByTagName("script")[0];
    ss.parentNode.insertBefore(s, ss);
    setChaportReady(() => true);
  }

  useEffect(() => {
    let chaportLauncher = document.querySelector(".chaport-launcher");
    if (isLogin) {
      window.chaportConfig.appId = "";

      if (chaportLauncher) {
        chaportLauncher.style.display = "none";
      }
    } else {
      createChatForNewUser(window, document);
    }
  }, [isLogin, isInit, chaportReady]);

  useEffect(() => {
    if (!isLogin || !isInit) {
      const openRegister = () => {
        if (window.location.hash === "#registry") {
          registerClickHandler();
        }
        if (window.location.hash === "#registry-announcer") {
          registerClickHandler(null, "announcer");
        }
        if (window.location.hash === "#login-restore") {
          showRestoreHandler();
        }
        if (window.location.hash === "#login") {
          showLoginHandler();
        }
        if (window.location.hash === "#feedback") {
          showFeedbackHandler();
        }
        return true;
      };
      window.addEventListener("hashchange", openRegister, false);
      openRegister();
    }
    if (isInit && isLogin) {
      document.addEventListener(APP_UPDATE_EVENT, getProfileLoad);
    }
    return () => {
      document.removeEventListener(APP_UPDATE_EVENT, getProfileLoad);
    };
    // eslint-disable-next-line
  }, [isInit, isLogin]);

  useEffect(() => {
    localStorage.setItem(
      "acceptCookies",
      localStorage.getItem("acceptCookies")
    );
  }, []);

  const closeAllModal = () => {
    if (showRestoreForm) {
      dispatch(setShowRestoreForm(false));
    }
    if (showLoginForm) {
      dispatch(setShowLoginForm(false));
    }
    if (showRegisterClientForm) {
      dispatch(setShowRegisterClientForm(false));
    }
    if (showRegisterAnnouncerForm) {
      dispatch(setShowRegisterAnnouncerForm(false));
    }
    if (showNewAnnouncerPopUp) {
      dispatch(setShowNewAnnouncerPopUp(false));
    }
    if (showWorkTime) {
      dispatch(setShowWorkTime(false));
    }
  };

  const handleSubmitLoginForm = (fields) => {
    LoginAction({ ...fields });
  };

  const handleSubmitRestoreForm = (fields) => {
    RestoreAction(fields);
    toast.info(textRestore);
    closeAllModal();
  };

  const handleSubmitFeedbackForm = (fields) => {
    CreateFeedback(fields);
    hideFeedbackForm();
  };

  useEffect(() => {
    if (registerClient.status === STATUS.SUCCESS && showRegisterClientForm) {
      dispatch(setShowRegisterClientForm(false));
      toast.info(textRegister);
      RegisterClientInit();
    }
    // eslint-disable-next-line
  }, [registerClient.status]);

  useEffect(() => {
    if (
      registerAnnouncer.status === STATUS.SUCCESS &&
      showRegisterAnnouncerForm
    ) {
      dispatch(setShowRegisterAnnouncerForm(false));
      toast.info(textRegister);
      RegisterAnnouncerInit();
    }
    // eslint-disable-next-line
  }, [registerAnnouncer.status]);

  useEffect(() => {
    if (
      window.location.pathname === "/" &&
      typeof userProfile.profile !== "undefined" &&
      typeof userProfile.profile.announcer !== "undefined" &&
      typeof userProfile.profile.announcer.sounds !== "undefined" &&
      !userProfile.profile.announcer.sounds.length
    ) {
      showNewAnnouncerPopUpHandler();
    } else {
      hideShowNewAnnouncerPopUp();
    }
  }, [user.isAnnouncer, userProfile]);

  const handleSubmitClientForm = (fields) => {
    RegisterClientAction(fields);
  };

  const handleSubmitAnnouncerForm = (fields) => {
    RegisterAnnouncerAction(fields);
  };

  const registerClickHandler = (event, type = "client") => {
    if (event) {
      event.stopPropagation();
    }
    closeAllModal();
    if (type === "client") {
      dispatch(setShowRegisterClientForm(true));
    } else {
      dispatch(setShowRegisterAnnouncerForm(true));
    }
  };

  // const showWorkTimeHandler = useCallback(() => {
  //     closeAllModal();
  //     dispatch(setShowWorkTime(true));
  //   }, [closeAllModal, setShowWorkTime])

  const showNewAnnouncerPopUpHandler = useCallback(() => {
    closeAllModal();
    dispatch(setShowNewAnnouncerPopUp(true));
  }, [closeAllModal, setShowNewAnnouncerPopUp]);

  const showRestoreHandler = useCallback(() => {
    closeAllModal();
    dispatch(setShowRestoreForm(true));
  }, [closeAllModal, setShowRestoreForm]);

  const showLoginHandler = useCallback(() => {
    closeAllModal();
    dispatch(setShowLoginForm(true));
  }, [closeAllModal, setShowLoginForm]);

  const deleteConfirmationFormHandler = useCallback(() => {
    deleteProfileHandler(user.id);
    closeAllModal();

    dispatch(setShowConfirmationForm(true));
  }, [closeAllModal, setShowConfirmationForm]);

  const showFeedbackHandler = useCallback(() => {
    closeAllModal();
    dispatch(setShowFeedbackForm(true));
  }, [closeAllModal, setShowFeedbackForm]);

  const hideRegisterClientWindow = useCallback(() => {
    dispatch(setShowRegisterClientForm(false));
    window.location.hash = "";
  }, [setShowRegisterClientForm]);

  const hideShowLoginForm = useCallback(() => {
    dispatch(setShowLoginForm(false));
    window.location.hash = "";
  }, [setShowLoginForm]);

  const hideConfirmationForm = useCallback(() => {
    dispatch(setShowConfirmationForm(false));
  }, [setShowConfirmationForm]);

  const hideFeedbackForm = useCallback(() => {
    dispatch(setShowFeedbackForm(false));
    window.location.hash = "";
  }, [setShowFeedbackForm]);

  const hideShowRestoreForm = useCallback(() => {
    dispatch(setShowRestoreForm(false));
    window.location.hash = "";
  }, [setShowRestoreForm]);

  const hideShowNewAnnouncerPopUp = useCallback(() => {
    dispatch(setShowNewAnnouncerPopUp(false));
    window.location.hash = "";
  }, [setShowNewAnnouncerPopUp]);

  const hideShowWorkTime = useCallback(() => {
    dispatch(setShowWorkTime(false));
    window.location.hash = "";
  }, [setShowWorkTime]);

  const hideShowRegisterAnnouncerForm = useCallback(() => {
    dispatch(setShowRegisterAnnouncerForm(false));
    window.location.hash = "";
  }, [setShowRegisterAnnouncerForm]);

  if (!isInit) {
    return null;
  }

  const checkFavorites = (e) => {
    if (!favListCount) {
      e.preventDefault();
      toast.info("Раздел пуст. Отметьте понравившиеся голоса.");
    }
  };

  if (isLogin) {
    return (
      <>
        <HeaderPanel
          showWorkTime={showWorkTime}
          hideShowWorkTime={hideShowWorkTime}
          favCount={favListCount}
          checkFav={checkFavorites}
        />
        <Modal show={showFeedbackForm} onCloseRequest={hideFeedbackForm}>
          <FeedbackForm onSubmit={handleSubmitFeedbackForm} />
        </Modal>

        <Modal
          className={"new-announcer-pop-up-width"}
          show={showNewAnnouncerPopUp}
          onCloseRequest={hideShowNewAnnouncerPopUp}
        >
          <div className="pop-up-text">
            <span className="title-pop-up">Расскажите о ваших навыках </span>
            Для отображения вашей карточки в списке исполнителей – перейдите в
            Настройки профиля. Загрузите демо и корректно разметьте примеры
            ваших работ согласно навыкам. Укажите стоимость и расскажите об
            условиях записи, оборудовании. Аккуратно заполненный профиль
            позволяет проще найти вас потенциальному клиенту и оставляет
            приятное впечатление 😊🎙️
          </div>
          <div className="block-controls block-controls-column">
            <a className={"popup-btn popup-btn__green"} href="/profile/edit">
              Перейти в Найстройки профиля
            </a>
            <button
              className={"popup-btn popup-btn__gray"}
              onClick={() => {
                hideShowNewAnnouncerPopUp();
              }}
            >
              Закрыть это окно
            </button>
          </div>
        </Modal>

        <Modal
          show={showConfirmationForm}
          onCloseRequest={hideConfirmationForm}
          key="confirmationFormModal"
        >
          <div className="head-login">
            Вы действительно хотите удалить профиль?
          </div>
          <div className="block-controls">
            <button
              className={"popup-btn popup-btn__green"}
              onClick={() => {
                deleteConfirmationFormHandler();
              }}
            >
              Да
            </button>
            <button
              className={"popup-btn popup-btn__gray"}
              onClick={() => {
                hideConfirmationForm();
              }}
            >
              Нет
            </button>
          </div>
        </Modal>
        <ToastContainer />
        <Notifications />
      </>
    );
  }

  const recoveryText =
    registerClient.status === "FAIL_BUSY" ||
    registerAnnouncer.status === "FAIL_BUSY" ? (
      <span className="warning-text">
        {" "}
        Вы можете воспользоваться{" "}
        <span
          className={"restore-link"}
          onClick={() => {
            showRestoreHandler();
          }}
        >
          Восстановлением доступа
        </span>
      </span>
    ) : null;

  return (
    <>
      <Modal show={showFeedbackForm} onCloseRequest={hideFeedbackForm}>
        <FeedbackForm onSubmit={handleSubmitFeedbackForm} />
      </Modal>

      <Modal
        show={showLoginForm}
        onCloseRequest={hideShowLoginForm}
        addClassName="mini-remodal"
        key="loginFormModal"
      >
        <div className="head-login">Вход</div>
        <LoginForm onSubmit={handleSubmitLoginForm} />
        <div className="block-link">
          <button onClick={showRestoreHandler}>Восстановить доступ</button>
          <button onClick={(event) => registerClickHandler(event, "client")}>
            Регистрация
          </button>
        </div>
      </Modal>

      <Modal
        show={showRestoreForm}
        onCloseRequest={hideShowRestoreForm}
        addClassName="mini-remodal"
      >
        <div className="head-login">
          Восстановить пароль или удаленный профиль
        </div>
        <RestoreForm onSubmit={handleSubmitRestoreForm} />
        <div className="block-link">
          <button onClick={showLoginHandler}>Вход</button>
          <button onClick={(event) => registerClickHandler(event, "client")}>
            Регистрация
          </button>
        </div>
      </Modal>

      <Modal
        className="register-client"
        show={showRegisterClientForm}
        onCloseRequest={hideRegisterClientWindow}
      >
        <div className="head-registration">
          Регистрация <span>клиента</span>
        </div>
        <p className="head-description">
          Пишите исполнителям напрямую и <span>получайте звук!</span>
        </p>
        <RegisterClientForm
          onSubmit={handleSubmitClientForm}
          showRecovery={recoveryText}
        />
        <div className="block-link">
          <button onClick={showLoginHandler}>Вход</button>
          <button onClick={(event) => registerClickHandler(event, "announcer")}>
            Регистрация исполнителем
          </button>
        </div>
      </Modal>

      <Modal
        className="register-announcer"
        show={showRegisterAnnouncerForm}
        onCloseRequest={hideShowRegisterAnnouncerForm}
      >
        <div className="head-registration">
          Регистрация <span>исполнителя</span>
        </div>
        <RegisterAnnouncerForm
          onSubmit={handleSubmitAnnouncerForm}
          showRecovery={recoveryText}
        />
        <div className="block-link">
          <button onClick={showLoginHandler}>Вход</button>
          <button onClick={(event) => registerClickHandler(event, "client")}>
            Регистрация клиентом
          </button>
        </div>
      </Modal>

      <ul className="menu">
        <li>
          <a
            href="/favorite"
            className={"favorites-btn"}
            onClick={(e) => checkFavorites(e)}
          >
            <span className={"tablet-no"}>Избранные голоса</span>
            <div className={"tooltip tooltip__bottom"}>
              <div className="icon">
                <IconHeart />
                <span>Перейти&nbsp;в&nbsp;избранные&nbsp;голоса</span>
              </div>
            </div>
            <NotifyCount count={favListCount} />
          </a>
        </li>
        <li className="profile">
          <span className={"profile-user mobile-only"}>
            <img src={avatarImgDef} alt="" />
          </span>
          <ul className="profile-dropdown">
            <li>
              <button
                className="default no-border"
                onClick={() => dispatch(setShowLoginForm(true))}
              >
                Вход
              </button>
            </li>
            <li>
              <button
                className="green mobile-no"
                onClick={() => dispatch(setShowMenuRegister(!showMenuRegister))}
              >
                Регистрация
              </button>
              <ul className="menu-drop_menu">
                <li onClick={(event) => registerClickHandler(event, "client")}>
                  <span>Регистрация клиента</span>
                </li>
                <li
                  onClick={(event) => registerClickHandler(event, "announcer")}
                >
                  <span>Регистрация исполнителя</span>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>

      <ToastContainer />
      <Notifications />

      {!acceptedCookies && (
        <div className="cookies-fixed">
          <p className="cookies-text">
            Мы используем 🍪cookie, чтобы сделать сайт максимально удобным для
            вас.{" "}
            <a className="cookies-link" href="/cookie-policy">
              Подробнее
            </a>
          </p>
          <button onClick={handleAcceptCookies} className="cookies-button">
            Принять
          </button>
        </div>
      )}
    </>
  );
}

function mapStateToProps({
  auth,
  app,
  registerClient,
  registerAnnouncer,
  favLocalList,
  user,
  header,
}) {
  let favListCount =
    user.user && user.user.hasOwnProperty("favoritesCount")
      ? user.user.favoritesCount
      : favLocalList.favList.length;
  const profile = user.user || {
    newNotifications: { quantity: 0, messages: [] },
  };

  return {
    header: header,
    user: profile,
    profile: user.user,
    favList: favLocalList.favList || [],
    favListCount: user.isLoading ? 0 : favListCount,
    isInit: app.initialized,
    registerAnnouncer,
    registerClient,
    auth,
  };
}

export default connect(mapStateToProps, {
  LoginAction,
  initializeApp,
  RestoreAction,
  RegisterAnnouncerAction,
  RegisterClientInit,
  RegisterAnnouncerInit,
  setIsMobile,
  RegisterClientAction,
  CreateFeedback,
  CreateReviewService,
  getProfileLoad,
  setFavLocalList,
})(Header);

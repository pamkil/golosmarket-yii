<?php

namespace api\modules\v1\models\filters\user;

use api\modules\v1\models\records\auth\User as UserModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UserSearch extends Model
{
    public function search($requestParams)
    {
        $query = UserModel::find();
        $searchField = $requestParams['query'] ?? null;

        if ($searchField) {
            $query->andFilterWhere([
                'OR',
                ['ilike', 'username', $searchField['username'] ?? null],
                ['ilike', 'email', $searchField['username'] ?? null],
                ['status' => $searchField['status'] ?? null],
            ]);

        }

        return \Yii::createObject([
            'class' => ActiveDataProvider::class,
            'query' => $query,
            'pagination' => [
                'params' => $requestParams,
            ],
            'sort' => [
                'params' => $requestParams,
            ],
        ]);
    }
}

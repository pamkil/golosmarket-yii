import React, { useState, useEffect } from 'react';

const PaymentForm = ({sumValue, sumValueChange, createBillHandler, bill, cancelBillHandler}) => (
    <div className="payment-form">
        <div className="left">
            <div className="first padding-bottom padding-top">
                <input
                    type="number"
                    min={2}
                    step={10}
                    onChange={sumValueChange}
                    value={sumValue}
                    placeholder="Введите сумму"
                />
            </div>
            <div className="second">
                <button onClick={createBillHandler} className="green">{bill ? 'Обновить' : 'Выставить'} счет</button>
            </div>
        </div>
        <div className="right">
            Введите сумму и нажмите <b>выставить счет</b> после оплаты появиться уведомление и вы сможете оставить отзыв
            {
                bill
                    ? <div className="right-bottom"><button onClick={cancelBillHandler} className="red">Отозвать счет</button></div>
                    : ''
            }
        </div>
    </div>
);

const CreateBill = ({createBill, bill = null, cancelBill}) => {
    const [open, setOpen] = useState(false);
    const [sumValue, setSumValue] = useState(bill ? bill.sum : '');
    useEffect(() => {
        if (bill) {
            setSumValue(bill.sum);
        }
    }, [bill])

    const showHandler = () => setOpen(oldShow => !oldShow);
    const sumValueChangeHandler = event => {
        setSumValue(event.target.value);
    }

    const createBillHandler = () => {
        const newBill = { ...bill, sum: parseFloat(sumValue)}
        createBill(newBill);
    }

    const cancelBillHandler = () => {
        setOpen(false);
        cancelBill(bill);
    }

    return (
        <div className="payform">
            <div className="cursor" onClick={ showHandler }>
                {
                    bill
                    ? 'Вы уже выставили счет, нажмите, чтобы отредактировать или отменить'
                    : 'Нажмите, чтобы выставить счет'
                }
            </div>
            {
                open && <PaymentForm
                    createBillHandler={createBillHandler}
                    cancelBillHandler={cancelBillHandler}
                    sumValueChange={sumValueChangeHandler}
                    sumValue={sumValue}
                    bill={bill}
                />
            }
        </div>
    );
}

export default CreateBill;
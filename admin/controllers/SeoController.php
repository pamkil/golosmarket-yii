<?php

namespace admin\controllers;

use admin\models\setting\RedirectSearch;
use admin\models\setting\Robot;
use common\models\site\Redirect;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class SeoController extends SecurityController
{
    public function actionRobots()
    {
        $fileRobots = Yii::getAlias('@frontend/web/robots.txt');
        $post = Yii::$app->request->post();
        $model = new Robot();
        if ($post) {
            $result = null;
            if (
                $model->load($post, '')
                && $model->validate()
                && ($result = file_put_contents($fileRobots, $model->message)) !== false
            ) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');

                return $this->redirect(['/seo/robots']);
            }

            if ($result === false) {
                Yii::$app->session->addFlash('error', 'Ошибка сохранения файла на диск');
            }

            foreach ($model->getErrorSummary(true) as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        } elseif (file_exists($fileRobots)) {
            $message = file_get_contents($fileRobots);
            $model->message = $message;
        }

        $this->view->title = "Изменение robots.txt";
        $this->view->params['breadcrumbs'][] = $this->view->title;


        return $this->render(
            '_robots',
            [
                'model' => $model,
            ]
        );
    }

    public function actionIndex()
    {
        $searchModel = new RedirectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title,
        ];

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]
        );
    }

    public function actionCreate()
    {
        $model = new Redirect();

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post)) {
                $isValid = $model->validate() ;
                if ($isValid) {
                    $model->save();
                    Yii::$app->session->addFlash('success', 'Успешно создано');

                    return $this->redirect(
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    );
                }
            }

            foreach ($model->getErrorSummary(true) as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Создание';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список',
            'url' => ['index'],
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        //        $model->active = 1;

        return $this->render(
            '_form',
            [
                'model' => $model,
            ]
        );
    }

    public function actionUpdate($id)
    {
        /* @var Redirect $model */
        $model = Redirect::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException();
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post)) {
                $isValid = $model->validate();
                if ($isValid) {
                    $model->save();
                    Yii::$app->session->addFlash('success', 'Успешно обновлено');

                    return $this->redirect(
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    );
                }
            }

            foreach ($model->getErrorSummary(true) as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = "Изменение {$model->id}";
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список',
            'url' => ['index'],
        ];
        $this->view->params['breadcrumbs'][] = $model->id;

        return $this->render(
            '_form',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        /* @var Redirect $model */
        $model = Redirect::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException();
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect('index');
    }
}

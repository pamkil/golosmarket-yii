<?php

namespace api\modules\v1\models\records\site;

use common\models\announcer\File;

/**
 * @OA\Schema(
 *     schema="SiteReview",
 *     title="Отзыв"
 * )
 */
class Review extends \common\models\site\Review
{
    /**
     * @OA\Property(property="id", type="integer", description="ИД")
     * @OA\Property(property="user_id", type="integer", description="ИД клиента")
     * @OA\Property(property="rating", type="integer", description="Рейтинг")
     * @OA\Property(property="text", type="string", description="Текст отзыва")
     * @OA\Property(property="clientName", type="string", description="Имя клиента")
     * @OA\Property(property="clientAvatar", type="string", description="Аватар клиента")
     * @OA\Property(property="created_at", type="boolean", description="Время создания")
     * @OA\Property(property="updated_at", type="boolean", description="Время обновления")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'user_id',
            'clientName' => 'clientName',
            'clientAvatar' => 'clientAvatar',
            'rating',
            'text',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
        ];
    }

    public function getClientName()
    {
        return $this->client ? $this->client->firstname : '';
    }

    public function getClientAvatar()
    {
        return $this->client ? $this->client->getPhotoUrl() : File::getEmptyAvatarUrl();
    }

    public static function getShowQuantity(int $userId)
    {
        return static::find()
            ->select('show_quantity')
            ->where([
                'AND',
                ['user_id' => $userId],
                ['<=', 'show_quantity', self::MAX_SHOW]
            ])
            ->scalar();
    }
}

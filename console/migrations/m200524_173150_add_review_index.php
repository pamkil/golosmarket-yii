<?php

use yii\db\Migration;

/**
 * Class m200524_173150_add_review_index
 */
class m200524_173150_add_review_index extends Migration
{
    private $table = '{{%user_review}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('IX-user_review-updated_at', $this->table, 'updated_at');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('IX-user_review-updated_at', $this->table);
    }
}

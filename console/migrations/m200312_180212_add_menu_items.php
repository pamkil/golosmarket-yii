<?php

use common\models\content\Menu;
use yii\db\Migration;

/**
 * Class m200312_180212_add_menu_items
 */
class m200312_180212_add_menu_items extends Migration
{
    private $table = '{{%content_menu}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'parent_id', $this->integer()->null()->after('type_id'));
        $this->addColumn($this->table, 'position', $this->integer()->notNull()->defaultValue(0)->after('parent_id'));
        $this->truncateTable($this->table);
        $menu = [
            [
                'label' => 'Инфо',
                'url' => 'javascript:void(0);',
                'items' => [
                    [
                        'label' => 'Как заказать озвучку',
                        'url' => ['/how-order'],
                    ],
                    [
                        'label' => 'Дикторам',
                        'url' => ['/announcers'],
                    ],
                    [
                        'label' => 'Звук под ключ',
                        'url' => ['/sound'],
                    ],
                ],
            ],
            [
                'label' => 'Обратная связь',
                'url' => '#feedback',
                'items' => [],
            ],
        ];

        $position = 1;
        foreach ($menu as $m) {
            $this->insert($this->table, [
                'type_id' => Menu::TYPE_TOP,
                'path' => $m['url'],
                'title' => $m['label'],
                'position' => $position++,
            ]);

            $id = Menu::find()
                ->select(['id'])
                ->where([
                    'type_id' => Menu::TYPE_TOP,
                    'title' => $m['label'],
                    'position' => $position - 1,
                ])
                ->scalar();

            foreach ($m['items'] as $item) {
                $this->insert($this->table, [
                    'type_id' => Menu::TYPE_TOP,
                    'parent_id' => $id,
                    'path' => $item['url'][0],
                    'title' => $item['label'],
                    'position' => $position++,
                ]);
            }
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable($this->table);
        $this->dropColumn($this->table, 'parent_id');
        $this->dropColumn($this->table, 'position');
    }
}

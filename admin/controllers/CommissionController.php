<?php

namespace admin\controllers;

use common\models\bill\PayedSearch;
use common\models\site\Setting;
use common\models\user\Bill;
use Yii;
use yii\web\NotFoundHttpException;

class CommissionController extends SecurityController
{
    public function actionIndex()
    {
        $searchModel = new PayedSearch();
        $dataProvider = $searchModel->search();

        $this->view->title = 'Список оплат комиссии';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        $percent = (int)(Setting::getSetting('percent') ?? 30);

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'percent' => $percent,
            ]
        );
    }

    public function actionUpdate($id)
    {
        if (!Yii::$app->request->isPost) {
            throw new NotFoundHttpException;
        }

        $ids = explode(',', Yii::$app->request->post('ids'));
        $bills = Bill::updateAll(
            [
                'is_send' => true,
                'status' => Bill::STATUS_PAID_COMMISSION,
            ],
            [
                'id' => $ids,
                'status' => [Bill::STATUS_PAID_ANNOUNCER, Bill::STATUS_PAID_CLIENT],
                'is_payed' => true,
                'is_send' => false,
                'is_canceled' => false,
                'announcer_id' => $id
            ]
        );

        Yii::$app->session->addFlash('success', 'Успешно отмечено ' . $bills . ' счетов');

        return $this->redirect(['/commission/index']);
    }
}
<?php

namespace common\repositories;

use common\models\content\Collection;
use common\models\content\CollectionSound;
use common\repositories\contract\CollectionRepositoryContract;
use common\repositories\contract\CollectionSoundRepositoryContract;
use common\service\collection\enum\CollectionStatusEnum;

class CollectionSoundRepository implements CollectionSoundRepositoryContract
{
    public function getAll(): array
    {
        return CollectionSound::find()->all();
    }

    public function getById(int $collectionId, int $soundId): ?CollectionSound
    {
        return CollectionSound::find()->where([
            'collection_id' => $collectionId,
            'sound_id' => $soundId
        ])->one();
    }

    public function existsById(int $collectionId, int $soundId): bool
    {
        return CollectionSound::find()->where([
            'collection_id' => $collectionId,
            'sound_id' => $soundId
        ])->exists();
    }
}

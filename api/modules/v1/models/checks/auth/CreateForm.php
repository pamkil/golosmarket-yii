<?php

namespace api\modules\v1\models\checks\auth;

use api\modules\v1\models\records\announcer\Announcer;
use api\modules\v1\models\records\auth\User;
use common\service\Mail;
use yii\base\InvalidValueException;
use yii\base\Model;
use yii\db\Query;
use yii\web\BadRequestHttpException;

/**
 * @OA\Schema(required={"username", "password", "email"}, title="Форма создания пользователя")
 */
class CreateForm extends Model
{
    /**
     * @OA\Property(
     *     type="string",
     *     description="Имя или псевдоним",
     *     title="firstname",
     * )
     *
     * @var string
     */
    public $firstname;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Фамилия",
     *     title="lastname",
     * )
     *
     * @var string
     */
    public $lastname;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Пароль",
     *     title="password",
     * )
     *
     * @var string
     */
    public $password;

    /**
     * @OA\Property(
     *     type="string",
     *     description="email",
     *     title="email",
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Телефон в международном формате",
     *     title="phone",
     * )
     *
     * @var string
     */
    public $phone;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Тип пользователя: Диктор - announcer; Клиент - client",
     *     enum={"announcer", "client"},
     *     title="type"
     * )
     *
     * @var string
     */
    public $type;

    /**
     * @var User
     */
    private $_user;

    public function formName()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'password' => [
                ['password'],
                'required',
            ],
            [
                ['type', 'email'],
                'required',
            ],
            [
                ['email'],
                'email',
            ],
            [
                ['phone'],
                'safe',
            ],
            [
                ['phone'],
                'filter',
                'filter' => function ($value) {
                    $value = preg_replace('/[^0-9]/', '', $value);

                    preg_match('/(?<prefix>\+?7|8)?(?<normal>\d{10})/', $value, $matches);

                    if (empty($matches['normal'])) {
                        return '';
                    }

                    return '+7' . $matches['normal'];
                }
            ],
            [
                ['email'], 'checkUniqueEmail'
            ],
            [
                ['firstname', 'lastname'],
                'string',
                'max' => 50
            ],
            [
                ['type'],
                'in',
                'range' => array_keys(User::getStringTypes()),
            ],
        ];
    }

    public function checkUniqueEmail($attribute)
    {
        $exists = (new Query)->from(User::tableName())->where(['email' => $this->{$attribute}])->exists();
        if ($exists) {
            $this->addError('uniqueEmail', 'Email '.$this->{$attribute}.' уже занят.');
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }

    /**
     * Создаём пользователя в статусе INACTIVE,
     *
     * @return bool whether the creating new account was successful and email was sent
     * @throws \yii\base\Exception
     */
    public function signup()
    {
        if (!$this->validate()) {
            foreach ($this->getFirstErrors() as $key => $firstError) {
                throw new InvalidValueException($firstError);
            }
        }

        $this->_user = $user = new User();
        $user->load($this->attributes);
        $user->setAttribute('type', $user->getStringTypesToInt()[$this->type]);
        $user->setPassword($this->password);
        $user->setAttribute('status', User::STATUS_INACTIVE);
        $user->setAttribute('notify_email', 1);

        $user->generateAuthKey();
        $user->generateEmailVerificationToken();

        if (!($result = $user->save())) {
            throw new BadRequestHttpException('Не может добавить пользователя');
        }

        // создание токена авторизации
        $user->auth();

        if ($user->type === User::TYPE_ANNOUNCER) {
            $announcer = new Announcer(['user_id' => $user->id]);
            $result = $announcer->save();
        }

        return $result && $this->sendEmail($user);
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $array = [
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'email' => $this->email,
            'phone' => $this->phone,
            'type' => User::getStringTypesToInt()[$this->type] ?? User::TYPE_CLIENT,
        ];

        if (!empty($this->password)) {
            $array['password'] = $this->password;
        }

        return $array;
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Mail::sendSignupEmail($user->email, $user->firstname, $user->verification_token);
    }
}

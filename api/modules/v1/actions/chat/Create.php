<?php

namespace api\modules\v1\actions\chat;

use api\modules\v1\models\checks\chat\ChatForm;
use api\modules\v1\models\filters\user\ChatSearch;
use api\modules\v1\models\records\announcer\Opponent;
use api\modules\v1\models\records\user\Bill;
use api\modules\v1\models\records\user\Chat;
use common\models\access\User;
use common\models\user\SendNotify;
use common\service\chat\enum\FileTypeEnum;
use Yii;
use yii\rest\Action;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;

/**
 * @OA\Post(
 *      path="/chat/{id}",
 *      summary="Написать сообщение собеседнику",
 *      tags={"Chat"},
 *      @OA\RequestBody(
 *        required=true,
 *        @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *            schema="filechat",
 *            title="File in chat",
 *            required={"announcerId"},
 *            @OA\Property(
 *              property="files",
 *              type="array",
 *              @OA\Items(
 *                  @OA\Property(property="file", type="file"),
 *              ),
 *            ),
 *            @OA\Property(
 *              property="text",
 *              type="string"
 *            ),
 *            @OA\Property(
 *              property="clientId",
 *              type="integer"
 *            ),
 *            @OA\Property(
 *              property="parentId",
 *              type="integer"
 *            ),
 *            @OA\Property(
 *              property="isWriteClient",
 *              type="boolean"
 *            ),
 *          )
 *        )
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="Объект токена",
 *          @OA\JsonContent(ref="#/components/schemas/Chat")
 *      ),
 *      @OA\Response(
 *          response="400",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="401",
 *          description="Unauthorized",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */

/**
 * Class Create
 * @throws BadRequestHttpException
 * @package api\modules\v1\actions\chat
 *
 */
class Create extends Action
{
    public function run($id)
    {
        $postData = Yii::$app->request->post();

        $sender = User::findOne(Yii::$app->user->identity->id);

        $user = User::findOne($id);

        if (!empty($user->telegram_chat_id)) {
            $message = '✉️ Вам отправлен файл';

            if (isset($postData['text'])) {
                $message = "👤 " . $sender->lastname . " " . $sender->firstname . " написал(а) вам сообщение. Ответьте на golosmarket.ru";

                $postText = str_replace("&nbsp;", "", strip_tags($postData['text']));
                if (strlen($postText) < 4000) {
                    $message = "*" . $postText . "*" . "\n\n" . $message;
                }
            }

            Yii::$app->telegram->sendMessage([
                'parse_mode' => 'markdown',
                'chat_id' => $user->telegram_chat_id,
                'text' => $message,
                // 'reply_markup' => json_encode([
                //     'inline_keyboard' => [
                //         [
                //             ['text' => 'Я на связи', 'callback_data' => Json::encode(['text' => 'Я на связи', 'id' => $user->id, 'sender_id' => $sender->id])],
                //             ['text' => 'Буду рад', 'callback_data' => Json::encode(['text' => 'Буду рад', 'id' => $user->id, 'sender_id' => $sender->id])],
                //         ],
                //         [
                //             ['text' => 'Скоро отвечу', 'callback_data' => Json::encode(['text' => 'Скоро отвечу', 'id' => $user->id, 'sender_id' => $sender->id])],
                //             ['text' => 'Срок исполнения?', 'callback_data' => Json::encode(['text' => 'Срок исполнения?', 'id' => $user->id, 'sender_id' => $sender->id])],
                //         ],
                //         [
                //             ['text' => 'Жду задачу', 'callback_data' => Json::encode(['text' => 'Жду задачу', 'id' => $user->id, 'sender_id' => $sender->id])],
                //             ['text' => 'Нет, к сожалению', 'callback_data' => Json::encode(['text' => 'Нет, к сожалению', 'id' => $user->id, 'sender_id' => $sender->id])],
                //         ]
                //     ]
                // ])
            ]);
        }

        if (Yii::$app->request->post("edit")) {
            $row = (new \yii\db\Query())
                ->select(['announcer_id', 'client_id'])
                ->from('user_chat')
                ->where(['id' => Yii::$app->request->post("message_id")])
                ->limit(1)
                ->one();

            if (Yii::$app->user->identity->isAnnouncer) {
                if ((int)$row['announcer_id'] !== (int)Yii::$app->user->identity->id) {
                    throw new BadRequestHttpException("Вы не можете редактировать чужое сообщение");
                }
            } else {
                if ((int)$row['client_id'] !== (int)Yii::$app->user->identity->id) {
                    throw new BadRequestHttpException("Вы не можете редактировать чужое сообщение");
                }
            }

            Yii::$app->db->createCommand()
                ->update('user_chat',
                    ['text' => Yii::$app->request->post("text"), 'is_edit' => true],
                    ['id' => Yii::$app->request->post("message_id")]
                )->execute();

            return;
        }

        if (empty($id)) {
            throw new BadRequestHttpException("Нет данных кому написать");
        }

        $typeReceiver = User::getType((int)$id);

        if (!$typeReceiver) {
            throw new BadRequestHttpException("Не найден пользователь которому вы пишете");
        }


        /** @var User $user */
        $user = Yii::$app->user->identity;

        if ($user->type === $typeReceiver) {
            throw new BadRequestHttpException("Не можете написать этому пользователю");
        }

        if ($user->isAnnouncer) {
            $postData['announcer_id'] = $user->id;
            $postData['client_id'] = $id;
            $postData['is_write_client'] = false;
        } else {
            $postData['announcer_id'] = $id;
            $postData['client_id'] = $user->id;
            $postData['is_write_client'] = true;
        }

        $transaction = Yii::$app->db->beginTransaction();
        $outModels = [];
        try {
            $chatForm = new ChatForm();
            $chatForm->load($postData);
            $chatForm->files = UploadedFile::getInstancesByName('files');
            if ($chatForm->upload()) {
                $model = new Chat();
                $model->load($chatForm->toArray(), '');
                if (!empty($model->text)) {
                    $model->save();
                    $model->refresh();
                    $outModels[] = $model;
                }

                if ($files = $chatForm->getFileModels()) {
                    $chatForm->text = null;
                    foreach ($files as $fileModels) {
                        $file = $fileModels['file'];
                        $fileWatermark = $fileModels['watermark'];
                        if ($file->save() === false || ($fileWatermark && $fileWatermark->save() === false)) {
                            throw new BadRequestHttpException($file->errors);
                        }
                        $outModels[] = $model = new Chat();
                        $model->load($chatForm->toArray(), '');
                        $model->file_id = $file->id;
                        $model->file_type = $fileWatermark ? FileTypeEnum::FILE_TYPE_ORIGIN : FileTypeEnum::FILE_TYPE_PUBLIC;
                        $model->file_watermark_id = $fileWatermark ? $fileWatermark->id : null;
                        if ($model->save() === false) {
                            throw new BadRequestHttpException($model->errors);
                        }
                        $model->refresh();
                    }
                }

                $transaction->commit();

                SendNotify::read(Yii::$app->user->id, SendNotify::TYPE_SMS);

                $isAnnouncer = Yii::$app->user->identity->type === User::TYPE_ANNOUNCER;
                $announcerId = $isAnnouncer ? Yii::$app->user->id : $id;
                $clientId = $isAnnouncer ? $id : Yii::$app->user->id;

                $opponent = Opponent::findOne((int)$id);
                $bill = Bill::getNew($announcerId, $clientId);

                return [
                    'items' => (new ChatSearch())->search($id, Yii::$app->user->id, $isAnnouncer, 0, 50),
                    'opponent' => $opponent,
                    'bill' => $bill,
                    'unread' => Chat::getUnread($opponent->id)
                ];
            }
            throw new BadRequestHttpException(reset($chatForm->firstErrors));
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }

    // public function run($id)
    // {
    //     $postData = Yii::$app->request->post();
    //     $clientId = $postData['clientId'] ?? null;
    //     $parentId = $postData['parentId'] ?? null;
    //     $isWriteClient = $postData['isWriteClient'] ?? false;
    //     $createMessageDto = new CreateMessageDto([
    //         'senderId' => Yii::$app->user->identity->id,
    //         'userId' => (int)$id,
    //         'clientId' => (int)$clientId,
    //         'parentId' => (int)$parentId,
    //         'isWriteClient' => $isWriteClient === "true",
    //         'text' => $postData['text'] ?? null,
    //         'files' => UploadedFile::getInstancesByName('files'),
    //     ]);

    //     try
    //     {
    //         $service = Yii::$container->get(ChatService::class);
    //         return $service->createMessage($createMessageDto);
    //     }
    //     catch (\Exception $exception)
    //     {
    //         Yii::error($exception);
    //     }
    // }
}

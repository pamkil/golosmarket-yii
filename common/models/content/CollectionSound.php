<?php

namespace common\models\content;

use api\modules\v1\models\records\announcer\Announcer;
use api\modules\v1\models\records\announcer\AnnouncerSound;
use common\models\announcer\File;

/**
 * This is the model class for table "{{%content_collection_2_sound}}".
 *
 * @property int $collection_id
 * @property int $announcer_id
 * @property int $sound_id
 * @property int $position
 *
 * @property Collection $collection
 * @property Announcer $announcer
 * @property File $sound
 */
class CollectionSound extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%content_collection_2_sound}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['collection_id', 'announcer_id', 'sound_id'], 'required'],
            [['collection_id', 'announcer_id', 'sound_id'], 'integer'],
            ['position', 'safe'],
            [
                ['collection_id', 'sound_id'],
                'unique',
                'targetAttribute' => ['collection_id', 'sound_id']
            ],
            [
                ['collection_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Collection::class,
                'targetAttribute' => ['collection_id' => 'id']
            ],
            [
                ['announcer_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Announcer::class,
                'targetAttribute' => ['announcer_id' => 'id']
            ],
            [
                ['sound_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => File::class,
                'targetAttribute' => ['sound_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'collection_id' => 'ID подборки',
            'announcer_id' => 'ID диктора',
            'sound_id' => 'ID демки',
            'position' => 'Порядковое место в списке',
        ];
    }

    public function fields()
    {
        return [
            'announcer',
            'announcer_id',
            'sound' => 'soundView',
            'sound_id',
            'url' => function (CollectionSound $model) {
                return $model->sound->url;
            }
        ];
    }


    /**
     * Gets query for [[Collection]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCollection()
    {
        return $this->hasOne(Collection::class, ['id' => 'collection_id']);
    }

    /**
     * Gets query for [[Announcer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnnouncer()
    {
        return $this->hasOne(Announcer::class, ['id' => 'announcer_id']);
    }

    /**
     * Gets query for [[Sound]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSound()
    {
        return $this->hasOne(File::class, ['id' => 'sound_id']);
    }

    public function getSoundView()
    {
        return $this->hasOne(AnnouncerSound::class, [
            'announcer_id' => 'announcer_id',
            'sound_id' => 'sound_id'
        ]);
    }
}

import React, {useEffect, useState} from "react";
import {Field, getFormValues, reduxForm} from 'redux-form';
import {useSelector} from 'react-redux';
import classes from "classnames";
import {createTextMask} from 'redux-form-input-masks';

import {FORMS} from '../../../data/constants'

const days = [
    'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс',
];

const InputWithError = ({ input, type, placeholder = null, meta: { touched, error, warning }, ...params }) => (
    <input
        type={ type }
        placeholder={ placeholder }
        { ...params }
        { ...input }
        className={classes('time-input', {error: touched && error })}
    />
);

const validate = values => {
    const errors = {};

    days.forEach((i, index) => {
        const key = index + 1;
        const fieldAt = `time_${ key }_at`;
        const fieldTo = `time_${ key }_to`;
        if (values[fieldAt] && !/^([0-1]?[0-9]|2[0-3])-[0-5][0-9]$/.test(values[fieldAt])) {
            errors[fieldAt] = 'Неверный формат';
        }
        if (values[fieldTo] && !/^([0-1]?[0-9]|2[0-3])-[0-5][0-9]$/.test(values[fieldTo])) {
            errors[fieldTo] = 'Неверный формат';
        }
        if (values[fieldAt] && !values[fieldTo]) {
            errors[fieldTo] = 'Должен быть заполнен';
        }
        if (!values[fieldAt] && values[fieldTo]) {
            errors[fieldAt] = 'Должен быть заполнен';
        }
    })

    return errors;
};

const phoneMask = createTextMask({
    pattern: '29-59',
    guide: false,
    stripMask: false,
    maskDefinitions: {
        2: {
            regExp: /[0-2]/,
        },
        5: {
            regExp: /[0-5]/,
        },
        9: {
            regExp: /[0-9]/,
        },
    }
});


let WorkTimeForm = ({ handleSubmit, submitting, valid, change, dispatch, initialValues, key }) => {
    const [isSchedule, setIsSchedule] = useState((initialValues ? !initialValues.undefined : true));
    useEffect(() => {
        dispatch(change('undefined', !isSchedule));
        // eslint-disable-next-line
    }, []);
    const values = useSelector(state => getFormValues(FORMS.WORK_TIME)(state));
    const handleClickClear = event => {
        const field = event.target.dataset.name;
        const value = event.target.dataset.value;
        if (value === 'true') {
            dispatch(change(`${ field }_at`, '08-00'))
            dispatch(change(`${ field }_to`, '17-00'))
        } else {
            dispatch(change(`${ field }_at`, ''))
            dispatch(change(`${ field }_to`, ''))
        }
    }

    const scheduleChangeHandler = () => {
        setIsSchedule(current => {
            dispatch(change('undefined', current));
            return !current;
        });
    }

    return (
        <form onSubmit={ handleSubmit } className="modal-form time-schedule-form" key={ key }>
            <div className="block-time dline">
                <input type="radio" checked={ isSchedule } onChange={ scheduleChangeHandler }/>
                Показывать Онлайн по расписанию (по Москве)
            </div>
            <div className="block-week" id="week" style={ { display: isSchedule ? 'block' : 'none' } }>
                {
                    days.map((day, index) => {
                        const key = index + 1;
                        const active = isSchedule && (!values || (!values[`time_${ key }_to`] && !values[`time_${ key }_at`]))
                        return (
                            <div className="block-time" key={ key }>
                                <div>{ day }.</div>
                                <div>
                                    <Field
                                        name={ `time_${ key }_at` }
                                        component={ InputWithError }
                                        type="text"
                                        placeholder="10-00"
                                        inputMode="decimal"
                                        // pattern="[0-9-]*"
                                        noValidate=""
                                        autoCorrect="off"
                                        { ...phoneMask }
                                        autoComplete="time-start"
                                    />
                                    <Field
                                        name={ `time_${ key }_to` }
                                        component={ InputWithError }
                                        type="text"
                                        placeholder="17-30"
                                        inputMode="decimal"
                                        // pattern="[0-9-]*"
                                        noValidate=""
                                        autoCorrect="off"
                                        { ...phoneMask }
                                        autoComplete="time-end"
                                    />
                                </div>
                                <div>
                                    <button
                                        onClick={ handleClickClear }
                                        data-name={ `time_${ key }` }
                                        data-value={ active }
                                        type="button"
                                        className={ classes({ active }) }
                                    >
                                        Выходной
                                    </button>
                                </div>
                            </div>
                        );
                    })
                }
            </div>
            <div className="block-time dline">
                <input type="radio" checked={ !isSchedule } onChange={ scheduleChangeHandler }/>
                НЕДОСТУПЕН. Планирую длительный перерыв
            </div>
            <div className={ classes('block-resume', { active: !isSchedule }) } id="resume">
                <Field
                    name="undefined_text"
                    component="textarea"
                    type="textarea"
                    placeholder="В свободной форме расскажите до какого числа не в доступе. Это сообщение появится в вашем профиле вместо Графика работы"
                />
            </div>
            <div className="block-time dline">
                <button
                    type="submit"
                    disabled={ submitting }
                    className={ classes({'error-submit': !valid}) }
                >
                    Применить
                </button>
            </div>
        </form>
    );
}

WorkTimeForm = reduxForm({
    form: FORMS.WORK_TIME,
    enableReinitialize: true,
    validate
})(WorkTimeForm);

export default WorkTimeForm;

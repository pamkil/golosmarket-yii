<?php

namespace api\modules\v1\models\helpers;

use Carbon\Carbon;
use yii\db\Expression;

class DateHelper
{
    /**
     * Вернет фунцию для парсинга даты модели
     *
     * @param $model
     * @param $fieldName
     * @return \Closure
     */
    public static function parseRfc($model, $fieldName)
    {
        return function () use ($model, $fieldName) {
            if ($model->{$fieldName} instanceof Expression) {
                $model->refresh();
            }

            return Carbon::parse($model->{$fieldName})->toRfc3339String();
        };
    }
}

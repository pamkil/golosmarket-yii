<?php

use admin\models\bill\BillSearch;
use common\models\user\Bill;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;

//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;

/**
 * @var ActiveDataProvider $dataProvider
 * @var BillSearch $searchModel
 */
?>

<div class="box box-primary">
    <!--    <div class="box-header with-border text-right">-->
    <!--        <a href="--><? //= Url::to(['create']) ?><!--" class="btn bg-olive btn-flat margin">-->
    <!--            Добавить-->
    <!--            <i class="fa fa-plus"></i>-->
    <!--        </a>-->
    <!--    </div>-->
    <div class="box-header">
        <p>Прошло денег всего через проект: <b><?=Yii::$app->formatter->asCurrency($sum['total']) ?></b></p>
        <p>Заработали дикторы: <b><?=Yii::$app->formatter->asCurrency($sum['sumAn']) ?></b></p>
        <p>Заработал проект: <b><?=Yii::$app->formatter->asCurrency($sum['sum']) ?></b></p>
    </div>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                'expand' => [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'width' => '50px',
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    // 'detailUrl'=>Url::to(['/site/book-details']),
                    'detail' => function ($model, $key, $index, $column) {
                        return $this->render('ya_bill', [
                            'model' => $model,
                        ]);
                    },
                    'headerOptions' => ['class' => 'kartik-sheet-style'],
                    'expandOneOnly' => true,
                    'pageSummary' => 'Итого',
                    'pageSummaryOptions' => ['colspan' => 5],
                ],
                [
                    'class' => '\kartik\grid\CheckboxColumn'
                ],
                'id',
                [
                    'label' => 'Диктор',
                    'attribute' => 'announcer',
                    'content' => function ($bill) {
                        if (!$bill->announcer) {
                            return '';
                        }

                        return "<span title='$bill->announcer_id'>{$bill->announcer->lastname} {$bill->announcer->firstname} ({$bill->announcer->email})</span>";
                    },
                ],
                [
                    'label' => 'Заказчик',
                    'attribute' => 'client',
                    'content' => function ($bill) {
                        if (!$bill->client) {
                            return '';
                        }

                        return "<span title='$bill->client_id'>{$bill->client->lastname} {$bill->client->firstname} ({$bill->client->email})</span>";
                    },
                ],
                [
                    'label' => 'Статус',
                    'attribute' => 'status',
                    'filter' => Bill::getStatuses(),
                    'filterInputOptions' => ['prompt' => 'Все', 'class' => 'form-control', 'id' => null],
                    'value' => function ($bill) {
                        return Bill::getStatuses()[$bill->status];
                    },
                ],
                'sum:currency',
                'percent',
                'is_canceled:boolean',
                'is_payed:boolean',
                'is_send:boolean',
                'date' => [
                    'class' => 'kartik\grid\DataColumn',
                    'attribute' => 'date',
                    'format' => 'date',
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'hAlign' => GridView::ALIGN_CENTER,
                    'width' => '180px',
                    'filter' => DatePicker::widget(\yii\helpers\ArrayHelper::merge([
                        'model' => $searchModel,
                        'attribute' => 'date_at',
                        'attribute2' => 'date_to',
                        'separator' => '-',
                        'type' => DatePicker::TYPE_RANGE,
                        'options' => ['placeholder' => 'От'],
                        'options2' => ['placeholder' => 'До', 'autocomplete' => 'off'],
                    ], Yii::$app->params['datepicker_options'])),
                ],

//                [
//                    'class' => ActionColumn::class,
//                    'template' => '{update}',
//                    'buttonOptions' => ['class' => 'margin']
//                ],
            ]
        ]); ?>
    </div>
</div>

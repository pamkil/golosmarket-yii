<?php

use yii\db\Migration;

/**
 * Class m200428_164522_announcer_rating
 */
class m200428_164522_announcer_rating extends Migration
{
    private $announcer = '{{%user_announcer}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn($this->announcer, 'likes');
        $this->dropColumn($this->announcer, 'dislikes');

        $this->dropColumn($this->announcer, 'quantity_reviews');
        $this->addColumn($this->announcer, 'rating', $this->decimal(3, 1)->after('discount'));
        $this->addColumn($this->announcer, 'quantity_reviews', $this->integer()->after('rating'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->announcer, 'rating');
        $this->dropColumn($this->announcer, 'quantity_reviews');
        $this->addColumn($this->announcer, 'quantity_reviews', $this->decimal(3, 1)->after('user_id'));

        $this->addColumn($this->announcer, 'likes', $this->integer()->notNull()->defaultValue(0)->after('discount'));
        $this->addColumn($this->announcer, 'dislikes', $this->integer()->notNull()->defaultValue(0)->after('likes'));
    }
}

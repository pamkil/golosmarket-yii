<?php

namespace admin\models\content;

use Carbon\Carbon;
use common\models\content\News;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class NewsSearch extends Model
{
    public $title;
    public $date;
    public $h1;
    public $active;

    public function rules()
    {
        return [
            [
                ['title', 'date', 'h1'],
                'string'
            ],
            [
                ['active',],
                'boolean'
            ],
        ];
    }

    public function search($params)
    {
        $query = News::find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'active',
                        'date',
                        'title',
                        'h1',
                    ]
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $dataProvider->sort->defaultOrder = ['date' => SORT_DESC,];

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        if ($this->date) {
            $this->date = Carbon::parse($this->date)->format('Y-m-d');
        }

        $query->andFilterWhere(
            [
                'AND',
                ['like', 'title', $this->title],
                ['like', 'h1', $this->h1],
                ['active' => $this->active],
            ]
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
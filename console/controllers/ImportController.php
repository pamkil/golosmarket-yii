<?php

namespace console\controllers;

use api\modules\v1\models\records\auth\User;
use Carbon\Carbon;
use common\models\announcer\Age;
use common\models\announcer\Announcer;
use common\models\announcer\AnnouncerSound;
use common\models\announcer\File;
use common\models\announcer\Language;
use common\models\announcer\Presentation;
use common\models\content\Collection;
use common\models\content\CollectionSound;
use common\models\content\PageAccordeon;
use common\models\user\Bill;
use common\models\user\Chat;
use common\models\user\Favorite;
use common\models\user\Review;
use console\models\ReviewImport;
use console\models\UserImport;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Connection;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\FileHelper;

/**
 * Page controller
 */
class ImportController extends Controller
{
    public $pathSound = 'oldFiles';

    private $tables = [
        'a_admins' => 'cmsa22_a_admins',
        'a_amenu' => 'cmsa22_a_amenu',
        'a_cfieldtypes' => 'cmsa22_a_cfieldtypes',
        'a_ctpls' => 'cmsa22_a_ctpls',
        'a_fieldtypes' => 'cmsa22_a_fieldtypes',
        'a_params' => 'cmsa22_a_params',
        'a_tpls' => 'cmsa22_a_tpls',
        'a_tplsfields' => 'cmsa22_a_tplsfields',
        'a_urls' => 'cmsa22_a_urls',
        'a_validator' => 'cmsa22_a_validator',
        'b_banner' => 'cmsa22_b_banner',
        'b_chat' => 'cmsa22_b_chat',
        'b_diktor' => 'cmsa22_b_diktor',
        'b_dop' => 'cmsa22_b_dop',
        'b_favorites' => 'cmsa22_b_favorites',
        'b_find' => 'cmsa22_b_find',
        'b_help' => 'cmsa22_b_help',
        'b_history' => 'cmsa22_b_history',
        'b_lang' => 'cmsa22_b_lang',
        'b_news' => 'cmsa22_b_news',
        'b_nps' => 'cmsa22_b_nps',
        'b_nps_history' => 'cmsa22_b_nps_history',
        'b_old' => 'cmsa22_b_old',
        'b_online' => 'cmsa22_b_online',
        'b_payments' => 'cmsa22_b_payments',
        'b_photo' => 'cmsa22_b_photo',
        'b_portfolio' => 'cmsa22_b_portfolio',
        'b_registry' => 'cmsa22_b_registry',
        'b_reviews' => 'cmsa22_b_reviews',
        'b_settings' => 'cmsa22_b_settings',
        'b_sex' => 'cmsa22_b_sex',
        'b_slider' => 'cmsa22_b_slider',
        'b_sounds' => 'cmsa22_b_sounds',
        'b_tagged' => 'cmsa22_b_tagged',
        'b_tembr' => 'cmsa22_b_tembr',
        'b_text' => 'cmsa22_b_text',
        'b_viewcount' => 'cmsa22_b_viewcount',
        'c_banner' => 'cmsa22_c_banner',
        'c_contacts' => 'cmsa22_c_contacts',
        'c_diktors' => 'cmsa22_c_diktors',
        'c_find' => 'cmsa22_c_find',
        'c_first' => 'cmsa22_c_first',
        'c_help' => 'cmsa22_c_help',
        'c_history' => 'cmsa22_c_history',
        'c_map' => 'cmsa22_c_map',
        'c_menu' => 'cmsa22_c_menu',
        'c_news' => 'cmsa22_c_news',
        'c_photo' => 'cmsa22_c_photo',
        'c_pref' => 'cmsa22_c_pref',
        'c_registry' => 'cmsa22_c_registry',
        'c_slider' => 'cmsa22_c_slider',
        'c_sounds' => 'cmsa22_c_sounds',
        'c_spravka' => 'cmsa22_c_spravka',
        'c_text' => 'cmsa22_c_text',
    ];

    /** @var Connection */
    private $dbOld;

    public function init()
    {
        parent::init();
        $this->dbOld = Yii::$app->db2;
    }

    /**
     * Displays homepage.
     *
     */
    public function actionIndex()
    {
//        $this->importAnnouncers();
//        $this->closeConnection();
//
//        $this->importReviews();
//        $this->closeConnection();
//
//        $this->importFavorites();
//        $this->closeConnection();
//
//        $this->calculateFavorite();
//        $this->closeConnection();

//        $this->importSounds();
//        $this->closeConnection();

//        $this->importImages();
//        $this->closeConnection();

//        $this->importChat();
//        $this->closeConnection();
//
//        $this->importBills();
//        $this->closeConnection();
//
//        $this->addUserToAdmin();
//        $this->closeConnection();
    }

    public function actionSoundsLanguage()
    {
        $languagesComparison = [
            'russian' => 'ru',
            'ukrainian' => 'uk',
            'belorussian' => 'be',
            'kazakh' => 'kk',
            'kyrgyz' => 'ky',
            'english' => 'en',
            'deutsch' => 'de',
            'spanish' => 'es',
            'italian' => 'it',
            'chinese' => 'zh',
            'french' => 'fr',
            'tajik' => 'tg',
            'bulgarian' => 'bg',
            'polish' => 'pl',
            'greek' => 'el',
            'tatar' => 'tt',
            'japanese' => 'ja',
            'georgian' => 'ka',
        ];

        $sounds = (new Query)->select('*')
            ->from(['as' => AnnouncerSound::tableName()])
            ->where(['as.lang_code' => null])
            ->all();

        $announcerLanguages = (new Query)->select('*')
            ->from(['al' => '{{%user_announcer_2_languages}}'])
            ->innerJoin(['l' => Language::tableName()],'al.language_id = l.id')
            ->orderBy(['al.announcer_id' => SORT_ASC, 'l.position' => SORT_ASC])
            ->groupBy(['al.announcer_id'])
            ->indexBy('announcer_id')
            ->all();

        $soundsCount = count($sounds);
        $i = 0;
        $soundsPrefix = 'Sounds: ';
        Console::startProgress($i, $soundsCount, $soundsPrefix);

        foreach ($sounds as $sound) {
            if (array_key_exists($sound['announcer_id'], $announcerLanguages)) {
                $lang = $announcerLanguages[$sound['announcer_id']];
                $langCode = $languagesComparison[$lang['code']];
            }

            $updatedSound = AnnouncerSound::find()
                ->where([
                    'AND',
                    ['announcer_id' => $sound['announcer_id']],
                    ['sound_id' => $sound['sound_id']]
                ])->one();

            $updatedSound->setAttribute('lang_code', $langCode ?? null);
            $updatedSound->update(false);
            Console::updateProgress(++$i, $soundsCount, $soundsPrefix);
        }

        Console::endProgress();
        return ExitCode::OK;
    }

    public function actionSoundsPresentation($name = 'other')
    {
        $sounds = (new Query)->select('*')
            ->from(['as' => AnnouncerSound::tableName()])
            ->where(['as.presentation_id' => null])
            ->all();

        $defaultPresentation = (new Query)->select('*')
            ->from(['ap' => Presentation::tableName()])
            ->where(['ap.code' => $name])
            ->one();

        if (empty($defaultPresentation)) {
            Console::error('This name not found in list of presentations');
            return ExitCode::DATAERR;
        }

        $soundsCount = count($sounds);
        $i = 0;
        $soundsPrefix = 'Sounds: ';
        Console::startProgress($i, $soundsCount, $soundsPrefix);

        foreach ($sounds as $sound) {
            $updatedSound = AnnouncerSound::find()
                ->where([
                    'AND',
                    ['announcer_id' => $sound['announcer_id']],
                    ['sound_id' => $sound['sound_id']]
                ])->one();

            $updatedSound->setAttribute('presentation_id', $defaultPresentation['id']);
            $updatedSound->update(false);
            Console::updateProgress(++$i, $soundsCount, $soundsPrefix);
        }

        Console::endProgress();
        return ExitCode::OK;
    }

    public function actionSoundsAge()
    {
        $sounds = (new Query())->select('*')
            ->from(['as' => AnnouncerSound::tableName()])
            ->where(['as.age_id' => null])
            ->all();

        $announcers = (new Query())->select([
            'a.id as announcer_id',
            'aa.id as age_id',
        ])
            ->from(['a' => Announcer::tableName()])
            ->innerJoin(
                ['a2a' => 'user_announcer_2_age'],
                'a2a.announcer_id = a.id'
            )
            ->leftJoin(
                ['aa' => Age::tableName()],
                'aa.id = a2a.age_id'
            )
            ->all();

        $announcersAge = [];
        foreach ($announcers as $announcer) {
            $announcersAge[(int) $announcer['announcer_id']][] = (int) $announcer['age_id'];
        }

        $soundsCount = count($sounds);
        $i = 0;
        $soundsPrefix = 'Sounds: ';
        Console::startProgress($i, $soundsCount, $soundsPrefix);

        foreach ($sounds as $sound) {
            $announcerAges = null;

            if (!empty($announcersAge[$sound['announcer_id']])) {
                $announcerAges = $announcersAge[$sound['announcer_id']];

                if (count($announcerAges) === 1) {
                    $updatedSound = AnnouncerSound::find()
                        ->where([
                            'AND',
                            ['announcer_id' => $sound['announcer_id']],
                            ['sound_id' => $sound['sound_id']]
                        ])->one();
                    $updatedSound->setAttribute('age_id', $announcerAges[0]);
                    $updatedSound->update(false);
                }
            }

            Console::updateProgress(++$i, $soundsCount, $soundsPrefix);
        }

        Console::endProgress();
        return ExitCode::OK;
    }

    public function actionSections()
    {
        $testDb = Yii::$app->db3;

        $accordeons = (new Query())->select('*')->from(PageAccordeon::tableName())
            ->all($testDb);

        $i = 0;
        $aCount = count($accordeons);
        Console::startProgress($i, $aCount, 'Accordeons: ');
        foreach ($accordeons as $accordeon) {
            $accordeonNew = new PageAccordeon([
                'page_id' => $accordeon['page_id'],
                'title' => $accordeon['title'],
                'body' => $accordeon['body'],
            ]);
            $accordeonNew->save();
            Console::updateProgress(++$i, $aCount);
        }
        Console::endProgress();

        $collections = (new Query())->select('*')->from(Collection::tableName())
            ->all($testDb);

        $j = 0;
        $collectionsCount = count($collections);
        Console::startProgress($j, $collectionsCount, 'Collections: ');
        foreach ($collections as $collection) {
            $collectionNew = new Collection([
                'name' => $collection['name'],
                'image_id' => $collection['image_id'],
                'description' => $collection['description'],
                'active' => $collection['active'],
                'position' => $collection['position'],
            ]);
            $collectionNew->save();
            Console::updateProgress(++$j, $collectionsCount);
        }
        Console::endProgress();

        $collectionSounds = (new Query())->select('*')->from(CollectionSound::tableName())
            ->all($testDb);

        $y = 0;
        $collectionSoundsCount = count($collectionSounds);
        Console::startProgress($y, $collectionSoundsCount, 'CollectionSounds: ');
        foreach ($collectionSounds as $collectionSound) {
            $collectionSoundNew = new CollectionSound([
                'collection_id' => $collectionSound['collection_id'],
                'announcer_id' => $collectionSound['announcer_id'],
                'sound_id' => $collectionSound['sound_id'],
                'position' => $collectionSound['position'],
            ]);
            $collectionSoundNew->save();
            Console::updateProgress(++$y, $collectionSoundsCount);
        }
        Console::endProgress();

        return ExitCode::OK;
    }

    private function importAnnouncers()
    {
        Console::stdout('Import Announcers');
        foreach ($this->getDataByTable($this->tables['b_diktor']) as $item) {
            $user = new UserImport();
            $user->setData($item);
            $user->run();
        }
    }

    private function closeConnection()
    {
        Yii::$app->db->close();
        $this->dbOld->close();
    }

    private function importReviews()
    {
        Console::stdout('Import Reviews');
        Yii::$app->db->createCommand()->delete(Review::tableName())->execute();
        foreach ($this->getDataByTable($this->tables['b_reviews']) as $item) {
            $review = new ReviewImport();
            $review->setData($item);
            $review->run();
        }
        $announcersId = Announcer::find()->select('user_id')->column();
        foreach ($announcersId as $userId) {
            $reviews = Review::find()
                ->where(['announcer_id' => $userId])
                ->asArray()
                ->all();
            $reviews = ArrayHelper::index($reviews, 'id', 'rating');
            $quantityAll = 0;
            $sum = 0;
            foreach ($reviews as $start => $items) {
                $quantity = count($items);
                $sum += (int)$start * $quantity;
                $quantityAll += $quantity;
            }

            $result = 0;
            if ($quantityAll !== 0) {
                $result = (float)$sum / (float)$quantityAll;
            }
            $result = round($result, 1);

            $countFavorites = Favorite::find()
                ->where(['announcer_id' => $userId])
                ->count();

            User::updateAll(
                [
                    'rating' => $result,
                    'quantity_reviews' => $quantityAll,
                    'quantity_favorite' => $countFavorites,
                ],
                ['id' => $userId]
            );
        }
    }

    private function importFavorites()
    {
        Console::stdout('Import Favorites');
        Yii::$app->db->createCommand()->delete(Favorite::tableName())->execute();
        $users = ReviewImport::getUsers();

        foreach ($this->getDataByTable($this->tables['b_favorites']) as $item) {
            $announcerId = $users[$item['stared']] ?? null;
            $clientId = $users[$item['did']] ?? null;
            if (!$announcerId || !$clientId) {
                continue;
            }
            $favorite = new Favorite();
            $favorite->announcer_id = $announcerId;
            $favorite->client_id = $clientId;
            $favorite->save();
        }
    }

    private function calculateFavorite()
    {
        Console::stdout('Calculate Favorites');
        $users = User::find()
            ->select(['id'])
            ->where(['type' => User::TYPE_ANNOUNCER])
            ->indexBy('id')
            ->column();

        foreach ($users as $announcerId) {
            Favorite::calculate($announcerId);
        }
    }

    private function importSounds()
    {
        Console::stdout('Import Sounds');
        //cmsa22_b_sounds
        $users = ReviewImport::getUsers();
        $announcers = Announcer::find()
            ->select(['id', 'user_id'])
            ->indexBy('user_id')
            ->column();

        $anIds = [];
        foreach ($users as $oldId => $newId) {
            if (!isset($announcers[$newId])) {
                continue;
            }
            $anIds[$oldId] = $announcers[$newId];
        }

        unset($announcers);

        $inserts = [];
        $path = Yii::getAlias('@runtimeRoot') . DIRECTORY_SEPARATOR . $this->pathSound . DIRECTORY_SEPARATOR;
        $otherPresentId = Presentation::find()->where(['code' => 'other'])->select('id')->scalar();
        foreach ($this->getDataByTable($this->tables['b_sounds']) as $item) {
            $userId = $anIds[$item['diktor']] ?? null;
            if (!$userId) {
                continue;
            }
            $fileFullName = $path . $item['file'];

            if (!file_exists($fileFullName)) {
                continue;
            }

            $file = new File();
            $file->name = mb_substr(trim($item['name']), 0, 63);
            $file->filename = sha1_file($fileFullName);
            $file->type = FileHelper::getMimeType($fileFullName);// 'audio/mpeg';
            $file->ext = strtolower(pathinfo($fileFullName, PATHINFO_EXTENSION));;//'mp3';
            $file->size = filesize($fileFullName);
            $file->default = $item['inmine'];
            $file->setPath();
            $result = $file->save();
            echo "{$fileFullName} -> {$file->getFullPath()} \n";
            if (!file_exists($file->getPathUpload())) {
                mkdir($file->getPathUpload(), 0777, true);
            }

            copy($fileFullName, $file->getFullPath());
            if (!$result) {
                dd($file->errors);
            }
            $userId = $anIds[$item['diktor']];
            $presentationId = null;

            $inserts[] = [$userId, $file->id, $item['inmine'] == '1' ? $presentationId : $otherPresentId];
            /** TODO add copy file */
        }

        Yii::$app->db->createCommand()->batchInsert(
            'user_announcer_2_sound',
            ['announcer_id', 'sound_id', 'presentation_id'],
            $inserts
        )->execute();
    }

    private function importImages()
    {
        Console::stdout('Import Images');
        //cmsa22_b_diktor
        $users = ReviewImport::getUsers();

        $path = Yii::getAlias('@runtimeRoot') . DIRECTORY_SEPARATOR . $this->pathSound . DIRECTORY_SEPARATOR;

        foreach ($this->getDataByTable($this->tables['b_diktor']) as $item) {
            $userId = $users[$item['id']] ?? null;
            if (!$userId || empty($item['avatar']) || $item['avatar'] == 'guest.jpg') {
                continue;
            }
            $fileFullName = $path . $item['avatar'];

            if (!file_exists($fileFullName)) {
                continue;
            }

            $file = new File();
            $file->name = mb_substr(trim($item['avatar']), 0, 63);
            $file->filename = sha1_file($fileFullName);
            $file->type = FileHelper::getMimeType($fileFullName);// 'audio/mpeg';
            $file->ext = strtolower(pathinfo($fileFullName, PATHINFO_EXTENSION));;//'mp3';
            $file->size = filesize($fileFullName);
            $file->setPath();
            $result = $file->save();
            echo "{$fileFullName} -> {$file->getFullPath()} \n";
            if (!file_exists($file->getPathUpload())) {
                mkdir($file->getPathUpload(), 0777, true);
            }

            copy($fileFullName, $file->getFullPath());
            if (!$result) {
                dd($file->errors);
            }
            User::updateAll(
                ['photo_id' => $file->id],
                ['id' => $userId]
            );
        }
    }

    private function importChat()
    {
        Console::stdout('Import Chat');
        //cmsa22_b_sounds
        Yii::$app->db->createCommand()->delete(Chat::tableName())->execute();
        $users = ReviewImport::getUsers();

        $typeUsers = User::find()
            ->select(['type', 'id'])
            ->indexBy('id')
            ->column();

        $inserts = [];
        foreach ($this->getDataByTable($this->tables['b_chat']) as $item) {
            $fromUserId = (int)($users[$item['fromid']] ?? 0);
            $toUserId = (int)($users[$item['toid']] ?? 0);

            if (!$fromUserId || !$toUserId) {
                d('not found', $item['fromid'], $item['toid']);
                continue;
            }

            if (mb_strpos($item['text'], 'время ответа 5-10 минут') !== false) {
                d('bot message', strip_tags($item['text']));
                continue;
            }

            if ($typeUsers[$fromUserId] == User::TYPE_CLIENT && $typeUsers[$toUserId] == User::TYPE_ANNOUNCER) {
                $clientId = $fromUserId;
                $anId = $toUserId;
                $isWriteClient = true;
            } elseif ($typeUsers[$toUserId] == User::TYPE_CLIENT && $typeUsers[$fromUserId] == User::TYPE_ANNOUNCER) {
                $clientId = $toUserId;
                $anId = $fromUserId;
                $isWriteClient = false;
            } else {
                d('none announcer', $item['fromid'], $item['toid']);
                continue;
            }

            $text = trim(strip_tags(str_replace('<br />', PHP_EOL, $item['text'])));
            if (empty($text)) {
                d('empty text');
                continue;
            }

            $chat = new Chat(['isImport' => true]);
            $fileId = null;
            if (!empty($item['file'])) {
                $fileId = $this->addFile($item['file'])->id;
            }

            $chat->load(
                [
                    'announcer_id' => $anId,
                    'client_id' => $clientId,
                    'file_id' => $fileId,
                    'text' => $text,
                    'is_write_client' => $isWriteClient,
                    'is_read' => true,//$item['send'],
                    'is_send' => $item['send'],
                ],
                ''
            );
            $chat->created_at = $chat->updated_at = $item['data'];

            $result = $chat->save();
            if (!$result) {
                dd($chat->errors);
            }
            if ($chat->id) {
                Chat::updateAll(
                    [
                        'created_at' => $item['data'],
                        'updated_at' => $item['data'],
                    ],
                    ['id' => $chat->id]
                );
            }
        }

        Yii::$app->db->createCommand()
            ->batchInsert(
                'user_announcer_2_sound',
                ['announcer_id', 'sound_id'],
                $inserts
            )
            ->execute();
    }

    private function importBills()
    {
        //cmsa22_b_sounds
        Console::stdout('Import Bills');
        Yii::$app->db->createCommand()->delete(Bill::tableName())->execute();
        $users = ReviewImport::getUsers();

        $typeUsers = User::find()
            ->select(['type', 'id'])
            ->indexBy('id')
            ->column();

        foreach ($this->getDataByTable($this->tables['b_payments']) as $item) {
            $fromUserId = (int)($users[$item['fromid']] ?? 0);
            $toUserId = (int)($users[$item['toid']] ?? 0);

            if (!$fromUserId || !$toUserId) {
                d('not found', $item['fromid'], $item['toid']);
                continue;
            }

            if ($typeUsers[$fromUserId] == User::TYPE_CLIENT && $typeUsers[$toUserId] == User::TYPE_ANNOUNCER) {
                $clientId = $fromUserId;
                $anId = $toUserId;
                $isWriteClient = true;
            } elseif ($typeUsers[$toUserId] == User::TYPE_CLIENT && $typeUsers[$fromUserId] == User::TYPE_ANNOUNCER) {
                $clientId = $toUserId;
                $anId = $fromUserId;
                $isWriteClient = false;
            } else {
                d('none announcer', $item['fromid'], $item['toid']);
                continue;
            }

            if ($isWriteClient) {
                dd('isWriteClient', $item);
            }

            $newBill = new Bill(['isImport' => true]);
            $newBill->announcer_id = $anId;
            $newBill->client_id = $clientId;
            $newBill->sum = ($item['sum'] ?? 0) > 999999 ? -1 : $item['sum'];
            $newBill->created_at = $newBill->updated_at = $newBill->date = $item['data'];
            $newBill->percent = $item['percent'];
            $item['status'] = (int)$item['status'];
            $item['status_d'] = (int)$item['status_d'];

            if (!empty($item['operation_id'])) {
                $newBill->ya_operation_id = $item['operation_id'];
                if (!empty($item['datatime'])) {
                    $newBill->ya_datetime = Carbon::parse($item['datatime'])->toDateTimeString();
                }
                $newBill->ya_sha1_hash = $item['sha1_hash'];
                $newBill->ya_amount = str_replace(',', '.', $item['amount']);
                $newBill->ya_withdraw_amount = $item['withdraw_amount'];
                $newBill->ya_label = $item['label'];
                $newBill->ya_currency = 643;
            } elseif ($item['status'] > 1) {
                $item['status'] = 3;
            }

            if ($item['status_d'] === 5 || $item['status_d'] === 4) {
                $newBill->status = Bill::STATUS_PAID_ANNOUNCER;
            } elseif ($item['status_d'] === 3) {
                $newBill->status = Bill::STATUS_CANCEL_ANNOUNCER;
            } elseif ($item['status_d'] === 2) {
                $newBill->status = Bill::STATUS_PAID_CLIENT;
            } else {
                $newBill->status = Bill::STATUS_NEW;
            }
//
//
//            switch ($item['status']) {
//                case 1:
//                    $newBill->status = Bill::STATUS_NEW;
//                    break;
//
//                case 4:
//                case 2:
//                    $newBill->ya_success = true;
//                    $newBill->is_payed = true;
//                    $newBill->status = Bill::STATUS_PAID_CLIENT;
//                    if ($item['status_d'] == 5) {
//                        $newBill->status = Bill::STATUS_PAID_ANNOUNCER;
//                    }
//                    break;
//
//                case 3:
//                    $newBill->is_canceled = true;
//                    $newBill->is_payed = false;
//                    break;
//            }

//            $newBill->ya_success = $item[''];
//            $newBill-> = $item[''];
//main.php?users&finance&newresult=1543
//
//            if ($ast['status_d'] == 1) {
//                $order_d = "счет выставлен";
//            }
//            if ($ast['status_d'] == 2) {
//                $order_d = "счет оплачен";
//            }
//            if ($ast['status_d'] == 3) {
//                $order_d = "счет отвергнут";
//            }
//            if ($ast['status_d'] == 4) {
//                $order_d = "счет в архиве";
//            }
//            if ($ast['status_d'] == 5) {
//                $order_d = "Деньги перечислены исполнителю";
//            }
//
//            if ($ast['status'] == 1) {
//                $order_c = "счет выставлен";
//            }
//            if ($ast['status'] == 2) {
//                $order_c = "счет оплачен";
//            }
//            if ($ast['status'] == 3) {
//                $order_c = "счет отвергнут";
//            }
//            if ($ast['status'] == 4) {
//                $order_c = "счет в архиве";
//            }
            $result = $newBill->save();

            if (!$result) {
                dd($newBill->errors);
            }

            if ($newBill->id) {
                Bill::updateAll(
                    [
                        'created_at' => $item['data'],
                        'updated_at' => $item['data'],
                    ],
                    ['id' => $newBill->id]
                );
            }
        }
    }

    private function getDataByTable(string $table)
    {
        return (new \yii\db\Query())
            ->from($table)
            ->each(100, $this->dbOld);
    }

    private function addFile($path)
    {
        $info = pathinfo($path);

        switch ($info['extension']) {
            case 'mp3':
                $type = 'audio/mpeg';
                break;

            case 'wav':
                $type = 'audio/wave';
                break;

            case 'doc':
                $type = 'application/msword';
                break;

            case 'docx':
                $type = 'application/vnd.openxmlformats-officedocument.wordprocessing';
                break;

            default:
                $type = $info['extension'];
                break;
        }

        $file = new File();
        $file->name = $info['filename'];
        $file->path = $info['dirname'];
        $file->filename = $info['basename'];
        $file->type = $type;
        $file->ext = $info['extension'];
        $file->size = rand(15000, 99999);
        $file->default = false;
        $result = $file->save();
        if (!$result) {
            dd($file->errors);
        }

        return $file;
    }
}

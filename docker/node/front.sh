#!/bin/bash

cd /app/frontend/appjs

if [ ! -d "node_modules" ]; then
  echo 'Установка front-end окружения'
  npm i
  echo 'Установка успешно завершена'
fi

npm run start

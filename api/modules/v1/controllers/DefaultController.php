<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class DefaultController
 * @package api\modules\v1\controllers
 */
class DefaultController extends RestController
{
    public function behaviors()
    {
        $behaviors = ArrayHelper::merge(parent::behaviors(), [
            'authenticator' => [
                'except' => ['swagger', 'options'],
            ],
        ]);


        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ]
        ];
    }

    public function actionSwagger()
    {
//        $token = Yii::$app->request->getQueryParam('access-token');

        Yii::$app->response->headers->set('content-type', 'application/json');
//        Yii::$app->response->headers->set('Authorization', "Bearer $token");
        Yii::$app->response->format = 'json';

        $openapi = \OpenApi\scan([
            Yii::getAlias('@api'),
            Yii::getAlias('@common') . '/models'
        ]);

        return json_decode($openapi->toJson(  JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
    }
}

<?php

namespace api\modules\v1\models\records\auth;

use api\modules\v1\models\queries\UserQuery;
use api\modules\v1\models\records\announcer\Announcer;
use api\modules\v1\models\records\announcer\Opponent;
use api\modules\v1\models\records\announcer\ScheduleWork;
use api\modules\v1\models\records\file\File;
use api\modules\v1\models\records\user\Chat;
use api\modules\v1\models\records\user\Notification;
use common\models\user\Bill;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * @OA\Schema(title="Пользователь")
 */
class User extends \common\models\access\User
{
    public $notNotify = false;

    /**
     * @return UserQuery
     */
    public static function find()
    {
        return new UserQuery(static::class);
    }

    /**
     * @OA\Property(property="id", type="integer",description="ID пользователя")
     * @OA\Property(property="username", type="string", description="username пользователя")
     * @OA\Property(property="email", type="string", description="email пользователя")
     * @OA\Property(property="amountCash", type="integer", description="сумма на счету у пользователя")
     * @OA\Property(property="newNotifications", type="integer", description="количество уведомлений у пользователя")
     * @OA\Property(property="newMessages", type="integer", description="количество новых сообщений у пользователя")
     * @OA\Property(property="avatar", type="string", description="Аватар пользователя")
     * @OA\Property(property="firstname", type="string", description="Имя пользователя")
     * @OA\Property(property="lastname", type="string", description="Фамилия пользователя")
     * @OA\Property(property="permissions", type="array", description="список рашзрешений у пользователя",
     *      @OA\Items(type="object", description="Permission of user", ref="#/components/schemas/Permission")
     * )
     * @OA\Property(property="roles", type="array", description="Роли пользователя",
     *      @OA\Items(
     *         @OA\Parameter(
     *             name="name",
     *             description="Название",
     *             @OA\Schema(
     *                 type="string",
     *             )
     *         ),
     *         @OA\Parameter(
     *             name="description",
     *             description="Описание",
     *             @OA\Schema(
     *                 type="string",
     *             )
     *         ),
     *     )
     * )
     * @OA\Property(property="isAnnouncer", type="boolean", description="пользователя является диктором")
     * @OA\Property(property="scheduleWork", type="object", description="Расписание пользователя")
     * @OA\Property(property="yamoney", type="string", description="Баланс пользователя")
     * @OA\Property(property="favoritesCount", type="integer", description="Количество избранных")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'id' => 'id',
            'username' => 'username',
            'email' => 'email',
            'amountCash' => 'amountCash',
            'newNotifications' => 'newNotifications',
            'newMessages' => 'newMessages',
            'avatar' => 'photoUrl',
            'firstname' => 'firstname',
            'lastname' => 'lastname',
            'permissions' => 'permissions',
            'roles' => 'roles',
            'isAnnouncer' => 'isAnnouncer',
            'scheduleWork',
            'yamoney' => 'cashYamoney',
            'favoritesCount' => 'favoritesCount',
            'myFavoritesCount' => 'myFavoritesCount',
            'last_visit',
            'paidBills' => 'paidBills',
        ];
    }

    /**
     * @OA\Property(property="setting", type="string", description="in expand: setting")
     * @return array|false
     */
    public function extraFields()
    {
        if ($this->id != Yii::$app->user->id) {
            return ['setting' => []];
        }

        return [
            'setting' => function () {
                return !empty($this->setting) ? $this->setting->setting : [];
            },
        ];
    }

    public function getCashYamoney()
    {
        if (!$this->announcer) {
            return null;
        }

        return $this->announcer->cash_yamoney;
    }

    public function getAmountCash()
    {
        if (!$this->getIsAnnouncer() || !$this->announcer) {
            return null;
        }

        return $this->announcer->sum;
    }

    public function getNewMessages()
    {
        return (int)Chat::getUnreadMessages($this->id, $this->isAnnouncer);
    }

    public function getNewNotifications()
    {
        $notify = Notification::getNotify(null, 11, false);
        $count = count($notify);

        if ($count === 11) {
            $quantity = Notification::getQuantity();
        } else {
            $quantity = $count;
        }

        return [
            'quantity' => (int)$quantity,
            'messages' => $this->addUserToNotify($notify),
        ];
    }

    public function addUserToNotify($notifies)
    {
        $billIds = [];
        foreach ($notifies as $notify) {
            if ($notify['type'] == Notification::TYPE_REVIEW_SEND) {
                $billIds[] = $notify['other_id'];
            }
        }

        $bills = Bill::find()
            ->where(['id' => $billIds])
            ->indexBy('id')
            ->asArray()
            ->all();
        $userIds = ArrayHelper::getColumn($bills, $this->isAnnouncer ? 'client_id' : 'announcer_id');

        $users = Opponent::find()
            ->where(['id' => $userIds])
            ->indexBy('id')
            ->all();


        foreach ($notifies as &$notify) {
            if ($notify['type'] == Notification::TYPE_REVIEW_SEND && isset($bills[$notify['other_id']])) {
                $userId = $bills[$notify['other_id']][$this->isAnnouncer ? 'client_id' : 'announcer_id'];
                $notify['opponent'] = $users[$userId] ?? [];
            }
        }

        return $notifies;
    }

    public function getPhoto()
    {
        return $this->hasOne(File::class, ['id' => 'photo_id']);
    }

    public function getPermissions()
    {
        $permissions = [];
        foreach (Yii::$app->authManager->getPermissionsByUser($this->id) as $permission) {
            $permissions[] = [
                'name' => $permission->name,
                'description' => $permission->description,
            ];
        }

        return $permissions;
    }

    public function getRoles()
    {
        $permissions = [];
        foreach (Yii::$app->authManager->getRolesByUser($this->id) as $role) {
            $permissions[] = [
                'name' => $role->name,
                'description' => $role->description,
            ];
        }

        return $permissions;
    }

    public function loadRoles(array $roles = [])
    {
//        if (!Yii::$app->user->can('updateRules')) {
//            return;
//        }

        if (empty($roles)) {
            return;
        }
        $authManager = Yii::$app->getAuthManager();

        $assignments = $authManager->getAssignments($this->id);

        if (!in_array(self::ROLE_GUEST, $roles)) {
            $roles[] = self::ROLE_GUEST;
        }

        foreach ($roles as $role) {
            if (!array_key_exists($role, $assignments)) {
                $roleModel = $authManager->getRole($role);
                $authManager->assign($roleModel, $this->id);
            }

            unset($assignments[$role]);
        }

        foreach ($assignments as $role => $assignment) {
            $roleModel = $authManager->getRole($role);
            $authManager->revoke($roleModel, $this->id);
        }
    }

    public function getAnnouncer()
    {
        return $this->hasOne(Announcer::class, ['user_id' => 'id']);
    }

    /**
     * @param string $tokenClass
     *
     * @return Token|null
     */
    public function auth(string $tokenClass = Token::class)
    {
        return parent::auth($tokenClass);
    }

    public function getScheduleWork()
    {
        return $this->hasOne(ScheduleWork::class, ['user_id' => 'id']);
    }

    public function getPaidBills()
    {
        $bills = [];

        if ($this->isAnnouncer) {
            $paidBills = Bill::find()
                ->andWhere([
                    'announcer_id' => $this->id,
                    'is_canceled' => false,
                    'is_payed' => true,
                    'is_send' => false,
                ])
                ->andWhere(['IN', 'status', [Bill::STATUS_PAID_ANNOUNCER, Bill::STATUS_PAID_CLIENT]])
                ->andWhere('card_number IS NOT NULL')
                ->all();

            /** @var Bill $bill */
            foreach ($paidBills as $bill) {
                $bills[] = [
                    'id' => $bill->id,
                    'sum' => number_format($bill->sum),
                    'date' => Yii::$app->formatter->asDatetime($bill->date),
                ];
            }
        }

        return $bills;
    }

}

/**
 * @OA\Schema(
 *      schema="Permission",
 *      title="Разрешение для пользователя",
 *      @OA\Property(property="name", type="string", description="наименивание разрешения"),
 *      @OA\Property(property="description", type="string", description="описание разрешения"),
 * )
 */

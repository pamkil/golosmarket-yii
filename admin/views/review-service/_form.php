<?php

use common\models\site\Feedback;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var \common\models\site\Review $model
 */

$form = ActiveForm::begin(
    [
        'options' => [
            'enctype' => 'multipart/form-data' // important
        ]
    ]
) ?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <?= Html::submitButton(
            '<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord ? 'Создать' : 'Сохранить'),
            ['class' => 'btn btn-success']
        ) ?>
        <?php
        if (!$model->isNewRecord) {
            echo Html::a(
                '<i class="fa fa-times"></i>&nbsp;' . 'Удалить',
                ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'title' => 'Удалить',
                    'data-confirm' => 'Вы уверены что хотите удалить?',
                    'data-method' => 'post'
                ]
            );
        } ?>
    </div>
    <div class="box-body">
        <?= $form->field($model, 'is_public')->checkbox(); ?>
        <?= $form->field($model, 'show_quantity'); ?>
        <?= $form->field($model, 'rating')->dropDownList([1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5]); ?>
        <?= $form->field($model, 'text')
            ->widget(TinyMce::class, Yii::$app->params['tinyMceOptions'] ?? ''); ?>

        <?php
        if ($model->user) {
            echo \yii\widgets\DetailView::widget(
                [
                    'model' => $model->user,
                    'attributes' => [
                        'id',
                        'firstname',
                        'lastname',
                        'email',
                        'phone',
                    ],
                ]
            );
        } ?>
    </div>
</div>

<?php
ActiveForm::end(); ?>

<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user_announcer_presentations}}`.
 */
class m240325_124453_add_use_default_column_to_user_announcer_presentations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_announcer_presentations}}', 'use_default', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_announcer_presentations}}', 'use_default');
    }
}

<?php

namespace api\modules\v1\actions\user;

use api\modules\v1\actions\common\IndexAction;
use api\modules\v1\models\filters\user\BillSearch;
use common\service\user\dto\BillSearchDto;
use Yii;

class Bill extends IndexAction
{
    /**
     * Список ролей
     * @OA\Get(
     *     path="/user/bill",
     *     summary="Получение списка заказов",
     *     tags={"User"},
     *     security={{"BearerAuth": {}}},
     *     @OA\Response(
     *         response="200",
     *         description="Объект токена",
     *         @OA\JsonContent(ref="#/components/schemas/Token")
     *     )
     * )
     *
     * @return mixed|null
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $billSearchModel = new BillSearch();
        $billSearchDto = $this->getBillSearchDto();

        return $billSearchModel->search($billSearchDto);
    }

    private function getBillSearchDto () {
        $user = Yii::$app->user;

        return new BillSearchDto([
            'isAnnouncer' => $user->getIdentity()->isAnnouncer,
            'userId' => $user->getId(),
            'page' => (int)Yii::$app->getRequest()->getQueryParam('page'),
            'perPage' => (int)Yii::$app->getRequest()->getQueryParam('per-page')
        ]);
    }
}

<?php

namespace common\models\user;

use common\models\access\User;
use common\models\Model;
use Yii;

/**
 * Class Notification
 *
 * @package common\models\user
 * @property int    $id
 * @property int    $user_id
 * @property int    $other_id
 * @property int    $status
 * @property int    $type
 * @property string $text
 * @property string $created_at
 * @property string $updated_at
 * @property User   $user
 */
class Notification extends Model
{
    public const STATUS_NEW = 1; //Новый
    public const STATUS_READ = 2; //Просмотренный

    public const TYPE_BILL = 1;
    public const TYPE_NO_PAID_BILL = 2;
    public const TYPE_REVIEW = 3;
    public const TYPE_NEWS = 4;
    public const TYPE_REVIEW_SEND = 5;
    public const TYPE_REVIEW_SERVICE_CLIENT = 6; //ReviewServiceForm on front

    public static function tableName()
    {
        return '{{%user_notification}}';
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }

    public static function nameStatuses()
    {
        return [
            self::STATUS_NEW => 'new',
            self::STATUS_READ => 'read',
        ];
    }

    public static function nameTypes()
    {
        return [
            self::TYPE_BILL => 'bill',
            self::TYPE_NO_PAID_BILL => 'no_paid_bill',
            self::TYPE_REVIEW => 'review',
            self::TYPE_REVIEW_SEND => 'review_send',
            self::TYPE_NEWS => 'news',
            self::TYPE_REVIEW_SERVICE_CLIENT => 'review_service',
        ];
    }

    public static function newNotify(string $text, int $type, int $otherId = null, int $userId = null)
    {
        $model = new static();

        if (!$userId) {
            $userId = Yii::$app->user->getId();
        }

        $model->user_id = $userId;
        $model->type = $type;
        $model->text = $text;
        $model->other_id = $otherId;
        $model->save();

        return $model;
    }

    public static function getQuantity(int $userId = null)
    {
        if (!$userId) {
            $userId = Yii::$app->user->getId();
        }

        return static::find()
            ->where(
                [
                    'user_id' => $userId,
                    'status' => self::STATUS_NEW,
                ]
            )
            ->count();
    }

    public static function getNotify(int $userId = null, $limit = 10, bool $asArray = true)
    {
        if (!$userId) {
            $userId = Yii::$app->user->getId();
        }

        return static::find()
            ->where(
                [
                    'user_id' => $userId,
                    'status' => self::STATUS_NEW,
                ]
            )
            ->asArray($asArray)
            ->limit($limit)
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
    }

    public function rules()
    {
        return [
            [
                ['user_id', 'text', 'type'],
                'required',
            ],
            [
                ['user_id', 'type', 'status', 'other_id'],
                'number',
            ],
            [
                ['status'],
                'default',
                'value' => Notification::STATUS_NEW,
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Польщзователь',
            'other_id' => 'Зависимы ID',
            'status' => 'Статус',
            'text' => 'Текст',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}

<?php

return [
    'enablePrettyUrl' => true,
    'enableStrictParsing' => false,
    'showScriptName' => false,
    'rules' => [
        '' => 'default/index',

        '<action:(login|logout)>' => 'default/<action>',

        '<controller>/<id:\d+>/<action>' => '<controller>/<action>',
        '<controller>/<action>' => '<controller>/<action>',
    ]
];

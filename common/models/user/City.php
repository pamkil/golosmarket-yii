<?php

namespace common\models\user;

use common\models\Model;

/**
 * Автор публикаций
 *
 * Class Review
 * @package common\models\article
 * @property int $id
 * @property string $code
 * @property string $title
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property int $deleted_at [timestamp]
 */
class City extends Model
{
    public static function tableName()
    {
        return '{{%user_city}}';
    }

    public function rules()
    {
        return [
            [
                ['title', 'code'],
                'string',
                'min' => 2,
                'max' => 100
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Символьный код',
            'title' => 'Наименование',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['sluggableBehavior']['attribute'] = 'title';

        return $behaviors;
    }
}
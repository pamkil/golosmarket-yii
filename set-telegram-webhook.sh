#!/bin/bash

TOKEN="6906603313:AAH9ZCw39pNfXsML2ROpWENvyKLWEpiWGZA"
URL="https://test.golosmarket.ru/api/v1/telegram/webhook/secret"

response=$(curl -s -g -X POST -H "Content-Type: application/json" "https://api.telegram.org/bot$TOKEN/setWebhook?url=$URL&allowed_updates=[\"callback_query\",\"message\"]")

if ! echo $response | jq -e '.ok' || [ "$(echo $response | jq -r '.ok')" != "true" ]; then
    echo "Failed to set webhook"
else
    echo "Webhook set successfully"
fi
<?php

/* @var $this yii\web\View */
/* @var $userName string */
/* @var $verifyUrl string */
?>

Здравствуйте, <?= $userName ?>. Для завершения регистрации на golosmarket.ru, перейдите по ссылке: <?= $verifyUrl ?>

<section class="content contentpage">
    <div class="container">
        <div class="titlepage">
            <a class="icon mobile-no" href="/">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M19 12H6M12 5l-7 7 7 7"/></svg>
            </a>
            <h1>Вопрос-ответ</h1>
        </div>
        <div class="accordion">
            <?
            foreach ($faqs as $faq) : ?>
                <div class="accordion-item <?= $faq === $faqs[0] ? 'is-active' : ''; ?>">
                    <div class="accordion-head"><?= $faq['question']; ?></div>
                    <div class="accordion-panel">
                        <?= $faq['answer'] ?>
                    </div>
                </div>
            <?
            endforeach; ?>
        </div>
    </div>
</section>
<style>
    .accordion {
        width: 100%;
    }

    .accordion-item.is-active .accordion-head {
        color: #66BA04;
    }

    .accordion-head {
        margin: 0;
        padding: 10px 0;
        cursor: pointer;
        font-weight: normal;
        font-size: 18px;
        border-bottom: 1px dashed #a4a4a4;
        transition: 0.5s;
    }

    .accordion-head::before {
        content: "";
        display: inline-block;
        height: 5px;
        width: 5px;
        margin-right: 15px;
        margin-left: 0;
        vertical-align: middle;
        border-right: 1px solid;
        border-bottom: 1px solid;
        -webkit-transform: rotate(-45deg);
        transform: rotate(-45deg);
        -webkit-transition: -webkit-transform 0.2s ease-out;
        transition: -webkit-transform 0.2s ease-out;
        transition: transform 0.2s ease-out;
        transition: transform 0.2s ease-out, -webkit-transform 0.2s ease-out;
    }

    .accordion-head:hover {
        color: #66BA04;
        transition: 0.5s;
    }

    /* Panel */
    .accordion-panel {
        margin: 0;
        padding: 10px;
        display: none;
        font-size: 16px;
        line-height: 30px;
    }

    /* Active */
    .accordion-item.is-active .accordion-head::before {
        -webkit-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script>
    $( function () {
        // (Optional) Active an item if it has the class "is-active"
        $( ".accordion > .accordion-item.is-active" ).children( ".accordion-panel" ).slideDown();

        $( ".accordion > .accordion-item" ).click( function () {
            // Cancel the siblings
            $( this ).siblings( ".accordion-item" ).removeClass( "is-active" ).children( ".accordion-panel" ).slideUp();
            // Toggle the item
            $( this ).toggleClass( "is-active" ).children( ".accordion-panel" ).slideToggle( "ease-out" );
        } );
    } );
</script>

<?php

use yii\db\Migration;

/**
 * Class m200409_201613_add_old_id
 */
class m200409_201613_add_old_id extends Migration
{
    private $table = '{{%user}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->table,
            'old_id',
            $this->integer()->null()->after('id')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'old_id');
    }
}

<?php

namespace common\service\collection;

use common\models\announcer\File;
use common\models\content\Collection;
use common\models\content\CollectionSound;
use common\repositories\contract\CollectionRepositoryContract;
use common\service\collection\contract\CollectionServiceContract;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class CollectionService implements CollectionServiceContract
{
    private CollectionRepositoryContract $collectionRepository;

    public function __construct(
        CollectionRepositoryContract $collectionRepository
    ) {
        $this->collectionRepository = $collectionRepository;
    }

    public function getEmptyModel(): Collection
    {
        return new Collection();
    }

    public function create(array $params): Collection
    {
        try {
            $model = new Collection();
            $model->load($params);

            if ($model->validate()) {
                $image = UploadedFile::getInstanceByName('image');
                if (!is_null($image)) {
                    $file = File::setFileByUpload($image);
                    $file->save();
                    $model->image = $file;
                } else {
                    $model->addError('image', 'Необходимо добавить изображение для подборки');
                }

                if (empty($params['collectionSounds'])) {
                    $model->addError('collectionSounds', 'Необходимо добавить хотя бы одну демо запись');
                }

                if (!$model->hasErrors() && $model->save()) {
                    $collectionSounds = [];
                    foreach ($params['collectionSounds'] as $sound) {
                        $collectionSounds[] = new CollectionSound([
                            'collection_id' => $model->id,
                            'announcer_id' => (int) $sound['announcer_id'],
                            'sound_id' => (int) $sound['sound_id']
                        ]);
                    }
                    $model->collectionSounds = $collectionSounds;

                    $model->save();
                }
            }
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
        }

        return $model;
    }

    public function update(Collection $model, array $params): Collection
    {
        $model->load($params);

        if ($model->validate()) {
            $image = UploadedFile::getInstanceByName('image');
            if (!is_null($image)) {
                $deletingImage = $model->image;
            } elseif (is_null($image) && empty($model->image)) {
                $model->addError('image', 'Необходимо добавить изображение для подборки');
            }

            if (empty($params['collectionSounds'])) {
                $model->addError('collectionSounds', 'Необходимо добавить хотя бы одну демо запись');
            }

            if (!$model->hasErrors() && $model->save()) {
                if (!empty($image)) {
                    $file = File::setFileByUpload($image);
                    $file->save();
                    $model->image = $file;
                }

                $collectionSounds = [];
                foreach ($params['collectionSounds'] as $sound) {
                    $collection = CollectionSound::findOne([
                        'collection_id' => $model->id,
                        'announcer_id' => $sound['announcer_id'],
                        'sound_id' => $sound['sound_id']
                    ]);

                    if (!$collection) {
                        $collectionSounds[] = new CollectionSound([
                            'collection_id' => $model->id,
                            'announcer_id' => $sound['announcer_id'],
                            'sound_id' => $sound['sound_id']
                        ]);
                    } else {
                        $collectionSounds[] = $collection;
                    }
                }
                $model->collectionSounds = $collectionSounds;

                if ($model->save() && !empty($deletingImage)) {
                    unlink($deletingImage->getFullPath());
                    $deletingImage->delete();
                }
            }
        }

        return $model;
    }
}

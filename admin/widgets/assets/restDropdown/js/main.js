function sendValue(that, url) {
    var obj = $(that),
        param = obj.data('param');

    $.ajax({
        url: url,
        type: 'PATCH',
        dataType: 'json',
        data: param + '=' + obj.val()
    }).done(function () {
        toastr.success('Успешно изменено');
        $(that).trigger('dropdown:success');
    }).fail(function (jqXHR) {
        var textError = '';
        var titleError = 'Ошибка изменения';
        if (jqXHR.status < 500) {
            textError = jqXHR.responseJSON.message;
        }
        toastr.error(textError, titleError, {timeOut: 10000});
      $(that).trigger('dropdown:error');
    });
}
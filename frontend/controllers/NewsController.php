<?php

namespace frontend\controllers;

use frontend\models\NewsSearch;
use yii\web\NotFoundHttpException;

/**
 * Page controller
 */
class NewsController extends BaseController
{
    /**
     * Displays homepage.
     *
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $newsModel = new NewsSearch();
        $provider = $newsModel->search();
        return $this->render('index', [
            'provider' => $provider,
        ]);
    }
}

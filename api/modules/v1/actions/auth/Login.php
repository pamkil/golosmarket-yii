<?php

namespace api\modules\v1\actions\auth;

use api\modules\v1\models\checks\auth\LoginForm;
use Carbon\Carbon;
use Yii;
use yii\base\Action;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class Login extends Action
{
    /**
     * Авторизация
     * @OA\Post(
     *      path="/auth/login",
     *      summary="Login as user. Получение токена по лоину и паролю",
     *      tags={"Auth"},
     *      @OA\RequestBody(
     *          description="Создание пользователья",
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/LoginForm")
     *      ),
     *    @OA\Response(
     *        response="200",
     *        description="Объект токена",
     *        @OA\JsonContent(ref="#/components/schemas/Token")
     *    )
     * )
     *
     * @return mixed|null
     * @throws \yii\base\Exception
     */
    public function run()
    {
        $model = new LoginForm();
        $model->load(Yii::$app->request->bodyParams);

        if ($token = $model->auth()) {
            return $token;
        } else {
            if ($model->hasErrors('uniqueEmail')) {
                Yii::$app->response->statusCode = 406;
                return [[
                    'field' => 'email',
                    'message' => 'Войти не удалось. Если вы забыли пароль или удаляли профиль - восстановите доступ'
                ]];
            }

            return $model;
        }
    }
}

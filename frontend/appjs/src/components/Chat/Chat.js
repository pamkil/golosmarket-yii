import React, { useRef, useCallback, useEffect, useState, useMemo } from 'react';
import MessageList from './MessageList/MessageList';
import usePathName from "../../data/pathName/usePathName";
import debounce from "lodash/debounce";
import isEmpty from 'lodash/isEmpty';
import {
    announcerChangeFavorite, APP_UPDATE_EVENT,
    cancelBill,
    createBill,
    editMessage,
    initializeApp,
    requestChats, requestDeleteMessage,
    requestMessages, requestReadChats, requestShareFile, sendFiles,
    sendMessage,
    updateBill,
    updateMessage
} from "../../data/redux";
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import ChatInput from "./ChatInput/ChatInput";
import ChatList from './ChatList/ChatList';
import { register } from 'timeago.js';
import ru from 'timeago.js/esm/lang/ru.js';
import TopBar from "./TopBar/TopBar";
import copy from "copy-to-clipboard";
import { FavoriteService } from "../../data/services";
import classNames from 'classnames';
import { isMobile as isMobileFunc } from "../../data/helpers/Tools";
import PromiseDialog from "../Common/PromiseDialog/PromiseDialog";
import { eventPlay, eventStop } from "../Common/PlayerEvent/playerEvent";
import ReadModeModal from "./ReadModeModal/ReadModeModal";
import {toast} from "react-toastify";
import {textCopyFile} from "../Announcer/Announcer";
import config from '../../data/config/';

register('ru_RU', ru);
const filters = [
    {
        id: 'man',
        title: 'Мужские'
    },
    {
        id: 'woman',
        title: 'Женские',
    },
    {
        id: 'favorite',
        title: 'Избранные'
    }
];

export const MESSAGES_LIMIT = 50;
const getFileMimeType = (file) => {
    if (!file) return 'text';

    return file.type.includes('audio')
        ? 'audio'
        : file.type.includes('image')
            ? 'image'
            : 'file';
};

export default function Chat() {
    const dispatch = useDispatch();
    const {
        user,
        isLogin,
        announcers,
        messages,
        isAnnouncer,
        bill,
        opponent,
        isInit,
        fileIsLoading,
        localDev,
    } = useSelector(state => ({
        localDev: state.app.localDev,
        user: state.user,
        isInit: state.app.initialized,
        isLogin: !!state.auth.token,
        pageAnnouncer: state.chat.chat.pagination.page,
        isAnnouncer: !!(state.user && state.user.user && state.user.user.isAnnouncer),
        opponent: state.chat.message.opponent,
        fileIsLoading: state.chat.message.fileIsLoading,
        bill: state.chat.message.bill,
        announcers: (state.chat.chat.items || [])
            .map(({ opponentId, avatar, name, unread, isOnline, deleteQuantity, createdAt, text }) => ({
                id: opponentId,
                avatar: avatar,
                title: name,
                unread,
                deleteQuantity,
                date: new Date(Date.UTC(createdAt)),
                subtitle: text,
                isOnline,
            })),
        messages: (state.chat.message.items || [])
            .map(
                ({  id, isWriteOpponent, createdAt, text, isRead, file, fileType, parent }) => ({

                    id,
                    position: isWriteOpponent ? 'left' : 'right',
                    isWriteOpponent,
                    file,
                    isRead,
                    fileType,
                    type: getFileMimeType(file),
                    date: new Date(createdAt),
                    text,
                    parent: {
                        ...parent,
                        type: getFileMimeType(parent && parent.file)
                    },
                })
            )
    }), shallowEqual);
    const hasOpponent = !isEmpty(opponent);
    const setInitialize = useCallback(initializeApp, [ initializeApp ]);
    const [ pathName, setPathParams ] = usePathName();
    const [ filter, setFilter ] = useState('');
    const [ activeAnnouncerId, setActiveAnnouncerId ] = useState(null);
    const [ replyMessage, setReplyMessage ] = useState(null);
    const [ isMobile, setIsMobile ] = useState(isMobileFunc());
    const [isEditingMessage, setIsEditingMessage] = useState(false)
    const [messageToEdit, setMessageToEdit] = useState(null)
    const requests = useCallback(filter => requestChats({ filter }), [ requestChats ]);
    const promiseDialogRef = useRef(null);
    const readForRequest = useRef([]);
    const messageListRef = useRef(null);
    const activeUnreadCount = useMemo(() => {
        const activeChat = announcers.find(({ id }) => id === activeAnnouncerId);

        return activeChat && activeChat.unread || 0;
    }, [ announcers, activeAnnouncerId ]);
    const activeDeleteCount = useMemo(() => {
        const activeChat = announcers.find(({ id }) => id === activeAnnouncerId);

        return activeChat && activeChat.deleteQuantity || 0;
    }, [ announcers, activeAnnouncerId ]);
    const handleRead = debounce(() => {
        const ids = readForRequest.current;

        if (!ids.length) return;

        readForRequest.current = [];
        dispatch(requestReadChats(activeAnnouncerId, ids));
    }, 1000);
    const [ readText, setReadText ] = useState('');

    const viewportHeight = useRef(null);
    const viewportWidth = useRef(null);
    const [ isPortrait, setIsPortrait ] = useState(false);

    useEffect(() => {
        if (!document.body.classList.contains('chat-mobile')) {
            document.body.classList.add('chat-mobile');
        }
        window.addEventListener('resize', checkResize);

        viewportHeight.current = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        viewportWidth.current = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        setIsPortrait(viewportHeight.current > viewportWidth.current);

        return () => {
            window.removeEventListener('resize', checkResize);
            document.body.classList.remove('chat-mobile');
            document.body.classList.remove('hide-header');
        }
    }, []);

    useEffect(() => {
        setInitialize();
    }, [ setInitialize ]);

    useEffect(() => {
        if (isLogin) {
            getChats();
            document.addEventListener(APP_UPDATE_EVENT, getChats);
        }

        return () => {
            document.removeEventListener(APP_UPDATE_EVENT, getChats);
        }
    }, [ requests, filter, isLogin ]);

    // Смена pathname
    useEffect(() => {
        if (pathName && pathName.length > 1) {
            setActiveAnnouncerId(parseInt(pathName[1]));
            document.body.classList.add('hide-header');
        } else {
            setActiveAnnouncerId(null);
            document.body.classList.remove('hide-header');
        }
    }, [ pathName ]);

    // Get messages with changed unread messages
    useEffect(() => {
        if (isLogin && (activeUnreadCount || activeDeleteCount)) {
            getMessages();
        }
    }, [ activeUnreadCount, activeDeleteCount, isLogin ]);

    // Update all messages by change delete count
    useEffect(() => {
        if (messages.length > MESSAGES_LIMIT) {
            const countPages = Math.ceil(messages.length / MESSAGES_LIMIT) - 1;

            for (let i = 1; i < countPages; i++) {
                const chatId = messages[MESSAGES_LIMIT * i - 1].id;

                dispatch(requestMessages(activeAnnouncerId, { chatId }));
            }
        }
    }, [ activeDeleteCount ]);

    // Get messages for logged in and active announcer
    useEffect(() => {
        if (isLogin && activeAnnouncerId) {
            getMessages();
        }
    }, [ activeAnnouncerId, isLogin ]);

    // Get first announcer with first load
    useEffect(() => {
        if (!isMobile && !activeAnnouncerId && announcers.length) {
            setActiveAnnouncerId(announcers[0].id);
        }
    }, [ announcers, activeAnnouncerId, isMobile ]);

    useEffect(() => {
        if (isInit && !isLogin && window.location.pathname.includes('chat')) {
            window.location = '/';
        }
    }, [ isInit, isLogin ])

    const checkResize = (e) => {
        const newViewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        const newViewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        const hasOrientationChanged = (newViewportHeight > newViewportWidth) !== isPortrait;
        let addressBarHeight = 130;

        if (!hasOrientationChanged && (newViewportHeight !== viewportHeight)) {
            addressBarHeight = Math.abs(newViewportHeight - viewportHeight);

            if (newViewportHeight < viewportHeight) {
                // Android Chrome address bar has appeared
            } else {
                // Android Chrome address bar has disappeared
            }
        } else if(hasOrientationChanged) {
            // Orientation change
        }

        viewportHeight.current = newViewportHeight;
        viewportWidth.current = newViewportWidth;
        setIsPortrait(viewportHeight.current > viewportWidth.current);
    };

    const getChats = () => {
        dispatch(requestChats(filter))
    }

    const getMessages = () => {
        dispatch(requestMessages(activeAnnouncerId));
    }

    const setAnnouncerIdHandler = id => {
        setPathParams(`/chat/${ id }`);
    }

    const addMessage = (messageText, messageToEdit = null) => {
        if(isEditingMessage){
            dispatch(editMessage(activeAnnouncerId, messageToEdit.id, messageText));
            handleClearReplyMessage();
            getMessages()
        }
        else{ 
            dispatch(sendMessage(activeAnnouncerId, messageText, replyMessage));
            handleClearReplyMessage();
            messageListRef.current.scrollTop = messageListRef.current.scrollHeight;
        }  
    }

    const createBillHandler = bill => {
        dispatch(createBill(activeAnnouncerId, bill.sum, bill.card_number));
    }

    const updateBillHandler = bill => {
        dispatch(updateBill(activeAnnouncerId, bill.sum));
    }

    const payedBillHandler = (bill, status) => {
        const is_payed = status === 2 || status === 3 || status === 7 ? 1 : 0
        dispatch(updateBill(activeAnnouncerId, is_payed, status));
    }

    const cancelBillHandler = () => {
        dispatch(cancelBill(activeAnnouncerId));
    }

    const copyHandler = (fileUrl) => {
        copy(document.location.origin + fileUrl);
        toast.info(textCopyFile);
    };

    const addToFavoriteHandler = (id, isFavorite) => {
        if (isLogin) {
            let service;

            if (isFavorite) {
                service = FavoriteService.removeFromFavorite(id);
            } else {
                service = FavoriteService.addToFavorite(id);
            }

            service.then(({data: {quantity}}) => dispatch(announcerChangeFavorite(id, !isFavorite, quantity)));
        }
    };

    const backClickHandler = (event) => {
        event.preventDefault();
        setPathParams('/chat/');
    }

    const sendFileHandler = (files) => {
        dispatch(sendFiles(activeAnnouncerId, files));
    }

    const handleReadMessages = (messageIds = []) => {
        if (messageIds && messageIds.length) {
            readForRequest.current = [ ...readForRequest.current, ...messageIds ];
            handleRead();
            // dispatch(requestReadChats(activeAnnouncerId, { id: messages.map(({ id }) => id) }))
        }
    }

    const handleShareFile = (messageId) => {
        dispatch(requestShareFile(messageId));
    }

    const handleDeleteFile = (messageId) => {
        if (!promiseDialogRef || !promiseDialogRef.current) return;

        promiseDialogRef.current.open({
            title: 'Удаление сообщения',
            content: `Вы уверены, что хотите удалить это сообщение?`,
            submitText: 'Удалить',
            danger: true,
        }).then(() => dispatch(requestDeleteMessage(messageId)));
    }

    const handleEditMessage = (messageId, text) => {

    }

    const handeEditMessageClick = useCallback((message, event)=>{
        setIsEditingMessage(true)
        setReplyMessage(message);
        setMessageToEdit(message);
    },[])

    const handlePlay = (file) => {
        eventPlay(opponent, file);
    }

    const handleStop = () => {
        eventStop();
    }

    const handleLoadPreview = (lastMessageId) => {
        dispatch(requestMessages(activeAnnouncerId, { chatId: lastMessageId }, true));
    }

    const handleReadMode = (message) => {
        setReadText(message);
    };

    const handleReplyClick = useCallback((message, event) => {
        setReplyMessage(message);
    }, []);

    const handleClearReplyMessage = useCallback(() => {
        setReplyMessage(null);
        setIsEditingMessage(false)
    }, []);

    const handleReplyMessageClick = useCallback((currentMessage, index, replyMessage) => {
        const element = document.querySelector(`[data-id="${replyMessage.id}"]`);

        if (element && messageListRef.current) {
            element.scrollIntoView({ block: "center", behavior: "smooth" });
            window.setTimeout(() => {
                element.classList.add('message-box__highlight')
                window.setTimeout(() => {
                    element.classList.remove('message-box__highlight')
                }, 2000)
            }, 500)
        }
    }, [messages, messageListRef]);

    const srvr = localDev ? config.URL : '';

    return (
        <section className='chat-container' key='chat-container'>
            {!!announcers.length && (
                <ChatList
                    className={ classNames('chat-list', { 'chat-list--open': !activeAnnouncerId }) }
                    srvr={srvr}
                    activeChatId={ activeAnnouncerId }
                    dataSource={ announcers }
                    onClick={ ({ id }) => setAnnouncerIdHandler(id) }
                />
            )}
            { hasOpponent && (
                <section className='chat-body' key='chat-body'>
                    <TopBar
                        srvr={srvr}
                        createBill={ createBillHandler }
                        updateBill={ updateBillHandler }
                        payedBill={ payedBillHandler }
                        bill={ bill }
                        cancelBill={ cancelBillHandler }
                        isAnnouncer={ isAnnouncer }
                        addToFavorite={ addToFavoriteHandler }
                        opponent={ opponent }
                        copyHandler={ copyHandler }
                        onBackClick={ backClickHandler }
                    />
                    <MessageList
                        className='message-list'
                        isAnnouncer={ isAnnouncer }
                        lockable
                        toBottomHeight={ '100%' }
                        messages={ messages }
                        unreadCount={ messages
                            .filter(({ isRead, isWriteOpponent }) => !isRead && isWriteOpponent)
                            .map(({ id }) => id).length }
                        downButton={ true }
                        onReadMessages={ handleReadMessages }
                        onShareFile={ handleShareFile }
                        onDelete={ handleDeleteFile }
                        onEdit={handleEditMessage}
                        onEditClick={handeEditMessageClick}
                        onPlay={ handlePlay }
                        onStop={ handleStop }
                        onLoadPreviews={ handleLoadPreview }
                        onReadMode={ handleReadMode }
                        onReplyClick={ handleReplyClick }
                        onReplyMessageClick={ handleReplyMessageClick }
                        cmpRef={(ref) => messageListRef.current = ref }
                    />
                    <ChatInput
                        isEditingMessage={isEditingMessage}
                        messageToEdit={messageToEdit}
                        onChange={ addMessage }
                        onSendFile={ sendFileHandler }
                        fileIsLoading={ fileIsLoading }
                        replyMessage={ replyMessage }
                        onClearReplyMessage={ handleClearReplyMessage }
                    />

                    {readText && (
                        <ReadModeModal
                            message={ readText }
                            onClose={ () => setReadText('') }
                        />
                    )}

                    <PromiseDialog ref={ promiseDialogRef }/>
                </section>
            ) }

            {user && !announcers.length && (
                <p className='chat__empty-announcers-msg'>В списке контактов пока нет ни одного пользователя</p>
            )}
        </section>
    );
}

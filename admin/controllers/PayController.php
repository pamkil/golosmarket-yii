<?php

namespace admin\controllers;

use admin\models\bill\BillSearch;
use common\models\site\Setting;
use common\models\user\Bill;
use Yii;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;

class PayController extends SecurityController
{
    public function actionIndex()
    {
        $searchModel = new BillSearch();
        $dataProvider = $searchModel->searchPay(Yii::$app->request->get());

        $this->view->title = 'Список';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        /** @var ActiveQuery $query */
        $query = clone $dataProvider->query;
        $query->andWhere(['is_payed' => true]);

        $percent = (int)(Setting::getSetting('percent') ?? 30);

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'percent' => $percent,
            ]
        );
    }

    public function actionUpdate($id)
    {
        if (!Yii::$app->request->isPost) {
            throw new NotFoundHttpException;
        }

        $ids = explode(',', Yii::$app->request->post('ids'));
        $bills = Bill::updateAll(
            [
                'status' => Bill::STATUS_PAID_ANNOUNCER,
                'is_payed' => true,
            ],
            [
                'id' => $ids,
                'status' => Bill::STATUS_PAID_CLIENT,
                'announcer_id' => $id
            ]
        );

        Yii::$app->session->addFlash('success', 'Успешно отмечено ' . $bills . ' счетов');

        return $this->redirect(['/pay/index']);
    }

    public function actionView($id)
    {
        $ids = explode('-', Yii::$app->request->getQueryParam('ids'));
        $bills = Bill::updateAll(
            [
                'status' => Bill::STATUS_PAID_ANNOUNCER,
                'is_payed' => true,
            ],
            [
                'id' => $ids,
                'status' => Bill::STATUS_PAID_CLIENT,
                'announcer_id' => $id
            ]
        );

        Yii::$app->session->addFlash('success', 'Успешно отмечено ' . $bills . ' счетов');

        return $this->redirect(['/pay/index']);
    }
}
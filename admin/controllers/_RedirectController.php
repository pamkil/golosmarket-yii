<?
namespace admin\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use common\models\seo\Redirect;
use common\models\seo\RedirectSearch;
use common\models\settings\Site;

class RedirectController extends SecurityController
{
    public function actionIndex()
    {
        if (!Yii::$app->user->can('viewRedirects')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $searchModel = new RedirectSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список редиректов';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionCreate()
    {
        if (!Yii::$app->user->can('updateRedirects')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $model = new Redirect;

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно создано');

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Создание редиректа';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список редиректов',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $sites = Site::find()
            ->where(['active' => 1])
            ->all();
        $sites = ArrayHelper::map($sites, 'id', 'name');

        $types = Redirect::getTypes();

        return $this->render('_form', [
            'sites' => $sites,
            'types' => $types,
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('updateRedirects')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Redirect $model */

        $model = Redirect::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }

            return $this->redirect([
                'update',
                'id' => $model->id
            ]);
        }

        $this->view->title = 'Изменение редиректа "' . $model->name . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список редиректов',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->name;

        $sites = Site::find()
            ->where(['active' => 1])
            ->all();
        $sites = ArrayHelper::map($sites, 'id', 'name');

        $types = Redirect::getTypes();

        return $this->render('_form', [
            'sites' => $sites,
            'types' => $types,
            'model' => $model
        ]);
    }

    public function actionDuplicate($id)
    {
        if (!Yii::$app->user->can('updateRedirects')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Redirect $model */

        $model = Redirect::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        /* @var Redirect $redirectModel */

        $redirectModel = $model->duplicate();

        if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/update') !== false) {
            return $this->redirect([
                'update',
                'id' => $redirectModel->id
            ]);
        } else {
            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('updateRedirects')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Redirect $model */

        $model = Redirect::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect([
            'index'
        ]);
    }
}
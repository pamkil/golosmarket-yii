<?php

use yii\db\Migration;

/**
 * Class m200526_160719_add_review_service
 */
class m200526_160719_add_review_service extends Migration
{
    private $table = '{{%site_review}}';
    private $user = '{{%user}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'is_public' => $this->boolean()->notNull()->defaultValue(false),
            'show_quantity' => $this->integer()->notNull()->defaultValue(0),
            'rating' => $this->smallInteger(),
            'text' => $this->text(),

            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),

        ], $tableOptions);

        $this->createIndex('IX-site_review-updated_at', $this->table, 'updated_at');

        $this->createIndex(
            'IX-site_review-user_id',
            $this->table,
            'user_id'
        );

        $this->addForeignKey(
            'FK-site_review-user_id',
            $this->table,
            'user_id',
            $this->user,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}

<?php
/** @var string $yesterdayAverage */
/** @var string $todayAverage */
?>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-green-sharp">
                    <span>
                        <?= number_format($todayAverage, 0, '.', ' '); ?>
                        <small class="font-green-sharp">₽</small>
                    </span>
                    <span style="font-size: 20px;">
                        (<span>
                            <?= number_format($yesterdayAverage, 0, '.', ' '); ?>

                        </span>
                        <small class="font-green-sharp">₽</small>)
                    </span>
                </h3>
                <small>Cр. чек за сегодня (вчера)</small>
            </div>
            <div class="icon">
                <i class="icon-basket"></i>
            </div>
        </div>
    </div>
</div>
<?php

namespace common\service\announcer\enum;

class AnnouncerCostEnum
{
    public const COST = 'cost';
    public const COST_A4 = 'cost_a4';
    public const COST_VOCAL = 'cost_vocal';
    public const COST_PARODY = 'cost_parody';
    public const COST_SOUND_MOUNT = 'cost_sound_mount';
    public const COST_AUDIO_ADV = 'cost_audio_adv';

    public static function getKeys()
    {
        return [
            self::COST,
            self::COST_A4,
            self::COST_VOCAL,
            self::COST_PARODY,
            self::COST_SOUND_MOUNT,
            self::COST_AUDIO_ADV
        ];
    }

    public static function getLabels()
    {
        return [
            self::COST => 'Цена за 30 сек',
            self::COST_A4 => 'Цена за лист А4',
            self::COST_VOCAL => 'Цена за вокал',
            self::COST_PARODY => 'Цена за пародию',
            self::COST_SOUND_MOUNT => 'Цена за монтирование звука',
            self::COST_AUDIO_ADV => 'Цена за сценарий'
        ];
    }

    public static function getShortLabels()
    {
        return [
            self::COST => '30 сек',
            self::COST_A4 => 'Начитка 1 страницы',
            self::COST_VOCAL => 'Вокал',
            self::COST_PARODY => 'Пародия',
            self::COST_SOUND_MOUNT => 'Изготовление аудиоролика',
            self::COST_AUDIO_ADV => 'Сценарий рекламы'
        ];
    }

    public static function getButtonsCost()
    {
        $buttonsCost = [];
        foreach (self::getShortLabels() as $value => $title) {
            $cost = new \stdClass();
            $cost->title = $title;
            $cost->value = $value;
            $buttonsCost[] = $cost;
        }

        return $buttonsCost;
    }

    public static function getFiltersCost()
    {
        $buttonsCost = [];
        foreach (self::getShortLabels() as $id => $name) {
            $cost = new \stdClass();
            $cost->id = $id;
            $cost->name = $name;
            $buttonsCost[] = $cost;
        }

        return $buttonsCost;
    }
}

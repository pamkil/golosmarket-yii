import React, {useCallback} from "react";
import {connect, useDispatch} from "react-redux";
import {
  setShowConfirmationForm
} from '../../data/redux';

import './deleteProfile.css';

const DeleteProfile = () => {
  const dispatch = useDispatch();

  const showConfirmationHandler = useCallback(() => {
    dispatch(setShowConfirmationForm(true));
  }, [setShowConfirmationForm]);

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="delete-profile">
          <span onClick={(e) => showConfirmationHandler()}>Удалить профиль</span>
        </div>
      </div>
    </div>
  )
}

export default connect(null)(DeleteProfile);

import React from "react";
import { Field, reduxForm } from 'redux-form';
import { FORMS } from '../../../data/constants';
import InputInline from "../../InputInline/InputInline";
import PhoneInput from "../../Common/PhoneInput/PhoneInput";
import {isValidPhoneNumber} from "libphonenumber-js";

const validate = values => {
    const errors = {};
    if (!values.agree) {
        errors.agree = 'Необходимо согласие';
    }
    if (!values.text) {
        errors.text = 'Необходимо заполнить';
    } else if (values.text.length < 5) {
        errors.text = 'Необходимо ввести не менее 5 символов';
    }
    if (!values.email && !values.phone) {
        errors.email = 'Необходимо заполнить e-mail';
    }
    if (values.email && !(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/).test(values.email)) {
        errors.email = 'Должен быть e-mail';
    }
    if (values.phone && !isValidPhoneNumber(values.phone)) {
        errors.phone = 'Неверный формат телефона';
    }

    return errors;
};


let FeedbackForm = ({ handleSubmit, pristine, submitting, valid }) => (
    <form onSubmit={ handleSubmit } className="modal-form feedback-form">
        <div className="head-registration">Обратная <span className="underline">связь</span></div>

        <div className="item">
            У Вас есть задача или вопрос? Укажите как с Вами связаться:
        </div>
        <div className="item">
            <div className="block">
                <Field
                    name="phone"
                    component={ PhoneInput }
                    placeholder="Телефон"
                />
            </div>
            <div className="block"/>
        </div>
        <div className="item">
            <div className="block">
                <Field
                    name="email"
                    component={ InputInline }
                    type="email"
                    placeholder="E-mail"
                />
            </div>
            <div className="block"/>
        </div>

        <div className="item text-feedback">
            <div className="block">
            <Field
                component={ InputInline }
                name="text"
                type="textarea"
                placeholder="Напишите сообщение. Будем&nbsp;рады&nbsp;выполнить&nbsp;заказ&nbsp;или&nbsp;ответить&nbsp;на&nbsp;вопрос"
            />
            </div>
            <div className="block"/>
        </div>

        <div className="item">
            <div className="block agree">
                <label>
                    <Field
                        name="agree"
                        component={ InputInline }
                        type="checkbox"
                    >
                        Нажимая "Отправить", я соглашаюсь с <a href="/rulespdn">правилами использования сайта </a>
                        и <a href="/rulespdn">даю согласие на обработку персональных данных</a>
                    </Field>
                </label>

            </div>
            <div className="block"/>
        </div>
        <div className="item">
            <div className="block">
                <button type="submit" disabled={ pristine || submitting } className={ !valid ? 'error-submit' : '' }>
                    Отправить
                </button>
            </div>
            <div className="block"/>
        </div>
    </form>
);

FeedbackForm = reduxForm({
    form: FORMS.FEEDBACK,
    validate
})(FeedbackForm);

export default FeedbackForm;

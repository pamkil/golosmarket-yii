<?php

namespace console\controllers;

use common\models\announcer\File;
use common\models\user\Chat;
use common\service\FileService;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Query;
use yii\helpers\Console;

/**
 * Page controller
 */
class DeleteFilesController extends Controller
{
    public function actionIndex()
    {
        Console::output('DeleteFilesController');
        $fileQuery = File::getExpiredFiles();
        $quantity = 0;

        try {
            foreach ($fileQuery->each() as $file) {
                /** @var File $file */
                $filename = $file->getFullPath();
                if (file_exists($filename)) {
                    if (unlink($filename)) {
                        $file->is_delete = true;
                    } else {
                        $error = 'Can`t unlink file '.$filename;
                        Console::error($error);
                        Yii::error($error);
                        return ExitCode::DATAERR;
                    }
                } else {
                    $file->is_delete = true;
                }
                $file->save();
                $quantity++;
            }

            /** @var FileService $fileService */
            $fileService = new FileService();
            $fileService->RemoveEmptySubFolders();
        } catch (\Exception $e) {
            Console::error($e->getMessage());
            Yii::error($e->getMessage());
            return ExitCode::DATAERR;
        }

        Console::output('Quantity deleted files is ' . $quantity);

        \Yii::$app->cache->flush();
        return ExitCode::OK;
    }

    /**
     * Удаляем файлы старше 7 дней из чата
     *
     * @return void
     */
    public function actionChat()
    {
        Console::output('Delete files in chat');
        $fileQuery = (new Query())->select([
            'id' => 'f.id',
            'path' => 'f.path',
            'filename' => 'f.filename',
            'ext' => 'f.ext',
            'is_delete' => 'f.is_delete',
            'chat_id' => 'c.id',
            'chat_created_at' => 'c.created_at'
        ])
            ->from(['c' => Chat::tableName()])
            ->innerJoin(['f' => File::tableName()], 'c.file_id = f.id')
            ->where([
                'AND',
                ['IS NOT', 'c.file_id', null],
                ['IS', 'c.file_watermark_id', null],
                ['<=', 'f.expired', date('Y-m-d H:i:s')],
                ['f.is_delete' => 0]
            ])
            ->groupBy(['f.id'])
            ->orderBy(['f.id' => SORT_ASC]);
        $fileWatermarkQuery = (new Query())->select([
            'id' => 'f.id',
            'path' => 'f.path',
            'filename' => 'f.filename',
            'ext' => 'f.ext',
            'is_delete' => 'f.is_delete',
            'chat_id' => 'c.id',
            'chat_created_at' => 'c.created_at'
        ])
            ->from(['c' => Chat::tableName()])
            ->innerJoin(['f' => File::tableName()], 'c.file_watermark_id = f.id')
            ->where([
                'AND',
                ['IS NOT', 'c.file_id', null],
                ['<=', 'f.expired', date('Y-m-d H:i:s')],
                ['f.is_delete' => 0]
            ])
            ->groupBy(['f.id'])
            ->orderBy(['f.id' => SORT_ASC]);

        $deletedFilesCount = 0;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $deletedFilesCount += $this->fileDeleted($fileQuery);
            $deletedFilesCount += $this->fileDeleted($fileWatermarkQuery);
            $transaction->commit();
        } catch (\Exception $e) {
            Yii::error('File cleared exist: ' . $e->getMessage());
            $transaction->rollBack();
        }

        Console::output('Clear empty folders');
        exec('find ' . Yii::getAlias('@uploadPath') . ' -type d -empty -delete' . PHP_EOL,$findOut, $findCode);
        if (!$findCode) {
            Console::output('Empty folders clearing');
        } else {
            Console::error('Cleaning failed with an error: ' . $findCode);
        }
        Console::output('Quantity deleted files is ' . $deletedFilesCount);

        \Yii::$app->cache->flush();
    }

    private function fileDeleted(Query $fileQuery): int
    {
        $count = 0;
        $fileCount = $fileQuery->count();
        $fileIterations = (int) round($fileCount / 100);

        Console::startProgress(0, $fileIterations);
        for ($i = 0; $i <= $fileIterations; $i++) {
            $fileQuery->limit(100);

            $updateFiles = [];
            $updateChats = [];
            foreach ($fileQuery->each() as $file) {
                $filename = Yii::getAlias('@uploadPath') . ($file['path'] ?? '') . DIRECTORY_SEPARATOR . $file['filename'] . '.' . $file['ext'];

                if (file_exists($filename)) {
                    unlink($filename);
                    ++$count;
                }
                $updateFiles[] = $file['id'];
                $updateChats[] = [
                    'id' => $file['chat_id'],
                    'created_at' => $file['chat_created_at']
                ];
            }
            foreach ($updateFiles as $updateFile) {
                Yii::$app->db->createCommand()->update(
                    File::tableName(),
                    [
                        'is_delete' => true,
                        'updated_at' => date('Y-m-d H:i:s')
                    ],
                    ['id' => $updateFile]
                )->execute();
            }
            foreach ($updateChats as $updateChat) {
                Yii::$app->db->createCommand()->update(
                    Chat::tableName(),
                    [
                        'created_at' => $updateChat['created_at'],
                        'deleted_at' => date('Y-m-d H:i:s')
                    ],
                    ['id' => $updateChat['id']]
                )->execute();
            }

            Console::updateProgress($i, $fileIterations);
        }
        Console::endProgress();

        return $count;
    }
}

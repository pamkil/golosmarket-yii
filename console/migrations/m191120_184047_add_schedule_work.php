<?php

use yii\db\Migration;

/**
 * Class m191120_184047_add_schedule_work
 */
class m191120_184047_add_schedule_work extends Migration
{
    private $scheduleWork = '{{%user_schedule_work}}';
    private $user = '{{%user}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->scheduleWork, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'type' => $this->smallInteger()->notNull()->defaultValue(4),

//            'time_1_is_week_end' => $this->boolean()->notNull()->defaultValue(false),
            'time_1_at' => $this->time(),
            'time_1_to' => $this->time(),

//            'time_2_is_week_end' => $this->boolean()->notNull()->defaultValue(false),
            'time_2_at' => $this->time(),
            'time_2_to' => $this->time(),

//            'time_3_is_week_end' => $this->boolean()->notNull()->defaultValue(false),
            'time_3_at' => $this->time(),
            'time_3_to' => $this->time(),

//            'time_4_is_week_end' => $this->boolean()->notNull()->defaultValue(false),
            'time_4_at' => $this->time(),
            'time_4_to' => $this->time(),

//            'time_5_is_week_end' => $this->boolean()->notNull()->defaultValue(false),
            'time_5_at' => $this->time(),
            'time_5_to' => $this->time(),

//            'time_6_is_week_end' => $this->boolean()->notNull()->defaultValue(false),
            'time_6_at' => $this->time(),
            'time_6_to' => $this->time(),

//            'time_7_is_week_end' => $this->boolean()->notNull()->defaultValue(false),
            'time_7_at' => $this->time(),
            'time_7_to' => $this->time(),

            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'deleted_at' => $this->timestamp(),
        ], $tableOptions);

        $this->createIndex('IX-user_schedule_work-user_id', $this->scheduleWork, 'user_id', true);

        $this->addForeignKey(
            'FK-user_schedule_work-user_id',
            $this->scheduleWork,
            'user_id',
            $this->user,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK-user_schedule_work-user_id', $this->scheduleWork);
        $this->dropTable($this->scheduleWork);
    }
}

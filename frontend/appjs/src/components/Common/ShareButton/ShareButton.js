import React, {useState, useRef, useEffect} from 'react';
import {ReactComponent as IconShare} from "../../../images/icon-share.svg";
import {ReactComponent as IconShareCollection} from "../../../images/icon-share__collection.svg";
import ReactDOM from "react-dom";
import {toast} from "react-toastify";
import copy from "copy-to-clipboard";
import {ReactComponent as IconVk} from '../../../images/vk.svg';
import {ReactComponent as IconOk} from '../../../images/ok.svg';
import {ReactComponent as IconTw} from '../../../images/tw.svg';
import {ReactComponent as IconFb} from '../../../images/fb.svg';
import {ReactComponent as IconWa} from '../../../images/wa.svg';
import {ReactComponent as IconTg} from '../../../images/tg.svg';
import {ReactComponent as IconLn} from '../../../images/link.svg';
import {textProfile} from "../../Announcer/Announcer";

const ShareButton = ({
                       disabled,
                       announcerId,
                       announcerName,
                       popup,
                       url,
                       top,
                       addClass,
                       isCollection
                     }) => {
  url = url ? url : document.location.origin + `/profile/${announcerId}`;
  const [open, setOpen] = useState(false);
  const dropdownRef = useRef();
  const buttonRef = useRef();

  const handleClickOutside = (event) => {
    const domNode = ReactDOM.findDOMNode(dropdownRef.current);
    const isInnerClick = domNode && domNode.contains(event.target);
    const ignoreClick = event.target.isEqualNode(buttonRef.current);

    if (!isInnerClick && !ignoreClick && open) {
      setOpen(false);
    }
  };

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true);

    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    }
  });

  const copyToClipboard = (url) => {
    copy(url);
    toast.info(popup || textProfile);
  }

  const isMobile = function () {
    return getComputedStyle(document.body, ':before').getPropertyValue('content') === '\"mobile\"';
  }

  return (
    <>
      <button
        ref={buttonRef}
        className={"share-button " + addClass}
        disabled={disabled}
        onClick={() => {
          if (isMobile() && navigator.share) {
            navigator.share({
              title: window.location.hostname,
              text: announcerName,
              url: url
            })
              .then(function () {
                console.log("Sharing successfully done")
              })
              .catch(function () {
                console.log("Sharing failed")
              })
          } else {
            setOpen(!open);
          }
        }}
      >
        <div className={'tooltip'}>
          <div className="icon">
            {
              isCollection ? (<IconShareCollection/>) : (<IconShare/>)
            }
            <span>Поделиться</span>
          </div>
        </div>
        {open && (
          <div ref={dropdownRef} className={'share-dropdown' + (top ? ' _top' : '')}>
            <div className={'popup__arrow popup__arrow_' + (top ? 'top' : 'bottom')}/>
            <div className="social-balloon">
              <div className="social-balloon__header">
                {/*<a*/}
                {/*  className="btn btn-default"*/}
                {/*  target="_blank"*/}
                {/*  rel="noopener noreferrer"*/}
                {/*  title="On Facebook"*/}
                {/*  href={`http://www.facebook.com/sharer.php?u=${url}`}*/}
                {/*>*/}
                {/*  <IconFb className="icon icon--social__facebook"/>*/}
                {/*</a>*/}
                <a
                  className="btn btn-default"
                  target="_blank"
                  rel="noopener noreferrer"
                  title="On Whatsapp"
                  href={`https://api.whatsapp.com/send?text=${encodeURIComponent(url)}`}
                >
                  <IconWa className="icon icon--social__whatsapp"/>
                </a>
                <a
                  className="btn btn-default"
                  target="_blank"
                  rel="noopener noreferrer"
                  title="On Telegram"
                  href={`https://t.me/share/url?text=golosmarket&url=${encodeURIComponent(url)}`}
                >
                  <IconTg className="icon icon--social__telegram"/>
                </a>
                <a
                  className="btn btn-default"
                  target="_blank"
                  rel="noopener noreferrer"
                  title="On VK.com"
                  href={`https://vk.com/share.php?url=${encodeURIComponent(url)}`}
                >
                  <IconVk className="icon icon--social__vk"/>
                </a>
                <a
                  className="btn btn-default"
                  target="_blank"
                  rel="noopener noreferrer"
                  title="On OK.com"
                  href={`https://connect.ok.ru/offer?url=${encodeURIComponent(url)}`}
                >
                  <IconOk className="icon icon--social__ok"/>
                </a>
                {/*<a*/}
                {/*  className="btn btn-default"*/}
                {/*  target="_blank"*/}
                {/*  rel="noopener noreferrer"*/}
                {/*  title="On Twitter"*/}
                {/*  href={`http://twitter.com/share?url=${url}`}*/}
                {/*>*/}
                {/*  <IconTw className="icon icon--social__twitter"/>*/}
                {/*</a>*/}
              </div>
              <div className="social-balloon__footer">
                <div onClick={() => copyToClipboard(url)} className="social-balloon__copy">
                  <IconLn className="icon icon--ui__link icon--ui__link_story"/>
                  <span>Скопировать ссылку</span></div>
              </div>
            </div>
          </div>
        )}
      </button>
    </>
  )
}

export default ShareButton

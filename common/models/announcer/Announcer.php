<?php

namespace common\models\announcer;

use common\models\access\User;
use common\models\Model;
use common\models\user\Review;
use common\models\user\ScheduleWork;
use common\service\announcer\enum\AnnouncerCostEnum;
use voskobovich\linker\LinkerBehavior;
use voskobovich\linker\updaters\ManyToManySmartUpdater;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Автор публикаций
 *
 * Class Announcer
 * @package common\models\announcer
 *
 * @property int $id
 * @property int $user_id [int(11)]
 * @property bool $script []
 * @property bool $sound_by_key [float]
 * @property int $sum [float]
 * @property string $operating_schedule [varchar(255)]  график работы
 * @property string $info
 * @property string $equipment оборудование
 * @property string $avatar [varchar(255)]
 * @property int $cost [int(11)] Стоимость до 30 сек.
 * @property int $cost_a4 [int(11)] Стоимость Начитки страницы А4
 * @property int $cost_vocal [int(11)] Стоимость вокала
 * @property int $cost_parody [int(11)] Стоимость пародии
 * @property int $cost_sound_mount [int(11)] Стоимость монтирования звука
 * @property int $cost_audio_adv [int(11)] Стоимость сценария для аудиорекламы
 * @property string $discount [varchar(255)]  Сделаю скидку
 * @property string $cash_yamoney [varchar(255)]
 * @property string $cash_sber [varchar(255)]
 * @property int $updated_at [timestamp]
 * @property int $created_at [timestamp]
 * @property int $deleted_at [timestamp]
 *
 * @property User $user
 * @property Age[] $ages
 * @property Gender[] $genders
 * @property File[] $photos
 * @property AnnouncerSound[] $sounds
 * @property File $sound
 * @property Presentation[] $presentations
 * @property Tag[] $tags
 * @property Timber[] $timbers
 * @property ScheduleWork $scheduleWork
 * @property Review $review
 * @property Review[] $reviews
 */
class Announcer extends Model
{
    public static function tableName()
    {
        return '{{%user_announcer}}';
    }

    /**
     * @deprecated
     */
    public static function table2Ages()
    {
        return '{{%user_announcer_2_age}}';
    }

    public static function table2Sounds()
    {
        return '{{%user_announcer_2_sound}}';
    }

    /**
     * @deprecated
     */
    public static function table2Genders()
    {
        return '{{%user_announcer_2_genders}}';
    }

    public static function table2Photos()
    {
        return '{{%user_announcer_2_photos}}';
    }

    public function rules()
    {
        return [
            [
                ['user_id',],
                'unique'
            ],
            [
                ['sound_by_key', 'script'],
                'boolean'
            ],
            [
                ['sound_by_key', 'script'],
                'default',
                'value' => false,
            ],
            [
                ['user_id', 'cost', 'cost_a4', 'cost_vocal', 'cost_parody', 'cost_sound_mount', 'cost_audio_adv'],
                'integer'
            ],
            [
                ['cost', 'cost_a4', 'cost_vocal', 'cost_parody', 'cost_sound_mount', 'cost_audio_adv'],
                'default',
                'value' => 0
            ],
            [
                ['operating_schedule', 'info', 'equipment', 'avatar', 'discount', 'cash_yamoney', 'cash_sber'],
                'string'
            ],
            [
                ['operating_schedule', 'info', 'equipment', 'avatar', 'discount', 'cash_yamoney', 'cash_sber'],
                'default',
                'value' => ''
            ],
            [
                ['operating_schedule', 'avatar', 'discount', 'cash_yamoney', 'cash_sber'],
                'filter',
                'filter' => function ($value) {
                    return mb_substr($value, 0, 254);
                }
            ],
            [
                [
                    'ages',
                    'genders',
                    'presentations',
//                    'timbers',
                    'photos',
                    'tags',
                ],
                'safe'
            ]
        ];
    }

    public function formName()
    {
        return '';
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            [
                'id' => 'ID',
                'quantity_favorite' => 'Количество избранных',
                'quantity_reviews' => 'Рейтинг по отзывам',
                'user_id' => 'Диктор',
                'sound_by_key' => 'Звук под ключ',
                'script' => 'Сценарий',
                'sum' => 'Сумма на счету у пользователя',
                'operating_schedule' => 'Время работы',
                'info' => 'Дополнительно',
                'equipment' => 'Оборудование',
                'avatar' => 'Фото',
                'discount' => 'Сделаю скидку',
                'cash_yamoney' => 'YandexMoney кошелёк',
                'cash_sber' => 'Сбербанк кошелёк',
                'created_at' => 'Время создания',
                'updated_at' => 'Время обновления',
            ],
            AnnouncerCostEnum::getLabels()
        );
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        $behaviors['manyToMany'] = [
            'class' => LinkerBehavior::class,
            'relations' => [
                'ages' => [
                    'ages',
                    'updater' => [
                        'class' => ManyToManySmartUpdater::class
                    ]
                ],
                'genders' => [
                    'genders',
                    'updater' => [
                        'class' => ManyToManySmartUpdater::class
                    ]
                ],
                'photos' => [
                    'photos',
                    'updater' => [
                        'class' => ManyToManySmartUpdater::class
                    ]
                ],
                'presentations' => [
                    'presentations',
                    'updater' => [
                        'class' => ManyToManySmartUpdater::class
                    ]
                ],
                'tags' => [
                    'tags',
                    'updater' => [
                        'class' => ManyToManySmartUpdater::class
                    ]
                ],
            ]
        ];

        return $behaviors;
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id'])->inverseOf('announcer');
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     * @deprecated
     */
    public function getAges()
    {
        return $this->hasMany(Age::class, ['id' => 'age_id'])
            ->viaTable(Announcer::table2Ages(), ['announcer_id' => 'id']);
    }

    public function getGenders()
    {
        return $this->hasMany(Gender::class, ['id' => 'gender_id'])
            ->viaTable(Announcer::table2Genders(), ['announcer_id' => 'id']);
    }

    public function getPhotos()
    {
        return $this->hasMany(File::class, ['id' => 'photo_id'])
            ->viaTable(Announcer::table2Photos(), ['announcer_id' => 'id']);
    }

    public function getSounds()
    {
        return $this->hasMany(AnnouncerSound::class, ['announcer_id' => 'id']);
    }

    public function getSound()
    {
        return $this->hasOne(File::class, ['id' => 'sound_id'])
            ->viaTable(self::table2Sounds(), ['announcer_id' => 'id'])
            ->orderBy(['default' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     * @deprecated
     */
    public function getPresentations()
    {
        return $this->hasMany(Presentation::class, ['id' => 'presentation_id'])
            ->viaTable('{{%user_announcer_2_presentations}}', ['announcer_id' => 'id']);
    }

    public function getTags()
    {
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])
            ->viaTable('{{%user_announcer_2_tags}}', ['announcer_id' => 'id']);
    }

    public function getScheduleWork()
    {
        return $this->hasOne(ScheduleWork::class, ['user_id' => 'user_id'])
            ->inverseOf('announcer');
    }

    public function getReview()
    {
        return $this->hasOne(Review::class, ['announcer_id' => 'user_id']);
    }

    public function getReviews()
    {
        return $this->hasMany(Review::class, ['announcer_id' => 'user_id']);
    }

    public static function getQuantityAnnouncers()
    {
        $sounds = (new Query())
            ->select('announcer_id')
            ->from(Announcer::table2Sounds());

        $genders = (new Query())
            ->select('announcer_id')
            ->from(Announcer::table2Genders());

        $announcers = Announcer::find()
            ->select(['user_id'])
            ->andWhere(['id' => $sounds])
            ->andWhere(['id' => $genders]);

        $quantityAnnouncers = User::find()
            ->andWhere(
                [
                    'id' => $announcers,
                    'is_blocked' => 0,
                    'status' => User::STATUS_ACTIVE,
                    'type' => User::TYPE_ANNOUNCER,
                ]
            );

        return $quantityAnnouncers->count();
    }

    /**
     * Check in announcer sounds vocal presentation
     *
     * @return bool
     */
    public function vocalExists(): bool
    {
        foreach ($this->sounds as $sound) {
            if ($sound->presentation->code === 'vocals') {
                return true;
            }
        }

        return false;
    }

    /**
     * Check in announcer sounds parodist presentation
     *
     * @return bool
     */
    public function parodistExists(): bool
    {
        foreach ($this->sounds as $sound) {
            if ($sound->presentation->code === 'parodist') {
                return true;
            }
        }

        return false;
    }
}

<?php

namespace api\modules\v1\actions\user;

use api\modules\v1\actions\common\IndexAction;
use Yii;
use yii\data\ArrayDataProvider;
use yii\rbac\Role;

class Roles extends IndexAction
{
    /**
     * Список ролей
     * @OA\Get(
     *     path="/user/roles",
     *     summary="Login roles fo users. Получение списка ролей",
     *     tags={"User"},
     *     security={{"BearerAuth": {}}},
     *     @OA\Response(
     *         response="200",
     *         description="Объект токена",
     *         @OA\JsonContent(ref="#/components/schemas/Token")
     *     )
     * )
     *
     * @return mixed|null
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        $queryParams = Yii::$app->getRequest()->getQueryParams();
        if (empty($requestParams)) {
            $requestParams = $queryParams;
        }

        $authManager = Yii::$app->getAuthManager();
        $roles = $authManager->getRoles();
        /* @var $modelClass Role */
        return \Yii::createObject([
            'class' => ArrayDataProvider::class,
            'modelClass' => reset($roles),
            'allModels' => $roles,
            'pagination' => [
                'params' => $requestParams,
            ],
            'sort' => [
                'params' => $requestParams,
            ],
        ]);
    }
}

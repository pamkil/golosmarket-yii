import React, { useCallback, useEffect, useState } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { toast, ToastContainer } from "react-toastify";
import addNotification from "react-push-notification";
import * as rdd from "react-device-detect";

import {
  clearUser,
  CreateReviewService,
  CreateUserReview,
  getPaymentInfo,
  updatePayment,
  getProfile,
  logout,
  read,
  setShowWorkTime,
  updateSchedule,
} from "../../../data/redux";

import toastMp3 from "../../../images/chat.mp3";
import msgIco from "../../../images/mail.ico";
import { ReactComponent as LetterImg } from "../../../images/letter.svg";
import { ReactComponent as BellImg } from "../../../images/bell.svg";
import closeIcon from "../../../images/close.svg";
import ProfileMenu from "../ProfileMenu/ProfileMenu";
import Modal from "../../Common/Modal/Modal";
import WorkTimeForm from "../Form/WorkTimeForm";
import ReviewServiceForm from "../Form/ReviewServiceForm";

import "./headerPanel.css";
import ReviewUserForm from "../Form/ReviewUserForm";
import NotifyCount from "../../Common/NotifyCount/NotifyCount";
import { NOTIFY_TYPE } from "./NotifyTypes";
import { ReactComponent as IconHeart } from "../../../images/icon-heart.svg";
import usePrevious from "../../../data/helpers/usePrevious";

const APP_MIN_HEIGHT = 300;
const CHAT_ROUTE = "/chat";

const Notify = ({
  item: { text, type, id, other_id },
  onClose,
  onReview,
  onShow,
}) => {
  let color = "";
  switch (type) {
    case "bill":
      color = "green";
      break;
    case "review":
      color = "yellow";
      break;
    case "no_paid_bill":
      color = "red";
      break;
    case "review_send":
      color = "blue";
      break;
    default:
      color = "gray";
      break;
  }

  const className = `notice warning-${color}`;

  return (
    <li className={className}>
      <div className="message-container">
        <span className="text">{text}</span>
        {[
          NOTIFY_TYPE.TYPE_REVIEW_SEND,
          NOTIFY_TYPE.TYPE_REVIEW_SERVICE_CLIENT,
        ].includes(type) ? (
          <div>
            <button
              onClick={onReview}
              data-id={id}
              data-other={other_id}
              data-notify={type}
            >
              Оставить отзыв
            </button>
          </div>
        ) : other_id &&
          [
            NOTIFY_TYPE.TYPE_REVIEW,
            NOTIFY_TYPE.TYPE_BILL,
            NOTIFY_TYPE.TYPE_NEWS,
          ].includes(type) ? (
          <button
            onClick={onShow}
            data-id={id}
            data-other={other_id}
            data-notify={type}
          >
            Посмотреть
          </button>
        ) : null}
      </div>
      <div className="close" onClick={onClose} data-id={id}>
        <img src={closeIcon} className="notify__close" alt="Закрыть" />
      </div>
    </li>
  );
};

const HeaderPanel = ({
  chatOpponent = null,
  amountCash = null,
  quantityMessages = 0,
  quantityNotifications = 0,
  notifyMessages = [],
  logout,
  read,
  getPaymentInfo,
  getProfile,
  updateSchedule,
  CreateUserReview,
  CreateReviewService,
  user,
  clearUser,
  avatarImg,
  initialValues,
  favCount,
  checkFav,
  showWorkTime,
  hideShowWorkTime,
}) => {
  const dispatch = useDispatch();
  const msgSound = new Audio(toastMp3);
  const titleMessageArray = ["Уведомление", "Голосмаркет"];
  const [titleAlarm, setTitleAlarm] = useState(false);
  const [titleMessageCounter, setTitleMessageCounter] = useState(0);
  const [titleBackup, setTitleBackup] = useState(document.title);
  const [showMenu, setShowMenu] = useState(false);
  const [showNotify, setShowNotify] = useState(false);
  const [showReviewUserForm, setShowReviewUserForm] = useState(false);
  const [showReviewServiceForm, setShowReviewServiceForm] = useState(false);
  const [opponent, setOpponent] = useState(null);
  const requests = useCallback(() => {
    getProfile();
  }, [getProfile]);

  const appHeight = () => {
    const doc = document.documentElement;
    const header = document.querySelector(".header");
    const sab =
      parseInt(
        getComputedStyle(document.documentElement).getPropertyValue("--sab"),
        10
      ) || 0;
    doc.style.setProperty(
      "--app-height",
      `${Math.max(APP_MIN_HEIGHT, window.innerHeight - sab)}px`
    );
    doc.style.setProperty(
      "--app-header-height",
      `${header.getBoundingClientRect().height}px`
    );
  };

  useEffect(() => {
    window.addEventListener("resize", appHeight);

    appHeight();

    return () => {
      window.removeEventListener("resize", appHeight);
    };
  }, []);

  useEffect(() => {
    requests();
    const timer = setTimeout(() => requests, 30000);

    return () => clearTimeout(timer);
  }, [requests]);

  const prevQuantityMessages = usePrevious(quantityMessages);
  const prevQuantityNotifications = usePrevious(quantityNotifications);

  const toastMsg = (
    <a href={CHAT_ROUTE} className={"Toastify__toast-link"}>
      <LetterImg />
      <div>
        <b>Новое сообщение</b>
        <br />
        <span>Зайдите в чат</span>
      </div>
    </a>
  );

  const updateFavicon = (ico) => {
    let favicon = document.querySelector('link[rel="icon"]');
    let shortcutIcon = document.querySelector('link[rel="shortcut icon"]');

    if (favicon) {
      favicon.setAttribute("href", ico);
    }
    if (shortcutIcon) {
      shortcutIcon.setAttribute("href", ico);
    }
  };

  let titleTimer;

  useEffect(() => {
    const updateCount = () => {
      titleTimer =
        !titleTimer &&
        setInterval(() => {
          setTitleMessageCounter((prevCount) =>
            prevCount < titleMessageArray.length - 1 ? prevCount + 1 : 0
          );
        }, 2000);
    };

    if (titleAlarm) {
      updateCount();
      updateFavicon("/favicon_m.ico");
    } else {
      clearInterval(titleTimer);
      document.title = titleBackup;
      updateFavicon("/favicon.ico");
    }

    return () => clearInterval(titleTimer);
  }, [titleAlarm]);

  useEffect(() => {
    document.title = titleAlarm
      ? titleMessageArray[titleMessageCounter]
      : titleBackup;
  }, [titleMessageCounter, titleAlarm]);

  useEffect(() => {
    const showNotification = (notificationPermission) => {
      if (
        !(
          (chatOpponent && chatOpponent.hasOwnProperty("id")) ||
          window.location.pathname.indexOf(CHAT_ROUTE) > -1
        )
      ) {
        if (
          prevQuantityMessages !== void 0 &&
          quantityMessages > prevQuantityMessages
        ) {
          if (notificationPermission && rdd.isDesktop) {
            addNotification({
              title: "Новое сообщение",
              subtitle: "subtitle",
              message: "Зайдите в чат",
              theme: "darkblue",
              icon: msgIco,
              duration: 3000,
              onClick: () => {
                window.location.pathname = CHAT_ROUTE;
              },
              native: true, // when using native, your OS will handle theming.
            });
          } else {
            msgSound.play();
            toast.info(toastMsg);
          }
        }
      }
    };

    if ("Notification" in window) {
      Notification.requestPermission().then((outcome) => {
        showNotification(outcome === "granted");
      });
    } else {
      showNotification(false);
    }

    setTitleAlarm(quantityNotifications + quantityMessages > 0);
  }, [quantityMessages, chatOpponent]);

  // useEffect(() => {
  //   if ((quantityMessages > prevQuantityMessages)) {
  //     //if (rdd.isDesktop) {
  //     //  addNotification({
  //     //    title: 'Новое уведомление',
  //     //    subtitle: 'subtitle',
  //     //    message: 'Зайдите в чат',
  //     //    theme: 'darkblue',
  //     //    icon: msgIco,
  //     //    duration: 3000,
  //     //    native: false // when using native, your OS will handle theming.
  //     //  });
  //     //} else {
  //     //  toast.info('Новое уведомление');
  //     //}

  //   }

  //   // setTitleAlarm((quantityNotifications + quantityMessages) > 0);
  // }, [quantityNotifications]);

  useEffect(() => {
    if (!user) {
      return;
    }
    if (user.isAnnouncer) {
      const elements = document.getElementsByClassName(
        "hide-to-message-announcer"
      );
      Array.prototype.forEach.call(elements, (elem) => {
        elem.remove();
      });
    }
  }, [user]);

  useEffect(() => {
    const rejectedNotifies = getStorageRejectedNotifies();
    const reviewSendNotify = notifyMessages.find(({ id, type }) => {
      return (
        type === NOTIFY_TYPE.TYPE_REVIEW_SEND && !rejectedNotifies.includes(id)
      );
    });
    const reviewServiceNotify = notifyMessages.find(({ id, type }) => {
      return (
        type === NOTIFY_TYPE.TYPE_REVIEW_SERVICE_CLIENT &&
        !rejectedNotifies.includes(id)
      );
    });

    if (reviewSendNotify) {
      setOpponent({
        otherId: reviewSendNotify.other_id,
        notifyId: reviewSendNotify.id,
        ...reviewSendNotify.opponent,
      });
      setShowReviewUserForm(true);
    } else if (reviewServiceNotify) {
      setOpponent({ notifyId: reviewServiceNotify.id });
      setShowReviewServiceForm(true);
    }
    if (notifyMessages.length === 0) {
      setShowNotify(false);
    }
  }, [notifyMessages]);

  const clickShowHandler = () => {
    setShowMenu((showMenu) => !showMenu);
  };

  const closeHandler = ({ target }) => {
    const id = target.dataset.id;
    read([id]);
  };

  const reviewHandler = ({ target }) => {
    const notifyId = parseInt(target.dataset.id);
    const otherId = parseInt(target.dataset.other);
    const type = target.dataset.notify;
    const current = notifyMessages.find((item) => item.id === notifyId);

    if (type === NOTIFY_TYPE.TYPE_REVIEW_SERVICE_CLIENT) {
      setOpponent({ notifyId });
      setShowReviewServiceForm(true);
    } else if (type === NOTIFY_TYPE.TYPE_REVIEW_SEND) {
      setOpponent({ otherId, notifyId, ...current.opponent });
      setShowReviewUserForm(true);
    }
  };

  const reviewByTimerHandler = (reviewMessagedata) => {
    const notifyId = reviewMessagedata.id;
    const otherId = reviewMessagedata.other_id;
    const type = reviewMessagedata.type;
    const current = notifyMessages.find((item) => item.id === notifyId);

    if (type === NOTIFY_TYPE.TYPE_REVIEW_SERVICE_CLIENT) {
      setOpponent({ notifyId });
      setShowReviewServiceForm(true);
    } else if (type === NOTIFY_TYPE.TYPE_REVIEW_SEND) {
      setOpponent({ otherId, notifyId, ...current.opponent });
      setShowReviewUserForm(true);
    }
  };

  const showHandler = ({ target }) => {
    const otherId = parseInt(target.dataset.other);
    const type = target.dataset.notify;

    let url = "";
    if (type === NOTIFY_TYPE.TYPE_BILL) {
      url = `${CHAT_ROUTE}/${otherId}`;
    } else if (type === NOTIFY_TYPE.TYPE_REVIEW) {
      url = `/profile/${otherId}/review`;
    } else if (type === NOTIFY_TYPE.TYPE_NEWS) {
      url = `/news`;
    }

    if (url.length) {
      window.location = url;
    }
  };

  const clickShowNotifyHandler = () => {
    if (showNotify || notifyMessages.length > 0) {
      setShowNotify((showNotify) => !showNotify);
    }
  };

  const logoutHandler = () => {
    logout();
    clearUser();
    window.location = "/";
  };

  const showWorkTimeHandler = () => {
    dispatch(setShowWorkTime(true));
  };

  const workTimeFormSubmitHandler = (values) => {
    updateSchedule(values);
    hideShowWorkTime();

    toast.info("Изменения вступили в силу. Скоро данные появятся в профиле");
  };

  const reviewUserFormSubmitHandler = (values) => {
    values.opponent_id = opponent.id || null;
    values.other_id = opponent.otherId || null;
    CreateUserReview(values);
    setShowReviewUserForm(false);
    read([opponent.notifyId]);

    toast.info("Отзыв успешно отправлен");
  };

  const reviewServiceFormSubmitHandler = (values) => {
    setShowReviewServiceForm(false);
    read([opponent.notifyId]);
    CreateReviewService(values);

    toast.info("Отзыв успешно отправлен");
  };

  const handleCloseReviewForm = (closeFunction = () => {}) => {
    const rejectedNotifies = getStorageRejectedNotifies();

    rejectedNotifies.push(opponent.notifyId);

    localStorage.setItem(
      "rejected-notifies",
      JSON.stringify(Array.from(new Set(rejectedNotifies)))
    );
    closeFunction(false);
  };

  const getStorageRejectedNotifies = () => {
    const data = localStorage.getItem("rejected-notifies");

    try {
      return JSON.parse(data) || [];
    } catch (e) {
      return [];
    }
  };
  // call when user click on "notify later"
  const createUserReviewFormNotificationTimer = () => {
    // put current time to localstorage

    let notifiesTimers = localStorage.getItem("notificationReviewTimer");
    if (notifiesTimers === null || JSON.parse(notifiesTimers).length === 0) {
      localStorage.setItem(
        "notificationReviewTimer",
        JSON.stringify([
          {
            date: Date.now(),
            opponent: opponent,
          },
        ])
      );
    } else {
      notifiesTimers = JSON.parse(notifiesTimers);
      notifiesTimers.push({
        date: Date.now(),
        opponent: opponent,
      });
      localStorage.setItem(
        "notificationReviewTimer",
        JSON.stringify(Array.from(new Set(notifiesTimers)))
      );
    }
    setShowReviewUserForm(false);
  };

  const createUserReviewServiceFormNotificationTimer = () => {
    // put current time to localstorage
    handleCloseReviewForm(setShowReviewServiceForm);
    localStorage.setItem("notificationReviewServiceFormTimer", {
      date: Date.now(),
      opponentNotifyId: opponent.notifyId,
    });
  };

  useEffect(() => {
    let notificationReviewTimer = JSON.parse(
      localStorage.getItem("notificationReviewTimer")
    );
    const notificationReviewServiceTimer = JSON.parse(
      localStorage.getItem("notificationReviewServiceTimer")
    );

    if (notificationReviewTimer !== null) {
      if (notificationReviewTimer.length !== 0 && notifyMessages.length) {
        notificationReviewTimer.find((reviewTimer) => {
          const sendNotification = Date.now() - reviewTimer.date > 86400000;
          const reviewSendNotify = notifyMessages.find(({ id, type }) => {
            return (
              type === NOTIFY_TYPE.TYPE_REVIEW_SEND &&
              reviewTimer.opponent.notifyId === id
            );
          });

          if (sendNotification && reviewSendNotify) {
            notificationReviewTimer = notificationReviewTimer.filter(
              (rejectedNotify) => {
                return rejectedNotify.opponent.notifyId !== reviewSendNotify.id;
              }
            );

            localStorage.setItem(
              "notificationReviewTimer",
              JSON.stringify(Array.from(new Set(notificationReviewTimer)))
            );

            reviewByTimerHandler(reviewSendNotify);
          }
        });
      }
    }
  }, [notifyMessages]);
  return (
    <div className="profile-info">
      <Modal show={showWorkTime} onCloseRequest={hideShowWorkTime}>
        <WorkTimeForm
          onSubmit={workTimeFormSubmitHandler}
          initialValues={initialValues}
        />
      </Modal>

      <Modal
        show={showReviewUserForm}
        onCloseRequest={() => handleCloseReviewForm(setShowReviewUserForm)}
      >
        <ReviewUserForm
          isAnnouncer={user.isAnnouncer}
          opponent={opponent}
          onSubmit={reviewUserFormSubmitHandler}
          createUserReviewFormNotificationTimer={
            createUserReviewFormNotificationTimer
          }
        />
      </Modal>

      <Modal
        show={showReviewServiceForm}
        onCloseRequest={() => handleCloseReviewForm(setShowReviewServiceForm)}
      >
        <ReviewServiceForm onSubmit={reviewServiceFormSubmitHandler} />
      </Modal>

      <div className="item">
        <div className="warning-list">
          <a
            href="/favorite"
            className={"favorites-btn"}
            onClick={(e) => checkFav(e)}
          >
            <span className={"tablet-no"}>Избранные голоса</span>
            <div className={"tooltip tooltip__bottom"}>
              <div className="icon">
                <IconHeart />
                <span>Перейти&nbsp;в&nbsp;избранные&nbsp;голоса</span>
              </div>
            </div>
            <NotifyCount count={favCount} />
          </a>
        </div>
      </div>

      <div className="item">
        <div className="warning-list">
          <a href={CHAT_ROUTE}>
            <div className={"tooltip tooltip__bottom"}>
              <div className="icon">
                <span>Список&nbsp;чатов</span>
                <LetterImg />
              </div>
            </div>
            <NotifyCount count={quantityMessages} />
          </a>
        </div>
      </div>

      <div className="item">
        <div className="warning-list">
          <button
            type="button"
            onClick={clickShowNotifyHandler}
            className="warning-list-btn"
          >
            <div className={"tooltip tooltip__bottom"}>
              <div className="icon">
                <BellImg />
                <span>Уведомления</span>
              </div>
            </div>
            <NotifyCount count={quantityNotifications} />
          </button>
          {showNotify && (
            <div className="warning">
              <div className="head">Уведомления</div>
              <ul className="notifies__list">
                {notifyMessages.map((item) => (
                  <Notify
                    item={item}
                    onClose={closeHandler}
                    onReview={reviewHandler}
                    onShow={showHandler}
                    key={item.id}
                  />
                ))}
              </ul>
            </div>
          )}
        </div>
      </div>
      <div className="item" onClick={clickShowHandler}>
        <ProfileMenu
          updatePayment={updatePayment}
          paidBills={user.paidBills}
          avatarImg={avatarImg}
          showMenu={showMenu}
          cash={amountCash}
          yamoney={user.yamoney}
          name={user.firstname || user.username}
          isAnnouncer={user.isAnnouncer}
          id={user.id}
          onLogout={logoutHandler}
          showWorkTimeHandler={showWorkTimeHandler}
        />
      </div>
      {/*<ToastContainer/>*/}
    </div>
  );
};

const mapStateToProps = ({ user, app, chat }) => {
  const profile = user.user || {
    newNotifications: { quantity: 0, messages: [] },
  };
  return {
    user: profile,
    chatOpponent: chat.message.opponent,
    amountCash: profile.amountCash,
    quantityNotifications: profile.newNotifications.quantity,
    notifyMessages: profile.newNotifications.messages,
    quantityMessages: profile.newMessages,
    avatarImg: profile.avatar,
    initialValues: profile.scheduleWork,
  };
};

export default connect(mapStateToProps, {
  logout,
  getProfile,
  updateSchedule,
  getPaymentInfo,
  read,
  CreateUserReview,
  clearUser,
  CreateReviewService,
})(HeaderPanel);

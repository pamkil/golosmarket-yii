<?php

use admin\models\bill\BillSearch;
use common\models\access\User;
use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var ActiveDataProvider $dataProvider
 * @var BillSearch $searchModel
 * @var int $percent
 */
?>

<div class="box box-primary">
    <div class="box-body">
        <?= GridView::widget(
            [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    'id:integer:ИД',
                    [
                        'label' => 'Диктор',
                        'attribute' => 'announcer',
                        'width' => '310px',
                        'value' => function ($user) {
                            return "{$user->lastname} {$user->firstname} ({$user->email})";
                        },
                        'filter' => false,
                        'group' => true,
                    ],
                    [
                        'label' => 'Кошелёк',
                        'attribute' => 'announcer.cash_yamoney',
                        'content' => function (User $user) {
                            return $user->announcer->cash_yamoney;
                        },
                    ],
                    [
                        'label' => 'Сумма исполнителю',
                        'attribute' => 'announcer.cash_yamoney',
                        'content' => function (User $user) use ($percent) {
                            $sum = 0.;
                            foreach ($user->bills as $bill) {
                                $sum += ($bill->ya_amount ?? $bill->sum) * (100 - $percent) / 100;
                            }

                            return Yii::$app->formatter->asCurrency($sum);
                        },
                    ],
                    [
                        'label' => 'Подробно',
                        'attribute' => 'announcer.cash_yamoney',
                        'content' => function (User $user) use ($percent) {
                            $billEc = [];
                            foreach ($user->bills as $bill) {
                                $sum = ($bill->ya_amount ?? $bill->sum) * (100 - $percent) / 100;
                                $sum = Yii::$app->formatter->asCurrency($sum);
                                $date = Yii::$app->formatter->asDatetime($bill->ya_datetime ?? $bill->date);
                                $billEc[] = "<span><b>$sum</b> ($date)</span>";
                            }

                            return implode('<br>', $billEc);
                        },
                    ],
                    [
                        'label' => 'Сумма сайту',
                        'attribute' => 'announcer.cash_yamoney',
                        'content' => function (User $user) use ($percent) {
                            $sum = 0.;
                            foreach ($user->bills as $bill) {
                                $sum += ($bill->ya_amount ?? $bill->sum) - ($bill->ya_amount ?? $bill->sum) * (100 - $percent) / 100;
                            }

                            return Yii::$app->formatter->asCurrency($sum);
                        },
                    ],
                    [
                        'label' => 'Сумма сайту',
                        'attribute' => 'announcer.cash_yamoney',
                        'content' => function (User $user) use ($percent) {
                            $sum = 0.;
                            $nums = [];
                            foreach ($user->bills as $bill) {
                                $sum += ($bill->ya_amount ?? $bill->sum) * (100 - $percent) / 100;
                                $nums[] = $bill->id;
                            }

                            if (!$user->announcer) {
                                return '';
                            }

                            $paramsUrl = [
                                'account' => $user->announcer->cash_yamoney,
                                'targets' => 'golosmarket (' . implode('-', $nums) . ')',
                                'label' => 'golosmarket',
                                'quickpay' => "small",
                                'any-card-payment-type' => "on",
                                'button-text' => "11",
                                'button-size' => "s",
                                'button-color' => "black",
                                'default-sum' => $sum,
                                'successURL' => Url::toRoute(
                                    ['view', 'id' => $user->id, 'ids' => implode('-', $nums)],
                                    true
                                )
                            ];
                            $str = http_build_query($paramsUrl);

                            $url = "https://yoomoney.ru/embed/small.xml?$str";

                            return Html::tag(
                                'iframe',
                                '',
                                [
                                    'title' => 'bill',
                                    'frameBorder' => '0',
                                    'scrolling' => 'no',
                                    'width' => '127',
                                    'height' => '25',
                                    'src' => $url
                                ]
                            );
                        },
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{update}',
                        'updateOptions' => ['label' => '<i class="glyphicon glyphicon-ruble" title="Оплатить"></i>'],
                        'buttons' => [
                            'update' => function ($url, User $model) {
                                $ids = [];
                                foreach ($model->bills as $bill) {
                                    $ids[] = $bill->id;
                                }

                                return Html::a(
                                    '<span class="glyphicon glyphicon-envelope" title="Отметить оплату вручную"></span>',
                                    [
                                        'update',
                                        'id' => $model->id,
                                    ],
                                    [
                                        'data' => [
                                            'confirm' => 'Вы уверены что хотите отметить оплату?',
                                            'form' => 'none',
                                            'method' => 'post',
                                            'pjax' => '1',
                                            'pjax-push' => '0',
                                            'params' => [
                                                'ids' => $ids
                                            ],
                                        ]
                                    ]
                                );
                            }
                        ]
                    ],
                ]
            ]
        ); ?>
    </div>
</div>

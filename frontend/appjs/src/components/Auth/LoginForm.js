import React from "react";
import { Field, reduxForm } from 'redux-form';
import { FORMS } from '../../data/constants'
import InputInline from "../InputInline/InputInline";

const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = 'Необходимо заполнить';
    }
    if (!values.password) {
        errors.password = 'Необходимо заполнить';
    }
    return errors;
};

let LoginForm = ({ handleSubmit, pristine, submitting, valid }) => (
    <form onSubmit={ handleSubmit } className="modal-form">
        <div className="item">
            <div className="block full-width">
                <Field
                    name="email"
                    component={ InputInline }
                    type="email"
                    placeholder="E-mail"
                />
            </div>
        </div>
        <div className="item">
            <div className="block full-width">
                <Field
                    name="password"
                    component={ InputInline }
                    type="password"
                    placeholder="Пароль"
                />
            </div>
        </div>
        {/*<div className="item">*/}
        {/*    <div className="block full-width">*/}
        {/*        <Field*/}
        {/*            name="other"*/}
        {/*            component={ InputInline }*/}
        {/*            type="checkbox"*/}
        {/*        /> Чужой компьютер*/}
        {/*    </div>*/}
        {/*</div>*/}
        <div className="item">
            <button
                type="submit"
                disabled={ pristine || submitting }
                className={ !valid ? 'error-submit' : '' }
            >
                Войти
            </button>
        </div>
    </form>
);

LoginForm = reduxForm({
    form: FORMS.LOGIN, validate
})(LoginForm);

export default LoginForm;

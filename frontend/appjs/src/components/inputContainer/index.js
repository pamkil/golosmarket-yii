import React from "react";
import classNames from "classnames";

const InputInlineContainer = ({ input, label, type, placeholder, className, classNameContainer, meta: { touched, error }, ...rest }) => (
    <div className={ classNames('item', { 'has-error': touched && error }, classNameContainer) }>
        {/*<label className="col-sm-2 control-label">*/}
        {/*    { label }*/}
        {/*</label>*/}
        <div className="block">
            <input
                type={ type }
                // className={ classNames("form-control", className) }
                placeholder={ placeholder || label }
                { ...input }
                { ...rest }
            />
            {/*{ touched && error && <span className="help-block">{ error }</span> }*/}
        </div>
    </div>
);

export default InputInlineContainer;



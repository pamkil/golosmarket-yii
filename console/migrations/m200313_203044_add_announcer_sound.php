<?php

use yii\db\Migration;

/**
 * Class m200313_203044_add_announcer_sound
 */
class m200313_203044_add_announcer_sound extends Migration
{
    private $file = '{{%file}}';
    private $announcer2sound = '{{%user_announcer_2_sound}}';
    private $announcer = '{{%user_announcer}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->announcer2sound, [
            'announcer_id' => $this->integer(),
            'sound_id' => $this->integer(),
            'PRIMARY KEY (announcer_id, sound_id)'
        ], $tableOptions);

        $this->createIndex(
            'IX-user_announcer_2_sound-announcer_id',
            $this->announcer2sound,
            'announcer_id'
        );
        $this->addForeignKey(
            'FK-user_announcer_2_sound-announcer_id',
            $this->announcer2sound,
            'announcer_id',
            $this->announcer,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('IX-user_announcer_2_sound-sound_id', $this->announcer2sound, 'sound_id');
        $this->addForeignKey(
            'FK-user_announcer_2_sound-sound_id',
            $this->announcer2sound,
            'sound_id',
            $this->file,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->announcer2sound);
    }
}

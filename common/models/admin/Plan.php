<?php

namespace common\models\admin;

use Carbon\Carbon;
use common\models\Model;

/**
 * Class Answer
 *
 * @package common\models\content
 *
 * @property int $id
 * @property int $date_plan
 * @property string $value
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 */
class Plan extends Model
{
    public static function tableName()
    {
        return '{{%admin_plans}}';
    }

    public function rules()
    {
        return [
            [
                ['value', 'date_plan'],
                'required'
            ],
            [
                ['value'],
                'integer'
            ],
            [
                ['date_plan'],
                'filter',
                'filter' => function ($value) {
                    return Carbon::parse($value)->startOfMonth()->toDateTimeString();
                }
            ],
            [
                ['date_plan'],
                'unique',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'План, %',
            'date_plan' => 'Месяц',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }
}
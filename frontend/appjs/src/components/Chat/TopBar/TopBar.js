import React, { useEffect, useMemo, useState } from "react";
import { ReactComponent as Favorite } from "../../../images/favorite.svg";
import downloadImg from "../../../images/download.svg";
import copyImg from "../../../images/copy.svg";
import { ReactComponent as LeftImg } from "../../../images/left-arrow.svg";
import Price from "../../Common/Price/Price";
import { ReactComponent as FavoriteRed } from "../../../images/favorite-red.svg";
import Bill from "../Bill/Bill";
import CreateBill from "../Bill/CreateBill/CreateBill";
import { eventPlay, eventStop } from "../../Common/PlayerEvent/playerEvent";
import pause from "../../../images/pause.png";
import play from "../../../images/play.png";
import { PLAYER } from "../../Common/PlayerEvent/playerConst";
import isEmpty from "lodash/isEmpty";
import "./TopBar.css";
import { useDispatch, useSelector } from "react-redux";
import { announcer, getProfileId } from "../../../data/redux";

export default function TopBar({
  addToFavorite,
  opponent,
  isAnnouncer,
  createBill,
  cancelBill,
  updateBill,
  payedBill,
  bill,
  copyHandler,
  onBackClick,
  srvr,
}) {
  const dispatch = useDispatch();
  const { isInit, userProfile } = useSelector((state) => {
    return {
      isInit: state.app.initialized,
      userProfile: state.profile.userProfile,
    };
  });
  const sound = useMemo(() => {
    if (opponent && opponent.sound && opponent && opponent.sound.length) {
      return opponent.sound[0];
    }

    return null;
  }, [opponent]);
  const [playing, setPlaying] = useState("");
  useEffect(() => {
    document.addEventListener(PLAYER.PLAYING, playingHandler);
    document.addEventListener(PLAYER.STOP, stopHandler);

    return () => {
      document.removeEventListener(PLAYER.PLAYING, playingHandler);
      document.removeEventListener(PLAYER.STOP, stopHandler);
    };
  }, []);

  useEffect(() => {
    if (isInit && opponent) {
      dispatch(
        getProfileId(
          opponent.id + (isAnnouncer === "true" ? "&expand=recommended" : "&expand=recommended,billsInfo")
        )
      );
    }
  }, [isInit, opponent]);

  if (!opponent) {
    return <div className="chat-top" />;
  }


  const playHandler = () => {
    eventPlay(opponent, sound, opponent.sound, false);
  };
  const stoppingHandler = () => {
    eventStop();
  };

  const stopHandler = () => {
    setPlaying("");
  };

  const playingHandler = ({ detail: { file } }) => {
    setPlaying(file.url);
  };

  return (
    <section className="chat-top">
      <div className="chat-top__row">
        <div className="chat-top__item">
          <div className="chat-top__back-icon">
            <LeftImg onClick={onBackClick} />
          </div>
          <a
            href={`/profile/${opponent.id}`}
            className="chat-top__profile-link"
          >
            <div className="chat-top__avatar">
              <img
                className="chat-top__avatar-img"
                src={srvr + opponent.avatar}
                alt={opponent && opponent.name}
              />
              {opponent.isOnline && <div className="link-icon" />}
            </div>
            <div className="chat-top++avatar-desc">
              <div className="chat-top__avatar-name">
                {opponent && opponent.name}
              </div>
              {!isAnnouncer && (
                <div className="chat-top__avatar-price">
                  {opponent && opponent.scheduleWorkText
                    ? opponent.scheduleWorkText
                    : ""}
                </div>
              )}
            </div>
          </a>
        </div>
        <div className="chat-top__item play-chat-block">
          {sound &&
            (playing ? (
              <img src={pause} alt="Остановить" onClick={stoppingHandler} />
            ) : (
              <img src={play} alt="Воспроизвести" onClick={playHandler} />
            ))}
        </div>
        <div className="chat-top__item">
          {/* {!isAnnouncer && (
            <span
              className="player-button player-button--like"
              onClick={() => addToFavorite(opponent.id, opponent.isFavorite)}
            >
              {opponent.isFavorite ? <FavoriteRed /> : <Favorite />}
            </span>
          )} */}
          {!!Object.keys(userProfile).length && (
            <div>
              {userProfile.isAnnouncer ? (
                <ul class="service-list">
                  <li>
                    <div class="service-name desktop">Начитка до 30 сек.</div>
                    <div class="service-name mobile">до 30 сек.</div>
                    <div class="service-price">
                      <p className="desktop">от</p> <b>{userProfile.announcer.cost}Р</b>
                    </div>
                  </li>
                  <li>
                  <div class="service-name desktop">Начитка 1 страницы</div>
                    <div class="service-name mobile">1 стр</div>
                    <div class="service-price">
                    <p className="desktop">от</p> <b>{userProfile.announcer.cost_a4}Р</b>
                    </div>
                  </li>
                </ul>
              ) : (
                <div>
                <p className="orders-text"> {userProfile.billsInfo.ordersCount > 0 ? "Заказов "+ userProfile.billsInfo.ordersCount : ""}</p>
                <p className="orders-text"> {userProfile.billsInfo.ordersCount > 0 ? 'Оплачено' + Math.ceil(100 * (parseInt(userProfile.billsInfo.paidOrdersCount) / parseInt(userProfile.billsInfo.ordersCount))) + '%' : 'Нет рейтинга. Рекомендуем работать по предоплате'}</p>
                </div>
              )}
            </div>
          )}
          {/* {
                        sound &&
                        <>
                            <a
                                className="player-button player-button--download"
                                download={ sound.name }
                                target="_blank"
                                href={ sound.url }
                                rel="noopener noreferrer"
                            >
                                <img src={ downloadImg } alt="скачать" title="Скачать звуковую дорожку"/>
                            </a>
                            <span
                                className="player-button player-button--copy"
                                onClick={ () => copyHandler(sound.url) }
                            >
                                <img src={ copyImg } alt="копировать" title="Скопировать ссылку на файл"/>
                            </span>
                        </>
                    } */}
        </div>
      </div>
      {!isEmpty(opponent) ? (
        isAnnouncer ? (
          <CreateBill
            createBill={bill && !bill.isPaid ? updateBill : createBill}
            payedBill={payedBill}
            bill={bill}
            cancelBill={cancelBill}
          />
        ) : (
          <Bill bill={bill} payedBill={payedBill} cancelBill={cancelBill} />
        )
      ) : null}
    </section>
  );
}

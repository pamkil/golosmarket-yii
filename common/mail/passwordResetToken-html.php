<?php

use yii\helpers\Html;

/* @var $username string */
/* @var $resetLink string */


?>
<div class="password-reset">
    <p>Здравствуйте, <?= Html::encode($username) ?>!</p>
    <p>Перейдите по ссылке для создания нового пароля:</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>

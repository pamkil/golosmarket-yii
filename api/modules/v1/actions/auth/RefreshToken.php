<?php

namespace api\modules\v1\actions\auth;

use api\modules\v1\models\checks\auth\RefreshForm;
use Yii;
use yii\base\Action;

class RefreshToken extends Action
{
    /**
     * Авторизация
     * @OA\Post(
     *      path="/auth/refresh",
     *      summary="Refresh access token for user. Получение токена по токену восстановления",
     *      tags={"Auth"},
     *      @OA\RequestBody(
     *          description="Токен восстановления",
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/RefreshForm")
     *      ),
     *    @OA\Response(
     *        response="200",
     *        description="Объект токена",
     *        @OA\JsonContent(ref="#/components/schemas/Token")
     *    )
     * )
     *
     * @return mixed|null
     * @throws \yii\base\Exception
     */
    public function run()
    {
        $model = new RefreshForm();
        $model->load(Yii::$app->request->bodyParams, '');

        if ($token = $model->refresh()) {
            return $token;
        } else {
            return $model;
        }
    }
}

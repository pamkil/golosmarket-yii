<?php

namespace api\modules\v1\actions\user;

use api\modules\v1\actions\common\UpdateAction;
use api\modules\v1\models\checks\user\AnnouncerForm;
use api\modules\v1\models\checks\user\UpdateForm;
use api\modules\v1\models\records\announcer\Announcer;
use api\modules\v1\models\records\announcer\UserEdit;
use common\models\announcer\Age;
use common\models\announcer\AnnouncerSound;
use common\models\announcer\Gender;
use common\models\announcer\Presentation;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;
use common\models\access\User;

class Update extends UpdateAction
{
    /**
     * @OA\Put(
     *   path="/user/full",
     *   summary="Update field Project. Обновление полей проекта",
     *   tags={"User"},
     *   security={{"BearerAuth": {}}},
     *   @OA\Response(
     *     response="200",
     *     description="Объект пользователя",
     *     @OA\JsonContent(ref="#/components/schemas/UserEdit")
     *   )
     * )
     * @param int $id
     *
     * @return \yii\db\ActiveRecordInterface
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id = 0)
    {
        /* @var $model UserEdit */
        $model = $this->findModel((int)Yii::$app->user->id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        $user = User::findOne($model->id);
        $user->notify_sms = Yii::$app->request->getBodyParam("notificationSms") === 'true';
        $user->notify_email = Yii::$app->request->getBodyParam("notificationEmail") === 'true';
        $user->notify_telegram = Yii::$app->request->getBodyParam("notificationTelegram") === 'true';

        if (!($user->validate() && $user->save())) {
            return;
        }

        $requestData = Yii::$app->request->bodyParams;
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $userForm = new UpdateForm();
            $userForm->setId($model->id);
            $userForm->load($requestData);
            $userForm->file = UploadedFile::getInstanceByName('addAvatar[0]');
            $announcerForm = new AnnouncerForm();

            if ($password = ($userForm->toArray()['password'] ?? null)) {
                $model->setPassword($password);
            }

            if ($model->getIsAnnouncer()) {
                if (!$model->announcer) {
                    $announcer = new Announcer(['user_id' => $model->id]);
                    $model->populateRelation('announcer', $announcer);
                }
                $announcerForm->load($requestData['announcer']);
                $announcerForm->photos = UploadedFile::getInstancesByName('addPhotos');
                $announcerForm->sounds = UploadedFile::getInstancesByName('addSounds');
            }

            if ($userForm->upload() && $announcerForm->validate()) {
                $model->load($userForm->toArray());
                if ($model->getIsAnnouncer()) {
                    $announcerForm->upload();
                    $model->announcer->load($announcerForm->toArray());
                    $model->announcer->save();
                    $indexSounds = -1;
                    $soundsData = ArrayHelper::index(
                        $requestData['announcer']['sounds'] ?? [],
                        function($element) use ($indexSounds) {
                            $indexSounds++;
                            if ($element['isNew'] ?? false) {
                                return $indexSounds;
                            } else {
                                return $element['sound']['url'];
                            }
                        }
                    );

                    $presentations = array_filter(
                        ArrayHelper::getColumn($requestData['announcer']['sounds'] ?? [], 'presentation_id.id')
                    );
                    $presentations = Presentation::find()
                        ->select('id')
                        ->where(['code' => $presentations])
                        ->indexBy('code')
                        ->column();
                    $presentations[''] = null;

                    $ages = array_filter(
                        ArrayHelper::getColumn($requestData['announcer']['sounds'] ?? [], 'age_id.id')
                    );
                    $ages = Age::find()
                        ->select('id')
                        ->where(['code' => $ages])
                        ->indexBy('code')
                        ->column();
                    $ages[''] = null;

                    $languages = array_filter(
                        ArrayHelper::getColumn($requestData['announcer']['sounds'] ?? [], 'lang_id.id')
                    );
                    $languages[''] = null;

                    $genders = array_filter(
                        ArrayHelper::getColumn($requestData['announcer']['sounds'] ?? [], 'gender_id.id')
                    );
                    $genders = Gender::find()
                        ->select('id')
                        ->where(['code' => $genders])
                        ->indexBy('code')
                        ->column();
                    $genders[''] = null;

                    if ($soundFiles = $announcerForm->getSoundModels()) {
                        $presentationsAdd = array_filter(
                            $requestData['announcer']['sounds'] ?? [],
                            function ($item) {
                                return $item['isNew'] ?? false;
                            }
                        );
                        $presentationsAdd = ArrayHelper::getColumn(
                            $presentationsAdd,
                            'presentation_id.id',
                            false
                        );

                        foreach ($soundFiles as $key => $file) {
                            if ($file->save() === false) {
                                throw new BadRequestHttpException($file->errors);
                            }
                            $presentationId = $presentations[$presentationsAdd[$key]] ?? null;
                            AnnouncerSound::create([
                                'sound_id' => $file->id,
                                'presentation_id' => $presentationId,
                                'announcer_id' => $model->announcer->id,
                            ]);
                        }
                    }

                    $soundsModels = $announcerForm->getSoundModels();
                    $findSoundModel = function ($sound) use (&$soundsModels) {
                        foreach ($soundsModels as $key => $soundModel) {
                            if ($soundModel->getUrl() === $sound->getUrl()) {
                                return $key;
                            }
                        }

                        return null;
                    };

                    $model->announcer->refresh();
                    $issetSoundsUrl = [];
                    foreach ($model->announcer->sounds as $key => $sound) {
                        $url = $sound->sound->getUrl();
                        if ($sound->sound && isset($soundsData[$url]) && !in_array($url, $issetSoundsUrl)) {
                            if (empty($languages[$key])) {
                                throw new BadRequestHttpException('Необходимо указать язык демо записи', );
                            }
                            $data = $soundsData[$sound->sound->getUrl()];
                            $sound->presentation_id = $presentations[$data['presentation_id']['id'] ?? -1] ?? null;
                            $sound->age_id = $ages[$data['age_id']['id'] ?? -1] ?? null;
                            $sound->lang_code = $languages[$key];
                            $sound->parodist_name = $data['parodist_name'] ?? '';
                            $sound->gender_id = $genders[$data['gender_id']['id'] ?? -1] ?? null;
                            $sound->save();
                            $sound->sound->name = $data['sound']['name'];
                            $sound->sound->save();
                            $issetSoundsUrl[] = $url;
                        } elseif (($keyModel = $findSoundModel($sound->sound) !== null)) {
                            unset($soundsModels);
                        } else {
                            $sound->delete();
                        }
                    }

                    $photosId = [];
                    if ($photoFiles = $announcerForm->getPhotoModels()) {
                        foreach ($photoFiles as $file) {
                            if ($file->save() === false) {
                                throw new BadRequestHttpException($file->errors);
                            }
                            $photosId[] = $file->id;
                        }
                    }

                    foreach ($model->announcer->photos as $photo) {
                        $photosId[] = $photo->id;
                    }

                    $model->announcer->photos = $photosId;
                    $model->announcer->save();
                }

                if ($file = $userForm->getFileModel()) {
                    if ($file->save() === false) {
                        throw new BadRequestHttpException($file->errors);
                    }
                    $model->photo_id = $file->id;
                }

                $model->save();
                $transaction->commit();
                $model->refresh();

                if (!empty($model->announcer->presentations)) {
                    if (!$model->announcer->vocalExists() && $model->announcer->cost_vocal > 0) {
                        $model->announcer->cost_vocal = 0;
                    }
                    if (!$model->announcer->parodistExists() && $model->announcer->cost_parody > 0) {
                        $model->announcer->cost_parody = 0;
                    }
                    $model->announcer->save();
                }

                return $model;
            }
            throw new BadRequestHttpException($userForm->errors);
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }
}

<?php

namespace console\controllers;

use Carbon\Carbon;
use common\models\access\User;
use common\models\announcer\Announcer;
use common\models\user\Bill;
use common\models\user\Chat;
use common\models\user\Favorite;
use common\models\user\Notification;
use common\models\user\Review;
use common\models\user\SendNotify;
use common\service\chat\ChatService;
use common\service\Epochta;
use common\service\Mail;
use Yii;
use yii\console\Controller;
use yii\db\Expression;
use yii\helpers\Console;

/**
 * Page controller
 */
class CalculateController extends Controller
{
    public function actionTestSms()
    {
        $phone = getenv('PHONE');
        $message = getenv('MESSAGE');

        if (empty($phone) || empty($message)) {
            echo 'phone or message need set.' . PHP_EOL
                . 'Example PHONE=9999 MESSAGE=\'Hi\' ./yii calculate/test-sms' . PHP_EOL;
            exit(1);
        }

        return var_dump(Epochta::sendSms('test', '+79026001380'));
    }

    public function actionTest()
    {
        return Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['emailFrom'])
            ->setTo('pamkil@ya.ru')
            ->setSubject('Message subject')
            ->setTextBody('Plain text content')
            ->setHtmlBody('<b>HTML content</b>')
            ->send();
    }

    public function actionIndex()
    {
        Console::stdout('Calculate Favorites');
        $users = User::find()
            ->select(['id', 'type'])
            ->indexBy('id')
            ->asArray();

        foreach ($users->each() as $user) {
            $isAnnouncer = User::isAnnouncer($user);
            $quantityFavorite = Favorite::calculate($user['id'], $isAnnouncer, false);
            [$averageReview, $quantityReview] = Review::calculate($user['id'], $isAnnouncer, false);
            User::updateAll(
                [
                    'rating' => (float)$averageReview,
                    'quantity_reviews' => (int)$quantityReview,
                    'quantity_favorite' => (int)$quantityFavorite,
                ],
                [
                    'id' => $user['id'],
                ]
            );
        }

        Yii::$app->cache->flush();
    }

    public function actionBalance()
    {
        Console::stdout('Calculate Balance for Announcers');

        /**
        select sum(sum - (`sum` * `percent` / 100)), announcer_id
        from user_bill
        where status=2
        group by announcer_id;
         */
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $bills = Bill::find()
                ->select([new Expression('SUM(`sum` - (`sum` * `percent` / 100)) as sum'), 'announcer_id'])
                ->where(['status' => Bill::STATUS_PAID_CLIENT])
                ->groupBy('announcer_id')
                ->asArray();

            $billsClone = clone $bills;
            $billsClone->select('announcer_id');

            $nullBills = Announcer::find()
                ->select(['user_id'])
                ->where(['NOT IN', 'user_id', $billsClone]);

            Announcer::updateAll(['sum' => 0], ['user_id' => $nullBills]);

            foreach ($bills->each() as $bill) {
                Announcer::updateAll(
                    ['sum' => round((float)$bill['sum'], 2)],
                    ['user_id' => $bill['announcer_id']]
                );
            }
            $transaction->commit();
            Yii::$app->cache->flush();
        } catch (\Exception $exception) {
            $transaction->rollBack();

            throw $exception;
        }
    }

    public function actionNotify()
    {
        $date = Carbon::now()->subDays(2);
        $dateOld = Carbon::now()->subMonths(2);
        $queryNotify = Notification::find()
            ->select('other_id')
            ->where(['type' => [Notification::TYPE_BILL, Notification::TYPE_NO_PAID_BILL]]);

        $queryBills = Bill::find()
            ->where([
                'AND',
                ['status' => Bill::STATUS_NEW],
                ['NOT IN', 'id', $queryNotify],
                ['<=', 'date', $date->toDateTimeString()],
                ['>', 'date', $dateOld->toDateString()],
            ])
            ->with(['announcer', 'client'])
            ->asArray();

        $rows = [];
        $attributes = [
            'user_id' => 'user_id',
            'other_id' => 'other_id',
            'status' => 'status',
            'type' => 'type',
            'text' => 'text',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
        ];

        $date = new Expression('NOW()');
        foreach ($queryBills->batch(50) as $bills) {
            foreach ($bills as $bill) {
                $billText = Bill::billToText($bill);
                $textAnnouncer = "{$bill['client']['firstname']} пока не оплатил $billText";
                $textClient = "Напоминаем, что {$bill['announcer']['firstname']} выставила Вам $billText";
                $rows[] = [
                    'user_id' => $bill['announcer_id'],
                    'other_id' => $bill['id'],
                    'status' => Notification::STATUS_NEW,
                    'type' => Notification::TYPE_NO_PAID_BILL,
                    'text' => $textAnnouncer,
                    'created_at' => $date,
                    'updated_at' => $date,
                ];
                $rows[] = [
                    'user_id' => $bill['client_id'],
                    'other_id' => $bill['id'],
                    'status' => Notification::STATUS_NEW,
                    'type' => Notification::TYPE_NO_PAID_BILL,
                    'text' => $textClient,
                    'created_at' => $date,
                    'updated_at' => $date,
                ];
            }

            Yii::$app->db->createCommand()
                ->batchInsert(
                    Notification::tableName(),
                    $attributes,
                    $rows
                )
                ->execute();
        }
    }

    public function actionEmail()
    {
        $chatMessages = Chat::unReadQuery();

        foreach ($chatMessages->each(50) as $message) {

            /** @var Chat $message */
            $user = $message->is_write_client ? $message->announcer : $message->client;

            if (empty($user) || empty($user->email)) {
                continue;
            }
            $email = $user->email;

            $notify = SendNotify::getNotify($user->id);
            if ($notify) {
                $notifyDate = Carbon::parse($notify->date);
                $messageDate = Carbon::parse($message->created_at);

                if ($messageDate < $notifyDate) {
                    continue;
                }
            }

            try {
                $result = Mail::sendRemindMessage($email);
                if ($result) {
                    SendNotify::remove($user->id);
                    SendNotify::send($user->id);
                }
            } catch (\Exception $exception) {
                Yii::error($exception);
            }
        }
    }

    public function actionSms()
    {
        if (YII_DEBUG) {
            Yii::warning('Start sms send');
        }
        try {
            $service = Yii::$container->get(ChatService::class);
            $messageSend = $service->sendSmsNotifyByUnreadMessages();
        } catch (\Exception $exception) {
            Yii::error($exception);
        }

        if (YII_DEBUG) {
            Yii::warning('Sms send: ' . $messageSend ? 'success' : 'error');
        }
    }
}

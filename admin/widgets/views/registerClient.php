<?php
//https://ionicons.com/v4/usage/
//https://ionicons.com/v4/
?>
<div class="info-box">
    <span class="info-box-icon bg-aqua"><i class="icon ion-md-shuffle"></i></span>
    <div class="info-box-content">
        <span class="info-box-text">Клиентов зарегистрировалось</span>
        <span class="info-box-number"><?= $cur; ?><small> (текущий месяц)</small></span>
        <span class="info-box-number"><?= $prev; ?><small> (прошлый месяц)</small></span>
    </div>
</div>
<?php

namespace api\modules\v1\actions\auth;

use frontend\models\RestoreRequestForm;
use Yii;
use yii\base\Action;

class Restore extends Action
{

    public function run()
    {
        $model = new RestoreRequestForm();

        if ($model->load(Yii::$app->request->bodyParams) && $model->validate()) {
            $model->sendEmail();
        } else {
            return [
                'errors' => $model->errors
            ];
        }

        return ['result' => 'send'];
    }
}

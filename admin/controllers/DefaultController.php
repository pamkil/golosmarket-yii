<?php

namespace admin\controllers;

use admin\models\LoginForm;
use Yii;

/**
 * Class DefaultController
 * @package admin\controllers
 */
class DefaultController extends SecurityController
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ]
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $this->view->title = 'Главная';

        return $this->render('index');
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!$this->isAccessAdminPanel()) {
            $this->layout = '/auth';
            $this->view->title = "Вход";

            $model = new LoginForm();
            if (!($model->load(Yii::$app->request->post()) && $model->login())) {
                return $this->render('auth', [
                    'model' => $model
                ]);
            }
        }

        if ($this->isAccessAdminPanel()) {
            $get = Yii::$app->request->get();
            if (!empty($get['url'])) {
                return $this->redirect($get['url']);
            }
        }

        return $this->redirect(['/default/index']);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        \Yii::$app->user->logout();

        return $this->redirect(['/default/index']);
    }
}

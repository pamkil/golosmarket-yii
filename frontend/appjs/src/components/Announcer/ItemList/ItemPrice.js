import React from 'react'
import Price from '../../Common/Price/Price'

const ItemPrice = ({cost, costBy, costFilter, filter}) => {
  const sortParam = filter.buttonsCost.find(f => f.id === costBy)

  return <div className="cost">
    <Price>{cost}</Price>
    <span className="val-sec">{sortParam ? sortParam.name : costFilter.costBy}</span>
  </div>
}

export default ItemPrice

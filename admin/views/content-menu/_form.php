<?php

use common\models\content\Menu;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var Menu $model
 * @var array $types
 */

?>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data' // important
    ]
]) ?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <?= Html::submitButton('<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord ? 'Создать' : 'Сохранить'), ['class' => 'btn btn-success']) ?>
        <?php if (!$model->isNewRecord) : ?>
            <?= Html::a('<i class="fa fa-times"></i>&nbsp;' . 'Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'title' => 'Удалить',
                'data-confirm' => 'Вы уверены что хотите удалить?',
                'data-method' => 'post'
            ]) ?>
        <?php endif; ?>
    </div>
    <div class="box-body">
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'path') ?>
        <?= $form->field($model, 'position') ?>
        <?= $form->field($model, 'type_id')->dropDownList($types) ?>
        <?= $form->field($model, 'parent_id') ?>
    </div>
</div>

<?php ActiveForm::end() ?>

<?php

namespace admin\widgets;

use Carbon\Carbon;
use common\models\user\Bill;
use yii\base\Widget;

class MonthProfit extends Widget
{
    public function run()
    {
        parent::run();

        /**
         * Заработано - считаем только чистый доход сервиса (30% от суммы всех счетов)
         */

        $dateLast = Carbon::today()->subMonth()->startOfMonth()->toDateTimeString();
        $dateEndLast = Carbon::today()->subMonth()->endOfMonth()->toDateTimeString();
        $dateCur = Carbon::today()->startOfMonth()->toDateTimeString();
        $dateEndCur = Carbon::today()->endOfDay()->toDateTimeString();

        $sumLast = Bill::find()
            ->cache(60 * 3)
            ->where(
                [
                    'AND',
                    ['>=', 'updated_at', $dateLast],
                    ['<=', 'updated_at', $dateEndLast],
                    ['status' => Bill::STATUS_PAID_COMMISSION],
                ]
            )
            ->sum('IF(`ya_amount` IS NULL or `ya_amount` = \'\', `sum`, `ya_amount`) * `percent` / 100');;

        $sumCur = Bill::find()
            ->cache(60 * 3)
            ->where(
                [
                    'AND',
                    ['>=', 'updated_at', $dateCur],
                    ['<=', 'updated_at', $dateEndCur],
                    ['status' => Bill::STATUS_PAID_COMMISSION],
                ]
            )
            ->sum('`sum` * `percent` / 100');

        $data = [
            'title' => 'Заработано за месяц',
            'value1' => \Yii::$app->formatter->asCurrency(round($sumCur ?? 0, 2)),
            'smallValue1' => '(текущий)',
            'value2' => \Yii::$app->formatter->asCurrency(round($sumLast ?? 0, 2)),
            'smallValue2' => '(прошлый)',
        ];

        echo $this->render('allTwoRowWidgets', $data);
    }
}
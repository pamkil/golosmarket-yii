<?php

namespace api\modules\v1\models\filters\user;

use api\modules\v1\models\records\site\Review;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ReviewSearch extends Model
{
    public function search($requestParams = [], int $page = 1, int $perPage = 12)
    {
        $perPage = $perPage > 50 ? 50 : $perPage;
        $query = Review::find()
            ->andWhere([
                'AND',
                ['is_public' => true],
                ['<>', 'text', ''],
            ]);
        $searchField = $requestParams['query'] ?? null;

        if ($searchField) {
            $query->andFilterWhere([
                'rating' => $searchField['rating'] ?? null,
            ]);

        }

        /** @var ActiveDataProvider $dataProvider */
        $dataProvider = Yii::createObject([
            'class' => ActiveDataProvider::class,
            'query' => $query,
            'pagination' => [
//                'params' => $requestParams,
                'pageSize' => 12
            ],
            'sort' => [
                'params' => $requestParams,
                'defaultOrder' => ['created_at' => SORT_DESC],
            ],
        ]);

        $dataProvider->pagination->page = $page - 1;
        $dataProvider->pagination->pageSize = $perPage;

        return $dataProvider;
    }
}

<?php

namespace admin\models\user;

use common\models\access\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class AnnouncerSearch extends Model
{
    public $username;
    public $firstname;
    public $lastname;
    public $email;
    public $phone;
    public $last_visit;
    public $status;
    public $is_blocked;
    public $likes;
    public $dislikes;
    public $cost;

    public function rules()
    {
        return [
            [
                ['username', 'firstname', 'lastname', 'email', 'phone', 'last_visit'],
                'string'
            ],
            [
                ['status', 'dislikes', 'likes', 'cost'],
                'integer'
            ],
            [
                ['is_blocked',],
                'boolean'
            ],
        ];
    }

    public function search($params)
    {
        $query = User::find()
            ->alias('user')
            ->where([
                'AND',
                ['user.type' => User::TYPE_ANNOUNCER],
                ['!=', 'user.status', User::STATUS_DELETED]
            ])
            ->with('city')
            ->joinWith(['announcer announcer']);
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'username',
                        'firstname',
                        'lastname',
                        'email',
                        'phone',
                        'last_visit',
                        'status',
                        'is_blocked',
                        'announcer.cost',
                        'announcer.likes',
                        'announcer.dislikes',
                    ]
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $dataProvider->sort->defaultOrder = ['last_visit' => SORT_DESC,];

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'AND',
                ['like', 'user.username', $this->username],
                ['like', 'user.firstname', $this->firstname],
                ['like', 'user.lastname', $this->lastname],
                ['like', 'user.email', $this->email],
                ['like', 'user.phone', $this->phone],
                ['like', 'user.last_visit', $this->last_visit],
                ['user.status' => $this->status],
                ['user.is_blocked' => $this->is_blocked],
                ['announcer.likes' => $this->likes],
                ['announcer.dislikes' => $this->dislikes],
                ['announcer.cost' => $this->cost],
            ]
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}

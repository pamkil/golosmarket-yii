<?php

namespace frontend\controllers;

use api\modules\v1\models\filters\announcer\AnnouncerSearch;
use common\models\content\Page;
use common\models\content\PageAccordeon;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Page controller
 */
class PageController extends BaseController
{
    /**
     * Displays homepage.
     *
     * @param string $code
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \JsonException
     */
    public function actionView(string $code)
    {
        $page = Page::getByPath($code, '', true);

        if (!$page) {
            throw new NotFoundHttpException();
        }

        $this->addSeo($page);

        $filters = null;
        $currentFilter = null;
        $similarFilter = null;

        if ($page['show_announcers'] || $page['show_similar']) {
            $filters = $this->getAllFilters();
        }
        if ($page['show_announcers']) {
            $filterPage = $page['filter'] ?? ['count' => 12];
            if (!empty($filterPage['a4'])) {
                $filterPage['costBy'] = 'cost_a4';
            }
            if (!empty($filterPage['cost_at'])) {
                $filterPage['costAt'] = $filterPage['cost_at'];
            }
            if (!empty($filterPage['cost_to'])) {
                $filterPage['costTo'] = $filterPage['cost_to'];
            }
            $currentFilter = $this->jsonEncode($this->getSelectFilers($filterPage));
            $announcersCount = $page['filter']['count'] ?: 12;
            $announcersModels = (new AnnouncerSearch())
                ->search($filterPage, 1, $announcersCount)
                ->getModels();

            $announcers = [];
            foreach ($announcersModels as $model) {
                $announcers[] = $model->toArray();
            }
            $announcersStr = $this->jsonEncode($announcers);
            $reviewsStr = $this->jsonEncode($reviews = []);
        }
        if ($page['show_similar']) {
            $similarFilter = $page['similar'] ?? ['count' => 12];
            $similarFilter = $this->getSelectFilers($similarFilter);
            if (!empty($similarFilter['cost_by_a4'])) {
                $similarFilter['costBy'] = 'a4';
            }
            if (!empty($similarFilter['cost_at'])) {
                $similarFilter['costAt'] = $similarFilter['cost_at'];
            }
            if (!empty($similarFilter['cost_to'])) {
                $similarFilter['costTo'] = $similarFilter['cost_to'];
            }
            $similarCount = $page['similar']['count'] ?: 12;
            $announcersSimilarModels = (new AnnouncerSearch())
                ->search($similarFilter, 1, $similarCount)
                ->getModels();

            $similarFilter = $this->jsonEncode($similarFilter);
            $announcersSimilar = [];

            foreach ($announcersSimilarModels as $model) {
                $announcersSimilar[] = $model->toArray();
            }
            $announcersSimilarStr = $this->jsonEncode($announcersSimilar);
        }

        $manifest = file_get_contents(Yii::getAlias('@webroot/static/asset-manifest.json'));
        $manifestData = json_decode($manifest, true, 512, JSON_THROW_ON_ERROR);

        $accordeon = PageAccordeon::findAll([
            'page_id' => $page['id']
        ]);

        return $this->render('page', [
            'page' => $page,
            'accordeon' => $accordeon,
            'filters' => $filters,
            'currentFilter' => $currentFilter,
            'similarFilter' => $similarFilter,
            'reviews' => $reviews ?? [],
            'announcers2' => [],
            'files' => $manifestData['files'],
            'announcers' => $announcers ?? [],
            'announcersSimilar' => $announcersSimilar ?? [],
            'announcersSimilarStr' => $announcersSimilarStr ?? '[]',
            'announcersStr' => $announcersStr ?? '[]',
            'announcers2Str' => '[]',
            'reviewsStr' => $reviewsStr ?? '[]',
            'disablePagination' => 'true',
        ]);
    }
}

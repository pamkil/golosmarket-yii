<?php if (!empty($filters)): ?>
    <script>
        var filtersList = <?= $filters; ?>;
        var currentFilter = <?= $currentFilter; ?>;
        var similarFilter = <?= $similarFilter; ?>;
        var announcers = <?= $announcersStr ?? '[]'; ?>;
        var announcersSimilar = <?= $announcersSimilarStr ?? '[]'; ?>;
        var announcers2 = <?= $announcers2Str ?? '[]'; ?>;
        var reviews = <?= $reviewsStr ?? '[]'; ?>;
        var disablePagination = <?= $disablePagination ?? 'false'; ?>
    </script>
<?php endif; ?>
<section class="content contentpage">
    <div class="container">
        <div class="titlepage">
            <a class="icon mobile-no" href="/">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M19 12H6M12 5l-7 7 7 7"/></svg>
            </a>
            <h1><?= $page['h1'] ?? ''; ?></h1>
        </div>
        <?php if (!empty($accordeon)) : ?>
            <div class="feature-holder">
                <?php /** @var PageAccordeon $item */
                foreach ($accordeon as $item) : ?>
                    <div>
                        <h2><?= $item->title ?></h2>
                        <?= $item->body ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?php if (!empty($page['body'])): ?>
            <?= $page['body']; ?>
        <?php endif; ?>
    </div>
</section>
<?php if ($page['show_announcers']): ?>
    <?php if ($page['show_filters']): ?>
        <section class="filter-main">
            <div id="filter" class="container"></div>
        </section>
    <?php endif; ?>
    <?= $this->render(
        '/announcer/list',
        [
            'announcers' => $announcers ?? [],
            'announcers2' => $announcers2 ?? [],
            'files' => $files ?? [],
            'reviews' => $reviews ?? [],
        ]
    ) ?>
<?php endif; ?>
<?php if (!empty($page['after_body'])): ?>
    <section class="content contentpage">
        <div class="container">
            <?= $page['after_body']; ?>
        </div>
    </section>
<?php endif; ?>
<?php if ($page['show_similar']): ?>
    <section class="content">
        <div class="container">
            <div>
                <p>Не нашли то, что искали?!</p>
                <p>Посмотрите пожалуйста других дикторов:</p>

            </div>

        </div>
    </section>
    <?= $this->render(
        '/announcer/list',
        [
            'announcers' => $announcersSimilar ?? [],
            'announcers2' => [],
            'files' => $files ?? [],
            'reviews' => [],
            'blockRender' => 'similar',
        ]
    ) ?>
<?php endif; ?>

<?php

use yii\db\Migration;

/**
 * Class m230327_172551_change_created_at_to_user_chat_table
 */
class m230327_172551_change_created_at_to_user_chat_table extends Migration
{
    private $table = '{{%user_chat}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(
            $this->table,
            'created_at',
            $this->timestamp()->null()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn(
            $this->table,
            'created_at',
            $this->timestamp()->notNull()
        );
    }
}

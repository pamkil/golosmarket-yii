<?php

namespace api\modules\v1\models\records\user;

/**
 * @OA\Schema(
 *     schema="UserReview",
 *     title="Отзыв об оппоненте"
 * )
 */
class Review extends \common\models\user\Review
{
    /**
     * @OA\Property(property="id", type="integer", description="ИД")
     * @OA\Property(property="announcer_id", type="integer", description="ИД клиента")
     * @OA\Property(property="client_id", type="integer", description="ИД клиента")
     * @OA\Property(property="type_writer", type="integer", description="ИД клиента")
     * @OA\Property(property="rating", type="integer", description="Рейтинг")
     * @OA\Property(property="text", type="string", description="Текст отзыва")
     * @OA\Property(property="is_public", type="boolean", description="Текст отзыва")
     * @OA\Property(property="created_at", type="string", description="Время создания")
     * @OA\Property(property="updated_at", type="string", description="Время обновления")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'announcer_id',
            'client_id',
            'type_writer',
            'is_public',
            'rating',
            'text',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
        ];
    }
}

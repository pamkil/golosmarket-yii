<?php

namespace api\modules\v1\models\records\announcer;

use api\modules\v1\models\records\auth\User;
use common\models\user\Favorite;
use Yii;

/**
 * @OA\Schema(title="Опонент по чату")
 */
class Opponent extends User
{
    /**
     * @OA\Property(property="id", type="integer", description="ИД")
     * @OA\Property(property="avatar", type="string", description="URL фото")
     * @OA\Property(property="name", type="string", description="Имя")
     * @OA\Property(property="cost", type="integer", description="Цена за рекламную начитку до 30 сек. (3-4 дубля)")
     * @OA\Property(property="isOnline", type="boolean", description="Оппонент онлайн")
     * @OA\Property(property="sound", type="array", description="Демо записи диктора",
     *      @OA\Items(type="object", description="Sounds", ref="#/components/schemas/AnnouncerSound")
     * )
     * @OA\Property(property="isFavorite", type="boolean", description="Оппонент в закладках")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'id' => 'id',
            'avatar' => 'photoUrl',
            'name',
            'cost' => 'costFilter',
            'isOnline',
            'isFavorite' => 'isFavoriteView',
            'sound' => 'soundView',
            'scheduleWork',
            'scheduleWorkText',
        ];
    }


    public function getCostFilter()
    {
        return $this->announcer ? $this->announcer->cost : null;
    }

    public function getIsFavoriteView()
    {
        return $this->getFavorite()
            ->andWhere([($this->getIsAnnouncer() ? 'client_id' : 'announcer_id') => Yii::$app->user->id])
            ->one() ? true : false;
    }

    public function getIsOnline()
    {
        if (!$this->announcer || !$this->announcer->scheduleWork) {
            return false;
        }

        [$isOnline] = $this->announcer->scheduleWork->getIsOnline($this);

        return !!$isOnline;
    }

    public function getSoundView()
    {
        return $this->announcer && $this->announcer->sounds ? $this->announcer->sounds : [];
    }

    public function getName()
    {
        $lastname = $this->lastname;
//        if (!$this->isAnnouncer) {
        $lastname = mb_substr($lastname, 0, 1);
//        }

        return $this->firstname . ' ' . $lastname;
    }

    public function getAnnouncer()
    {
        return $this->hasOne(Announcer::class, ['user_id' => 'id']);
    }

    public function getFavorite()
    {
        if ($this->getIsAnnouncer()) {
            return $this->hasOne(Favorite::class, ['announcer_id' => 'id']);
        }

        return $this->hasOne(Favorite::class, ['client_id' => 'id']);
    }

    public function getScheduleWorkText()
    {
        if (
            isset($this->announcer) &&
            isset($this->announcer->scheduleWork)
        ) {
            return $this->announcer->getTimeText();
        } else {
            return self::getIsOnline() ? 'Онлайн' : 'Недоступен';
        }
    }

    public function getScheduleWork()
    {
        return $this->announcer->scheduleWork ?? null;
    }
}

const SERVER_OFFSET_HOURS = 3;

export const getTimezoneOffset = () => {
    return (new Date()).getTimezoneOffset() / 60 * -1;
}

export const convertToCurrentTimezone = (date) => {
    if (!date) return;

    return date.setTime( date.getTime() + date.getTimezoneOffset() * 60 * 1000 );
}
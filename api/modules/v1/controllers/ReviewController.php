<?php

namespace api\modules\v1\controllers;

use api\modules\v1\actions\review\Create;
use api\modules\v1\actions\review\CreateService;
use api\modules\v1\actions\review\CreateUser;
use api\modules\v1\actions\review\Index;
use api\modules\v1\models\records\site\Review;
use Yii;
use yii\filters\HttpCache;
use yii\helpers\ArrayHelper;
use yii\rest\OptionsAction;

/**
 * @SWG\Tag(
 *   name="Review",
 *   description="Review"
 * )
 */
class ReviewController extends RestController
{
    public $modelClass = Review::class;

    private $lastModified = null;
    private $etag = '';

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'HttpCache' => [
                    'class' => HttpCache::class,
                    'lastModified' => function () {
                        if ($this->lastModified) {
                            return $this->lastModified;
                        } else {
                            return $this->lastModified = strtotime(Review::getMax());
                        }
                    },
                    'etagSeed' => function () {
                        if ($this->etag) {
                            return $this->etag;
                        } else {
                            return $this->etag = md5(
                                json_encode(
                                    [
                                        Yii::$app->request->get(),
                                        $this->modelClass,
                                        $this->lastModified,
                                    ]
                                )
                            );
                        }
                    },
                ],
                'authenticator' => [
                    'optional' => ['index', 'view'],
                    'except' => ['options'],
                ],
            ]
        );
    }

    public function checkAccess($action, $model = null, $params = [])
    {
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => Index::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create' => [
                'class' => Create::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
            'service' => [
                'class' => CreateService::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
            'user' => [
                'class' => CreateUser::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
            'options' => [
                'class' => OptionsAction::class,
            ],
        ];
    }
}
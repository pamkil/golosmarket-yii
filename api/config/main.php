<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);
$urlManager = require(__DIR__ . '/urlManager.php');

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => api\modules\v1\Module::class
        ],
        'swagger' => [
            'basePath' => '@app/modules/swagger',
            'class' => api\modules\swagger\Module::class
        ],
    ],
    'aliases' => [
        '@api' => dirname(__DIR__, 2) . '/api',
    ],
    'components' => [
        'errorHandler' => [
            'errorAction' => 'api/default/error'
        ],
        'response' => [
            'format' => 'json',
            'formatters' => [
                'json' => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ]
            ]
        ],
        'request' => [
            'baseUrl' => '/api',
            'enableCookieValidation' => false,
            'parsers' => [
                'application/json' => yii\web\JsonParser::class,
                'multipart/form-data' => yii\web\MultipartFormDataParser::class
            ]
        ],
        'user' => [
            'identityClass' => api\modules\v1\models\records\auth\User::class,
            'enableAutoLogin' => false,
            'enableSession' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'db' => [
            'enableSchemaCache' => true,

            // Продолжительность кеширования схемы.
            'schemaCacheDuration' => 3600,

            // Название компонента кеша, используемого для хранения информации о схеме
            'schemaCache' => 'cache',
        ],

        'urlManager' => $urlManager,
        'telegram' => [
            'class' => 'aki\telegram\Telegram',
            'botToken' => env('TELEGRAM_TOKEN'),
        ]
    ],
    'params' => $params,
];

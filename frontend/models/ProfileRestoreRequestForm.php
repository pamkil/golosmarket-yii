<?php
namespace frontend\models;

use Carbon\Carbon;
use common\models\access\User;
use common\service\Mail;
use Yii;
use yii\base\Model;

/**
 * Password reset request form
 * @deprecated
 */
class ProfileRestoreRequestForm extends Model
{
    public $email;

    public function formName()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'userExists'],
        ];
    }

    public function userExists($attribute, $params, $validator) {
        $userExist = User::findDeleted()->andWhere([
            'email' => $this->{$attribute},
            'status' => User::STATUS_DELETED
        ])->exists();

        if (!$userExist) {
            $this->addError($attribute, 'Пользователь не найден');
        }
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findDeleted()->andWhere(['email' => $this->email])->one();

        if (!$user) {
            return false;
        }

        $user->generateEmailVerificationToken();
        if (!$user->save()) {
            return false;
        }

        return Mail::sendProfileRestore(
            $user->email,
            $user->firstname,
            $user->verification_token
        );
    }
}

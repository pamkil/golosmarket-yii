<?php

namespace api\modules\v1\actions\chat;

use api\modules\v1\actions\common\IndexAction;
use api\modules\v1\models\filters\user\ChatSearch;
use api\modules\v1\models\records\auth\User;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * @OA\Get(
 *      path="/chat",
 *      summary="List of chats - Список опонентов с кем была переписка",
 *      tags={"Chat"},
 *      @OA\Parameter(
 *          name="interlocutorUserId",
 *          in="query",
 *          description="User Id собесебника ",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="number Page",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="per_page",
 *          in="query",
 *          description="quantity announcers in Page",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="ok",
 *          @OA\JsonContent(
 *              type="array",
 *              @OA\Items(ref="#/components/schemas/ListChat")
 *          )
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */
class Index extends IndexAction
{
    /** @inheritdoc
     * @return ActiveDataProvider
     */
    public function run()
    {
        parent::run();

        $request = Yii::$app->request;

        $page = (int)$request->get('page', 1);
        $perPage = (int)$request->get('per_page', 50);
        $filter = $request->get('filter', '');
        $isAnnouncer = Yii::$app->user->identity->type === User::TYPE_ANNOUNCER;

        return (new ChatSearch())->getList(Yii::$app->user->id, $isAnnouncer, $filter, $page, $perPage);
    }
}

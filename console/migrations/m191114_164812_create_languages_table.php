<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_announcer_languages}}`.
 */
class m191114_164812_create_languages_table extends Migration
{
    private $languages = '{{%user_announcer_languages}}';
    private $announcer2languages = '{{%user_announcer_2_languages}}';
    private $announcer = '{{%user_announcer}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->languages, [
            'id' => $this->primaryKey(),
            'active' => $this->boolean()->notNull()->defaultValue(true),
            'position' => $this->integer(),
            'code' => $this->string(25),
            'name' => $this->string(),
            'menu_title' => $this->string(),
        ], $tableOptions);
        $this->addCommentOnTable($this->languages, 'Язык голоса');

        $this->createTable($this->announcer2languages, [
            'announcer_id' => $this->integer(),
            'language_id' => $this->integer(),
            'PRIMARY KEY (announcer_id, language_id)'
        ], $tableOptions);

        $this->createIndex(
            'IX-user_announcer_2_languages-announcer_id',
            $this->announcer2languages,
            'announcer_id'
        );
        $this->addForeignKey(
            'FK-user_announcer_2_languages-announcer_id',
            $this->announcer2languages,
            'announcer_id',
            $this->announcer,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('IX-user_announcer_2_languages-language_id', $this->announcer2languages, 'language_id');
        $this->addForeignKey(
            'FK-user_announcer_2_languages-language_id',
            $this->announcer2languages,
            'language_id',
            $this->languages,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addData();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->announcer2languages);
        $this->dropTable($this->languages);
    }

    private function addData()
    {
        $this->batchInsert(
            $this->languages,
            ['code', 'name', 'menu_title', 'position', 'active'],
            [
                ['russian', 'Русский', 'Русские дикторы', 1, 1],
                ['ukrainian', 'Украинский', 'Украинские дикторы', 2, 1],
                ['belorussian', 'Белорусский', 'Белорусские дикторы', 3, 1],
                ['kazakh', 'Казахский', 'Казахские дикторы', 4, 1],
                ['kyrgyz', 'Киргизский', 'Киргизские дикторы', 5, 1],
                ['english', 'Английский', 'Английские дикторы', 6, 1],
                ['deutsch', 'Немецкий', 'Немецкие дикторы', 7, 1],
                ['spanish', 'Испанский', 'Испанские дикторы', 9, 1],
                ['italian', 'Итальянский', 'Итальянские дикторы', 10, 1],
                ['chinese', 'Китайский', 'Китайские дикторы', 11, 1],
                ['french', 'Французский', 'Французские дикторы', 12, 1],
                ['tajik', 'Таджикский', 'Таджикские дикторы', 13, 1],
                ['bulgarian', 'Болгарский', 'Болгарские дикторы', 14, 1],
                ['polish', 'Польский', 'Польские дикторы', 15, 1],
                ['greek', 'Греческий', 'Греческие дикторы', 16, 1],
                ['arab', 'Арабский', 'Арабские дикторы', 17, 1],
                ['tatar', 'Татарский', 'Татарские дикторы', 18, 1],
                ['japanese', 'Японский', 'Японские дикторы', 19, 1],
                ['georgian', 'Грузинский', 'Грузинские дикторы', 20, 0],
            ]
        );
    }
}

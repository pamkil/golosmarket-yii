<?

use admin\controllers\SecurityController;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\DatePicker;
use admin\widgets\RestCheckbox;
use admin\widgets\AvailableInline;
use admin\widgets\File;
use common\models\content\Banner;
use common\models\settings\Lang;

/**
 * @var Banner $model
 */
?>

<? $form = ActiveForm::begin([
    'options' => [
        'data' => [
            'key' => $model->id,
            'pjax' => true,
        ],
        'enctype' => 'multipart/form-data' // important
    ]
]) ?>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <?= $form->field($model, 'active', ['options' => ['tag' => 'div', 'style' => 'display: inline-block;']])->widget(RestCheckbox::class, ['size' => 'big'])->label(false) ?>
                    <div class="actions btn-set">
                        <?= Html::submitButton('<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord ? Yii::t('admin', 'Создать') : Yii::t('admin', 'Сохранить')), ['class' => 'btn btn-success']) ?>
                        <? if (!$model->isNewRecord) { ?>
                            <?= Html::a('<i class="fa fa-copy"></i>&nbsp;' . Yii::t('admin', 'Копировать'), ['duplicate', 'id' => $model->id], [
                                'class' => 'btn yellow-crusta',
                                'title' => Yii::t('admin', 'Копировать'),
                                'data-confirm' => Yii::t('admin', 'Вы уверены что хотите скопировать?')
                            ]) ?>
                            <?= Html::a('<i class="fa fa-times"></i>&nbsp;' . Yii::t('admin', 'Удалить'), ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'title' => Yii::t('admin', 'Удалить'),
                                'data-confirm' => Yii::t('admin', 'Вы уверены что хотите удалить?'),
                                'data-method' => 'post'
                            ]) ?>
                        <? } ?>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_general" data-toggle="tab"><?= Yii::t('admin', 'Общие') ?> </a>
                            </li>
                            <li>
                                <a href="#tab_translate" data-toggle="tab"><?= Yii::t('admin', 'Переводы') ?> </a>
                            </li>
                            <li>
                                <a href="#tab_available" data-toggle="tab"><?= Yii::t('admin', 'Доступность') ?> </a>
                            </li>
                            <? if (Yii::$app->user->can('viewHistory')) { ?>
                                <li>
                                    <a href="#tab_history" data-toggle="tab"><?= Yii::t('admin', 'История') ?> </a>
                                </li>
                            <? } ?>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_general">
                                <?= File::widget([
                                    'model' => $model,
                                    'attr' => 'image'
                                ]) ?>
                                <?= $form->field($model, 'code') ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <? /* Если установлена скидка для баннера, то ссылки и даты показа наследуются из нее */ ?>
                                        <?= $form->field($model, 'url') ?>
                                    </div>
                                    <div class="col-md-6"><?= $form->field($model, 'position')->textInput(['type' => 'number']) ?></div>
                                </div>
                                <div class="row">
                                    <? if (!$model->discount) { ?>
                                        <div class="col-md-4">
                                            <?= $form->field($model, 'date_start')->widget(DatePicker::class, ArrayHelper::merge(
                                                Yii::$app->params['datepicker_options'],
                                                ['pjaxContainerId' => SecurityController::PJAX_CONTAINER_ID]
                                            )); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?= $form->field($model, 'date_end')->widget(DatePicker::class, ArrayHelper::merge(
                                                Yii::$app->params['datepicker_options'],
                                                ['pjaxContainerId' => SecurityController::PJAX_CONTAINER_ID]
                                            )); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?= $form->field($model, 'countdown')->checkbox() ?>
                                        </div>

                                    <? } else { ?>
                                        <div class="col-md-4">
                                            <?= $form->field($model->discount, 'name')
                                                ->textInput(['readonly' => true])->label('Акция') ?>
                                        </div>
                                    <? } ?>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_translate">
                                <div class="tabbable-line">
                                    <? $langs = Lang::getAll();
                                    $translations = $model->getVariationModels(); ?>
                                    <ul class="nav nav-tabs">
                                        <? $i = 0;
                                        foreach ($langs as $lang) { ?>
                                            <li class="<?= $i++ == 0 ? 'active' : '' ?> <?= !$lang->active ? 'hide' : '' ?>">
                                                <a href="#lang_<?= $lang->id ?>" data-toggle="tab">
                                                    <?= $lang->name ?>
                                                </a>
                                            </li>
                                        <? } ?>
                                    </ul>
                                    <div class="tab-content">
                                        <? $i = 0;
                                        foreach ($langs as $lang) { ?>
                                            <div class="tab-pane fade <?= $i == 0 ? 'in active' : '' ?>" id="lang_<?= $lang->id ?>">
                                                <? if ($lang->active) {
                                                    $i++;
                                                }
                                                foreach ($translations as $index => $translation) {
                                                    if ($translation->lang_id != $lang->id) {
                                                        continue;
                                                    } ?>
                                                    <?= Html::activeHiddenInput($translation, '[' . $index . ']lang_id', ['value' => $translation->lang->id]) ?>
                                                    <?= $form->field($translation, '[' . $index . ']name') ?>
                                                    <?= $form->field($translation, '[' . $index . ']sub_name') ?>
                                                    <?= $form->field($translation, '[' . $index . ']button_text') ?>
                                                <? } ?>
                                            </div>
                                        <? } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_available">
                                <?= AvailableInline::widget([
                                    'form' => $form,
                                    'model' => $model
                                ]) ?>
                            </div>
                            <? if (Yii::$app->user->can('viewHistory')) { ?>
                                <div class="tab-pane" id="tab_history">
                                    <?= $this->render('//history/_list', ['model' => $model, 'data' => $model->history]) ?>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? ActiveForm::end() ?>

<?
namespace admin\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use common\models\sale\Currency;
use common\models\settings\Site;
use common\models\settings\SiteSearch;
use common\models\settings\Lang;

class SiteController extends SecurityController
{
    public function actionIndex()
    {
        if (!Yii::$app->user->can('viewSites')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $searchModel = new SiteSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список сайтов';
        $this->view->params['breadcrumbs'] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionCreate()
    {
        if (!Yii::$app->user->can('updateSites')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $model = new Site;

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно создано');

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Создание сайта';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список сайтов',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $model->active = 1;

        $langs = Lang::find()
            ->where(['active' => 1])
            ->all();
        $langs = ArrayHelper::map($langs, 'id', 'name');

        $currencies = Currency::find()
            ->where(['active' => 1])
            ->all();
        $currencies = ArrayHelper::map($currencies, 'id', 'name');

        return $this->render('_form', [
            'model' => $model,
            'langs' => $langs,
            'domains' => [],
            'currencies' => $currencies
        ]);
    }

    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('updateSites')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Site $model */

        $model = Site::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }

            return $this->redirect([
                'update',
                'id' => $model->id
            ]);
        }

        $this->view->title = 'Изменение сайта "' . $model->name . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список сайтов',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->name;

        $langs = Lang::find()
            ->where(['active' => 1])
            ->all();
        $langs = ArrayHelper::map($langs, 'id', 'name');

        $currencies = Currency::find()
            ->where(['active' => 1])
            ->all();
        $currencies = ArrayHelper::map($currencies, 'id', 'name');

        $domains = [''];
        if ($model->domains) {
            foreach ($model->domains as $domain) {
                $domains[] = $domain->name;
            }
        }

        return $this->render('_form', [
            'model' => $model,
            'langs' => $langs,
            'domains' => $domains,
            'currencies' => $currencies
        ]);
    }

    public function actionDuplicate($id)
    {
        if (!Yii::$app->user->can('updateSites')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Site $model */

        $model = Site::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        /* @var Site $newModel */

        $newModel = $model->duplicate();

        if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/update') !== false) {
            return $this->redirect([
                'update',
                'id' => $newModel->id
            ]);
        } else {
            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('updateSites')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Site $model */

        $model = Site::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect([
            'index'
        ]);
    }
}

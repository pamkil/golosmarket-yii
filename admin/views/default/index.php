<?php

use admin\widgets\AverageYear;
use admin\widgets\BayClient;
use admin\widgets\IndicatorFinance;
use admin\widgets\MonthCountOrder;
use admin\widgets\MonthProfit;
use admin\widgets\RegisterClient;
use admin\widgets\TodayCountOrder;
use admin\widgets\TodayProfit;

?>
<div class="row">
    <div class="col-md-4 col-sm-6">
        <?= TodayCountOrder::widget(); ?>
    </div>
    <div class="col-md-4 col-sm-6">
        <?= TodayProfit::widget(); ?>
    </div>

    <div class="col-md-4 col-sm-6">
        <?= MonthCountOrder::widget(); ?>
    </div>
    <div class="col-md-4 col-sm-6">
        <?= MonthProfit::widget(); ?>
    </div>

    <div class="col-md-4 col-sm-6">
        <?= AverageYear::widget(); ?>
    </div>

    <div class="col-md-4 col-sm-6">
        <?= RegisterClient::widget(); ?>
    </div>

    <div class="col-md-4 col-sm-6">
        <?= BayClient::widget(); ?>
    </div>
    <div class="col-md-4 col-sm-6">
        <?= IndicatorFinance::widget(); ?>
    </div>
</div>
<?php

namespace admin\controllers;

use admin\models\bill\BillSearch;
use common\models\user\Bill;
use Yii;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;

class BillController extends SecurityController
{
    public function actionIndex()
    {
        $searchModel = new BillSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        /** @var ActiveQuery $query */
        $query = clone $dataProvider->query;
        $query->andWhere(['is_payed' => true]);
        $queryAn = clone $query;
        $total = (float)$query->sum("bill.sum");
        $sumAn = (float)$queryAn->sum("bill.sum - bill.sum * bill.percent / 100");
        $sum = $total - $sumAn;

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'sum' => [
                    'sum' => $sum,
                    'sumAn' => $sumAn,
                    'total' => $total
                ]
            ]
        );
    }

    public function actionUpdate($id)
    {
        /* @var Bill $model */
        $model = Bill::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');

                return $this->redirect(
                    [
                        'update',
                        'id' => $model->id
                    ]
                );
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Изменение Тэга "' . $model->name . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список Тэгов',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->name;

        return $this->render(
            '_form',
            [
                'model' => $model
            ]
        );
    }
}
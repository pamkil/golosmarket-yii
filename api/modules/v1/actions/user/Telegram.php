<?php

namespace api\modules\v1\actions\user;

use api\modules\v1\actions\common\ViewAction;
use api\modules\v1\models\records\auth\User;
use Yii;
use yii\web\IdentityInterface;

class Telegram extends ViewAction
{
    public function run($id = null)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $user = $id ? User::findIdentity($id) : Yii::$app->user->identity;

        if (!$user instanceof IdentityInterface) {
            throw new NotFoundHttpException('The requested user does not exist.');
        }

        $security = Yii::$app->security;
        $telegramStartId = $security->generateRandomString(16);

        if (!$user->telegram_start_id) {
            $user->telegram_start_id = $telegramStartId;
        }

        $isUnique = true;
        do {
            if (!$user->validate() || !$user->save()) {
                $telegramStartId = $security->generateRandomString(16);
                $user->telegram_start_id = $telegramStartId;
                $isUnique = false;
            } else {
                $linkData = $user->telegram_start_id;

                return ['url' => 'https://t.me/glsmrkt_bot?start=' . $linkData];
            }
        } while (!$isUnique);
    }
}
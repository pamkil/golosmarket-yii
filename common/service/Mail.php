<?php

namespace common\service;

use common\models\site\Feedback;
use Exception;
use Yii;

class Mail
{
    private static $instance;

    public static function instance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param Feedback $feedback
     */
    public static function sendFeedback(Feedback $feedback)
    {
        $mailer = Yii::$app->mailer->compose('feedback', [
            'feedback' => $feedback
        ])
            ->setFrom(Yii::$app->params['emailFrom'])
            ->setTo(self::getEmailToSend(Yii::$app->params['emailTo']))
            ->setSubject('👋 Обратная связь на сайте golosmarket.ru');

        try {
            $mailer->send();
        }
        catch (Exception $exception) {
            Yii::error($exception->getMessage());
        }
    }

    /**
     * @param string $emailTo
     *
     * @return bool
     */
    public static function sendRemindMessage(string $emailTo)
    {
        $mailer = Yii::$app->mailer->compose('remindMessage')
            ->setFrom(Yii::$app->params['emailFrom'])
            ->setTo(self::getEmailToSend($emailTo))
            ->setSubject('📩 Сообщение на golosmarket.ru');

        $result = false;
        try {
            $result = $mailer->send();
        }
        catch (Exception $exception) {
            Yii::error($exception->getMessage());
        }

        return $result;
    }

    public static function sendSignupEmail(string $email, string $userName, string $verificationToken)
    {
        $verifyUrl = Yii::$app->urlManager->createAbsoluteUrl(['/verify-email', 'token' => $verificationToken]);

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                [
                    'userName' => $userName,
                    'verifyUrl' => $verifyUrl,
                ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo(self::getEmailToSend($email))
            ->setSubject('👍 Регистрация пользователя на ' . Yii::$app->name)
            ->send();
    }

    public static function sendPasswordReset(string $email, $passwordResetToken, $username = '')
    {

        $resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/reset', 'token' => $passwordResetToken]);

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                [
                    'username' => $username,
                    'resetLink' => $resetLink,
                ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo(self::getEmailToSend($email))
            ->setSubject('🔐 Восстановление пароля на ' . Yii::$app->name)
            ->send();
    }

    public static function sendProfileRestore(string $email, string $userName, string $verificationToken)
    {
        $resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/restore-profile', 'token' => $verificationToken]);

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'profileRestore-html', 'text' => 'profileRestore-text'],
                [
                    'userName' => $userName,
                    'resetLink' => $resetLink,
                ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo(self::getEmailToSend($email))
            ->setSubject('🤗 Восстановление профиля пользователя ' . Yii::$app->name)
            ->send();
    }

    private static function getEmailToSend($email)
    {
        if (!empty(Yii::$app->params['sendRealEmail'])) {
            return $email;
        }

        return ['zvr84@mail.ru' => 'real '. $email];
    }
}

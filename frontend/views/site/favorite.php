<script>
    var filtersList = <?= $filters ?? ''; ?>;
    var currentFilter = <?= $currentFilter ?? ''; ?>;
</script>

<section class="content contentpage favorite">
    <div class="container">
        <div class="titlepage">
            <a class="icon mobile-no" href="/">
                <svg xmlns="http://www.w3.org/2000/svg"
                     width="24"
                     height="24"
                     viewBox="0 0 24 24"
                     fill="none"
                     stroke="#000000"
                     stroke-width="2"
                     stroke-linecap="round"
                     stroke-linejoin="round"
                >
                    <path d="M19 12H6M12 5l-7 7 7 7"/>
                </svg>
            </a>
            <h1>Избранные голоса</h1>
        </div>
    </div>
</section>

<section class="filter-main">
    <div id="promo" class="container">
        <div id="filter" class="filter-holder"></div>
    </div>
</section>

<div id="favorite" class="favorite"></div>

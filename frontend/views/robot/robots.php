<?
/**
 * @var array $disallowYandex
 * @var array $disallowGoogle
 * @var array $disallowAll
 * @var array $allowYandex
 * @var array $allowGoogle
 * @var array $allowAll
 * @var string $domain
 * @var string $noIndex
 **/
?>
<?php if ($noIndex) : ?>
User-Agent: *
Disallow: /
<?php else : ?>
<?php if ($disallowYandex) { ?>
User-agent: Yandex<?= "\r\n" ?>
<? foreach ($disallowYandex as $item) { ?>
Disallow: <?= $item ?><?= "\r\n" ?>
<? }
foreach ($allowYandex as $item) { ?>
Allow: <?= $item ?><?= "\r\n" ?>
<? }
}
if ($disallowGoogle) { ?>
<?= "\r\n" ?>User-agent: Googlebot<?= "\r\n" ?>
<? foreach ($disallowGoogle as $item) { ?>
Disallow: <?= $item ?><?= "\r\n" ?>
<? }
foreach ($allowGoogle as $item) { ?>
Allow: <?= $item ?><?= "\r\n" ?>
<? }
}
if ($disallowAll) { ?>
<?= "\r\n" ?>User-agent: *<?= "\r\n" ?>
<? foreach ($disallowAll as $item) { ?>
Disallow: <?= $item ?><?= "\r\n" ?>
<? } ?>
<? foreach ($allowAll as $item) { ?>
Allow: <?= $item ?><?= "\r\n" ?>
<? }
} ?>
<?= "\r\n" ?>Host: <?= $domain ?><?= "\r\n" ?>
Sitemap: <?= $domain ?>/sitemap.xml
<?php endif; ?>
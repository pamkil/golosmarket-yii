<?php

use yii\db\Migration;

/**
 * Class m191120_213501_add_menu
 */
class m191120_213501_add_menu extends Migration
{
    private $menu = '{{%content_menu}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            $this->menu,
            [
                'id' => $this->primaryKey(),
                'type_id' => $this->integer()->notNull(),
                'path' => $this->string(1024)->notNull(),
                'title' => $this->string()->notNull(),
                'created_at' => $this->timestamp()->notNull(),
                'updated_at' => $this->timestamp()->notNull(),
                'deleted_at' => $this->timestamp(),
            ],
            $tableOptions
        );

        $this->createIndex('IX-content_menu-type_id', $this->menu, 'type_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->menu);
    }
}

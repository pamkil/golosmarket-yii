$(document).ready(function () {
    if (typeof wsServerUrl !== 'undefined') {
        var socket = (window.MySocket !== undefined) ? window.MySocket : window.MySocket = io(wsServerUrl);

        socket.on('update list', function (data) {
            //todo params list static data!
            var row = $('tr[data-class="' + data.baseClass + '"][data-key="' + data.baseId + '"]');
            var attrs = data.attributes;
            if (attrs) {
                var isReload = false;
                $.each(attrs, function (index, value) {
                    var td = row.find('td[data-attribute="' + index + '"]');
                    if (td.length) {
                        td.html(value);
                        var table = $('table.reload-page[data-class="' + data.baseClass + '"]');
                        if (table.length) {
                            isReload = true;
                        }
                    }
                });
                if (isReload) {
                    reloadPjax();
                }
            }

            if (data.baseClass === 'OrderManager') {
                reloadOrderManager();
            }
        });

        socket.on('update detail', function (data) {
            //todo params list static data!
            var form = $('form[data-key="' + data.baseId + '"]');
            var attrs = data.attributes;
            if (attrs) {
                $.each(attrs, function (index, value) {
                    form.find('input[name^="' + data.supportClass + '"][name$="\[' + index + '\]"]').val(value);
                });
            }
        });

        socket.on('delete list', function (data) {
            var row = $('tr[data-class="' + data.baseClass + '"][data-key="' + data.baseId + '"]');
            if (row.length) {
                row.remove();
            }
        });

        socket.on('delete detail', function (data) {
            var form = $('form[data-key="' + data.baseId + '"]');
            if (form.length) {
                if (form.find('input[name^="' + data.baseClass + '\["]').length) {
                    // TODO: fix reload in main page, or redirect to index
                    reloadPjax();
                }
            }
        });

        socket.on('insert list', function (data) {
            if (data.baseClass === 'Order'
              && $('.is-first-page').length
              && $('table.reload-page[data-class="' + data.baseClass + '"]').length
            ) {
                reloadPjax();
            }

            if (data.baseClass === 'OrderManager') {
                reloadOrderManager();
            }
        });
    }

    function reloadOrderManager() {
        setTimeout(function () {
            $.pjax.reload('#mainOrderManagerContainer', {
                url: '/default/get-order-manager',
                push: false,
                replace: false
            });
        }, 7000);
    }

    function reloadPjax() {
        var containerName = '#mainPjaxContainer',
          pjaxContainer = $(containerName);
        if (pjaxContainer.length) {
            $.pjax.reload(containerName, {timeout : 0});

            return true;
        } else {
            location.reload();
        }

        return false;
    }

    // ping пользователя, обновление асинхронно время последнего посещения
    var ping = function () {
        let timeout = 60000;
        $.ajax({
            url : '/default/ping',
            complete : function () {
                setTimeout(function () {
                    ping();
                }, timeout);
            },
            fail: function() {
                timeout = timeout * 2;
            }
        });
    };
    ping();
});

<?php

use yii\db\Migration;

/**
 * Class m200215_100231_add_refresh_user_token
 */
class m200215_100231_add_refresh_user_token extends Migration
{
    private $userToken = '{{%user_token}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->userToken,
            'refresh_token',
            $this->string(255)->null()->after('expired_at')
        );

        $this->addColumn(
            $this->userToken,
            'refresh_expired_at',
            $this->dateTime()->null()->after('refresh_token')
        );

        $this->createIndex('ix-user_token-refresh_token', $this->userToken, 'refresh_token');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->userToken, 'refresh_token');
        $this->dropColumn($this->userToken, 'refresh_expired_at');
    }
}

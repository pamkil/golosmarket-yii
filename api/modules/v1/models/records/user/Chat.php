<?php

namespace api\modules\v1\models\records\user;

use api\modules\v1\models\records\auth\User;
use api\modules\v1\models\records\file\File;
use Carbon\Carbon;
use common\service\chat\enum\FileTypeEnum;

/**
 * @OA\Schema(required={"name"}, title="Чат")
 */
class Chat extends \common\models\user\Chat
{
    public $unreadMessages = 1;

    /**
     * @OA\Property(property="id", type="integer", description="ИД диктора")
     * @OA\Property(property="announcer_id", type="integer", description="ИД диктора")
     * @OA\Property(property="client_id", type="integer", description="ИД клиента")
     * @OA\Property(property="file_id", type="integer", description="ИД прикрепленного файла")
     * @OA\Property(property="text", type="string", description="URL фото диктора")
     * @OA\Property(property="is_read", type="boolean", description="Флаг прочитано сообщения")
     * @OA\Property(property="is_send", type="boolean", description="Фдаг доставлен")
     * @OA\Property(property="is_edit", type="boolean", description="Отредактирован")
     * @OA\Property(property="created_at", type="boolean", description="Время создания")
     * @OA\Property(property="updated_at", type="boolean", description="Время обновления")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'opponentId' => 'opponentId',
            'avatar' => 'avatar',
            'name' => 'name',
            'text',
            'file',
            'fileType' => 'fileType',
            'cost',
            'isRead' => 'isRead',
            'isWriteOpponent' => 'isWriteOpponent',
            'parent' => 'parent',
            'createdAt' => 'createdAt',
            'updatedAt' => 'updatedAt',
        ];
    }

    public function getCreatedAt()
    {
        if (empty($this->updated_at)) {
            return null;
        }

        return Carbon::parse($this->created_at, 'UTC')->toIso8601String();
    }

    public function getUpdatedAt()
    {
        if (empty($this->updated_at)) {
            return null;
        }

        return Carbon::parse($this->updated_at, 'UTC')->toIso8601String();
    }

    public function getFileType()
    {
        if (!$this->file_id) {
            return null;
        }

        if (self::isAnnouncer() || $this->file_type === FileTypeEnum::FILE_TYPE_ALL) {
            return $this->file_type;
        }

        return $this->file_type === FileTypeEnum::FILE_TYPE_ORIGIN ? FileTypeEnum::FILE_TYPE_WATERMARK : FileTypeEnum::FILE_TYPE_PUBLIC;
    }

    private static $isAnnouncer;

    protected static function isAnnouncer()
    {
        if (self::$isAnnouncer) {
            return self::$isAnnouncer;
        }

        return self::$isAnnouncer = \Yii::$app->user->identity->type === User::TYPE_ANNOUNCER;
    }

    public function getOpponentId()
    {
        return self::isAnnouncer() ? $this->client_id : $this->announcer_id;
    }

    public function getIsRead()
    {
        return !!$this->is_read;
    }

    public function getCost()
    {
        return self::isAnnouncer() ? null : $this->announcer->announcer->cost;
    }

    public static function getUnread(int $opponentId, bool $_isAnnouncer = null)
    {
        $__isAnnouncer = $_isAnnouncer !== null ? $_isAnnouncer : self::isAnnouncer();

        $fieldSelf = $__isAnnouncer ? 'announcer_id' : 'client_id';
        $fieldOpponent = $__isAnnouncer ? 'client_id' : 'announcer_id';

        return (int)self::find()
            ->where([
                $fieldSelf => \Yii::$app->user->getId(),
                $fieldOpponent => $opponentId,
                'is_read' => 0,
                'is_write_client' => $__isAnnouncer,
                'deleted_at' => 0,
            ])
            ->count();
    }

    public function getIsWriteOpponent()
    {
        return (self::isAnnouncer() && $this->is_write_client) || (!self::isAnnouncer() && !$this->is_write_client);
    }

    public function getIsOnline()
    {
        if (self::isAnnouncer() || !$this->announcer->announcer || !$this->announcer->announcer->scheduleWork) {
            return false;
        }

        [$isOnline] = $this->announcer->announcer->scheduleWork->getIsOnline($this->announcer);

        return !!$isOnline;
    }

    public function getName()
    {
        $user = self::isAnnouncer() ? $this->client : $this->announcer;
        $lastName = mb_substr($user->lastname ?? '', 0, 1);

        return "$user->firstname $lastName";
    }

    public function getAvatar()
    {
        return self::isAnnouncer() ? $this->client->getPhotoUrl() : $this->announcer->getPhotoUrl();
    }

    public function getAnnouncer()
    {
        return $this->hasOne(User::class, ['id' => 'announcer_id']);
    }

    public function getClient()
    {
        return $this->hasOne(User::class, ['id' => 'client_id']);
    }

    public function getParent()
    {
        return $this->hasOne(Chat::class, ['id' => 'parent_id']);
    }

    public function getFile()
    {
        if (in_array($this->file_type, [FileTypeEnum::FILE_TYPE_ALL, FileTypeEnum::FILE_TYPE_PUBLIC])) {
            return $this->hasOne(File::class, ['id' => 'file_id']);
        }

        return $this->hasOne(File::class, ['id' => 'file_watermark_id']);
    }

    public static function findByRequest($filter, int $page = 1, int $perPage = 26)
    {
        $limit = $perPage > 50 ? 50 : $perPage;
        $offset = ($page - 1) * $limit;

        $user = self::find()
            ->alias('announcer')
            ->joinWith(['user user'])
            ->where([])
            ->limit($limit)
            ->offset($offset)
            ->all();
    }
}

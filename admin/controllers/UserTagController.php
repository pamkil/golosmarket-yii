<?php

namespace admin\controllers;

use admin\models\announcer\TagSearch;
use common\models\announcer\Tag;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class UserTagController extends SecurityController
{
    public function actionIndex()
    {
        $searchModel = new TagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionCreate()
    {
        $model = new Tag();

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно создано');

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Создание тембров';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $model->active = 1;

        return $this->render('_form', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        /* @var Tag $model */
        $model = Tag::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Изменение Тэга "' . $model->name . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список Тэгов',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->name;

        return $this->render('_form', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        /* @var Tag $model */
        $model = Tag::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect('index');
    }
}
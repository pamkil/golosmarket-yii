<?php
/**
 * @var int $profit
 * @var int $plan
 * @var int $delta
 * @var int $percent
 * @var int $percentDay
 */
$delta = number_format($delta, 0, ',', ' ');
$percent = number_format($percent, 0, ',', ' ');
$percentDay = number_format($percentDay, 0, ',', ' ');
?>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-green-sharp">
                    <span class="tooltips" title="Дельта план-факт: <?= $delta; ?>">
                        <?= number_format($profit, 0, ',', ' '); ?>
                    </span>
                    <small class="font-green-sharp">₽</small>
                    <span style="font-size: 20px;">
                        (<span class="tooltips" title="Дельта план-факт: <?= $delta; ?>"
                            ><?= number_format($plan, 0, ',', ' '); ?>
                        </span>
                        <small class="font-green-sharp">₽</small>)
                        <small title="На сегодня: <?= $percentDay; ?>%"><?= $percent; ?>%</small>
                    </span>
                </h3>
                <small>Прибыль факт. (план)</small>
            </div>
            <div class="icon">
                <i class="icon-pie-chart"></i>
            </div>
        </div>
    </div>
</div>
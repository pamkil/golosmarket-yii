<?php

namespace admin\controllers;

use admin\models\review\ReviewSearch;
use Carbon\Carbon;
use common\models\user\Review;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ReviewController extends SecurityController
{
    public function actionIndex()
    {
        $searchModel = new ReviewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список отзывов';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionCreate()
    {
        throw new BadRequestHttpException('не поддерживается');
    }

    public function actionUpdate($id)
    {
        /* @var Review $model */
        $model = Review::findOne(['id' => (int)$id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Изменение отзыва "' . $model->id . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список отзывов',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->id;

        if (isset($model->created_at)) {
            $model->created_at = Carbon::parse($model->created_at)->format('d.m.Y');
        }

        return $this->render('_form', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        /* @var Review $model */
        $model = Review::findOne(['id' => (int)$id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect('/news/index');
    }
}
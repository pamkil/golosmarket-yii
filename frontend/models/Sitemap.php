<?php

namespace frontend\models;

use api\modules\v1\models\records\announcer\Announcer;
use common\models\content\News;
use common\models\content\Page;
use Exception;
use yii\base\Model;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

class Sitemap extends Model
{
    /**
     * Получение статических страниц
     *
     * @return array
     */
    private function getStaticPages()
    {
        $priority = '0.9';
        $result = [];
        $pageQuery = Page::find()
            ->select(['id', 'path'])
            ->where(['<>', 'path', '/'])
            ->asArray();

        $result[] = [
            'loc' => Url::to(['site/main'], true),
            'priority' => '1.0',
        ];
        $result[] = [
            'loc' => Url::to(['faq/index'], true),
            'priority' => '0.9',
        ];

        foreach ($pageQuery->each() as $page) {
            $result[] = [
                'loc' => Url::to(['page/view', 'code' => ltrim($page['path'], '/')], true),
                'priority' => $priority,
            ];
        }

        return $result;
    }

    /**
     * Получение статических страниц
     *
     * @return array
     */
    private function getProfilePages()
    {
        $priority = '0.9';
        $result = [];
        $pageQuery = Announcer::find()
            ->select(['id', 'user_id'])
            ->with(['reviews' => function($query) {
                $query->where(['is_public' => true])->limit(1);
            }])
            ->asArray();

        foreach ($pageQuery->each() as $user) {
            $result[] = [
                'loc' => Url::to(['profile/view', 'id' => $user['user_id']], true),
                'priority' => $priority,
            ];
            if (!empty($user['reviews'])) {
                $result[] = [
                    'loc' => Url::to(['profile/review', 'id' => $user['user_id']], true),
                    'priority' => $priority,
                ];
            }
        }
        return $result;
    }

    /**
     * Получение страниц новостей
     *
     * @return array
     * @throws Exception
     */
    private function getNewsPages()
    {
        $perPage = 20;
        $news = News::find()
            ->where(['active' => true])
            ->count();

        $totalPages = ceil($news / $perPage);

        $results = [
            [
                'loc' => Url::to(['news/index'], true),
                'priority' => '0.9',
            ]
        ];


        if ($totalPages > 1 ) {
            foreach (range(2, $totalPages) as $page) {
                $results[] = [
                    'loc' => Url::to(['news/index', 'page' => $page], true),
                    'priority' => '0.8',
                ];
            }
        }

        return $results;
    }

    public function generate()
    {
        return ArrayHelper::merge(
            $this->getStaticPages(),
            $this->getNewsPages(),
            $this->getProfilePages()
        );
    }
}

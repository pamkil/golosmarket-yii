export const STORAGE_KEY = {
    LAST_PATHNAME: 'last-pathname',
    TOKEN: 'token',
    TOKEN_EXPIRED: 'token-expired',
    OTHER: 'other',
    TOKEN_REFRESH: 'token-refresh',
    TOKEN_REFRESH_EXPIRED: 'token-refresh-expired',
    FAVORITES: 'favorites',
};

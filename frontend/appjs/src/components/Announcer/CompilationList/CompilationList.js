import React, {useEffect, useState} from 'react';
import PlayElement from "../ItemList/PlayElement";
import {eventPlay, eventStop} from "../../Common/PlayerEvent/playerEvent";
import {toast} from "react-toastify";
import {textCopy} from "../Announcer";
import {PLAYER} from "../../Common/PlayerEvent/playerConst";
import copy from "copy-to-clipboard";
import ShareButton from "../../Common/ShareButton/ShareButton";
import './collection.css'

const CompilationList = ({items, mobile, ...props}) => {
  const visibleCountStep = 3;
  const desktopFactor = mobile ? 0 : 3;
  const [visibleCount, setVisibleCount] = useState(3);
  const [playing, setPlaying] = useState('');

    useEffect(() => {
        document.addEventListener(PLAYER.PLAYING, playingHandler);
        document.addEventListener(PLAYER.STOP, stopHandler);

        return () => {
            document.removeEventListener(PLAYER.PLAYING, playingHandler);
            document.removeEventListener(PLAYER.STOP, stopHandler);
        }
    }, []);
    const playHandler = (file, itemFiles, announcer) => {
        const files = [];
        for (let annons of [itemFiles]) {
            for (let item of annons) {
                let itemAnnouncer = item.announcer;
                if (itemAnnouncer.sound && itemAnnouncer.sound.length > 0) {
                    item.sound.sound.announcer = itemAnnouncer;
                    files.push(item.sound.sound);
                }
            }
        }
        eventPlay(file.announcer, file.sound, files, true);
    };
    const stopHandler = () => {
        setPlaying('');
    };

    const stopClickHandler = () => {
        eventStop();
    };

    const playingHandler = ({detail: {file}}) => {
        setPlaying(file.url);
    };
    const copyHandler = url => {
        copy(document.location.origin + url);
        toast.info(textCopy);
    }

  return (
    <div className="container">
      <ul className="compilation_list">
        {
          items.slice(0, visibleCount + desktopFactor).map((item, ind) => <li key={ind}>
            <div className="compilation_item">
              <div className="compilation_item--wrapper">
                <div className="compilation_item--content">
                  <a href={'/collection/' + item.id}>
                    <div className="compilation_item--image">
                      <img src={'/upload/' + item.image.path + '/' + item.image.filename + '.' + item.image.ext}
                           alt={item.name}/>
                    </div>
                  </a>
                  <ShareButton
                    addClass="collection-list"
                    popup="Ссылка на подборку скопирована"
                    isCollection={true}
                    url={document.location.origin + '/collection/' + item.id}
                  />
                  <PlayElement
                    {...{
                      sound: item.collectionSounds,
                      isFavorite: false
                    } }
                    copyHandler={copyHandler}
                    onClickPlay={playHandler}
                    onClickStop={stopClickHandler}
                    playingSong={playing}
                    addClass={'collection_button'}
                  />
                </div>
              </div>
              <div className="compilation_item--description">
                <h4>{item.name}</h4>
                <p>{item.description}</p>
              </div>
            </div>
          </li>)
        }
      </ul>

      {items.length > (visibleCount + desktopFactor) ? <div className="catalog__more">
        <button onClick={() => {
          setVisibleCount(visibleCountStep + visibleCount);
        }} className="green">Показать больше подборок
        </button>
      </div> : null}
    </div>
  )
};

export default CompilationList;

import React from 'react';
import {ReactComponent as MsgSquare} from '../../../images/icon-msg-square.svg';


const SendAnnouncerButton = ({
                               isAnnouncer,
                               isLogin,
                               id,
                               onClickSendMessage
                             }) => {
  if (!isLogin) {
    return (
      <button
        className="send-announcer-button"
        type="button"
        disabled={isAnnouncer}
        onClick={onClickSendMessage}
      >
        <div className={'tooltip'}>
          <div className="icon">
            <MsgSquare/>
            <span>Написать&nbsp;в&nbsp;чат</span>
          </div>
        </div>
      </button>
    )
  }

  if (!isAnnouncer && isLogin) {
    return (
      <a href={`/chat/${id}`} className="send-announcer-button">
        <MsgSquare/>
      </a>
    )
  }

  return null;
}

export default SendAnnouncerButton

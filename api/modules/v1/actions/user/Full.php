<?php

namespace api\modules\v1\actions\user;

use api\modules\v1\actions\common\ViewAction;
use api\modules\v1\models\records\announcer\UserEdit;
use common\models\announcer\Age;
use common\models\announcer\AnnouncerSound as AnnouncerSoundModel;
use common\models\announcer\Gender;
use common\models\announcer\Presentation;
use common\models\announcer\Timber;
use Yii;
use common\models\access\User;

class Full extends ViewAction
{
    /**
     * @OA\Get(
     *   path="/user/full",
     *   summary="Profile of user. Получение полей пользователя",
     *   tags={"User"},
     *   security={{"BearerAuth": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="query",
     *     description="id of user",
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Объект ползователя",
     *     @OA\JsonContent(ref="#/components/schemas/User")
     *   )
     * )
     *
     * @param null|int $id
     * @return array
     */
    public function run($id = null)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $id = Yii::$app->request->getQueryParam('id');
        $profile = UserEdit::findModel($id ?? Yii::$app->user->id);

        $user = User::findOne($id ?? Yii::$app->user->id);

        $presentations = $this->convertOptions(Presentation::getAllList('name'));

        $optionLanguages = AnnouncerSoundModel::getAllLanguages();

        return [
            'profile' => $profile,
            'options' => [
                'optionGenders' => $this->convertOptions(Gender::getAllList('name')),
                'optionAges' => $this->convertOptions(Age::getAllList('name')),
                'optionPresentations' => $presentations,
                'optionLanguages' => $optionLanguages,
                'optionTags' => [],
            ],
            'skills' => $profile->getSkills(),
            'notifications' => [
                'sms' => $user->notify_sms === 1,
                'email' => $user->notify_email === 1,
                'telegram' => !empty($user->telegram_chat_id)
            ]
        ];
    }

    private function convertOptions(array $options)
    {
        $out = [];

        foreach ($options as $code => $option) {
            $out[] = [
                'id' => $code,
                'name' => $option,
            ];
        }

        return $out;
    }
}

<?php

namespace common\service\profile\enum;

class WeekDaysEnum
{
    public const MONDAY = 1;
    public const TUESDAY = 2;
    public const WEDNESDAY  = 3;
    public const THURSDAY = 4;
    public const FRIDAY = 5;
    public const SATURDAY = 6;
    public const SUNDAY = 7;
    public const WEEKENDS = 'dayOff';

    public static function getKeys()
    {
        return [
            self::MONDAY,
            self::TUESDAY,
            self::WEDNESDAY,
            self::THURSDAY,
            self::FRIDAY,
            self::SATURDAY,
            self::SUNDAY,
            self::WEEKENDS,
        ];
    }

    public static function getLabels()
    {
        return [
            self::MONDAY => 'Понедельник',
            self::TUESDAY => 'Вторник',
            self::WEDNESDAY => 'Среда',
            self::THURSDAY => 'Четверг',
            self::FRIDAY => 'Пятница',
            self::SATURDAY => 'Суббота',
            self::SUNDAY => 'Воскресенье',
            self::WEEKENDS => 'Выходные',
        ];
    }

    public static function getShortLabels()
    {
        return [
            self::MONDAY => 'ПН',
            self::TUESDAY => 'ВТ',
            self::WEDNESDAY => 'СР',
            self::THURSDAY => 'ЧТ',
            self::FRIDAY => 'ПТ',
            self::SATURDAY => 'СБ',
            self::SUNDAY => 'ВС',
            self::WEEKENDS => 'Вых',
        ];
    }
}

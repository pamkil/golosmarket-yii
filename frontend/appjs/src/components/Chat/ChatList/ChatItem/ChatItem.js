import React, { Component } from 'react';
import { Avatar } from 'react-chat-elements';
import './ChatItem.css';
import { format } from 'timeago.js';
import classNames from 'classnames';
import NotifyCount from "../../../Common/NotifyCount/NotifyCount";

export class ChatItem extends Component {
    constructor(props) {
        super(props);
        this.messageTitleRef = React.createRef();
      }
    componentDidMount(){
        const element = this.messageTitleRef.current;
        // const element = document.querySelector('.rce-citem-body--bottom-title');
        // if (element) {
        //         console.log(this.props.title)
        //   element.innerHTML = this.props.title;
        
        if (element) {
            element.innerHTML = this.props.subtitle;
          }
    }

    render() {
        const { isActive, className, statusColorType, srvr } = this.props;

        return (
            <div
                className={classNames('rce-container-citem', className)}
                onClick={this.props.onClick}
                onContextMenu={this.props.onContextMenu}
            >
                <div className={classNames("rce-citem", { 'rce-citem--active': isActive })}>
                    <div className={classNames(
                        "rce-citem-avatar",
                        {
                            'rce-citem-status-encircle': statusColorType === 'encircle',
                        }
                    )}>
                        <Avatar
                            src={srvr + this.props.avatar}
                            alt={this.props.alt}
                            className={statusColorType === 'encircle' ? 'rce-citem-avatar-encircle-status' : ''}
                            size="large"
                            letterItem={this.props.letterItem}
                            sideElement={
                                this.props.statusColor &&
                                <span
                                    className='rce-citem-status'
                                    style={statusColorType === 'encircle' ? {
                                        boxShadow: `inset 0 0 0 2px ${this.props.statusColor}, inset 0 0 0 5px #FFFFFF`
                                    } : {
                                        backgroundColor: this.props.statusColor,
                                    }}>
                                    {this.props.statusText}
                                </span>
                            }
                            onError={this.props.onAvatarError}
                            lazyLoadingImage={this.props.lazyLoadingImage}
                            type={classNames('circle', {'flexible': this.props.avatarFlexible})}
                        />
                        {this.props.isOnline && <div className='rce-citem-avatar__online' />}
                    </div>

                    <div className="rce-citem-body">
                        <div className="rce-citem-body--top">
                            <div className="rce-citem-body--top-title">
                            {/* <p ref={this.messageTitleRef} ></p>  */}
                             {this.props.title}
                            </div>
                            <div className="rce-citem-body--top-time">
                                {
                                    this.props.date &&
                                    !isNaN(this.props.date) &&
                                    (
                                        this.props.dateString ||
                                        format(this.props.date, 'ru_RU')
                                    )
                                }
                            </div>
                        </div>

                        <div className="rce-citem-body--bottom">
                            <div ref={this.messageTitleRef} className="rce-citem-body--bottom-title">
                               {this.props.subtitle}
                            </div>
                            <div className="rce-citem-body--bottom-status">
                                {
                                    this.props.unread > 0 &&
                                        <NotifyCount count={this.props.unread}/>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ChatItem.defaultProps = {
    id: '',
    onClick: null,
    avatar: '',
    avatarFlexible: false,
    alt: '',
    title: '',
    subtitle: '',
    date: new Date(),
    unread: 0,
    statusColor: null,
    statusColorType: 'badge',
    statusText: null,
    dateString: null,
    lazyLoadingImage: undefined,
    onAvatarError: () => void(0),
}

export default ChatItem;

<?php

use yii\rest\UrlRule;

return [
    'enablePrettyUrl' => true,
    'enableStrictParsing' => true,
    'showScriptName' => false,
    'rules' => [
        'swagger.json' => 'v1/default/swagger',
        'swagger/?<id:>?' => 'swagger/ui/swagger',
        'test' => 'swagger/ui/test',

        // chat
        [
            'class' => UrlRule::class,
            'controller' => [
                'v1/chat',
            ],
            'patterns' => [
                'GET,HEAD <id:\d+>' => 'view',
                'POST <id:\d+>' => 'create',
                'DELETE <id:\d+>' => 'delete',
                'POST <id:\d+>/read' => 'read',
                'GET,HEAD' => 'index',
                '<id:\d+>' => 'options',
                'GET,HEAD <id:\d+>/bill' => 'viewBill',
                'POST <id:\d+>/bill' => 'createBill',
                'POST <id:\d+>/share' => 'shareFile',
                'PUT,PATCH <id:\d+>/bill' => 'updateBill',
                'DELETE <id:\d+>/bill' => 'deleteBill',
                '<id:\d+>/bill' => 'options',
                '<id:\d+>/share' => 'options',
                '<id:\d+>/read' => 'options',
                '' => 'options',
            ],
            'pluralize' =>  false,
        ],

        // announcer
        [
            'class' => UrlRule::class,
            'controller' => [
                'v1/announcer',
            ],
            'extraPatterns' => [
                'OPTIONS deletes' => 'options',
            ],
            'pluralize' =>  false,
        ],

        // collections
        [
            'class' => UrlRule::class,
            'controller' => [
                'v1/collection',
            ],
            'extraPatterns' => [
                'POST sounds' => 'sounds',
                'OPTIONS sounds' => 'options'
            ],
            'pluralize' =>  false,
        ],

        // money
        [
            'class' => UrlRule::class,
            'controller' => [
                'v1/money',
            ],
            'patterns' => [
                'GET,POST' => 'create',
                '' => 'options',
            ],
            'pluralize' =>  false,
        ],

        // review
        [
            'class' => UrlRule::class,
            'controller' => [
                'v1/review',
            ],
            'patterns' => [
                'POST {id}' => 'create',
                'POST service' => 'service',
                'POST user' => 'user',
                'GET,HEAD' => 'index',
                'service' => 'options',
                'user' => 'options',
                '' => 'options',
            ],
            'pluralize' =>  false,
        ],

        // feedback
        [
            'class' => UrlRule::class,
            'controller' => [
                'v1/feedback',
            ],
            'patterns' => [
                'POST' => 'create',
                '' => 'options',
            ],
            'pluralize' =>  false,
        ],

        // notify
        [
            'class' => UrlRule::class,
            'controller' => [
                'v1/notify',
            ],
            'patterns' => [
                'POST' => 'read',
                '' => 'options',
            ],
            'pluralize' =>  false,
        ],

        // favorite
        [
            'class' => UrlRule::class,
            'controller' => 'v1/favorite',
            'patterns' => [
                'GET,HEAD {id}' => 'view',
                'POST {id}' => 'create',
                'DELETE {id}' => 'delete',
                'PATCH' => 'update',
                '{id}' => 'options',
                '' => 'options',
            ],
            'pluralize' =>  false,
        ],

        // file
        [
            'class' => UrlRule::class,
            'controller' => 'v1/file',
            'patterns' => [
                'POST download/{id}' => 'download',
                'POST download' => 'download',
                '{id}' => 'options',
                'download' => 'options',
            ],
            'pluralize' =>  false,
        ],

        // auth
        [
            'class' => UrlRule::class,
            'controller' => 'v1/auth',
            'patterns' => [
                'POST login' => 'login',
                'OPTIONS login' => 'options',
                'POST refresh' => 'refresh',
                'OPTIONS refresh' => 'options',
                'POST register' => 'register',
                'OPTIONS register' => 'options',
                'POST restore' => 'restore',
                'OPTIONS restore' => 'options',
                'OPTIONS' => 'options',
            ],
            'pluralize' =>  false,
        ],

        // user
        [
            'class' => UrlRule::class,
            'controller' => 'v1/user',
            'patterns' => [
                'GET profile' => 'profile',
                'GET telegram' => 'telegram',
                'OPTIONS profile' => 'options',

                'GET full' => 'full',
                'PUT,PATCH full' => 'update',
                'OPTIONS full' => 'options',

                'GET bill' => 'bill',
                'OPTIONS bill' => 'options',

                'GET payed' => 'payedView',
                'PUT payed' => 'payedUpdate',
                'OPTIONS payed' => 'options',

                'PUT,PATCH schedule' => 'schedule',
                'OPTIONS schedule' => 'options',

                'GET index' => 'index',
                'DELETE {id}' => 'delete',
                'OPTIONS index' => 'options',
                'OPTIONS {id}' => 'options',
                'OPTIONS' => 'options',

//                'POST create' => 'create',
//                'OPTIONS create' => 'options',

                'GET roles' => 'roles',
                'OPTIONS roles' => 'options',

                'PUT file' => 'file',
                'OPTIONS file' => 'options',

                'PUT sound' => 'sound',
                'OPTIONS sound' => 'options',
            ],
            'pluralize' =>  false,
        ],

        // telegram bot
        [
            'class' => UrlRule::class,
            'controller' => 'v1/telegram',
            'patterns' => [
                'POST webhook/<token>' => 'webhook'
            ],
            'pluralize' => false
        ]
    ],
];

<?php

use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Url;

/**
 * @var ActiveDataProvider $dataProvider
 * @var \admin\models\content\BannerSearch $searchModel
 */
?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <a href="<?= Url::to(['create']) ?>" class="btn bg-olive btn-flat margin">
            Добавить
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <div class="box-body">
        <?= GridView::widget(
            [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    'id',
                    'active:boolean',
                    'position',
                    [
                        'content' => function ($data) {
                            return '<img src="' . $data->file->getUrl()
                                . '" class="img-responsive" style="max-height: 250px; max-width: 250px;"/>';
                        }
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{update}  {delete}',
                        'buttonOptions' => ['class' => 'margin']
                    ],
                ]
            ]
        ) ?>
    </div>
</div>

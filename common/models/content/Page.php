<?php

namespace common\models\content;

use common\models\Model;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;

/**
 * Автор публикаций
 *
 * Class Page
 *
 * @package common\models\content
 *
 * @property int         $id
 * @property string      $title
 * @property string      $path
 * @property string      $description
 * @property string      $h1
 * @property string      $body
 * @property string      $after_body
 * @property string      $keywords
 * @property string      $code [varchar(255)]
 * @property int         $created_at [timestamp]
 * @property int         $updated_at [timestamp]
 * @property int         $deleted_at [timestamp]
 * @property PageFilter  $filter
 * @property PageSimilar $similar
 * @property bool        $show_announcers [tinyint(1)]
 * @property bool        $show_filters [tinyint(1)]
 * @property bool        $show_similar [tinyint(1)]
 *
 * @property PageAccordeon $accordeon
 */
class Page extends Model
{
    public static function tableName()
    {
        return '{{%content_pages}}';
    }

    public function rules()
    {
        return [
            [
                ['title', 'path', 'description', 'h1', 'keywords', 'body', 'after_body', 'code'],
                'string',
            ],
            [
                ['show_announcers', 'show_filters', 'show_similar'],
                'boolean',
            ],
            [
                ['accordeon'], 'safe'
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Символьный код',
            'show_announcers' => 'Показывать дикторов',
            'show_similar' => 'Показывать похожих дикторов',
            'show_filters' => 'Показывать блок фильтров дикторов',
            'title' => 'Заголовок | GolosMarket',
            'path' => 'Путь к странице',
            'description' => 'SEO description',
            'h1' => 'Заголовок H1 на странице',
            'keywords' => 'SEO keywords',
            'body' => 'Содержимое страницы до дикторов (или без них)',
            'after_body' => 'Содержимое страницы после дикторов',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
            'accordeon' => 'Пункты аккордеона на странице',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['sluggableBehavior']['attribute'] = 'title';
        $behaviors['saveRelations'] = [
            'class' => SaveRelationsBehavior::class,
            'relations' => [
                'accordeon'
            ]
        ];

        return $behaviors;
    }

    public static function getMainPage($queryString = '')
    {
        return Page::find()->where(
            [
                'OR',
                ['path' => '/'],
                ['path' => '/?' . $queryString],
            ]
        )->orderBy(['path' => SORT_DESC])->asArray()->one();
    }

    public static function getByPath(string $code = '', string $param = '', bool $withFilter = false)
    {
        if ($param) {
            $where = [
                'OR',
                ['path' => '/' . $code],
                ['path' => '/' . $param],
            ];
        } else {
            $where = ['path' => '/' . $code];
        }

        $pageQuery = Page::find()->where($where)->asArray();

        if ($withFilter) {
            $call = static function ($query) {
                $query->where(['active' => true])->select('code');
            };
            $pageQuery->with(
                [
                    'filter',
                    'filter.age' => $call,
                    'filter.language' => $call,
                    'filter.presentation' => $call,
                    'filter.gender' => $call,
                    'similar',
                    'similar.age' => $call,
                    'similar.language' => $call,
                    'similar.presentation' => $call,
                    'similar.gender' => $call,
                ]
            );
        }

        return $pageQuery->one();
    }

    public function getFilter()
    {
        return $this->hasOne(PageFilter::class, ['page_id' => 'id']);
    }

    public function getSimilar()
    {
        return $this->hasOne(PageSimilar::class, ['page_id' => 'id']);
    }

    public function getAccordeon()
    {
        return $this->hasMany(PageAccordeon::class, ['page_id' => 'id']);
    }
}

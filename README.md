# Recommendation
setting for nginx in path `./nginx/golosmarket.conf` 

#DIRECTORY STRUCTURE
-------------------

```
common   - Общие файлы
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
console   - Консольные команды
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
admin  - Панель администратора
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for backend application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
api  - RestApi (АПИ) для работы с сайтом для React framework приложений
    config/              contains backend configurations
    modules/swagger      contains swagger UI
    modules/v1           contains RestAPI controller, actions, models classes
    models/              empty path
    runtime/             contains files generated during runtime
    web/                 contains the entry script and Web resources
frontend   - ОТобрадение сайта клиентам
    appjs/               contains frontend REACT project files (JavaScript and CSS)
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```

# Действия при разворачивании
## 0 Сделать backup текущих БД (шпаргалка по БД)
Одной БД
```bash
mysqldump -u [user] -p [database_name] > [filename].sql
```
Всех БД
```bash
mysqldump --all-databases --single-transaction --quick --lock-tables=false > full-backup-$(date +%F).sql -u root -p
```
Загрузить дамп из файла
```bash
mysql -u [user] -p [database_name] < [filename].sql
```
```bash
#command
now=`date +"%Y-%m-%d"` && mysqldump -u [user] -p zvr84m02_golos > zvr84m02_golos_${now}.sql
```
## 1 Установить php7.4, mariadb, redis, composer
```bash
#curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"  && \
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer --1  && \
php -r "unlink('composer-setup.php');"  && \
composer global require hirak/prestissimo  && \
composer global require "fxp/composer-asset-plugin:^1.2.0" --no-progress

composer install --no-dev --prefer-dist --no-progress --no-suggest --optimize-autoloader
```
## 1.2 Настроить соединения с базой данных (db2 - только для импорта):
```php
'db' => [
    'class' => \yii\db\Connection::class,
    'dsn' => 'mysql:host=localhost;dbname=golosmarket',
    'username' => 'pamkil',
    'password' => '',
    'charset' => 'utf8',
],
'db2' => [
    'class' => \yii\db\Connection::class,
    'dsn' => 'mysql:host=localhost;dbname=zvr84m02_golos',
    'username' => 'pamkil',
    'password' => '',
    'charset' => 'utf8',
],
```
## 1.3 Настроить линк старых файлов к новой директории для перемещения их (только для импорта)
```bash
ln -ls ../../../files/all /home/web/golosmarket/runtime/oldFiles
```
## 2 Запустить:
### 2.1 Запустить инициализацию Yii2
```bash
php init        (develop)
```
### 2.1 Запустить миграции
```bash
./yii migrate
```
### 2.2 Запустить импорт: (только для импорта)
```bash
./yii import/index 
./yii import/admin 
```
### 2.3 Запустить заполнение тестовыми данными: (только для локальной разработки)
```bash
./yii init/seeds 
```

## 3 Проверить работоспособность.
## 4 Дать/Проверить права в админку
```bash
./yii init/admin admin@test.ru
```
## 6 Пометить все
## 7 Добавить в крон задачи (если необходимо чтоб пересчитывались показатели и рассылались уведомления, почта и смс)
```
# m h  dom mon dow   command

* * * * * /usr/bin/php /app/golosmarket/yii calculate
* * * * * /usr/bin/php /app/golosmarket/yii calculate/email
* * * * * /usr/bin/php /app/golosmarket/yii calculate/notify
* * * * * /usr/bin/php /app/golosmarket/yii calculate/sms
```
## 6 После проверок
В настройках `common/config/params-local.php` поменять на `true` (для production сервера)
```php
 'sendRealEmail' => true, //Если true то будет осуществлятся рассылка email, sms, notifications
```
## 7 Для обновлений сборки js и css необходимо установить nodejs (nvm - легче всего) 14 версии.
```bash
cd ./frontend/appjs && npm i && npm run build
```
после этих манипуляций обновляться файлы в `./frontend/web/static`


# Настройки webserver Nginx
Пример находится в папке `nginx/golosmarket.conf`
Прописать в `/etc/hosts`
```
127.0.0.1 golosmarket.local;
```
Либо при желании можно указать свой (любой) и заменить в `golosmarket.conf` `servername` на соответствующий.

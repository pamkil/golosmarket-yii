<?php
/** @var \common\models\access\User $user */
$user = Yii::$app->user->identity;
$userIcon = $user->photo ? $user->photo->getUrl() : $directoryAsset . '/img/user2-160x160.jpg';;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $userIcon; ?>" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= $user->firstname; ?> <?= $user->lastname; ?></p>
            </div>
        </div>

        <!-- search form -->
        <!--        <form action="#" method="get" class="sidebar-form">-->
        <!--            <div class="input-group">-->
        <!--                <input type="text" name="q" class="form-control" placeholder="Search..."/>-->
        <!--              <span class="input-group-btn">-->
        <!--                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>-->
        <!--                </button>-->
        <!--              </span>-->
        <!--            </div>-->
        <!--        </form>-->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Меню управления', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Пользователи',
                        'icon' => 'users',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Клиенты', 'icon' => 'users', 'url' => ['/user-client']],
                            ['label' => 'Голоса', 'icon' => 'users', 'url' => ['/user-announcer']],
                            ['label' => 'Чаты', 'icon' => 'users', 'url' => ['/chat']],
                            ['label' => 'Счета', 'icon' => 'users', 'url' => ['/bill']],
                            ['label' => 'Вывод средств', 'icon' => 'users', 'url' => ['/pay']],
                            ['label' => 'Долг по комиссии', 'icon' => 'users', 'url' => ['/commission']],
                        ],
                    ],

                    [
                        'label' => 'Контент пользователей',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Отзывы о пользователе', 'icon' => 'newspaper-o', 'url' => ['/review']],
                            ['label' => 'Отзывы о Голосмаркет', 'icon' => 'newspaper-o', 'url' => ['/review-service']],
                            ['label' => 'Обратная связь', 'icon' => 'newspaper-o', 'url' => ['/feedback']],
                        ]
                    ],

                    [
                        'label' => 'Контент сервиса',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Новости', 'icon' => 'newspaper-o', 'url' => ['/content-news']],
                            ['label' => 'FAQ', 'icon' => 'newspaper-o', 'url' => ['/content-faq']],
                            ['label' => 'Текст на страницах', 'icon' => 'newspaper-o', 'url' => ['/content-helper']],
                            ['label' => 'Меню', 'icon' => 'newspaper-o', 'url' => ['/content-menu']],
                            ['label' => 'Страницы', 'icon' => 'newspaper-o', 'url' => ['/content-page']],
//                            ['label' => 'Вопросы', 'icon' => 'newspaper-o', 'url' => ['/content-question']],
//                            ['label' => 'Ответы', 'icon' => 'newspaper-o', 'url' => ['/content-answer']],
                            ['label' => 'Главное изображение', 'icon' => 'newspaper-o', 'url' => ['/content-banner']],
                            ['label' => 'Подборки', 'icon' => 'newspaper-o', 'url' => ['/content-collection']],
                            ['label' => 'Письмо для оплаты комиссии', 'icon' => 'newspaper-o', 'url' => ['/content-commission-mail']],
                        ]
                    ],

                    [
                        'label' => 'Фильтры дикторов',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Тембр', 'icon' => 'users', 'url' => ['/user-timber']],
                            ['label' => 'Таги', 'icon' => 'users', 'url' => ['/user-tag']],
                            ['label' => 'Презентации', 'icon' => 'users', 'url' => ['/user-presentation']],
                            ['label' => 'Языки', 'icon' => 'users', 'url' => ['/user-language']],
                            ['label' => 'Пол', 'icon' => 'users', 'url' => ['/user-gender']],
                            ['label' => 'Возраст', 'icon' => 'users', 'url' => ['/user-age']],
                        ],
                    ],

                    ['label' => 'План', 'icon' => 'cog', 'url' => ['admin-plan/index']],
                    [
                        'label' => 'Настройки',
                        'icon' => 'cog',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Настройки', 'icon' => 'cog', 'url' => ['setting/index']],
                            ['label' => 'Редирект', 'icon' => 'cog', 'url' => ['redirect/index']],
                            ['label' => 'Robots.txt', 'icon' => 'cog', 'url' => ['seo/robots']],
                        ],
                    ],




                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
//                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
//                    [
//                        'label' => 'Some tools',
//                        'icon' => 'share',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
//                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
//                            [
//                                'label' => 'Level One',
//                                'icon' => 'circle-o',
//                                'url' => '#',
//                                'items' => [
//                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
//                                    [
//                                        'label' => 'Level Two',
//                                        'icon' => 'circle-o',
//                                        'url' => '#',
//                                        'items' => [
//                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                        ],
//                                    ],
//                                ],
//                            ],
//                        ],
//                    ],
                ],
            ]
        ) ?>

    </section>

</aside>

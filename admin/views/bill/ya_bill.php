<h2>Юмани аттрибуты</h2>
<?= \yii\widgets\DetailView::widget([
    'model' => $model,
    'attributes' => [
        'ya_notification_type',
        'ya_operation_id',
        'ya_amount:currency',
        'ya_withdraw_amount:currency',
        'ya_currency',
        'ya_datetime:datetime',
        'ya_label',
        'ya_sha1_hash',
        'ya_codepro:boolean',
        'ya_test_notification:boolean',
        'ya_unaccepted:boolean',
        'ya_success:boolean',
        'created_at:datetime', // creation date formatted as datetime
    ],
]);

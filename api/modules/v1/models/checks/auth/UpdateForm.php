<?php

namespace api\modules\v1\models\checks\auth;

/**
 * @OA\Schema(
 *     schema="AuthUpdateForm",
 *     required={"username", "password", "email"},
 *     title="Форма создания пользователя"
 * )
 */
class UpdateForm extends CreateForm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $parent = parent::rules();
        unset($parent['password']);

        return $parent;
    }
}

<?php

namespace api\modules\v1\models\records\user;

/**
 * @OA\Schema(title="Уведомление")
 */
class Notification extends \common\models\user\Notification
{
    public $opponent;
    /**
     * @OA\Property(property="id", type="integer", description="ИД")
     * @OA\Property(property="announcer_id", type="integer", description="ИД диктора")
     * @OA\Property(property="client_id", type="integer", description="ИД клиента")
     * @OA\Property(property="sum", type="integer", description="Сумма счета")
     * @OA\Property(property="num", type="string", description="Номер счета")
     * @OA\Property(property="created_at", type="boolean", description="Время создания")
     * @OA\Property(property="updated_at", type="boolean", description="Время обновления")
     *
     * @return array|false
     */
    public function fields()
    {
        $results = [
            'id',
            'user_id',
            'other_id' => 'opponentId',
            'type' => 'typeName',
            'status' => 'statusName',
            'text',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
        ];

        if ($this->type === self::TYPE_REVIEW_SEND) {
            $results['opponent'] = 'opponent';
        }

        return $results;
    }

    public function getStatusName()
    {
        return static::nameStatuses()[$this->status] ?? 'unknown';
    }

    public function getTypeName()
    {
        return static::nameTypes()[$this->type] ?? 'unknown';
    }

    public function getOpponentId()
    {
        if ($this->type === self::TYPE_REVIEW) {
            return $this->user_id;
        }

        if ($this->type === self::TYPE_BILL) {
            $announcerId = Bill::find()
                ->where(['id' => $this->other_id])
                ->select('announcer_id')
                ->scalar();

            return $announcerId ? (int)$announcerId : null;
        }

        return $this->other_id;
    }
}
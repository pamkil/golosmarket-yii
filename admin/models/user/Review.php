<?php

namespace admin\models\user;

use common\models\access\User;
use common\models\Model;
use common\service\Mail;

/**
 * Отзыв о пользователе
 *
 * Class Review
 * @package common\models\user
 * @property int $id
 * @property int $announcer_id [int(11)]
 * @property int $client_id [int(11)]
 * @property int $bill_id [int(11)]
 * @property int $type_writer [smallint(6)]
 * @property bool $is_public [tinyint(1)]
 * @property bool $rating [tinyint(3)]
 * @property string $text
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property int $deleted_at [timestamp]
 * @property User $client
 * @property User $announcer
 */
class Review extends \common\models\user\Review
{
    public function getAnnouncer()
    {
        return $this->hasOne(User::class, ['id' => 'announcer_id'])->where([]);
    }

    public function getClient()
    {
        return $this->hasOne(User::class, ['id' => 'client_id'])->where([]);
    }
}

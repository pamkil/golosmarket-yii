<?php

namespace admin\models\setting;

use common\models\site\Setting;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SettingSearch extends Model
{
    public $code;
    public $value;
    public $description;

    public function rules()
    {
        return [
            [
                ['code', 'value', 'description'],
                'string'
            ],
        ];
    }

    public function search($params)
    {
        $query = Setting::find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'code',
                        'value',
                        'description',
                    ]
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $dataProvider->sort->defaultOrder = ['code' => SORT_ASC,];

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'AND',
                ['like', 'code', $this->code],
                ['like', 'value', $this->value],
                ['like', 'description', $this->description],
            ]
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}
<?php

use yii\db\Migration;

/**
 * Class m200813_163925_add_position_faq
 */
class m200813_163925_add_position_faq extends Migration
{
    private $faq = '{{%content_faq}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->faq, 'position', $this->smallInteger()->after('active'));
        $this->createIndex('IX-content_faq-active-position', $this->faq, ['active', 'position']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('IX-content_faq-active-position', $this->faq);
        $this->dropColumn($this->faq, 'position');
    }
}

<?php

namespace common\models\content;

use Carbon\Carbon;
use common\models\Model;
use common\service\Notification;

/**
 * Автор публикаций
 *
 * Class News
 * @package common\models\content
 *
 * @property int $id
 * @property string $title
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property int $deleted_at [timestamp]
 * @property bool $active [tinyint(1)]
 * @property string $date [datetime]
 * @property string $description [varchar(255)]
 * @property string $h1 [varchar(255)]
 * @property string $keywords [varchar(255)]
 * @property string $body
 */
class News extends Model
{
    public static function tableName()
    {
        return '{{%content_news}}';
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }

    public function rules()
    {
        return [
            [
                ['title', 'date', 'description', 'h1', 'keywords', 'body'],
                'string'
            ],
            [
                ['active',],
                'boolean'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Активность',
            'date' => 'Дата',
            'title' => 'SOE Заголовок страницы',
            'description' => 'SEO description',
            'h1' => 'Заголовк H1',
            'keywords' => 'SEO keywords',
            'body' => 'Текст',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!empty($this->date)) {
                $this->date = Carbon::parse($this->date)->toDateString();
            }

            return true;
        }

        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            Notification::addNewsNotify(static::getDb(), $this->id);
        }
    }
}
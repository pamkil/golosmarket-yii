<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(__DIR__, 2) . '/frontend');
Yii::setAlias('@api', dirname(__DIR__, 2) . '/api');
Yii::setAlias('@console', dirname(__DIR__, 2) . '/console');
Yii::setAlias('@uploadPath', dirname(__DIR__, 2) . '/runtime/upload');
Yii::setAlias('@runtimeRoot', dirname(__DIR__, 2) . '/runtime');

Yii::$container->set(\yii\data\Pagination::class, \common\data\Pagination::class);

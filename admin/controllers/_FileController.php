<?

namespace admin\controllers;

use Yii;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;
use common\models\settings\File;
use common\models\settings\FileSearch;
use yii\web\UploadedFile;

class FileController extends Controller
{
    public $defaultAction = 'show';

    public function actionIndex()
    {
        if (!Yii::$app->user->can('viewFiles')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $searchModel = new FileSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список файлов';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionMce()
    {
        if (!Yii::$app->user->can('viewFiles')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $query = File::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10
            ]
        ]);

        return $this->renderAjax('mce', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        if (!Yii::$app->user->can('updateFiles')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $model = new File;

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->get('file') && $model->upload()) {
                Yii::$app->session->addFlash('success', 'Успешно создано');

                $model->updateThumbs($post['Thumbs']['profile'] ?? []);

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Создание файла';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список файлов',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('_form', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('viewFiles')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var File $model */

        $model = File::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            $model->load($post);
            $model->get('file');
            $model->upload();

            $model->updateThumbs($post['Thumbs']['profile'] ?? []);

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }

            return $this->redirect([
                'update',
                'id' => $model->id
            ]);
        }

        $this->view->title = 'Изменение файла "' . $model->name . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список файлов',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->name;

        return $this->render('_form', [
            'model' => $model
        ]);
    }

    public function actionDuplicate($id)
    {
        if (!Yii::$app->user->can('updateFiles')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var File $model */

        $model = File::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        /* @var File $newModel */

        $newModel = $model->duplicate();

        if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/update') !== false) {
            return $this->redirect([
                'update',
                'id' => $newModel->id
            ]);
        } else {
            return $this->redirect([
                'index'
            ]);
        }
    }
    
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('updateFiles')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var File $model */

        $model = File::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        if (strpos(Yii::$app->request->referrer, Yii::$app->request->hostInfo . Url::to(['file/index'])) !== false) {
            return $this->redirect([
                'index'
            ]);
        } else {
            return Json::encode([
                'ok'
            ]);
        }
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUploadThumb($id)
    {
        $model = File::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if (Yii::$app->request->isPost) {
            $file = UploadedFile::getInstance($model, 'thumbnail[0]');

            if ($file && $file->tempName && $model->uploadThumb($file)) {
                return Json::encode([
                    'ok'
                ]);
            } else {
                return Json::encode([
                    'error' => Yii::t('admin', 'Размер изображения файла preview не соответсвует не одному из профилей файла')
                ]);
            }
        }

        return Json::encode([
            'error' => Yii::t('admin', 'Empty POST response')
        ]);
    }
}
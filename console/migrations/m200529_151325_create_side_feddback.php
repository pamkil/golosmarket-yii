<?php

use yii\db\Migration;

/**
 * Class m200529_151325_create_side_feddback
 */
class m200529_151325_create_side_feddback extends Migration
{
    private $feedback = '{{%site_feedback}}';
    private $user = '{{%user}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->feedback, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->null(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'email' => $this->string(),
            'phone' => $this->string(20),
            'text' => $this->text()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'IX-site_feedback-user_id',
            $this->feedback,
            'user_id'
        );
        $this->addForeignKey(
            'FK-site_feedback-user_id',
            $this->feedback,
            'user_id',
            $this->user,
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->feedback);
    }
}

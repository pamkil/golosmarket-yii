<?
namespace admin\controllers;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use common\models\content\Banner;
use common\models\content\BannerSearch;
use common\models\settings\Site;

class BannerController extends SecurityController
{
    public function actionIndex()
    {
        if (!Yii::$app->user->can('viewBanners')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $searchModel = new BannerSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список баннеров';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionCreate()
    {
        if (!Yii::$app->user->can('updateBanners')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $model = new Banner;

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && Model::loadMultiple($model->getVariationModels(), $post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно создано');

                $model->loadImage('image');

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Создание баннера';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список баннеров',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $model->active = 1;
        $model->date_start = date('d.m.Y');

        $sites = Site::find()
            ->where(['active' => 1])
            ->all();
        $sites = ArrayHelper::map($sites, 'id', 'name');

        return $this->render('_form', [
            'sites' => $sites,
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('updateBanners')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Banner $model */

        $model = Banner::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && Model::loadMultiple($model->getVariationModels(), $post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');
            }

            $model->loadImage('image');

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }

            if (isset($post['fileId']) && Yii::$app->request->isAjax && $returnEr = $model->getErrorAction()) {
                return $returnEr;
            } else {
                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }
        }

        $this->view->title = 'Изменение баннера "' . $model->name . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список баннеров',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->name;

        if (isset($model->date_start)) {
            $model->date_start = Yii::$app->formatter->asDate($model->date_start);
        }
        if (isset($model->date_end)) {
            $model->date_end = Yii::$app->formatter->asDate($model->date_end);
        }

        $sites = Site::find()
            ->where(['active' => 1])
            ->all();
        $sites = ArrayHelper::map($sites, 'id', 'name');

        return $this->render('_form', [
            'sites' => $sites,
            'model' => $model
        ]);
    }

    public function actionDuplicate($id)
    {
        if (!Yii::$app->user->can('updateBanners')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Banner $model */

        $model = Banner::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        /* @var Banner $newModel */

        $newModel = $model->duplicate();

        if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/update') !== false) {
            return $this->redirect([
                'update',
                'id' => $newModel->id
            ]);
        } else {
            return $this->redirect([
                'index'
            ]);
        }
    }
    
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('updateBanners')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Banner $model */

        $model = Banner::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect([
            'index'
        ]);
    }
}
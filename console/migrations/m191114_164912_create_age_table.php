<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_announcer_age}}`.
 */
class m191114_164912_create_age_table extends Migration
{
    private $age = '{{%user_announcer_age}}';
    private $announcer2age = '{{%user_announcer_2_age}}';
    private $announcer = '{{%user_announcer}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->age, [
            'id' => $this->primaryKey(),
            'active' => $this->boolean()->notNull()->defaultValue(true),
            'position' => $this->integer(),
            'code' => $this->string(25),
            'name' => $this->string(),
            'menu_title' => $this->string(),
        ], $tableOptions);
        $this->addCommentOnTable($this->age, 'Возраст голоса');

        $this->createTable($this->announcer2age, [
            'announcer_id' => $this->integer(),
            'age_id' => $this->integer(),
            'PRIMARY KEY (announcer_id, age_id)'
        ], $tableOptions);

        $this->createIndex(
            'IX-user_announcer_2_age-announcer_id',
            $this->announcer2age,
            'announcer_id'
        );
        $this->addForeignKey(
            'FK-user_announcer_2_age-announcer_id',
            $this->announcer2age,
            'announcer_id',
            $this->announcer,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('IX-user_announcer_2_age-age_id', $this->announcer2age, 'age_id');
        $this->addForeignKey(
            'FK-user_announcer_2_age-age_id',
            $this->announcer2age,
            'age_id',
            $this->age,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addData();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->announcer2age);
        $this->dropTable($this->age);
    }

    private function addData()
    {
        $this->batchInsert(
            $this->age,
            ['name', 'menu_title', 'code', 'position'],
            [
                ['Детский', 'Детский голос', 'children', 1],
                ['Подростковый', 'Подростковый голос', 'teenage', 2],
                ['Молодой', 'Молодой голос', 'young', 3],
                ['Средний', 'Средний голос', 'average', 4],
                ['Зрелый', 'Зрелый голос', 'mature', 5],
                ['Пожилой', 'Пожилой голос', 'elderly', 6],
            ]
        );
    }
}

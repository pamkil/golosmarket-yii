<?php

namespace api\modules\v1\models\records\user;

use api\modules\v1\models\records\auth\User;
use Carbon\Carbon;
use common\models\site\Setting;

/**
 * @OA\Schema(title="Счет")
 */
class Bill extends \common\models\user\Bill
{
    /**
     * @param $announcerId
     * @param $clientId
     *
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getNew(int $announcerId, int $clientId): ?Bill
    {
        return Bill::find()
            ->where([
                'AND',
                ['announcer_id' => $announcerId],
                ['client_id' => $clientId],
                ['is_canceled' => false],
                ['status' => Bill::STATUS_NEW],
            ])
            ->one();
    }

    public static function getLastPaid(int $announcerId, int $clientId, int $subHours = 48): ?Bill
    {
        return Bill::find()
            ->where([
                'AND',
                ['announcer_id' => $announcerId],
                ['client_id' => $clientId],
                ['is_canceled' => false],
                ['status' => [Bill::STATUS_PAID_CLIENT]],
                ['>=', 'updated_at', Carbon::now()->subHours($subHours)->toDateTimeString()],
            ])
            ->one();
    }

    /**
     * @OA\Property(property="id", type="integer", description="ИД")
     * @OA\Property(property="announcer_id", type="integer", description="ИД диктора")
     * @OA\Property(property="client_id", type="integer", description="ИД клиента")
     * @OA\Property(property="sum", type="integer", description="Сумма счета")
     * @OA\Property(property="num", type="string", description="Номер счета")
     * @OA\Property(property="created_at", type="boolean", description="Время создания")
     * @OA\Property(property="updated_at", type="boolean", description="Время обновления")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'announcer_id',
            'client_id',
            'sum' => 'newSum',
            'status',
            'card_number',
            'num',
            'account',
            'isPaid' => 'isPaid',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
        ];
    }

    public function getNewSum()
    {
        return number_format($this->sum);
    }

    public function getNum()
    {
        return self::genNum($this->announcer_id, $this->id);
    }

    public function getAccount()
    {
        return Setting::getSetting(Setting::YA_MONEY);
    }

    public function getIsPaid()
    {
        return $this->is_payed;
    }

    public function getAnnouncer()
    {
        return $this->hasOne(User::class, ['id' => 'announcer_id'])->where([]);
    }

    public function getClient()
    {
        return $this->hasOne(User::class, ['id' => 'client_id'])->where([]);
    }

    public static function genNum($announcerId, $billId)
    {
        return "gm-$announcerId-$billId";
    }
}
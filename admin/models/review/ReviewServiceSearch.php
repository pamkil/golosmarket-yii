<?php

namespace admin\models\review;

use Carbon\Carbon;
use common\models\site\Feedback;
use common\models\site\Review;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ReviewServiceSearch extends Model
{
    public $user;
    public $is_public;
    public $show_quantity;
    public $rating;
    public $text;
    public $date_at;
    public $date_to;

    public function rules()
    {
        return [
            [
                ['show_quantity', 'rating'],
                'integer'
            ],
            [
                ['text'],
                'string'
            ],
            [
                ['is_public'],
                'boolean'
            ],
            [
                ['date_at'],
                'filter',
                'filter' => function ($val) {
                    if (empty($val)) {
                        return '';
                    }

                    return Carbon::parse($val)->toDateTimeString();
                }
            ],
            [
                ['date_to'],
                'filter',
                'filter' => function ($val) {
                    if (empty($val)) {
                        return '';
                    }

                    return Carbon::parse($val)->endOfDay()->toDateTimeString();
                }
            ],
        ];
    }

    public function search($params)
    {
        $query = Review::find()
            ->alias('review')
            ->joinWith(['user user']);
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'review.id',
                        'review.is_public',
                        'review.show_quantity',
                        'review.rating',
                        'review.text',
                        'review.created_at',
                    ],
                    'defaultOrder' => [
                        'review.created_at' => SORT_DESC
                    ],
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->user)) {
            $query->andWhere(
                [
                    'OR',
                    ['like', 'user.firstname', $this->user],
                    ['like', 'user.lastname', $this->user],
                ]
            );
        }

        $query->andFilterWhere(
            [
                'AND',
                ['review.is_public' => $this->is_public],
                ['review.show_quantity' => $this->show_quantity],
                ['review.rating' => $this->rating],
                ['review.text' => $this->text],
                ['>=', 'review.created_at', $this->date_at],
                ['<=', 'review.created_at', $this->date_to],
            ]
        );

        if (!empty($this->date_at)) {
            $this->date_at = Carbon::parse($this->date_at)->format('d.m.Y');
        }
        if (!empty($this->date_to)) {
            $this->date_to = Carbon::parse($this->date_to)->format('d.m.Y');
        }

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}

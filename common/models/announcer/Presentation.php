<?php

namespace common\models\announcer;

use common\models\access\User;
use common\models\Model;

/**
 * Презентация
 *
 * @OA\Schema(title="Презентация")
 *
 * Class Presentation
 * @package common\models\announcer
 *
 * @property int $id
 * @property string $name
 * @property User[] $announcers
 * @property bool $active [tinyint(1)]
 * @property int $position [int(11)]
 * @property string $code [varchar(25)]
 * @property string $menu_title [varchar(255)]
 * @property bool $use_default [tinyint(1)]
 */
class Presentation extends Model
{
    public static function tableName()
    {
        return '{{%user_announcer_presentations}}';
    }

    public function behaviors()
    {
        $return = parent::behaviors();

        unset($return['timestamp']);

        return $return;
    }

    public function rules()
    {
        return [
            [
                ['name', 'code'],
                'unique'
            ],
            [
                ['name', 'menu_title', 'code'],
                'string'
            ],
            [
                ['position'],
                'number'
            ],
            [
                ['active', 'use_default'],
                'boolean'
            ],
            [
                ['code', 'name', 'menu_title'],
                'required'
            ],
            [
                ['use_default'],
                'validateUserDefault'
            ],
        ];
    }

    public function validateUserDefault($attribute, $params)
    {
        if (!$this->hasErrors())
        {
            $id = $this->id;

            \Yii::$app->db->createCommand()
                ->update($this->tableName(), ['use_default' => false], ['<>', 'id', $id])
                ->execute();

            return true;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'active' => 'Активность',
        ];
    }

    public function getAnnouncers()
    {
        return $this->hasMany(Announcer::class, ['id' => 'announcer_id'])
            ->viaTable('{{%user_announcer_2_presentations}}', ['presentation_id' => 'id']);
    }
}

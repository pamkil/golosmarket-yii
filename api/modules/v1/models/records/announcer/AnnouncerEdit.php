<?php

namespace api\modules\v1\models\records\announcer;

use api\modules\v1\models\records\auth\User;
use api\modules\v1\models\records\file\File;

/**
 * @OA\Schema(required={"name"}, title="Диктор")
 */
class AnnouncerEdit extends \common\models\announcer\Announcer
{
    /**
     * @OA\Property(property="id", type="integer", description="ИД диктора")
     * @OA\Property(property="info", type="string", description="URL фото диктора")
     * @OA\Property(property="equipment", type="string", description="Имя диктора")
     * @OA\Property(property="cost", type="integer", description="Стоимость до 30 сек.")
     * @OA\Property(property="cost_a4", type="integer", description="Стоимость Начитки страницы А4")
     * @OA\Property(property="cost_vocal", type="integer", description="Стоимость вокала")
     * @OA\Property(property="cost_parody", type="integer", description="Стоимость пародии")
     * @OA\Property(property="cost_sound_mount", type="integer", description="Стоимость монтирования звука")
     * @OA\Property(property="cost_audio_adv", type="integer", description="Стоимость сценария для аудиорекламы")
     * @OA\Property(property="discount", type="string", description="Имя диктора")
     * @OA\Property(property="script", type="boolean", description="Сценарий")
     * @OA\Property(property="sound_by_key", type="boolean", description="Звук под ключ")
     * @OA\Property(property="cash_yamoney", type="string", description="cash_yamoney")
     * @OA\Property(property="tags", type="array", description="tags",
     *      @OA\Items(type="object", description="Tags", ref="#/components/schemas/Tag")
     * )
     * @OA\Property(property="photos", type="array", description="список рашзрешений у пользователя",
     *      @OA\Items(type="object", description="Photos", ref="#/components/schemas/File")
     * )
     * @OA\Property(property="sounds", type="array", description="список рашзрешений у пользователя",
     *      @OA\Items(type="object", description="Sounds", ref="#/components/schemas/RecordsAnnouncerSound")
     * )
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'info' => 'info',
            'equipment' => 'equipment',
            'cost',
            'cost_a4',
            'cost_vocal',
            'cost_parody',
            'cost_sound_mount',
            'cost_audio_adv',
            'discount',
            'cash_yamoney',
            'script',
            'sound_by_key',

            'photos',
            'sounds' => 'soundOut',
            'presentations' => function () {
                return $this->getOptions('presentations');
            },
            'tags'
        ];
    }

    public function getSoundOut()
    {
        $values = $this->sounds;

        return array_map(
            function ($item) {
                $item = $item->toArray();
                if (!empty($item['presentation'])) {
                    $item['presentation_id'] = [
                        'id' => $item['presentation']['code'],
                        'name' => $item['presentation']['name'],
                    ];
                }
                unset($item['presentation']);

                if (!empty($item['age'])) {
                    $item['age_id'] = [
                        'id' => $item['age']['code'],
                        'name' => $item['age']['name'],
                    ];
                }
                unset($item['age']);

                if (!empty($item['lang_code'])) {
                    $item['lang_id'] = [
                        'id' => $item['lang_code'],
                        'name' => $item['language'],
                    ];
                }
                unset($item['language']);
                unset($item['lang_code']);

                if (!empty($item['gender'])) {
                    $item['gender_id'] = [
                        'id' => $item['gender']['code'],
                        'name' => $item['gender']['name'],
                    ];
                }
                unset($item['gender']);

                return $item;
            },
            $values
        );
    }

    public function getOptions($name)
    {
        $values = $this->{$name};
        return array_map(
            function ($item) {
                return [
                    'id' => $item['code'],
                    'name' => $item['name'],
                ];
            },
            $values
        );
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getPhotos()
    {
        return $this->hasMany(File::class, ['id' => 'photo_id'])
            ->viaTable('{{%user_announcer_2_photos}}', ['announcer_id' => 'id']);
    }

    public function getSounds()
    {
        return $this->hasMany(AnnouncerSound::class, ['announcer_id' => 'id']);
    }
}

<?php
//https://ionicons.com/v4/usage/
//https://ionicons.com/v4/
?>
<div class="info-box">
    <span class="info-box-icon bg-aqua"><i class="icon ion-md-shuffle"></i></span>
    <div class="info-box-content">
        <span class="info-box-text">Средний чек за прошлый месяц</span>
        <span class="info-box-number"><?= $cost; ?></span>
    </div>
</div>
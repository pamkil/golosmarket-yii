<?php

use yii\db\Migration;

/**
 * Class m210614_070555_create_content_page_2_filter
 */
class m210614_070555_create_content_page_2_filter extends Migration
{
    public $table = '{{%content_page_2_filter}}';
    public $page = '{{%content_pages}}';
    public $gender = '{{%user_announcer_genders}}';
    public $age = '{{%user_announcer_age}}';
    public $presentation = '{{%user_announcer_presentations}}';
    public $language = '{{%user_announcer_languages}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            $this->table,
            [
                'id' => $this->primaryKey(),
                'page_id' => $this->integer()->notNull(),
                'gender_id' => $this->integer()->null(),
                'age_id' => $this->integer()->null(),
                'presentation_id' => $this->integer()->null(),
                'language_id' => $this->integer()->null(),
                'cost' => $this->integer()->null()->defaultValue(0),
                'cost_a4' => $this->integer()->null()->defaultValue(0),
                'sound_by_key' => $this->boolean()->null(),
                'script' => $this->boolean()->null(),
                'photo' => $this->boolean()->null(),
                'online' => $this->boolean()->null(),
                'created_at' => $this->dateTime()->null(),
                'updated_at' => $this->dateTime()->null(),
                'INDEX `IX-content_page_2_filter-page_id` (page_id)',
            ],
            $tableOptions
        );

        $this->addForeignKey(
            'FK-content_page_2_filter-page_id',
            $this->table,
            'page_id',
            $this->page,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK-content_page_2_filter-gender_id',
            $this->table,
            'gender_id',
            $this->gender,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK-content_page_2_filter-age_id',
            $this->table,
            'age_id',
            $this->age,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK-content_page_2_filter-presentation_id',
            $this->table,
            'presentation_id',
            $this->presentation,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK-content_page_2_filter-language_id',
            $this->table,
            'language_id',
            $this->language,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}

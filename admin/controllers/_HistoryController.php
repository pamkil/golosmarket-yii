<?
namespace admin\controllers;

use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use common\models\settings\History;
use common\models\settings\HistorySearch;

class HistoryController extends SecurityController
{
    public function actionIndex()
    {
        if (!Yii::$app->user->can('viewHistory')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $searchModel = new HistorySearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'История изменений';
        $this->view->params['breadcrumbs'] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionView($id)
    {
        if (!Yii::$app->user->can('viewHistory')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $model = History::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $this->view->title = 'Детальная история #' . $model->id;
        $this->view->params['breadcrumbs'][] = [
            'label' => 'История изменений',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('view', [
            'model' => $model
        ]);
    }
}
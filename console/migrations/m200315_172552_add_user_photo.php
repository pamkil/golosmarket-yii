<?php

use yii\db\Migration;

/**
 * Class m200315_172552_add_user_photo
 */
class m200315_172552_add_user_photo extends Migration
{
    private $table = '{{%user}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'photo_id', $this->integer()->null()->after('city_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'photo_id');
    }
}

import React, { useState } from 'react';
import { ReactComponent as UploadImg } from '../../../images/file-upload.svg';
import telegramImg from '../../../images/telegram.svg';
import classes from 'classnames'

import './Bottom.css';

const Bottom = ({ onChange, onSendFile }) => {
    const [message, setMessage] = useState('');
    const [isDragOver, setIsDragOver] = useState(false);
    const [isUploading, setIsUploading] = useState(false);
    const [hasFocus, setHasFocus] = useState(false);

    const keydownHandler = event => {
        if (event.keyCode === 13 && event.ctrlKey) {
            stopEvent(event);
            sendText();
        }
    };

    const handleChange = event => setMessage(event.target.value);

    const sendText = () => {
        if (message.length) {
            onChange(message);
            setMessage('');
        }
    };

    //region Send File
    const stopEvent = event => {
        event.preventDefault();
        event.stopPropagation();
    }

    const dragHandler = event => {
        stopEvent(event);
    }

    const dragOverHandler = event => {
        stopEvent(event);
        if (isUploading) return false;
        setIsDragOver(true);
    }

    const dragOverEndHandler = event => {
        stopEvent(event);
        if (isUploading) return false;
        setIsDragOver(false);
    }

    const dropHandler = event => {
        stopEvent(event);
        if (isUploading) return false;

        setIsDragOver(false);
        triggerFormSubmit(event.dataTransfer.files);
    }

    const inputOnChangeHandler = event => {
        if (isUploading) return false;
        triggerFormSubmit(event.target.files);
    }

    const inputOnFocusHandler = () => setHasFocus(true);

    const inputOnBlurHandler = () => setHasFocus(false);

    const submitHandler = eventSubmit => stopEvent(eventSubmit);

    const triggerFormSubmit = files => {
        setIsUploading(true);
        try {
            if (files && files.length) {
                onSendFile(files);
            }
        } catch (e) {
        }
        setTimeout(() => {
            setIsUploading(false);
            inputOnBlurHandler();
        }, 300);
    };
    //endregion

    return (
        <div className="chat-bottom">
            <div className="chat-file">
                <form
                    encType="multipart/form-data"
                    className={ classes(
                        'form-box',
                        'has-advanced-upload',
                        {
                            'is-dragover': isDragOver,
                            'is-uploading': isUploading
                        }
                    ) }
                    onSubmit={ submitHandler }
                    onDrag={ dragHandler }
                    onDragStart={ dragHandler }
                    onDragEnd={ dragOverEndHandler }
                    onDragOver={ dragOverHandler }
                    onDragEnter={ dragOverHandler }
                    onDragLeave={ dragOverEndHandler }
                    onDrop={ dropHandler }
                >
                    <textarea
                        cols="30" rows="2"
                        placeholder="Введите ваше сообщение"
                        value={ message }
                        onChange={ handleChange }
                        onKeyDown={ keydownHandler }
                    />
                    <div className="file-upload">
                        <div className="box__input">
                            <label
                                htmlFor="file"
                                onClick={ inputOnFocusHandler }
                                className={ classes({ 'has-focus': hasFocus }) }
                            >
                                <UploadImg />
                            </label>

                            <input
                                id="file"
                                type="file"
                                name="files[]"
                                multiple
                                className={ classes('box__file', { 'has-focus': hasFocus }) }
                                onChange={ inputOnChangeHandler }
                            />
                        </div>
                    </div>
                    <div className="telegram">
                        <button onClick={ sendText }>
                            <img src={ telegramImg } alt="telegramImg" title="Ctr+Enter"/>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Bottom;
function sendActive(that, url) {
    var obj = $(that),
        active = 0;

    if (obj.prop('checked')) {
        active = 1;
    } else {
        active = 0;
    }

    var regex = /\[([a-zA-Z0-9-_]*)\]$/ig,
        res = regex.exec(obj.prop('name'));
    if (!res) {
        res = obj.prop('name');
    } else {
        res = res[1];
    }

    $.ajax({
        url: url,
        type: 'PATCH',
        dataType: 'json',
        data: res + '=' + active
    }).done(function () {
        if (active) {
            toastr.success('Успешно активировано');
        } else {
            toastr.success('Успешно деактивировано');
        }
    }).fail(function () {
        if (active) {
            toastr.error('Ошибка активации');
        } else {
            toastr.error('Ошибка деактивации');
        }
    });
}
<?php

namespace api\modules\swagger\controllers;

use api\modules\v1\models\records\auth\User;
use common\models\access\Token;
use Yii;
use yii\web\Controller;

class UiController extends Controller
{
    public function actionSwagger($id = null)
    {
        Yii::$app->response->format = 'html';
        $this->layout = false;
        if ($id) {
            $user = User::find()->where(['username' => $id])->one();

            if ($user) {
                /** @var Token $tokenModel */
                $tokenModel = $user->getTokens()->orderBy(['expired_at' => SORT_DESC])->one();
            }
        }

        $tokenModel = $tokenModel ?? Token::find()->orderBy(['expired_at' => SORT_DESC])->one();

        return $this->render('swagger', [
            'token' => $tokenModel->token ?? ''
        ]);
    }

    public function actionTest()
    {
        phpinfo();
        die();
    }
}

<?php

namespace common\repositories\contract;

use yii\db\ActiveQuery;

interface ChatRepositoryContract
{
    /**
     * Request to count unread messages
     * @return int
     */
    public function countUnreadMessages(): int;

    /**
     * Create db request to find unread messages in chat
     * @return array
     */
    public function findUnreadToSms(): array;
}
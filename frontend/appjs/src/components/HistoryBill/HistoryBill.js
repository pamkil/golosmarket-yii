import React, { useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from "react-redux";

import { getBills } from "../../data/redux/user";
import Loader from "../Common/Loader/Loader";
import Pagination from "./Pagination/Pagination";
import { ReactComponent as LeftImg } from '../../images/left-arrow.svg'

const History = () => {
    const dispatch = useDispatch();
    const [isInit, isLogin, bills, billsIsLoading, pagination, user, isLoadingUser] = useSelector( (
        {
            app: {
                initialized: isInit
            },
            auth: {
                token: isLogin
            },
            user: {
                bills,
                billsIsLoading,
                pagination,
                user,
                isLoading
            }
        }
    ) => [!!isInit, !!isLogin, bills, billsIsLoading, pagination, user, isLoading ] );
    const isAnnouncer = user && user.isAnnouncer

    useEffect( () => {
        if ( isInit && isLogin ) {
            dispatch( getBills( 1 ) );
        }
        // eslint-disable-next-line
    }, [isLogin, isInit] );

    const setPageHandler = useCallback( (page) => {
        dispatch( getBills( page ) );
    }, [dispatch] );

    if ( !isLogin ) {
        return <div className="non-auth">
            <p>Необходимо <a href="#login">войти</a> или <a href="#registry">зарегистрироваться</a></p>
            <a href="/">Перейти на главную страницу?</a>
        </div>;
    }

    if ( billsIsLoading || isLoadingUser || !user ) {
        return (
            <section className="catalog" key="loader-announcer">
                <Loader/>
            </section>
        );
    }

    return <div className="container">
        <div className="titlepage">
            <div className="icon mobile-no">
                <a href="/">
                    <LeftImg/>
                </a>
            </div>
            <h1>
                История заказов { user.firstname } { user.lastname }
            </h1>
        </div>
        <div className="table-responsive">
            <table style={ { width: '100%' } } className="history-table">
                <thead>
                <tr>
                    <th>№ счета</th>
                    <th> { isAnnouncer ? 'Заказчик' : 'Исполнитель' }</th>
                    <th>Сумма счета</th>
                    {
                        isAnnouncer
                            ? <th>На баланс</th>
                            : ''
                    }
                    <th>Дата выставления счета</th>
                    <th>Статус</th>
                </tr>
                </thead>
                <tbody>
                { bills.map( bill =>
                    <tr key={bill.id}>
                        <td className="center">{ bill.id }</td>
                        <td>{ bill.opponent.name }</td>
                        <td className="right">{ bill.sum }</td>
                        {
                            isAnnouncer
                                ? <td className="right">{  bill.sumBalance }</td>
                                : ''
                        }
                        <td className="right" ><div dangerouslySetInnerHTML={{__html:bill.date}} /></td>
                        <td className="center">{ bill.status } </td>
                    </tr>
                ) }
                </tbody>
            </table>
        </div>
        <Pagination pagination={ pagination } setPage={ setPageHandler }/>
    </div>
}

export default History;

<?php

namespace common\service\chat\contract;

use common\service\chat\dto\CreateMessageDto;

interface ChatServiceContract
{
    public function sendSmsNotifyByUnreadMessages(): bool;

    public function createMessage(CreateMessageDto $createMessageDto);
}
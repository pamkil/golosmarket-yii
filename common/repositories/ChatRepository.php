<?php

namespace common\repositories;

use common\models\user\Chat;
use common\repositories\contract\ChatRepositoryContract;
use yii\db\Expression;
use yii\db\Query;

class ChatRepository implements ChatRepositoryContract
{
    private Chat $model;

    public function __construct()
    {
        $this->model = new Chat();
    }

    /**
     * @inheritDoc
     */
    public function countUnreadMessages(): int
    {
        return (int) $this->model::find()
            ->with(['client', 'announcer'])
            ->where([
                'AND',
                ['is_read' => false],
                ['<', 'created_at', new Expression('SUBTIME(UTC_TIMESTAMP(), '.$this->model::UNREAD_SMS_MINUTES.' * 60)')],
                ['IS NOT', 'deleted_at', new Expression("true")]
            ])->andWhere(['is_write_client' => true])->count();
    }

    /**
     * @inheritDoc
     */
    public function findUnreadToSms(): array
    {
        $chatTable = Chat::tableName();
        $firstMessageQuery = (new Query)->select(['uc1.created_at'])
            ->from(['uc1' => $chatTable])
            ->andWhere(new Expression('uc1.announcer_id = uc.announcer_id'))
            ->andWhere(new Expression('uc1.client_id = uc.client_id'))
            ->andWhere(new Expression('uc1.is_read = false'))
            ->andWhere(new Expression('uc1.is_write_client = true'))
            ->andWhere([
                '<',
                'uc1.created_at',
                new Expression('UTC_TIMESTAMP()')
            ])
            ->andWhere([
                '>',
                'uc1.created_at',
                new Expression("SUBTIME(UTC_TIMESTAMP(), '2:0')")
            ])->andWhere(new Expression('uc1.deleted_at IS NOT TRUE'))
            ->orderBy(['uc1.created_at' => SORT_ASC])
            ->limit(1);
        $lastMessageQuery = (new Query)->select(['uc2.created_at'])
            ->from(['uc2' => $chatTable])
            ->andWhere(new Expression('uc2.announcer_id = uc.announcer_id'))
            ->andWhere(new Expression('uc2.client_id = uc.client_id'))
            ->andWhere(new Expression('uc2.is_read = false'))
            ->andWhere(new Expression('uc2.is_write_client = true'))
            ->andWhere([
                '<',
                'uc2.created_at',
                new Expression('UTC_TIMESTAMP()')
            ])
            ->andWhere([
                '>',
                'uc2.created_at',
                new Expression("SUBTIME(UTC_TIMESTAMP(), '2:0')")
            ])->andWhere(new Expression('uc2.deleted_at IS NOT TRUE'))
            ->orderBy(['uc2.created_at' => SORT_DESC])
            ->limit(1);

        $query = (new Query)->select([
            'uc.id',
            'uc.announcer_id',
            'uc.client_id',
            'uc.is_write_client',
            'uc.is_read',
            'uc.created_at',
            'first_message_time' => $firstMessageQuery,
            'last_message_time' => $lastMessageQuery
        ])->from(['uc' => $chatTable])
            ->andWhere(new Expression('uc.is_read = false'))
            ->andWhere(new Expression('uc.is_write_client = true'))
            ->andWhere([
                '<',
                'uc.created_at',
                new Expression('UTC_TIMESTAMP()')
            ])
            ->andWhere([
                '>',
                'uc.created_at',
                new Expression("SUBTIME(UTC_TIMESTAMP(), '2:0')")
            ])->andWhere(new Expression('uc.deleted_at IS NOT TRUE'))
            ->groupBy(['uc.announcer_id','uc.client_id'])
            ->orderBy('first_message_time');

        return $query->all();
        /*return $this->model::find()
            ->with(['client', 'announcer'])
            ->where([
                'AND',
                ['is_read' => false],
                ['<', 'created_at', new Expression('SUBTIME(NOW(), '.$this->model::UNREAD_SMS_MINUTES.' * 60)')],
                ['IS NOT', 'deleted_at', new Expression("true")]
            ])->all();*/
    }
}
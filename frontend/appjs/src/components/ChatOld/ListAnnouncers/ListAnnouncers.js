import React from 'react';
import AutoSizer from "react-virtualized-auto-sizer";
import { List, InfiniteLoader } from 'react-virtualized';

import AnnouncerItem from "../AnnouncerItem/AnnouncerItem";

const Row = ({ index, isScrolling, isVisible, style, idAnnouncerActive, getAnnouncer, setIdActive }) => {
    const item = getAnnouncer(index, isScrolling, isVisible);

    if (item === false) {
        return <div className="announcer-loaded" style={ style }>Загружается...</div>
    }

    return <AnnouncerItem
        key={ item.id }
        { ...item }
        onChange={ (id) => setIdActive(id) }
        isActive={ idAnnouncerActive === item.opponentId }
        style={ style }
    />
}

const ListAnnouncers = (
    {
        children,
        idAnnouncerActive = null,
        setIdActive,
        totalCount,
        isRowLoaded,
        loadMoreRows,
        getAnnouncer,
        keyItem
    }) => (
    <div className="left-chat-block">
        { children }
        <div className="humans">
            <InfiniteLoader
                isRowLoaded={ isRowLoaded }
                loadMoreRows={ loadMoreRows }
                rowCount={ totalCount }
                key={keyItem}
            >
                { ({ onRowsRendered, registerChild }) => (
                    <AutoSizer defaultHeight={ 81 }>
                        { ({ height, width }) => (
                            <List
                                ref={ registerChild }
                                overscanRowCount={ 5 }
                                width={ width }
                                height={ height }
                                rowHeight={ 81 }
                                onRowsRendered={ onRowsRendered }
                                rowCount={ totalCount }
                                rowRenderer={ props => <Row
                                    key={ keyItem + props.index }
                                    { ...props }
                                    idAnnouncerActive={ idAnnouncerActive }
                                    getAnnouncer={ getAnnouncer }
                                    setIdActive={ setIdActive }
                                /> }
                            />
                        ) }
                    </AutoSizer>
                ) }
            </InfiniteLoader>
        </div>
    </div>
);

export default ListAnnouncers;
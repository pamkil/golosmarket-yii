<?php

use yii\db\Migration;

/**
 * Class m200409_193632_add_cost_announcer
 */
class m200409_193632_add_cost_announcer extends Migration
{
    private $table = '{{%user_announcer}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->table,
            'cost_a4',
            $this->integer()->notNull()->defaultValue(0)->after('cost')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'cost_a4');
    }
}

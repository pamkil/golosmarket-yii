<?php

namespace admin\controllers;

use admin\models\content\PageSearch;
use common\models\content\Page;
use common\models\content\PageFilter;
use common\models\content\PageSimilar;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ContentPageController extends SecurityController
{
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title,
        ];

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]
        );
    }

    public function actionCreate()
    {
        $model = new Page();
        $filter = new PageFilter();
        $similar = new PageSimilar();

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $filter->load($post)) {
                $isValid = $model->validate() && $filter->validate() && $similar->validate();
                if ($isValid) {
                    Yii::$app->db->transaction(
                        static function () use ($model, $filter, $similar) {
                            $model->save();
                            $filter->page_id = $model->id;
                            $filter->save();
                            $similar->page_id = $model->id;
                            $similar->save();
                        }
                    );

                    Yii::$app->session->addFlash('success', 'Успешно создано');

                    return $this->redirect(
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    );
                }
            }

            foreach ($model->getErrorSummary(true) as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Создание';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список',
            'url' => ['index'],
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render(
            '_form',
            [
                'model' => $model,
                'filter' => $filter,
                'similar' => $similar,
            ]
        );
    }

    public function actionUpdate($id)
    {
        /* @var Page $model */
        $model = Page::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException;
        }
        $filter = $model->filter;
        if (!$filter) {
            $filter = new PageFilter(['page_id' => $model->id]);
        }
        $similar = $model->similar;
        if (!$similar) {
            $similar = new PageSimilar(['page_id' => $model->id]);
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $filter->load($post) && $similar->load($post)) {
                $isValid = $model->validate() && $filter->validate() && $similar->validate();
                if ($isValid) {
                    Yii::$app->db->transaction(
                        static function () use ($model, $filter, $similar) {
                            $model->save();
                            $filter->page_id = $model->id;
                            $filter->save();
                            $similar->page_id = $model->id;
                            $similar->save();
                        }
                    );
                    Yii::$app->session->addFlash('success', 'Успешно обновлено');

                    return $this->redirect(
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    );
                }
            }

            foreach ($model->getErrorSummary(true) as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Изменение "' . $model->title . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список',
            'url' => ['index'],
        ];
        $this->view->params['breadcrumbs'][] = $model->title;

        return $this->render(
            '_form',
            [
                'model' => $model,
                'filter' => $filter,
                'similar' => $similar,
            ]
        );
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        /* @var Page $model */
        $model = Page::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException();
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect('index');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m200529_170021_add_user_review
 */
class m200529_170021_add_user_review extends Migration
{
    private $table = '{{%user_review}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->table,
            'type_writer',
            $this->smallInteger()->notNull()->after('client_id')->defaultValue(1)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'type_writer');
    }
}

<?php

namespace common\queries;

use yii\db\ActiveQuery;

class SoftDeleteQuery extends ActiveQuery
{
    public function __construct($modelClass, $deleted = false, $config = [])
    {
        parent::__construct($modelClass, $config);
        $this->notDeleted(!$deleted);
    }

    public function notDeleted($state = true)
    {
        if ($state) {
            [$tableName] = $this->getTableNameAndAlias();
            $this->andWhere(['!=', $tableName . '.status', 0]);
        }

        return $this;
    }
}

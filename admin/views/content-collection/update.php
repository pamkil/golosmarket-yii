<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\content\Collection */

$this->title = 'Изменить подборку: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Подборки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="collection-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

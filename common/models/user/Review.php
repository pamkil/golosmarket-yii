<?php

namespace common\models\user;

use common\models\access\User;
use common\models\Model;

/**
 * Отзыв о пользователе
 *
 * Class Review
 * @package common\models\user
 * @property int $id
 * @property int $announcer_id [int(11)]
 * @property int $client_id [int(11)]
 * @property int $bill_id [int(11)]
 * @property int $type_writer [smallint(6)]
 * @property bool $is_public [tinyint(1)]
 * @property bool $rating [tinyint(3)]
 * @property string $text
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property int $deleted_at [timestamp]
 * @property User $client
 * @property User $announcer
 */
class Review extends Model
{
    public const TYPE_WRITE_CLIENT = 1;
    public const TYPE_WRITE_ANNOUNCER = 2;

    public $isImport = false;

    public static function tableName()
    {
        return '{{%user_review}}';
    }

    public function rules()
    {
        return [
            [
                ['announcer_id', 'client_id', 'bill_id', 'rating', 'type_writer'],
                'integer',
            ],
            [
                ['is_public'],
                'boolean',
            ],
            [
                ['text'],
                'string',
            ],
            [
                ['text'],
                'filter',
                'filter' => function ($value) {
                    if (!empty($value)) {
                        $value = strip_tags($value, '<br>');
                        $value = str_replace('<br>', PHP_EOL, $value);
                        $value = str_replace('<br/>', PHP_EOL, $value);
                    }

                    return $value;
                },
            ],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);
        if ($this->isImport) {
            unset($behaviors['timestamp']);
        }

        return $behaviors;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'announcer_id' => 'Диктор',
            'bill_id' => 'Счет',
            'type_writer' => 'Кто пишет',
            'client_id' => 'Клиент',
            'is_public' => 'Опубликован',
            'text' => 'Текс отзыва',
            'rating' => 'Рейтинг',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function getAnnouncer()
    {
        return $this->hasOne(User::class, ['id' => 'announcer_id']);
    }

    public function getClient()
    {
        return $this->hasOne(User::class, ['id' => 'client_id']);
    }

    public function getBill()
    {
        return $this->hasOne(Bill::class, ['id' => 'bill_id']);
    }

    public static function getTypes()
    {
        return [
            self::TYPE_WRITE_CLIENT => 'Написал клиент',
            self::TYPE_WRITE_ANNOUNCER => 'Написал диктор',
        ];
    }

    public static function getReviews(int $quantity = 2, int $userId = null, bool $isAnnouncer = false)
    {
        return static::find()
            ->where([
                'is_public' => true,
                'type_writer' => $isAnnouncer ? self::TYPE_WRITE_CLIENT : self::TYPE_WRITE_ANNOUNCER
            ])
            ->andFilterWhere([($isAnnouncer ? 'announcer_id' : 'client_id') => $userId])
            ->joinWith([($isAnnouncer ? 'client' : 'announcer')])
//            ->joinWith(['client', 'client.photo', 'announcer', 'announcer.photo'])
            ->andFilterWhere(['user.status' => User::STATUS_ACTIVE])
            ->limit($quantity)
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
    }

    public static function getCount(int $announcerId = null)
    {
        return static::find()
            ->where([
                'is_public' => true,
                'type_writer' => self::TYPE_WRITE_CLIENT
            ])
            ->andFilterWhere(['announcer_id' => $announcerId])
            ->count();
    }

    public static function calculate(int $userId, bool $isAnnouncer = true, bool $setValue = false)
    {
        $field = $isAnnouncer ? 'announcer_id' : 'client_id';
        $type = $isAnnouncer ? self::TYPE_WRITE_CLIENT : self::TYPE_WRITE_ANNOUNCER;
        $query = self::find()
            ->where([
                $field => $userId,
                'is_public' => true,
                'type_writer' => $type
            ]);

        $average = (float)(clone $query)->average('rating');
        $quantityAll = (int)$query->count();

        if ($setValue) {
            User::updateAll(
                [
                    'rating' => (float)$average,
                    'quantity_reviews' => (int)$quantityAll,
                ],
                [
                    'id' => $userId,
                ]
            );
        }

        return [(float)$average, (int)$quantityAll];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (($insert && $this->is_public) || ($this->is_public && $changedAttributes['is_public'])) {
            \common\service\Notification::addReviewNotify($this);
        }
    }
}

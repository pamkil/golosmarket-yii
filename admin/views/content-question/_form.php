<?php

use common\models\content\Question;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var Question $model
 */
?>
<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data' // important
    ]
]) ?>

<div class="box box-primary">
    <div class="box-header with-border text-right">
        <?= Html::submitButton('<i class="fa fa-check"></i>&nbsp;' . ($model->isNewRecord ? 'Создать' : 'Сохранить'), ['class' => 'btn btn-success']); ?>
        <? if (!$model->isNewRecord) { ?>
            <?= Html::a('<i class="fa fa-times"></i>&nbsp;' . 'Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'title' => 'Удалить',
                'data-confirm' => 'Вы уверены что хотите удалить?',
                'data-method' => 'post'
            ]); ?>
        <? } ?>
    </div>
    <div class="box-body">
        <?= $form->field($model, 'active')->checkbox(); ?>
        <?= $form->field($model, 'position'); ?>
        <?= $form->field($model, 'text')->textarea(); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

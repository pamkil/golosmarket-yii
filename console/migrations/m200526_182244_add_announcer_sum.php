<?php

use yii\db\Migration;

/**
 * Class m200526_182244_add_announcer_sum
 */
class m200526_182244_add_announcer_sum extends Migration
{
    private $table = '{{%user_announcer}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->table,
            'sum',
            $this->decimal(9, 2)
                ->after('user_id')->notNull()->defaultValue(0.0)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'sum');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: pamkil
 * Date: 12.02.19
 * Time: 1:04
 */

namespace api\modules\v1\models\queries;

use api\modules\v1\models\records\auth\User;
use yii\db\ActiveQuery;

class UnDeletedQuery extends ActiveQuery
{
    public function unDeleted()
    {
        return $this->andWhere(['deleted_at' => null]);
    }

    /**
     * @param null $db
     * @return array|User[]
     */
    public function all($db = null)
    {
        $this->unDeleted();

        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|null|User
     */
    public function one($db = null)
    {
        $this->unDeleted();

        return parent::one($db);
    }
}
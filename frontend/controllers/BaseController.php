<?php

namespace frontend\controllers;

use common\models\announcer\Age;
use common\models\announcer\AnnouncerSound;
use common\models\announcer\Presentation;
use common\models\site\Redirect;
use common\service\announcer\enum\AnnouncerCostEnum;
use JsonException;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\helpers\Json;

/**
 * Site controller
 */
class BaseController extends Controller
{
    public function runAction($id, $params = [])
    {
        $url = $this->request->url;
        $redirect = Redirect::find()->where(['from' => $url, 'active' => true])->asArray()->one();
        if ($redirect) {
            return $this->redirect($redirect['to']);
        }

        return parent::runAction($id, $params);
    }


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    protected function addSeo($page, string $image = null)
    {
        if (!$page) {
            return;
        }

        $this->addMetaTags(
            $page['title'] . ' | GolosMarket',
            $page['keywords'] ?? '',
            $page['description'] ?? '',
            $image
        );
    }

    protected function addMetaTags(
        string $title,
        string $keywords,
        string $description,
        string $image = null
    ): BaseController {
        $this->view->title = $title;

        $this->view->registerMetaTag(
            [
                'name' => 'keywords',
                'content' => $keywords,
            ],
            'keywords'
        );

        $this->view->registerMetaTag(
            [
                'name' => 'description',
                'content' => $description,
            ],
            'description'
        );

        $this->view->registerMetaTag(
            [
                'property' => 'og:description',
                'content' => $description,
            ],
            'og:description'
        );

        $this->view->registerMetaTag(
            [
                'name' => 'twitter:description',
                'content' => $description,
            ],
            'twitter:description'
        );

        $this->view->registerMetaTag(
            [
                'property' => 'og:title',
                'content' => $this->view->title,
            ],
            'og:title'
        );

        $this->view->registerMetaTag(
            [
                'name' => 'twitter:title',
                'content' => $this->view->title,
            ],
            'twitter:title'
        );

        $this->view->registerMetaTag(
            [
                'property' => 'og:url',
                'content' => Url::current([], true),
            ],
            'og:url'
        );

        if ($image) {
            $this->view->registerMetaTag(
                [
                    'property' => 'og:image',
                    'content' => $image,
                ],
                'og:image'
            );

            $this->view->registerMetaTag(
                [
                    'name' => 'twitter:image',
                    'content' => $image,
                ],
                'twitter:image'
            );

            $this->view->registerMetaTag(
                [
                    'name' => 'twitter:card',
                    'content' => 'summary_large_image',
                ],
                'twitter:card'
            );
        }

        return $this;
    }

    /**
     * @return string json encode filters data
     * @throws JsonException
     */
    protected function getAllFilters(): string
    {
        // TODO есть мысль что это вообще можно переместить в отдельный метод класса AnnouncerSound
        $filters = [
            'presentations' => AnnouncerSound::getExistingPresentations(),
            'age' => AnnouncerSound::getExistingAges(),
            'lang' => AnnouncerSound::getExistingLanguages(),
            'cost' => AnnouncerCostEnum::getFiltersCost()
        ];

        return Json::encode($filters, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }

    protected function getSelectFilers(array $filter = null)
    {
        if (!$filter) {
            return [];
        }

        $fields = [
            'gender',
            'presentation',
            'language',
            'age',
            'cost_by_a4',
            'cost_at',
            'cost_to',
            'sound_by_key',
            'script',
            'photo',
            'online',
        ];

        $result = [];

        foreach ($fields as $field) {
            if ($field === null || !isset($filter[$field]) || $filter[$field] === '0') {
                continue;
            }

            $value = $filter[$field];

            if (is_array($value) && !empty($value['code'])) {
                $value = $value['code'];
            }

            $value = $value === '1' ? true : $value;

            $result[$field] = $value;
        }

        return $result;
    }

    protected function jsonEncode($data): string
    {
        return Json::encode(
            $data,
            (YII_DEBUG ? JSON_PRETTY_PRINT : 0) | JSON_UNESCAPED_UNICODE
        );
    }
}

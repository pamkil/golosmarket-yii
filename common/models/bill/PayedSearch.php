<?php

namespace common\models\bill;

use common\models\access\User;
use common\models\user\Bill;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class PayedSearch extends Model
{
    public $announcer_id;

    public function search()
    {
        $query = User::find()
            ->alias('user')
            ->with('announcer');

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'user.id',
                    ],
                    'defaultOrder' => [
                        'user.id' => SORT_DESC
                    ],
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $queryBill = Bill::find()
            ->select('announcer_id')
            ->andWhere([
                'is_canceled' => false,
                'is_payed' => true,
                'is_send' => false,
            ])
            ->andWhere(['IN', 'status', [Bill::STATUS_PAID_ANNOUNCER, Bill::STATUS_PAID_CLIENT]])
            ->andWhere('card_number IS NOT NULL');

        $query->where(['id' => $queryBill])
            ->with(
                [
                    'bills' => function (\yii\db\ActiveQuery $query) {
                        $query->andWhere([
                            'is_canceled' => false,
                            'is_payed' => true,
                            'is_send' => false
                        ])->andWhere(['IN', 'status', [Bill::STATUS_PAID_ANNOUNCER, Bill::STATUS_PAID_CLIENT]])
                        ->andWhere('card_number IS NOT NULL');
                    }
                ]
            )
            ->andFilterWhere(['user.id' => $this->announcer_id]);

        return $dataProvider;
    }
}
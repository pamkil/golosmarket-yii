<?php

namespace admin\controllers;

use admin\models\setting\RedirectSearch;
use common\models\site\Redirect;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class RedirectController extends SecurityController
{
    public function actionIndex()
    {
        $searchModel = new RedirectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title,
        ];

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]
        );
    }

    public function actionCreate()
    {
        $model = new Redirect();

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post)) {
                $isValid = $model->validate() ;
                if ($isValid) {
                    $model->save();
                    Yii::$app->session->addFlash('success', 'Успешно создано');

                    return $this->redirect(
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    );
                }
            }

            foreach ($model->getErrorSummary(true) as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Создание';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список',
            'url' => ['index'],
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        //        $model->active = 1;

        return $this->render(
            '_form',
            [
                'model' => $model,
            ]
        );
    }

    public function actionUpdate($id)
    {
        /* @var Redirect $model */
        $model = Redirect::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post)) {
                $isValid = $model->validate();
                if ($isValid) {
                    $model->save();
                    Yii::$app->session->addFlash('success', 'Успешно обновлено');

                    return $this->redirect(
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    );
                }
            }

            foreach ($model->getErrorSummary(true) as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = "Изменение {$model->id}";
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список',
            'url' => ['index'],
        ];
        $this->view->params['breadcrumbs'][] = $model->id;

        return $this->render(
            '_form',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        /* @var Redirect $model */
        $model = Redirect::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect('index');
    }
}

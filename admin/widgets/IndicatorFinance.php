<?php

namespace admin\widgets;

use Carbon\Carbon;
use common\models\admin\Plan;
use common\models\user\Bill;
use yii\base\Widget;

class IndicatorFinance extends Widget
{
    public function run()
    {
        parent::run();

        /**
         * Показатели прошлого месяца к этому же периоду прошлого года: +5%/цель + 10%. Не достигнута 😒.
         * Рост показываем за полный прошедший месяц. Например, в августе мы показываем как мы отработали июль.
         * Цель это финансовый показатель (в процентном выражении) по сравнению с таким же периодом прошлого года.
         * Соответственно цель надо как-то редактировать/выставлять.
         */

        $dateLast = Carbon::today()->subMonth()->startOfMonth()->toDateTimeString();
        $dateEndLast = Carbon::today()->subMonth()->endOfMonth()->toDateTimeString();
        $dateYear = Carbon::today()->subMonth()->subYear()->startOfMonth()->toDateTimeString();
        $dateEndYear = Carbon::today()->subMonth()->subYear()->endOfDay()->toDateTimeString();
        $plan = (int)(Plan::find()->select('value')->where(['date_plan' => $dateLast])->scalar() ?? 5);

        $sumLast = Bill::find()
            ->cache(60 * 3)
            ->where(
                [
                    'AND',
                    ['>=', 'updated_at', $dateLast],
                    ['<=', 'updated_at', $dateEndLast],
                    ['status' => Bill::STATUS_PAID_COMMISSION],
                ]
            )
            ->sum('`sum` * `percent` / 100');;

        $sumYear = Bill::find()
            ->cache(60 * 3)
            ->where(
                [
                    'AND',
                    ['>=', 'updated_at', $dateYear],
                    ['<=', 'updated_at', $dateEndYear],
                    ['status' => Bill::STATUS_PAID_COMMISSION],
                ]
            )
            ->sum('`sum` * `percent` / 100');

        $data = [
            'title' => 'Показатели прибыли',
            'value1' => round($sumLast ?? 0, 2),
            'smallValue1' => '(прошлый месяц)',
            'value2' => round($sumYear ?? 0, 2),
            'smallValue2' => '(месяц прошлого года)',
        ];
        $data['upValue'] = $data['value1'] == 0 ? 0 : round(100 - $data['value2'] / $data['value1'] * 100, 2);
        $znak = $plan > 0 ? '+' : '';
        $data['upPlan'] = "Цель на мес. {$znak}{$plan}% " . ($data['upValue'] >= $plan ? 'Достигнута' : "Не достигнута");

        echo $this->render('indicatorFinanceWidgets', $data);
    }
}
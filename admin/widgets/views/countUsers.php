<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-purple-soft">
            <span data-counter="counterup" data-value="<?= /** @var string $value */
            $value ?>"><?= $value ?></span>
                </h3>
                <small>Новых регистраций</small>
            </div>
            <div class="icon">
                <i class="icon-user"></i>
            </div>
        </div>
    </div>
</div>

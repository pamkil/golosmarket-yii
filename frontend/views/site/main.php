<?php

use \common\models\content\PageAccordeon;

?>
<section class="filter-main">
    <div id="promo" class="container">
        <div class="promo-holder">
            <div class="promo-content">
                <div class="promo-title">
                    Находите лучшие голоса для ваших задач
                </div>
                <p>
                    Слушайте демо, пишите в чат, создавайте свою подборку, делитесь
                </p>
            </div>
            <div class="promo-image"><img src="<?= $banner ?? '' ?>" alt="promo"></div>
        </div>

        <div id="filter" class="filter-holder"></div>
    </div>
</section>

<div id="announcer"></div>

<section class="quote">
    <div class="container">
        <?php if (!empty($page)): ?>
            <div class="content">
                <?php if (!empty($page['title'])) : ?>
                    <h1><?= $page['title']; ?></h1>
                <?php endif; ?>

                <?php if (!empty($accordion)) : ?>
                    <div class="feature-holder">
                    <?php /** @var PageAccordeon $item */
                        foreach ($accordion as $key => $item) : ?>
                            <div <?= ($key === 0) ? ' class="opened"' : '' ?>>
                                <h2><?= $item->title ?></h2>
                                <?= $item->body ?>
                            </div>
                    <?php endforeach; ?>
                    </div>
                <?php endif; ?>

                <?= $page['body']; ?>
            </div>
        <?php endif; ?>
    </div>
</section>

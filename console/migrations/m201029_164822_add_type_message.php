<?php

use yii\db\Migration;

/**
 * Class m201029_164822_add_type_message
 */
class m201029_164822_add_type_message extends Migration
{
    private $chat = '{{%user_chat}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->chat, 'parent_id', $this->integer()->null()->after('id'));
        $this->addColumn($this->chat, 'file_type', $this->string(25)->null()->after('file_id'));
        $this->addColumn($this->chat, 'file_watermark_id', $this->integer()->null()->after('file_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->chat, 'file_type');
        $this->dropColumn($this->chat, 'parent_id');
    }
}

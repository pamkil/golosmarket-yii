<?php
namespace frontend\models;

use Carbon\Carbon;
use common\models\access\User;
use common\service\Mail;
use yii\base\Model;

/**
 * Password reset request form
 */
class RestoreRequestForm extends Model
{
    public $email;

    public function formName()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'userExists'],
        ];
    }

    public function userExists($attribute) {
        $deleteUserExist = User::findDeleted()->andWhere([
            'email' => $this->{$attribute},
            'status' => User::STATUS_DELETED
        ])->exists();

        $activeUserExists = User::find()->where([
            'AND',
            ['email' => $this->{$attribute}],
            ['status' => User::STATUS_ACTIVE],
            [
                'OR',
                ['<', 'blocked_at', Carbon::now()->toDateTimeString()],
                ['blocked_at' => null]
            ]
        ]);

        if (!$deleteUserExist && !$activeUserExists) {
            $this->addError($attribute, 'Пользователь не найден');
        }
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was sending
     */
    public function sendEmail()
    {
        $mailSending = $this->sendPasswordRestoreMail();

        if (!$mailSending) {
            $mailSending = $this->sendRestoreProfileMail();
        }

        return $mailSending;
    }

    private function sendRestoreProfileMail()
    {
        /* @var $user User */
        $user = User::findDeleted()->andWhere(['email' => $this->email])->one();

        if (!$user) {
            return false;
        }

        $user->generateEmailVerificationToken();
        if (!$user->save()) {
            return false;
        }

        return Mail::sendProfileRestore(
            $user->email,
            $user->firstname,
            $user->verification_token
        );
    }

    private function sendPasswordRestoreMail()
    {
        /* @var $user User */
        $user = User::findByEmail($this->email);

        if (!$user) {
            return false;
        }

        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        return Mail::sendPasswordReset(
            $user->email,
            $user->password_reset_token,
            $user->firstname
        );
    }
}

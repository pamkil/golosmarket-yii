import React, { Component } from 'react';
import './AudioMessage.css';

import FaError from 'react-icons/lib/fa/exclamation-triangle';
import FaFile from 'react-icons/lib/fa/file';
import pause from "../../../../images/pause.png";
import play from "../../../../images/play.png";
import { PLAYER } from "../../../Common/PlayerEvent/playerConst";
import downloadImg from "../../../../images/download.svg";
import downloadImgWhite from "../../../../images/download-white.svg";
import copyImg from "../../../../images/copy.svg";
import copyImgWhite from "../../../../images/copy-white.svg";
import copy from "copy-to-clipboard";
import {toast} from "react-toastify";
import {textCopyFile} from "../../../Announcer/Announcer";

export const FILE_TYPE = {
    WATERMARK: 'watermark',
    ORIGIN: 'origin',
    PUBLIC: 'public',
    ALL: 'all'
}

export class AudioMessage extends Component {
    state = {
        playing: false
    };

    componentDidMount() {
        document.addEventListener(PLAYER.PLAYING, this.playHandler);
        document.addEventListener(PLAYER.STOP, this.stopHandler);
    }

    componentWillUnmount() {
        document.removeEventListener(PLAYER.PLAYING, this.playHandler);
        document.removeEventListener(PLAYER.STOP, this.stopHandler);
    }

    onClick(e) {
        // if (!this.props.data.status)
        //     return;

        if (!this.props.data.status.download && this.props.onDownload instanceof Function) {
            this.props.onDownload(e);
        } else if (this.props.data.status.download && this.props.onOpen instanceof Function) {
            this.props.onOpen(e);
        }
    }

    playingHandler = () => {
        this.props.onPlay(this.props.file);
    }

    stoppingHandler = () => {
        this.props.onStop();
    }

    playHandler = ({ detail: { file } }) => {
        this.setState({ playing: file.url === this.props.file.url });
    }

    stopHandler = () => {
        this.setState({ playing: false });
    }

    copyHandler = (fileUrl) => {
        copy(document.location.origin + fileUrl);
        toast.info(textCopyFile);
    };

    handleShareFile = () => {
        this.props.onShareFile(this.props.id);
    }

    render() {
        const { playing } = this.state;
        const { fileType } = this.props;
        const isAudio = this.props.data && this.props.data.type.includes('audio');
        const error = this.props.data.status && this.props.data.status.error === true;

        return (
            <div className='rce-mbox-audio'>
                <div className='rce-mbox-audio_player'>
                    {
                        isAudio
                            ? playing
                                ? <img className='rce-mbox-audio_play' src={ pause } alt="Остановить" onClick={ this.stoppingHandler }/>
                                : <img className='rce-mbox-audio_play' src={ play } alt="Воспроизвести" onClick={ this.playingHandler }/>
                            : <div className="rce-mbox-audio--icon">
                                <FaFile color='#aaa'/>
                                <div className="rce-mbox-audio--size">
                                    {this.props.data.size}
                                </div>
                            </div>
                    }

                    {fileType === FILE_TYPE.PUBLIC ? <>
                        <a
                          className="rce-mbox-audio_download"
                          download={this.props.file.name}
                          target="_blank"
                          href={this.props.file.url}
                          rel="noopener noreferrer"
                          title="Скачать файл"
                        >
                            <img src={this.props.position === 'left' ? downloadImg : downloadImgWhite} alt="скачать"/>
                        </a>
                        <span
                          className="rce-mbox-audio_copy"
                          title="Скопировать ссылку на файл"
                          onClick={() => this.copyHandler(this.props.file.url)}
                        >
                            <img src={this.props.position === 'left' ? copyImg : copyImgWhite} alt="копировать"/>
                        </span>
                    </> : null}

                    <div className="rce-mbox-audio_text">
                        {this.props.text}
                    </div>
                </div>

                <div className='rce-mbox-audio__buttons'>
                    {fileType === FILE_TYPE.ORIGIN && !this.props.isWriteOpponent && (
                        <button
                            className='rce-mbox-audio_button'
                            onClick={this.handleShareFile}
                        >Дать доступ</button>
                    )}
                </div>

                <div className='rce-mbox-audio_message'>
                    {fileType === FILE_TYPE.WATERMARK && (
                        'Добавлены ватермарки. Исполнитель должен открыть возможность скачать'
                    )}

                    {fileType === FILE_TYPE.ORIGIN && (
                        'Добавлены ватермарки. Можно слушать, но скачать без Вашего разрешения нельзя.'
                    )}

                    {fileType === FILE_TYPE.PUBLIC && (
                        'Скачайте файл. Мы будем хранить его еще 7 дней от этой даты.'
                    )}
                </div>
                <div className="rce-mbox-audio--buttons">
                    {
                        error &&
                        <span className="rce-error-button">
                            <FaError color='#ff3d3d'/>
                        </span>
                    }
                </div>
            </div>
        );
    }
}

AudioMessage.defaultProps = {
    text: '',
    data: {},
    onClick: null,
    onDownload: null,
    onOpen: null,
};

export default AudioMessage;

<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m240405_104209_add_notify_columns_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'notify_email', $this->boolean());
        $this->addColumn('{{%user}}', 'notify_sms', $this->boolean());
        $this->addColumn('{{%user}}', 'notify_telegram', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'notify_email');
        $this->dropColumn('{{%user}}', 'notify_sms');
        $this->dropColumn('{{%user}}', 'notify_telegram');
    }
}

export const VALIDATE = {
  REQUIRED: 'Поле обязательно для заполнения',
  REQUIRED_TRANSLATE: 'Перевод должен быть',
  MAX_LENGTH: 'Длина строки не более',
};

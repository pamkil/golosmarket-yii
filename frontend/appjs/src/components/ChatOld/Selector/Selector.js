import React from 'react';
import SelectorItem from "./SelectorItem";

const Selector = ({ filterActive = 'man', onChange, filters, isAnnouncer }) =>
    isAnnouncer
        ? null
        : (
        <div className="sortchat">
            <ul>
                {
                    filters.map(item =>
                        <SelectorItem
                            key={ item.id }
                            id={ item.id }
                            title={ item.title }
                            isActive={ filterActive === item.id }
                            onClick={ onChange }
                        />
                    )
                }
            </ul>
        </div>
    );


export default Selector;
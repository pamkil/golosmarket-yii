import React from "react";
import { Field, reduxForm } from 'redux-form';
import { FORMS } from '../../../data/constants'
const validate = values => {
    const errors = {};
    if ( !values.agree ) {
        errors.agree = 'Необходимо согласие';
    }
    if ( !values.firstname ) {
        errors.firstname = 'Необходимо заполнить';
    } else if ( values.firstname.length < 2 ) {
        errors.firstname = 'Необходимо ввести не менее 2 символов';
    }
    if ( !values.email ) {
        errors.email = 'Необходимо заполнить';
    }
    return errors;
};

let ReviewForm = ({ handleSubmit, pristine, submitting, valid }) => (
    <form onSubmit={ handleSubmit } className="modal-form">
        <div className="item">
            <div className="block agree">
                <Field
                    name="agree"
                    component="input"
                    type="checkbox"
                />
                Нажимая "Зарегистрироваться", я соглашаюсь с <a href="/rulespdn">правилами использования сайта</a>
                и <a href="/rulespdn">даю согласие на обработку персональных данных</a>
            </div>
            <div className="block"/>
        </div>
        <div className="item">
            <button type="submit" disabled={ pristine || submitting } className={ !valid ? 'error-submit' : '' }>
                Зарегистрироваться
            </button>
        </div>
    </form>
);

ReviewForm = reduxForm( {
    form: FORMS.REVIEW,
    validate
} )( ReviewForm );

export default ReviewForm;

<?php

namespace common\models\user;

use common\models\access\User;
use common\models\announcer\File;
use common\models\Model;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Автор публикаций
 *
 * Class Chat
 * @package common\models\user
 * @property int $id
 * @property int parent_id [int(11)]
 * @property int $announcer_id [int(11)]
 * @property int $client_id [int(11)]
 * @property int $file_id [int(11)]
 * @property int file_watermark_id [int(11)]
 * @property string $file_type [varchar(25)]
 * @property string $text
 * @property bool $is_read [tinyint(1)]
 * @property bool $is_send [tinyint(1)]
 * @property bool $is_edit [tinyint(1)]
 * @property bool $is_write_client [tinyint(1)]
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property int $deleted_at [timestamp]
 * @property User $announcer
 * @property User $client
 * @property File $file
 */
class Chat extends Model
{
    public const UNREAD_SMS_MINUTES = 5;
    public const UNREAD_EMAIL_MINUTES = 2;

    public bool $isImport = false;

    public static function tableName()
    {
        return '{{%user_chat}}';
    }

    public static function find()
    {
        return parent::find()->where(['deleted_at' => 0]);
    }

    public function formName()
    {
        return '';
    }

    public function rules()
    {
        return [
            [
                ['announcer_id', 'client_id', 'is_write_client'],
                'required'
            ],
            [
                ['announcer_id', 'client_id', 'file_id', 'parent_id', 'file_watermark_id'],
                'integer'
            ],
            [
                ['is_read', 'is_send', 'is_edit', 'is_write_client'],
                'boolean'
            ],
            [
                ['text', 'file_type'],
                'string'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'parent ID',
            'announcer_id' => 'Диктор',
            'client_id' => 'Клиент',
            'file_id' => 'Файл',
            'file_watermark_id' => 'Файл watermark',
            'file_type' => 'Тип Файл',
            'is_write_client' => 'Написал клиент, иначе диктор',
            'is_read' => 'Прочитан',
            'is_send' => 'Получен',
            'is_edit' => 'Отредактирован',
            'text' => 'Текс сообщения',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);
        if ($this->isImport) {
            unset($behaviors['timestamp']);
        }

        return $behaviors;
    }

    public function getAnnouncer()
    {
        return $this->hasOne(User::class, ['id' => 'announcer_id']);
    }

    public function getClient()
    {
        return $this->hasOne(User::class, ['id' => 'client_id']);
    }

    public function getFile()
    {
        return $this->hasOne(File::class, ['id' => 'file_id']);
    }

    public static function getLastModifiedUserChat(int $userOneId)
    {
        return static::find()
            ->where([
                'OR',
                ['announcer_id' => $userOneId],
                ['client_id' => $userOneId],
            ])
            ->max('updated_at');
    }

    /**
     * @param int $userId
     *
     * @param bool $isAnnouncer
     * @return int
     */
    public static function getUnreadMessages(int $userId, bool $isAnnouncer)
    {
        $field = $isAnnouncer ? 'announcer_id' : 'client_id';
        return (int)static::find()
            ->andWhere([
                'AND',
                ['is_read' => false],
                [$field => $userId],
                ['is_write_client' => $isAnnouncer],
                ['deleted_at' => 0]
            ])
            ->count();
    }

    public static function unReadQuery(int $minutes = self::UNREAD_EMAIL_MINUTES): ActiveQuery
    {
        $chatMessages = Chat::find()
            ->with(['client', 'announcer'])
            ->where([
                'AND',
                ['is_read' => false],
                ['<', 'created_at', new Expression("SUBTIME(NOW(), $minutes * 60)")],
                ['IS NOT', 'deleted_at', new Expression("true")]
            ]);

        return $chatMessages;
    }
}

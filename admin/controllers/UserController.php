<?

namespace admin\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\debug\models\timeline\DataProvider;
use yii\i18n\Formatter;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use common\models\access\User;

class UserController extends SecurityController
{
    public function actionIndex()
    {
//        if (!Yii::$app->user->can('viewUsers')) {
//            throw new ForbiddenHttpException('Доступ запрещен');
//        }

        $searchModel = new User();
        $dataProvider = new ActiveDataProvider();
        $dataProvider->query = User::find();
//        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список пользователей';
        $this->view->params['breadcrumbs'] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionCreate()
    {
        if (!Yii::$app->user->can('updateUsers')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $model = new User;

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                $model->loadRoles($post['User']['roles'] ?? []);
                $model->loadPhones($post['UserPhones'] ?? []);
                $model->loadEmails($post['UserEmails'] ?? []);
                $model->loadSocials($post['UserSocials'] ?? []);
                $model->loadSip($post['UserSipLines'] ?? []);

                Yii::$app->session->addFlash('success', 'Успешно создано');

                return $this->redirectAjax('user/view', [
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Создание пользователя';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список пользователей',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $roles = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');

        $model->status = User::STATUS_ACTIVE;
        $model->gender = User::GENDER_MALE;

        return $this->render('_form', [
            'model' => $model,
            'phones' => [],
            'emails' => [],
            'roles' => $roles,
            'searchModel' => null,
            'dataProvider' => null,
            'searchCouponModel' => null,
            'dataCouponProvider' => null,
            'reviewProvider' => null,
            'reviewSearchModel' => null,
        ]);
    }

    public function actionView($id)
    {
        if (!Yii::$app->user->can('viewUsers')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var User $model */

        $model = User::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $this->view->title = 'Просмотр пользователя "' . $model->name . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список пользователей',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->name;
        $getParams = Yii::$app->request->get();
        $commentModel = new UserCommentSearch;
        $commentProvider = $commentModel->search($id, $getParams);
        $commentProvider->pagination->pageParam = 'c-page';
        $commentProvider->pagination->pageSizeParam = 'c-per-page';

        $searchModel = new OrderSearch;
        $getParams['user_id'] = $model->id;
        $dataProvider = $searchModel->search($getParams, 20);

        $reviewSearchModel = new ReviewSearch;
        $reviewProvider = $reviewSearchModel->search(['ReviewSearch' => ['user_id' => $model->id]]);
        $reviewProvider->pagination->pageParam = 'r-page';
        $reviewProvider->pagination->pageSizeParam = 'r-per-page';

        $searchCouponModel = new CouponSearch;
        $dataCouponProvider = $searchCouponModel->search(['CouponSearch' => ['user_id' => $model->id]]);
        $dataCouponProvider->getModels();

        // todo async load by request
        if (Yii::$app->user->can('viewUserCalls') && !empty($model->getUserCurrentPhone())) {
            $CdrModel = new CdrSearch();
            $CdrProvider = $CdrModel
                ->setWithFile(Yii::$app->user->can('viewUserCallsAndSounds'))
                ->search(['phone' => $model->getUserCurrentPhone()]);
            $CdrProvider->pagination->pageParam = 'cdr-page';
            $CdrProvider->pagination->pageSizeParam = 'cdr-per-page';
            try {
                $CdrProvider->getModels();
            } catch (\yii\db\Exception $exception) {
                $CdrProvider = null;
            }
        }

        $dateStart = Yii::$app->request->get('dateStart', 'first day of this month');
        $dateEnd = Yii::$app->request->get('dateEnd', 'now');
        [$statistics, $all, $total, $dateStartB, $dateEndB] = $model->getUserStatistics($dateStart, $dateEnd);

        return $this->render('view', [
            'model' => $model,
            'commentProvider' => $commentProvider,
            'statistics' => $statistics,
            'all' => $all,
            'total' => $total,
            'dateStart' => $dateStartB,
            'dateEnd' => $dateEndB,
            'searchModel' => $searchModel,
            'orderProvider' => $dataProvider,
            'reviewSearchModel' => $reviewSearchModel,
            'reviewProvider' => $reviewProvider,
            'dataCouponProvider' => $dataCouponProvider,
            'cdrProvider' => $CdrProvider ?? null,
            'searchCouponModel' => $searchCouponModel,
        ]);
    }

    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('updateUsers')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var User $model */

        $model = User::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if (isset($post['fileId'])) {
                $model->loadImage('photo');
                return $model->getErrorAction();
            }
            if ($model->load($post) && $model->save()) {
                $model->loadRoles($post['User']['roles'] ?? []);
                $model->loadPhones($post['UserPhones'] ?? []);
                $model->loadEmails($post['UserEmails'] ?? []);
                $model->loadSocials($post['User']['socials'] ?? []);
                $model->loadSip($post['UserSipLines'] ?? []);

                $model->addition->load($post);
                $model->addition->save();

                if ($model->getOrders()->count() > 0) {
                    $model->setDiscount(true);
                }

                Yii::$app->session->addFlash('success', 'Успешно обновлено');

                return $this->redirectAjax('user/update', [
                    'id' => $model->id
                ]);

            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', is_array($error) ? $error[0] : $error);
            }
        }

        $this->view->title = 'Изменение пользователя "' . $model->name . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список пользователей',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->name;

        $phones = [];
        if ($model->phones) {
            foreach ($model->phones as $phone) {
                $phones[$phone->id] = $phone->value;
            }
        }

        $emails = [];
        if ($model->emails) {
            foreach ($model->emails as $email) {
                $emails[$email->id] = $email->value;
            }
        }

        $roles = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');

        $searchModel = new OrderSearch;
        $dataProvider = $searchModel->search(['user_id' => $model->id]);

        if (!$model->addition) {
            $model->link('addition', new UserAdditions());
        }
        if ($model->birthday) {
            $model->birthday = Yii::$app->formatter->asDate($model->birthday, Formatter::FORMAT_WIDTH_SHORT);
        }

        return $this->render('_form', [
            'model' => $model,
            'phones' => $phones,
            'emails' => $emails,
            'roles' => $roles,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionDuplicate($id)
    {
        if (!Yii::$app->user->can('updateUsers')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var User $model */

        $model = User::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        /* @var User $newModel */

        $newModel = $model->duplicate();

        if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/update') !== false) {
            return $this->redirectAjax('user/update', [
                'id' => $newModel->id
            ]);
        } else {
            return $this->redirectAjax('user/index');
        }
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('updateUsers')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var User $model */

        $model = User::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirectAjax('user/index');
    }

    public function actionCreateComment($id)
    {
        if (!Yii::$app->user->can('updateUserComments')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var User $user */

        $user = User::findOne(['id' => $id]);
        if ($user === NULL) {
            throw new MethodNotAllowedHttpException;
        }

        $model = new UserComment;

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post)) {
                $model->link('user', $user);
                Yii::$app->session->addFlash('success', 'Успешно создано');

                return $this->redirectAjax('user/view', [
                    'id' => $id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        return $this->renderAjax('_form_comment', [
            'model' => $model
        ]);
    }

    public function actionUpdateComment($id)
    {
        if (!Yii::$app->user->can('updateUserComments')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var UserComment $model */

        $model = UserComment::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new MethodNotAllowedHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');

                return $this->redirectAjax('user/view', [
                    'id' => $model->user_id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        return $this->renderAjax('_form_comment', [
            'model' => $model
        ]);
    }

    public function actionDeleteComment($id)
    {
        if (!Yii::$app->user->can('updateUserComments')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var UserComment $model */

        $model = UserComment::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $user_id = $model->user->id;

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirectAjax('user/view', [
            'id' => $user_id
        ]);
    }

    public function actionAuth($id)
    {
        if (!Yii::$app->user->can('updateUsers')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        if (!Yii::$app->user->can('superAdmin')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $model = User::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        Yii::$app->user->login($model, 0);

        return $this->redirect(['default/index']);
    }
}

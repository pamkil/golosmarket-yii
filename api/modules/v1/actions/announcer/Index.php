<?php

namespace api\modules\v1\actions\announcer;

use api\modules\v1\actions\common\IndexAction;
use api\modules\v1\models\filters\announcer\AnnouncerSearch;
use common\service\announcer\enum\AnnouncerCostEnum;
use Yii;
use yii\web\BadRequestHttpException;

/**
 * @OA\Get(
 *      path="/announcer",
 *      summary="List of announcer",
 *      tags={"Announcer"},
 *      @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="number Page",
 *          required=false,
 *          @OA\Schema(
 *            type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="per_page",
 *          in="query",
 *          description="quantity announcers in Page",
 *          required=false,
 *          @OA\Schema(
 *            type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="filter",
 *          in="query",
 *          description="filter for search announcers",
 *          required=false,
 *          @OA\JsonContent(ref="#/components/schemas/AnnouncerSearch")
 *
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="ok",
 *          @OA\JsonContent(
 *              type="array",
 *              @OA\Items(ref="#/components/schemas/Announcer")
 *          )
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */
class Index extends IndexAction
{
    /** @inheritdoc
     * @throws BadRequestHttpException
     */
    public function run()
    {
        $request = Yii::$app->request;

        $page = (int)$request->get('page', 1);
        $perPage = (int)$request->get('per_page', 12);
        $announcerSearch = new AnnouncerSearch();
        $announcerSearch->load($request->queryParams);

        if ($announcerSearch->validate()) {
            $announcers = $announcerSearch->search($page, $perPage);
        } else {
            $error = $announcerSearch->firstErrors;
            throw new BadRequestHttpException(array_shift($error));
        }

        return [
            'announcers' => $announcers,
            'buttonsCost' => AnnouncerCostEnum::getButtonsCost()
        ];
    }
}

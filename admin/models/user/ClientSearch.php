<?php

namespace admin\models\user;

use common\models\access\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ClientSearch extends Model
{
    public $username;
    public $firstname;
    public $lastname;
    public $email;
    public $phone;
    public $last_visit;
    public $status;
    public $is_blocked;

    public function rules()
    {
        return [
            [
                ['username', 'firstname', 'lastname', 'email', 'phone', 'last_visit'],
                'string'
            ],
            [
                ['status',],
                'integer'
            ],
            [
                ['is_blocked',],
                'boolean'
            ],
        ];
    }

    public function search($params)
    {
        $query = User::find()
            ->where([
                'AND',
                ['type' => User::TYPE_CLIENT],
                ['!=', 'status', User::STATUS_DELETED]
            ])
            ->with('city');
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'username',
                        'firstname',
                        'lastname',
                        'email',
                        'phone',
                        'last_visit',
                        'status',
                        'is_blocked',
                    ]
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $dataProvider->sort->defaultOrder = ['last_visit' => SORT_DESC,];

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'AND',
                ['like', 'username', $this->username],
                ['like', 'firstname', $this->firstname],
                ['like', 'lastname', $this->lastname],
                ['like', 'email', $this->email],
                ['like', 'phone', $this->phone],
                ['like', 'last_visit', $this->last_visit],
                ['status' => $this->status],
                ['is_blocked' => $this->is_blocked],
            ]
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}

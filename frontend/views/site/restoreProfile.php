<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;

$this->title = 'Профиль восстановлен';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content contentpage">
    <div class="container">
        <div class="titlepage">
            <a class="icon mobile-no" href="/">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M19 12H6M12 5l-7 7 7 7"></path></svg>
            </a>
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="list-news">
            <p>Теперь вам доступны все возможности golosmarket.ru.</p>
            <p>В правом верхнем углу нажмите кнопку Вход и введите логин/пароль указанные при регистрации.</p>
            <p><a href="/" class="button">На главную</a></p>
        </div>
    </div>
</section>

<?php

namespace api\modules\v1\actions\favorite;

use common\models\access\User;
use common\models\user\Favorite;
use Yii;
use yii\rest\Action;
use yii\web\BadRequestHttpException;

/**
 * @OA\Delete(
 *      path="/favorite/{id}",
 *      summary="Удалить диктора из избранного",
 *      tags={"Favorite"},
 *      @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          description="Идентификатор избранного пользователя",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="Success",
 *          @OA\JsonContent(
 *              @OA\Property(
 *                  property="announcerId",
 *                  type="integer",
 *                  description="Идентификатор избранного пользователя"
 *              ),
 *              @OA\Property(
 *                  property="quantity",
 *                  type="integer",
 *                  description="Количество голосов избранного пользователя"
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *          response="400",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="401",
 *          description="Unauthorized",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */

/**
 * Class Delete
 * @throws BadRequestHttpException
 * @package api\modules\v1\actions\favorite
 *
 */
class Delete extends Action
{
    public function run($id)
    {
        $clientId = Yii::$app->user->id;

        return [
            'announcerId' => $id,
            'quantity' => Favorite::deleteFavorite($id, $clientId)
        ];
    }
}

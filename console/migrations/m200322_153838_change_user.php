<?php

use yii\db\Migration;

/**
 * Class m200322_153838_change_user
 */
class m200322_153838_change_user extends Migration
{
    private $table = '{{%user}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn($this->table, 'username');
        $this->addColumn($this->table, 'username', $this->string()->null()->after('type'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn($this->table, 'username', $this->string()->notNull()->unique());
    }
}

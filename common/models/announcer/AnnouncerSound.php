<?php

namespace common\models\announcer;

use common\models\Model;
use yii\db\Query;
use yii\helpers\StringHelper;

/**
 * Class AnnouncerSound
 * @package common\models\user
 *
 * @OA\Schema(title="Демо запись")
 *
 * @property int $announcer_id
 * @property int $sound_id
 * @property int $presentation_id
 * @property int $age_id
 * @property int $gender_id
 * @property string $lang_code
 * @property string|null $parodist_name
 * @property Presentation $presentation
 * @property Announcer $announcer
 * @property File $sound
 * @property Age $age
 * @property Gender $gender
 */
class AnnouncerSound extends Model
{
    public static function tableName()
    {
        return '{{%user_announcer_2_sound}}';
    }

    public function rules()
    {
        return [
            [
                ['announcer_id', 'presentation_id', 'sound_id', 'age_id', 'gender_id'],
                'integer'
            ],

            [
                ['parodist_name', 'lang_code'],
                'safe'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'announcer_id' => 'announcer',
            'presentation_id' => 'presentation',
            'sound_id' => 'sound',
            'age_id' => 'age',
            'gender_id' => 'gender',
            'lang_code' => 'Language',
            'parodist_name' => 'Parodist Name',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);
        unset($behaviors['timestamp']);

        return $behaviors;
    }

    public function getPresentation()
    {
        return $this->hasOne(Presentation::class, ['id' => 'presentation_id']);
    }

    public function getAnnouncer()
    {
        return $this->hasOne(Announcer::class, ['id' => 'announcer_id']);
    }

    public function getSound()
    {
        return $this->hasOne(File::class, ['id' => 'sound_id']);
    }

    public function getAge()
    {
        return $this->hasOne(Age::class, ['id' => 'age_id']);
    }

    public function getGender()
    {
        return $this->hasOne(Gender::class, ['id' => 'gender_id']);
    }

    public static function getExistingLanguages()
    {
        $languagesCode = (new Query())->from(self::tableName())->select('lang_code')
            ->where(['IS NOT', 'lang_code', null])
            ->groupBy('lang_code')
            ->column();

        $languages = [];
        foreach ($languagesCode as $code) {
            $languages[] = [
                'id' => $code,
                'name' => StringHelper::mb_ucfirst(\Locale::getDisplayLanguage($code))
            ];
        }

        return $languages;
    }

    public static function getAllLanguages()
    {
        $locales = array_filter(\ResourceBundle::getLocales(''), function ($value) {
            return strpos($value, '_') === false && strlen($value) === 2;
        });
        foreach ($locales as $code) {
            $languages[] = [
                'id' => $code,
                'name' => StringHelper::mb_ucfirst(\Locale::getDisplayLanguage($code))
            ];
        }
        $ids = array_column($languages, 'id');
        $names = array_column($languages, 'name');
        array_multisort(
            $names,
            SORT_ASC,
            $ids,
            SORT_ASC,
            $languages
        );

        return $languages;
    }

    public static function getExistingAges()
    {
        return (new Query)->select([
            'id' => 'age.code',
            'name' => 'age.name'
        ])->from(['sound' => self::tableName()])
            ->leftJoin(['age' => Age::tableName()], 'age.id = sound.age_id')
            ->where(['IS NOT', 'sound.age_id', null])
            ->andWhere(['age.active' => 1])
            ->groupBy(['age_id'])
            ->all();
    }

    public static function getExistingPresentations()
    {
        return (new Query)->select([
            'id' => 'p.code',
            'name' => 'p.name'
        ])->from(['sound' => self::tableName()])
            ->leftJoin(['p' => Presentation::tableName()], 'p.id = sound.presentation_id')
            ->where(['IS NOT', 'sound.presentation_id', null])
            ->andWhere(['p.active' => 1])
            ->groupBy(['presentation_id'])
            ->all();
    }
}

import React, { useState, useEffect, useCallback } from "react";
import {
  Field,
  FieldArray,
  reduxForm,
  change,
  submit,
  getFormValues,
} from "redux-form";
import { connect, useDispatch, useSelector } from "react-redux";

import { ReactComponent as LeftImg } from "../../images/left-arrow.svg";
import Loader from "../../images/loader.svg";
import { FORMS } from "../../data/constants";
import QRCode from "qrcode";
import "./profileForm.css";
import InputInlineLite from "../InputInline/InputInlineLite";
import { PLAYER } from "../Common/PlayerEvent/playerConst";
import PhoneInputMask from "../Common/PhoneMaskInput/PhoneInputMask";
import { requiredFieldAboveZero } from "../../data/helpers/Tools";
import config from "../../data/config/";
import DeleteProfile from "./DeleteProfile";
import ProfileSounds from "./ProfileSounds";
import CustomFileInput from "./CustomFileInput";
import ProfilePhoto from "./ProfilePhoto";
import ProfilePhotos from "./ProfilePhotos";
import { setShowWorkTime } from "../../data/redux";
import ModalNew from "../Common/Modal/Modal";

let phoneValidationError = "";

const auto_grow = (element) => {
  element.style.height = "5px";
  element.style.height = element.scrollHeight + 4 + "px";
};

const validate = (values) => {
  const errors = {};
  const requireFields = ["firstname", "email"];

  requireFields.forEach((field) => {
    if (!values[field]) {
      errors[field] = "Необходимо заполнить";
    }
  });

  if (phoneValidationError) {
    errors.phone = phoneValidationError;
  }

  return errors;
};

const Title = ({ children, title }) => (
  <div className="speaker-voice-features">
    <div className="block">
      {title ? <div className="title">{title}</div> : null}
      <div>{children}</div>
    </div>
  </div>
);

const Cell = ({ children, title }) => (
  <div className="speaker-voice-cell">{children}</div>
);

const Item = ({ children, title }) => (
  <div className="speaker-voice-features">
    <div className="block">
      {title ? <div className="label">{title}</div> : null}
      <div>{children}</div>
    </div>
  </div>
);

const Price = ({ children, title, cls }) => (
  <div className={"speaker-price-features" + (cls || "")}>
    <div className="title">
      <span>{title}</span>
    </div>
    <div className={"speaker-info"}>
      <span>от</span>
      {children}
    </div>
  </div>
);

const soundValidate = (values) => {
  if (!values || !values.length) return;
  let errorsArray = [];

  if (errorsArray.every((value) => !value)) {
    return;
  }

  return errorsArray.length ? errorsArray : undefined;
};

let ProfileForm = ({
  user,
  profile,
  keyForm,
  fileLoading,
  handleSubmit,
  onFileUpload,
  onRemoveSound,
  onRemovePhoto,
  onRemoveAvatar,
  onDefaultSound,
  showWorkTimePopUp,
  setShowWorkTimePopUp,
  showTelegramQRPopUp,
  setShowTelegramQRPopUp,
  notifications,
  setNotifications,
  telegramLink,
  setTelegramLink,

  pristine,
  submitting,
  valid,
  reset,
  isSubmitting,
  submitErrors,
  meta,
  errors,
  localDev,
  formStates,
  optionGenders,
  optionAges,
  optionTimbers,
  optionPresentations,
  optionLanguages,
  optionTags,
  showTelegramQRPopUpHandler,

  ...props
}) => {
  let fileUploadTimer;
  const dispatch = useDispatch();
  const profileForm = React.createRef();
  const announcerInfoRef = React.createRef();
  const announcerEquipmentRef = React.createRef();
  const [playing, setPlaying] = useState(null);
  const [phoneValid, setPhoneValid] = useState("");
  const [customLang, setCustomLang] = useState([]);
  const [parodistListIds, setParodistListIds] = useState([]);
  const [vocalListIds, setVocalListIds] = useState([]);
  const [priceReady, setPriceReady] = useState(false);
  const [qrTgLink, setQrTgLink] = useState("");
  const srvr = localDev ? config.URL : "";

  const onRemovePhotoHandler = (file, field) => {
    onRemovePhoto(file);
  };
  const onRemoveAvatarHandler = (file, field) => {
    onRemoveAvatar(file);
  };
  const onRemoveSoundHandler = (file, field) => {
    onRemoveSound(file);
  };
  const onFileUploadHandler = (file) => {
    fileUploadTimer = setTimeout(() => {
      props.dispatch(submit(FORMS.PROFILE));
    }, 10);
  };
  const onDefaultSoundHandler = (file, field) => {
    onDefaultSound(file);
  };
  const playHandler = ({ detail: { file } }) => {
    setPlaying(file);
  };
  const stopHandler = () => {
    setPlaying(null);
  };

  // события проигрывания презентации
  useEffect(() => {
    document.addEventListener(PLAYER.PLAYING, playHandler);
    document.addEventListener(PLAYER.STOP, stopHandler);

    return () => {
      clearTimeout(fileUploadTimer);
      document.removeEventListener(PLAYER.PLAYING, playHandler);
      document.removeEventListener(PLAYER.STOP, stopHandler);
    };
  }, []);

  // очищаем поле стоимости пародии если нет таких презентаций
  useEffect(() => {
    if (priceReady && !parodistListIds.length) {
      props.dispatch(change(FORMS.PROFILE, "announcer.cost_parody", ""));
    }
  }, [parodistListIds, priceReady]);

  // очищаем поле стоимости вокала если нет таких презентаций
  useEffect(() => {
    if (priceReady && !vocalListIds.length) {
      props.dispatch(change(FORMS.PROFILE, "announcer.cost_vocal", ""));
    }
  }, [vocalListIds, priceReady]);

  // Создаём списки презентаций пародии или вокала, проверяем есть ли на них цены
  useEffect(() => {
    if (
      profile &&
      profile.hasOwnProperty("announcer") &&
      profile.announcer &&
      profile.announcer.hasOwnProperty("sounds") &&
      profile.announcer.sounds.length
    ) {
      const parodistList = [];
      const vocalList = [];

      profile.announcer.sounds.forEach((sound, soundIndex) => {
        if (sound.presentation && sound.presentation.hasOwnProperty("code")) {
          switch (sound.presentation.code) {
            case "parodist":
              parodistList.push(soundIndex);
              break;
            case "vocals":
              vocalList.push(soundIndex);
              break;
            default:
              break;
          }
        }
      });

      setParodistListIds(parodistList);
      setVocalListIds(vocalList);
      setPriceReady(true);
    }
  }, [profile]);

  useEffect(() => {
    if (announcerInfoRef.current) {
      auto_grow(announcerInfoRef.current);
    }
  }, [announcerInfoRef.current]);

  useEffect(() => {
    phoneValidationError = phoneValid;
  }, [phoneValid]);

  useEffect(() => {
    if (announcerEquipmentRef.current) {
      auto_grow(announcerEquipmentRef.current);
    }
  }, [announcerEquipmentRef.current]);

  const { isLoading } = useSelector((state) => ({
    isLoading: state.profile.isLoading,
  }));

  const handleNotificationClick = (notificationType) => {
    setNotifications((prevNotifications) => ({
      ...prevNotifications,
      [notificationType]: !prevNotifications[notificationType],
    }));
  };

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (!token || token === null) {
      return;
    }
    const bearer = "Bearer " + token.slice(1, -1);
    fetch("/api/v1/user/telegram", {
      headers: { "Content-Type": "application/json", Authorization: bearer },
    })
      .then((res) => res.json())
      .then((res) => {
        setTelegramLink(res.url);
      })
      .catch((err) => {
        console.log("err token", err);
      });
  }, []);

  useEffect(() => {
    if (telegramLink.length === 0) {
      return;
    }
    QRCode.toDataURL(telegramLink)
      .then((url) => {
        setQrTgLink(url);
      })
      .catch((err) => {
        console.error(err);
      });
  }, [telegramLink]);
  return (
    <>
      <ModalNew
        className={"new-announcer-pop-up-width"}
        show={showWorkTimePopUp}
        onCloseRequest={() => setShowWorkTimePopUp(false)}
      >
        <div className="pop-up-text">
          <span className="title-pop-up">
            Не забудьте отредактировать График работы
          </span>
          Укажите в профиле актуальное время, когда вы В доступе для записи 🎙️📅
        </div>
        <div className="block-controls block-controls-column">
          <button
            className={"popup-btn popup-btn__green"}
            onClick={() => {
              dispatch(setShowWorkTime(true));
            }}
          >
            Перейти в График работы
          </button>
          <button
            className={"popup-btn popup-btn__gray"}
            onClick={() => {
              setShowWorkTimePopUp(false);
            }}
          >
            Закрыть это окно
          </button>
        </div>
      </ModalNew>
      <ModalNew
        className={"new-announcer-pop-up-width"}
        show={showTelegramQRPopUp}
        onCloseRequest={() => setShowTelegramQRPopUp(false)}
      >
        <div className="pop-up-text">
          <span className="title-pop-up">
            Для подключения перейдите в приложение
          </span>
          <a
            href={telegramLink}
            className={"popup-btn popup-btn__green telegram-btn"}
          >
            Перейти в Телеграм
          </a>
          <span className="title-pop-up">Или отсканируйте QR-код:</span>
          <img className="telegram-qr-img" src={qrTgLink} alt="" />
        </div>
      </ModalNew>
      <form
        name={"profile_form"}
        ref={profileForm}
        onSubmit={handleSubmit}
        encType="multipart/form-data"
      >
        <section
          className={
            "content profile" + (fileLoading && isLoading ? " __busy" : "")
          }
        >
          <div className="container-bt">
            <div className="titlepage __aside mobile-no">
              <div className="icon">
                <a href="/">
                  <LeftImg />
                </a>
              </div>
              <h1>{profile.isAnnouncer ? "Профиль" : "Клиент"}</h1>
              <div className="titlepage-btn titlepage-btn-fixed">
                <button type="submit" className="green">
                  Сохранить изменения
                </button>
              </div>
            </div>

            <div className="row">
              <div className="col-md-3">
                {fileLoading === "avatar" ? (
                  <div className="preloader-holder">
                    <img alt="preloader" src={Loader} />
                  </div>
                ) : (
                  <Title title="">
                    <div className="row">
                      <div className="col-md-6">
                        <Field
                          name="photo"
                          component={ProfilePhoto}
                          srvr={srvr}
                          onRemove={onRemoveAvatarHandler}
                        />
                      </div>
                    </div>
                    <label htmlFor="profile_avatar" className="sounds__button">
                      <Field
                        name="addAvatar"
                        id={"profile_avatar"}
                        component={CustomFileInput}
                        onChange={() => {
                          onFileUploadHandler("avatar");
                        }}
                        type="file"
                        keyForm={keyForm}
                        accept="image/*"
                      />
                      <span>Загрузить аватар</span>
                    </label>
                  </Title>
                )}
              </div>
              <div className="col-md-3">
                <Cell>
                  <Field
                    name="firstname"
                    component={InputInlineLite}
                    inputClass={"input-v3"}
                    floatPlaceholder={true}
                    type="text"
                    placeholder="Имя/Псевдоним"
                  />
                </Cell>
              </div>

              <div className="col-md-3">
                <Cell>
                  <Field
                    name="lastname"
                    component={InputInlineLite}
                    inputClass={"input-v3"}
                    floatPlaceholder={true}
                    type="text"
                    placeholder="Фамилия/Псевдоним"
                  />
                </Cell>
              </div>

              <div className="col-md-3">
                <Cell>
                  <Field
                    name="phone"
                    component={PhoneInputMask}
                    inputClass={"speaker-tel-input"}
                    floatPlaceholder={true}
                    setPhoneValid={setPhoneValid}
                    type="tel"
                    placeholder="Номер телефона"
                    required
                  />
                </Cell>
              </div>

              <div className="col-md-3">
                <Cell>
                  <Field
                    name="email"
                    component={InputInlineLite}
                    inputClass={"input-v3"}
                    floatPlaceholder={true}
                    type="email"
                    placeholder="Логин/E-mail"
                  />
                </Cell>
              </div>

              <div className="col-md-3">
                <Cell>
                  <Field
                    name="password"
                    component={InputInlineLite}
                    inputClass={"input-v3"}
                    floatPlaceholder={true}
                    type="password"
                    placeholder="Пароль"
                  />
                </Cell>
              </div>
            </div>
            <div>
              <Title title="Настройки уведомлений">
                <Item
                  title={
                    <div className="telegram-connect-description">
                      <div className="telegram-connect-text-container">
                        <span>
                          Выберите удобный способ получения уведомлений о новых
                          сообщениях в чате Голосмаркета. Самый оперативный
                          способ – Телеграм. Нажмите соответствующую кнопку и во
                          всплывающем окне привяжите ваш аккаунт к боту
                        </span>
                      </div>
                    </div>
                  }
                ></Item>
                <div style={{ display: "flex", marginBottom: "20px" }}>
                  <button
                    type="button"
                    onClick={() => handleNotificationClick("email")}
                    style={{
                      backgroundColor: notifications.email
                        ? "#77bf22"
                        : "white",
                      display: "block",
                      width: "100%",
                      borderRadius: "8px",
                      textAlign: "center",
                      border: notifications.email
                        ? "2px solid #77bf22"
                        : "2px solid black",
                      padding: "10px 0",
                      userSelect: "none",
                      marginRight: "10px",
                      color: notifications.email ? "white" : "black",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                      }}
                    >
                      {notifications.email ? (
                        <svg
                          width="20"
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke-width="1.5"
                          stroke="currentColor"
                          class="w-6 h-6"
                        >
                          <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            d="M9 12.75 11.25 15 15 9.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"
                          />
                        </svg>
                      ) : (
                        <svg
                          width="20"
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke-width="1.5"
                          stroke="currentColor"
                          class="w-6 h-6"
                        >
                          <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            d="m9.75 9.75 4.5 4.5m0-4.5-4.5 4.5M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"
                          />
                        </svg>
                      )}
                      <div style={{ marginLeft: "8px", lineHeight: "20px" }}>
                        Email
                      </div>
                    </div>
                  </button>
                  <button
                    type="button"
                    onClick={() => {
                      showTelegramQRPopUpHandler();
                    }}
                    style={{
                      backgroundColor: notifications.telegram
                        ? "#77bf22"
                        : "white",
                      display: "block",
                      width: "100%",
                      borderRadius: "8px",
                      textAlign: "center",
                      border: notifications.telegram
                        ? "2px solid #77bf22"
                        : "2px solid black",
                      padding: "10px 0",
                      marginRight: "10px",
                      userSelect: "none",
                      color: notifications.telegram ? "white" : "black",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                      }}
                    >
                      {notifications.telegram ? (
                        <svg
                          width="20"
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke-width="1.5"
                          stroke="currentColor"
                          class="w-6 h-6"
                        >
                          <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            d="M9 12.75 11.25 15 15 9.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"
                          />
                        </svg>
                      ) : (
                        <svg
                          width="20"
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke-width="1.5"
                          stroke="currentColor"
                          class="w-6 h-6"
                        >
                          <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            d="m9.75 9.75 4.5 4.5m0-4.5-4.5 4.5M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"
                          />
                        </svg>
                      )}
                      <div style={{ marginLeft: "8px", lineHeight: "20px" }}>
                        Telegram
                      </div>
                    </div>
                  </button>
                  {user.user !== null && user.user.isAnnouncer && (
                    <button
                      type="button"
                      onClick={() => handleNotificationClick("sms")}
                      style={{
                        backgroundColor: notifications.sms
                          ? "#77bf22"
                          : "white",
                        display: "block",
                        width: "100%",
                        borderRadius: "8px",
                        textAlign: "center",
                        border: notifications.sms
                          ? "2px solid #77bf22"
                          : "2px solid black",
                        padding: "10px 0",
                        userSelect: "none",
                        marginRight: "10px",
                        color: notifications.sms ? "white" : "black",
                      }}
                    >
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "center",
                        }}
                      >
                        {notifications.sms ? (
                          <svg
                            width="20"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke-width="1.5"
                            stroke="currentColor"
                            class="w-6 h-6"
                          >
                            <path
                              stroke-linecap="round"
                              stroke-linejoin="round"
                              d="M9 12.75 11.25 15 15 9.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"
                            />
                          </svg>
                        ) : (
                          <svg
                            width="20"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke-width="1.5"
                            stroke="currentColor"
                            class="w-6 h-6"
                          >
                            <path
                              stroke-linecap="round"
                              stroke-linejoin="round"
                              d="m9.75 9.75 4.5 4.5m0-4.5-4.5 4.5M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"
                            />
                          </svg>
                        )}
                        <div style={{ marginLeft: "8px", lineHeight: "20px" }}>
                          SMS
                        </div>
                      </div>
                    </button>
                  )}
                </div>
              </Title>
            </div>
            {profile.isAnnouncer ? (
              <>
                <div className="voice-info">
                  <Title title="Загрузите демо и расскажите что в нём">
                    <Item title="Загрузите примеры звучания вашего голоса (mp3 или wav до 15 Mb) и разметьте их согласно содержанию">
                      <FieldArray
                        name="announcer.sounds"
                        component={ProfileSounds}
                        isLoading={isLoading}
                        fileLoading={fileLoading}
                        formStates={formStates}
                        sound={playing}
                        onFileUpload={onFileUploadHandler}
                        onRemove={onRemoveSoundHandler}
                        onDefault={onDefaultSoundHandler}
                        profile={profile}
                        optionPresentations={optionPresentations}
                        optionAges={optionAges}
                        optionLanguages={optionLanguages}
                        optionGenders={optionGenders}
                        keyForm={keyForm}
                        customLang={customLang}
                        setCustomLang={setCustomLang}
                        // Отправляем списки идентификаторов пародий
                        parodistListIds={parodistListIds}
                        setParodistListIds={setParodistListIds}
                        // Отправляем списки идентификаторов вокала
                        vocalListIds={vocalListIds}
                        setVocalListIds={setVocalListIds}
                        validate={soundValidate}
                      />
                    </Item>
                  </Title>
                </div>

                {
                  // цены на услуги
                }
                <div className="voice-price">
                  <div className="row">
                    <div className="col-md-12">
                      <Title title="Укажите стоимость услуг">
                        <div className="label">
                          В рублях учётом отчисления на развитие сервиса 30%.
                          Подробнее в разделе <a href="/finance">Финансы</a>.
                        </div>
                        <div className="label">
                          Поля Вокал и Пародия будут активны после загрузки
                          соответствующих демо.
                        </div>
                      </Title>
                    </div>

                    <div className="col-md-6">
                      <Price title="Начитка до 30 сек. (неск.дублей)">
                        <Field
                          name="announcer.cost"
                          component={InputInlineLite}
                          inputClass={"input-v1"}
                          type="number"
                          placeholder="Стоимость"
                        />
                      </Price>
                      <Price title="Начитка 1 страницы">
                        <Field
                          name="announcer.cost_a4"
                          component={InputInlineLite}
                          inputClass={"input-v1"}
                          type="number"
                          placeholder="Стоимость"
                        />
                      </Price>
                      <Price
                        title="Вокал"
                        cls={vocalListIds.length ? "" : " __disabled"}
                      >
                        <Field
                          disabled={!vocalListIds.length}
                          name="announcer.cost_vocal"
                          component={InputInlineLite}
                          inputClass={"input-v1"}
                          type="number"
                          placeholder="Стоимость"
                          validate={
                            vocalListIds.length ? [requiredFieldAboveZero] : []
                          }
                        />
                      </Price>
                    </div>

                    <div className="col-md-6">
                      <Price
                        title="Пародия"
                        cls={parodistListIds.length ? "" : " __disabled"}
                      >
                        <Field
                          disabled={!parodistListIds.length}
                          name="announcer.cost_parody"
                          component={InputInlineLite}
                          inputClass={"input-v1"}
                          type="number"
                          placeholder="Стоимость"
                          validate={
                            parodistListIds.length
                              ? [requiredFieldAboveZero]
                              : []
                          }
                        />
                      </Price>
                      <Price title="Смонтировать звук (рекламный ролик)">
                        <Field
                          name="announcer.cost_sound_mount"
                          component={InputInlineLite}
                          inputClass={"input-v1"}
                          type="number"
                          placeholder="Стоимость"
                        />
                      </Price>
                      <Price title="Сценарий для аудиорекламы">
                        <Field
                          name="announcer.cost_audio_adv"
                          component={InputInlineLite}
                          inputClass={"input-v1"}
                          type="number"
                          placeholder="Стоимость"
                        />
                      </Price>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-12">
                    <Title title="Дополнительно">
                      <Field
                        refName={announcerInfoRef}
                        name="announcer.info"
                        component={InputInlineLite}
                        inputClass={"input-v1"}
                        data={false}
                        type="textarea"
                        onChange={(e) => {
                          auto_grow(e.target);
                        }}
                        placeholder="Расскажите про среднюю скорость выполнения задачи, про порядок оплаты, скидки и другие нюансы работы"
                      />
                    </Title>

                    <Title title="Оборудование">
                      <Field
                        refName={announcerEquipmentRef}
                        name="announcer.equipment"
                        component={InputInlineLite}
                        inputClass={"input-v1"}
                        type="textarea"
                        onChange={(e) => {
                          auto_grow(e.target);
                        }}
                        placeholder=""
                      />
                    </Title>

                    <Title title="Фото студии, тон-кабины, оборудования">
                      <>
                        <FieldArray
                          name="announcer.photos"
                          srvr={srvr}
                          component={ProfilePhotos}
                          onRemove={onRemovePhotoHandler}
                        />
                        {fileLoading === "photos" ? (
                          <div className="preloader-holder">
                            <img alt="preloader" src={Loader} />
                          </div>
                        ) : (
                          <label
                            htmlFor="profile_photos"
                            className="sounds__button"
                          >
                            <Field
                              name="addPhotos"
                              id={"profile_photos"}
                              component={CustomFileInput}
                              onChange={() => {
                                onFileUploadHandler("photos");
                              }}
                              keyForm={keyForm}
                              type="file"
                              accept="image/*"
                              multiple
                            />
                            <span>Добавить фото рабочего места</span>
                          </label>
                        )}
                      </>
                    </Title>
                  </div>
                </div>
              </>
            ) : null}

            <DeleteProfile />

            <div className="row save-row">
              <button
                type="submit"
                className="green"
                disabled={pristine || submitting || fileLoading || !valid}
              >
                Сохранить изменения
              </button>
            </div>
          </div>
        </section>
      </form>
    </>
  );
};

ProfileForm = reduxForm({
  form: FORMS.PROFILE,
  touchOnChange: true,
  enableReinitialize: true,
  validate,
})(ProfileForm);

ProfileForm = connect((state) => {
  return {
    ...state.profile.options,
    formStates: getFormValues(FORMS.PROFILE)(state),
    initialValues: state.profile.profile,
    isInit: state.app.initialized,
    localDev: state.app.localDev,
  };
})(ProfileForm);

export default ProfileForm;

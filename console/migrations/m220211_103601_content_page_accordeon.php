<?php

use yii\db\Migration;

/**
 * Class m220211_103601_content_page_accordeon
 */
class m220211_103601_content_page_accordeon extends Migration
{
    private $accordeon = '{{%content_page_accordeon}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            $this->accordeon,
            [
                'id' => $this->primaryKey(),
                'page_id' => $this->integer()->notNull(),
                'title' => $this->string()->notNull(),
                'body' => $this->text()->notNull(),
                'created_at' => $this->timestamp()->notNull(),
                'updated_at' => $this->timestamp()->notNull(),
            ],
            $tableOptions
        );

        $this->createIndex('IX-page_accordeon-page_id', $this->accordeon, 'page_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->accordeon);
    }
}

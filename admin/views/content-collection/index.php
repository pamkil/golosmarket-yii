<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\content\CollectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Список подборок</h3>
        <?= Html::a(
            'Добавить <i class="fa fa-plus"></i>',
            ['create'],
            ['class' => ['btn','bg-olive','btn-flat','margin','pull-right']]
        ) ?>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'headerOptions' => ['style' => 'min-width:45px'],
                ],
                'name',
                'description:html',
                [
                    'attribute' => 'image_id',
                    'content' => function ($data) {
                        if (!empty($data->image)) {
                            return '<img src="' . $data->image->getUrl()
                                . '" class="img-responsive" style="max-height: 250px; max-width: 250px;"/>';
                        }

                        return '';
                    }
                ],
                'active:boolean',
                'position:integer',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]) ?>
    </div>
</div>

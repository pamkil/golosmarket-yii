<?php

use yii\db\Migration;

/**
 * Class m200409_202528_add_relation
 */
class m200409_202528_add_relation extends Migration
{
    private $table = '{{%auth_assignment}}';
    private $user = '{{%user}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'FK-auth_assignment-user_id',
            $this->table,
            'user_id',
            $this->user,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK-auth_assignment-user_id', $this->table);
    }
}

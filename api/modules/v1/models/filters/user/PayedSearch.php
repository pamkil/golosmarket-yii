<?php

namespace api\modules\v1\models\filters\user;

use api\modules\v1\models\records\user\PayedProfile;
use common\service\user\dto\PayedSearchDto;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * @OA\Schema(title="Получение информации о заработке и комиссии")
 */
class PayedSearch extends Model
{
    /**
     * @param PayedSearchDto $dto
     * @return ActiveDataProvider
     */
    public function search(PayedSearchDto $dto): ActiveDataProvider
    {
        PayedProfile::$isAnnouncer = $dto->isAnnouncer;

        $query = PayedProfile::find()->select(['announcer_id','SUM(sum) AS sum','min(updated_at) AS updated_at']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $query->andWhere([
            'announcer_id' => $dto->userId,
            'is_payed' => true,
            'is_send' => false,
            'is_canceled' => false
        ]);
        /*$query->andWhere(['is_payed' => true]);
        $query->andWhere(['is_send' => false]);
        $query->andWhere(['is_canceled' => false]);*/
        $query->andWhere(['IN', 'status', [PayedProfile::STATUS_PAID_ANNOUNCER, PayedProfile::STATUS_PAID_CLIENT]]);
        $query->andWhere('card_number IS NOT NULL');

        $query->groupBy('announcer_id');

        return $dataProvider;
    }

    public function searchNotConfirmation(PayedSearchDto $dto)
    {
        $query = PayedProfile::find();
        $query->andWhere([
            'announcer_id' => $dto->userId,
            'is_payed' => true,
            'is_send' => false,
            'is_canceled' => false,
            'status' => PayedProfile::STATUS_PAID_COMMISSION
        ]);
        $query->andWhere('card_number IS NOT NULL');
        $query->groupBy('announcer_id');

        return $query->exists();
    }
}

<?
namespace admin\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use common\models\settings\Site;
use common\models\settings\Setting;
use common\models\settings\SettingSearch;

class SettingController extends SecurityController
{
    public function actionIndex()
    {
        if (!Yii::$app->user->can('viewSettings')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $searchModel = new SettingSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->view->title = 'Список настроек';
        $this->view->params['breadcrumbs'] = [
            'label' => $this->view->title
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionCreate()
    {
        if (!Yii::$app->user->can('updateSettings')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $model = new Setting;

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно создано');

                return $this->redirect([
                    'update',
                    'id' => $model->id
                ]);
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        $this->view->title = 'Создание настройки';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список настроек',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $model->active = 1;

        $sites = Site::find()
            ->where(['active' => 1])
            ->all();
        $sites = ArrayHelper::map($sites, 'id', 'name');

        return $this->render('_form', [
            'sites' => $sites,
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('updateSettings')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Setting $model */
        $model = Setting::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        $post = Yii::$app->request->post();
        if ($post) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->session->addFlash('success', 'Успешно обновлено');
            }

            foreach ($model->getErrors() as $error) {
                Yii::$app->session->addFlash('error', $error);
            }

            return $this->redirect([
                'update',
                'id' => $model->id
            ]);
        }

        $this->view->title = 'Изменение настройки "' . $model->name . '"';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Список настроек',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->name;

        $sites = Site::find()
            ->where(['active' => 1])
            ->all();
        $sites = ArrayHelper::map($sites, 'id', 'name');

        return $this->render('_form', [
            'sites' => $sites,
            'model' => $model
        ]);
    }

    public function actionDuplicate($id)
    {
        if (!Yii::$app->user->can('updateSettings')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Setting $model */

        $model = Setting::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        /* @var Setting $newModel */

        $newModel = $model->duplicate();

        if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/update') !== false) {
            return $this->redirect([
                'update',
                'id' => $newModel->id
            ]);
        } else {
            return $this->redirect([
                'index'
            ]);
        }
    }
    
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('updateSettings')) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        /* @var Setting $model */

        $model = Setting::findOne(['id' => $id]);
        if ($model === NULL) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка удаления');
        }

        return $this->redirect([
            'index'
        ]);
    }
}
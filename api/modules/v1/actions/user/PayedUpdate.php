<?php

namespace api\modules\v1\actions\user;

use api\modules\v1\actions\common\IndexAction;
use common\models\user\Bill;
use common\service\user\dto\PayedSearchDto;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Yii;

class PayedUpdate extends IndexAction
{
    /**
     * Список ролей
     * @OA\Put(
     *     path="/user/payed",
     *     summary="Отметка об оплате комиссии",
     *     tags={"User"},
     *     security={{"BearerAuth": {}}},
     *     @OA\Response(
     *         response="200",
     *         description="Объект токена",
     *         @OA\JsonContent(ref="#/components/schemas/Token")
     *     )
     * )
     *
     * @return mixed|null
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        /** Пока что блокируем запрос */
        return ['error' => 'Временно недоступен'];

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $dto = $this->getPayedUpdateDto();
        if (!$dto->isAnnouncer) {
            throw new AccessDeniedException();
        }

        $billModel = new Bill();
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $billModel::updateAll(
                [
                    'status' => Bill::STATUS_PAID_COMMISSION
                ],
                [
                    'announcer_id' => $dto->userId,
                    'is_payed' => true,
                    'is_send' => false,
                    'status' => Bill::STATUS_PAID_ANNOUNCER
                ]
            );
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            return ['success' => false];
        }

        return ['success' => true];
    }

    private function getPayedUpdateDto () {
        $user = Yii::$app->user;

        return new PayedSearchDto([
            'isAnnouncer' => $user->identity->isAnnouncer,
            'userId' => $user->id
        ]);
    }
}

import React, { useState, useEffect } from 'react'

import ItemInfo from './ItemInfo';
import ItemAvatar from "./ItemAvatar";
import PlayElement from "./PlayElement";
import ItemReviews from "./ItemReviews";
import ItemPrice from './ItemPrice';
import ItemRating from './ItemRating';
import ItemButtons from './ItemButtons'

const ItemList = (props) => {
  let {id, announcer} = props
  const [isVisibleItemInfo, setIsVisibleItemInfo] = useState(false)

  useEffect(()=>{
    if (announcer) {
      setIsVisibleItemInfo(prev => !prev)
    }
  },[])
  return (
    <div className="item">
      {
        isVisibleItemInfo &&
        <ItemInfo announcerFullInfo={announcer}/>
      }
      <div className='item-main-part'>
        <div className="row">
          <ItemAvatar {...props}/>
          <ItemPrice {...props} />
        </div>
        <div className="row">
          <div className="flex item__info">
            <ItemRating {...props} />
            <ItemReviews {...props}/>
          </div>
          <PlayElement
            {...props}
          />
  
          <div className="flex item__controls">
            <ItemButtons {...props} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ItemList;

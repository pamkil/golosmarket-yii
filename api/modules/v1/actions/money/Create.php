<?php

namespace api\modules\v1\actions\money;

use api\modules\v1\models\records\money\Money;
use api\modules\v1\models\records\user\Bill;
use Yii;
use yii\rest\Action;
use yii\web\BadRequestHttpException;

/**
 * Class Create
 * @throws BadRequestHttpException
 * @package api\modules\v1\actions\money
 *
 */
class Create extends Action
{
    public function run()
    {
        $postData = Yii::$app->request->post();

        if (!$postData) {
            $postData = Yii::$app->request->getQueryParams();
        }

        Yii::warning($postData, 'PayBill');
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $money = new Money(['notification_secret' => Yii::$app->params['ya_notification_secret'] ?? '-1']);
            $money->load($postData, '');
            if ($money->validate()) {
                $bill = null;
                $announcerId = null;
                if ($money->label) {
                    $bill = Bill::find()
                        ->where([
                            'AND',
                            ['ya_label' => $money->label],
                            ['status' => Bill::STATUS_NEW],
                        ])
                        ->orderBy(['id' => SORT_DESC])
                        ->one();
                    
                    if (!$bill) {
                        $labels = explode('-', $money->label);
                        if (count($labels) === 3) {
                            $bill = Bill::find()
                                ->where(['id' => (int)$labels[2]])
                                ->one();

                            if (!$bill && (int)$labels[1] > 1) {
                                if (mb_strpos($labels[1], 'gm-') !== false) {
                                    $announcerId = (int)mb_substr($labels[1], 3);
                                };
                            }
                        }
                    }
                }


                if (!$bill) {
                    $bill = new Bill();
                    $bill->announcer_id = $announcerId;
                    $bill->date = $money->datetime;
                    $bill->sum = round((float)$money->withdraw_amount, 2);
                    $bill->sum = round((float)$money->withdraw_amount, 2);
                }

                if (!$bill) {
                    return '';
                }

                $money->setToBill($bill);
                $bill->ya_success = true;

                if (
                    in_array($bill->status, [Bill::STATUS_NEW, Bill::STATUS_CANCEL_CLIENT, Bill::STATUS_CANCEL_ANNOUNCER])
                    && $bill->sum <= $bill->ya_withdraw_amount
                ) {
                    $bill->status = Bill::STATUS_PAID_CLIENT;
                }

                $bill->save();
                $transaction->commit();
            }
        } catch (\Exception $exception) {
            $transaction->rollBack();
        }

        return '';
    }
}

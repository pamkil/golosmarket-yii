import React, { useEffect, useMemo, useState, useRef, useCallback } from 'react';
import { useDispatch, useSelector } from "react-redux";

import { Virtuoso } from 'react-virtuoso'

import MessageItem from "../MessageItem/MessageItem";
import { requestMessages, requestReadChats, sendFiles, sendMessage } from "../../../data/redux";
import Bottom from "../Bottom/Bottom";
import { user } from "../../../data/redux/user";
import { debounce } from "underscore";

const Row = ({ message, index }) => {
    if (message === false) {
        return <div className="announcer-loaded">Загружается...</div>
    }

    return <div className="message-box">
        <MessageItem
            isLast={ false }
            index={ index }
            { ...message }
        />
    </div>
}

const ListMessages = ({ announcerId, bottom, viewport }) => {
    const virtuoso = useRef(null);
    const dispatch = useDispatch();
    const [storeMessages, setStoreMessages] = useState({ length: 0 });
    const isSendMsg = useRef(false);

    const [messages, quantity, totalCount, page, perPage] = useSelector((
        {
            chat: {
                message: {
                    items: messages,
                    pagination: {
                        perPage,
                        page = 0,
                        quantity,
                        totalCount
                    },
                }
            }
        }
    ) => [messages, quantity, totalCount, page, perPage,]);

    useEffect(() => {
        setStoreMessages({ 1: true, length: 0 })
        dispatch(requestMessages(announcerId, { page: 1 }));
        const timerId = setInterval(() => dispatch(requestMessages(announcerId, { page: 1 })), 5000);
        return () => {
            clearInterval(timerId);
        }
    }, [dispatch, announcerId]);

    //add new messages to storage
    useEffect(() => {
        if (page < 1) {
            return;
        }

        const total = Object
            .values(storeMessages)
            .filter(item => Array.isArray(item))
            .reduce((t, arr) => t + arr.length, 0);

        if (page === 1 && messages.length && storeMessages[1] === true) {
            dispatch(requestReadChats(announcerId, { date: messages[0].createdAt }));
        }

        setStoreMessages({
            ...storeMessages,
            [page]: messages.length ? messages : false,
            length: total + messages.length,
        });
        if (virtuoso.current) {
            virtuoso.current.adjustForPrependedItems(0)
        }
        //\\eslint-disable-next-line
    }, [messages]);

    const loadMoreMessages = useCallback(({ startIndex }) => {
        const page = parseInt((totalCount - startIndex - 1) / perPage) + 1;

        if (storeMessages[page] === true || storeMessages[page]) {
            return;
        }

        const length = Object.values(storeMessages)
            .filter(item => Array.isArray(item))
            .reduce((t, arr) => t + arr.length, 0);

        setStoreMessages({
            ...storeMessages,
            [page]: true,
            length,
        })
        dispatch(requestMessages(announcerId, { page: page }));
    }, [totalCount, perPage, storeMessages, dispatch, announcerId]);


    // useEffect(
    //     () => {
    //
    //     },
    //     [storeMessages]
    // );

    const itemsRenderedHandler = () => {
        // console.log('storeMessages', storeMessages, !storeMessages || !storeMessages[1] || storeMessages[1] === true)
        // if (!storeMessages || !storeMessages[1] || storeMessages[1] === true) {
        //     return;
        // }
        // if (isSendMsg.current === true) {
        //     isSendMsg.current = false;
        //     // virtuoso.
        //     console.log('send_after to', storeMessages.length, storeMessages)
        //     virtuoso.current.scrollToIndex({
        //         index: storeMessages.length - 1,
        //         align: 'end',
        //     })
        // }
    }

    const getMessage = index => {
        index = totalCount - index - 1;
        const page = parseInt(index / perPage) + 1;
        const position = index - (page - 1) * perPage;
        if (Array.isArray(storeMessages[page]) && storeMessages[page][position]) {
            return storeMessages[page][position];
        }
        return false;
    }

    const addMessages = (text) => {
        setStoreMessages({
            1: true,
            length: storeMessages[1].length
        });

        setTimeout(() => dispatch(sendMessage(announcerId, text)), 200);
    };

    useEffect(() => {
        const reload = () => {
            setStoreMessages({
                1: true,
                length: storeMessages[1].length
            });

            setTimeout(() => dispatch(requestMessages(announcerId, { page: 1 })), 200);
        };

        const interval = setInterval(reload, 10000);

        return clearInterval(interval);
        // eslint-disable-next-line
    }, []);

    const sendFileHandler = files => {
        dispatch(sendFiles(announcerId, files));
    }

    const initialIndexOffset = useRef(0)
    useEffect(() => {
        if (totalCount) {
            initialIndexOffset.current = totalCount;
        }
    }, [totalCount])

    return (
        <>
            <div className="chat-middle">
                {
                    storeMessages.length > 0
                        ?
                        <Virtuoso
                            ref={ virtuoso }
                            key={ announcerId + '-' + totalCount }
                            style={ { width: '100%', height: '100%' } }
                            totalCount={ totalCount }
                            initialTopMostItemIndex={ totalCount }
                            defaultItemHeight={ 90 }
                            rangeChanged={ ({ startIndex, endIndex }) => {
                                loadMoreMessages({ startIndex, endIndex })
                            } }
                            item={ index => {
                                const message = getMessage(index);

                                return (
                                    <Row message={ message } index={ index } key={ message.id || null }/>
                                )
                            } }
                        />
                        : null
                }
            </div>
            <Bottom onChange={ text => addMessages(text) } onSendFile={ sendFileHandler }/>
        </>
    );
}

export default ListMessages;
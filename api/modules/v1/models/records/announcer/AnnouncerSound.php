<?php

namespace api\modules\v1\models\records\announcer;

use api\modules\v1\models\records\file\File;
use common\models\announcer\Age;
use common\models\announcer\Gender;
use common\models\announcer\Presentation;
use yii\helpers\StringHelper;

/**
 * @OA\Schema(
 *     schema="RecordsAnnouncerSound",
 *     required={"name"},
 *     title="Диктор"
 * )
 */
class AnnouncerSound extends \common\models\announcer\AnnouncerSound
{
    /**
     * @OA\Property(property="id", type="integer", description="ИД диктора")
     *
     * @return array|false
     */
    public function fields()
    {
        return [
            'presentation_id' => 'present',
            'announcer_id',
            'sound_id',
            'lang_code',
            'presentation',
            'sound',
            'age',
            'language',
            'gender',
            'parodist_name',
        ];
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $result = parent::toArray($fields, $expand, $recursive);

        if (!empty($result['sound'])) {
            foreach ($result['sound'] as $field => $value) {
                $result[$field] = $value;
            }
        }

        return $result;
    }

    public function getPresent()
    {
        return $this->presentation ? $this->presentation->code : '';
    }

    public function getPresentation()
    {
        return $this->hasOne(Presentation::class, ['id' => 'presentation_id']);
    }

    public function getAnnouncer()
    {
        return $this->hasOne(AnnouncerEdit::class, ['id' => 'announcer_id']);
    }

    public function getSound()
    {
        return $this->hasOne(File::class, ['id' => 'sound_id']);
    }

    public function getAge()
    {
        return $this->hasOne(Age::class, ['id' => 'age_id']);
    }

    public function getGender()
    {
        return $this->hasOne(Gender::class, ['id' => 'gender_id']);
    }

    public function getLanguage()
    {
        return $this->lang_code ? StringHelper::mb_ucfirst(\Locale::getDisplayLanguage($this->lang_code)) : null;
    }
}

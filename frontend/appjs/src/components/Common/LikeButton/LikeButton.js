import React from 'react';
import PropTypes from 'prop-types';
import {ReactComponent as IconHeart} from "../../../images/icon-heart.svg";
import {ReactComponent as IconHeartActive} from "../../../images/icon-heart--active.svg";

const LikeButton = ({count, active, disabled, onClick}) => {
  return (
    <button
      className="like-button"
      disabled={disabled}
      onClick={onClick}
    >
      <div className={'tooltip'}>
        <div className="icon">
          {active ? <IconHeartActive/> : <IconHeart/>}
          <span
            dangerouslySetInnerHTML={{__html: active ? 'Удалить&nbsp;из&nbsp;Избранного' : 'Добавить&nbsp;в&nbsp;Избранные&nbsp;голоса'}}/>
        </div>
      </div>
      <span className="like-button__quantity">
        {count && count > 0 ? count : ''}
    </span>
    </button>
  )
}

export default LikeButton

LikeButton.propTypes = {
  count: PropTypes.number,
  disabled: PropTypes.bool,
  active: PropTypes.bool,
  onClick: PropTypes.func.isRequired
}

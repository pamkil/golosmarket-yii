import React from 'react';

const toDateFormat = ( value ) => {
    return (new window.Date(value)).toLocaleString();
};

const Date = ( { value } ) => (
    <>
        { toDateFormat( value ) }
    </>
);

export default Date;

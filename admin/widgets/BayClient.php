<?php

namespace admin\widgets;

use Carbon\Carbon;
use common\models\user\Bill;
use yii\base\Widget;

class BayClient extends Widget
{
    public function run()
    {
        parent::run();

        /**
         * Давай еще впихнем параметр Средний чек за прошлый месяц: деление общей суммы совершённых покупок на их количество за прошлый месяц
         * Считаем сколько клиент потратил
         */

        $date = Carbon::today()->subMonth()->startOfMonth()->toDateTimeString();
        $dateEnd = Carbon::today()->subMonth()->endOfMonth()->toDateTimeString();

        $sum = Bill::find()
            ->cache(60 * 3)
            ->where(
                [
                    'AND',
                    ['>=', 'updated_at', $date],
                    ['<=', 'updated_at', $dateEnd],
                    ['status' => Bill::STATUS_PAID_COMMISSION],
                ]
            )
            ->average('sum');

        echo $this->render('bayClient', ['cost' => \Yii::$app->formatter->asCurrency(round($sum ?? 0, 2))]);
    }
}

<?php

namespace common\models\user;

use common\models\access\User;
use common\models\announcer\Announcer;
use common\models\Model;
use yii\web\BadRequestHttpException;

/**
 * Автор публикаций
 *
 * Class Favorite
 * @package common\models\user
 *
 * @property int $id
 * @property int $announcer_id [int(11)]
 * @property int $client_id [int(11)]
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 */
class Favorite extends Model
{
    public static function tableName()
    {
        return '{{%user_favorite}}';
    }

    public function rules()
    {
        return [
            [
                ['announcer_id', 'client_id'],
                'unique',
                'targetAttribute' => ['announcer_id', 'client_id'],
            ],
            [
                ['announcer_id', 'client_id',],
                'integer',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'announcer_id' => 'Диктор',//Которого добавили в избранное
            'client_id' => 'Клиент',//Кто себе добавил в избранное
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }

    public function getAnnouncer()
    {
        return $this->hasOne(User::class, ['id' => 'announcer_id']);
    }

    public function getClient()
    {
        return $this->hasOne(User::class, ['id' => 'client_id']);
    }

    public static function deleteFavorite(int $announcerId, int $clientId)
    {
        Favorite::deleteAll([
            'announcer_id' => $announcerId,
            'client_id' => $clientId,
        ]);

        return self::calculate($announcerId);
    }

    /**
     * @param int $announcerId
     * @param int $clientId
     *
     * @return int
     * @throws BadRequestHttpException
     */
    public static function addFavorite(int $announcerId, int $clientId)
    {
        $favorite = Favorite::find()
            ->where([
                'announcer_id' => $announcerId,
                'client_id' => $clientId,
            ])
            ->one();

        if ($favorite) {
            return self::calculate($announcerId);
        }

        /**
         * @var Favorite $favorite
         */
        $favorite = new Favorite([
            'announcer_id' => $announcerId,
            'client_id' => $clientId,
        ]);

        if ($favorite->save()) {
            return self::calculate($announcerId);
        }

        throw new BadRequestHttpException($favorite->errors);
    }

    public static function getFavorite(int $announcerId, int $clientId)
    {
        return Favorite::find()
            ->where([
                'announcer_id' => $announcerId,
                'client_id' => $clientId,
            ])
            ->asArray()
            ->one();
    }

    public static function calculate(int $userId, bool $isAnnouncer = true, bool $setValue = true)
    {
        $field = $isAnnouncer ? 'announcer_id' : 'client_id';
        $quantity = (int)Favorite::find()
            ->cache(-1)
            ->where([$field => $userId])
            ->count();

        if ($setValue) {
            User::updateAll(
                [
                    'quantity_favorite' => $quantity,
                ],
                [
                    'id' => $userId,
                ]
            );
        }

        return $quantity;
    }
}

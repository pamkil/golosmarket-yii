<?php

namespace common\models\content;

use api\modules\v1\models\records\announcer\Announcer;
use api\modules\v1\models\records\file\File as SoundFile;
use common\models\announcer\AnnouncerSound;
use common\models\announcer\File;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%content_collection}}".
 *
 * @property int $id
 * @property string $name
 * @property int|null $image_id
 * @property string|null $description
 * @property int $active
 * @property int|null $position
 * @property string $created_at
 * @property string $updated_at
 *
 * @property File $image
 * @property CollectionSound[] $collectionSounds
 * @property SoundFile[] $sounds
 */
class Collection extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%content_collection}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()')
            ],
            'saveRelations' => [
                'class' => SaveRelationsBehavior::class,
                'relations' => [
                    'image',
                    'collectionSounds',
                ],
            ],
        ];
    }


    public function formName()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['image_id', 'active', 'position'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['image_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => File::class,
                'targetAttribute' => ['image_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'image_id' => 'Изображение',
            'description' => 'Описание',
            'active' => 'Активна',
            'position' => 'Позиция',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',

            'image' => 'Обложка',
        ];
    }

    public function fields()
    {
        return [
            'id',
            'name',
            'description',
            'image',
            'active',
            'position',
            'collectionSounds'
        ];
    }


    /**
     * Gets query for [[Image]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::class, ['id' => 'image_id']);
    }

    /**
     * Gets query for [[ContentCollection2Sounds]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCollectionSounds()
    {
        return $this->hasMany(CollectionSound::class, ['collection_id' => 'id']);
    }

    /**
     * Gets query for [[Sounds]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSounds()
    {
        return $this->hasMany(SoundFile::class, ['id' => 'sound_id'])
            ->viaTable(CollectionSound::tableName(), ['collection_id' => 'id']);
    }

    public function getAnnouncerSounds()
    {
        return $this->hasMany(AnnouncerSound::class, ['sound_id' => 'id'])
            ->via('sounds');
    }

    public function getAnnouncers()
    {
        return $this->hasMany(Announcer::class, ['id' => 'announcer_id']);
    }
}

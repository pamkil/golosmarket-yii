<?php

use yii\db\Migration;

/**
 * Class m200411_064611_add_quantity_favorite
 */
class m200411_064611_add_quantity_favorite extends Migration
{
    private $table = '{{%user_announcer}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->table,
            'quantity_favorite',
            $this
                ->integer()
                ->notNull()
                ->defaultValue(0)
                ->after('user_id')
                ->comment('Вычесляемое поле по суммам всех избранных')
        );

        $this->addColumn(
            $this->table,
            'quantity_reviews',
            $this
                ->decimal(3,1)
                ->notNull()
                ->defaultValue(0)
                ->after('user_id')
                ->comment('Вычесляемое поле по stars всех отзывов')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'quantity_favorite');
        $this->dropColumn($this->table, 'quantity_reviews');
    }
}

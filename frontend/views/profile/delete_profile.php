<?php

use Carbon\Carbon;
use common\models\access\User;
use common\models\announcer\Announcer;
use common\models\user\Review;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var string $name
 * @var Announcer $announcer
 * @var User $user
 * @var Review[] $reviews
 * @var string $timeOnline
 * @var string $title
 */

?>
    <section class="content profile">
        <div class="container">
            <div class="titlepage">
                <a class="mobile-no icon" href="/">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M19 12H6M12 5l-7 7 7 7"/></svg>
                </a>
                <h1>
                    <span>
                        <?= Html::encode($title) ?>
                    </span>
                </h1>
            </div>
            <div class="left">

                <div class="announcer-info">
                    <?php if (count($reviews)) : ?>
                        <div class="announcer-reviews">
                            <div class="head">Отзывы</div>
                            <?php foreach ($reviews as $review) : ?>
                                <?php /** @var Review $review */?>
                                <?php $userReview = ($review->type_writer === Review::TYPE_WRITE_CLIENT)
                                    ? $review->client : $review->announcer ; ?>
                                <div class="item">
                                    <div class="item-left">
                                        <div class="image">
                                            <div class="box">
                                                <a href="<?= Url::to(['profile/view', 'id' => $userReview->id]); ?>">
                                                    <img
                                                        src="<?= $userReview->getPhotoUrl() ?>"
                                                        class="img-fluid"
                                                        alt="<?= Html::encode(
                                                            $userReview->firstname . ' ' . mb_substr($userReview->lastname, 0, 1)
                                                        ) ?>"
                                                    >
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-right">
                                        <div class="recent-review">
                                            <a href="<?= Url::to(['profile/view', 'id' => $userReview->id]); ?>">
                                                <div class="name">
                                                    <?= $userReview->firstname ?? $userReview->lastname ?? '' ?>
                                                </div>
                                            </a>
                                            <div class="stars">
                                                <?php $ratingReview = (int)$review->rating; ?>
                                                <?php foreach (range(5, 1) as $item) : ?>
                                                    <?php if ($item <= $ratingReview) : ?>
                                                        <span class="greenstar">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="11" height="15"
                                                                 viewBox="0 -6 24 24" fill="#77BF22" stroke="#77BF22"
                                                                 stroke-width="1.5" stroke-linecap="round"
                                                                 stroke-linejoin="round"><polygon
                                                                        points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                        </span>
                                                    <?php else: ?>
                                                        <span class="whitestar">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="11" height="15"
                                                             viewBox="0 -6 24 24" fill="none" stroke="#77BF22"
                                                             stroke-width="1.5" stroke-linecap="round"
                                                             stroke-linejoin="round"><polygon
                                                                    points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                    </span>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </div>
                                            <div class="date">
                                                <?= Carbon::parse($review->created_at)->format('d.m.Y') ?>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <?= $review->text ?: 'Отзыв без текста'; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php else: ?>
                        <div class="announcer-reviews">
                            <div class="head">
                                Отзывов и рейтинга нет
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
            <div class="right">
                <div class="profile-avatar">
                    <div class="image">
                        <div class="box">
                            <img src="<?= $user->getPhotoUrl() ?>" alt="<?= Html::encode($name) ?>" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>

            <div id="save-row" class="save-row"></div>
        </div>
    </section>

    <script>
      let equipmentSlier;

      const isMobile = function () {
        return getComputedStyle(document.body, ':before').getPropertyValue('content') === '\"mobile\"';
      }

      document.addEventListener('DOMContentLoaded', function () {
        let shareElements = document.querySelectorAll('.js-share-btn');

        //let $fotoramaDiv = $('.fotorama').fotorama();

        Array.prototype.forEach.call(shareElements, function (el, i) {
          el.addEventListener('click', function (e) {
            if (isMobile() && navigator.share) {
              navigator.share({
                title: window.location.hostname,
                text: el.getAttribute('data-name'),
                url: window.location.href
              })
                .then(function () {
                  console.log("Sharing successfully done")
                })
                .catch(function () {
                  console.log("Sharing failed")
                })
            } else {
              e.target.classList.toggle('__opened');
            }

            return false;
          }, false);
        });

        document.addEventListener('click', function (e) {
          if (!e.target.classList.contains('__opened') || e.target.closest('.js-share-btn') === null) {
            Array.prototype.forEach.call(shareElements, function (el, i) {
              el.classList.remove('__opened');
            });
          }
        });
      });
    </script>
<?php if (!empty($page)) : ?>
    <section class="quote">
        <div class="container">
            <div class="content">
                <?php if (!empty($page->title)) : ?>
                    <h2><?= $page->title ?></h2>
                <?php endif; ?>
                <?= $page->body ?? '' ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php

return [
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        // default url
        '' => 'site/main',
        'filter-<filter:.*>' => 'site/main',
        'reset' => 'site/reset',
        'restore-profile' => 'site/restore-profile',
        'verify-email' => 'site/verify-email',
        'announcer/<id:\d+>' => 'site/announcer',
        'collection/<collectionId:\d+>' => 'site/collection',
        'review/<id:\d+>' => 'site/review',
        '<action:chat|profile|favorite>' => 'site/<action>',
        'chat/<id:.*>' => 'site/chat',
        'history' => 'site/history',

        // profile
        'profile/<id:\d+>' => 'profile/view',
        'profile/<id:\d+>/review' => 'profile/review',
        'profile/edit' => 'profile/update',

        // news
        'news' => 'news/index',

        // faq
        'faq' => 'faq/index',

        // robots
        'sitemap.xml' => 'robot/sitemap',

        // page
        '<code>' => 'page/view',
    ],
];

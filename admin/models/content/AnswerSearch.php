<?php

namespace admin\models\content;

use common\models\content\Answer;
use common\models\content\Question;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class AnswerSearch extends Model
{
    public $active;
    public $position;
    public $question_id;
    public $text;

    public function rules()
    {
        return [
            [
                ['text',],
                'string'
            ],
            [
                ['active',],
                'boolean'
            ],
            [
                ['position', 'question_id'],
                'integer'
            ],
        ];
    }

    public function search($params)
    {
        $query = Answer::find()
            ->with('question');

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'text',
                        'active',
                        'position',
                        'question_id'
                    ]
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
            ]
        );

        $dataProvider->sort->defaultOrder = ['position' => SORT_ASC,];

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'AND',
                ['like', 'text', $this->text],
                ['like', 'active', $this->active],
                ['like', 'position', $this->position],
                ['like', 'question_id', $this->question_id],
            ]
        );

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }

    public function getQuestion()
    {
        return Question::find()->where(['id' => $this->question_id])->asArray()->one();
    }
}

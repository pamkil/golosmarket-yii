<?php

namespace api\modules\v1\actions\review;

use api\modules\v1\models\checks\site\ReviewUserForm;
use api\modules\v1\models\records\user\Review;
use common\models\access\User;
use Exception;
use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\web\ForbiddenHttpException;

/**
 * @OA\Post(
 *      path="/review/user",
 *      summary="Добавление в избранное",
 *      tags={"Chat"},
 *      @OA\Response(
 *          response="200",
 *          description="Объект токена",
 *          @OA\JsonContent(ref="#/components/schemas/Chat")
 *      ),
 *      @OA\Response(
 *          response="400",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="401",
 *          description="Unauthorized",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */

/**
 * Class Create
 * @package api\modules\v1\actions\review
 *
 */
class CreateUser extends Action
{
    /**
     * @var string the scenario to be assigned to the new model before it is validated and saved.
     */
    public $scenario = Model::SCENARIO_DEFAULT;

    /**
     * @return ReviewUserForm|Review
     * @throws ForbiddenHttpException
     */
    public function run()
    {
        $clientId = Yii::$app->user->id;
        if (!$clientId) {
            throw new ForbiddenHttpException();
        }

        $postData = Yii::$app->request->post();

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $review = new ReviewUserForm();
            $review->load($postData, '');
            /**
             * @var User $user
             */
            $user = Yii::$app->user->identity;
            if ($review->validate()) {
                $isAnnouncer = $user->isAnnouncer;
                $reviewModel = new Review();
                $reviewModel->is_public = true;
                $reviewModel->load($review->toArray(), '');
                $reviewModel->announcer_id = $isAnnouncer ? $clientId : $review->opponent_id;
                $reviewModel->client_id = $isAnnouncer ? $review->opponent_id : $clientId;
                $reviewModel->type_writer = $isAnnouncer ? Review::TYPE_WRITE_ANNOUNCER : Review::TYPE_WRITE_CLIENT;
                $reviewModel->bill_id = $review->other_id;

                if ($reviewModel->save()) {
                    $reviewModel->refresh();
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }

                return $reviewModel;
            }
            $transaction->rollBack();
            return $review;
        } catch (Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }
}

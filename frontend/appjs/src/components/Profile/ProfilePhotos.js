import {Field} from "redux-form";
import React from "react";
import ProfilePhoto from "./ProfilePhoto";

const ProfilePhotos = ({fields, onRemove, srvr}) => (
    <div className="row">
        {
            fields.map((field, index) => (
                <div key={index} className="col-md-3 col-sm-4 col-xs-6">
                    <Field
                        name={field}
                        srvr={srvr}
                        component={ProfilePhoto}
                        onRemove={file => {
                            onRemove(file);
                            fields.remove(index)
                        }}
                    />
                </div>
            ))
        }
    </div>
);

export default ProfilePhotos;

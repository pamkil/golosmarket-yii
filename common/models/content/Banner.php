<?php

namespace common\models\content;

use common\models\announcer\File;
use common\models\Model;

/**
 *
 * Class Banner
 * @package common\models\content
 *
 * @property int $id
 * @property bool $active
 * @property int $position
 * @property int $file_id
 * @property File $file
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 */
class Banner extends Model
{
    public static function tableName()
    {
        return '{{%content_banner}}';
    }

    public function rules()
    {
        return [
            [
                ['active',],
                'boolean'
            ],
            [
                ['position'],
                'integer'
            ],
            [
                'file_id',
                'safe'
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Активность',
            'position' => 'Позиция',
            'file_id' => 'Файл',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['sluggableBehavior']);

        return $behaviors;
    }

    public function getFile()
    {
        return $this->hasOne(File::class, ['id' => 'file_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->active) {
            Banner::updateAll(['active' => false], ['not in', 'id', $this->id]);
        }
    }
}

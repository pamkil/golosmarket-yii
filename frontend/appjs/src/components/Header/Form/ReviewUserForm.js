import React from "react";
import { Field, reduxForm } from "redux-form";
import { FORMS } from "../../../data/constants";
import StartInput from "./StarInput";

const validate = (values) => {
  const errors = {};

  if (values.text && values.text.length > 0 && values.text.length < 5) {
    errors.text = "Необходимо ввести не менее 5 символов";
  }
  if (!values.rating) {
    errors.rating = "Необходимо заполнить рейтинг";
  }

  return errors;
};

let ReviewUserForm = ({
  handleSubmit,
  pristine,
  submitting,
  valid,
  isAnnouncer,
  opponent,
  createUserReviewFormNotificationTimer,
}) => (
  <form onSubmit={handleSubmit} className="modal-form review-user-form">
    {opponent && (
      <>
        <div className="image">
          <div className="box">
            <img src={opponent.avatar} alt={`Фото ${opponent.name}`} />
          </div>
        </div>
        <div className="head-registration">
          <span className="underline">{opponent.name}</span>
        </div>
        <Field
          name="opponent_id"
          id="opponent_id"
          component="input"
          type="hidden"
          required
        />
      </>
    )}

    <div className="head-registration">
      {isAnnouncer ? "Проверьте поступление на вашу карту и оцените работу с заказчиком" : "исполнителя"}
    </div>
    <div className="item">
      <Field name="rating" id="rating" component={StartInput} required />
    </div>
    <div className="item text-feedback">
      <Field
        name="text"
        component="textarea"
        type="text"
        placeholder="Расскажите как все прошло"
      />
    </div>
    <div style={{ flexDirection: "column" }} className="item">
      <button
        type="submit"
        disabled={pristine || submitting}
        className={!valid ? "error-submit" : ""}
      >
        Отправить
      </button>
     {createUserReviewFormNotificationTimer !== null ? <button
        className="remodal-btn-gray"
        onClick={createUserReviewFormNotificationTimer}
      >
        Напомнить позже
      </button> : <></>}
    </div>
    <div className="item">
      {isAnnouncer && createUserReviewFormNotificationTimer !== null && (
        <p style={{fontSize: "19px"}}>
          Информация по оплате комиссии скоро придет на почту 😊
        </p>
      )}{" "}
    </div>
  </form>
);

ReviewUserForm = reduxForm({
  form: FORMS.REVIEW_USER,
  validate,
})(ReviewUserForm);

export default ReviewUserForm;

import React, { useCallback, useEffect, useRef, useState } from "react";
import { ReactComponent as UploadImg } from "../../../images/file-upload.svg";
import { ReactComponent as CloseIcon } from "../../../images/close.svg";
import telegramImg from "../../../images/telegram.svg";
import classes from "classnames";
import Loader from "../../Common/Loader/Loader";
import { linkParser } from "../../../data/helpers/Tools";
import { MESSAGE_TYPE, MESSAGE_TYPE_RU } from "../messageType";
import FaFile from "react-icons/lib/fa/file";
import { toast } from "react-toastify";
import italic from "../../../images/italic.svg";
import bold from "../../../images/bold.svg";
import underline from "../../../images/underline.svg";
import upArrow from "../../../images/chevron-up.svg";
import downArrow from "../../../images/chevron-down.svg";

import "./ChatInput.css";

// emojies
import emojiMenu from "../../../images/emojies/emoji-menu.png";
import smilingFaceWithSmilingEyes from "../../../images/emojies/smiling-face-with-smiling-eyes.png";
import smilingFaceWithSunglasses from "../../../images/emojies/smiling-face-with-sunglasses.png";
import smilingFaceWithHeartEyes from "../../../images/emojies/smiling-face-with-heart-eyes.png";
import pleadingFace from "../../../images/emojies/pleading-face.png";
import grinningFaceWithSmilingEyes from "../../../images/emojies/grinning-face-with-smiling-eyes.png";
import grinningFaceWithSweat from "../../../images/emojies/grinning-face-with-sweat.png";
import rollingOnTheFloorLaughing from "../../../images/emojies/rolling-on-the-floor-laughing.png";
import winkingFace from "../../../images/emojies/winking-face.png";

import smilingFaceWithHalo from "../../../images/emojies/smiling-face-with-halo.png";
import faceBlowingAKiss from "../../../images/emojies/face-blowing-a-kiss.png";
import faceSavoringFood from "../../../images/emojies/face-savoring-food.png";
import squintingFaceWithTongue from "../../../images/emojies/squinting-face-with-tongue.png";
import faceWithHandOverMouth from "../../../images/emojies/face-with-hand-over-mouth.png";
import thinkingFace from "../../../images/emojies/thinking-face.png";
import faceWithMonocle from "../../../images/emojies/face-with-monocle.png";
import zipperMouthFace from "../../../images/emojies/zipper-mouth-face.png";
import faceWithRaisedEyebrow from "../../../images/emojies/face-with-raised-eyebrow.png";
import grimacingFace from "../../../images/emojies/grimacing-face.png";
import sleepingFace from "../../../images/emojies/sleeping-face.png";
import faceWithHeadBandage from "../../../images/emojies/face-with-head-bandage.png";
import nauseatedFace from "../../../images/emojies/nauseated-face.png";
import sneezingFace from "../../../images/emojies/sneezing-face.png";
import explodingHead from "../../../images/emojies/exploding-head.png";
import partyingFace from "../../../images/emojies/partying-face.png";
import confusedFace from "../../../images/emojies/confused-face.png";
import fearfulFace from "../../../images/emojies/fearful-face.png";
import flushedFace from "../../../images/emojies/flushed-face.png";
import loudlyCryingFace from "../../../images/emojies/loudly-crying-face.png";

import confoundedFace from "../../../images/emojies/confounded-face.png";
import tiredFace from "../../../images/emojies/tired-face.png";
import enragedFace from "../../../images/emojies/enraged-face.png";
import pileOfPoo from "../../../images/emojies/pile-of-poo.png";
import wavingHand from "../../../images/emojies/waving-hand.png";
import loveYouGesture from "../../../images/emojies/love-you-gesture.png";

import backhandIndexPointingLeft from "../../../images/emojies/backhand-index-pointing-left.png";
import backhandIndexPointingRight from "../../../images/emojies/backhand-index-pointing-right.png";
import backhandIndexPointingUp from "../../../images/emojies/backhand-index-pointing-up.png";
import backhandIndexPointingDown from "../../../images/emojies/backhand-index-pointing-down.png";
import thumbsUp from "../../../images/emojies/thumbs-up.png";
import thumbsDown from "../../../images/emojies/thumbs-down.png";
import foldedHands from "../../../images/emojies/folded-hands.png";
import flexedBiceps from "../../../images/emojies/flexed-biceps.png";
import microphone from "../../../images/emojies/microphone.png";

const MAX_HEIGHT = 150;
const MAX_FILE_SIZE_MB = 16;
const emojiesList = [
  smilingFaceWithSmilingEyes,
  smilingFaceWithSunglasses,
  smilingFaceWithHeartEyes,
  pleadingFace,
  grinningFaceWithSmilingEyes,
  grinningFaceWithSweat,
  rollingOnTheFloorLaughing,
  winkingFace,
  smilingFaceWithHalo,
  faceBlowingAKiss,
  faceSavoringFood,
  squintingFaceWithTongue,
  faceWithHandOverMouth,
  thinkingFace,
  faceWithMonocle,
  zipperMouthFace,
  faceWithRaisedEyebrow,
  grimacingFace,
  sleepingFace,
  faceWithHeadBandage,
  nauseatedFace,
  sneezingFace,
  explodingHead,
  partyingFace,
  confusedFace,
  fearfulFace,
  flushedFace,
  loudlyCryingFace,
  confoundedFace,
  tiredFace,
  enragedFace,
  pileOfPoo,
  wavingHand,
  loveYouGesture,
  backhandIndexPointingLeft,
  backhandIndexPointingRight,
  backhandIndexPointingUp,
  backhandIndexPointingDown,
  thumbsUp,
  thumbsDown,
  foldedHands,
  flexedBiceps,
  microphone,
];
const ChatInput = ({
  isEditingMessage,
  messageToEdit,
  onChange,
  onSendFile,
  fileIsLoading,
  replyMessage,
  onClearReplyMessage,
}) => {
  const [message, setMessage] = useState("");
  const [isDragOver, setIsDragOver] = useState(false);
  const [isUploading, setIsUploading] = useState(false);
  const [hasFocus, setHasFocus] = useState(false);
  const textareaRef = useRef(null);
  const replyMessageTextRef = useRef(null);
  const inputFileRef = useRef(null);
  const [isFormatMenuActive, setIsFormatMenuActive] = useState(false);
  const [isEmojiBoardOpen, setIsEmojiBoardOpen] = useState(false);
  const [isBoldActive, setIsBoldActive] = useState(
    document.queryCommandState("bold")
  );
  const [isItalicActive, setIsItalicActive] = useState(
    document.queryCommandState("italic")
  );
  const [isUnderlineActive, setIsUnderlineActive] = useState(
    document.queryCommandState("underline")
  );

  useEffect(() => {
    if (
      replyMessageTextRef === null ||
      replyMessage === null ||
      replyMessage.text === null
    ) {
      return;
    }
    replyMessageTextRef.current.innerHTML = replyMessage.text;

    if (isEditingMessage) {
      textareaRef.current.innerHTML = replyMessage.text;
    }
  }, [replyMessage]);

  useEffect(() => {
    if (replyMessage) {
      focus();
    }
  }, [replyMessage]);

  const keydownHandler = (event) => {
    if (event.keyCode === 13 && event.ctrlKey) {
      stopEvent(event);
      sendText();
    }

    if (event.keyCode === 27 && replyMessage) {
      onClearReplyMessage();
    }

    delayedResize();
  };

  const handleChange = (event) => {
    setMessage(event.currentTarget.innerHTML);
    // setMessage(event.currentTarget.textContent);
    resizeTextarea();
  };

  const sendText = () => {
    if (message.length) {
      onChange(linkParser(message.trim()), messageToEdit);
      textareaRef.current.innerHTML = "";
      setMessage("");
      resizeTextarea();
      setIsEmojiBoardOpen(false);
    }
  };

  const resizeTextarea = useCallback(() => {
    const textarea = textareaRef.current;

    if (!textarea) {
      return;
    }

    textarea.style.maxHeight = "auto";
    textarea.style.maxHeight = `${
      textarea.scrollHeight <= MAX_HEIGHT ? textarea.scrollHeight : MAX_HEIGHT
    }px`;
  }, [textareaRef]);

  const onPaste = (e) => {
    e.preventDefault();
    const text = (e.originalEvent || e).clipboardData.getData("text/plain");
    document.execCommand("insertHTML", false, text);
  };

  const delayedResize = useCallback(
    (e) => {
      window.setTimeout(resizeTextarea, 0);
    },
    [resizeTextarea]
  );

  const focus = useCallback(() => {
    if (textareaRef.current) {
      textareaRef.current.focus();
    }
    setIsBoldActive(document.queryCommandState("bold"));
    setIsItalicActive(document.queryCommandState("italic"));
    setIsUnderlineActive(document.queryCommandState("underline"));
  }, [textareaRef]);

  //region Send File
  const stopEvent = (event) => {
    event.preventDefault();
    event.stopPropagation();
  };

  const dragHandler = (event) => {
    stopEvent(event);
  };

  const dragOverHandler = (event) => {
    stopEvent(event);
    if (isUploading) return false;
    setIsDragOver(true);
  };

  const dragOverEndHandler = (event) => {
    stopEvent(event);
    if (isUploading) return false;
    setIsDragOver(false);
  };

  const dropHandler = (event) => {
    stopEvent(event);
    if (isUploading) return false;

    setIsDragOver(false);
    triggerFormSubmit(event.dataTransfer.files);
  };

  const inputOnChangeHandler = (event) => {
    if (isUploading) return false;
    if (event.target.files[0].size > 1024 * 1024 * MAX_FILE_SIZE_MB) {
      return toast.error(
        `Размер файла не может превышать ${MAX_FILE_SIZE_MB} Mb`
      );
    }
    triggerFormSubmit(event.target.files);
  };

  const inputOnFocusHandler = () => setHasFocus(true);

  const inputOnBlurHandler = () => setHasFocus(false);

  const submitHandler = (eventSubmit) => stopEvent(eventSubmit);

  const triggerFormSubmit = (files) => {
    setIsUploading(true);
    try {
      if (files && files.length) {
        onSendFile(files);
        inputFileRef.current.value = "";
      }
    } catch (e) {}
    setTimeout(() => {
      setIsUploading(false);
      inputOnBlurHandler();
    }, 300);
  };
  //endregion

  useEffect(() => {
    if (isEditingMessage && textareaRef) {
      setMessage(replyMessage.text);
    }
  }, [isEditingMessage]);

  function onCloseReplyMessage() {
    if (isEditingMessage) {
      setMessage("");
      textareaRef.current.innerHTML = "";
    }
    setMessage("");
    onClearReplyMessage();
  }

  function onMouseUp() {
    setIsBoldActive(document.queryCommandState("bold"));
    setIsItalicActive(document.queryCommandState("italic"));
    setIsUnderlineActive(document.queryCommandState("underline"));
  }
  return (
    <div className="chat-input">
      {replyMessage && (
        <div className="chat-input__reply-message">
          <span className="chat-inpmessageut__reply-message-icon">>></span>
          <div className="chat-input__reply-message-box">
            {replyMessage.type !== MESSAGE_TYPE.text && (
              <div className="chat-input__reply-message-type">
                <FaFile />
                <span className="chat-input__reply-message-type-text">
                  {MESSAGE_TYPE_RU[replyMessage.type]}{" "}
                  {replyMessage.file ? `(${replyMessage.file.name})` : ""}
                </span>
              </div>
            )}
            {replyMessage.text && (
              <p
                ref={replyMessageTextRef}
                className="chat-input__reply-message-text"
              ></p>
            )}
          </div>
          <button className="chat-input__close" onClick={onCloseReplyMessage}>
            <CloseIcon />
          </button>
        </div>
      )}
      <form
        encType="multipart/form-data"
        className={classes("chat-input__form", "has-advanced-upload", {
          "is-dragover": isDragOver,
          "is-uploading": isUploading,
        })}
        onSubmit={submitHandler}
        onDrag={dragHandler}
        onDragStart={dragHandler}
        onDragEnd={dragOverEndHandler}
        onDragOver={dragOverHandler}
        onDragEnter={dragOverHandler}
        onDragLeave={dragOverEndHandler}
        onDrop={dropHandler}
      >
        <div className="chat-input__field">
          <div className="chat-input__emojies-section">
            {" "}
            <button
              onClick={() => {
                if (textareaRef === null) {
                  return;
                }
                textareaRef.current.focus();
                setIsEmojiBoardOpen((prev) => !prev);
              }}
              className="chat-input__button"
            >
              <img height={30} width={30} src={emojiMenu} />
            </button>
            <div
              style={{ display: isEmojiBoardOpen ? "flex" : "none" }}
              className="chat-input__emojies-board"
            >
              {emojiesList.map((emojiData, i) => {
                return (
                  <img
                    id="emoji"
                    onClick={(e) => {
                      textareaRef.current.focus();

                      let emojiTemplate = document.createElement("img");
                      emojiTemplate.classList.add("emoji");
                      emojiTemplate.src = e.currentTarget.src;

                      let sel, range, html;
                      if (window.getSelection) {
                        sel = window.getSelection();
                        if (sel.getRangeAt && sel.rangeCount) {
                          range = sel.getRangeAt(0);
                          range.deleteContents();
                          range.insertNode(emojiTemplate);

                          var textNode = document.createTextNode("\u00A0");
                          range.setStartAfter(emojiTemplate);
                          range.insertNode(textNode);
                          range.setStartAfter(textNode);
                          range.collapse(true);
                          sel = window.getSelection();
                          sel.removeAllRanges();
                          sel.addRange(range);

                          setMessage(textareaRef.current.innerHTML);
                        }
                      }
                    }}
                    className="emoji-img"
                    src={emojiData}
                  />
                );
              })}
            </div>
          </div>
        </div>
        <div
          autoFocus
          ref={textareaRef}
          className="chat-input__textarea"
          rows="2"
          placeholder="Введите ваше сообщение"
          value={message}
          placeholder="Введите сообщение"
          onInput={handleChange}
          onKeyDown={keydownHandler}
          onDrop={delayedResize}
          onCut={delayedResize}
          onPaste={onPaste}
          onMouseUp={onMouseUp}
          contentEditable
        ></div>
        <button
          style={{ display: "flex" }}
          onClick={() => {
            setIsFormatMenuActive((prev) => !prev);
          }}
        >
          {" "}
          {isFormatMenuActive ? <img src={upArrow} /> : <img src={downArrow} />}
        </button>
        <div className="chat-input__field">
          <label
            onClick={inputOnFocusHandler}
            className={classes("chat-input__file-label", {
              "has-focus": hasFocus,
            })}
            title={`Максимальный размер файла ${MAX_FILE_SIZE_MB} Мб`}
          >
            {fileIsLoading ? <Loader size="s" /> : <UploadImg />}
            <input
              ref={inputFileRef}
              type="file"
              name="files[]"
              multiple
              className={classes("chat-input__file-field", {
                "has-focus": hasFocus,
              })}
              onChange={inputOnChangeHandler}
              disabled={fileIsLoading}
            />
          </label>
        </div>
        <div className="chat-input__field">
          <button className="chat-input__button" onClick={sendText}>
            <img src={telegramImg} alt="telegramImg" title="Ctr+Enter" />
          </button>
        </div>
      </form>
      <div
        style={{ display: isFormatMenuActive ? "flex" : "none" }}
        className="chat-input__format-menu"
      >
        <button
          title="Жирный"
          style={{
            border: isBoldActive && "1px solid gray",
            borderRadius: "5px",
          }}
          className="chat-input-format-button"
          onClick={() => {
            document.execCommand("bold");
            focus();
            setIsBoldActive(document.queryCommandState("bold"));
          }}
        >
          <img className="chat-input-format-icon" src={bold} />
        </button>
        <button
          title="Курсив"
          style={{
            border: isItalicActive && "1px solid gray",
            borderRadius: "5px",
          }}
          className="chat-input-format-button"
          onClick={() => {
            document.execCommand("italic");
            focus();
            setIsItalicActive(document.queryCommandState("italic"));
          }}
        >
          {" "}
          <img className="chat-input-format-icon" src={italic} />
        </button>
        <button
          title="Подчеркивание"
          style={{
            border: isUnderlineActive && "1px solid gray",
            borderRadius: "5px",
          }}
          className="chat-input-format-button"
          onClick={() => {
            document.execCommand("underline");
            focus();
            setIsUnderlineActive(document.queryCommandState("underline"));
          }}
        >
          <img className="chat-input-format-icon" src={underline} />
        </button>
      </div>
    </div>
  );
};

export default ChatInput;

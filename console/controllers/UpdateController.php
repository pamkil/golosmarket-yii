<?php

namespace console\controllers;

use Carbon\Carbon;
use common\models\access\User;
use common\models\announcer\Announcer;
use common\models\announcer\AnnouncerSound;
use common\models\user\Bill;
use common\models\user\Chat;
use common\models\user\Favorite;
use common\models\user\Notification;
use common\models\user\Review;
use common\models\user\SendNotify;
use common\service\Epochta;
use common\service\Mail;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Expression;
use yii\helpers\Console;

/**
 * Page controller
 */
class UpdateController extends Controller
{

    public function actionIndex()
    {
        Console::output('Controller test.');
        return ExitCode::OK;
    }

    public function actionAddGenderToSounds()
    {
        Console::output('Start update sounds');

        /**
         */
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $announcersCount = Announcer::find()->count();
            $i = 0;
            Console::startProgress($i, $announcersCount, 'Announcers update: ');

            foreach (Announcer::find()->batch() as $announcers) {
                /** @var Announcer $announcer */
                foreach ($announcers as $announcer) {
                    ++$i;
                    /** @var AnnouncerSound $sound */
                    foreach ($announcer->sounds as $sound) {
                        $sound->gender_id = $announcer->genders[0]->id ?? null;
                        $sound->save();
                    }
                    Console::updateProgress($i, $announcersCount);
                }
            }
            $transaction->commit();
            Console::endProgress();
        } catch (\Exception $exception) {
            $transaction->rollBack();

            throw $exception;
        }

        Console::output('End update sounds.');
        return ExitCode::OK;
    }

    public function actionCheckPayed()
    {
        Console::output('Check commission payment.');

        $announcers = Bill::find()->select('*')
            ->andWhere([
                'is_send' => true
            ]);

        return ExitCode::OK;
    }

    public function actionAutoConfirmPayed()
    {
        $items = Bill::find()->select('*')
            ->andWhere([
                'status' => Bill::STATUS_PAID_CLIENT,
                'is_canceled' => false,
                'is_payed' => false,
                'is_send' => false
            ])
            ->andWhere('card_number IS NOT NULL')
            ->all();

        /** @var Bill $item */
        foreach ($items as $item) {
            $now = Carbon::now();
            $expiration = Carbon::createFromDate($item->updated_at)->addMinutes(15);

            if ($now->greaterThan($expiration)) {
                $item->status = Bill::STATUS_PAID_ANNOUNCER;
                $item->is_payed = 1;

                if (!$item->save()) {
                    Yii::error(['error' => 'Не удалось поменять статус оплаты', 'messages' => $item->getErrors()], 'console');
                }
            }
        }

        return ExitCode::OK;
    }
}

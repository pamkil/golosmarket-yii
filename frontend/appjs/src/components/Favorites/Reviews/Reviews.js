import React, {useRef} from "react";
import Slider from "react-slick";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import prevImg from "../../../images/prev.svg";
import nextImg from "../../../images/next.svg";
import ReviewItem from "./ReviewItem";

const Reviews = ({reviews}) => {
  const sliderRef = useRef(null);
  const slickNext = () => {
    if (sliderRef) {
      sliderRef.current.slickNext();
    }
  }
  const slickPrev = () => {
    if (sliderRef) {
      sliderRef.current.slickPrev();
    }
  }

  const settings = {
    dots: false,
    arrows: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    rows: 1,
    slidesPerRow: 1,
    responsive: [
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          initialSlide: 1
        }
      },
      {
        breakpoint: 680,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  return (
    <div className="container no-flex">
      <div className="headtitle">Отзывы о сервисе</div>
      <div className="slick-reviews">
        <div className="prev slick-arrow" onClick={slickPrev}>
          <img src={prevImg} alt="prev"/>
        </div>
        <div className="next slick-arrow" onClick={slickNext}>
          <img src={nextImg} alt="next"/>
        </div>

        <Slider {...settings} ref={sliderRef}>
          {
            reviews.map((item, ind) => <ReviewItem key={ind} {...item} />)
          }
        </Slider>
      </div>
    </div>
  );
}

export default Reviews;

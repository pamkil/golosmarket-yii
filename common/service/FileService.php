<?php

namespace common\service;

use Yii;

class FileService
{
    public function RemoveEmptySubFolders(string $path = '')
    {
        $empty=true;

        if (empty($path)) {
            $path = Yii::getAlias('@uploadPath');
        }

        foreach (glob($path.DIRECTORY_SEPARATOR."*") as $file) {
            if (is_dir($file)) {
                if (!self::RemoveEmptySubFolders($file)) {
                    $empty = false;
                }
            } else {
                $empty = false;
            }
        }

        if ($empty) rmdir($path);

        return $empty;
    }
}
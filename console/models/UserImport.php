<?php

namespace console\models;

use api\modules\v1\models\records\auth\Token;
use api\modules\v1\models\records\auth\User;
use common\models\announcer\Age;
use common\models\announcer\Announcer;
use common\models\announcer\Gender;
use common\models\announcer\Language;
use common\models\announcer\Presentation;
use common\models\announcer\Tag;
use common\models\announcer\Timber;
use common\models\user\City;
use Exception;
use Yii;
use yii\helpers\ArrayHelper;

class UserImport
{
    private $data;

    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return Token|User|bool|void|null
     * @throws \yii\base\Exception
     * @throws Exception
     */
    public function run()
    {
        if ($this->data['typ'] && (int)$this->data['typ'] !== 0) {
            return !!$this->create((int)$this->data['typ']);
        }

        return false;
    }

    /**
     * @param array $data
     *
     * @return Token|User|null
     * @throws \yii\base\Exception
     * @throws Exception
     */
    private function addModel(array $data)
    {
        $roles = [User::ROLE_GUEST];

        if ($data['type'] === User::TYPE_ANNOUNCER) {
            $roles[] = User::ROLE_ANNOUNCER;
        } else {
            $roles[] = User::ROLE_CLIENT;
        }

        $transaction = Yii::$app->db->beginTransaction();
        $user = User::findOne(['email' => $data['email']]);
        if (!$user) {
            $user = new User();
        }
        $user->load($data, '');
        $user->generateAuthKey();
//        $user->generatePasswordResetToken();

        try {
            if ($user->save()) {
                try {
                    if (!empty($data['created_at'])) {
                        $user->created_at = $data['created_at'];
                        $user->save();
                    }
                } catch (\Exception $exception)
                {}

                $user->loadRoles($roles ?? []);

                if (isset($data['announcer'])) {
                    $announcer = $user->announcer;
                    if (!$announcer) {
                        $announcer = new Announcer(['user_id' => $user->id]);
                    }
                    $announcer->load($data['announcer'], '');
                    $announcer->save();
                }

                $result = $user->auth();

            } else {
                $result = $user;
            }
            $transaction->commit();
            return $result;
        } catch (Exception $exception) {
            $transaction->rollBack();

            throw $exception;
        }
    }

    /**
     * @return Token|User|null
     * @throws \yii\base\Exception
     */
    private function create(int $typ)
    {
        $isAnnouncer = $typ === 1;
        $fields = [
            'id' => 'old_id',
            'name' => 'firstname',
            'fam' => 'lastname',
            'email' => 'email',
            'phone' => 'phone',
            'ban' => 'is_blocked',
            'city' => 'city_id',
            'password' => 'password',
            'type' => 'type',
            'datestart' => 'created_at',
        ];
        $announcerFields = [
            'fieldOld' => 'fieldNew',
            'grafik' => 'operating_schedule',
            'rentnachitka' => 'cost',
            'format_a4' => 'cost_a4',
            'discount' => 'discount',
            'yamoney' => 'cash_yamoney',
            'sber' => 'cash_sber',
            'dopinfo' => 'info',
            'obor' => 'equipment',
            'plus' => 'likes',
            'minus' => 'dislikes',
        ];

        if (!empty($this->data['password']) && $len = (mb_strlen($this->data['password'])) < 6) {
            if ($len >= 3) {
                $this->data['password'] .= $this->data['password'];
            } else {
                $this->data['password'] .= $this->data['password'] . $this->data['password'];
            }
        }

        $data = [
            'type' => $isAnnouncer ? User::TYPE_ANNOUNCER : User::TYPE_CLIENT,
        ];

        foreach ($fields as $fieldOld => $fieldNew) {
            if (isset($this->data[$fieldOld])) {
                $data[$fieldNew] = $this->data[$fieldOld] ?? null;
                if (!empty($data[$fieldNew])) {
                    $data[$fieldNew] = trim($data[$fieldNew]);
                }
            }
        }

        if ($isAnnouncer) {
            foreach ($announcerFields as $fieldOld => $fieldNew) {
                if (isset($this->data[$fieldOld])) {
                    $data['announcer'][$fieldNew] = $this->data[$fieldOld] ?? null;
                    if (!empty($data['announcer'][$fieldNew])) {
                        $data['announcer'][$fieldNew] = trim($data['announcer'][$fieldNew]);
                    }
                }
            }

            $data['announcer'] = $this->addTags($data['announcer'] ?? []);
            $data['announcer'] = $this->addLangs($data['announcer'] ?? []);
//            $data['announcer'] = $this->addPresents($data['announcer'] ?? []);
            $data['announcer'] = $this->addAge($data['announcer'] ?? []);
            $data['announcer'] = $this->addTimber($data['announcer'] ?? []);
            $data['announcer'] = $this->addGender($data['announcer'] ?? []);
            $data['announcer'] = $this->addLikes($data['announcer'] ?? []);
        }
        $data = $this->addCity($data);

        return $this->addModel($data);
    }

    private function addCity(array $data)
    {
        if (!empty($data['city_id'])) {
            $city = City::find()->where(['title' => $data['city_id']])->asArray()->one();
            if (!$city) {
                $city = City::create(['title' => $data['city_id']], false);
            }
            $data['city_id'] = $city['id'];
        } else {
            unset($data['city_id']);
        }

        return $data;
    }

    private function addTags(array $dataAn)
    {
        $oldTags = explode(',', $this->data['tegs']);
        if (count($oldTags) === 1) {
            $oldTags = explode(PHP_EOL, $this->data['tegs']);
        }
        $oldTags = array_map('trim', $oldTags);

        $newTags = [];

        $tagsModel = Tag::find()
            ->where(['name' => $oldTags])
            ->asArray()
            ->all();

        $tagsModel = ArrayHelper::index($tagsModel, function ($model) {
            return mb_substr(str_replace('ё', 'е', mb_strtolower($model['name'])), 0, 25);
        });

        foreach ($oldTags as $tagText) {
            $tagTextKey = mb_substr(str_replace('ё', 'е', mb_strtolower($tagText)), 0, 25);
            if (isset($tagsModel[$tagTextKey])) {
                $tagModel = $tagsModel[$tagTextKey];
            } else {
                $tagModel = $tagsModel[$tagTextKey] = Tag::create(['active' => true, 'name' => $tagText], false);
            }
            if (isset($tagModel['id'])) {
                $newTags[] = $tagModel['id'];
            }
        }

        $dataAn['tags'] = array_unique($newTags);

        return $dataAn;
    }

    private function getExplode($value, string $delimiter = '|')
    {
        $data = explode($delimiter, $value ?? '');
        return array_filter($data);
    }

    private function getNewIds($oldValues, $newValues, $covertArray)
    {
        $results = [];
        foreach ($oldValues as $value) {
            if (isset($covertArray[$value])) {
                $results[] = $newValues[$covertArray[$value]];
            }
        }

        return $results;
    }

    private function addLangs(array $dataAn)
    {
        $oldLangs = $this->getExplode($this->data['lang']);
        $dataAn['languages'] = $this->getNewIds($oldLangs, $this->getNewLanguages(), self::$oldLang);

        return $dataAn;
    }

    private function addPresents(array $dataAn)
    {
        $old = $this->getExplode($this->data['dop']);
        $dataAn['presentations'] = $this->getNewIds($old, $this->getNewPresents(), self::$oldPresent);

        return $dataAn;
    }

    private function addAge(array $dataAn)
    {
        $old = $this->getExplode($this->data['old']);
        $dataAn['ages'] = $this->getNewIds($old, $this->getNewAges(), self::$oldAge);

        return $dataAn;
    }

    private function addTimber(array $dataAn)
    {
        $old = $this->getExplode($this->data['tembr']);
        $dataAn['timbers'] = $this->getNewIds($old, $this->getNewTimbers(), self::$oldTimber);

        return $dataAn;
    }

    private function addGender(array $dataAn)
    {
        $old = $this->getExplode($this->data['sex']);
        $dataAn['genders'] = $this->getNewIds($old, $this->getNewGender(), self::$oldGender);

        return $dataAn;
    }

    private function addLikes(array $dataAn)
    {
//        $oldLangs = $this->getExplode($dataAn['lang']);
//        $dataAn['languages'] = $this->getNewIds($oldLangs, $this->getNewLanguages(), self::$oldLang);

        return $dataAn;
    }


    private static $langs;

    private function getNewLanguages()
    {
        if (!self::$langs) {
            self::$langs = Language::find()
                ->select(['id', 'code'])
                ->indexBy('code')
                ->column();
        }

        return self::$langs;
    }

    private static $presents;

    private function getNewPresents()
    {
        if (!self::$presents) {
            self::$presents = Presentation::find()
                ->select(['id', 'code'])
                ->indexBy('code')
                ->column();
        }

        return self::$presents;
    }

    private static $ages;

    private function getNewAges()
    {
        if (!self::$ages) {
            self::$ages = Age::find()
                ->select(['id', 'code'])
                ->indexBy('code')
                ->column();
        }

        return self::$ages;
    }

    private static $timbers;

    private function getNewTimbers()
    {
        if (!self::$timbers) {
            self::$timbers = Timber::find()
                ->select(['id', 'code'])
                ->indexBy('code')
                ->column();
        }

        return self::$timbers;
    }

    private static $genders;

    private function getNewGender()
    {
        if (!self::$genders) {
            self::$genders = Gender::find()
                ->select(['id', 'code'])
                ->indexBy('code')
                ->column();
        }

        return self::$genders;
    }

    private static $oldLang = [
        1 => 'russian',
        2 => 'ukrainian',
        3 => 'belorussian',
        4 => 'kazakh',
        5 => 'kyrgyz',
        6 => 'english',
        7 => 'deutsch',
        8 => 'french',
        9 => 'spanish',
        10 => 'italian',
        11 => 'chinese',
        18 => 'french',
        19 => 'tajik',
        20 => 'bulgarian',
        23 => 'polish',
        24 => 'greek',
        25 => 'arab',
        27 => 'tatar',
        28 => 'japanese',
        29 => 'georgian',
    ];


    private static $oldTimber = [
        1 => 'high',
        2 => 'middle',
        3 => 'low',
        4 => 'bas',
        5 => 'universal',
    ];

    private static $oldPresent = [
        1 => 'informational',
        2 => 'game',
        3 => 'announcer',
        4 => 'answering',
        5 => 'клубная',
        6 => 'vocals',
        7 => 'rap',
        8 => 'brandvois',
        9 => 'sexy',
        10 => 'romantic',
        11 => 'pathetic',
        12 => 'juicy',
        13 => 'voiced',
        14 => 'deaf',
        15 => 'entertainer',
        16 => 'parodist',

    ];

    private static $oldAge = [
        1 => 'children',
        2 => 'teenage',
        3 => 'young',
        4 => 'average',
        5 => 'mature',
        6 => 'elderly',
    ];

    private static $oldGender = [
        1 => 'man',
        2 => 'woman',
        3 => 'neuter',
    ];
}
import React from "react";
import PropTypes from 'prop-types';
import classnames from 'classnames'

import SendMessageIcon from '../../../images/send-message.svg'

const TimeButton = ({ id, timeText, isBefore, isAnnouncer, onClickSendMessage, isLogin }) => (
    <div className="block">
        <span className={ isBefore ? 'gray' : 'reds' }>{ timeText }</span>
        {
            isAnnouncer || !isLogin
                ? <button
                    className={classnames('mobile-no', 'write', {'hide-btn': isAnnouncer})}
                    type="button"
                    disabled={ isAnnouncer }
                    onClick={ onClickSendMessage }
                >
                    Написать
                </button>
                : <a className="mobile-no write" href={ `/chat/${ id }` }>Написать</a>
        }

        <div className="user-buttons-mobile">
            {
                isAnnouncer || !isLogin
                    ? <button
                        className={classnames('write', 'write-mobile', {'hide-btn': isAnnouncer})}
                        type="button"
                        disabled={ isAnnouncer }
                        onClick={ onClickSendMessage }
                    >
                        <img src={ SendMessageIcon } alt="send message" className="msg"/>
                    </button>
                    : <a href={ `/chat/${ id }` } className="write-mobile">
                        <img src={ SendMessageIcon } alt="send message" className="msg"/>
                    </a>
            }
        </div>
    </div>
);

TimeButton.propTypes = {
    time: PropTypes.string,
    onWrite: PropTypes.func,
};

TimeButton.defaultProps = {
    time: '',
    onWrite: () => {
    },
};

export default TimeButton;
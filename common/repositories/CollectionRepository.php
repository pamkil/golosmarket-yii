<?php

namespace common\repositories;

use common\models\content\Collection;
use common\repositories\contract\CollectionRepositoryContract;
use common\service\collection\enum\CollectionStatusEnum;
use yii\web\NotFoundHttpException;

class CollectionRepository implements CollectionRepositoryContract
{
    public function getAll(): array
    {
        return Collection::find()->all();
    }

    public function getAllActive(): array
    {
        return Collection::findAll(['active' => CollectionStatusEnum::ACTIVE]);
    }

    public function getById(int $id): ?Collection
    {
        $collection = Collection::findOne($id);

        if (!$collection) {
            throw new NotFoundHttpException();
        }

        return $collection;
    }

    public function getActiveById(int $id): ?Collection
    {
        return Collection::find()->where([
            'id' => $id,
            'active' => CollectionStatusEnum::ACTIVE
        ])->one();
    }
}

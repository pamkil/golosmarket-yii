<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_announcer_genders}}`.
 */
class m191114_165012_create_gender_table extends Migration
{
    private $genders = '{{%user_announcer_genders}}';
    private $announcer2genders = '{{%user_announcer_2_genders}}';
    private $announcer = '{{%user_announcer}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->genders, [
            'id' => $this->primaryKey(),
            'active' => $this->boolean()->notNull()->defaultValue(true),
            'position' => $this->integer(),
            'code' => $this->string(25),
            'name' => $this->string(),
            'menu_title' => $this->string(),
        ], $tableOptions);
        $this->addCommentOnTable($this->genders, 'Пол голоса');

        $this->createTable($this->announcer2genders, [
            'announcer_id' => $this->integer(),
            'gender_id' => $this->integer(),
            'PRIMARY KEY (announcer_id, gender_id)'
        ], $tableOptions);

        $this->createIndex(
            'IX-user_announcer_2_genders-announcer_id',
            $this->announcer2genders,
            'announcer_id'
        );
        $this->addForeignKey(
            'FK-user_announcer_2_genders-announcer_id',
            $this->announcer2genders,
            'announcer_id',
            $this->announcer,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('IX-user_announcer_2_genders-gender_id', $this->announcer2genders, 'gender_id');
        $this->addForeignKey(
            'FK-user_announcer_2_genders-gender_id',
            $this->announcer2genders,
            'gender_id',
            $this->genders,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addData();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->announcer2genders);
        $this->dropTable($this->genders);
    }

    private function addData()
    {
        $this->batchInsert(
            $this->genders,
            ['name', 'menu_title', 'code', 'position', 'active'],
            [
                ['Мужской', 'Мужской голос', 'man', 1, 1],
                ['Женский', 'Женский голос', 'woman', 2, 1],
                ['Средний', 'Средний голос', 'neuter', 3, 0],
            ]
        );
    }
}

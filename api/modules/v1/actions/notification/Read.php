<?php

namespace api\modules\v1\actions\notification;

use common\models\user\Notification;
use Exception;
use Yii;
use yii\rest\CreateAction;
use yii\web\BadRequestHttpException;

/**
 * @OA\Post(
 *      path="/notify",
 *      summary="Пометить как прочитано",
 *      tags={"Notify"},
 *      @OA\Response(
 *          response="200",
 *          description="Объект токена",
 *          @OA\JsonContent(ref="#/components/schemas/Notification")
 *      ),
 *      @OA\Response(
 *          response="400",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="401",
 *          description="Unauthorized",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      ),
 *      @OA\Response(
 *          response="default",
 *          description="unexpected error",
 *          @OA\Schema(ref="#/components/schemas/errorModel")
 *      )
 * )
 */

/**
 * Class Create
 * @package api\modules\v1\actions\feedback
 *
 */
class Read extends CreateAction
{
    /**
     * @return \yii\web\IdentityInterface|null
     * @throws BadRequestHttpException
     */
    public function run()
    {
        $ids = Yii::$app->request->post('ids');

        if (empty($ids)) {
            throw new BadRequestHttpException("Не задан параметр ids");
        }

        $userId = Yii::$app->user->getId();
        if (empty($userId)) {
            throw new BadRequestHttpException("Не авторизован");
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $condition['user_id'] = $userId;
            if ($ids !== 'all') {
                $ids = array_map('intval', $ids);
                $condition['id'] = $ids;
            }

            Notification::updateAll(
                [
                    'status' => Notification::STATUS_READ,
                ],
                $condition
            );

            $transaction->commit();
        } catch (Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }

        return Yii::$app->user->identity;
    }
}
